using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using DataProcessingBlocks;
using System.Diagnostics;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks.MessageRule
{
    public partial class RuleDetails : Form
    {
        #region Private Members
        private static RuleDetails m_RuleDetails;

        private string ProductCaption = EDI.Constant.Constants.ProductName;

        private static long m_MessageRuleID = -1;

        private static bool temp;

        private MailSettings.MessageRules m_MessageRule = new DataProcessingBlocks.MailSettings.MessageRules();

        private static long m_RuleId;
        private static string m_Name;
        private static string m_TradingPartner;
        private static string m_MsgExactMatch;
        private static string m_MsgSpecificWordMatch;
        private static string m_FileExactMatch;
        private static string m_FileSpecificWordMatch;
        private static string m_TransactionType;
        private static string m_UseMapping;
        private static string m_SuccessStatus;
        private static string m_ErrorStatus;
        private static string m_RuleStatus;
        private static bool m_SoundChime;
        private static bool m_SoundAlarm;

        #endregion

        #region Constructor
        public RuleDetails()
        {
            InitializeComponent();
        }

        public RuleDetails(bool is_button)
        {
            temp = is_button;
        }

        public RuleDetails(long RuleId, string Name, string TradingPartner, string MsgExactMatch, string MsgSpecificWordMatch, string FileExactMatch, string FileSpecificWordMatch, string TransactionType, string UseMapping, string SuccessStatus, string ErrorStatus, string RuleStatus, bool Chime, bool Alarm)
        {
            m_RuleId = RuleId;
            m_Name = Name;
            m_TradingPartner = TradingPartner;
            m_MsgExactMatch = MsgExactMatch;
            m_MsgSpecificWordMatch = MsgSpecificWordMatch;
            m_FileExactMatch = FileExactMatch;
            m_FileSpecificWordMatch = FileSpecificWordMatch;
            m_TransactionType = TransactionType;
            m_UseMapping = UseMapping;
            m_SuccessStatus = SuccessStatus;
            m_ErrorStatus = ErrorStatus;
            m_RuleStatus = RuleStatus;
            m_SoundChime = Chime;
            m_SoundAlarm = Alarm;
        }
        #endregion

        #region Public Methods
        public static RuleDetails GetInstance()
        {
            if (m_RuleDetails == null)
                m_RuleDetails = new RuleDetails();
            return m_RuleDetails;
        }

      
        #endregion

        #region Event Handler

        private void buttonCancelOne_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            MessageRule.GetInstance().ShowRulesDataGridView();
            m_RuleDetails = null;
        }

        private void buttonPrevious_Click(object sender, EventArgs e)
        {
          //  panelMsgRuleTwo.Visible = false;
            panelMsgRuleOne.Visible = true;
        }

        private void buttonCancelTwo_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            MessageRule.GetInstance().ShowRulesDataGridView();
            m_RuleDetails = null;
        }
        #endregion

        private void RuleDetails_Load(object sender, EventArgs e)
        {
            if (temp)
            {
                //If new button is clicked.
                Initialise();
                m_MessageRuleID = -1;
            }
            else if (temp == false)
            {
                //If Edit button is clicked.
                Initialise();

                m_MessageRuleID = m_RuleId;
                textBoxRuleName.Text = m_Name;

                int indexTp = -1;
                //DataView dtTPView = ((DataTable)comboBoxTradingPartner.DataSource).DefaultView;
                for(int count = 0;count < comboBoxTradingPartner.Items.Count; count++)
                {
                    if(m_TradingPartner == comboBoxTradingPartner.Items[count].ToString())
                    {
                        indexTp = count;
                        break;
                    }
                }

                //for (int count = 0; count < dtTPView.Count; count++)
                //{
                //    if (m_TradingPartner == dtTPView[count].Row[0].ToString())
                //    {
                //        indexTp = count;
                //        break;
                //    }
                //}
                comboBoxTradingPartner.SelectedIndex = indexTp;

                if (!string.IsNullOrEmpty(m_MsgExactMatch))
                {
                    textBoxSubjectExactMatch.Text = m_MsgExactMatch;
                    radioButtonSubjectExactMatch.Checked = true;
                }
                if (!string.IsNullOrEmpty(m_MsgSpecificWordMatch))
                {
                    textBoxSubjectSpecificWords.Text = m_MsgSpecificWordMatch;
                    radioButtonSubjectSpecificWords.Checked = true;
                }
                if (!string.IsNullOrEmpty(m_FileExactMatch))
                {
                    textBoxFileExactMatch.Text = m_FileExactMatch;
                    radioButtonFileExactMatch.Checked = true;
                }
                if (!string.IsNullOrEmpty(m_FileSpecificWordMatch))
                {
                    textBoxFileSpecificWord.Text = m_FileSpecificWordMatch;
                    radioButtonFileSpecificWord.Checked = true;
                }
                comboBoxTransactionTypes.Text = m_TransactionType;

                int indexMapping = -1;
                for (int count = 0; count < comboBoxUseMapping.Items.Count; count++)
                {
                    if (comboBoxUseMapping.Items[count].Equals(m_UseMapping))
                    {
                        indexMapping = count;
                        break;
                    }
                }
                comboBoxUseMapping.SelectedIndex = indexMapping;

                int indexSuccess = -1;
                DataView dtSuccessView = ((DataTable)comboBoxSuccessStatus.DataSource).DefaultView;
                for (int count = 0; count < dtSuccessView.Count; count++)
                {
                    if (m_SuccessStatus == dtSuccessView[count].Row[0].ToString())
                    {
                        indexSuccess = count;
                        break;
                    }
                }
                comboBoxSuccessStatus.SelectedIndex = indexSuccess;

                int indexError = -1;
                DataView dtErrorView = ((DataTable)comboBoxErrorStatus.DataSource).DefaultView;
                for (int count = 0; count < dtErrorView.Count; count++)
                {
                    if (m_ErrorStatus == dtErrorView[count].Row[0].ToString())
                    {
                        indexError = count;
                        break;
                    }
                }
                comboBoxErrorStatus.SelectedIndex = indexError;

                if (m_RuleStatus == "Active")
                {
                    checkBoxActive.Checked = true;
                }
                else
                {
                    checkBoxActive.Checked = false;
                }
                //if (!string.IsNullOrEmpty(comboBoxSuccessStatus.Text))
                //{
                //    checkBoxSoundChime.Enabled = true;
                //    checkBoxSoundChime.Checked = m_SoundChime;
                //}
                //if (!string.IsNullOrEmpty(comboBoxErrorStatus.Text))
                //{
                //    checkBoxSoundAlarm.Enabled = true;
                //    checkBoxSoundAlarm.Checked = m_SoundAlarm;
                //}
            }
        }

        /// <summary>
        /// Radiobutton check event for the subject. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButtonSubjectExactMatch_CheckedChanged(object sender, EventArgs e)
        {
            textBoxSubjectSpecificWords.Text = string.Empty;
            if (radioButtonSubjectExactMatch.Checked)
                textBoxSubjectExactMatch.Enabled = true;
            else
                textBoxSubjectExactMatch.Enabled = false;
        }

        /// <summary>
        /// Radiobutton check event for Subject. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButtonSubjectSpecificWords_CheckedChanged(object sender, EventArgs e)
        {
            textBoxSubjectExactMatch.Text = string.Empty;
            if (radioButtonSubjectSpecificWords.Checked)
                textBoxSubjectSpecificWords.Enabled = true;
            else
                textBoxSubjectSpecificWords.Enabled = false;
        }

        /// <summary>
        /// Radiobutton check for the File.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButtonFileExactMatch_CheckedChanged(object sender, EventArgs e)
        {
            textBoxFileSpecificWord.Text = string.Empty;
            if (radioButtonFileExactMatch.Checked)
                textBoxFileExactMatch.Enabled = true;
            else
                textBoxFileExactMatch.Enabled = false;
        }

        /// <summary>
        /// Radiobutton check for the File attachment.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButtonFileSpecificWord_CheckedChanged(object sender, EventArgs e)
        {
            textBoxFileExactMatch.Text = string.Empty;
            if (radioButtonFileSpecificWord.Checked)
                textBoxFileSpecificWord.Enabled = true;
            else
                textBoxFileSpecificWord.Enabled = false;
        }

        /// <summary>
        /// Function initialises the form.
        /// </summary>
        public void Initialise()
        {
            textBoxRuleName.Focus();
            textBoxRuleName.Text = string.Empty;
            radioButtonSubjectExactMatch.Checked = false;
            radioButtonSubjectSpecificWords.Checked = false;
            radioButtonFileExactMatch.Checked = false;
            radioButtonFileSpecificWord.Checked = false;
            textBoxSubjectExactMatch.Text = string.Empty;
            textBoxSubjectSpecificWords.Text = string.Empty;
            textBoxFileExactMatch.Text = string.Empty;
            textBoxFileSpecificWord.Text = string.Empty;
            textBoxSubjectExactMatch.Enabled = false;
            textBoxSubjectSpecificWords.Enabled = false;
            textBoxFileExactMatch.Enabled = false;
            textBoxFileSpecificWord.Enabled = false;
            comboBoxTransactionTypes.SelectedIndex = 0;
            textBoxRuleName.Focus();
            //change by mayura
            checkBoxActive.Checked = true;
            //checkBoxSoundAlarm.Checked = false;
            //checkBoxSoundChime.Checked = false;
            //checkBoxSoundAlarm.Enabled = false;
            //checkBoxSoundChime.Enabled = false;

            //comboBoxTradingPartner.DataSource = MailSettings.MessageRules.GetTradingPartner();
            //comboBoxTradingPartner.DisplayMember = DataProcessingBlocks.TradingPartnerColumns.EmailID.ToString();
            //comboBoxTradingPartner.ValueMember = DataProcessingBlocks.TradingPartnerColumns.EmailID.ToString();
            comboBoxTradingPartner.Items.Clear();
            DataTable tpName = MailSettings.MessageRules.GetTradingPartner();
            DataTable serviceNames = MailSettings.MessageRules.GetServiceName();

            foreach (DataRow tp in tpName.Rows)
                comboBoxTradingPartner.Items.Add(tp[0].ToString());


            foreach (DataRow dr in serviceNames.Rows)         
                comboBoxTradingPartner.Items.Add(dr[0].ToString());

            comboBoxTradingPartner.SelectedIndex = -1;


            comboBoxSuccessStatus.DataSource = MailSettings.MessageRules.GetMessageStatus();
            comboBoxSuccessStatus.DisplayMember = DataProcessingBlocks.MessageStatusColumns.Name.ToString();
            comboBoxSuccessStatus.ValueMember = DataProcessingBlocks.MessageStatusColumns.Name.ToString();
            comboBoxSuccessStatus.SelectedIndex = -1;

            comboBoxErrorStatus.DataSource = MailSettings.MessageRules.GetMessageStatus();
            comboBoxErrorStatus.DisplayMember = DataProcessingBlocks.MessageStatusColumns.Name.ToString();
            comboBoxErrorStatus.ValueMember = DataProcessingBlocks.MessageStatusColumns.Name.ToString();
            comboBoxErrorStatus.SelectedIndex = -1;

            comboBoxUseMapping.Items.Clear();
            GetMappings();
            comboBoxUseMapping.SelectedIndex = -1;
        }

        /// <summary>
        /// Get mappings from settings.xml file.
        /// </summary>
        public void GetMappings()
        {
            string settingsPath = string.Empty;
            //settingsPath = Application.StartupPath;
            //settingsPath += @"\Settings.xml";

          //  settingsPath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
            //System.IO.Directory.CreateDirectory(m_settingsPath);
           
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    //if (settingsPath.Contains("Program Files (x86)"))
                    //{
                    //    settingsPath = settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                    //}
                    //else
                    //{
                 
                       // settingsPath = settingsPath.Replace("Program Files", Constants.xpPath);
                  //  settingsPath = settingsPath.Replace("Program Files", Constants.xpPath);
                    settingsPath = settingsPath.Replace(Constants.xpmsgPath, "Axis");
                 
                    //}
                }
                else
                {
                    if (settingsPath.Contains("Program Files (x86)"))
                    {
                        settingsPath = settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    //else
                    //{
                    //    settingsPath = settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                    //}
                    else
                    {

                        settingsPath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                        settingsPath += @"\Settings.xml";

                    }
                }    
            }
            catch { }
          // settingsPath += @"\Settings.xml";

            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(settingsPath);
            }
            catch (Exception) { }
            try
            {
                XmlNode root = (XmlNode)xmlDoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.Name == "name")
                            {
                                comboBoxUseMapping.Items.Add(node.FirstChild.InnerText.ToString());
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// This function resets every field of the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonReset_Click(object sender, EventArgs e)
        {
            Initialise();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxRuleName.Text.Trim()))
            {
                MessageBox.Show("Please enter the rule name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxRuleName.Focus();
                return;
            }
            if (MailSettings.MessageRules.IsExistRule(textBoxRuleName.Text, m_MessageRuleID))
            {
                MessageBox.Show("The rule name already exist. Please enter different name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxRuleName.Focus();
                return;
            }

            if (string.IsNullOrEmpty(comboBoxTradingPartner.Text.Trim()))
            {
                MessageBox.Show("Please select the Trading Partner.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxTradingPartner.Focus();
                return;
            }
            if (radioButtonFileExactMatch.Checked == false && radioButtonFileSpecificWord.Checked == false)
            {
                MessageBox.Show("Please enter the criteria for the File attachment.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radioButtonFileExactMatch.Focus();
                return;
            }
            if (string.IsNullOrEmpty(comboBoxTransactionTypes.Text.Trim()) || comboBoxTransactionTypes.SelectedIndex == -1)
            {
                MessageBox.Show("Please select the transaction type.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxTransactionTypes.Focus();
                return;
            }

            if (comboBoxUseMapping.Items.Count == 0)
            {
                MessageBox.Show("Please first create Mappings.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxUseMapping.Focus();
                return;
            }

            if (string.IsNullOrEmpty(comboBoxUseMapping.Text.Trim()) || comboBoxUseMapping.SelectedIndex == -1)
            {
                MessageBox.Show("Please select the Mappings.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxUseMapping.Focus();
                return;
            }
            if (!m_MessageRule.MatchTypeWithMapping(comboBoxTransactionTypes.Text.ToString().Trim(), comboBoxUseMapping.Text.ToString().Trim()))
            {
                MessageBox.Show("Please select the correct type of mapping.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxUseMapping.Focus();
                return;
            }
            if (string.IsNullOrEmpty(comboBoxSuccessStatus.Text.Trim()) || comboBoxSuccessStatus.SelectedIndex == -1)
            {
                MessageBox.Show("Please select the status on Success.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxSuccessStatus.Focus();
                return;
            }
            if (string.IsNullOrEmpty(comboBoxErrorStatus.Text.Trim()) || comboBoxErrorStatus.SelectedIndex == -1)
            {
                MessageBox.Show("Please select the status on Error.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxErrorStatus.Focus();
                return;
            }

            //if (comboBoxSuccessStatus.SelectedIndex == -1)
            //    checkBoxSoundAlarm.Enabled = false;
            //else
            //    checkBoxSoundAlarm.Enabled = true;

            //if (comboBoxErrorStatus.SelectedIndex == -1 || string.IsNullOrEmpty(comboBoxErrorStatus.Text))
            //    checkBoxSoundChime.Enabled = false;
            //else
            //    checkBoxSoundChime.Enabled = true;


            MailSettings.MessageRules messagerules = new MailSettings.MessageRules(m_MessageRuleID, textBoxRuleName.Text.Trim(), comboBoxTradingPartner.Text.ToString(), textBoxSubjectExactMatch.Text.Trim(), textBoxSubjectSpecificWords.Text.Trim(), textBoxFileExactMatch.Text.Trim(), textBoxFileSpecificWord.Text.Trim(), comboBoxTransactionTypes.SelectedItem.ToString(), comboBoxUseMapping.Text.ToString(), comboBoxSuccessStatus.Text.ToString(), comboBoxErrorStatus.Text.ToString(), checkBoxActive.Checked.ToString(), false, false);
          // 
            DBConnection.MySqlDataAcess.ExecuteDataset1(string.Format(SQLQueries.Queries.SQ66, comboBoxSuccessStatus.Text.ToString()));

          //  DBConnection.MySqlDataAcess.ExecuteDataset1(string.Format(SQLQueries.Queries.SQ67,OB.ApplicationBlocks.Data.OdbcHelper.ds_value,);

            if (messagerules.SaveMessageRuleDetails())
            {
                Initialise();
                MessageBox.Show("Rule is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (m_MessageRuleID != -1)
                {
                    MessageRule.GetInstance().ShowRulesDataGridView();
                    this.DialogResult = DialogResult.Cancel;
                    //panelMsgRuleTwo.Visible = false;
                    panelMsgRuleOne.Visible = true;
                    this.Close();
                    DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.SQ67, OB.ApplicationBlocks.Data.OdbcHelper.ds_value, m_RuleId));
                  //  DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.SQ059,OB.ApplicationBlocks.Data.OdbcHelper.ds_value,m_RuleId));
              //  DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.SQ036,16,5));





                }
                else
                {
                    MessageRule.GetInstance().ShowRulesDataGridView();
                    //panelMsgRuleTwo.Visible = false;
                    panelMsgRuleOne.Visible = true;
                }
            }
            else
            {
                Initialise();
                DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.SQ67, OB.ApplicationBlocks.Data.OdbcHelper.ds_value, m_RuleId));
                MessageBox.Show("Rule is saved successfully.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (m_MessageRuleID != -1)
                {
                    this.DialogResult = DialogResult.Cancel;
                   // panelMsgRuleTwo.Visible = false;
                    panelMsgRuleOne.Visible = true;
                    this.Close();
                }
                else
                {
                    //panelMsgRuleTwo.Visible = false;
                    panelMsgRuleOne.Visible = true;
                }
            }
        }

        private void comboBoxSuccessStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (comboBoxSuccessStatus.SelectedIndex == -1)
            //{
            //    checkBoxSoundChime.Checked = false;
            //    checkBoxSoundChime.Enabled = false;
            //}
            //else
            //    checkBoxSoundChime.Enabled = true;
        }

        private void comboBoxErrorStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (comboBoxErrorStatus.SelectedIndex == -1)
            //{
            //    checkBoxSoundAlarm.Checked = false;
            //    checkBoxSoundAlarm.Enabled = false;
            //}
            //else
            //    checkBoxSoundAlarm.Enabled = true;
        }

        private void buttonResetCompletion_Click(object sender, EventArgs e)
        {
            comboBoxSuccessStatus.SelectedIndex = -1;
            comboBoxErrorStatus.SelectedIndex = -1;
            //checkBoxSoundAlarm.Checked = false;
            //checkBoxSoundChime.Checked = false;
            //checkBoxSoundAlarm.Enabled = false;
            //checkBoxSoundChime.Enabled = false;
        }

        private void buttonResetSubject_Click(object sender, EventArgs e)
        {
            radioButtonSubjectExactMatch.Checked = false;
            radioButtonSubjectSpecificWords.Checked = false;
            textBoxSubjectExactMatch.Text = string.Empty;
            textBoxSubjectSpecificWords.Text = string.Empty;
        }

        private void buttonResetAttachment_Click(object sender, EventArgs e)
        {
            radioButtonFileExactMatch.Checked = false;
            radioButtonFileSpecificWord.Checked = false;
            textBoxFileExactMatch.Text = string.Empty;
            textBoxFileSpecificWord.Text = string.Empty;
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxRuleName.Text.Trim()))
            {
                MessageBox.Show("Please enter the rule name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxRuleName.Focus();
                return;
            }
            if (MailSettings.MessageRules.IsExistRule(textBoxRuleName.Text, m_MessageRuleID))
            {
                MessageBox.Show("The rule name already exist. Please enter different name.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxRuleName.Focus();
                return;
            }

            if (string.IsNullOrEmpty(comboBoxTradingPartner.Text.Trim()))
            {
                MessageBox.Show("Please select the Trading Partner.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxTradingPartner.Focus();
                return;
            }
            if (radioButtonFileExactMatch.Checked == false && radioButtonFileSpecificWord.Checked == false)
            {
                MessageBox.Show("Please enter the criteria for the File attachment.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                radioButtonFileExactMatch.Focus();
                return;
            }
            //if ((radioButtonSubjectExactMatch.Checked && string.IsNullOrEmpty(textBoxSubjectExactMatch.Text.Trim())) || (radioButtonSubjectSpecificWords.Checked && string.IsNullOrEmpty(textBoxSubjectSpecificWords.Text.Trim())))
            //{
            //    MessageBox.Show("Please enter the text for the selected criteria of Message Subject.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            //    if (radioButtonSubjectExactMatch.Checked)
            //        textBoxSubjectExactMatch.Focus();
            //    else
            //        textBoxSubjectSpecificWords.Focus();
            //    return;
            //}

            if ((radioButtonFileExactMatch.Checked && string.IsNullOrEmpty(textBoxFileExactMatch.Text.Trim())) || (radioButtonFileSpecificWord.Checked && string.IsNullOrEmpty(textBoxFileSpecificWord.Text.Trim())))
            {
                MessageBox.Show("Please enter the text for the selected criteria of File Attachment.", ProductCaption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (radioButtonFileExactMatch.Checked)
                    textBoxFileExactMatch.Focus();
                else
                    textBoxFileSpecificWord.Focus();
                return;
            }
            panelMsgRuleOne.Visible = false;
            //panelMsgRuleTwo.Visible = true;
            comboBoxTransactionTypes.Focus();
        }

        private void comboBoxTradingPartner_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

       
    }
}