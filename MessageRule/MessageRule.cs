using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks.MessageRule
{
    public partial class MessageRule : Form
    {
        #region Private Members
        private static MessageRule m_MessageRule;
        private string ProductCaption = EDI.Constant.Constants.ProductName;
        #endregion

        #region Public Methods
        public static MessageRule GetInstance()
        {
            if (m_MessageRule == null)
                m_MessageRule = new MessageRule();
            return m_MessageRule;
        }
        #endregion

        #region Constructor
        public MessageRule()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handler
        private void buttonNew_Click(object sender, EventArgs e)
        {
            bool is_New = true;
            RuleDetails ruledetails = new RuleDetails(is_New);            
            DataProcessingBlocks.MessageRule.RuleDetails.GetInstance().ShowDialog();         
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
            m_MessageRule = null;
        }

        /// <summary>
        /// Attaching the rule details from the database to the datagridview.
        /// </summary>
        public void ShowRulesDataGridView()
        {
            DataTable dtMessageRule = MailSettings.MessageRules.GetMessageRulesDetails().Tables[0];
            dataGridViewMessageRules.DataSource = dtMessageRule;

            if (dataGridViewMessageRules.RowCount == 0)
            {
                buttonEdit.Enabled = false;
                buttonDelete.Enabled = false;
            }
            else
            {
                buttonEdit.Enabled = true;
                buttonDelete.Enabled = true;
            }
        }
        #endregion

        private void MessageRule_Load(object sender, EventArgs e)
        {
            dataGridViewMessageRules.DataSource = null;
            this.dgColumnsBinding();
            ShowRulesDataGridView();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to delete the record?", ProductCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                long ID = Convert.ToInt32(dataGridViewMessageRules.SelectedRows[0].Cells[0].Value);
                MailSettings.MessageRules.DeleteMessageRuleByID(ID);
                dataGridViewMessageRules.DataSource = null;
                this.dgColumnsBinding();
                ShowRulesDataGridView();
                //dataGridViewMessageRules.Rows[0].DefaultCellStyle.Font = new Font(Font.FontFamily, Font.Size, FontStyle.Bold);
            }
        }

        /// <summary>
        /// Binding columns to the datagridview.
        /// </summary>
        private void dgColumnsBinding()
        {
            dataGridViewMessageRules.Columns.Add("RulesId", "ID");
            dataGridViewMessageRules.Columns.Add("Name", "Name");

            dataGridViewMessageRules.Columns.Add("TradingPartnerID", "Trading Partner");
            dataGridViewMessageRules.Columns.Add("CondSubEquals", "CondSubEquals ");
            
            dataGridViewMessageRules.Columns.Add("CondSubMatches", "CondSubMatches ");
            dataGridViewMessageRules.Columns.Add("CondFileEquals", "CondFileEquals");
            dataGridViewMessageRules.Columns.Add("CondFileMatches", "CondFileMatches");
            
            dataGridViewMessageRules.Columns.Add("ActTransType", "Transaction Type");
            dataGridViewMessageRules.Columns.Add("ActUsingMappingId", "Mappings");
            dataGridViewMessageRules.Columns.Add("OnCompleteStatusID", "OnCompleteStatusID ");
            dataGridViewMessageRules.Columns.Add("OnCompleteErrStatusID", "OnCompleteErrStatusID ");
            dataGridViewMessageRules.Columns.Add("Status", "Status");
            dataGridViewMessageRules.Columns.Add("SoundChime", "SoundChime  ");
            dataGridViewMessageRules.Columns.Add("SoundAlarm", "SoundAlarm ");

            dataGridViewMessageRules.Columns["RulesId"].DataPropertyName = MessageRulesColumn.RulesId.ToString();
            dataGridViewMessageRules.Columns["Name"].DataPropertyName = MessageRulesColumn.Name.ToString();
            dataGridViewMessageRules.Columns["Status"].DataPropertyName = MessageRulesColumn.Status.ToString();
            dataGridViewMessageRules.Columns["TradingPartnerID"].DataPropertyName = MessageRulesColumn.TradingPartnerID.ToString();
            dataGridViewMessageRules.Columns["CondSubEquals"].DataPropertyName = MessageRulesColumn.CondSubEquals.ToString();
            dataGridViewMessageRules.Columns["CondSubMatches"].DataPropertyName = MessageRulesColumn.CondSubMatches.ToString();
            dataGridViewMessageRules.Columns["CondFileEquals"].DataPropertyName = MessageRulesColumn.CondFileEquals.ToString();
            dataGridViewMessageRules.Columns["CondFileMatches"].DataPropertyName = MessageRulesColumn.CondFileMatches.ToString();
            dataGridViewMessageRules.Columns["OnCompleteStatusID"].DataPropertyName = MessageRulesColumn.OnCompleteStatusID.ToString();
            dataGridViewMessageRules.Columns["OnCompleteErrStatusID"].DataPropertyName = MessageRulesColumn.OnCompleteErrStatusId.ToString();
            dataGridViewMessageRules.Columns["SoundChime"].DataPropertyName = MessageRulesColumn.SoundChime.ToString();
            dataGridViewMessageRules.Columns["SoundAlarm"].DataPropertyName = MessageRulesColumn.SoundAlarm.ToString();
            dataGridViewMessageRules.Columns["ActTransType"].DataPropertyName = MessageRulesColumn.ActTransType.ToString();
            dataGridViewMessageRules.Columns["ActUsingMappingId"].DataPropertyName = MessageRulesColumn.ActUsingMappingId.ToString();

            this.dataGridViewMessageRules.Columns["RulesId"].Visible = false;
            this.dataGridViewMessageRules.Columns["TradingPartnerID"].Visible = false;
            this.dataGridViewMessageRules.Columns["CondSubEquals"].Visible = false;
            this.dataGridViewMessageRules.Columns["CondSubMatches"].Visible = false;
            this.dataGridViewMessageRules.Columns["OnCompleteStatusID"].Visible = false;
            this.dataGridViewMessageRules.Columns["OnCompleteErrStatusID"].Visible = false;
            this.dataGridViewMessageRules.Columns["SoundChime"].Visible = false;
            this.dataGridViewMessageRules.Columns["SoundAlarm"].Visible = false;
            this.dataGridViewMessageRules.Columns["CondFileEquals"].Visible = false;
            this.dataGridViewMessageRules.Columns["CondFileMatches"].Visible = false;

            this.dataGridViewMessageRules.ColumnHeadersDefaultCellStyle.Font = new Font("Verdana", Font.Size, FontStyle.Bold);
            this.dataGridViewMessageRules.Font = new Font("Verdana", Font.Size, FontStyle.Regular);
        }

        private void dataGridViewMessageRules_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            buttonEdit_Click(sender, e);
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            bool is_Edit = false;
            RuleDetails ruledetails = new RuleDetails(is_Edit);
            try
            {
                RuleDetails comboboxData = new RuleDetails(Convert.ToInt64(dataGridViewMessageRules.SelectedRows[0].Cells[0].Value),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[1].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[2].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[3].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[4].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[5].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[6].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[7].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[8].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[9].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[10].Value.ToString(),
                                                           dataGridViewMessageRules.SelectedRows[0].Cells[11].Value.ToString(),
                                                           Convert.ToBoolean(dataGridViewMessageRules.SelectedRows[0].Cells[12].Value),
                                                           Convert.ToBoolean(dataGridViewMessageRules.SelectedRows[0].Cells[13].Value));

                DataProcessingBlocks.MessageRule.RuleDetails.GetInstance().ShowDialog();
            }
            catch (ArgumentOutOfRangeException)
            { }
        }
    }
}