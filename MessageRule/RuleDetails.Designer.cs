namespace DataProcessingBlocks.MessageRule
{
    partial class RuleDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RuleDetails));
            this.panelMsgRuleOne = new System.Windows.Forms.Panel();
            this.groupBoxCompletion = new System.Windows.Forms.GroupBox();
            this.buttonResetCompletion = new System.Windows.Forms.Button();
            this.comboBoxErrorStatus = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxSuccessStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancelTwo = new System.Windows.Forms.Button();
            this.groupBoxAction = new System.Windows.Forms.GroupBox();
            this.comboBoxUseMapping = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxTransactionTypes = new System.Windows.Forms.ComboBox();
            this.labelTransactionType = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.groupBoxFileAttached = new System.Windows.Forms.GroupBox();
            this.buttonResetAttachment = new System.Windows.Forms.Button();
            this.textBoxFileSpecificWord = new System.Windows.Forms.TextBox();
            this.radioButtonFileSpecificWord = new System.Windows.Forms.RadioButton();
            this.textBoxFileExactMatch = new System.Windows.Forms.TextBox();
            this.radioButtonFileExactMatch = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxMessageFrom = new System.Windows.Forms.GroupBox();
            this.comboBoxTradingPartner = new System.Windows.Forms.ComboBox();
            this.labelTP = new System.Windows.Forms.Label();
            this.checkBoxActive = new System.Windows.Forms.CheckBox();
            this.textBoxRuleName = new System.Windows.Forms.TextBox();
            this.groupBoxMessageSubject = new System.Windows.Forms.GroupBox();
            this.buttonResetSubject = new System.Windows.Forms.Button();
            this.textBoxSubjectSpecificWords = new System.Windows.Forms.TextBox();
            this.radioButtonSubjectSpecificWords = new System.Windows.Forms.RadioButton();
            this.textBoxSubjectExactMatch = new System.Windows.Forms.TextBox();
            this.radioButtonSubjectExactMatch = new System.Windows.Forms.RadioButton();
            this.panelMsgRuleOne.SuspendLayout();
            this.groupBoxCompletion.SuspendLayout();
            this.groupBoxAction.SuspendLayout();
            this.groupBoxFileAttached.SuspendLayout();
            this.groupBoxMessageFrom.SuspendLayout();
            this.groupBoxMessageSubject.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMsgRuleOne
            // 
            this.panelMsgRuleOne.Controls.Add(this.groupBoxCompletion);
            this.panelMsgRuleOne.Controls.Add(this.buttonSave);
            this.panelMsgRuleOne.Controls.Add(this.buttonCancelTwo);
            this.panelMsgRuleOne.Controls.Add(this.groupBoxAction);
            this.panelMsgRuleOne.Controls.Add(this.buttonReset);
            this.panelMsgRuleOne.Controls.Add(this.groupBoxFileAttached);
            this.panelMsgRuleOne.Controls.Add(this.label4);
            this.panelMsgRuleOne.Controls.Add(this.groupBoxMessageFrom);
            this.panelMsgRuleOne.Controls.Add(this.checkBoxActive);
            this.panelMsgRuleOne.Controls.Add(this.textBoxRuleName);
            this.panelMsgRuleOne.Controls.Add(this.groupBoxMessageSubject);
            this.panelMsgRuleOne.Location = new System.Drawing.Point(0, 3);
            this.panelMsgRuleOne.Name = "panelMsgRuleOne";
            this.panelMsgRuleOne.Size = new System.Drawing.Size(637, 647);
            this.panelMsgRuleOne.TabIndex = 7;
            // 
            // groupBoxCompletion
            // 
            this.groupBoxCompletion.Controls.Add(this.buttonResetCompletion);
            this.groupBoxCompletion.Controls.Add(this.comboBoxErrorStatus);
            this.groupBoxCompletion.Controls.Add(this.label2);
            this.groupBoxCompletion.Controls.Add(this.comboBoxSuccessStatus);
            this.groupBoxCompletion.Controls.Add(this.label3);
            this.groupBoxCompletion.Location = new System.Drawing.Point(15, 492);
            this.groupBoxCompletion.Name = "groupBoxCompletion";
            this.groupBoxCompletion.Size = new System.Drawing.Size(591, 115);
            this.groupBoxCompletion.TabIndex = 11;
            this.groupBoxCompletion.TabStop = false;
            this.groupBoxCompletion.Text = "On Completion";
            // 
            // buttonResetCompletion
            // 
            this.buttonResetCompletion.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonResetCompletion.Location = new System.Drawing.Point(495, 65);
            this.buttonResetCompletion.Name = "buttonResetCompletion";
            this.buttonResetCompletion.Size = new System.Drawing.Size(75, 23);
            this.buttonResetCompletion.TabIndex = 4;
            this.buttonResetCompletion.Text = "&Reset";
            this.buttonResetCompletion.UseVisualStyleBackColor = true;
            this.buttonResetCompletion.Click += new System.EventHandler(this.buttonResetCompletion_Click);
            // 
            // comboBoxErrorStatus
            // 
            this.comboBoxErrorStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxErrorStatus.FormattingEnabled = true;
            this.comboBoxErrorStatus.Location = new System.Drawing.Point(207, 67);
            this.comboBoxErrorStatus.Name = "comboBoxErrorStatus";
            this.comboBoxErrorStatus.Size = new System.Drawing.Size(258, 21);
            this.comboBoxErrorStatus.TabIndex = 1;
            this.comboBoxErrorStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxErrorStatus_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Change Status On Error";
            // 
            // comboBoxSuccessStatus
            // 
            this.comboBoxSuccessStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSuccessStatus.FormattingEnabled = true;
            this.comboBoxSuccessStatus.Location = new System.Drawing.Point(207, 19);
            this.comboBoxSuccessStatus.Name = "comboBoxSuccessStatus";
            this.comboBoxSuccessStatus.Size = new System.Drawing.Size(258, 21);
            this.comboBoxSuccessStatus.TabIndex = 0;
            this.comboBoxSuccessStatus.SelectedIndexChanged += new System.EventHandler(this.comboBoxSuccessStatus_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Change Status On Success";
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(221, 615);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 1;
            this.buttonSave.Text = "&Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancelTwo
            // 
            this.buttonCancelTwo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancelTwo.Location = new System.Drawing.Point(334, 615);
            this.buttonCancelTwo.Name = "buttonCancelTwo";
            this.buttonCancelTwo.Size = new System.Drawing.Size(75, 23);
            this.buttonCancelTwo.TabIndex = 2;
            this.buttonCancelTwo.Text = "&Cancel";
            this.buttonCancelTwo.UseVisualStyleBackColor = true;
            this.buttonCancelTwo.Click += new System.EventHandler(this.buttonCancelTwo_Click);
            // 
            // groupBoxAction
            // 
            this.groupBoxAction.Controls.Add(this.comboBoxUseMapping);
            this.groupBoxAction.Controls.Add(this.label1);
            this.groupBoxAction.Controls.Add(this.comboBoxTransactionTypes);
            this.groupBoxAction.Controls.Add(this.labelTransactionType);
            this.groupBoxAction.Location = new System.Drawing.Point(15, 376);
            this.groupBoxAction.Name = "groupBoxAction";
            this.groupBoxAction.Size = new System.Drawing.Size(591, 95);
            this.groupBoxAction.TabIndex = 10;
            this.groupBoxAction.TabStop = false;
            this.groupBoxAction.Text = "Action";
            // 
            // comboBoxUseMapping
            // 
            this.comboBoxUseMapping.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUseMapping.FormattingEnabled = true;
            this.comboBoxUseMapping.Location = new System.Drawing.Point(206, 62);
            this.comboBoxUseMapping.Name = "comboBoxUseMapping";
            this.comboBoxUseMapping.Size = new System.Drawing.Size(258, 21);
            this.comboBoxUseMapping.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Use Mapping";
            // 
            // comboBoxTransactionTypes
            // 
            this.comboBoxTransactionTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTransactionTypes.FormattingEnabled = true;
            this.comboBoxTransactionTypes.Items.AddRange(new object[] {
            "Journal",
            "Estimate",
            "SalesOrder",
            "Invoice",
            "SalesReceipt",
            "CreditMemo",
            "BillPaymentCreditCard",
            "BillPaymentCheck",
            "Deposit",
            "Bill",
            "CreditCardCharge",
            "CreditCardCredit",
            "Check",
            "VendorCredit",
            "TimeTracking",
            "ReceivePayment",
            "PriceLevel",
            "PurchaseOrder",
            "BillPayment",
            "InventoryAdjustment",
            "JournalEntry",
            "TimeActivity",
            "CashPurchase",
            "CheckPurchase",
            "CreditCardPurchase",
            "Payment",
            "Employee",
            "Items",
            "Customer"});
            this.comboBoxTransactionTypes.Location = new System.Drawing.Point(207, 19);
            this.comboBoxTransactionTypes.Name = "comboBoxTransactionTypes";
            this.comboBoxTransactionTypes.Size = new System.Drawing.Size(258, 21);
            this.comboBoxTransactionTypes.TabIndex = 0;
            // 
            // labelTransactionType
            // 
            this.labelTransactionType.AutoSize = true;
            this.labelTransactionType.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTransactionType.Location = new System.Drawing.Point(32, 22);
            this.labelTransactionType.Name = "labelTransactionType";
            this.labelTransactionType.Size = new System.Drawing.Size(103, 13);
            this.labelTransactionType.TabIndex = 0;
            this.labelTransactionType.Text = "Transaction Type";
            // 
            // buttonReset
            // 
            this.buttonReset.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReset.Location = new System.Drawing.Point(116, 615);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 17;
            this.buttonReset.Text = "&Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // groupBoxFileAttached
            // 
            this.groupBoxFileAttached.Controls.Add(this.buttonResetAttachment);
            this.groupBoxFileAttached.Controls.Add(this.textBoxFileSpecificWord);
            this.groupBoxFileAttached.Controls.Add(this.radioButtonFileSpecificWord);
            this.groupBoxFileAttached.Controls.Add(this.textBoxFileExactMatch);
            this.groupBoxFileAttached.Controls.Add(this.radioButtonFileExactMatch);
            this.groupBoxFileAttached.Location = new System.Drawing.Point(15, 243);
            this.groupBoxFileAttached.Name = "groupBoxFileAttached";
            this.groupBoxFileAttached.Size = new System.Drawing.Size(591, 95);
            this.groupBoxFileAttached.TabIndex = 4;
            this.groupBoxFileAttached.TabStop = false;
            this.groupBoxFileAttached.Text = "File Attached";
            // 
            // buttonResetAttachment
            // 
            this.buttonResetAttachment.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonResetAttachment.Location = new System.Drawing.Point(492, 55);
            this.buttonResetAttachment.Name = "buttonResetAttachment";
            this.buttonResetAttachment.Size = new System.Drawing.Size(78, 25);
            this.buttonResetAttachment.TabIndex = 19;
            this.buttonResetAttachment.Text = "&Reset";
            this.buttonResetAttachment.UseVisualStyleBackColor = true;
            this.buttonResetAttachment.Click += new System.EventHandler(this.buttonResetAttachment_Click);
            // 
            // textBoxFileSpecificWord
            // 
            this.textBoxFileSpecificWord.Location = new System.Drawing.Point(204, 64);
            this.textBoxFileSpecificWord.MaxLength = 50;
            this.textBoxFileSpecificWord.Name = "textBoxFileSpecificWord";
            this.textBoxFileSpecificWord.Size = new System.Drawing.Size(258, 20);
            this.textBoxFileSpecificWord.TabIndex = 3;
            // 
            // radioButtonFileSpecificWord
            // 
            this.radioButtonFileSpecificWord.AutoSize = true;
            this.radioButtonFileSpecificWord.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFileSpecificWord.Location = new System.Drawing.Point(31, 63);
            this.radioButtonFileSpecificWord.Name = "radioButtonFileSpecificWord";
            this.radioButtonFileSpecificWord.Size = new System.Drawing.Size(131, 17);
            this.radioButtonFileSpecificWord.TabIndex = 2;
            this.radioButtonFileSpecificWord.TabStop = true;
            this.radioButtonFileSpecificWord.Text = "With Specific Word";
            this.radioButtonFileSpecificWord.UseVisualStyleBackColor = true;
            this.radioButtonFileSpecificWord.CheckedChanged += new System.EventHandler(this.radioButtonFileSpecificWord_CheckedChanged);
            // 
            // textBoxFileExactMatch
            // 
            this.textBoxFileExactMatch.Location = new System.Drawing.Point(207, 16);
            this.textBoxFileExactMatch.MaxLength = 50;
            this.textBoxFileExactMatch.Name = "textBoxFileExactMatch";
            this.textBoxFileExactMatch.Size = new System.Drawing.Size(258, 20);
            this.textBoxFileExactMatch.TabIndex = 1;
            // 
            // radioButtonFileExactMatch
            // 
            this.radioButtonFileExactMatch.AutoSize = true;
            this.radioButtonFileExactMatch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFileExactMatch.Location = new System.Drawing.Point(35, 19);
            this.radioButtonFileExactMatch.Name = "radioButtonFileExactMatch";
            this.radioButtonFileExactMatch.Size = new System.Drawing.Size(93, 17);
            this.radioButtonFileExactMatch.TabIndex = 0;
            this.radioButtonFileExactMatch.TabStop = true;
            this.radioButtonFileExactMatch.Text = "Exact Match";
            this.radioButtonFileExactMatch.UseVisualStyleBackColor = true;
            this.radioButtonFileExactMatch.CheckedChanged += new System.EventHandler(this.radioButtonFileExactMatch_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Name";
            // 
            // groupBoxMessageFrom
            // 
            this.groupBoxMessageFrom.Controls.Add(this.comboBoxTradingPartner);
            this.groupBoxMessageFrom.Controls.Add(this.labelTP);
            this.groupBoxMessageFrom.Location = new System.Drawing.Point(27, 40);
            this.groupBoxMessageFrom.Name = "groupBoxMessageFrom";
            this.groupBoxMessageFrom.Size = new System.Drawing.Size(579, 46);
            this.groupBoxMessageFrom.TabIndex = 2;
            this.groupBoxMessageFrom.TabStop = false;
            this.groupBoxMessageFrom.Text = "Message From";
            // 
            // comboBoxTradingPartner
            // 
            this.comboBoxTradingPartner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTradingPartner.FormattingEnabled = true;
            this.comboBoxTradingPartner.Location = new System.Drawing.Point(192, 16);
            this.comboBoxTradingPartner.Name = "comboBoxTradingPartner";
            this.comboBoxTradingPartner.Size = new System.Drawing.Size(258, 21);
            this.comboBoxTradingPartner.TabIndex = 0;
            this.comboBoxTradingPartner.SelectedIndexChanged += new System.EventHandler(this.comboBoxTradingPartner_SelectedIndexChanged);
            // 
            // labelTP
            // 
            this.labelTP.AutoSize = true;
            this.labelTP.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTP.Location = new System.Drawing.Point(19, 24);
            this.labelTP.Name = "labelTP";
            this.labelTP.Size = new System.Drawing.Size(95, 13);
            this.labelTP.TabIndex = 1;
            this.labelTP.Text = "Trading Partner";
            // 
            // checkBoxActive
            // 
            this.checkBoxActive.AutoSize = true;
            this.checkBoxActive.Checked = true;
            this.checkBoxActive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxActive.Location = new System.Drawing.Point(526, 17);
            this.checkBoxActive.Name = "checkBoxActive";
            this.checkBoxActive.Size = new System.Drawing.Size(56, 17);
            this.checkBoxActive.TabIndex = 1;
            this.checkBoxActive.Text = "Active";
            this.checkBoxActive.UseVisualStyleBackColor = true;
            // 
            // textBoxRuleName
            // 
            this.textBoxRuleName.Location = new System.Drawing.Point(222, 11);
            this.textBoxRuleName.MaxLength = 50;
            this.textBoxRuleName.Name = "textBoxRuleName";
            this.textBoxRuleName.Size = new System.Drawing.Size(258, 20);
            this.textBoxRuleName.TabIndex = 0;
            // 
            // groupBoxMessageSubject
            // 
            this.groupBoxMessageSubject.Controls.Add(this.buttonResetSubject);
            this.groupBoxMessageSubject.Controls.Add(this.textBoxSubjectSpecificWords);
            this.groupBoxMessageSubject.Controls.Add(this.radioButtonSubjectSpecificWords);
            this.groupBoxMessageSubject.Controls.Add(this.textBoxSubjectExactMatch);
            this.groupBoxMessageSubject.Controls.Add(this.radioButtonSubjectExactMatch);
            this.groupBoxMessageSubject.Location = new System.Drawing.Point(15, 104);
            this.groupBoxMessageSubject.Name = "groupBoxMessageSubject";
            this.groupBoxMessageSubject.Size = new System.Drawing.Size(591, 110);
            this.groupBoxMessageSubject.TabIndex = 3;
            this.groupBoxMessageSubject.TabStop = false;
            this.groupBoxMessageSubject.Text = "Message Subject";
            // 
            // buttonResetSubject
            // 
            this.buttonResetSubject.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonResetSubject.Location = new System.Drawing.Point(492, 64);
            this.buttonResetSubject.Name = "buttonResetSubject";
            this.buttonResetSubject.Size = new System.Drawing.Size(72, 25);
            this.buttonResetSubject.TabIndex = 18;
            this.buttonResetSubject.Text = "&Reset";
            this.buttonResetSubject.UseVisualStyleBackColor = true;
            this.buttonResetSubject.Click += new System.EventHandler(this.buttonResetSubject_Click);
            // 
            // textBoxSubjectSpecificWords
            // 
            this.textBoxSubjectSpecificWords.Location = new System.Drawing.Point(204, 68);
            this.textBoxSubjectSpecificWords.MaxLength = 50;
            this.textBoxSubjectSpecificWords.Name = "textBoxSubjectSpecificWords";
            this.textBoxSubjectSpecificWords.Size = new System.Drawing.Size(258, 20);
            this.textBoxSubjectSpecificWords.TabIndex = 3;
            // 
            // radioButtonSubjectSpecificWords
            // 
            this.radioButtonSubjectSpecificWords.AutoSize = true;
            this.radioButtonSubjectSpecificWords.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonSubjectSpecificWords.Location = new System.Drawing.Point(32, 67);
            this.radioButtonSubjectSpecificWords.Name = "radioButtonSubjectSpecificWords";
            this.radioButtonSubjectSpecificWords.Size = new System.Drawing.Size(131, 17);
            this.radioButtonSubjectSpecificWords.TabIndex = 2;
            this.radioButtonSubjectSpecificWords.TabStop = true;
            this.radioButtonSubjectSpecificWords.Text = "With Specific Word";
            this.radioButtonSubjectSpecificWords.UseVisualStyleBackColor = true;
            this.radioButtonSubjectSpecificWords.CheckedChanged += new System.EventHandler(this.radioButtonSubjectSpecificWords_CheckedChanged);
            // 
            // textBoxSubjectExactMatch
            // 
            this.textBoxSubjectExactMatch.Location = new System.Drawing.Point(204, 19);
            this.textBoxSubjectExactMatch.MaxLength = 50;
            this.textBoxSubjectExactMatch.Name = "textBoxSubjectExactMatch";
            this.textBoxSubjectExactMatch.Size = new System.Drawing.Size(258, 20);
            this.textBoxSubjectExactMatch.TabIndex = 1;
            // 
            // radioButtonSubjectExactMatch
            // 
            this.radioButtonSubjectExactMatch.AutoSize = true;
            this.radioButtonSubjectExactMatch.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonSubjectExactMatch.Location = new System.Drawing.Point(35, 19);
            this.radioButtonSubjectExactMatch.Name = "radioButtonSubjectExactMatch";
            this.radioButtonSubjectExactMatch.Size = new System.Drawing.Size(93, 17);
            this.radioButtonSubjectExactMatch.TabIndex = 0;
            this.radioButtonSubjectExactMatch.TabStop = true;
            this.radioButtonSubjectExactMatch.Text = "Exact Match";
            this.radioButtonSubjectExactMatch.UseVisualStyleBackColor = true;
            this.radioButtonSubjectExactMatch.CheckedChanged += new System.EventHandler(this.radioButtonSubjectExactMatch_CheckedChanged);
            // 
            // RuleDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(660, 656);
            this.Controls.Add(this.panelMsgRuleOne);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RuleDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rule Details ";
            this.Load += new System.EventHandler(this.RuleDetails_Load);
            this.panelMsgRuleOne.ResumeLayout(false);
            this.panelMsgRuleOne.PerformLayout();
            this.groupBoxCompletion.ResumeLayout(false);
            this.groupBoxCompletion.PerformLayout();
            this.groupBoxAction.ResumeLayout(false);
            this.groupBoxAction.PerformLayout();
            this.groupBoxFileAttached.ResumeLayout(false);
            this.groupBoxFileAttached.PerformLayout();
            this.groupBoxMessageFrom.ResumeLayout(false);
            this.groupBoxMessageFrom.PerformLayout();
            this.groupBoxMessageSubject.ResumeLayout(false);
            this.groupBoxMessageSubject.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMsgRuleOne;
        private System.Windows.Forms.GroupBox groupBoxFileAttached;
        private System.Windows.Forms.TextBox textBoxFileSpecificWord;
        private System.Windows.Forms.RadioButton radioButtonFileSpecificWord;
        private System.Windows.Forms.TextBox textBoxFileExactMatch;
        private System.Windows.Forms.RadioButton radioButtonFileExactMatch;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxMessageFrom;
        private System.Windows.Forms.ComboBox comboBoxTradingPartner;
        private System.Windows.Forms.Label labelTP;
        private System.Windows.Forms.CheckBox checkBoxActive;
        private System.Windows.Forms.TextBox textBoxRuleName;
        private System.Windows.Forms.GroupBox groupBoxMessageSubject;
        private System.Windows.Forms.TextBox textBoxSubjectSpecificWords;
        private System.Windows.Forms.RadioButton radioButtonSubjectSpecificWords;
        private System.Windows.Forms.TextBox textBoxSubjectExactMatch;
        private System.Windows.Forms.RadioButton radioButtonSubjectExactMatch;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonResetAttachment;
        private System.Windows.Forms.Button buttonResetSubject;
        private System.Windows.Forms.GroupBox groupBoxCompletion;
        private System.Windows.Forms.Button buttonResetCompletion;
        private System.Windows.Forms.ComboBox comboBoxErrorStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxSuccessStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancelTwo;
        private System.Windows.Forms.GroupBox groupBoxAction;
        private System.Windows.Forms.ComboBox comboBoxUseMapping;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxTransactionTypes;
        private System.Windows.Forms.Label labelTransactionType;

    }
}