using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class MappingMessages : Form
    {
        private static MappingMessages m_Mess;
        
        public static MappingMessages getInstance()
        {
            if (m_Mess == null)
                m_Mess = new MappingMessages();
            return m_Mess;
        }


        public MappingMessages()
        {
            InitializeComponent();
        }

        public void SetDialogBox(string message, MessageBoxIcon icon)
        {

            //buttonIgnore.Visible = true;
            //this.buttonCancel.Location = new System.Drawing.Point(64, 155);

            this.growLabelMessags.Text = message;

            this.pictureBox1.Image = System.Drawing.SystemIcons.Warning.ToBitmap();

        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonIgnore_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Ignore;
            this.Close();
        }

        
    }
}