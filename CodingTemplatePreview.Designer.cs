namespace DataProcessingBlocks
{
    partial class CodingTemplatePreview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodingTemplatePreview));
            this.comboBoxPreviewCodingTemplate = new System.Windows.Forms.ComboBox();
            this.buttonPreviewOK = new System.Windows.Forms.Button();
            this.buttonPreviewEdit = new System.Windows.Forms.Button();
            this.panelComboCoding = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonAddNewCodingTemplate = new System.Windows.Forms.Button();
            this.lblTemplateName = new System.Windows.Forms.Label();
            this.panelEditOk = new System.Windows.Forms.Panel();
            this.buttonImportCodingTemplate = new System.Windows.Forms.Button();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.radGridCodingTemplate = new Telerik.WinControls.UI.RadGridView();
            this.dataGridViewPreviewCodingTemplate = new System.Windows.Forms.DataGridView();
            this.backgroundWorkerProcessLoderForCodingTemplate = new System.ComponentModel.BackgroundWorker();
            this.panelComboCoding.SuspendLayout();
            this.panelEditOk.SuspendLayout();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridCodingTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridCodingTemplate.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPreviewCodingTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxPreviewCodingTemplate
            // 
            this.comboBoxPreviewCodingTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPreviewCodingTemplate.FormattingEnabled = true;
            this.comboBoxPreviewCodingTemplate.Location = new System.Drawing.Point(91, 6);
            this.comboBoxPreviewCodingTemplate.Name = "comboBoxPreviewCodingTemplate";
            this.comboBoxPreviewCodingTemplate.Size = new System.Drawing.Size(189, 21);
            this.comboBoxPreviewCodingTemplate.TabIndex = 1;
            this.comboBoxPreviewCodingTemplate.SelectedIndexChanged += new System.EventHandler(this.comboBoxPreviewCodingTemplate_SelectedIndexChanged);
            // 
            // buttonPreviewOK
            // 
            this.buttonPreviewOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPreviewOK.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonPreviewOK.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPreviewOK.Location = new System.Drawing.Point(786, 8);
            this.buttonPreviewOK.Name = "buttonPreviewOK";
            this.buttonPreviewOK.Size = new System.Drawing.Size(57, 23);
            this.buttonPreviewOK.TabIndex = 2;
            this.buttonPreviewOK.Text = "Cancel";
            this.buttonPreviewOK.UseVisualStyleBackColor = false;
            this.buttonPreviewOK.Click += new System.EventHandler(this.buttonPreviewOK_Click);
            // 
            // buttonPreviewEdit
            // 
            this.buttonPreviewEdit.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonPreviewEdit.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPreviewEdit.Location = new System.Drawing.Point(286, 4);
            this.buttonPreviewEdit.Name = "buttonPreviewEdit";
            this.buttonPreviewEdit.Size = new System.Drawing.Size(77, 24);
            this.buttonPreviewEdit.TabIndex = 3;
            this.buttonPreviewEdit.Text = "Edit";
            this.buttonPreviewEdit.UseVisualStyleBackColor = false;
            this.buttonPreviewEdit.Click += new System.EventHandler(this.buttonPreviewEdit_Click);
            // 
            // panelComboCoding
            // 
            this.panelComboCoding.AutoSize = true;
            this.panelComboCoding.Controls.Add(this.button1);
            this.panelComboCoding.Controls.Add(this.buttonAddNewCodingTemplate);
            this.panelComboCoding.Controls.Add(this.lblTemplateName);
            this.panelComboCoding.Controls.Add(this.buttonPreviewEdit);
            this.panelComboCoding.Controls.Add(this.comboBoxPreviewCodingTemplate);
            this.panelComboCoding.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelComboCoding.Location = new System.Drawing.Point(0, 0);
            this.panelComboCoding.Name = "panelComboCoding";
            this.panelComboCoding.Size = new System.Drawing.Size(948, 31);
            this.panelComboCoding.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(813, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Find matches";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // buttonAddNewCodingTemplate
            // 
            this.buttonAddNewCodingTemplate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddNewCodingTemplate.Location = new System.Drawing.Point(369, 4);
            this.buttonAddNewCodingTemplate.Name = "buttonAddNewCodingTemplate";
            this.buttonAddNewCodingTemplate.Size = new System.Drawing.Size(81, 24);
            this.buttonAddNewCodingTemplate.TabIndex = 5;
            this.buttonAddNewCodingTemplate.Text = "New";
            this.buttonAddNewCodingTemplate.UseVisualStyleBackColor = true;
            this.buttonAddNewCodingTemplate.Click += new System.EventHandler(this.buttonAddNewCodingTemplate_Click);
            // 
            // lblTemplateName
            // 
            this.lblTemplateName.AutoSize = true;
            this.lblTemplateName.Location = new System.Drawing.Point(6, 10);
            this.lblTemplateName.Name = "lblTemplateName";
            this.lblTemplateName.Size = new System.Drawing.Size(80, 13);
            this.lblTemplateName.TabIndex = 4;
            this.lblTemplateName.Text = "Template name";
            // 
            // panelEditOk
            // 
            this.panelEditOk.Controls.Add(this.buttonImportCodingTemplate);
            this.panelEditOk.Controls.Add(this.buttonPreviewOK);
            this.panelEditOk.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelEditOk.Location = new System.Drawing.Point(0, 453);
            this.panelEditOk.Name = "panelEditOk";
            this.panelEditOk.Size = new System.Drawing.Size(948, 40);
            this.panelEditOk.TabIndex = 5;
            // 
            // buttonImportCodingTemplate
            // 
            this.buttonImportCodingTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonImportCodingTemplate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonImportCodingTemplate.Location = new System.Drawing.Point(863, 8);
            this.buttonImportCodingTemplate.Name = "buttonImportCodingTemplate";
            this.buttonImportCodingTemplate.Size = new System.Drawing.Size(75, 23);
            this.buttonImportCodingTemplate.TabIndex = 4;
            this.buttonImportCodingTemplate.Text = "Import";
            this.buttonImportCodingTemplate.UseVisualStyleBackColor = true;
            this.buttonImportCodingTemplate.Click += new System.EventHandler(this.buttonImportCodingTemplate_Click);
            // 
            // panelGrid
            // 
            this.panelGrid.AutoSize = true;
            this.panelGrid.Controls.Add(this.radGridCodingTemplate);
            this.panelGrid.Controls.Add(this.dataGridViewPreviewCodingTemplate);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 31);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(948, 422);
            this.panelGrid.TabIndex = 6;
            // 
            // radGridCodingTemplate
            // 
            this.radGridCodingTemplate.AutoScroll = true;
            this.radGridCodingTemplate.BackColor = System.Drawing.Color.DarkTurquoise;
            this.radGridCodingTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridCodingTemplate.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridCodingTemplate
            // 
            this.radGridCodingTemplate.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridCodingTemplate.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.radGridCodingTemplate.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridCodingTemplate.MasterTemplate.EnableFiltering = true;
            this.radGridCodingTemplate.MasterTemplate.EnableGrouping = false;
            this.radGridCodingTemplate.MasterTemplate.ShowFilteringRow = false;
            this.radGridCodingTemplate.MasterTemplate.ShowHeaderCellButtons = true;
            this.radGridCodingTemplate.Name = "radGridCodingTemplate";
            this.radGridCodingTemplate.ShowGroupPanel = false;
            this.radGridCodingTemplate.ShowHeaderCellButtons = true;
            this.radGridCodingTemplate.Size = new System.Drawing.Size(948, 422);
            this.radGridCodingTemplate.TabIndex = 1;
            this.radGridCodingTemplate.Text = "radGridCodingTemplate";
            this.radGridCodingTemplate.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.radGridCodingTemplate_CellFormatting);
            this.radGridCodingTemplate.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.radGridCodingTemplate_ViewCellFormatting);
            this.radGridCodingTemplate.CellBeginEdit += new Telerik.WinControls.UI.GridViewCellCancelEventHandler(this.radGridCodingTemplate_CellBeginEdit);
            this.radGridCodingTemplate.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridCodingTemplate_CellClick);
            // 
            // dataGridViewPreviewCodingTemplate
            // 
            this.dataGridViewPreviewCodingTemplate.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dataGridViewPreviewCodingTemplate.AllowUserToAddRows = false;
            this.dataGridViewPreviewCodingTemplate.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.dataGridViewPreviewCodingTemplate.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewPreviewCodingTemplate.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewPreviewCodingTemplate.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewPreviewCodingTemplate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPreviewCodingTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPreviewCodingTemplate.EnableHeadersVisualStyles = false;
            this.dataGridViewPreviewCodingTemplate.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewPreviewCodingTemplate.Name = "dataGridViewPreviewCodingTemplate";
            this.dataGridViewPreviewCodingTemplate.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridViewPreviewCodingTemplate.Size = new System.Drawing.Size(948, 422);
            this.dataGridViewPreviewCodingTemplate.TabIndex = 1;
            this.dataGridViewPreviewCodingTemplate.Visible = false;
            this.dataGridViewPreviewCodingTemplate.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPreviewCodingTemplate_CellClick);
            this.dataGridViewPreviewCodingTemplate.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewPreviewCodingTemplate_DataError);
            // 
            // CodingTemplatePreview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(948, 493);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelEditOk);
            this.Controls.Add(this.panelComboCoding);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CodingTemplatePreview";
            this.Text = " Statement Coding Template Preview";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CodingTemplatePreview_FormClosed);
            this.Load += new System.EventHandler(this.CodingTemplatePreview_Load);
            this.panelComboCoding.ResumeLayout(false);
            this.panelComboCoding.PerformLayout();
            this.panelEditOk.ResumeLayout(false);
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridCodingTemplate.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridCodingTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPreviewCodingTemplate)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonPreviewOK;
        private System.Windows.Forms.Button buttonPreviewEdit;
        public System.Windows.Forms.ComboBox comboBoxPreviewCodingTemplate;
        private System.Windows.Forms.Panel panelComboCoding;
        private System.Windows.Forms.Panel panelEditOk;
        private System.Windows.Forms.Panel panelGrid;
        public System.Windows.Forms.DataGridView dataGridViewPreviewCodingTemplate;
        private System.Windows.Forms.Label lblTemplateName;
        public Telerik.WinControls.UI.RadGridView radGridCodingTemplate;
       
        private System.Windows.Forms.Button buttonImportCodingTemplate;
  		private System.ComponentModel.BackgroundWorker backgroundWorkerProcessLoderForCodingTemplate;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarIndicator=new System.Windows.Forms.ToolStripProgressBar();
        private System.Windows.Forms.Button buttonAddNewCodingTemplate;
        private System.Windows.Forms.Button button1;
   
    }
}