using System;
using System.Collections.Generic;
using System.Text;
using DataProcessingBlocks;
using Microsoft.SqlServer.Server;

namespace SQLQueries
{
    /// <summary>
    /// Class contains queries used in the application. 
    /// MySql does not support Stored procedures so this class is used to avoid hard coded queries 
    /// and reusability of parameterized queries.
    /// </summary>
    internal class Queries
    {
        internal const string MailSetupPasswordKey = "#4%6";
        /// <summary>
        /// Insert new message into message_schema
        /// </summary>
        internal static string SQ001 = "INSERT INTO " + Tables.message_schema.ToString() +
                                                   " (" + MessageColumns.MessageDate.ToString() + "," +
                                                            MessageColumns.ArchiveDate.ToString() + "," +
                                                            MessageColumns.FromAddress.ToString() + "," +
                                                            MessageColumns.ToAddress.ToString() + "," +
                                                            MessageColumns.RefMessageID.ToString() + "," +
                                                            MessageColumns.Subject.ToString() + "," +
                                                            MessageColumns.Body.ToString() + "," +
                                                            MessageColumns.Status.ToString() + "," +
                                                            MessageColumns.ReadFlag.ToString() + ") VALUES ('{0}','{1}','{2}','{3}',{4},'{5}','{6}','{7}',{8})";



        /// <summary>
        /// Get all records of mail_protocol_Schema
        /// </summary>
        internal static string SQ002 = "SELECT * FROM " + Tables.mail_protocol_schema.ToString() + "";
        
        /// <summary>
        /// Insert new record into trading_partner_schema
        /// </summary>
        internal static string SQ003 = "INSERT INTO "+ Tables.trading_partner_schema.ToString()+"("
                                         + TradingPartnerColumns.TPName.ToString()+","
                                         + TradingPartnerColumns.MailProtocolID.ToString()+","
                                         + TradingPartnerColumns.EmailID.ToString()+","
                                         + TradingPartnerColumns.AlternateEmailID.ToString()+","
                                         + TradingPartnerColumns.ClientID.ToString()+","
                                         + TradingPartnerColumns.XSDLocation.ToString()+")"      
                                         + " VALUES ('{0}',{1},'{2}','{3}','{4}','{5}')";
        
        
      
        /// <summary>
        /// Get number of record(s) from trading_partner_schema where emailID is matched
       /// </summary>
        internal static string SQ004 = "SELECT COUNT(*) FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                         + TradingPartnerColumns.EmailID.ToString() + " = '{0}' AND "
                                         + TradingPartnerColumns.TradingPartnerID.ToString()+" != {1} OR " 
                                         + TradingPartnerColumns.AlternateEmailID.ToString()+" = '{0}' AND "
                                         + TradingPartnerColumns.TradingPartnerID.ToString()+" != {1}";


        /// <summary>
        /// Get number of record(s) from trading_partner_schema where either emailID or alternate emailID is matched
        /// </summary>
        internal static string SQ005 = "SELECT COUNT(*) FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                        + TradingPartnerColumns.EmailID.ToString() + " = '{0}' AND "
                                        + TradingPartnerColumns.TradingPartnerID.ToString() + " != {2} OR "
                                        + TradingPartnerColumns.AlternateEmailID.ToString() + " = '{0}' AND "
                                        + TradingPartnerColumns.TradingPartnerID.ToString() + " != {2} OR "
                                        + TradingPartnerColumns.EmailID.ToString() + " = '{1}' AND "
                                        + TradingPartnerColumns.TradingPartnerID.ToString() + " != {2} OR "
                                        + TradingPartnerColumns.AlternateEmailID.ToString() + " = '{1}' AND "
                                        + TradingPartnerColumns.TradingPartnerID.ToString() + " != {2}";
        
        /// <summary>
        /// Insert new record with password into mail setup schema 
        /// </summary>
        internal static string SQ006 = "INSERT INTO " + Tables.mail_setup_schema.ToString() + "("
                                         + MailSetupColumns.MailProtocolID.ToString() + ","
                                         + MailSetupColumns.InPortNo.ToString() + ","
                                         + MailSetupColumns.OutPortNo.ToString() + ","
                                         + MailSetupColumns.IsSSL.ToString() + ","
                                         + MailSetupColumns.IncommingServer.ToString() + ","
                                         + MailSetupColumns.OutgoingServer.ToString() + ","
                                         + MailSetupColumns.UserName.ToString() + ","
                                         + MailSetupColumns.EmailID.ToString() + ","
                                         + MailSetupColumns.Password.ToString() + ","
                                         + MailSetupColumns.AutomaticFlag.ToString() + ","
                                         + MailSetupColumns.OutboundFlag.ToString() + ","
                                         + MailSetupColumns.InboundFlag.ToString() + ","
                                         + MailSetupColumns.ScheduleInMin.ToString() + ")"
                                         + " VALUES ({0},{1},{2},{3},'{4}','{5}','{6}','{7}',AES_ENCRYPT('{8}','" + MailSetupPasswordKey + "'),{9},{10},{11},{12})";
        /// <summary>
        /// Insert new record without password into mail setup schema 
        /// </summary>
        internal static string SQ007 = "INSERT INTO " + Tables.mail_setup_schema.ToString() + "("
                                         + MailSetupColumns.MailProtocolID.ToString() + ","
                                         + MailSetupColumns.InPortNo.ToString() + ","
                                         + MailSetupColumns.OutPortNo.ToString() + ","
                                         + MailSetupColumns.IsSSL.ToString() + ","
                                         + MailSetupColumns.IncommingServer.ToString() + ","
                                         + MailSetupColumns.OutgoingServer.ToString() + ","
                                         + MailSetupColumns.UserName.ToString() + ","
                                         + MailSetupColumns.EmailID.ToString() + ","
                                         + MailSetupColumns.AutomaticFlag.ToString() + ","
                                         + MailSetupColumns.OutboundFlag.ToString() + ","
                                         + MailSetupColumns.InboundFlag.ToString() + ","
                                         + MailSetupColumns.ScheduleInMin.ToString() + ")"
                                         //+ MailSetupColumns.Password.ToString() + ","
                                         + " VALUES ({0},{1},{2},{3},'{4}','{5}','{6}','{7}',{8},{9},{10},{11})";
        /// <summary>
        /// Delete all records from mailsetup schema
        /// </summary>
        internal static string SQ008 = "DELETE FROM " + Tables.mail_setup_schema.ToString() + "";

        /// <summary>
        /// Get date from export_filter by export filter data type.
        /// </summary>
        internal static string SQ010 = "SELECT "+ ExportFilterColumns.FilterDate.ToString()+" FROM "
                                                + Tables.export_filter.ToString() +" WHERE "
                                                + ExportFilterColumns.FilterDataType.ToString()+" = '{0}'";

       /// <summary>
       /// Insert new records into export filter 
       /// </summary>
        internal static string SQ011 = "INSERT INTO " + Tables.export_filter.ToString() + "("
                                                     + ExportFilterColumns.FilterDataType.ToString() + ","
                                                     + ExportFilterColumns.FilterDate.ToString() + ")"
                                                     + " VALUES('{0}','{1}')";
        /// <summary>
        /// Update the export_filter schema by FilterDataType
        /// </summary>
        internal static string SQ012 = "UPDATE " + Tables.export_filter.ToString() + " SET "
                                                 + ExportFilterColumns.FilterDate.ToString()+" = '{0}' WHERE "
                                                 + ExportFilterColumns.FilterDataType.ToString() + "= '{1}'";

        /// <summary>
        /// Get emailID from mail_setup_schema.
        /// </summary>
        internal static string SQ013 = "SELECT " + MailSetupColumns.EmailID.ToString() + " FROM "
                                                 + Tables.mail_setup_schema.ToString() + "";

        /// <summary>
        /// Get email messages from message_schema for the particular message_status
        /// </summary>
        internal static string SQ014 = "SELECT "  + MessageColumns.ReadFlag.ToString() + ", "
                                                  + MessageColumns.FromAddress.ToString() + ", "
                                                  + MessageColumns.ToAddress.ToString() + ", "  
                                                  + MessageColumns.Subject.ToString() + ", "
                                                  + MessageStatusColumns.Name.ToString() + ", "
                                                  + MessageColumns.MessageID.ToString() + ", "            
                                                  + "Date_Format(" + MessageColumns.MessageDate.ToString()+ ",'%m/%d/%Y %H:%i %p')" +"AS " +MessageColumns.MessageDate.ToString()+ " FROM "   
                                                  + Tables.message_schema.ToString() + " , "
                                                  + Tables.message_status_schema.ToString()  + " WHERE ("
                                                  + MessageColumns.Status.ToString() + " = '{0}' OR "
                                                  + MessageColumns.Status.ToString() + " = '{1}' OR "
                                                  + MessageColumns.Status.ToString() + " = '{2}' OR "
                                                  + MessageColumns.Status.ToString() + " = '{3}') AND "
                                                  + MessageStatusColumns.StatusId.ToString() + " = "
                                                  + MessageColumns.Status.ToString()     
                                                  + " ORDER BY " + MessageColumns.MessageID.ToString() + " DESC";
       
        
        /// <summary>
        /// Get the mail configuration details
        /// </summary>
        internal static string SQ015 = "SELECT " + MailSetupColumns.MailSetupID.ToString() + ","
                                         + MailSetupColumns.MailProtocolID.ToString() + ","
                                         + MailSetupColumns.InPortNo.ToString() + ","
                                         + MailSetupColumns.OutPortNo.ToString() + ","
                                         + MailSetupColumns.IsSSL.ToString() + ","
                                         + MailSetupColumns.IncommingServer.ToString() + ","
                                         + MailSetupColumns.OutgoingServer.ToString() + ","
                                         + MailSetupColumns.UserName.ToString() + ","
                                         + MailSetupColumns.EmailID.ToString() + ",CAST(AES_DECRYPT("
                                         + MailSetupColumns.Password.ToString() + ",'" + MailSetupPasswordKey + "')AS CHAR)" + ","
                                         + MailSetupColumns.AutomaticFlag.ToString() + ","
                                         + MailSetupColumns.OutboundFlag.ToString() + ","
                                         + MailSetupColumns.InboundFlag.ToString() + ","
                                         + MailSetupColumns.ScheduleInMin.ToString() + " FROM "
                                         + Tables.mail_setup_schema.ToString();
        
        /// <summary>
        ///  Get number of row of export_filter by export filter data type.
        /// </summary>
        internal static string SQ016 = "SELECT COUNT(*) FROM " + Tables.export_filter.ToString() + " WHERE "
                                        + ExportFilterColumns.FilterDataType.ToString() + " = '{0}'";


        /// <summary>
        /// Insert new message into message_schema
        /// </summary>
        internal static string SQ021 = "INSERT INTO " + Tables.message_schema.ToString() +
                                                   " (" + MessageColumns.MessageDate.ToString() + "," +
                                                            MessageColumns.ArchiveDate.ToString() + "," +
                                                            MessageColumns.FromAddress.ToString() + "," +
                                                            MessageColumns.ToAddress.ToString() + "," +
                                                            MessageColumns.RefMessageID.ToString() + "," +
                                                            MessageColumns.Subject.ToString() + "," +
                                                            MessageColumns.Body.ToString() + "," +
                                                            MessageColumns.Status.ToString() + "," +
                                                            MessageColumns.ReadFlag.ToString() +
                                                            ") VALUES ('{0}','{1}','{2}','{3}',{4},'{5}','{6}','{7}',{8})";

        /// <summary>
        /// Select all record into trading_partner_schema
        /// </summary>
        internal static string SQ022 = "SELECT "+ TradingPartnerColumns.TradingPartnerID.ToString() + ","
                                                 + TradingPartnerColumns.TPName.ToString() + ","
                                                 + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                                 + TradingPartnerColumns.EmailID.ToString() + ","
                                                 + TradingPartnerColumns.AlternateEmailID.ToString() + ","
                                                 + TradingPartnerColumns.ClientID.ToString() + ","
                                                 + TradingPartnerColumns.XSDLocation.ToString() + " FROM " 
                                                 + Tables.trading_partner_schema.ToString();

        internal static string SQ022POP3 = "SELECT " + TradingPartnerColumns.TradingPartnerID.ToString() + ","
                                                 + TradingPartnerColumns.TPName.ToString() + ","
                                                 + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                                 + TradingPartnerColumns.EmailID.ToString() + ","
                                                 + TradingPartnerColumns.AlternateEmailID.ToString() + ","
                                                 + TradingPartnerColumns.ClientID.ToString() + ","
                                                 + TradingPartnerColumns.XSDLocation.ToString() + " FROM "
                                                 + Tables.trading_partner_schema.ToString() + " "
                                                  + " WHERE " + TradingPartnerColumns.MailProtocolID.ToString() + "=1";
                                                 


        /// <summary>
        /// Get email messages from message_schema for the particular messageId
        /// </summary>
        internal static string SQ023 = "SELECT " + MessageColumns.FromAddress.ToString() + ", "
                                                  + MessageColumns.ToAddress.ToString() + ", "
                                                  + MessageColumns.Subject.ToString() + ", "
                                                  + MessageColumns.Body.ToString() + ", "
                                                  + MessageColumns.MessageDate.ToString() + ", "
                                                  + MessageColumns.RefMessageID.ToString() + ", "
                                                  + MessageColumns.Cc.ToString() + " FROM "
                                                  + Tables.message_schema.ToString() + " WHERE "
                                                  + MessageColumns.MessageID.ToString() + " = {0} AND ("
                                                  + MessageColumns.Status.ToString() + " = '{1}' OR "
                                                  + MessageColumns.Status.ToString() + " = '{2}' OR "
                                                  + MessageColumns.Status.ToString() + " = '{3}' OR "
                                                  + MessageColumns.Status.ToString() + " = '{4}')";


        /// <summary>
        /// Update message into message_schema
        /// </summary>
        internal static string SQ024 = "UPDATE " + Tables.message_schema.ToString() + " SET "
                                                 + MessageColumns.MessageDate.ToString() + " = '{0}', " 
                                                 + MessageColumns.ArchiveDate.ToString() + " = '{1}'," 
                                                 + MessageColumns.FromAddress.ToString() + " = '{2}'," 
                                                 + MessageColumns.ToAddress.ToString() + " = '{3}'," 
                                                 + MessageColumns.RefMessageID.ToString() + " = {4}," 
                                                 + MessageColumns.Subject.ToString() + " = '{5}'," 
                                                 + MessageColumns.Body.ToString() + " = '{6}'," 
                                                 + MessageColumns.Status.ToString() + " = '{7}', " 
                                                 + MessageColumns.Cc.ToString() + " = '{8}' "  + " WHERE " 
                                                 + MessageColumns.MessageID.ToString() + " = {9}";

        /// <summary>
        /// Update message into message_schema STATUS
        /// </summary>
        internal static string SQ053 = "UPDATE " + Tables.message_schema.ToString() + " SET "                                                
                                                 + MessageColumns.Status.ToString() + " = '{0}'"
                                                 + " WHERE "
                                                 + MessageColumns.MessageID.ToString() + " = {1}"; 

        internal static string SQLDeleteMail = "DELETE FROM " + Tables.message_schema.ToString() + " WHERE "
                                                      + MessageColumns.MessageID.ToString() + " = {0}";
        internal static string SQLDeleteAttachment = "DELETE FROM " + Tables.mail_attachment.ToString() + " WHERE "
                                                      + MailAttachmentColumns.MessageID.ToString() + " = {0}";


        /// <summary>
        /// Get Latest Message id number
        /// </summary>
        internal static string SQ027 = "SELECT MAX("+MessageColumns.MessageID.ToString()+") FROM " + Tables.message_schema.ToString();

        /// <summary>
        /// Insert Mail attachment message into database.
        /// </summary>
        internal static string SQ028 = "INSERT INTO " + Tables.mail_attachment.ToString() +
                                                   " (" + MailAttachmentColumns.MessageID.ToString() + "," +
                                                          MailAttachmentColumns.AttachName.ToString() + "," +
                                                          MailAttachmentColumns.AttachFile.ToString() + "," +
                                                          MailAttachmentColumns.Status.ToString() + 
                                                         ") VALUES ({0},'{1}',{2},'{3}')";

        /// <summary>
        /// Get email attachment names for the particular messageId
        /// </summary>
        internal static string SQ029 = "SELECT " + MailAttachmentColumns.AttachName.ToString() + ","
                                                  + MailAttachmentColumns.AttachID.ToString() + ","
                                                  + MailAttachmentColumns.MessageID.ToString() + " FROM "
                                                  + Tables.mail_attachment.ToString() + " WHERE "
                                                  + MailAttachmentColumns.MessageID.ToString() + " = {0} AND ("
                                                  + MailAttachmentColumns.Status.ToString() + " = '{1}' OR "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{2}' OR "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{3}' OR "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{4}')";


        /// <summary>
        /// Get email attachment files for the particular messageId
        /// </summary>
        internal static string SQ030 = "SELECT " + MailAttachmentColumns.AttachName.ToString() + ","
                                                 + MailAttachmentColumns.AttachFile.ToString() + ","   
                                                  + MailAttachmentColumns.AttachID.ToString() + " FROM "
                                                  + Tables.mail_attachment.ToString() + " WHERE "
                                                  + MailAttachmentColumns.MessageID.ToString() + " = {0} AND ("
                                                  + MailAttachmentColumns.Status.ToString() + " = '{1}' OR "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{2}' OR "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{3}')";



		//bug 481
        /// <summary>
        /// Get email attachment files for the particular messageId with any status
        /// </summary>
        internal static string SQ070 = "SELECT " + MailAttachmentColumns.AttachName.ToString() + ","
                                                 + MailAttachmentColumns.AttachFile.ToString() + ","
                                                  + MailAttachmentColumns.AttachID.ToString() + " FROM "
                                                  + Tables.mail_attachment.ToString() + " WHERE "
                                                  + MailAttachmentColumns.MessageID.ToString() + " = {0}";  




        /// <summary>
        /// Get email attachment files for the particular messageId and file name
        /// </summary>
        internal static string SQ031 = "SELECT "  + MailAttachmentColumns.AttachFile.ToString() + " FROM "
                                                  + Tables.mail_attachment.ToString() + " WHERE "
                                                  + MailAttachmentColumns.MessageID.ToString() + " = {0} AND ( "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{1}' OR "
                                                   + MailAttachmentColumns.Status.ToString() + " = '{4}' OR "
                                                   + MailAttachmentColumns.Status.ToString() + " = '{5}' OR "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{3}' ) AND "
                                                  + MailAttachmentColumns.AttachName.ToString() + " = '{2}'";

        /// <summary>
        /// Change Message status
        /// </summary>
        internal static string SQ032 = "Update " + Tables.message_schema.ToString() + " SET "
                                                  + MessageColumns.Status.ToString() + " = '{1}' WHERE "
                                                  + MessageColumns.MessageID.ToString() + " = {0}";
        /// <summary>
        /// Change Message status
        /// </summary>
        internal static string SQ033 = "Update " + Tables.mail_attachment.ToString() + " SET "
                                                  + MailAttachmentColumns.Status.ToString() + " = '{1}' WHERE "
                                                  + MailAttachmentColumns.MessageID.ToString() + " = {0}";
        /// <summary>
        /// Update message into message_schema
        /// </summary>
        internal static string SQ034 = "UPDATE " + Tables.message_schema.ToString() + " SET "
                                                 + MessageColumns.MessageDate.ToString() + " = '{0}', "
                                                 + MessageColumns.FromAddress.ToString() + " = '{1}',"
                                                 + MessageColumns.ToAddress.ToString() + " = '{2}',"
                                                 + MessageColumns.Subject.ToString() + " = '{3}',"
                                                 + MessageColumns.Body.ToString() + " = '{4}',"
                                                 + MessageColumns.Status.ToString() + " = '{5}', "
                                                 + MessageColumns.Cc.ToString() + " = '{6}' " + " WHERE "
                                                 + MessageColumns.MessageID.ToString() + " = {7}";

        /// <summary>
        /// Update message into message_schema
        /// </summary>
        internal static string SQ035 = "SELECT " + MessageColumns.MessageID.ToString() + ", "
                                                  + MessageColumns.ToAddress.ToString() + ", "
                                                  + MessageColumns.Cc.ToString() + ", "  
                                                  + MessageColumns.Subject.ToString() + ", "
                                                  + MessageColumns.Body.ToString()  + " FROM "
                                                  + Tables.message_schema.ToString() + " WHERE "
                                                  + MessageColumns.Status.ToString() + " = '{0}' AND "
                                                  + MessageColumns.ToAddress.ToString() + " IS NOT NULL AND "
                                                  + MessageColumns.ToAddress.ToString() + " != '' AND "
                                                  + MessageColumns.ToAddress.ToString() + " != 'support@zed-systems.com' "  
                                                  + "ORDER BY " + MessageColumns.MessageID.ToString() + " DESC";

        /// <summary>
        /// Update message into message_schema
        /// </summary>
        internal static string SQ036 = "Update " + Tables.message_schema.ToString() + " SET "
                                                  + MessageColumns.ReadFlag.ToString() + " = 1 WHERE "
                                                  + MessageColumns.MessageID.ToString() + " = {0}";


        /// <summary>
        /// Update message into message_schema
        /// </summary>
        internal static string SQ037 = "SELECT COUNT(" + MessageColumns.MessageID.ToString() + ") FROM "
                                                  + Tables.message_schema.ToString() + " WHERE "
                                                  + MessageColumns.Status.ToString() + " = '{0}' AND "
                                                  + MessageColumns.ToAddress.ToString() + " IS NOT NULL AND "
                                                  + MessageColumns.ToAddress.ToString() + " != '' ";

        /// <summary>
        /// Updating the trading partner by ID
        /// </summary>
        internal static string SQ038 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                                + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                                + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
                                                + TradingPartnerColumns.EmailID.ToString() + " = '{2}',"
                                                + TradingPartnerColumns.AlternateEmailID.ToString() + " = '{3}',"
                                                + TradingPartnerColumns.ClientID.ToString() + " = '{4}',"
                                                + TradingPartnerColumns.XSDLocation.ToString() + " = '{5}' WHERE "
                                                + TradingPartnerColumns.TradingPartnerID.ToString()+" = {6}";

        /// <summary>
        /// Delete trading partner details by ID
        /// </summary>
        internal static string SQ039 = "DELETE FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                        + TradingPartnerColumns.TradingPartnerID.ToString() + " = {0}";

        /// <summary>
        /// Select TradingPartner Details
        /// </summary>
        internal static string SQ040 = "SELECT TP." + TradingPartnerColumns.TradingPartnerID.ToString() + ",TP."
                                               + TradingPartnerColumns.TPName.ToString() + ",MP."
                                               + MailProtocolColumns.MailProtocol.ToString() + ",TP."
                                               + TradingPartnerColumns.EmailID.ToString() + ",TP."
                                               + TradingPartnerColumns.AlternateEmailID.ToString() + ",TP."
                                               + TradingPartnerColumns.ClientID.ToString() + ",TP."
                                               + TradingPartnerColumns.XSDLocation.ToString() + " FROM "
                                               + Tables.trading_partner_schema.ToString() + " TP,"
                                               + Tables.mail_protocol_schema.ToString() + " MP WHERE TP."
                                               + TradingPartnerColumns.MailProtocolID.ToString() + " = MP."
                                               + MailProtocolColumns.MailProtocolID.ToString();


        /// <summary>
        /// Get Trading Partner Details.
        /// </summary>
        internal static string TPSQ040 = "SELECT TP." + TradingPartnerColumns.TradingPartnerID.ToString() + ",TP."
                                              + TradingPartnerColumns.TPName.ToString() + ",MP."
                                              + MailProtocolColumns.MailProtocol.ToString() + ",TP."
                                              + TradingPartnerColumns.EmailID.ToString() + ",TP."
                                              + TradingPartnerColumns.AlternateEmailID.ToString() + ",TP."
                                              + TradingPartnerColumns.ClientID.ToString() + ",TP."
                                              + TradingPartnerColumns.XSDLocation.ToString() + " ,TP."
                                              + TradingPartnerColumns.UserName.ToString() + ",TP."
                                              + TradingPartnerColumns.Password.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayAuthToken.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayDevId.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayCertId.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayAppId.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayItemMapping.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayItem.ToString() + ",TP."
                                              + TradingPartnerColumns.eBayDiscountItem.ToString() + ",TP."
                                               + MessageColumns.LastUpdatedDate.ToString() + ",TP."
                                              + TradingPartnerColumns.OSCWebStoreUrl.ToString() + ",TP."
                                              + TradingPartnerColumns.OSCDatabase.ToString() + ",TP."
                                              + TradingPartnerColumns.AccountNumber.ToString() + ",TP."
                                              + TradingPartnerColumns.CompanyName.ToString() + " FROM "
                                              + Tables.trading_partner_schema.ToString() + " TP,"
                                              + Tables.mail_protocol_schema.ToString() + " MP WHERE TP."
                                              + TradingPartnerColumns.MailProtocolID.ToString() + " = MP."
                                              + MailProtocolColumns.MailProtocolID.ToString();
        /// <summary>
        /// Get all records of trading_partner_Schema.
        /// </summary>
        internal static string SQ041 = "SELECT " + TradingPartnerColumns.EmailID.ToString() + " FROM "
                                       + Tables.trading_partner_schema.ToString() + " WHERE "
                                       + TradingPartnerColumns.MailProtocolID + " = 1 ";



   /// <summary>
        /// Get all records of message_status_schema.
        /// </summary>
        internal static string SQ042 = "SELECT " + MessageStatusColumns.Name.ToString() + " FROM " 
                                       + Tables.message_status_schema.ToString() + "";

        /// <summary>
        /// Get all records of message_status_schema for message rule.
        /// </summary>
        internal static string SQ054 = "SELECT " + MessageStatusColumns.Name.ToString() + " FROM "
                                       + Tables.message_status_schema.ToString() + " WHERE "
                                       + MessageStatusColumns.StatusId + " != 3 ";

        /// <summary>
        /// Get the service names from message _monitor.
        /// </summary>
        internal static string SQMServiceName = "SELECT " + MessageMonitorColumns.Name.ToString() + " FROM "
                                       + Tables.message_monitor.ToString() + "";

        
        /// <summary>
        /// This Query is used for insert Message Monitor Details into MYOB.
        /// </summary>
        internal static string SQMS28 = "INSERT INTO " + Tables.message_monitor.ToString() +
                                                  " (" + MessageMonitorColumns.Name.ToString() + "," +
                                                         MessageMonitorColumns.Folder.ToString() + "," +
                                                         MessageMonitorColumns.Period.ToString() + "," +
                                                         MessageMonitorColumns.IsActive.ToString() +
                                                        ") VALUES ('{0}','{1}','{2}',{3})";


 /// <summary>
        /// Insert record into the message_rule.
        /// </summary>
        internal static string SQ043 = "INSERT INTO " + Tables.message_rule.ToString() + "("
                                         + MessageRulesColumn.Name.ToString()+","
                                         + MessageRulesColumn.TradingPartnerID.ToString()+","
                                         + MessageRulesColumn.CondSubEquals.ToString()+","
                                         + MessageRulesColumn.CondSubMatches.ToString()+","
                                         + MessageRulesColumn.CondFileEquals.ToString()+","
                                         + MessageRulesColumn.CondFileMatches.ToString()+","
                                         + MessageRulesColumn.ActTransType.ToString()+","
                                         + MessageRulesColumn.ActUsingMappingId.ToString()+","
                                         + MessageRulesColumn.OnCompleteStatusID.ToString()+","
                                         + MessageRulesColumn.OnCompleteErrStatusId.ToString()+","
                                         + MessageRulesColumn.Status.ToString()+","
                                         + MessageRulesColumn.SoundChime.ToString()+","
                                         + MessageRulesColumn.SoundAlarm.ToString()+")"
                                         + " VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',{11},{12})";
        

        /// <summary>
  /// <summary>
        /// Update rule into the message_rule.
        /// </summary>
        internal static string  SQ044 = "UPDATE " + Tables.message_rule.ToString() + " SET "
                                         + MessageRulesColumn.Name.ToString()+" = '{0}', "
                                         + MessageRulesColumn.TradingPartnerID.ToString()+" = '{1}', "
                                         + MessageRulesColumn.CondSubEquals.ToString()+" = '{2}', "
                                         + MessageRulesColumn.CondSubMatches.ToString()+" = '{3}', "
                                         + MessageRulesColumn.CondFileEquals.ToString()+" = '{4}', "
                                         + MessageRulesColumn.CondFileMatches.ToString()+" = '{5}', "
                                         + MessageRulesColumn.ActTransType.ToString()+" = '{6}', "
                                         + MessageRulesColumn.ActUsingMappingId.ToString()+" = '{7}', "
                                         + MessageRulesColumn.OnCompleteStatusID.ToString()+" = '{8}', "
                                         + MessageRulesColumn.OnCompleteErrStatusId.ToString()+" = '{9}', "
                                         + MessageRulesColumn.Status.ToString()+" = '{10}', "
                                         + MessageRulesColumn.SoundChime.ToString()+" = {11}, "
                                         + MessageRulesColumn.SoundAlarm.ToString()+" = {12} WHERE "
                                         + MessageRulesColumn.RulesId.ToString()+" = {13} ";

        /// <summary>
        /// Get records from message_rule.
        /// </summary>
        internal static string SQ045 = "SELECT " + MessageRulesColumn.RulesId.ToString() + ","
                                                + MessageRulesColumn.Name.ToString()+","
                                                + MessageRulesColumn.TradingPartnerID.ToString()+","
                                                + MessageRulesColumn.CondSubEquals.ToString()+","
                                                + MessageRulesColumn.CondSubMatches.ToString()+","
                                                + MessageRulesColumn.CondFileEquals.ToString()+","
                                                + MessageRulesColumn.CondFileMatches.ToString()+","
                                                + MessageRulesColumn.ActTransType.ToString()+","
                                                + MessageRulesColumn.ActUsingMappingId.ToString()+","
                                                + MessageRulesColumn.OnCompleteStatusID.ToString()+","
                                                + MessageRulesColumn.OnCompleteErrStatusId.ToString()+","
                                                + MessageRulesColumn.Status.ToString()+","
                                                + MessageRulesColumn.SoundChime.ToString()+","
                                                + MessageRulesColumn.SoundAlarm.ToString()+" FROM "
                                                + Tables.message_rule.ToString();


        /// <summary>
        /// Delete message rule details by ID.
        /// </summary>
        internal static string SQ046 = "DELETE FROM " + Tables.message_rule.ToString() + " WHERE "
                                        + MessageRulesColumn.RulesId.ToString() + " = {0}";

        
        /// <summary>
        /// Get number of record(s) from message_rule where rulename is matched.
        /// </summary>
        internal static string SQ047 = "SELECT COUNT(*)  FROM " + Tables.message_rule.ToString() + " WHERE "
                                        + MessageRulesColumn.Name.ToString() + " = '{0}' AND "
                                         + MessageRulesColumn.RulesId.ToString()+" != {1}";

    

        internal static string SQ048 = "SELECT * FROM " + Tables.message_schema.ToString() + " ORDER BY "
                                        + MessageColumns.MessageID.ToString()+ " DESC ";

        internal static string SQ049 = "SELECT * FROM " + Tables.mail_attachment.ToString() + " WHERE "
                                        + MailAttachmentColumns.MessageID.ToString() + " = {0} ";

        /// <summary>
        /// Get records from active message_rule.
        /// </summary>
        internal static string SQ050 = "SELECT " + MessageRulesColumn.RulesId.ToString() + ","
                                                + MessageRulesColumn.Name.ToString() + ","
                                                + MessageRulesColumn.TradingPartnerID.ToString() + ","
                                                + MessageRulesColumn.CondSubEquals.ToString() + ","
                                                + MessageRulesColumn.CondSubMatches.ToString() + ","
                                                + MessageRulesColumn.CondFileEquals.ToString() + ","
                                                + MessageRulesColumn.CondFileMatches.ToString() + ","
                                                + MessageRulesColumn.ActTransType.ToString() + ","
                                                + MessageRulesColumn.ActUsingMappingId.ToString() + ","
                                                + MessageRulesColumn.OnCompleteStatusID.ToString() + ","
                                                + MessageRulesColumn.OnCompleteErrStatusId.ToString() + ","
                                                + MessageRulesColumn.Status.ToString() + ","
                                                + MessageRulesColumn.SoundChime.ToString() + ","
                                                + MessageRulesColumn.SoundAlarm.ToString() + " FROM "
                                                + Tables.message_rule.ToString() + " WHERE "
                                                + MessageRulesColumn.Status.ToString() + " = 'Active' ";


        /// <summary>
        /// Update message into mail_attachment.
        /// </summary>
		//bug481
        internal static string SQ051 = "UPDATE " + Tables.mail_attachment.ToString() + " SET "
                                                 + MailAttachmentColumns.Status + " = {0} ,"
                                                 + MailAttachmentColumns.AttachFile + " = '{1}'"
                                                 + " WHERE "
                                                 + MessageColumns.MessageID.ToString() + " = {2}";

        #region Shipping Request Queries

        /// <summary>
        ///Get mailProtocolId from message_schema.
        /// </summary>
        internal static string SQ052 = "SELECT " + MessageColumns.MailProtocolId.ToString() + " FROM " +
                                       Tables.message_schema.ToString() + " WHERE " +
                                       MessageColumns.MessageID.ToString() + " = {0}";

        /// <summary>
        ///Get MailProtocol Name from mail_protocol_schema.
        /// </summary>
        internal static string SQ055 = "SELECT " + MailProtocolColumns.MailProtocol.ToString() + " FROM " +
                                       Tables.mail_protocol_schema.ToString() + " WHERE " +
                                       MailProtocolColumns.MailProtocolID.ToString() + " = {0}";


        /// <summary>
        /// Get Company name of Allied Express from Trading Partner Address Book.
        /// </summary>
        internal static string SQ056 = "SELECT " + TradingPartnerColumns.CompanyName.ToString() + " FROM " +
                                       Tables.trading_partner_schema.ToString() + " WHERE " +
                                       "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + 
                                       Tables.mail_protocol_schema.ToString() + " WHERE " +
                                       MailProtocolColumns.MailProtocol.ToString() +  " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();

        /// <summary>
        /// Get Account Number of Allied Express from Trading Partner Adddress Book.
        /// </summary>
        internal static string SQ057 = "SELECT " + TradingPartnerColumns.AccountNumber.ToString() + " FROM " +
                                       Tables.trading_partner_schema.ToString() + " WHERE " +
                                       "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " +
                                       Tables.mail_protocol_schema.ToString() + " WHERE " +
                                       MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();



        /// <summary>
        /// Get Ebay Authentication ticket from trading partner schema;
        /// </summary>
        internal static string SQ058 = "SELECT " + TradingPartnerColumns.eBayAuthToken.ToString() + " FROM " +
                                       Tables.trading_partner_schema.ToString() + " WHERE " +
                                       TradingPartnerColumns.TradingPartnerID.ToString() + " = {0}";

        internal static string SQ0EbayType = "SELECT " + TradingPartnerColumns.CompanyName.ToString() + " FROM " +
                                      Tables.trading_partner_schema.ToString() + " WHERE " +
                                      TradingPartnerColumns.TradingPartnerID.ToString() + " = {0}";

        /// <summary>
        /// Update message status to sent.
        /// </summary>        
        internal static string SQ059 = "UPDATE " + Tables.message_schema.ToString() + " SET "
                                                 + MessageColumns.Status.ToString() + " = '{0}'" +  " WHERE "
                                                 + MessageColumns.MessageID.ToString() + " = {1}";

        /// <summary>
        /// Change mail attachment status.
        /// </summary>
        internal static string SQ060 = "Update " + Tables.mail_attachment.ToString() + " SET "
                                                 + MailAttachmentColumns.Status.ToString() + " = '{0}' WHERE "
                                                 + MailAttachmentColumns.MessageID.ToString() + " = {1}";


        /// <summary>
        /// Get Company Phone Number of Allied Express from trading partner address Book.
        /// </summary>
        internal static string SQ061 = "SELECT " + TradingPartnerColumns.EmailID.ToString() + " FROM " +
                                       Tables.trading_partner_schema.ToString() + " WHERE " +
                                       "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " +
                                       Tables.mail_protocol_schema.ToString() + " WHERE " +
                                       MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();

        /// <summary>
        /// Get Prefix for cannote number within Allied Express System.
        /// Alternate Email ID is used to store Prefix for cannote number.
        /// </summary>
        internal static string SQ062 = "SELECT " + TradingPartnerColumns.AlternateEmailID.ToString() + " FROM " +
                                       Tables.trading_partner_schema.ToString() + " WHERE " +
                                       "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " +
                                       Tables.mail_protocol_schema.ToString() + " WHERE " +
                                       MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();

        /// <summary>
        /// Get type of connection with Allied Express.
        /// </summary>
        internal static string SQ063 = "SELECT " + TradingPartnerColumns.eBayItemMapping.ToString() + " FROM " +
                                        Tables.trading_partner_schema.ToString() + " WHERE " +
                                        "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " +
                                        Tables.mail_protocol_schema.ToString() + " WHERE " +
                                        MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();

        /// <summary>
        /// Get Account Code with Allied Express.
        /// </summary>
        internal static string SQ064 = "SELECT " + TradingPartnerColumns.UserName.ToString() + " FROM " +
                                        Tables.trading_partner_schema.ToString() + " WHERE " +
                                        "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " +
                                        Tables.mail_protocol_schema.ToString() + " WHERE " +
                                        MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();

        /// <summary>
        /// Get Allied Express State within Allied Express Sytem.
        /// </summary>
        internal static string SQ065 = "SELECT " + TradingPartnerColumns.Password.ToString() + " FROM " +
                                        Tables.trading_partner_schema.ToString() + " WHERE " +
                                        "(SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " +
                                        Tables.mail_protocol_schema.ToString() + " WHERE " +
                                        MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')" + " = " + TradingPartnerColumns.MailProtocolID.ToString();


         
        /// <summary>
        /// Fetch the OSCommerce Details by tradingPartnerID.
        /// </summary>
        internal static string TPWithOSCommerce = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE  "
                                     + TradingPartnerColumns.TradingPartnerID + " = {0}";

        //Axis 8
        /// <summary>
        /// Fetch the XCart Details by tradingPartnerID.
        /// </summary>
        internal static string TPWithXCart = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE  "
                                     + TradingPartnerColumns.TradingPartnerID + " = {0}";

        /// <summary>
        /// Get max order status id from orders_status_history.
        /// </summary>
        internal static string OSCommerceMaxStatusID = " SELECT MAX(" + OrdersStatusHistory.orders_status_id.ToString() + ")" + " FROM {0}" +
                                                    Tables.orders_status_history.ToString() + " WHERE " +
                                                    OrdersStatusHistory.orders_id.ToString() + " = {1}";
         
       
        /// <summary>
        /// Update status in order_status_history.
        /// </summary>
        /// " UPDATE trading_partner_schema SET LastUpdatedDate = '" + MessageDate + "' WHERE TradingPartnerID =" + tradingPartnerID + "";
        internal static string UpdateStatus = " UPDATE  {0}" + Tables.orders_status_history.ToString() + " SET " + OrdersStatusHistory.date_added.ToString()
                                              + " = '{1}'," + OrdersStatusHistory.comments.ToString() + " = '{2}' " + " WHERE " + 
                                              OrdersStatusHistory.orders_id + " = {3}" + " And " + OrdersStatusHistory.orders_status_id + " = {4}";

        internal static string SelectPendingStatus = " SELECT " + OrderStatus.order_status_id.ToString() + " FROM {0}" + Tables.order_status.ToString() 
                                                     + " WHERE " + OrderStatus.order_status_name.ToString() + " = 'Processing'";
        internal static string UpdatePendingStatus = "UPDATE {0}" + Tables.orders.ToString() + " SET " + Orders.orders_status.ToString() + " = 2" 
                                                     + " WHERE " + Orders.orders_id + " = {1}";
       
                
        
        /// <summary>
        ///Insert order status in order_status_history.
        /// </summary>
        internal static string InsertStatus = " INSERT INTO {0}" + Tables.orders_status_history.ToString() + "(orders_id, orders_status_id, date_added, comments)" 
                                             + " VALUES ( '{1}' , '{2}', '{3}', '{4}')";
        /// <summary>
        /// Get AttachID from mail_atttachment using messageID.
        /// </summary>
        internal static string MAWithAttachID = "SELECT * FROM " 
                                             + Tables.mail_attachment.ToString() + " WHERE "
                                             + MailAttachmentColumns.MessageID.ToString() + " = {0}";
        /// <summary>
        /// Update Attachment when booking is send to Allied Express.
        /// </summary>
        internal static string UpdateAttachFile = "UPDATE " + Tables.mail_attachment.ToString() + " SET "
                                             + MailAttachmentColumns.AttachFile.ToString() + " = '{0}'" + " WHERE "
                                             + MailAttachmentColumns.AttachID.ToString() + " = '{1}'";
        
        #endregion

        /// This Query is used for Update Message Monitor Details into MYOB.
        /// </summary>
        internal static string SQMS29 = "UPDATE " + Tables.message_monitor.ToString() + " SET "
                                                 + MessageMonitorColumns.Name.ToString() + " = '{0}',"
                                                 + MessageMonitorColumns.Folder.ToString() + " = '{1}',"
                                                 + MessageMonitorColumns.Period.ToString() + " = '{2}',"
                                                 + MessageMonitorColumns.IsActive.ToString() + " = '{3}'"
                                                 + " WHERE " + MessageMonitorColumns.MonitorId.ToString() + " = {4}";


        /// <summary>
        /// This Query is used for delete the Monitor Service Details from Database By Passing Monitor ID.
        /// </summary>
        internal static string SQMS30 = "DELETE FROM " + Tables.message_monitor.ToString() +
                                                  " WHERE " + MessageMonitorColumns.MonitorId.ToString() + " = {0}";


        /// <summary>
        /// This Query is used for getting details of service by Passing Service Name.
        /// </summary>
        internal static string SQMS31 = "SELECT * FROM " + Tables.message_monitor.ToString() +
                                                  " WHERE " + MessageMonitorColumns.Name.ToString() + " = '{0}'";

        /// <summary>
        /// This Query is used for checking duplicate service name by Passing Service Name.
        /// </summary>
        internal static string SQMS32 = "SELECT COUNT(*) FROM " + Tables.message_monitor.ToString() +
                                                  " WHERE " + MessageMonitorColumns.Name.ToString() + " = '{0}'";

        /// <summary>
        /// This Query is used for checking duplicate trading partner name by Passing trading partner.
        /// </summary>
        internal static string SQMSTP32 = "SELECT COUNT(*) FROM " + Tables.trading_partner_schema.ToString() +
                                                " WHERE " + TradingPartnerColumns.TPName + " = '{0}'";

        /// <summary>
        /// This Query is used for checking allied express trading partner.
        /// </summary>
        internal static string SQMSAE01 = "SELECT COUNT(*) FROM " + Tables.trading_partner_schema.ToString() +
                                               " WHERE " + TradingPartnerColumns.MailProtocolID + " = {0}";

        /// <summary>
        /// This Query is used for getting service details.
        /// </summary>
        internal static string SQMS33 = "SELECT * FROM " + Tables.message_monitor.ToString() + "";

        /// <summary>
        /// This Query is used for getting MonitorID by passing Service Name.
        /// </summary>
        internal static string SQMS34 = "SELECT " + MessageMonitorColumns.MonitorId.ToString() + " FROM " + Tables.message_monitor.ToString() + " WHERE " + MessageMonitorColumns.Name.ToString() + "  ='{0}'";

        internal static string SQLGetMsg = "SELECT * FROM " + Tables.message_schema.ToString() + " WHERE "
                                                 + MessageColumns.ReadFlag + " = 0 AND "
                                                 + MessageColumns.Status.ToString() + " = 3 ";



        ///
        internal static string SQ66 = "SELECT " + MessageStatusColumns.StatusId + " FROM "
                                       + Tables.message_status_schema.ToString() + " WHERE "
                                       + MessageStatusColumns.Name.ToString() + " ='{0}' ";

         

        internal static string SQ67 = "UPDATE " + Tables.message_schema.ToString() + " SET "
                                                 + MessageColumns.Status.ToString() + " = {0}"
                                                 + " WHERE " +
                                                 MessageColumns.FromAddress.ToString()+"=( SELECT " + MessageRulesColumn.TradingPartnerID + " FROM " + Tables.message_rule + " WHERE " + MessageRulesColumn.RulesId + "= {1})";
       

       // (SELECT TradingPartnerID FROM message_rule WHERE rulesid=6)

        ///

        /// <summary>
        /// Insert new eBay record into trading_partner_schema
        /// </summary>
        internal static string eBaySQL0001 = "INSERT INTO " + Tables.trading_partner_schema.ToString() + "("
                                         + TradingPartnerColumns.TPName.ToString() + ","
                                         + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                         + TradingPartnerColumns.UserName.ToString() + ","
                                         + TradingPartnerColumns.Password.ToString() + ","
                                         + TradingPartnerColumns.eBayAuthToken.ToString() + ","
                                         + TradingPartnerColumns.eBayDevId.ToString() + ","
                                         + TradingPartnerColumns.eBayCertId.ToString() + ","
                                         + TradingPartnerColumns.eBayAppId.ToString() + ","
                                         + TradingPartnerColumns.eBayItemMapping.ToString() + ","
                                         + TradingPartnerColumns.eBayItem.ToString() + ","
                                         + TradingPartnerColumns.eBayDiscountItem.ToString() + ","
                                         + TradingPartnerColumns.CompanyName.ToString() + ","
                                         + MessageColumns.LastUpdatedDate.ToString() + ")"
                                         + " VALUES ('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}')";

        /// <summary>
        /// Insert new OSCommerce record into trading_partner_schema
        /// </summary>
        internal static string OSCommerceSQL0001 = "INSERT INTO " + Tables.trading_partner_schema.ToString() + "("
                                         + TradingPartnerColumns.TPName.ToString() + ","
                                         + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                         + TradingPartnerColumns.UserName.ToString() + ","
                                         + TradingPartnerColumns.Password.ToString() + ","
                                         + TradingPartnerColumns.OSCWebStoreUrl.ToString() + ","
                                         + TradingPartnerColumns.eBayAuthToken.ToString() + ","
                                         + TradingPartnerColumns.OSCDatabase.ToString() + ","
                                         + TradingPartnerColumns.eBayItem.ToString() + ","
                                         + TradingPartnerColumns.eBayItemMapping.ToString() + ","
                                         + TradingPartnerColumns.eBayDevId.ToString() + ","
                                         + TradingPartnerColumns.eBayCertId.ToString() + ","
                                         + TradingPartnerColumns.eBayAppId.ToString() + ","
                                         + TradingPartnerColumns.XSDLocation.ToString() + ","
                                         + MessageColumns.LastUpdatedDate.ToString() + ")"
                                         + " VALUES ('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')";

        //X-Cart
        /// <summary>
        /// Insert new X-Cart record into trading_partner_schema
        /// </summary>
        internal static string XCartSQL0001 = "INSERT INTO " + Tables.trading_partner_schema.ToString() + "("
                                         + TradingPartnerColumns.TPName.ToString() + ","
                                         + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                         + TradingPartnerColumns.UserName.ToString() + ","
                                         + TradingPartnerColumns.Password.ToString() + ","
                                         + TradingPartnerColumns.OSCWebStoreUrl.ToString() + ","
                                         + TradingPartnerColumns.eBayAuthToken.ToString() + ","
                                         + TradingPartnerColumns.OSCDatabase.ToString() + ","
                                         + TradingPartnerColumns.eBayItem.ToString() + ","
                                         + TradingPartnerColumns.eBayItemMapping.ToString() + ","
                                         + TradingPartnerColumns.eBayDevId.ToString() + ","
                                         + TradingPartnerColumns.eBayCertId.ToString() + ","
                                         + TradingPartnerColumns.eBayAppId.ToString() + ","
                                         + TradingPartnerColumns.XSDLocation.ToString() + ","
                                         + MessageColumns.LastUpdatedDate.ToString() + ")"
                                         + " VALUES ('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')";


        /// <summary>
        /// Insert new ZenCart record into trading_partner_schema
        /// </summary>
        internal static string ZenCartSQL001 = "INSERT INTO " + Tables.trading_partner_schema.ToString() + "("
                                         + TradingPartnerColumns.TPName.ToString() + ","
                                         + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                         + TradingPartnerColumns.UserName.ToString() + ","
                                         + TradingPartnerColumns.Password.ToString() + ","
                                         + TradingPartnerColumns.OSCWebStoreUrl.ToString() + ","
                                         + TradingPartnerColumns.eBayAuthToken.ToString() + ","
                                         + TradingPartnerColumns.OSCDatabase.ToString() + ","
                                         + TradingPartnerColumns.eBayItem.ToString() + ","
                                         + TradingPartnerColumns.eBayItemMapping.ToString() + ","
                                         + TradingPartnerColumns.eBayDevId.ToString() + ","
                                         + TradingPartnerColumns.eBayCertId.ToString() + ","
                                         + TradingPartnerColumns.eBayAppId.ToString() + ","
                                         + TradingPartnerColumns.XSDLocation.ToString() + ","
                                         + MessageColumns.LastUpdatedDate.ToString() + ")"
                                         + " VALUES ('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}')";



        /// <summary>
        /// Insert new Allied Express record into trading_partner_schema
        /// EmailID: used to store company Phone.
        /// Alternate EmailID used to store Prefix for cannote number.
        /// UserName used to store Account Code.
        /// Password used to store AEState.
        /// </summary>
        internal static string AlliedExpressSQL0001 = "INSERT INTO " + Tables.trading_partner_schema.ToString() + "("
                                         + TradingPartnerColumns.TPName.ToString() + ","
                                         + TradingPartnerColumns.MailProtocolID.ToString() + ","
                                         + TradingPartnerColumns.AccountNumber.ToString() + ","
                                         + TradingPartnerColumns.UserName.ToString() + ","
                                         + TradingPartnerColumns.Password.ToString() + ","
                                         + TradingPartnerColumns.CompanyName.ToString() + ","
                                         + TradingPartnerColumns.EmailID.ToString() + ","
                                         + TradingPartnerColumns.eBayItemMapping.ToString() + ","
                                         + TradingPartnerColumns.AlternateEmailID.ToString() + ")"
                                         + " VALUES ('{0}',{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}')";

        /// <summary>
        /// Select Trading Partner details using trading partner id of trading_partner_schema
        /// </summary>
        internal static string eBaySQL0002 = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                         + TradingPartnerColumns.TradingPartnerID.ToString() + " = {0} ";

        /// <summary>
        /// Select Trading Partner details of eBay  using trading partner name of trading_partner_schema
        /// </summary>
        internal static string eBaySQL0003 = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                         + TradingPartnerColumns.TPName.ToString() + " = '{0}' AND " + TradingPartnerColumns.MailProtocolID + " = (SELECT " 
                                         + MailProtocolColumns.MailProtocolID.ToString() + " FROM "+Tables.mail_protocol_schema.ToString()+" WHERE "+ MailProtocolColumns.MailProtocol.ToString()+ " = 'eBay')";
        /// <summary>
        /// Select Trading Partner details of OSCommerce using trading partner name of trading_partner_schema
        /// </summary>
        internal static string OSCommerceSQL0002 = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                         + TradingPartnerColumns.TPName.ToString() + " = '{0}' AND " + TradingPartnerColumns.MailProtocolID + " = (SELECT "
                                         + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + Tables.mail_protocol_schema.ToString() + " WHERE " + MailProtocolColumns.MailProtocol.ToString() + " = 'OSCommerce')";


        /// <summary>
        /// Select Trading Partner details of Allied Expres using trading partner name of trading_partner_schema
        /// </summary>
        internal static string AlliedExpressSQL0002 = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                         + TradingPartnerColumns.TPName.ToString() + " = '{0}' AND " + TradingPartnerColumns.MailProtocolID + " = (SELECT "
                                         + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + Tables.mail_protocol_schema.ToString() + " WHERE " + MailProtocolColumns.MailProtocol.ToString() + " = 'Allied Express')";

        /// <summary>
        /// Select eBay and OSCommerce Trading Partner List.
        /// </summary>
        internal static string eBaySQL0004 = "SELECT " + TradingPartnerColumns.TPName.ToString() + " FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                           + TradingPartnerColumns.MailProtocolID + " in ( SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + Tables.mail_protocol_schema.ToString() + " WHERE " + MailProtocolColumns.MailProtocol.ToString() + " IN ('eBay','OSCommerce'))";

        /// <summary>
        /// Select ebay Trading Partner List
        /// </summary>
        internal static string eBaySQL0006 = "SELECT " + TradingPartnerColumns.TPName.ToString() + ", " + TradingPartnerColumns.MailProtocolID + "  FROM " + Tables.trading_partner_schema.ToString() + " WHERE "
                                          + TradingPartnerColumns.MailProtocolID + " in ( SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + Tables.mail_protocol_schema.ToString() + " WHERE " + MailProtocolColumns.MailProtocol.ToString() + " IN('eBay','OSCommerce'))";


        /// <summary>
        /// this method is used for update eBay Trading Partner details.
        /// </summary>
        internal static string eBaySQL0005 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                              + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                              + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
                                              + TradingPartnerColumns.UserName.ToString() + " = '{2}',"
                                              + TradingPartnerColumns.Password.ToString() + " = '{3}',"
                                              + TradingPartnerColumns.eBayAuthToken.ToString() + " = '{4}',"
                                              + TradingPartnerColumns.eBayItemMapping.ToString() + " = '{5}',"
                                              + TradingPartnerColumns.eBayItem.ToString() + " = '{6}',"
                                              + TradingPartnerColumns.eBayDiscountItem.ToString() + " = '{7}',"
                                              + TradingPartnerColumns.CompanyName.ToString() + " = '{8}',"
                                              + MessageColumns.LastUpdatedDate.ToString() + " = '{9}' WHERE "
                                              + TradingPartnerColumns.TradingPartnerID.ToString() + " = {10}";
        /// <summary>
        /// This query is used to update Zencart trading partner details
        /// </summary>
        internal static string ZenCartSQL0003 =  "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                            + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                            + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
                                            + TradingPartnerColumns.UserName.ToString() + " = '{2}',"
                                            + TradingPartnerColumns.Password.ToString() + " = '{3}',"
                                            + TradingPartnerColumns.OSCWebStoreUrl.ToString() + " = '{4}',"
                                            + TradingPartnerColumns.eBayAuthToken.ToString() + " = '{5}',"
                                            + TradingPartnerColumns.OSCDatabase.ToString() + " = '{6}',"
                                            + TradingPartnerColumns.eBayItem.ToString() + " = '{7}',"
                                            + TradingPartnerColumns.eBayItemMapping.ToString() + " = '{8}',"
                                            + TradingPartnerColumns.eBayDevId.ToString() + " = '{9}',"
                                            + TradingPartnerColumns.eBayCertId.ToString() + " = '{10}',"
                                            + TradingPartnerColumns.eBayAppId.ToString() + " = '{11}',"
                                            + TradingPartnerColumns.XSDLocation.ToString() + " = '{12}',"
                                            + MessageColumns.LastUpdatedDate.ToString() + " = '{13}' WHERE "
                                            + TradingPartnerColumns.TradingPartnerID.ToString() + " = {14}";



        /// <summary>
        /// This method is used for update OSCommerce Trading Partner Details.
        /// </summary>
        internal static string OSCommerceSQL0003 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                            + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                            + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
                                            + TradingPartnerColumns.UserName.ToString() + " = '{2}',"
                                            + TradingPartnerColumns.Password.ToString() + " = '{3}',"
                                            + TradingPartnerColumns.OSCWebStoreUrl.ToString() + " = '{4}',"
                                            + TradingPartnerColumns.eBayAuthToken.ToString() + " = '{5}',"
                                            + TradingPartnerColumns.OSCDatabase.ToString() + " = '{6}',"
                                            + TradingPartnerColumns.eBayItem.ToString() + " = '{7}',"
                                            + TradingPartnerColumns.eBayItemMapping.ToString() + " = '{8}',"
                                            + TradingPartnerColumns.eBayDevId.ToString() + " = '{9}',"
                                            + TradingPartnerColumns.eBayCertId.ToString() + " = '{10}',"
                                            + TradingPartnerColumns.eBayAppId.ToString() + " = '{11}',"
                                            + TradingPartnerColumns.XSDLocation.ToString() + " = '{12}',"
                                            + MessageColumns.LastUpdatedDate.ToString() + " = '{13}' WHERE "
                                            + TradingPartnerColumns.TradingPartnerID.ToString() + " = {14}";


        /// <summary>
        /// This method is used for update X-Cart Trading Partner Details.
        /// </summary>
        //internal static string XCartSQL0003 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
        //                                    + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
        //                                    + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
        //                                    + TradingPartnerColumns.UserName.ToString() + " = '{2}',"
        //                                    + TradingPartnerColumns.Password.ToString() + " = '{3}',"
        //                                    + TradingPartnerColumns.OSCWebStoreUrl.ToString() + " = '{4}',"
        //                                    + TradingPartnerColumns.eBayAuthToken.ToString() + " = '{5}',"
        //                                    + TradingPartnerColumns.OSCDatabase.ToString() + " = '{6}',"
        //                                    + TradingPartnerColumns.XSDLocation.ToString() + " = '{7}',"
        //                                    + MessageColumns.LastUpdatedDate.ToString() + " = '{8}' WHERE "
        //                                    + TradingPartnerColumns.TradingPartnerID.ToString() + " = {9}";

        internal static string XCartSQL0003 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                            + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                            + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
                                            + TradingPartnerColumns.UserName.ToString() + " = '{2}',"
                                            + TradingPartnerColumns.Password.ToString() + " = '{3}',"
                                            + TradingPartnerColumns.OSCWebStoreUrl.ToString() + " = '{4}',"
                                            + TradingPartnerColumns.eBayAuthToken.ToString() + " = '{5}',"
                                            + TradingPartnerColumns.OSCDatabase.ToString() + " = '{6}',"
					                        + TradingPartnerColumns.eBayItem.ToString() + " = '{7}',"
                                            + TradingPartnerColumns.eBayItemMapping.ToString() + " = '{8}',"
                                            + TradingPartnerColumns.eBayDevId.ToString() + " = '{9}',"
                                            + TradingPartnerColumns.eBayCertId.ToString() + " = '{10}',"
                                            + TradingPartnerColumns.eBayAppId.ToString() + " = '{11}',"
                                            + TradingPartnerColumns.XSDLocation.ToString() + " = '{12}',"
                                            + MessageColumns.LastUpdatedDate.ToString() + " = '{13}' WHERE "
                                            + TradingPartnerColumns.TradingPartnerID.ToString() + " = {14}";


        /// <summary>
        /// This method is used for update Allied Express Trading Partner Details.
        /// EmailID : used to store Company PhoneNumber.
        /// AlternateEmailId : used to store prefix for cannote number.
        /// UserName used to store AccountCode.
        /// Password used to store AEState.
        /// </summary>
        internal static string AlliedExpressSQL0003 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                        + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                        + TradingPartnerColumns.MailProtocolID.ToString() + " = {1},"
                                        + TradingPartnerColumns.AccountNumber.ToString() + " = '{2}',"
                                        + TradingPartnerColumns.UserName.ToString() + " = '{3}',"
                                        + TradingPartnerColumns.Password.ToString() + " = '{4}',"
                                        + TradingPartnerColumns.CompanyName.ToString() + " = '{5}',"
                                        + TradingPartnerColumns.EmailID.ToString() + " = '{6}',"
                                        + TradingPartnerColumns.eBayItemMapping.ToString() + " = '{7}',"
                                        + TradingPartnerColumns.AlternateEmailID.ToString() + " = '{8}' WHERE "
                                        + TradingPartnerColumns.TradingPartnerID.ToString() + " = {9}";

        /// <summary>
        /// This method is used for update Allied Express Trading Partner Details without Trading partner ID.
        /// EmailID : used to store Company PhoneNumber.
        /// UserName used to save AccountCode.
        /// Password used to save AEState.
        /// </summary>
        internal static string AlliedExpressSQL0004 = "UPDATE " + Tables.trading_partner_schema.ToString() + " SET "
                                        + TradingPartnerColumns.TPName.ToString() + " = '{0}',"
                                        + TradingPartnerColumns.AccountNumber.ToString() + " = '{1}',"
                                        + TradingPartnerColumns.UserName.ToString() + " = '{2}',"
                                        + TradingPartnerColumns.Password.ToString() + " = '{3}',"
                                        + TradingPartnerColumns.CompanyName.ToString() + " = '{4}'," 
                                        + TradingPartnerColumns.EmailID.ToString() + " = '{5}',"
                                        + TradingPartnerColumns.eBayItemMapping.ToString() + " = '{6}',"
                                        + TradingPartnerColumns.AlternateEmailID.ToString() + " = '{7}' WHERE "
                                        + TradingPartnerColumns.MailProtocolID.ToString() + " = {8}";

        
        /// <summary>
        /// This method is used to get eBay Details by passing Trading Partner details.
        /// </summary>
        internal static string eBaySQL0007 = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE " + TradingPartnerColumns.TPName + " = '{0}' AND "
                                        + TradingPartnerColumns.MailProtocolID + " in ( SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + Tables.mail_protocol_schema.ToString() + " WHERE " + MailProtocolColumns.MailProtocol.ToString() + " IN('eBay','OSCommerce'))";


        /// <summary>
        /// This method is used to get All Trading Partner List with their data.
        /// </summary>
        internal static string TPWithEBayOSCommerce = "SELECT * FROM " + Tables.trading_partner_schema.ToString() + " WHERE  "
                                        + TradingPartnerColumns.MailProtocolID + " in ( SELECT " + MailProtocolColumns.MailProtocolID.ToString() + " FROM " + Tables.mail_protocol_schema.ToString() + " WHERE " + MailProtocolColumns.MailProtocol.ToString() + " = 'eBay' OR " + MailProtocolColumns.MailProtocol.ToString() + " = 'OSCommerce' OR " + MailProtocolColumns.MailProtocol.ToString() + " = 'X-Cart' OR " + MailProtocolColumns.MailProtocol.ToString() +" = 'Zen Cart')";


        /// <summary>
        /// This method is used for getting LastUpdated Time from Trading Partner.
        /// </summary>
        internal static string TPGetLastUpdatedDate = "SELECT " + MessageColumns.LastUpdatedDate + " FROM " + Tables.trading_partner_schema + " WHERE " + TradingPartnerColumns.TradingPartnerID + " = {0}";

        /// <summary>
        /// This method is used for getting Trading Partner ID by passing message id.
        /// </summary>
        internal static string TPGetTradingPartnerID = "SELECT " + TradingPartnerColumns.TradingPartnerID + " FROM " + Tables.message_schema + " WHERE " + MessageColumns.MessageID + " = {0}";


        /// <summary>
        /// THis method is used for getting ebay Item mapping of eBay.
        /// </summary>
        internal static string TPGeteBayItemMapping = "SELECT " + TradingPartnerColumns.eBayItemMapping+ " FROM " + Tables.trading_partner_schema + " WHERE " + TradingPartnerColumns.TradingPartnerID + " = {0}";

        /// <summary>
        /// THis method is used for getting ebay Item of eBay.
        /// </summary>
        internal static string TPGeteBayShippingItem = "SELECT " + TradingPartnerColumns.eBayItem + " FROM " + Tables.trading_partner_schema + " WHERE " + TradingPartnerColumns.TradingPartnerID + " = {0}";

        /// <summary>
        /// THis method is used for getting ebay Item of eBay.
        /// </summary>
        internal static string TPGeteBayDiscountItem = "SELECT " + TradingPartnerColumns.eBayDiscountItem + " FROM " + Tables.trading_partner_schema + " WHERE " + TradingPartnerColumns.TradingPartnerID + " = {0}";

    }
}
