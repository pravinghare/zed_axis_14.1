namespace DataProcessingBlocks
{
    partial class PopUpMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PopUpMessage));
            this.labelPopUpMessage = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonLater = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.labelMessageNew = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelPopUpMessage
            // 
            this.labelPopUpMessage.AutoSize = true;
            this.labelPopUpMessage.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPopUpMessage.Location = new System.Drawing.Point(4, 21);
            this.labelPopUpMessage.Name = "labelPopUpMessage";
            this.labelPopUpMessage.Size = new System.Drawing.Size(56, 13);
            this.labelPopUpMessage.TabIndex = 0;
            this.labelPopUpMessage.Text = "Message";
            // 
            // buttonOk
            // 
            this.buttonOk.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOk.Location = new System.Drawing.Point(134, 89);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(85, 27);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "&OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonLater
            // 
            this.buttonLater.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLater.Location = new System.Drawing.Point(259, 89);
            this.buttonLater.Name = "buttonLater";
            this.buttonLater.Size = new System.Drawing.Size(119, 27);
            this.buttonLater.TabIndex = 2;
            this.buttonLater.Text = "&Remind me later";
            this.buttonLater.UseVisualStyleBackColor = true;
            this.buttonLater.Click += new System.EventHandler(this.buttonLater_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(416, 89);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(89, 27);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // labelMessageNew
            // 
            this.labelMessageNew.AutoSize = true;
            this.labelMessageNew.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMessageNew.Location = new System.Drawing.Point(5, 41);
            this.labelMessageNew.Name = "labelMessageNew";
            this.labelMessageNew.Size = new System.Drawing.Size(80, 13);
            this.labelMessageNew.TabIndex = 4;
            this.labelMessageNew.Text = "MessageNew";
            // 
            // PopUpMessage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(690, 126);
            this.Controls.Add(this.labelMessageNew);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonLater);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.labelPopUpMessage);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PopUpMessage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zed Axis";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPopUpMessage;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonLater;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelMessageNew;
    }
}