<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" />
<xsl:template match=" / ">
<xsl:element name="Configuration">
<xsl:element name="UserMappings">
<xsl:element name="mappingName">mapp</xsl:element>
<xsl:element name="MappingType">QuickBooks</xsl:element>
<xsl:element name="ImportType">TimeTracking</xsl:element>
<xsl:element name="TxnDate">TxnDate</xsl:element>
<xsl:element name="EntityFullName">EntityRefFullName</xsl:element>
<xsl:element name="CustomerFullName"></xsl:element>
<xsl:element name="ItemServiceFullName"></xsl:element>
<xsl:element name="Duration">Duration</xsl:element>
<xsl:element name="ClassFullName"></xsl:element>
<xsl:element name="PayrollItemWageFullName"></xsl:element>
<xsl:element name="Notes">Notes</xsl:element>
<xsl:element name="BillableStatus"></xsl:element>
<xsl:element name="IsBillable">IsBillable</xsl:element>
</xsl:element>
<xsl:element name="ConstantMappings">
<xsl:element name="mappingName">mapp</xsl:element>
<xsl:element name="MappingType">QuickBooks</xsl:element>
<xsl:element name="ImportType">TimeTracking</xsl:element>
<xsl:element name="TxnDate"></xsl:element>
<xsl:element name="EntityRefFullName"></xsl:element>
<xsl:element name="CustomerRefFullName"></xsl:element>
<xsl:element name="ItemServiceRefFullName"></xsl:element>
<xsl:element name="Duration"></xsl:element>
<xsl:element name="ClassRefFullName"></xsl:element>
<xsl:element name="PayrollItemWageRefFullName"></xsl:element>
<xsl:element name="Notes"></xsl:element>
<xsl:element name="BillableStatus"></xsl:element>
<xsl:element name="IsBillable"></xsl:element>
</xsl:element>
<xsl:element name="FunctionMappings">
<xsl:element name="MappingName">mapp</xsl:element>
<xsl:element name="MappingType">QuickBooks</xsl:element>
<xsl:element name="ImportType">TimeTracking</xsl:element>
<xsl:element name="TxnDate"></xsl:element>
<xsl:element name="EntityFullName"></xsl:element>
<xsl:element name="CustomerFullName"></xsl:element>
<xsl:element name="ItemServiceFullName"></xsl:element>
<xsl:element name="Duration"></xsl:element>
<xsl:element name="ClassFullName"></xsl:element>
<xsl:element name="PayrollItemWageFullName"></xsl:element>
<xsl:element name="Notes"></xsl:element>
<xsl:element name="BillableStatus"></xsl:element>
<xsl:element name="IsBillable"></xsl:element>
</xsl:element>
</xsl:element>
</xsl:template>
</xsl:stylesheet>
