using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DataProcessingBlocks;
using EDI.Constant;
using System.Threading;

namespace TransactionImporter
{
    public partial class ImportSummary : Form
    {
       
        private static ImportSummary m_frm = null;
        static Thread m_Thread = null;
        //TransactionImporter trans = TransactionImporter.GetInstance();
        static string m_Status = "";
        public bool barstatusflag = false;
        private string m_failMSG;
        private string m_succesMSG;
        private string m_SDKMsg;
        private string m_skipped;
        private DataTable m_DataTable;
       
        public string FailMsg
        {
            set { this.m_failMSG = value; }
        }
        public string SuccesMsg
        {
            set { this.m_succesMSG= value; }
        }
        public string SDKMSG
        {
            set { this.m_SDKMsg = value; }
        }
        public string Skipped
        {
            set { this.m_skipped = value; }
        }
        public DataTable ErrorDataTable
        {
            set { this.m_DataTable = value; }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Axis 10.2

        public void CloseImportSplash()
        {
            this.Close();
        }


        public string MessageToShow
        {
            set
            {
                this.labelDeleting.Text = value;
            }
            get
            {
                return this.labelDeleting.Text;
            }
        }

        public int PercentageComplete
        {
            set
            {
                this.progressBarDelet.Value = value;
            }
        }

        public bool IsEndLessLoop
        {
            set
            {
                if (value)
                {
                    this.timerStatus.Start();
                }
                else
                {
                    this.timerStatus.Stop();
                }
            }
        }
        public void SetMessageDelet(string message)
        {
            this.MessageToShow = message;
        }
        public void SetDeletPercentageComplete(int percentageComplete)
        {
            this.PercentageComplete = percentageComplete;
            
        }
     
        public  static string trantype;

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
         public ImportSummary()
         {
             InitializeComponent();
             this.timerStatus.Interval = 50;
             Cursor.Current = Cursors.WaitCursor;
         }
        public ImportSummary(string txntype)
        {
            trantype = txntype;
        }


        #endregion
        //Axis 10.2
        #region Static Methods
        /// <summary>
        /// Shows splash screen
        /// </summary>
        static public void ShowSUmmaryScreen()
        {
            // Make sure it's only launched once.
            if (m_frm != null)
                return;
            m_Thread = new Thread(new ThreadStart(ImportSummary.ShowForm));
            m_Thread.IsBackground = true;
            m_Thread.SetApartmentState(ApartmentState.STA);
            m_Thread.Start();
        }

        /// <summary>
        /// Sets the message for Splash screen.
        /// </summary>
        /// <param name="statusMessage"></param>
        static public void SetSatusString(string statusMessage)
        {
            m_Status = statusMessage;
        }

        /// <summary>
        /// A private entry point for the thread.
        /// </summary>
        static private void ShowForm()
        {
            m_frm = new ImportSummary();
            Application.Run(m_frm);
        }

        /// <summary>
        /// Close the Splash screen
        /// </summary>
        static public void CloseForm()
        {
#if DEBUG
           
#else
            {
                m_frm.Close();
            }
#endif
            if (m_frm != null && m_frm.IsDisposed == false)
            {
                // Make it start going away.
            }
            m_Thread = null;	// we don't need these any more.
            m_frm = null;


        }
        #endregion
       

        public static ImportSummary GetInstance()
        {
            m_frm = new ImportSummary();
            return m_frm;
        }
        
        public void ShowImportsummary(string message, bool isEndlessLoop)
        {
            try
            {
                if (this.IsDisposed)
                {
                    m_frm = null;
                }

            }
            catch { }
            try
            {
                //splashScreen = null;
            }
            catch { }

            if (m_frm == null)
            {
                m_frm = new ImportSummary();
            }
            //Assigning message to splash screen.
            this.MessageToShow = message;

            //Assigning loop value to splash.
            this.IsEndLessLoop = isEndlessLoop;
            //if (message.Equals(EDI.Constant.Constants.QBflashmessage.ToString()) || message.Equals(EDI.Constant.Constants.ExportingDataFlashMessage.ToString()))
            //    splashScreen.CancelSplash = true;
            //else
            //    splashScreen.CancelSplash = false;

            try
            {
                this.ShowDialog();

                //splashScreen.Show();
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        //End Axis 10.2 chnages
        public void DisplayImportSummary(List<string> idlist, int SuccesCount, List<bool> idlistshow, int FailureCount)
        {

            string display = string.Empty;
            // string msg = string.Empty;
            string error = string.Empty;
            bool flag = false;
            for (int i = 0; i < idlistshow.Count; i++)
            {
                if (idlistshow[i] == false)
                {
                    display += "\n" + idlist[i] + "\n";
                    flag = false;

                }
                else
                {
                    display += "\n" + idlist[i] + "\n";
                    flag = true;
                }
            }
            // msg = "\n" + SuccesCount + " " + "Record is deleted \n";
            if (flag == false)
            {
                if (SuccesCount == 1)
                {
                    richTextBoxSDKMessage.Text = SuccesCount + " " + "Record deleted " + " \n " + display + " \n ";
                }
                else
                {
                    richTextBoxSDKMessage.Text = SuccesCount + " " + "Records deleted " + " \n " + display + " \n ";
                }
            }
            else
            {
                richTextBoxSDKMessage.Text = FailureCount + " " + "Records not deleted " + " \n " + display + " \n ";
            }

        }
        private void ImportSummary_Load(object sender, EventArgs e)
        {
            this.labelFailMsg.Text = this.m_failMSG;
            //this.labelSuccesMsg.Text = this.m_succesMSG;
            this.labelSkipped.Text = this.m_skipped;
            if (barstatusflag == true || CommonUtilities.GetInstance().barstatusflag1 == true)
            {

                this.progressBarDelet.Visible = true;
                this.lbldeleteheading.Visible = true;
                this.labelDeleting.Visible = true;
             //   this.labelSuccesMsg.Text = CommonUtilities.GetInstance().ImportingRowCount;
                this.richTextBoxSDKMessage.Text = CommonUtilities.GetInstance().succesmg;

                int[] a = CommonUtilities.GetInstance().importrec;
                int aa = CommonUtilities.GetInstance().totalImport_count;

                for (int i = 1; i <= aa ; i++)
                {
                    this.progressBarDelet.Value = (i * 100) / aa;

                   //this.labelSuccesMsg.Text= "Deleting " + i + " of " + aa + " records" ;
                }
            }
            else
            {
                this.progressBarDelet.Visible = false;
                this.lbldeleteheading.Visible = false;
                this.labelDeleting.Visible = false;
                this.richTextBoxSDKMessage.Text = this.m_SDKMsg;
            }

            string path = Application.StartupPath.ToString() + "\\LOG\\ErrorMsg" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    path = path.Replace("Program Files", Constants.xpPath);
                else
                    path = path.Replace("Program Files", "Users\\Public\\Documents"); 
            }
            catch { }
            if (this.labelFailMsg.Text != "0 Errors encountered.")
            {
                try
                {
                    if (this.m_DataTable != null)
                    {
                        StringBuilder errorData = ErrorData();
                        
                        if (File.Exists(path))
                            File.AppendAllText(path, errorData.ToString());
                        else
                            File.WriteAllText(path, errorData.ToString());
                    }
                    else
                    {
                        if (File.Exists(path))
                            File.AppendAllText(path, this.m_SDKMsg);
                        else
                            File.WriteAllText(path, this.m_SDKMsg);
                    }
                }
                catch (Exception ex)
                {
                   // CommonUtilities.WriteErrorLog(ex.Message);
                }
            }
        }


       

        /// <summary>
        /// Get the summary error data in a correct format.
        /// </summary>
        /// <returns></returns>
        public StringBuilder ErrorData()
        {
            StringBuilder stringData = new StringBuilder();
            char[] NewLine = { '\n','\r' };
            string[] items = new string[this.m_DataTable.Columns.Count];
            string[] splitErrorMsg = this.m_SDKMsg.Split(NewLine);

            try
            {
                //First msg heading from the SDK.
                stringData.AppendLine("------------------------------------------------------------------------------------------------");
                stringData.AppendLine(splitErrorMsg[0].ToString() + "\t" + DateTime.Now.ToString());

                for (int index = 3; index < splitErrorMsg.Length; index+=3)
                {
                    if (splitErrorMsg[index].Contains("The Error location is ".ToString()))
                    {
                        stringData.AppendLine();
                        stringData.AppendLine(splitErrorMsg[index].ToString());

                        int dataRowCount = Convert.ToInt32(splitErrorMsg[index].Substring(splitErrorMsg[index].LastIndexOf(":")).Replace(":", "").Trim());
                        for (int colName = 0; this.m_DataTable.Columns.Count > colName; colName++)
                            stringData.Append(this.m_DataTable.Columns[colName].ColumnName + "\t");

                        stringData.AppendLine();

                        //Get the data line form the datatable.
                        for (int temp = 0; this.m_DataTable.Columns.Count > temp; temp++)
                            stringData.Append(this.m_DataTable.Rows[dataRowCount - 1][temp].ToString() + "\t");

                        stringData.AppendLine();
                        stringData.AppendLine(splitErrorMsg[index + 1].ToString());
                        stringData.AppendLine();
                    }
                    else
                    {
                        for (int temp = 2; temp < splitErrorMsg.Length; temp += 2)
                        {
                            stringData.AppendLine();
                            stringData.AppendLine(splitErrorMsg[temp].ToString());
                        }
                        break;
                    }
                }
                stringData.AppendLine("------------------------------------------------------------------------------------------------");
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return stringData;
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            if (this.progressBarDelet.Value == this.progressBarDelet.Maximum)
            {
                this.progressBarDelet.Value = 0;
            }
            this.progressBarDelet.Value += 1;

        }

        private void buttonQuit_Click(object sender, EventArgs e)
        {
            TransactionImporter axisForm = (TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;
            if (axisForm.backgroundWorkerDelet.IsBusy)
           {
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerDelet;
                bkWorker.CancelAsync();
               // bkWorker.Dispose();
               
            }
            this.CloseImportSplash();
        }

       

      
                     
    }
}