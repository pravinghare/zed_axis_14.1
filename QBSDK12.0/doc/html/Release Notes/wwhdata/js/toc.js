function  WWHBookData_AddTOCEntries(P)
{
var A=P.fN("","");
var B=A.fN("Release Note History","1");
B=A.fN("Table of Contents","2");
B=A.fN("A Note About Credit Card Numbers in QB 2008 and Later","3");
B=A.fN("What\u2019s New in SDK 12.0?","4");
var C=B.fN("Overview","4#201997");
C=B.fN("Deprecated Features or Features No Longer Supported","4#213681");
C=B.fN("Do You Integrate With QBMS?","4#213683");
B=A.fN("QuickBooks 2011 Multi-Instance Implications with the SDK","5");
B=A.fN("Request/Field-Level Detail Of New and Changed Features","6");
B=A.fN("New and Changed Numeric Types","7");
B=A.fN("Changes Impacting QBFC Integrations","8");
B=A.fN("Bugs Fixed in This Release","9");
B=A.fN("Known Bugs and Workarounds","10");
B=A.fN("New or Modified Files of Interest","11");
B=A.fN("Installation Instructions","12");
C=B.fN("Installing on a System that Doesn\u2019t Have QuickBooks","12#200603");
B=A.fN("Registering an Application for QuickBooks Online Edition","13");
B=A.fN("Importing the Correct Library","14");
B=A.fN("About Redistributing QBXMLRP2","15");
B=A.fN("Creating Applications that Can Run on 64-Bit Operating Systems","16");
B=A.fN("Documentation","17");
B=A.fN("Tools","18");
B=A.fN("Samples","19");
C=B.fN("New Samples","19#210673");
C=B.fN("Downloading MSXML 6.0","19#174153");
C=B.fN("Building the C++ Samples","19#174156");
C=B.fN("UIandEventTest Samples","19#174159");
C=B.fN("Known Problems in VB Sample Applications","19#174167");
B=A.fN("Using Payroll Reports","20");
B=A.fN("Deprecation Policy","21");
B=A.fN("System Requirements","22");
B=A.fN("QuickBooks Products and qbXML/QBFC Support","23");
B=A.fN("If You Have Questions . . .","24");
}
