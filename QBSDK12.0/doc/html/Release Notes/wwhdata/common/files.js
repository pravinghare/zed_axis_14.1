function  WWHBookData_Files(P)
{
P.fA("ReleaseNotes.1.1.html","ReleaseNotes.1.1.html");
P.fA("Release Note History","ReleaseNotes.1.2.html");
P.fA("Table of Contents","ReleaseNotes.1.3.html");
P.fA("A Note About Credit Card Numbers in QB 2008 and Later","ReleaseNotes.1.4.html");
P.fA("What\u2019s New in SDK 12.0?","ReleaseNotes.1.5.html");
P.fA("QuickBooks 2011 Multi-Instance Implications with the SDK","ReleaseNotes.1.6.html");
P.fA("Request/Field-Level Detail Of New and Changed Features","ReleaseNotes.1.7.html");
P.fA("New and Changed Numeric Types","ReleaseNotes.1.8.html");
P.fA("Changes Impacting QBFC Integrations","ReleaseNotes.1.9.html");
P.fA("Bugs Fixed in This Release","ReleaseNotes.1.10.html");
P.fA("Known Bugs and Workarounds","ReleaseNotes.1.11.html");
P.fA("New or Modified Files of Interest","ReleaseNotes.1.12.html");
P.fA("Installation Instructions","ReleaseNotes.1.13.html");
P.fA("Registering an Application for QuickBooks Online Edition","ReleaseNotes.1.14.html");
P.fA("Importing the Correct Library","ReleaseNotes.1.15.html");
P.fA("About Redistributing QBXMLRP2","ReleaseNotes.1.16.html");
P.fA("Creating Applications that Can Run on 64-Bit Operating Systems","ReleaseNotes.1.17.html");
P.fA("Documentation","ReleaseNotes.1.18.html");
P.fA("Tools","ReleaseNotes.1.19.html");
P.fA("Samples","ReleaseNotes.1.20.html");
P.fA("Using Payroll Reports","ReleaseNotes.1.21.html");
P.fA("Deprecation Policy","ReleaseNotes.1.22.html");
P.fA("System Requirements","ReleaseNotes.1.23.html");
P.fA("QuickBooks Products and qbXML/QBFC Support","ReleaseNotes.1.24.html");
P.fA("If You Have Questions . . .","ReleaseNotes.1.25.html");
}
