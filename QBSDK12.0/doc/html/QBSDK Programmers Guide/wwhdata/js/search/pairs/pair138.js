function FileData_Pairs(x)
{
x.t("included","quickbooks");
x.t("tag","quickbooks");
x.t("refundamount","specifies");
x.t("refundamount","1.00");
x.t("qbfc","public");
x.t("qbfc","listing");
x.t("(psscompleted)","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.txnauthorizationtime.setvalue");
x.t("9f-1131074959","perform");
x.t("9f-1131074959","/txnid");
x.t("resultmessage","status");
x.t("authorizationcode","185pni");
x.t("transactions","don\u2019t");
x.t("transactions","credit");
x.t("transactions","want");
x.t("transactions","link");
x.t("transactions","linking");
x.t("transactions","xml");
x.t("transactions","whose");
x.t("araccountref","credit");
x.t("araccountref","fullname");
x.t("arrefundcreditcardadd","customerref");
x.t("arrefundcreditcardadd","listing:");
x.t("arrefundcreditcardadd","request");
x.t("arrefundcreditcardadd","txndate");
x.t("available","notes");
x.t("notes","want");
x.t("aggregate.","link");
x.t("properties","refund");
x.t("(0)","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.resultmessage.setvalue");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.paymentgroupingcode.setvalue","(4)");
x.t("here.","optional:");
x.t("just","credit");
x.t("building","refund");
x.t("420050223","2005-02-23");
x.t("2008","/expirationyear");
x.t("customerref","required");
x.t("customerref","normally");
x.t("customerref","fullname");
x.t("shown","osr.");
x.t("within","quickbooks.");
x.t("sessionmanager.closeconnection","end");
x.t("memos.","listing");
x.t("5.0","qbxml");
x.t("/paymentmethodref","memo");
x.t("/paymentgroupingcode","paymentstatus");
x.t("1109192233","/txnauthorizationstamp");
x.t("error.","memo");
x.t("error.","adding");
x.t("build","credit");
x.t("pretty","straightforward.");
x.t("fso","new");
x.t("arrefundcreditcardaddrq","requestid");
x.t("required","match");
x.t("required","elements");
x.t("source","account");
x.t("always","refund.");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.creditcardtxntype.setvalue","ccttrefund");
x.t("creditcardtxnresultinfo","sub");
x.t("creditcardtxnresultinfo","resultcode");
x.t("sub","aggregates");
x.t("sub","aggregate");
x.t("sub","qbfc_addarrefundcreditcard");
x.t("sub","adding");
x.t("aggregates","along");
x.t("don\u2019t","fact");
x.t("don\u2019t","checks");
x.t("don\u2019t","exceed");
x.t("microsoft","scripting");
x.t("reconbatchid","420050223");
x.t("credit","transactions");
x.t("credit","memos.");
x.t("credit","card");
x.t("credit","amounts");
x.t("credit","memo");
x.t("credit","you\u2019ll");
x.t("credit","transaction");
x.t("responsemsgset.toxmlstring","fname");
x.t("version=","5.0");
x.t("version=","1.0");
x.t("shows","build");
x.t("shows","simple");
x.t("shows","osr");
x.t("shows","refund");
x.t("shows","remainder");
x.t("last","digits.");
x.t("roestop","add");
x.t("supply","just");
x.t("supply","card");
x.t("supply","aggregate");
x.t("supply","creditcardtxninputinfo");
x.t("supply","values");
x.t("supply","company");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.expirationyear.setvalue","(2008)");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.resultcode.setvalue","(0)");
x.t("customer","target");
x.t("customer","information");
x.t("refnumber","supplied");
x.t("type","credit");
x.t("type","automatically.");
x.t("type","you\u2019ll");
x.t("fact","can\u2019t");
x.t("this.","listing");
x.t("sample","add");
x.t("sample","/memo");
x.t("sample","sessionmanager.beginsession");
x.t("responsemsgset","imsgsetresponse");
x.t("responsemsgset","sessionmanager.dorequests");
x.t("close","session");
x.t("/creditcardtxninputinfo","creditcardtxnresultinfo");
x.t("response","request.");
x.t("response","quickbooks");
x.t("public","sub");
x.t("onerror","stoponerror");
x.t("/authorizationcode","reconbatchid");
x.t("qbmsxml","response");
x.t("qbmsxml","request");
x.t("qbmsxml","request/response");
x.t("qbmsxml","interact");
x.t("qbmsxml","refund");
x.t("qbmsxml","transaction");
x.t("qbmsxml","2.0");
x.t("connection","quickbooks.");
x.t("2005-11-01t00:00:00","/txnauthorizationtime");
x.t("default","account");
x.t("default","undeposited");
x.t("default","accounts");
x.t("to.","figure");
x.t("automatically","supply");
x.t("original","qbms");
x.t("checks","sure");
x.t("partial","refund");
x.t("xml_request.doc","dim");
x.t("card","type");
x.t("card","info");
x.t("card","transactions.");
x.t("card","linked");
x.t("card","number");
x.t("card","data");
x.t("card","information");
x.t("card","refund");
x.t("card","transaction");
x.t("card","used");
x.t("card","transaction.");
x.t("want","save");
x.t("want","link.");
x.t("supplied","automatically");
x.t("supplied","via");
x.t("supplied","data");
x.t("developer\u2019s","guide");
x.t("link","least");
x.t("link","refund");
x.t("link","want.");
x.t("link","two");
x.t("among","transactions");
x.t("new","filesystemobject");
x.t("new","qbsessionmanager");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.expirationmonth.setvalue","(11)");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.nameoncard.setvalue","jack");
x.t("automatically.","specify");
x.t("amounts","you\u2019ll");
x.t("(11)","arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.nameoncard.setvalue");
x.t("/merchantaccountnumber","authorizationcode");
x.t("need","this.");
x.t("need","add");
x.t("listing:","credit");
x.t("info","qbmsxml");
x.t("status","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.creditcardtransid.setvalue");
x.t("status","/resultmessage");
x.t("15.0","pre-beta");
x.t("file","grins.");
x.t("filesystemobject","dim");
x.t("ts.write","(xml)");
x.t("add","request");
x.t("add","request.");
x.t("add","arrefundcreditcard");
x.t("add","optional");
x.t("add","object");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.authorizationcode.setvalue","185pni");
x.t("textstream","fso.createtextfile");
x.t("expirationmonth","/expirationmonth");
x.t("normally","need");
x.t("check","subscribedservices");
x.t("guide","quickbooks");
x.t("exceed","credit");
x.t("hardcoded","dim");
x.t("results","file");
x.t("simple","refund");
x.t("arrefundcreditcardadd.","customerref");
x.t("sure","don\u2019t");
x.t("sure","account");
x.t("qbms","credit");
x.t("qbms","15.0");
x.t("qbms","account");
x.t("qbms","attempts");
x.t("qbms","arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.creditcardnumber.");
x.t("qbms","required.");
x.t("qbms","service.");
x.t("qbms","transaction");
x.t("transactions.","determine");
x.t("resultinfo","sub");
x.t("false","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.txnauthorizationstamp.setvalue");
x.t("/txnauthorizationstamp","/creditcardtxnresultinfo");
x.t("via","customerref");
x.t("paymentmethodref","specifies");
x.t("paymentmethodref","fullname");
x.t("2005-02-23","qbms");
x.t("refundappliedtotxn","arrefundccadd.refundappliedtotxnaddlist.append");
x.t("refundappliedtotxn","irefundappliedtotxnadd");
x.t("requires","microsoft");
x.t("true","ts.write");
x.t("/expirationmonth","expirationyear");
x.t("cardnotpresent","/transactionmode");
x.t("functionality","adding");
x.t("account","here.");
x.t("account","qbmsxml");
x.t("account","asset");
x.t("account","matches");
x.t("account","used.");
x.t("account","refund");
x.t("account","specify");
x.t("refundappliedtotxnadd","aggregate.");
x.t("refundappliedtotxnadd","txnid");
x.t("imsgsetrequest","requestmsgset");
x.t("arrefundccadd.paymentmethodref.fullname.setvalue","visa");
x.t("setvalue","4111111111111111");
x.t("(4)","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.paymentstatus.setvalue");
x.t("obtain","response");
x.t("receivable","account");
x.t("receivable","arrefundccadd.paymentmethodref.fullname.setvalue");
x.t("receivable","/fullname");
x.t("initialize","message");
x.t("williams","arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.creditcardtxntype.setvalue");
x.t("williams","/nameoncard");
x.t("williams","arrefundccadd.refundfromaccountref.fullname.setvalue");
x.t("williams","/fullname");
x.t("aggregate","supply");
x.t("aggregate","qbmsxml");
x.t("aggregate","qbms");
x.t("aggregate","optional");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.resultmessage.setvalue","status");
x.t("perform","request");
x.t("session","connection");
x.t("/qbxmlmsgsrq","/qbxml");
x.t("omit","tag");
x.t("omit","default");
x.t("omit","undeposited");
x.t("determine","sdk");
x.t("sessionmanager.createmsgsetrequest","initialize");
x.t("completed","/paymentstatus");
x.t("/paymentstatus","txnauthorizationtime");
x.t("address","information");
x.t("sdk","simply");
x.t("attempts","connect");
x.t("arrefundccadd","iarrefundcreditcardadd");
x.t("arrefundccadd","requestmsgset.appendarrefundcreditcardaddrq");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.creditcardtransid.setvalue","v54a60275101");
x.t("target","transactions");
x.t("target","credit");
x.t("linked","transactions");
x.t("linked","credit");
x.t("txnid","9f-1131074959");
x.t("txnid","hardcoded");
x.t("txnid","unique");
x.t("txnid","b0-1131081904");
x.t("arrefundccadd.refundappliedtotxnaddlist.append","refundappliedtotxn.refundamount.setvalue");
x.t("scripting","runtime");
x.t("fname","xml_request.doc");
x.t("fname","true");
x.t("fname","string");
x.t("string","dim");
x.t("string","xml");
x.t("6.0","greater");
x.t("2005-11-01","false");
x.t("sessionmanager.endsession","sessionmanager.closeconnection");
x.t("/arrefundcreditcardadd","/arrefundcreditcardaddrq");
x.t("request","qbfc");
x.t("request","credit");
x.t("request","want");
x.t("request","qbms");
x.t("request","obtain");
x.t("request","linked");
x.t("request","6.0");
x.t("request","message");
x.t("request","greater");
x.t("request","qbfc.");
x.t("request","effected");
x.t("request","result");
x.t("request","two");
x.t("request","object");
x.t("preceding","qbmsxml");
x.t("/creditcardnumber","expirationmonth");
x.t("listing","arrefundcreditcardadd");
x.t("listing","arrefundcreditcardadd.");
x.t("listing","arrefundcreditcardadd:");
x.t("listing","21-1");
x.t("listing","21-2");
x.t("listing","21-3");
x.t("listing","happens");
x.t("notice","creditcardtranstype");
x.t("notice","including");
x.t("services.","original");
x.t("digits.","notice");
x.t("dim","fso");
x.t("dim","responsemsgset");
x.t("dim","textstream");
x.t("dim","refundappliedtotxn");
x.t("dim","arrefundccadd");
x.t("dim","fname");
x.t("dim","xml");
x.t("dim","sessionmanager");
x.t("dim","requestmsgset");
x.t("iarrefundcreditcardadd","arrefundccadd");
x.t("ccttrefund","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.resultcode.setvalue");
x.t("paymentstatus","completed");
x.t("request.","paymentmethodref");
x.t("request.","include");
x.t("arrefundcreditcardadd:","figure");
x.t("visa","arrefundccadd.memo.setvalue");
x.t("visa","/fullname");
x.t("1.0","qbxml");
x.t("/refundamount","/refundappliedtotxnadd");
x.t("transactionmode","cardnotpresent");
x.t("asset","account");
x.t("means","supply");
x.t("qbxml","version=");
x.t("qbxml","listing");
x.t("qbxml","qbxmlmsgsrq");
x.t("qbxml","spec");
x.t("type.","refundamount");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.creditcardnumber.","setvalue");
x.t("(1109192233)","link");
x.t("/araccountref","paymentmethodref");
x.t("quickbooks.","sessionmanager.endsession");
x.t("quickbooks.","information");
x.t("number","masked");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.reconbatchid.setvalue","420050223");
x.t("/resultcode","resultmessage");
x.t("requestmsgset.appendarrefundcreditcardaddrq","properties");
x.t("/transactionmode","creditcardtxntype");
x.t("creditcardtransid","v54a60275101");
x.t("specifies","type");
x.t("specifies","linked");
x.t("creditcardtxninputinfo","creditcardtxnresultinfo");
x.t("creditcardtxninputinfo","creditcardnumber");
x.t("against","quickbooks");
x.t("unique","among");
x.t("refunded.","specify");
x.t("idn","add");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.merchantaccountnumber.setvalue","4269281420247209");
x.t("imsgsetresponse","responsemsgset");
x.t("fso.createtextfile","fname");
x.t("bank","account");
x.t("linking","to.");
x.t("qbfc_addarrefundcreditcard","dim");
x.t("interaction","qbms.");
x.t("21-1","shows");
x.t("21-1","osr");
x.t("21-1","constructing");
x.t("match","customer");
x.t("made.","typically");
x.t("xml","responsemsgset.toxmlstring");
x.t("xml","version=");
x.t("xml","string");
x.t("qbxmlmsgsrq","onerror");
x.t("/memo","refundappliedtotxnadd");
x.t("/memo","creditcardtxninfo");
x.t("creditcardtxntype","refund");
x.t("/reconbatchid","paymentgroupingcode");
x.t("/txnauthorizationtime","txnauthorizationstamp");
x.t("memo","transactions");
x.t("memo","available");
x.t("memo","partial");
x.t("memo","using");
x.t("you\u2019ll","need");
x.t("you\u2019ll","runtime");
x.t("21-2","arrefundcreditcardadd");
x.t("21-2","shows");
x.t("simply","company");
x.t("(1)","refundappliedtotxn.txnid.setvalue");
x.t("requestid","arrefundcreditcardadd");
x.t("txnauthorizationtime","2005-11-01t00:00:00");
x.t("non-credit","card");
x.t("interactions","qbms");
x.t("required.","code");
x.t("code","building");
x.t("sessionmanager","qbsessionmanager");
x.t("end","sub");
x.t("/arrefundcreditcardaddrq","/qbxmlmsgsrq");
x.t("21-3","arrefundcreditcardadd");
x.t("21-3","shows");
x.t("osr","listing");
x.t("optional.","omit");
x.t("level","salesreceiptadd");
x.t("fullname","visa");
x.t("fullname","jack");
x.t("fullname","undeposited");
x.t("fullname","accounts");
x.t("matches","araccountref");
x.t("least","transactions");
x.t("message","request");
x.t("message","request's");
x.t("stoponerror","arrefundcreditcardaddrq");
x.t("/creditcardtxnresultinfo","/creditcardtxninfo");
x.t("greater","credit");
x.t("greater","qbxml");
x.t("v54a60275101","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.merchantaccountnumber.setvalue");
x.t("v54a60275101","/creditcardtransid");
x.t("whose","txnid");
x.t("(xml)","close");
x.t("resultcode","/resultcode");
x.t("used.","sure");
x.t("used.","aracountref");
x.t("creditcardtxninfo","aggregate");
x.t("creditcardtxninfo","creditcardtxninputinfo");
x.t("service.","notice");
x.t("service.","inputinfo");
x.t("constructing","credit");
x.t("request's","attributes");
x.t("1.00","/refundamount");
x.t("request/response","interaction");
x.t("/nameoncard","transactionmode");
x.t("funds","default");
x.t("funds","account");
x.t("funds","/fullname");
x.t("funds","arrefundccadd.araccountref.fullname.setvalue");
x.t("inputinfo","subaggregate");
x.t("include","credit");
x.t("include","data");
x.t("amount","greater");
x.t("requestmsgset","imsgsetrequest");
x.t("requestmsgset","sessionmanager.createmsgsetrequest");
x.t("arrefundccadd.customerref.fullname.setvalue","jack");
x.t("refundappliedtotxn.txnid.setvalue","9f-1131074959");
x.t("refundappliedtotxn.txnid.setvalue","b0-1131081904");
x.t("salesreceiptadd","request");
x.t("qbms.","link");
x.t("qbms.","listing");
x.t("(requestmsgset)","save");
x.t("/creditcardtransid","merchantaccountnumber");
x.t("figure","21-1");
x.t("figure","21-2");
x.t("aracountref","optional.");
x.t("subscribedservices","aggregate");
x.t("qbfc.","listing");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.txnauthorizationstamp.setvalue","(1109192233)");
x.t("creditcardnumber","4111111111111111");
x.t("originating","qbmsxml");
x.t("setsessionmanager","new");
x.t("arrefundccadd.refundfromaccountref.fullname.setvalue","undeposited");
x.t("txndate","refnumber");
x.t("valid","qbms");
x.t("arrefundcreditcard","sample");
x.t("sessionmanager.dorequests","(requestmsgset)");
x.t("/refundappliedtotxnadd","refundappliedtotxnadd");
x.t("/refundappliedtotxnadd","/arrefundcreditcardadd");
x.t("/creditcardtxntype","/creditcardtxninputinfo");
x.t("paymentgroupingcode","/paymentgroupingcode");
x.t("along","required");
x.t("arrefundccadd.memo.setvalue","partial");
x.t("information","supplied");
x.t("information","developer\u2019s");
x.t("information","omit");
x.t("information","contained");
x.t("data","included");
x.t("data","within");
x.t("data","qbmsxml");
x.t("data","supplied");
x.t("data","qbms");
x.t("data","request");
x.t("data","preceding");
x.t("data","xml");
x.t("data","originating");
x.t("data","data");
x.t("optional","credit");
x.t("optional","supply");
x.t("refund.","means");
x.t("interact","qbms");
x.t("query","check");
x.t("effected","credit");
x.t("sessionmanager.beginsession","omdontcare");
x.t("irefundappliedtotxnadd","refundappliedtotxn");
x.t("save","results");
x.t("save","qbms");
x.t("save","transaction.");
x.t("creditcardtranstype","always");
x.t("spec","level");
x.t("result","interactions");
x.t("happens","include");
x.t("refund","qbfc");
x.t("refund","pretty");
x.t("refund","sample");
x.t("refund","add");
x.t("refund","functionality");
x.t("refund","target");
x.t("refund","request");
x.t("refund","qbxml");
x.t("refund","made.");
x.t("refund","/creditcardtxntype");
x.t("refund","transaction");
x.t("refund","transaction.");
x.t("link.","refundfromaccountref");
x.t("optional:","omit");
x.t("values","want.");
x.t("want.","address");
x.t("want.","txnid");
x.t("runtime","error.");
x.t("runtime","dim");
x.t("pre-beta","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.paymentgroupingcode.setvalue");
x.t("pre-beta","/reconbatchid");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.txnauthorizationtime.setvalue","2005-11-01");
x.t("expirationyear","2008");
x.t("nameoncard","jack");
x.t("merchantaccountnumber","4269281420247209");
x.t("subaggregate","data");
x.t("jack","williams");
x.t("grins.","requires");
x.t("transaction","qbmsxml");
x.t("transaction","qbms");
x.t("transaction","type.");
x.t("transaction","against");
x.t("transaction","refunded.");
x.t("transaction","figure");
x.t("transaction","data");
x.t("transaction","using");
x.t("transaction","adding");
x.t("osr.","supply");
x.t("qbsessionmanager","setsessionmanager");
x.t("qbsessionmanager","sessionmanager.openconnection");
x.t("4269281420247209","/merchantaccountnumber");
x.t("4269281420247209","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.authorizationcode.setvalue");
x.t("two","transactions");
x.t("two","linked");
x.t("/customerref","refundfromaccountref");
x.t("/txnid","refundamount");
x.t("/creditcardtxninfo","refundappliedtotxnadd");
x.t("typically","default");
x.t("quickbooks","supply");
x.t("quickbooks","customer");
x.t("quickbooks","dim");
x.t("quickbooks","merchant");
x.t("4111111111111111","arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.expirationyear.setvalue");
x.t("4111111111111111","/creditcardnumber");
x.t("b0-1131081904","refundappliedtotxn");
x.t("b0-1131081904","/txnid");
x.t("/fullname","/paymentmethodref");
x.t("/fullname","/araccountref");
x.t("/fullname","/customerref");
x.t("/fullname","/refundfromaccountref");
x.t("2.0","request");
x.t("txnauthorizationstamp","1109192233");
x.t("used","refund");
x.t("contained","creditcardtxninfo");
x.t("elements","shown");
x.t("masked","except");
x.t("except","last");
x.t("using","credit");
x.t("using","refundappliedtotxnadd");
x.t("connect","qbms.");
x.t("requestmsgset.attributes.onerror","roestop");
x.t("arrefundccadd.araccountref.fullname.setvalue","accounts");
x.t("adding","credit");
x.t("remainder","osr");
x.t("company","valid");
x.t("company","query");
x.t("(2008)","arrefundccadd.creditcardtxninfo.creditcardtxninputinfo.expirationmonth.setvalue");
x.t("/qbxml","listing");
x.t("/expirationyear","nameoncard");
x.t("/resultmessage","creditcardtransid");
x.t("undeposited","funds");
x.t("accounts","receivable");
x.t("transaction.","credit");
x.t("transaction.","resultinfo");
x.t("transaction.","figure");
x.t("omdontcare","dim");
x.t("including","qbms");
x.t("straightforward.","don\u2019t");
x.t("185pni","/authorizationcode");
x.t("185pni","arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.reconbatchid.setvalue");
x.t("arrefundccadd.creditcardtxninfo.creditcardtxnresultinfo.paymentstatus.setvalue","(psscompleted)");
x.t("refundappliedtotxn.refundamount.setvalue","(1)");
x.t("/refundfromaccountref","araccountref");
x.t("refundfromaccountref","source");
x.t("refundfromaccountref","fullname");
x.t("merchant","services.");
x.t("merchant","service.");
x.t("specify","bank");
x.t("specify","non-credit");
x.t("specify","amount");
x.t("specify","transaction");
x.t("can\u2019t","specify");
x.t("sessionmanager.openconnection","idn");
x.t("attributes","requestmsgset.attributes.onerror");
x.t("object","dim");
x.t("object","arrefundccadd.customerref.fullname.setvalue");
}
