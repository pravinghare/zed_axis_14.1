function FileData_Pairs(x)
{
x.t("example","create");
x.t("looks","this:");
x.t("transactions","printed");
x.t("available","various");
x.t("off:","won\u2019t");
x.t("columns","display/print");
x.t("columns","tab");
x.t("shown","create");
x.t("needs","return");
x.t("create","invoice");
x.t("create","custom");
x.t("create","invoices");
x.t("queries.","stop");
x.t("queries.","end");
x.t("currently","effect");
x.t("built-in","other1");
x.t("various","templates");
x.t("doing","templates");
x.t("user","check");
x.t("user","edit");
x.t("user","makes");
x.t("user","chooses");
x.t("user","next");
x.t("user","opens");
x.t("user","won\u2019t");
x.t("user","finished");
x.t("user","turn");
x.t("user","warned");
x.t("user","do.");
x.t("current","selection");
x.t("uses","layout");
x.t("uses","display");
x.t("shows","template");
x.t("note:","doing");
x.t("field","named");
x.t("field","ours");
x.t("tell","user");
x.t("look","figure");
x.t("modern","template.");
x.t("basic","customization");
x.t("customer","built-");
x.t("form","current");
x.t("form","layout");
x.t("form","header");
x.t("form","displayed");
x.t("form","template");
x.t("form","it.");
x.t("form","transaction");
x.t("back","queries.");
x.t("default","custom");
x.t("product","invoice");
x.t("customization","form");
x.t("customization","button");
x.t("turned","off:");
x.t("items","user");
x.t("new","custom");
x.t("new","headers");
x.t("default.","means");
x.t("able","custom");
x.t("able","write");
x.t("need","tell");
x.t("need","investigate");
x.t("need","quickbooks");
x.t("lets","look");
x.t("list","create");
x.t("list","selecting");
x.t("again","changing");
x.t("built-","custom");
x.t("add","new");
x.t("directly.","user");
x.t("check","items");
x.t("check","checkboxes");
x.t("desired.","again");
x.t("investigate","advance");
x.t("checkboxes","custom");
x.t("checkboxes","checked");
x.t("via","sdk");
x.t("customizations","may");
x.t("invoice","modern");
x.t("invoice","form");
x.t("invoice","forms");
x.t("fields.","we\u2019ve");
x.t("stop","able");
x.t("edit","templates");
x.t("edit","turn");
x.t("this:","form");
x.t("layout","editor");
x.t("layout","impact");
x.t("custom","field");
x.t("custom","fields.");
x.t("custom","fields");
x.t("printable.","need");
x.t("print.","user");
x.t("inherited","customer");
x.t("inherited","item");
x.t("indeed","available");
x.t("continue","additional");
x.t("makes","custom");
x.t("sdk","indeed");
x.t("selection","product");
x.t("selection","list");
x.t("double-click","template");
x.t("fields","user");
x.t("fields","need");
x.t("fields","inherited");
x.t("fields","write");
x.t("fields","display");
x.t("fields","private");
x.t("fields","expected");
x.t("fields","added");
x.t("fields","won\u2019t");
x.t("fields","visible/printable");
x.t("fields","show");
x.t("fields","data");
x.t("fields","item");
x.t("write","data");
x.t("we\u2019ve","added");
x.t("we\u2019ve","already");
x.t("lists-","templates");
x.t("chooses","continue");
x.t("other.","columns");
x.t("intuit","pre-printed");
x.t("display","print.");
x.t("display","print");
x.t("display","transaction");
x.t("template.","figure");
x.t("notice","screen");
x.t("display/print","desired.");
x.t("other1","other2");
x.t("next","needs");
x.t("button","bottom");
x.t("other2","custom");
x.t("means","two");
x.t("headers","and/or");
x.t("quickbooks.","transaction");
x.t("cause","transaction");
x.t("pre-printed","invoice");
x.t("pre-printed","forms.");
x.t("advance","tell");
x.t("private","data");
x.t("templates","turned");
x.t("templates","list");
x.t("templates","directly.");
x.t("templates","main");
x.t("templates","result");
x.t("may","cause");
x.t("editor","add");
x.t("editor","position");
x.t("opens","templates");
x.t("expected","quickbooks");
x.t("added","via");
x.t("added","two");
x.t("you\u2019ll","need");
x.t("impact","intuit");
x.t("ext:","using");
x.t("end","user");
x.t("return","basic");
x.t("won\u2019t","able");
x.t("won\u2019t","print.");
x.t("won\u2019t","visible");
x.t("issue","you\u2019ll");
x.t("header","tab");
x.t("printed","quickbooks.");
x.t("click","additional");
x.t("form.","note:");
x.t("finished","fields");
x.t("on.","lets");
x.t("displayed","looks");
x.t("and/or","columns");
x.t("changing","layout");
x.t("dropdown","selection");
x.t("main","menubar.");
x.t("additional","customization");
x.t("named","custom");
x.t("named","other.");
x.t("ours","notice");
x.t("already","shown");
x.t("already","described");
x.t("figure","13-6");
x.t("selecting","lists-");
x.t("visible/printable","we\u2019ve");
x.t("making","custom");
x.t("forms","example");
x.t("forms","transactions");
x.t("forms","issue");
x.t("template","currently");
x.t("template","edit");
x.t("template","dropdown");
x.t("template","quickbooks");
x.t("template","used");
x.t("screen","print");
x.t("show","quickbooks");
x.t("data","back");
x.t("data","fields");
x.t("data","ext:");
x.t("data","making");
x.t("it.","default");
x.t("it.","click");
x.t("result","user");
x.t("print","checkboxes");
x.t("print","we\u2019ve");
x.t("print","expected.");
x.t("print","making");
x.t("print","data");
x.t("print","it.");
x.t("print","improperly");
x.t("described","uses");
x.t("transaction","forms");
x.t("transaction","template");
x.t("transaction","print");
x.t("transaction","bring");
x.t("two","custom");
x.t("quickbooks","user");
x.t("quickbooks","won\u2019t");
x.t("quickbooks","print");
x.t("quickbooks","transaction");
x.t("quickbooks","itself");
x.t("itself","uses");
x.t("turn","custom");
x.t("turn","on.");
x.t("used","transaction");
x.t("bottom","form.");
x.t("warned","customizations");
x.t("forms.","user");
x.t("using","custom");
x.t("bring","basic");
x.t("tab","custom");
x.t("invoices","form");
x.t("menubar.","double-click");
x.t("effect","invoice");
x.t("do.","user");
x.t("position","new");
x.t("visible","printable.");
x.t("visible","quickbooks");
x.t("13-6","shows");
x.t("13-6","template");
x.t("improperly","intuit");
x.t("item","built-in");
x.t("item","named");
x.t("checked","default.");
}
