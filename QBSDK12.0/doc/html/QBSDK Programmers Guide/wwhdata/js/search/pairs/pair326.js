function FileData_Pairs(x)
{
x.t("example","visual");
x.t("method","reference");
x.t("build","product");
x.t("supported.","example");
x.t("qbxmlrp2lib.requestprocessor2","dim");
x.t("retval","short");
x.t("basic","dim");
x.t("qbxmlrp","qbxmlrp2lib.requestprocessor2");
x.t("product","software.");
x.t("new","feature");
x.t("certain","feature");
x.t("short","preleasenumber");
x.t("need","check");
x.t("processor.","parameters");
x.t("processor.","usage");
x.t("check","release");
x.t("parameters","preleasenumber");
x.t("determine","certain");
x.t("hresult","releasenumber");
x.t("fix","new");
x.t("request","processor.");
x.t("preleasenumber","pointer");
x.t("preleasenumber","returns");
x.t("dim","qbxmlrp");
x.t("dim","ireleasenumber");
x.t("qbxml","request");
x.t("qbxml","requestprocessor");
x.t("feature","supported.");
x.t("feature","reflected");
x.t("number","determine");
x.t("number","qbxml");
x.t("number","identifies");
x.t("number","varies");
x.t("bug","fix");
x.t("reflected","release");
x.t("may","need");
x.t("requestprocessor","method");
x.t("release","qbxml");
x.t("release","number");
x.t("release","number.");
x.t("reference","releasenumber");
x.t("identifies","release");
x.t("ireleasenumber","qbxmlrp.releasenumber");
x.t("ireleasenumber","integer");
x.t("visual","basic");
x.t("usage","release");
x.t("releasenumber","retval");
x.t("releasenumber","hresult");
x.t("releasenumber","qbxml");
x.t("releasenumber","releasenumber");
x.t("major","bug");
x.t("number.","application");
x.t("application","may");
x.t("varies","build");
x.t("integer","ireleasenumber");
x.t("pointer","number");
x.t("pointer","pointer");
x.t("returns","release");
x.t("software.","major");
}
