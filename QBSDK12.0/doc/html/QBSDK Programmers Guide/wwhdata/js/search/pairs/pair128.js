function FileData_Pairs(x)
{
x.t("example","user");
x.t("transactions","split");
x.t("transactions","instead");
x.t("available","enterprise");
x.t("building","assemblies");
x.t("simultaneously","users.");
x.t("bills","order.");
x.t("bills","link");
x.t("bills","post");
x.t("purchaseorder","invoice");
x.t("create","problem");
x.t("controlled","preference");
x.t("order.","new");
x.t("itemreceipt","1/1/2011");
x.t("itemreceipt","1/1/2011.");
x.t("itemreceipt","targets");
x.t("itemreceipt","cost");
x.t("itemreceipt","bill");
x.t("itemreceipt","bill.");
x.t("current","existing");
x.t("current","transaction");
x.t("user","writes");
x.t("posted","itemreceipts");
x.t("1/1/2011","receiving");
x.t("introduced","wash");
x.t("inventory","asset/");
x.t("inventory","requirements");
x.t("inventory","offset");
x.t("type","allow");
x.t("hand","building");
x.t("hand","average");
x.t("costing","bills");
x.t("identify","itemreceipt");
x.t("received.","current");
x.t("to.","itemreceipt");
x.t("new","inventory");
x.t("new","link");
x.t("new","fields");
x.t("new","two");
x.t("link","type");
x.t("link","to.");
x.t("link","types");
x.t("asset/","income/expense");
x.t("enterprise","itemreceipt");
x.t("enterprise","linking");
x.t("enterprise","users");
x.t("check","box");
x.t("transactions.","purchase");
x.t("requirements","users");
x.t("invoice","sales");
x.t("solve","inventory");
x.t("2/1/2011","bill");
x.t("2/1/2011","replace");
x.t("box","identify");
x.t("account","introduced");
x.t("account","payable");
x.t("account","calculating");
x.t("1/1/2011.","create");
x.t("1/1/2011.","feature");
x.t("preference","allow");
x.t("brought","separately");
x.t("average","costing.");
x.t("average","costing");
x.t("sales","order");
x.t("assemblies","depend");
x.t("design.","itemreceipt");
x.t("src","targets");
x.t("src","target");
x.t("continue","transaction");
x.t("allows","itemreceipt");
x.t("writes","itemreceipt");
x.t("targets","bills");
x.t("targets","inventory");
x.t("targets","contain");
x.t("targets","item");
x.t("post","against");
x.t("allow","linking");
x.t("allow","users");
x.t("fields","hold");
x.t("target","inventory");
x.t("receiving","inventories");
x.t("contain","average");
x.t("contain","two");
x.t("separately","edited");
x.t("payable","src");
x.t("purchase","order");
x.t("hold","information");
x.t("calculating","quantity");
x.t("split","design.");
x.t("split","feature");
x.t("split","option");
x.t("split","itemreceipt.");
x.t("feature","available");
x.t("feature","allows");
x.t("feature","created");
x.t("depend","receiving");
x.t("allowed","edited");
x.t("cost","longer");
x.t("design","bill");
x.t("design","transaction");
x.t("design","completely");
x.t("enough","quantity");
x.t("bill","transactions");
x.t("bill","received.");
x.t("bill","2/1/2011");
x.t("bill","brought");
x.t("bill","split");
x.t("bill","bill");
x.t("bill","separate");
x.t("bill","two");
x.t("bill","reverse");
x.t("share","check");
x.t("share","itemreceipt/bill");
x.t("edited","simultaneously");
x.t("edited","users");
x.t("against","inventory");
x.t("against","account");
x.t("linking","itemreceipt");
x.t("linking","purchase");
x.t("linking","itemreceipt/bill");
x.t("instead","transaction");
x.t("replace","itemreceipt");
x.t("types","introduced.");
x.t("switch","new");
x.t("dist","targets");
x.t("option","quickbooks");
x.t("income/expense","dist");
x.t("existing","design");
x.t("longer","allowed");
x.t("longer","share");
x.t("pick","itemreceipt");
x.t("states.","itemreceipt/bill");
x.t("itemreceipt.","new");
x.t("separate","transactions.");
x.t("information","bills");
x.t("monies","posted");
x.t("itemreceipt/bill","purchaseorder");
x.t("itemreceipt/bill","split");
x.t("itemreceipt/bill","states.");
x.t("problem","enough");
x.t("two","transactions");
x.t("two","new");
x.t("two","transaction");
x.t("transaction","split");
x.t("transaction","design");
x.t("transaction","share");
x.t("transaction","solve.");
x.t("users","controlled");
x.t("users","current");
x.t("users","continue");
x.t("users","contain");
x.t("inventories","2/1/2011");
x.t("inventories","1/1/2011.");
x.t("itemreceipts","post");
x.t("itemreceipts","bills.");
x.t("history","pick");
x.t("quickbooks","enterprise");
x.t("solve.","example");
x.t("offset","account");
x.t("offset","src");
x.t("offset","dist");
x.t("order","itemreceipt");
x.t("order","targets");
x.t("order","bill");
x.t("created","solve");
x.t("reverse","itemreceipt");
x.t("bills.","itemreceipts");
x.t("introduced.","new");
x.t("wash","monies");
x.t("users.","longer");
x.t("bill.","two");
x.t("completely","switch");
x.t("quantity","hand");
x.t("item","history");
}
