function FileData_Pairs(x)
{
x.t("objects:","billpaymentcheckadd");
x.t("equal","greater");
x.t("equal","amount");
x.t("example","shown");
x.t("example","total");
x.t("example","results");
x.t("example","billtopayquery");
x.t("example","amount");
x.t("example","following");
x.t("/billtopayqueryrq","listing");
x.t("bill1","$100");
x.t("transactions","bill");
x.t("transactions","handling");
x.t("method","user");
x.t("method","two");
x.t("discount","example");
x.t("discount","discount");
x.t("discount","bills");
x.t("discount","$10");
x.t("discount","bill");
x.t("discount","types");
x.t("discount","amount");
x.t("discount","$45");
x.t("combined","total");
x.t("combined","credits");
x.t("bills","example");
x.t("bills","multiple");
x.t("bills","vendor.");
x.t("bills","window");
x.t("bills","bill");
x.t("bills","billtxnlist.");
x.t("bills","example.");
x.t("bills","credit.");
x.t("bills","credits");
x.t("bills","vendors.");
x.t("bills","generates");
x.t("payeeentityref","fullname");
x.t("2002-08-13","/txndate");
x.t("/billtopayqueryrs","/qbxmlmsgsrs");
x.t("/appliedamount","/setcredit");
x.t("$60","discount");
x.t("version","payment");
x.t("applied","bill1");
x.t("applied","multiple");
x.t("applied","particular");
x.t("applied","bill");
x.t("shown","listing");
x.t("full","appliedtotxnret.");
x.t("currently","supported");
x.t("paying","bill");
x.t("paying","closing");
x.t("paying","two");
x.t("paying","general");
x.t("txntype","vendorcredit");
x.t("txntype","bill");
x.t("error.","discount");
x.t("time.","sdk");
x.t("always","applied");
x.t("august","2002.");
x.t("2002-08-16","/duedate");
x.t("created.","credit");
x.t("created.","return");
x.t("fe-1029260914","/txnid");
x.t("1250.00","/amountdue");
x.t("apply","credit");
x.t("apply","$20");
x.t("apply","credits");
x.t("few","examples");
x.t("80.00","/paymentamount");
x.t("credit","discount");
x.t("credit","$60");
x.t("credit","applied");
x.t("credit","apply");
x.t("credit","credit");
x.t("credit","$10");
x.t("credit","payment");
x.t("credit","appears");
x.t("credit","bill");
x.t("credit","amount");
x.t("credit","result");
x.t("credit","card.");
x.t("credit","transaction");
x.t("credit","created");
x.t("user","select");
x.t("user","selects.");
x.t("appliedamount","20.00");
x.t("appliedamount","60.00");
x.t("appliedamount","10.00");
x.t("shows","example");
x.t("shows","open");
x.t("shows","appliedtotxnadd");
x.t("due.","case");
x.t("/listid","fullname");
x.t("overpaying","bill");
x.t("/payeeentityref","apaccountref");
x.t("object.","returned");
x.t("billpaymentcreditcardret","billpaymentcheckret");
x.t("$10","discount");
x.t("$10","credit");
x.t("$10","applies");
x.t("$10","bill");
x.t("$10","bill.");
x.t("payments","credits");
x.t("open","bills");
x.t("type","bill");
x.t("amount.","billtopayquery");
x.t("allison","bmw");
x.t("bmw","august");
x.t("bmw","/fullname");
x.t("refnumber","0300");
x.t("refnumber","0125");
x.t("refnumber","0126");
x.t("response","shown");
x.t("response","billtopayqueryrs");
x.t("response","message");
x.t("/duedate","/billtopayqueryrq");
x.t("/duedate","amountdue");
x.t("0300","/refnumber");
x.t("/credittoapply","/billtopayret");
x.t("$100:","payment");
x.t("billpaymentcheckaddrq","appliedtotxnadd");
x.t("multiple","bills");
x.t("multiple","vendors");
x.t("lean","version");
x.t("lean","appliedtotxnret");
x.t("total","payment");
x.t("total","cannot");
x.t("total","amount");
x.t("$80","credit");
x.t("$80","bill");
x.t("2002-07-26","/txndate");
x.t("amountdue","1250.00");
x.t("amountdue","152.00");
x.t("objects","billpaymentcheckaddrq");
x.t("objects","results");
x.t("objects","returned");
x.t("apaccountref","listid");
x.t("apaccountref","fullname");
x.t("appliedtotxnret","object.");
x.t("appliedtotxnret","objects");
x.t("setcredit","credittxnid");
x.t("julyrebate","/credittxnid");
x.t("vendor.","payment");
x.t("list","open");
x.t("info","statusmessage=");
x.t("status","code");
x.t("status","billtopayret");
x.t("credittoapply","txnid");
x.t("error:","attempting");
x.t("billtopayqueryrq","requestid");
x.t("123456","payeeentityref");
x.t("123456","statuscode=");
x.t("vendorcredit","/txntype");
x.t("scenarios.","example:");
x.t("sum","payments");
x.t("check","credit");
x.t("/billtopayret","/billtopayqueryrs");
x.t("/billtopayret","billtopayret");
x.t("results","error.");
x.t("results","error");
x.t("entry","created");
x.t("exceed","total");
x.t("exceed","bill");
x.t("exceed","amount");
x.t("due","$100:");
x.t("due","$50.");
x.t("due","bill");
x.t("due","bill.");
x.t("/discountaccountref","/appliedtotxnadd");
x.t("applies","credit");
x.t("applies","following");
x.t("depends","payment");
x.t("available.","example:");
x.t("billtopayqueryrs","requestid=");
x.t("/txndate","refnumber");
x.t("journal","entry");
x.t("45.00","/discountamount");
x.t("helper","query");
x.t("billtopay","txnid");
x.t("vendors","pay");
x.t("obtain","transaction");
x.t("creating","credit");
x.t("billtopayquery","response");
x.t("billtopayquery","obtain");
x.t("billtopayquery","request");
x.t("billtopayquery","listing");
x.t("exactly","equal");
x.t("aggregate","transaction.");
x.t("perform","additional");
x.t("window","quickbooks");
x.t("window","list.");
x.t("(b1)","$100.");
x.t("discounts","applied");
x.t("discounts","exceed");
x.t("discounts","exactly");
x.t("$110.","$10");
x.t("pays","bill");
x.t("appliedtotxnret.","returned");
x.t("find","bill");
x.t("sdk","pay");
x.t("2002.","corresponding");
x.t("/qbxmlmsgsrs","/qbxml");
x.t("txnid","fe-1029260914");
x.t("txnid","101-1029260941");
x.t("txnid","104-1029260962");
x.t("txnid","/txnid");
x.t("attempt","discount");
x.t("addition","bill");
x.t("request","bills");
x.t("request","billtopayqueryrq");
x.t("101-1029260941","/txnid");
x.t("creditremaining","125.00");
x.t("$50","credit");
x.t("attempting","apply");
x.t("payment","objects:");
x.t("payment","transactions");
x.t("payment","method");
x.t("payment","combined");
x.t("payment","applied");
x.t("payment","always");
x.t("payment","credit");
x.t("payment","amount.");
x.t("payment","$80");
x.t("payment","scenarios.");
x.t("payment","$50");
x.t("payment","bill");
x.t("payment","made");
x.t("payment","deposit");
x.t("payment","amount");
x.t("payment","examples");
x.t("payment","transaction");
x.t("payment","either");
x.t("payment","links");
x.t("payment","object");
x.t("supported","sdk.");
x.t("listing","18-10");
x.t("listing","18-5");
x.t("listing","18-6");
x.t("listing","18-7");
x.t("listing","18-8");
x.t("listing","18-9");
x.t("payable","/fullname");
x.t("error","example");
x.t("error","combined");
x.t("queries","links");
x.t("104-1029260962","/txnid");
x.t("suppose","pay");
x.t("suppose","specify");
x.t("$50.","note");
x.t("select","multiple");
x.t("billpaymentcheckadd","billpaymentcreditcardadd");
x.t("appears","credits");
x.t("/creditremaining","/credittoapply");
x.t("20.00","/appliedamount");
x.t("listid","280000-1026788471");
x.t("/credittxnid","appliedamount");
x.t("/credittxnid","/setcredit");
x.t("banking","currently");
x.t("bill","discount");
x.t("bill","shown");
x.t("bill","full");
x.t("bill","apply");
x.t("bill","credit");
x.t("bill","lean");
x.t("bill","perform");
x.t("bill","(b1)");
x.t("bill","$110.");
x.t("bill","find");
x.t("bill","payment");
x.t("bill","listing");
x.t("bill","bill");
x.t("bill","cannot");
x.t("bill","closes");
x.t("bill","returned");
x.t("bill","/txntype");
x.t("bill","amount");
x.t("bill","appliedtotxnadd");
x.t("bill","using");
x.t("bill","without");
x.t("receive","payment");
x.t("particular","transaction.");
x.t("id.","query");
x.t("billtxnlist.","attempt");
x.t("cannot","exceed");
x.t("different","bill");
x.t("18-10","paying");
x.t("18-10","applies");
x.t("60.00","/appliedamount");
x.t("handling","receive");
x.t("types","objects");
x.t("types","bill");
x.t("returned","response");
x.t("returned","object");
x.t("closes","sum");
x.t("discounts.","pay");
x.t("requestid","123456");
x.t("/billpaymentcheckaddrq","two");
x.t("/txntype","apaccountref");
x.t("code","error:");
x.t("fullname","allison");
x.t("fullname","/fullname");
x.t("fullname","accounts");
x.t("fullname","goodcustomerdiscount");
x.t("duedate","2002-08-16");
x.t("duedate","2002-07-17");
x.t("0125","/refnumber");
x.t("return","object");
x.t("overpayment.","listing");
x.t("0126","/refnumber");
x.t("2002-07-17","/duedate");
x.t("message","bill");
x.t("greater","amount");
x.t("billpaymentcreditcardadd","type");
x.t("less","equal");
x.t("closing","bill");
x.t("made","bill");
x.t("deposit","transactions");
x.t("amount","due.");
x.t("amount","due");
x.t("amount","payment");
x.t("amount","less");
x.t("amount","appliedtotxnadd");
x.t("amount","overpayment");
x.t("amount","bill.");
x.t("includelinkedtxns","flag.");
x.t("selects.","paying");
x.t("additional","queries");
x.t("pay","bills");
x.t("pay","time.");
x.t("pay","multiple");
x.t("pay","$80");
x.t("pay","bill");
x.t("/apaccountref","duedate");
x.t("/apaccountref","txndate");
x.t("credittxnid","julyrebate");
x.t("credittxnid","/credittxnid");
x.t("$20","credit");
x.t("example.","payment");
x.t("280000-1026788471","/listid");
x.t("actually","created.");
x.t("10.00","/appliedamount");
x.t("10.00","/discountamount");
x.t("sdk.","addition");
x.t("statuscode=","statusseverity=");
x.t("txndate","2002-08-13");
x.t("txndate","2002-07-26");
x.t("online","banking");
x.t("appliedtotxnadd","apply");
x.t("appliedtotxnadd","aggregate");
x.t("appliedtotxnadd","txnid");
x.t("appliedtotxnadd","setting");
x.t("appliedtotxnadd","specify");
x.t("credit.","listing");
x.t("overpayment","example");
x.t("overpayment","created.");
x.t("overpayment","credit");
x.t("query","pay");
x.t("query","returns");
x.t("information","two");
x.t("152.00","/amountdue");
x.t("billtopayret","credittoapply");
x.t("billtopayret","billtopay");
x.t("examples","few");
x.t("examples","different");
x.t("result","overpaying");
x.t("result","overpayment.");
x.t("result","quickbooks");
x.t("50.00","/paymentamount");
x.t("card.","online");
x.t("case","lean");
x.t("case","overpayment");
x.t("/appliedtotxnadd","/billpaymentcheckaddrq");
x.t("/appliedtotxnadd","appliedtotxnadd");
x.t("/appliedtotxnadd","result");
x.t("/appliedtotxnadd","example:");
x.t("/refnumber","creditremaining");
x.t("/refnumber","duedate");
x.t("/paymentamount","setcredit");
x.t("discountamount","45.00");
x.t("discountamount","10.00");
x.t("transaction","payment");
x.t("transaction","receive");
x.t("transaction","id.");
x.t("transaction","return");
x.t("transaction","actually");
x.t("transaction","transaction");
x.t("transaction","used");
x.t("credits","allison");
x.t("credits","available.");
x.t("credits","window");
x.t("credits","discounts");
x.t("credits","discounts.");
x.t("two","bills");
x.t("two","appliedtotxnret");
x.t("two","types");
x.t("/txnid","txntype");
x.t("/txnid","setcredit");
x.t("/txnid","paymentamount");
x.t("125.00","/creditremaining");
x.t("note","credit");
x.t("quickbooks","shows");
x.t("quickbooks","pays");
x.t("vendors.","user");
x.t("/fullname","/payeeentityref");
x.t("/fullname","/discountaccountref");
x.t("/fullname","/apaccountref");
x.t("created","billpaymentcreditcardret");
x.t("created","depends");
x.t("created","bill");
x.t("created","result");
x.t("statusseverity=","info");
x.t("used","pay");
x.t("used","credits");
x.t("either","check");
x.t("special","helper");
x.t("corresponding","response");
x.t("/amountdue","/billtopay");
x.t("links","created");
x.t("links","using");
x.t("following","applied");
x.t("following","bill");
x.t("using","includelinkedtxns");
x.t("using","appliedtotxnadd");
x.t("example:","paying");
x.t("example:","billtopayquery");
x.t("example:","error");
x.t("example:","overpayment");
x.t("example:","applying");
x.t("statusmessage=","status");
x.t("setting","discount");
x.t("setting","credit");
x.t("general","journal");
x.t("paymentamount","80.00");
x.t("paymentamount","50.00");
x.t("/setcredit","/appliedtotxnadd");
x.t("/setcredit","discountamount");
x.t("returns","list");
x.t("returns","information");
x.t("18-5","shows");
x.t("18-5","billtopayquery");
x.t("requestid=","123456");
x.t("/qbxml","setting");
x.t("discountaccountref","fullname");
x.t("transaction.","payment");
x.t("transaction.","listing");
x.t("18-6","billtopayquery");
x.t("18-6","returns");
x.t("accounts","payable");
x.t("without","paying");
x.t("applying","payment");
x.t("/discountamount","discountaccountref");
x.t("goodcustomerdiscount","/fullname");
x.t("bill.","listing");
x.t("bill.","bill");
x.t("bill.","example:");
x.t("18-7","paying");
x.t("18-7","shows");
x.t("$100","listing");
x.t("$45","example");
x.t("list.","special");
x.t("billpaymentcheckret","transaction");
x.t("18-8","example");
x.t("18-8","creating");
x.t("generates","two");
x.t("object","vendor.");
x.t("object","bill");
x.t("object","appliedtotxnadd");
x.t("object","case");
x.t("object","created");
x.t("specify","payment");
x.t("specify","transaction");
x.t("/billtopay","/billtopayret");
x.t("$100.","suppose");
x.t("18-9","status");
x.t("18-9","payment");
}
