function  WWHBookData_AddTOCEntries(P)
{
var A=P.fN("Introduction","1");
var B=A.fN("Why an SDK for QuickBooks?","2");
var C=B.fN("Sharing Data with QuickBooks","2#102441");
C=B.fN("A Common SDK Foundation","2#102893");
C=B.fN("Support for Multiple Programming Languages","2#102756");
B=A.fN("Design Principles","3");
B=A.fN("Runtime Components of the QuickBooks SDK","4");
C=B.fN("QuickBooks Online Edition","4#102220");
B=A.fN("What\u2019s Included in the QuickBooks SDK?","5");
B=A.fN("Which API to Use","6");
B=A.fN("Supported Versions and Products","7");
C=B.fN("FAQs about Versioning","7#104403");
B=A.fN("Before You Begin","8");
C=B.fN("The Getting Started Video","8#106953");
C=B.fN("Onscreen Reference","8#106964");
C=B.fN("Programmer\u2019s Guide","8#106317");
B=A.fN("What\u2019s Next?","9");
A=P.fN("Communicating with QuickBooks","10");
B=A.fN("Typical Sequence","11");
C=B.fN("Setting Up the Communications Channel","11#102459");
C=B.fN("What\u2019s in a Message?","11#103741");
C=B.fN("Key Concepts","11#102441");
B=A.fN("Receiving Events from QuickBooks","12");
B=A.fN("QuickBooks (U.S. and Canadian Editions)","13");
C=B.fN("qbXML Request Processor API","13#103754");
C=B.fN("QBFC API","13#103770");
B=A.fN("QuickBooks Online Edition","14");
B=A.fN("Security Concerns","15");
C=B.fN("Authentication","15#102175");
C=B.fN("Authorization","15#104065");
C=B.fN("Access to Personal Data","15#104114");
C=B.fN("Access to Payroll Data","15#105438");
B=A.fN("Error Handling","16");
B=A.fN("Synchronization","17");
A=P.fN("Request and Response Messages","18");
B=A.fN("\u201cObjects\u201d in the SDK","19");
C=B.fN("Lists","19#106067");
C=B.fN("Transactions","19#106464");
B=A.fN("Operations","20");
B=A.fN("Naming Conventions for Messages","21");
C=B.fN("Request Messages","21#107091");
C=B.fN("Response Messages","21#106093");
B=A.fN("Return Status","22");
B=A.fN("Elements and Aggregates","23");
C=B.fN("Object References","23#107246");
B=A.fN("qbXML Sample Request and Response","24");
B=A.fN("Queries and Filters","25");
B=A.fN("Reports","26");
C=B.fN("Categories of Reports","26#112620");
C=B.fN("Customization","26#112841");
B=A.fN("Messages Supported by the SDK for QuickBooks","27");
A=P.fN("Glossary","28");
A=P.fN("Examples","29");
}
