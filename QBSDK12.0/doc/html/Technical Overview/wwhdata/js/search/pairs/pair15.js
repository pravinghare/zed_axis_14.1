function FileData_Pairs(x)
{
x.t("supports","user");
x.t("example","certain");
x.t("example","social");
x.t("available","application");
x.t("rogue","application");
x.t("digital","signature");
x.t("digital","certificate");
x.t("digital","code-signing");
x.t("publisher","tampered");
x.t("just","applications.");
x.t("owner","user");
x.t("owner","application");
x.t("owner","controls");
x.t("describes","obtain");
x.t("currently","signed");
x.t("currently","subscribed");
x.t("key","name");
x.t("required","session");
x.t("required","valid");
x.t("required","above");
x.t("required","intuit-signed");
x.t("events","authorizing");
x.t("microsoft","authenticode");
x.t("edition.","quickbooks");
x.t("user","just");
x.t("user","interface");
x.t("user","qboe");
x.t("user","granted");
x.t("user","privilege");
x.t("user","makes");
x.t("user","appropriate");
x.t("user","application");
x.t("user","grant");
x.t("user","quickbooks");
x.t("user","company");
x.t("user","assurance");
x.t("digitally","signed");
x.t("integrations","hosted");
x.t("unattended","mode.");
x.t("sites","user");
x.t("interface","authentication");
x.t("points","connection");
x.t("register","hosted");
x.t("supply","certain");
x.t("administrator","authorizes");
x.t("administrator","controls");
x.t("best","assure");
x.t("originated","software");
x.t("technology.","quickbooks");
x.t("initiates","authorization");
x.t("assure","small-business");
x.t("public","key");
x.t("editions","beginsession");
x.t("time","application");
x.t("connection","ticket.");
x.t("connection","ticket");
x.t("connection","process");
x.t("party","certificate");
x.t("canceling","changing");
x.t("u.s.","canadian");
x.t("back","application.");
x.t("access","user");
x.t("access","certain");
x.t("access","company\u2019s");
x.t("access","payroll");
x.t("access","personal");
x.t("access","quickbooks.");
x.t("access","data");
x.t("access","quickbooks");
x.t("certain","points");
x.t("certain","payroll");
x.t("certain","personal");
x.t("certain","types");
x.t("certain","user-initiated");
x.t("certain","urls");
x.t("certain","users");
x.t("signature","provides");
x.t("authorizes","further");
x.t("qboe","connector");
x.t("qboe","qbms.");
x.t("canadian","editions");
x.t("security","concerns");
x.t("security","number");
x.t("invoked","certain");
x.t("need","supply");
x.t("openconnection2","call");
x.t("proper","authorization:");
x.t("file","currently");
x.t("issued","certificate");
x.t("beginsession","call");
x.t("granted","application");
x.t("network","website");
x.t("logged","administrative");
x.t("results","check");
x.t("check","valid");
x.t("ticket.","isn\u2019t");
x.t("guide","information.");
x.t("remove","web");
x.t("concerns","security");
x.t("concerns","communicating");
x.t("concerns","_________________________________________________________________________________________________________________");
x.t("company\u2019s","quickbooks");
x.t("qbms","integrations");
x.t("privilege","structure");
x.t("ticket","user.");
x.t("ticket","application.");
x.t("ticket","authorized");
x.t("obtain","digital");
x.t("obtain","required");
x.t("name","expiration");
x.t("name","certificate");
x.t("transparently","application");
x.t("social","security");
x.t("payroll","service.");
x.t("payroll","data");
x.t("payroll","service");
x.t("provides","user");
x.t("provides","end");
x.t("authorizing","application");
x.t("further","application");
x.t("session","ticket");
x.t("session","authentication");
x.t("combine","ensure");
x.t("authenticode","technology.");
x.t("makes","proper");
x.t("attempts","access");
x.t("developer.intuit.com","describes");
x.t("web","sites");
x.t("web","service\u2019s");
x.t("web","connector");
x.t("web","services");
x.t("web","application");
x.t("web","applications");
x.t("web","service");
x.t("post","requests");
x.t("sdk","supports");
x.t("certificate","post");
x.t("certificate","third");
x.t("certificate","serial");
x.t("certificate","application.");
x.t("certificate","authority");
x.t("certificate","using");
x.t("certificate","contains");
x.t("expiration","date");
x.t("third","party");
x.t("authorization.","authorization");
x.t("permissions","application");
x.t("addition","user");
x.t("personal","data");
x.t("files.","quickbooks");
x.t("intuit","developer");
x.t("hosted","web");
x.t("serial","number.");
x.t("first","time");
x.t("number","mechanisms");
x.t("number","contained");
x.t("quickbooks.","intuit");
x.t("applications.","addition");
x.t("service\u2019s","permission");
x.t("itself.","access");
x.t("small-business","owner");
x.t("behalf","application.");
x.t("software","publisher");
x.t("different","quickbooks");
x.t("specifies","permissions");
x.t("whether","given");
x.t("requests","qboe");
x.t("stores","connection");
x.t("accessing","company");
x.t("handling","posts");
x.t("types","data");
x.t("returned","application.");
x.t("section","application");
x.t("subscription","payroll");
x.t("end","user");
x.t("code","originated");
x.t("client","certificate");
x.t("connector","transparently");
x.t("connector","itself.");
x.t("connector","used");
x.t("connector","obtains");
x.t("connector","maintains");
x.t("authorization:","connector");
x.t("programmer\u2019s","guide");
x.t("information.","authorization");
x.t("authorization","invoked");
x.t("authorization","web");
x.t("authorization","process");
x.t("authorization","process.");
x.t("authorization","_____________________________________________________________________________________________________");
x.t("given","integrated");
x.t("previous","section");
x.t("communicating","quickbooks");
x.t("ensure","application");
x.t("website","http://");
x.t("user-initiated","events");
x.t("changing","authorization.");
x.t("service.","access");
x.t("mechanisms","combine");
x.t("masquerading","application");
x.t("qbms.","required");
x.t("qbms.","register");
x.t("running","logged");
x.t("signed","subscription");
x.t("signed","application");
x.t("developer","network");
x.t("wherein","example");
x.t("process","behalf");
x.t("process","different");
x.t("process","used");
x.t("isn\u2019t","qboe");
x.t("view","personal");
x.t("authentication","required");
x.t("authentication","authorization");
x.t("authentication","_____________________________________________________________________________________________________");
x.t("valid","connection");
x.t("valid","server");
x.t("administrative","user");
x.t("administrative","user.");
x.t("established","quickbooks");
x.t("services","_____________________________________________________________________________________________________");
x.t("online","edition.");
x.t("online","edition");
x.t("user.","administrator");
x.t("user.","session");
x.t("application.","access");
x.t("application.","certificate");
x.t("application.","programmer\u2019s");
x.t("application.","authentication");
x.t("application.","urls");
x.t("information","public");
x.t("urls","used");
x.t("data","example");
x.t("data","available");
x.t("data","returned");
x.t("data","described");
x.t("data","quickbooks");
x.t("data","following");
x.t("data","_____________________________________________________________________________________________________");
x.t("code-signing","certificate");
x.t("number.","digital");
x.t("appropriate","web");
x.t("application","digitally");
x.t("application","access");
x.t("application","need");
x.t("application","obtain");
x.t("application","attempts");
x.t("application","accessing");
x.t("application","masquerading");
x.t("application","permission");
x.t("application","users");
x.t("application","used");
x.t("application","starts");
x.t("application","leads");
x.t("application","tampered");
x.t("application","remains");
x.t("http://","developer.intuit.com");
x.t("grant","remove");
x.t("met:","administrative");
x.t("authority","issued");
x.t("authority","handling");
x.t("notifies","end");
x.t("posts","qboe");
x.t("structure","established");
x.t("above","process");
x.t("permission","access");
x.t("permission","certain");
x.t("permission","view");
x.t("described","previous");
x.t("date","name");
x.t("with.","quickbooks");
x.t("users","access");
x.t("users","specifies");
x.t("two","conditions");
x.t("quickbooks","unattended");
x.t("quickbooks","administrator");
x.t("quickbooks","best");
x.t("quickbooks","u.s.");
x.t("quickbooks","security");
x.t("quickbooks","provides");
x.t("quickbooks","sdk");
x.t("quickbooks","web");
x.t("quickbooks","files.");
x.t("quickbooks","running");
x.t("quickbooks","wherein");
x.t("quickbooks","administrative");
x.t("quickbooks","online");
x.t("quickbooks","notifies");
x.t("quickbooks","quickbooks");
x.t("quickbooks","company");
x.t("applications","required");
x.t("applications","integrate");
x.t("inform","certain");
x.t("controls","access");
x.t("controls","whether");
x.t("edition","qboe");
x.t("edition","qbms");
x.t("call","initiates");
x.t("call","back");
x.t("call","results");
x.t("used","openconnection2");
x.t("used","obtain");
x.t("used","small-business");
x.t("used","inform");
x.t("used","call");
x.t("process.","first");
x.t("starts","quickbooks");
x.t("service","programmer\u2019s");
x.t("service","authorized");
x.t("contained","quickbooks");
x.t("following","two");
x.t("_____________________________________________________________________________________________________","certain");
x.t("_____________________________________________________________________________________________________","web");
x.t("_____________________________________________________________________________________________________","authorization");
x.t("_____________________________________________________________________________________________________","applications");
x.t("_____________________________________________________________________________________________________","quickbooks");
x.t("using","microsoft");
x.t("company","canceling");
x.t("company","file");
x.t("company","file.");
x.t("mode.","quickbooks");
x.t("intuit-signed","client");
x.t("obtains","stores");
x.t("maintains","session");
x.t("without","permission");
x.t("assurance","code");
x.t("leads","administrative");
x.t("_________________________________________________________________________________________________________________","number");
x.t("tampered","rogue");
x.t("tampered","with.");
x.t("file.","without");
x.t("subscribed","payroll");
x.t("conditions","met:");
x.t("remains","tamperproof");
x.t("tamperproof","small-business");
x.t("integrate","quickbooks");
x.t("contains","information");
x.t("server","certificate");
x.t("authorized","user");
x.t("authorized","quickbooks");
x.t("integrated","application");
}
