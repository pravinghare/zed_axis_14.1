using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using QBFC5Lib;
using QBXMLRP2ELib;
using System.Text;

namespace CustomerQuery
{
	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public class WebForm1 : System.Web.UI.Page
	{
		protected System.Web.UI.WebControls.TextBox txtBx_CustomerName;
		protected System.Web.UI.WebControls.TextBox txtBx_Result;
		protected System.Web.UI.WebControls.TextBox txtBx_Trace;
		protected System.Web.UI.WebControls.Label Label1;
		protected System.Web.UI.WebControls.Label Label2;
		protected System.Web.UI.WebControls.Label Label3;
		protected System.Web.UI.WebControls.Label Label4;
		protected System.Web.UI.WebControls.Label Label5;
		protected System.Web.UI.WebControls.Button Btn_Submit;
	
		private void Page_Load(object sender, System.EventArgs e)
		{
			// Put user code to initialize the page here
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Btn_Submit.Click += new System.EventHandler(this.Btn_Submit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private void Btn_Submit_Click(object sender, System.EventArgs e) {
			//Step1: verify that Name is not empty
			if(txtBx_CustomerName.Text.Length<=0){
				txtBx_Result.Text="Cannot continue. Please enter a customer name.";
				return;
			}
			//Step2: use QBFC to format the request
			string customerName=txtBx_CustomerName.Text.ToString();
			QBSessionManagerClass sessMgr = new QBSessionManagerClass();
			txtBx_Trace.Text=txtBx_Trace.Text + "QBSessionManagerClass instantiated." + "\r\n";
			IMsgSetRequest msgSetRq = sessMgr.CreateMsgSetRequest("US", 2, 0);
			txtBx_Trace.Text=txtBx_Trace.Text + "MessageSetRequest created." + "\r\n";
			msgSetRq.Attributes.OnError=ENRqOnError.roeContinue;
			ICustomerQuery customerQuery=msgSetRq.AppendCustomerQueryRq();
			customerQuery.ORCustomerListQuery.CustomerListFilter.ORNameFilter.NameFilter.MatchCriterion.SetValue(ENMatchCriterion.mcContains);
			customerQuery.ORCustomerListQuery.CustomerListFilter.ORNameFilter.NameFilter.Name.SetValue(customerName);
			string inputRq=msgSetRq.ToXMLString();
			txtBx_Trace.Text=txtBx_Trace.Text + inputRq.ToString() + "\r\n";
			//Step3: use qbXMLRP2e wrapper (EXE wrapper around qbXML2RP) to call QB
			RequestProcessor2Class rp = new RequestProcessor2Class();
			string ticket=""; //System.Guid.NewGuid().ToString();
			string outputRs="";
			try{
				rp.OpenConnection("c#-ASP.NET", "CustomerQuery ASP.NET sample");
				txtBx_Trace.Text=txtBx_Trace.Text + "OpenConnection() was successful" + "\r\n";
				ticket=rp.BeginSession("", QBFileModeE.qbFileOpenDoNotCare);
				txtBx_Trace.Text=txtBx_Trace.Text + "BeginSession() was successful" + "\r\n";
				txtBx_Trace.Text=txtBx_Trace.Text + "Sending Request XML" + "\r\n";
				outputRs=rp.ProcessRequest(ticket, inputRq);
				txtBx_Trace.Text=txtBx_Trace.Text + "Request processing was successful." + "\r\n";
				txtBx_Trace.Text=txtBx_Trace.Text + "Response received." + "\r\n";
				txtBx_Trace.Text=txtBx_Trace.Text + outputRs.ToString();
			}
			catch(Exception ex){
				txtBx_Result.Text=ex.Message+"\r\n" + ex.StackTrace + "\r\n";
				txtBx_Trace.Text=txtBx_Trace.Text + ex.Message+"\r\n" + ex.StackTrace + "\r\n";
			}
			finally{
				if(ticket!=null){
					rp.EndSession(ticket);
					txtBx_Trace.Text=txtBx_Trace.Text + "EndSession() was successful" + "\r\n";
				}
				if(rp!=null){
					rp.CloseConnection();
					txtBx_Trace.Text=txtBx_Trace.Text + "CloseConnection() was successful" + "\r\n";
				}
			}
			if(outputRs.Length<=0){
				txtBx_Result.Text="Empty response XML received from QuickBooks.";
				return;
			}
			//Step4: retrieve returned data with QBFC and return a message
			IMsgSetResponse msgSetRs=sessMgr.ToMsgSetResponse(outputRs, "US", 2, 0);
			StringBuilder msg=new StringBuilder();
			//check if we have one response for our single query request
			if(msgSetRs.ResponseList.Count==1){
				IResponse rs=msgSetRs.ResponseList.GetAt(0);
				string retStatusCode=rs.StatusCode.ToString();
				string retStatusMessage=rs.StatusMessage;
				msg.AppendFormat("statusCode= {0}, statusMessage = {1}", retStatusCode, retStatusMessage);
				//retrieve some CustomerRet values
				//Query return a RetList object in rs.Detail 
				ICustomerRetList customerRetList=rs.Detail as ICustomerRetList;
				if(customerRetList==null){
					txtBx_Result.Text="No customer was returned for this name.";
					return;
				}
				for (int j=0; j<=customerRetList.Count-1;j++){ 
					ICustomerRet customerRet=customerRetList.GetAt(j);
					string fullName=customerRet.FullName.GetValue();
					double balance=customerRet.Balance.GetValue();
					msg.AppendFormat("\r\n" + "Customer fullname = {0} -- Balance = {1}", fullName, balance);
				}//for
				txtBx_Result.Text=msg.ToString();
			}//if(msgSetRs.ResponseList
			return;
		}




	}
}
