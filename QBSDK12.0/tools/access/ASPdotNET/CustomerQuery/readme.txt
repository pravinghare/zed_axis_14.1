
This sample is a simple ASP.NET web form application that queries a customer from Quickbooks by FullName.
QuickBooks and .NET must be installed on the machine. The sample uses c#-ASP.NET language.

The sample shows how to mix QBFC and qbXMLRP2 calls. QBFC is used to build the qbXML request 
and parse the qbXML response.

Because of IIS4 and 5 threading and security model, an ASP.NET page cannot access directly 
qbXMLRP2.dll because its BeginSession fails. To solve this problem, an EXE COM wrapper around qbXMLRP2.dll
was created (qbXMLRP2e) to insure the right configuration. This wrapper is configured to run
as the interactive user, thus insuring that it shares the same active desktop as QuickBooks.

Building/Installing the sample
-------------------
- From a command prompt navigate to SDKInstalldir\tools\access\qbxmlrp2e. Register the COM qbXMLRP2e.exe wrapper, by running 'qbXMLRP2e.exe /RegServer'
- Copy the entire CustomerQuery sample directory under Inetpub\wwwroot. 
- Make sure that the Inetpub\wwwroot\CustomerQuery\bin files are not read-only.
- Open Inetpub\wwwroot\CustomerQuery\CustomerQuery.sln in Microsoft Visual Studio .NET and build the solution.
- Create CustomerQuery IIS virtual directory for this folder. The fast way to do this last step is to:
	a) open IIS
	b) right mouse click, and select Properties for 'CustomerQuery' 
	c) on the CustomerQuery Properties->Directory tab, click on 'Create' button (which changes right away into 'Remove'), then click OK.
- Give the ASPNET account permissions to launch and access qbXMLRP2e COM object using dcomcnfg by doing the following: 
	a) Launch DCOMCNFG by clicking the Start button, selecting Run, and typing "Dcomcnfg" in the Run dialog box
	b) Select qbXMLRP2e and click 'Properties...' button  (on Windows XP you will need to navigate the following path in the management console: Console Root->Component Services->Computers->My Computer->DCOM Components->qbXMLRP2e
	c) In the Security tab select 'Use Custom access permissions' and click Edit, then add ASPNET user and INTERACTIVE to the Registry Value Permissions dialog box
   	Note that IIS must be installed before the .NET framework in order for the ASPNET account to be created.
	d) Repeat step 3 for the 'Use Custom launch permissions' button.

Running the sample
------------------
Before running CustomerQuery.exe, make sure that .NET runtime is installed on the machine, and QuickBooks is running with a company file opened. 

- From a command prompt navigate to SDKInstalldir\tools\access\qbxmlrp2e. Register the COM qbXMLRP2e.exe wrapper, by running 'qbXMLRP2e.exe /RegServer'
- Copy the entire CustomerQuery sample directory under Inetpub\wwwroot. 
- Make sure that the Inetpub\wwwroot\CustomerQuery\bin files are not read-only.
- Open Inetpub\wwwroot\CustomerQuery\CustomerQuery.sln in Microsoft Visual Studio .NET and build the solution.
- Create CustomerQuery IIS virtual directory for this folder. The fast way to do this last step is to:
	a) open IIS
	b) right mouse click, and select Properties for 'CustomerQuery' 
	c) on the CustomerQuery Properties->Directory tab, click on 'Create' button (which changes right away into 'Remove'), then click OK.
- Give the ASPNET account permissions to launch and access qbXMLRP2e COM object using dcomcnfg by doing the following: 
	a) Launch DCOMCNFG by clicking the Start button, selecting Run, and typing "Dcomcnfg" in the Run dialog box
	b) Select qbXMLRP2e and click 'Properties...' button  (on Windows XP you will need to navigate the following path in the management console: Console Root->Component Services->Computers->My Computer->DCOM Components->qbXMLRP2e
	c) In the Security tab select 'Use Custom access permissions' and click Edit, then add ASPNET user and INTERACTIVE to the Registry Value Permissions dialog box
   	Note that IIS must be installed before the .NET framework in order for the ASPNET account to be created.
	d) Repeat step 3 for the 'Use Custom launch permissions' button.
- Open an internet browser and go to url: http://localhost/CustomerQuery/CustomerQuery.aspx
