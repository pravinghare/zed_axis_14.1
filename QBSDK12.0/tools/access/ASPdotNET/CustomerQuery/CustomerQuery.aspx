<%@ Page language="c#" Codebehind="CustomerQuery.aspx.cs" AutoEventWireup="false" Inherits="CustomerQuery.WebForm1" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>WebForm1</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="C#" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<asp:textbox id="txtBx_CustomerName" style="Z-INDEX: 100; LEFT: 8px; POSITION: absolute; TOP: 136px"
				runat="server" ForeColor="#0000C0"></asp:textbox>
			<asp:Label id="Label5" style="Z-INDEX: 109; LEFT: 8px; POSITION: absolute; TOP: 112px" runat="server"
				Height="24px" Width="456px" Font-Names="Courier New" Font-Size="Smaller">Enter a name of customer to search</asp:Label>
			<asp:Label id="Label4" style="Z-INDEX: 108; LEFT: 8px; POSITION: absolute; TOP: 32px" runat="server"
				Height="40px" Width="456px" Font-Names="Courier New" Font-Size="Smaller">This sample demonstrates how to use QBFC to build a CustomerQuery request and  use QBXMLRP2E to send the request XML to QuickBooks for processing. Finally, it displays the query result below. </asp:Label>
			<asp:Label id="Label3" style="Z-INDEX: 106; LEFT: 8px; POSITION: absolute; TOP: 216px" runat="server"
				Height="24px" Width="160px" Font-Names="Courier New" Font-Size="Smaller">Result</asp:Label><asp:button id="Btn_Submit" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 168px"
				runat="server" Text="Submit"></asp:button><asp:textbox id="txtBx_Result" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 240px"
				runat="server" Height="128px" Width="456px" TextMode="MultiLine" ForeColor="#0000C0"></asp:textbox>
			<asp:TextBox id="txtBx_Trace" style="Z-INDEX: 103; LEFT: 8px; POSITION: absolute; TOP: 400px"
				runat="server" Height="280px" Width="456px" TextMode="MultiLine" ForeColor="#0000C0"></asp:TextBox>
			<asp:Label id="Label1" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px" runat="server"
				Height="16px" Width="320px" Font-Names="Courier New" ForeColor="Black" Font-Bold="True">c#-ASP.NET Customer Query Sample</asp:Label>
			<asp:Label id="Label2" style="Z-INDEX: 105; LEFT: 8px; POSITION: absolute; TOP: 376px" runat="server"
				Height="24px" Width="160px" Font-Names="Courier New" Font-Size="Smaller">Execution trace</asp:Label></form>
	</body>
</HTML>
