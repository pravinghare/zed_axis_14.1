Imports QBXMLRP2ELib
Imports QBFC5Lib


'----------------------------------------------------------
' Form: CustomerAdd
'
' Description:  This form shows how to use qbXMLRPe wrapper to call
'               the QuickBooks SDK, when running under IIS ASP.NET.
'               Also this sample shows how to mix using QBFC and qbXML calls.
'
' Created On: 09/10/2002
'
' Copyright � 2002 Intuit Inc. All rights reserved.
' Use is subject to the terms specified at:
'      http://developer.intuit.com/legal/devsite_tos.html
'
'----------------------------------------------------------

Public Class CustomerAddWebForm
    Inherits System.Web.UI.Page
    Protected WithEvents Name As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents XMLResponse As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents CustomerAdd As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
    End Sub

    Private Sub CustomerAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CustomerAdd.Click

        XMLResponse.Text = ""
        'step1: verify that Name.Text is not empty
        Dim newName As String = Name.Text.Trim()
        If (newName.Length() = 0) Then
            XMLResponse.Text = "Please enter a name."
            Exit Sub
        End If

        'step2: use QBFC to format the request
        Dim sessMgr As New QBSessionManager
        Dim msgSetRq As IMsgSetRequest
        msgSetRq = sessMgr.CreateMsgSetRequest("US", 2, 0)
        msgSetRq.AppendCustomerAddRq.Name.SetValue(newName)
        Dim inputRq As String = msgSetRq.ToXMLString()

        'step3: use qbXMLRPe wrapper (EXE wrapper around qbXMLRP) to call QB
        Dim rp As New RequestProcessor2Class
        Dim ticket As String
        Dim outputRs As String
        Try
            rp = New RequestProcessor2Class
            rp.OpenConnection("ASP.NET", "CustomerAdd ASP.NET sample ")
            ticket = rp.BeginSession("", QBFileModeE.qbFileOpenDoNotCare)
            outputRs = rp.ProcessRequest(ticket, inputRq)
        Catch
            XMLResponse.Text = Hex(Err.Number).ToString & ": " & Err.Description
        Finally
            If Not (ticket Is Nothing) Then
                rp.EndSession(ticket)
            End If
            If Not rp Is Nothing Then
                rp.CloseConnection()
            End If
        End Try

        If (outputRs Is Nothing) Then
            Exit Sub
        End If

        'step4: retrieve returned data with QBFC and return a message
        Dim msgSetRs As IMsgSetResponse
        msgSetRs = sessMgr.ToMsgSetResponse(outputRs, "US", 2, 0)
        Dim popupMessage As New System.Text.StringBuilder()

        'for sure we have one response for our single add request, but check anyway
        If (msgSetRs.ResponseList.Count = 1) Then
            Dim rs As IResponse
            rs = msgSetRs.ResponseList.GetAt(0)

            'get the status code and info 
            Dim retStatusCode As String = rs.StatusCode
            Dim retStatusMessage As String = rs.StatusMessage
            popupMessage.AppendFormat("statusCode = {0}, statusMessage = {1}", _
                retStatusCode, retStatusMessage)

            'retrieve some CustomerRet values
            'Add and Mod Rq return a single Ret object in rs.Detail
            'Query return a RetList object in rs.Detail 
            Dim customerRet As ICustomerRet
            customerRet = rs.Detail
            If (Not customerRet Is Nothing) Then
                Dim listID As String = customerRet.ListID.GetValue()
                Dim fullName As String = customerRet.FullName.GetValue()

                popupMessage.AppendFormat( _
                    ControlChars.CrLf & "<BR> Customer ListID = {0} <BR> Customer FullName={1}", _
                    listID, fullName)

                'retrieve a field that may NOT be part of the QuickBook response    
                If Not (customerRet.Phone Is Nothing) Then
                    popupMessage.AppendFormat("<BR> Customer Phone = {0}", _
                      customerRet.Phone.GetValue)
                End If
            End If

            XMLResponse.Text = popupMessage.ToString()
        End If

    End Sub
End Class
