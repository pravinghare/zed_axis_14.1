

What is qbXMLRP2e
----------------
qbXMLRP2e wrapper is an EXE COM wrapper around qbXMLRP2.dll. 
qbXMLRP2e allows an application to communicate with QuickBooks SDK when using DCOM, 
or when running under IIS, as in ASP or ASP.NET solutions.
Note: If you run into problems using qbxmlrp2e, a workaround might be to use the interactive user as the identity for qbxmlrp2e, with the interactive user currently logged on.



Redistribution
--------------
You can redistribute this qbXMLRP2e.exe with your application, as long as you don't modify this exe. 
To register qbXMLRP2e in the registry, your installer must call qbXMLRP2e.exe /RegServer.

Important note: 
If your application uses DCOM on Win95/Win98/ME, then after registering qbXMLRP2e, 
the computer must be re-booted.


What if I want QuickBooks to show my application certificate
-------------------------------------------------------------
When using qbXMLRP2e.exe with no modification, QuickBooks will not pick up your exe's certificate, and 
the end-user will be told that an application with no certificate is requiring access to the current 
company file. In this scenario, you need to provide a new wrapper, called qbXMLRP<Your application name>.exe, 
which you can sign. 

Important note:
You are responsible to insure that you don't break other applications that use the existing
qbXMLRP2e: your new wrapper and existing qbXMLRP2e must be able to coexist on the same machine, 
and they don't interfere with each other. 
All the CLSIDs, the typelibrary object names for interfaces, or COM classes must be changed!

On purpose, the source files cannot be compiled as such. The CLSIDs are obfuscated, 
you need to provide your own and make the appropriate changes as stated above.