
qbXMLRPeps.dll: dlldata.obj qbXMLRPe_p.obj qbXMLRPe_i.obj
	link /dll /out:qbXMLRPeps.dll /def:qbXMLRPeps.def /entry:DllMain dlldata.obj qbXMLRPe_p.obj qbXMLRPe_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del qbXMLRPeps.dll
	@del qbXMLRPeps.lib
	@del qbXMLRPeps.exp
	@del dlldata.obj
	@del qbXMLRPe_p.obj
	@del qbXMLRPe_i.obj
