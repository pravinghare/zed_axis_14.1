
/* AuthPreferencesE.h : Declaration of the CAuthPreferencesE
 *
 * This class represents the COM object for the auth preferences in QB 
 *
 * (c) 2005 Intuit Inc. All rights reserved.
 * Use is subject to the terms specified at:                            
 *     http://developer.intuit.com/legal/   
 */

#ifndef __AUTHPREFERENCESE_H_
#define __AUTHPREFERENCESE_H_

#include "resource.h"       // main symbols
#include "qbXMLRP2e.h" 
#import "..\..\..\Qbfc\Includes\qbxmlrp2.dll"	// specify the path so we know which one we're using

/////////////////////////////////////////////////////////////////////////////
// CAuthPreferencesE
class ATL_NO_VTABLE CAuthPreferencesE : 
  public CComObjectRootEx<CComSingleThreadModel>,
  public CComCoClass<CAuthPreferencesE/*, &CLSID_AuthPreferencesE*/>,
  public ISupportErrorInfo,
  public IDispatchImpl<IAuthPreferencesE, &IID_IAuthPreferencesE, &LIBID_QBXMLRP2ELib>
{
public:
  CAuthPreferencesE();


static HRESULT WINAPI UpdateRegistry(BOOL bRegister){return S_OK;};

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CAuthPreferencesE)
  COM_INTERFACE_ENTRY(IAuthPreferencesE)
  COM_INTERFACE_ENTRY(IDispatch)
  COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
  STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

public:  
  void SetAuthPreferences(QBXMLRP2Lib::IAuthPreferencesPtr realAuthPrefsPtr);

// IAuthPreferencesE
public:
  STDMETHOD(WasAuthPreferencesObeyed)( /*[in]*/ BSTR ticket, /*[out, retval]*/ VARIANT_BOOL *pWasAuthPreferencesObeyed);
  STDMETHOD(GetIsReadOnly)( /*[in]*/ BSTR ticket, /*[out, retval]*/ VARIANT_BOOL *pIsReadOnly);
  STDMETHOD(PutIsReadOnly)( /*[in]*/ VARIANT_BOOL isReadOnly);
  STDMETHOD(GetUnattendedModePref)( /*[in]*/ BSTR ticket, /*[out, retval]*/ QBXMLRPUnattendedModePrefTypeE *pUnattendedModePref);
  STDMETHOD(PutUnattendedModePref)( /*[in]*/ QBXMLRPUnattendedModePrefTypeE unattendedModePref);
  STDMETHOD(GetPersonalDataPref)( /*[in]*/ BSTR ticket, /*[out, retval]*/ QBXMLRPPersonalDataPrefTypeE *pPersonalDataPref);
  STDMETHOD(PutPersonalDataPref)( /*[in]*/ QBXMLRPPersonalDataPrefTypeE personalDataPref);
  STDMETHOD(PutAuthFlags)( /*[in]*/ long authFlags);

private:
  QBXMLRP2Lib::IAuthPreferencesPtr authPrefsPtr;
};

#endif //__AUTHPREFERENCESE_H_
