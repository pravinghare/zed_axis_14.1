// RequestProcessor.h : Declaration of the CRequestProcessor

#ifndef __REQUESTPROCESSOR_H_
#define __REQUESTPROCESSOR_H_

#include "AuthPreferencesE.h"	// added to support IRequestProcessor4's AuthPreferences property
#include "resource.h"       // main symbols
#import "..\..\..\Qbfc\Includes\qbxmlrp2.dll"	// specify the path so we know which one we're using

/////////////////////////////////////////////////////////////////////////////
// CRequestProcessor
class ATL_NO_VTABLE CRequestProcessor : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRequestProcessor, &CLSID_RequestProcessor2>,
	public IDispatchImpl<IRequestProcessor4, &IID_IRequestProcessor4, &LIBID_QBXMLRP2ELib>
{
public:
	CRequestProcessor()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_REQUESTPROCESSOR)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CRequestProcessor)
	COM_INTERFACE_ENTRY(IRequestProcessor3)
	COM_INTERFACE_ENTRY(IRequestProcessor4)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IRequestProcessor
public:
	STDMETHOD(get_QBXMLVersionsForSession)(/*[in]*/ BSTR ticket, /*[out, retval]*/ SAFEARRAY **pVal);
	STDMETHOD(get_ReleaseNumber)(/*[out, retval]*/ short *pVal);
	STDMETHOD(get_ReleaseLevel)(/*[out, retval]*/ QBXMLRPReleaseLevelE *pVal);
	STDMETHOD(get_MinorVersion)(/*[out, retval]*/ short *pVal);
	STDMETHOD(get_MajorVersion)(/*[out, retval]*/ short *pVal);
	STDMETHOD(GetCurrentCompanyFileName)(/*[in]*/ BSTR ticket, /*[out, retval]*/ BSTR* pFileName);
	STDMETHOD(EndSession)(/*[in]*/ BSTR ticket);
	STDMETHOD(BeginSession)(/*[in]*/ BSTR qbFileName, /*[in]*/ QBFileModeE reqFileMode, /*[out, retval]*/ BSTR* pTicket);
	STDMETHOD(CloseConnection)();
	STDMETHOD(ProcessRequest)(/*[in]*/ BSTR ticket, /*[in]*/ BSTR inputRequest, /*[out, retval]*/ BSTR* outputResponse);
	STDMETHOD(OpenConnection)( /*[in]*/ BSTR appID, /*[in]*/ BSTR appName);
	STDMETHOD(get_ConnectionType)(/*[out, retval]*/ QBXMLRPConnectionTypeE *pVal);
	STDMETHOD(ProcessSubscription)(	/*[in]*/ BSTR inputRequest, /*[out, retval]*/ BSTR	*outputResponse);
 	STDMETHOD(get_QBXMLVersionsForSubscription)(/*[out, retval]*/ SAFEARRAY **ppsa);
	STDMETHOD(OpenConnection2)(BSTR appID, BSTR appName, QBXMLRPConnectionTypeE connPref);
	STDMETHOD(get_AuthPreferences)(IAuthPreferencesE** ppAuthPreferences);

private:
  QBXMLRP2Lib::IRequestProcessor4Ptr qbXMLRPPtr;
  STDMETHODIMP VerifyQBXMLRP();
  CComPtr<IAuthPreferencesE> authPrefsEPtr;
};

#endif //__REQUESTPROCESSOR_H_
