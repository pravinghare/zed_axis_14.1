// TORUtil.cpp: implementation of the TORUtil class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "QBServerUtil.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQBServerUtil::CQBServerUtil()
{

}

CQBServerUtil::~CQBServerUtil()
{

}

const bool CQBServerUtil::IsNT()
{
	OSVERSIONINFO osvi;
	
	memset(&osvi, 0, sizeof(OSVERSIONINFO));
	osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
	if ( ::GetVersionEx (&osvi) )
	{
		if (osvi.dwPlatformId == VER_PLATFORM_WIN32_NT)
			return true;
	}

	return false;
}

BOOL CQBServerUtil::IsRegValueExist(HKEY mainKey, PTCHAR keyPath, PTCHAR keyName)
{
	BOOL bResult = false;

	CRegKey keyRunServices;
	LONG lRes = keyRunServices.Create(mainKey, keyPath);
	if (lRes == ERROR_SUCCESS)
	{
		TCHAR szValue[_MAX_PATH+1];
		ULONG ulLen = _MAX_PATH+1;
		DWORD dwType = 0;
		lRes = keyRunServices.QueryValue(keyName, &dwType, szValue, &ulLen);
		if (lRes == ERROR_SUCCESS)
			bResult = true;
	}
	return bResult;
}

BOOL CQBServerUtil::SetRegValue(HKEY mainKey, PTCHAR keyPath, PTCHAR keyName, LPCTSTR value)
{
	CRegKey keyRunServices;
	LONG lRes = keyRunServices.Create(mainKey, keyPath);
	if (lRes == ERROR_SUCCESS)
	{
		ULONG nBytes = strlen( value ) + 1;
		lRes = keyRunServices.SetValue(keyName, REG_SZ, value, nBytes );
		if (lRes == ERROR_SUCCESS)
			return true;
	}
	return false;
}

BOOL CQBServerUtil::DeleteRegValue(HKEY mainKey, PTCHAR keyPath, PTCHAR keyName)
{
	CRegKey keyRunServices;
	LONG lRes = keyRunServices.Create(mainKey, keyPath);
	if (lRes == ERROR_SUCCESS)
	{
		lRes = keyRunServices.DeleteValue(keyName);
		if (lRes == ERROR_SUCCESS)
			return true;
	}
	return false;
}

typedef DWORD (WINAPI *PFNREGSERVICE)(DWORD, DWORD);

BOOL CQBServerUtil::Register9xService(BOOL bRegister)
{
	BOOL bReturn = true;

	HMODULE hKernel32 = NULL;
	hKernel32 = LoadLibrary(_T("Kernel32.dll"));
	if (hKernel32)
	{
		PFNREGSERVICE pRegService = (PFNREGSERVICE) GetProcAddress(hKernel32, (LPCTSTR) _T("RegisterServiceProcess"));
		if (pRegService)
		{
			DWORD dwSuccess = (*pRegService)(NULL, bRegister);
			if (!dwSuccess)
				bReturn = false;
		}
		else
			bReturn = false;
		FreeLibrary(hKernel32);
	}
	else 
		bReturn = false;
	return bReturn;
}


