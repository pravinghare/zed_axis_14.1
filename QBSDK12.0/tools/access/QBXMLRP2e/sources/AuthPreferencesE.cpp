
/* AuthPreferencesE.cpp : Implementation of CAuthPreferencesE
 *
 * This class represents the COM object for the auth preferences in QB 
 *
 * (c) 2005 Intuit Inc. All rights reserved.
 * Use is subject to the terms specified at:                            
 *     http://developer.intuit.com/legal/   
 */

#include "stdafx.h"
#include "AuthPreferencesE.h"
  
/////////////////////////////////////////////////////////////////////////////
// CAuthPreferencesE
CAuthPreferencesE::CAuthPreferencesE() {
}

STDMETHODIMP CAuthPreferencesE::InterfaceSupportsErrorInfo(REFIID riid) {
  static const IID* arr[] = 
  {
    &IID_IAuthPreferencesE
  };
  for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)  {
    if (InlineIsEqualGUID(*arr[i],riid)) {
      return S_OK;
    }
  }
  return S_FALSE;
}

void CAuthPreferencesE::SetAuthPreferences(QBXMLRP2Lib::IAuthPreferencesPtr realAuthPrefsPtr){
	authPrefsPtr = realAuthPrefsPtr;
}

STDMETHODIMP CAuthPreferencesE::WasAuthPreferencesObeyed(BSTR ticket, VARIANT_BOOL *pWasAuthPreferencesObeyed){
	if (pWasAuthPreferencesObeyed == NULL) {
		return E_POINTER;
	} 

	try {
		*pWasAuthPreferencesObeyed = authPrefsPtr->WasAuthPreferencesObeyed(ticket);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::GetIsReadOnly(BSTR ticket, VARIANT_BOOL *pIsReadOnly){
	if (pIsReadOnly == NULL) {
		return E_POINTER;
	} 

	try {
		*pIsReadOnly = authPrefsPtr->GetIsReadOnly(ticket);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::PutIsReadOnly(VARIANT_BOOL isReadOnly){
	try {
		authPrefsPtr->PutIsReadOnly(isReadOnly);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::GetUnattendedModePref(BSTR ticket, QBXMLRPUnattendedModePrefTypeE *pUnattendedModePref){
	if (pUnattendedModePref == NULL) {
		return E_POINTER;
	} 

	try {
		*pUnattendedModePref = (QBXMLRPUnattendedModePrefTypeE)authPrefsPtr->GetUnattendedModePref(ticket);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::PutUnattendedModePref(QBXMLRPUnattendedModePrefTypeE unattendedModePref){
	try {
		authPrefsPtr->PutUnattendedModePref((QBXMLRP2Lib::QBXMLRPUnattendedModePrefType)unattendedModePref);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::GetPersonalDataPref(BSTR ticket, QBXMLRPPersonalDataPrefTypeE *pPersonalDataPref){
	if (pPersonalDataPref == NULL) {
		return E_POINTER;
	} 

	try {
		*pPersonalDataPref = (QBXMLRPPersonalDataPrefTypeE)authPrefsPtr->GetPersonalDataPref(ticket);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::PutPersonalDataPref(QBXMLRPPersonalDataPrefTypeE personalDataPref){
	try {
		authPrefsPtr->PutPersonalDataPref((QBXMLRP2Lib::QBXMLRPPersonalDataPrefType)personalDataPref);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}

STDMETHODIMP CAuthPreferencesE::PutAuthFlags(long authFlags){
	try {
		QBXMLRP2Lib::IAuthPreferences2Ptr authPrefs2Ptr = authPrefsPtr; 
		authPrefs2Ptr->PutAuthFlags(authFlags);
	} catch ( _com_error e ){
		SetErrorInfo(0, e.ErrorInfo() );
		return e.Error();
	}

	return S_OK;
}
