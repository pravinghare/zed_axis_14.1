VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form UIForm 
   Caption         =   "QBSDK20.Samples.Desktop.VB.DCOM_SDKTest"
   ClientHeight    =   6540
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6675
   LinkTopic       =   "Form1"
   ScaleHeight     =   6540
   ScaleWidth      =   6675
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Caption         =   "Name of computer where QuickBooks is installed."
      Height          =   1095
      Left            =   240
      TabIndex        =   6
      Top             =   1080
      Width           =   6255
      Begin VB.TextBox ComputerName 
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   5895
      End
      Begin VB.Label Label2 
         Caption         =   "Leave blank to specify this computer."
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   720
         Width           =   4455
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Show Request"
      Height          =   495
      Left            =   1515
      TabIndex        =   5
      Top             =   2400
      Width           =   1455
   End
   Begin VB.TextBox inputFile 
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   4935
   End
   Begin VB.CommandButton browseButton 
      Caption         =   "Browse"
      Height          =   375
      Left            =   5280
      TabIndex        =   2
      Top             =   480
      Width           =   1215
   End
   Begin MSComDlg.CommonDialog browseDialog 
      Left            =   4320
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox logBox 
      Height          =   2895
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      Top             =   3360
      Width           =   6375
   End
   Begin VB.CommandButton goBut 
      Caption         =   "Process Request"
      Height          =   495
      Left            =   3705
      TabIndex        =   0
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Label Label3 
      Caption         =   "Status:"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   3000
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "qbXML Request File"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   120
      Width           =   2055
   End
End
Attribute VB_Name = "UIForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
' Copyright � 2002 Intuit Inc. All rights reserved.
' Use is subject to the terms specified at:
'      http://developer.intuit.com/legal/devsite_tos.html
'
Option Explicit
'
'
'
Private Sub Command1_Click()
    ' Ensure that there's an input xml file.
    Dim fso As New FileSystemObject
    Dim InputFilename As String
    InputFilename = inputFile.Text
    If (False = fso.FileExists(InputFilename)) Then
        MsgBox ("Please select a qbXML Request File: ")
        Exit Sub
    End If
    
    Log.showURL (InputFilename)
End Sub

'
' This method is called by the VB runtime when the application is
' first started.
'
Private Sub Form_Load()
    Log.clear
End Sub
'
' This method is invoked when the user hits the Go button.
'
Private Sub goBut_Click()

    Log.clear

    ' Ensure that there's an input xml file.
    Dim fso As New FileSystemObject
    Dim InputFilename As String
    InputFilename = inputFile.Text
    If (False = fso.FileExists(InputFilename)) Then
        MsgBox ("Please select a qbXML Request File: ")
        Exit Sub
    End If
    
    ' Load the file into a string
    Dim xmldoc As String
    Const ForReading = 1
    Dim f As File
    Dim ts As TextStream
    Set f = fso.GetFile(InputFilename)
    Set ts = f.OpenAsTextStream(ForReading, TristateUseDefault)
    xmldoc = ts.ReadAll

    ' Send the request to QuickBooks and display the response.
    Dim response As String
    response = qbooks.post(xmldoc)
    If (response = "") Then
        Exit Sub
    End If
    Log.showXMLStream (response)

End Sub
'
' Vanilla Windows UI code for file browsing.
'
Private Sub browseButton_Click()
  On Error GoTo ErrorHandler

  ' Set CancelError to True
  browseDialog.CancelError = True
  
  ' Set filters
  browseDialog.Filter = "All Files (*.*)|*.*|qbXML Request Files" & _
        "(*.xml)|*.xml"
  
  ' Specify default filter
  browseDialog.FilterIndex = 2
  
  ' Set flags
  browseDialog.Flags = cdlOFNHideReadOnly
  
  ' Display the Open dialog box
  browseDialog.ShowOpen
  
  ' Set up the selected file in textBox
  inputFile.Text = browseDialog.FileName
  Exit Sub
  
ErrorHandler:
  Exit Sub
End Sub
