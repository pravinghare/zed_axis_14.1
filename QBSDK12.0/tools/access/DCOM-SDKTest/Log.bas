Attribute VB_Name = "Log"
'
' Copyright � 2002 Intuit Inc. All rights reserved.
' Use is subject to the terms specified at:
'      http://developer.intuit.com/legal/devsite_tos.html
'
Option Explicit
'
' Write to streaming log-message window.
'
Public Sub out(msg As String)
    UIForm.logBox.Text = msg & vbCrLf & UIForm.logBox.Text
End Sub
'
' Write to streaming log-message window.
'
Public Sub clear()
    UIForm.logBox.Text = ""
End Sub
'
' Display file in browser.
'
Public Sub showURL(iFname As String)
    Dim IE1 As New InternetExplorer
    IE1.Visible = True
    IE1.Navigate (iFname)
End Sub
'
' Save xml to a file and then display in browser.
'
Public Sub showXMLStream(xml As String)
    On Error GoTo errHandler
    
    Dim fname As String
    fname = App.Path & "\LastResponse.xml"
    Const ForWriting = 2
    Dim fso As New FileSystemObject
    Dim ts As TextStream
    Set ts = fso.CreateTextFile(fname, True)
    ts.Write (xml)
    showURL (fname)
    
    Exit Sub
    
errHandler:
    Log.out ("Error in showXMLStream" & vbCrLf & "HRESULT = " & Err.Number & " (" & Hex(Err.Number) & ") " & vbCrLf & Err.Description)
End Sub


