
This directory provides you with the qbXMLRPe.exe wrapper and sample usage of this COM exe component.
qbXMLRPe wrapper is an EXE COM wrapper around qbXMLRP.dll. 
qbXMLRPe allows an application to communicate with QuickBooks SDK when using DCOM, 
or when running under IIS, as in ASP or ASP.NET solutions.

You can read more info in the readme files for each subdirectory:

qbXMLRPe
========
This folder contains the redistributable qbXMLRPe.exe and sources to build your own wrapper,
if you need to provide one that contains your application certificate.

ASP
====
Contains a simple asp page that uses qbXMLRPe to show a list of Customers.

ASPDotNet
==========
Contains a Web .NET application that shows how to add a customer to QuickBooks. 
The sample shows how to mix QBFC and qbXMLRP calls, and it uses qbXMLRPe wrapper.

DCOM-SDKTest
============
Contains a modified version of SDK test sample, and shows how to use qbXMLRPe to call
the SDK from another machine using DCOM.




