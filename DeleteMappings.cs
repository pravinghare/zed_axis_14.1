﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class DeleteMappings : Form
    {
        private static DeleteMappings m_mappings;

        public static DeleteMappings getInstance()
        {
            

            if (m_mappings == null)
                m_mappings = new DeleteMappings();
            return m_mappings;
        }

        public void SetDialogBox(string message, MessageBoxIcon icon)
        {
            
        }

        public DeleteMappings()
        {            
            InitializeComponent();

            //this.labelMessage.Text = "Mapping is not valid for this file.";
            this.labelMessage.Text = "Axis has detected that import file structure has changed. This mapping may no longer be valid.";
            this.pictureBoxWarning.Image = System.Drawing.SystemIcons.Warning.ToBitmap();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        private void buttonContinue_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();

        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Retry;
            this.Close();
        }


       

    }
}
