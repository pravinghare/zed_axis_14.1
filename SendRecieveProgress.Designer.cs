namespace EDI.Message
{
    partial class SendRecieveProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendRecieveProgress));
            this.TabControlTasksErrrors = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LabelCount = new System.Windows.Forms.Label();
            this.LabelProgressRecieve = new System.Windows.Forms.Label();
            this.LabelProgressSend = new System.Windows.Forms.Label();
            this.LabelTimeRemRecieve = new System.Windows.Forms.Label();
            this.LabelTimeRemSend = new System.Windows.Forms.Label();
            this.LabelRecieveComplete = new System.Windows.Forms.Label();
            this.PictureBoxRecieve = new System.Windows.Forms.PictureBox();
            this.PictureBoxSend = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ProgressBarRecieve = new System.Windows.Forms.ProgressBar();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.LabelSendComplete = new System.Windows.Forms.Label();
            this.ProgressBarSend = new System.Windows.Forms.ProgressBar();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelStatus = new System.Windows.Forms.Label();
            this.backgroundWorkerRecieve = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerSend = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerMerge = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerEbayService = new System.ComponentModel.BackgroundWorker();
            this.labelTradingPartner = new System.Windows.Forms.Label();
            this.backgroundWorkerOsCommerceService = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerXCartService = new System.ComponentModel.BackgroundWorker();
			this.backgroundWorkerZenCart = new System.ComponentModel.BackgroundWorker();
            this.TabControlTasksErrrors.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxRecieve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxSend)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControlTasksErrrors
            // 
            this.TabControlTasksErrrors.Controls.Add(this.tabPage1);
            this.TabControlTasksErrrors.Location = new System.Drawing.Point(8, 19);
            this.TabControlTasksErrrors.Name = "TabControlTasksErrrors";
            this.TabControlTasksErrrors.SelectedIndex = 0;
            this.TabControlTasksErrrors.Size = new System.Drawing.Size(420, 206);
            this.TabControlTasksErrrors.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(412, 180);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Tasks";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.LabelCount);
            this.panel1.Controls.Add(this.LabelProgressRecieve);
            this.panel1.Controls.Add(this.LabelProgressSend);
            this.panel1.Controls.Add(this.LabelTimeRemRecieve);
            this.panel1.Controls.Add(this.LabelTimeRemSend);
            this.panel1.Controls.Add(this.LabelRecieveComplete);
            this.panel1.Controls.Add(this.PictureBoxRecieve);
            this.panel1.Controls.Add(this.PictureBoxSend);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.ProgressBarRecieve);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.menuStrip);
            this.panel1.Controls.Add(this.LabelSendComplete);
            this.panel1.Controls.Add(this.ProgressBarSend);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(403, 159);
            this.panel1.TabIndex = 0;
            // 
            // LabelCount
            // 
            this.LabelCount.AutoSize = true;
            this.LabelCount.Location = new System.Drawing.Point(201, 135);
            this.LabelCount.Name = "LabelCount";
            this.LabelCount.Size = new System.Drawing.Size(0, 13);
            this.LabelCount.TabIndex = 1;
            // 
            // LabelProgressRecieve
            // 
            this.LabelProgressRecieve.AutoSize = true;
            this.LabelProgressRecieve.Location = new System.Drawing.Point(227, 98);
            this.LabelProgressRecieve.Name = "LabelProgressRecieve";
            this.LabelProgressRecieve.Size = new System.Drawing.Size(0, 13);
            this.LabelProgressRecieve.TabIndex = 5;
            // 
            // LabelProgressSend
            // 
            this.LabelProgressSend.AutoSize = true;
            this.LabelProgressSend.Location = new System.Drawing.Point(227, 46);
            this.LabelProgressSend.Name = "LabelProgressSend";
            this.LabelProgressSend.Size = new System.Drawing.Size(0, 13);
            this.LabelProgressSend.TabIndex = 6;
            // 
            // LabelTimeRemRecieve
            // 
            this.LabelTimeRemRecieve.AutoSize = true;
            this.LabelTimeRemRecieve.Location = new System.Drawing.Point(324, 92);
            this.LabelTimeRemRecieve.Name = "LabelTimeRemRecieve";
            this.LabelTimeRemRecieve.Size = new System.Drawing.Size(0, 13);
            this.LabelTimeRemRecieve.TabIndex = 12;
            // 
            // LabelTimeRemSend
            // 
            this.LabelTimeRemSend.AutoSize = true;
            this.LabelTimeRemSend.Location = new System.Drawing.Point(325, 40);
            this.LabelTimeRemSend.Name = "LabelTimeRemSend";
            this.LabelTimeRemSend.Size = new System.Drawing.Size(0, 13);
            this.LabelTimeRemSend.TabIndex = 11;
            // 
            // LabelRecieveComplete
            // 
            this.LabelRecieveComplete.AutoSize = true;
            this.LabelRecieveComplete.Location = new System.Drawing.Point(144, 98);
            this.LabelRecieveComplete.Name = "LabelRecieveComplete";
            this.LabelRecieveComplete.Size = new System.Drawing.Size(57, 13);
            this.LabelRecieveComplete.TabIndex = 10;
            this.LabelRecieveComplete.Text = "Completed";
            this.LabelRecieveComplete.Visible = false;
            // 
            // PictureBoxRecieve
            // 
            this.PictureBoxRecieve.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxRecieve.Image")));
            this.PictureBoxRecieve.Location = new System.Drawing.Point(316, 82);
            this.PictureBoxRecieve.Name = "PictureBoxRecieve";
            this.PictureBoxRecieve.Size = new System.Drawing.Size(36, 36);
            this.PictureBoxRecieve.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PictureBoxRecieve.TabIndex = 9;
            this.PictureBoxRecieve.TabStop = false;
            this.PictureBoxRecieve.Visible = false;
            // 
            // PictureBoxSend
            // 
            this.PictureBoxSend.Image = ((System.Drawing.Image)(resources.GetObject("PictureBoxSend.Image")));
            this.PictureBoxSend.Location = new System.Drawing.Point(317, 31);
            this.PictureBoxSend.Name = "PictureBoxSend";
            this.PictureBoxSend.Size = new System.Drawing.Size(36, 36);
            this.PictureBoxSend.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PictureBoxSend.TabIndex = 8;
            this.PictureBoxSend.TabStop = false;
            this.PictureBoxSend.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Receiving";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = " Sending";
            // 
            // ProgressBarRecieve
            // 
            this.ProgressBarRecieve.Location = new System.Drawing.Point(121, 99);
            this.ProgressBarRecieve.Name = "ProgressBarRecieve";
            this.ProgressBarRecieve.Size = new System.Drawing.Size(100, 12);
            this.ProgressBarRecieve.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(296, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Remaining";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(137, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Progress";
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(403, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // LabelSendComplete
            // 
            this.LabelSendComplete.AutoSize = true;
            this.LabelSendComplete.Location = new System.Drawing.Point(144, 46);
            this.LabelSendComplete.Name = "LabelSendComplete";
            this.LabelSendComplete.Size = new System.Drawing.Size(57, 13);
            this.LabelSendComplete.TabIndex = 7;
            this.LabelSendComplete.Text = "Completed";
            this.LabelSendComplete.Visible = false;
            // 
            // ProgressBarSend
            // 
            this.ProgressBarSend.Location = new System.Drawing.Point(121, 46);
            this.ProgressBarSend.Name = "ProgressBarSend";
            this.ProgressBarSend.Size = new System.Drawing.Size(100, 12);
            this.ProgressBarSend.TabIndex = 3;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.TabControlTasksErrrors);
            this.GroupBox1.Location = new System.Drawing.Point(4, 40);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(442, 235);
            this.GroupBox1.TabIndex = 2;
            this.GroupBox1.TabStop = false;
            // 
            // LabelStatus
            // 
            this.LabelStatus.AutoSize = true;
            this.LabelStatus.Location = new System.Drawing.Point(24, 9);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(0, 13);
            this.LabelStatus.TabIndex = 3;
            // 
            // backgroundWorkerRecieve
            // 
            this.backgroundWorkerRecieve.WorkerReportsProgress = true;
            this.backgroundWorkerRecieve.WorkerSupportsCancellation = true;
            this.backgroundWorkerRecieve.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerRecieve_DoWork);
            this.backgroundWorkerRecieve.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerRecieve_RunWorkerCompleted);
            this.backgroundWorkerRecieve.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerRecieve_ProgressChanged);
            // 
            // backgroundWorkerSend
            // 
            this.backgroundWorkerSend.WorkerReportsProgress = true;
            this.backgroundWorkerSend.WorkerSupportsCancellation = true;
            this.backgroundWorkerSend.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerSend_DoWork);
            this.backgroundWorkerSend.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerSend_RunWorkerCompleted);
            this.backgroundWorkerSend.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerSend_ProgressChanged);
            // 
            // backgroundWorkerMerge
            // 
            this.backgroundWorkerMerge.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerMerge_DoWork);
            this.backgroundWorkerMerge.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerMerge_RunWorkerCompleted);
            // 
            // backgroundWorkerEbayService
            // 
            this.backgroundWorkerEbayService.WorkerReportsProgress = true;
            this.backgroundWorkerEbayService.WorkerSupportsCancellation = true;
            this.backgroundWorkerEbayService.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerEbayService_DoWork);
            this.backgroundWorkerEbayService.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerEbayService_RunWorkerCompleted);
            this.backgroundWorkerEbayService.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerEbayService_ProgressChanged);
            // 
            // labelTradingPartner
            // 
            this.labelTradingPartner.AutoSize = true;
            this.labelTradingPartner.Location = new System.Drawing.Point(163, 9);
            this.labelTradingPartner.Name = "labelTradingPartner";
            this.labelTradingPartner.Size = new System.Drawing.Size(0, 13);
            this.labelTradingPartner.TabIndex = 4;
            // 
            // backgroundWorkerOsCommerceService
            // 
            this.backgroundWorkerOsCommerceService.WorkerReportsProgress = true;
            this.backgroundWorkerOsCommerceService.WorkerSupportsCancellation = true;
            this.backgroundWorkerOsCommerceService.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerOsCommerceService_DoWork);
            this.backgroundWorkerOsCommerceService.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerOsCommerceService_RunWorkerCompleted);
            this.backgroundWorkerOsCommerceService.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerOsCommerceService_ProgressChanged);
            // 
            // backgroundWorkerXCartService
            // 
            this.backgroundWorkerXCartService.WorkerReportsProgress = true;
            this.backgroundWorkerXCartService.WorkerSupportsCancellation = true;
            this.backgroundWorkerXCartService.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerXCartService_DoWork);
            this.backgroundWorkerXCartService.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerXCartService_RunWorkerCompleted);
            this.backgroundWorkerXCartService.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerXCartService_ProgressChanged);
			
			// 
            // backgroundWorkerZenCart
            // 
            this.backgroundWorkerZenCart.WorkerReportsProgress = true;
            this.backgroundWorkerZenCart.WorkerSupportsCancellation = true;
            this.backgroundWorkerZenCart.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerZenCart_DoWork);
            this.backgroundWorkerZenCart.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerZenCart_RunWorkerCompleted);
            this.backgroundWorkerZenCart.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerZenCart_ProgressChanged);
			
            // 
            // SendRecieveProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(445, 271);
            this.Controls.Add(this.labelTradingPartner);
            this.Controls.Add(this.LabelStatus);
            this.Controls.Add(this.GroupBox1);
            this.MainMenuStrip = this.menuStrip;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(461, 309);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(461, 309);
            this.Name = "SendRecieveProgress";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zed Axis Send/Recieve Progress";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SendRecieveProgress_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SendRecieveProgress_FormClosing);
            this.TabControlTasksErrrors.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxRecieve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxSend)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabControlTasksErrrors;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox GroupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar ProgressBarRecieve;
        private System.Windows.Forms.ProgressBar ProgressBarSend;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelStatus;
        private System.Windows.Forms.Label LabelSendComplete;
        private System.Windows.Forms.PictureBox PictureBoxSend;
        private System.Windows.Forms.Label LabelRecieveComplete;
        private System.Windows.Forms.PictureBox PictureBoxRecieve;
        private System.Windows.Forms.Label LabelTimeRemRecieve;
        private System.Windows.Forms.Label LabelTimeRemSend;
        private System.Windows.Forms.Label LabelProgressRecieve;
        private System.Windows.Forms.Label LabelProgressSend;
        private System.ComponentModel.BackgroundWorker backgroundWorkerRecieve;
        private System.ComponentModel.BackgroundWorker backgroundWorkerSend;
        private System.ComponentModel.BackgroundWorker backgroundWorkerMerge;
        private System.Windows.Forms.Label LabelCount;
        private System.ComponentModel.BackgroundWorker backgroundWorkerEbayService;
        private System.Windows.Forms.Label labelTradingPartner;
        private System.ComponentModel.BackgroundWorker backgroundWorkerOsCommerceService;
        private System.ComponentModel.BackgroundWorker backgroundWorkerXCartService;
	    private System.ComponentModel.BackgroundWorker backgroundWorkerZenCart;
    }
}