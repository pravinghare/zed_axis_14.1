﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Streams
{
  public class BaseBankTransferList
    {
        #region Protected Members

        protected string m_TxnID;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected string m_TxnNumber;
        protected string m_TxnDate;
        protected string[] m_TransferFromAccountRef = new string[30000];
        protected decimal?[] m_FromAccountBalance = new decimal?[30000];
        protected string[] m_TransferToAccountRef= new string[30000];
        protected decimal?[] m_ToAccountBalance = new decimal?[30000];
        protected string[] m_ClassRef = new string[30000];
        protected decimal?[] m_Amount = new decimal?[30000];
        protected string m_Memo;
               #endregion
    }
}
