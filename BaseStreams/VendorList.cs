using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Vendor list.
    /// </summary>
    /// <summary>
    /// This class provides members of Vendor list.
    /// </summary>
    public class BaseVendorList
    {
        #region Private Member

        protected string[] m_ListID = new string[30000];
        protected string m_Name;
        protected string m_IsActive;
        protected string m_CompanyName;
        protected string m_Salutation;
        protected string m_FirstName;
        protected string m_MiddleName;
        protected string m_LastName;
        protected string m_VendorAddress;
        protected string m_VendorAddress1;
        protected string m_VendorAddress2;
        protected string m_VendorAddress3;
        protected string m_VendorAddress4;
        protected string m_VendorAddress5;
        protected string m_City;
        protected string m_State;
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_Note;
        protected string m_Phone;
        protected string m_AltPhone;
        protected string m_Fax;
        protected string m_Email;
        protected string m_Contact;
        protected string m_AltContact;
        protected string m_NameOnCheck;
        protected string m_AccountNumber;
        protected string m_Notes;
        protected string m_VendorTypeFullName;
        protected string m_TermFullName;
        protected string m_CreditLimit;
        protected string m_VendorTaxIndent;
        protected string m_IsVendorEligibleFor1099;
        protected string m_Balance;
        protected string m_BillingRateFullName;
        protected string m_ExternalGUID;
        protected string m_PrefillAccountRefFullName;
        protected string m_CurrencyRefFullName;
        protected string m_OwnerID;
        protected string m_DataExtName;
        protected string m_DataExtType;



        


        #endregion

    }
}
