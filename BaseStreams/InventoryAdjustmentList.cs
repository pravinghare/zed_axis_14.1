﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataProcessingBlocks.BaseStreams
{
    class InventoryAdjustmentList
    {
        #region Protected Members

        protected string m_TxnID;
        protected string m_CustomerRefFullName;
        protected string m_APAccountRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_ClassRefFullName;
        protected string m_Memo;
        protected string m_IsTaxIncluded;
        protected string m_SalesTaxCodeRefFullName;
        protected string m_IsPaid;
        protected string m_LinkedTxnID;
        protected decimal? m_OpenAmount;

        //For  InventoryAdjustmentLineRet,repeat
        protected string[] m_TxnLineID = new string[30000];
        protected string[] m_ItemRefFullName = new string[30000];
        protected string[] m_SerialNumber = new string[30000];
        protected string[] m_ExpSalesTaxFullName = new string[30000];
        protected string[] m_BillableStatus = new string[30000];
        protected string[] m_InventorySiteLocationRefFullName = new string[30000];
        protected decimal?[] m_QuantityDifference = new decimal?[30000];
        protected decimal?[] m_ValueDifference = new decimal?[30000];


        //For DataExtRet, repeat
        protected string[] m_DataExtName = new string[30000];
        protected string[] m_DataExtType = new string[30000];
        protected string[] m_DataExtValue = new string[30000];



        //protected decimal?[] m_Quantity = new decimal?[30000];
        //protected string[] m_UOM = new string[30000];
        //protected decimal?[] m_Cost = new decimal?[30000];
        //protected decimal?[] m_ItemAmount = new decimal?[30000];
        //protected string[] m_ItemCustomerRefFullName = new string[30000];
        //protected string[] m_ItemClassRefFullName = new string[30000];
        //protected string[] m_ItemSalesTaxFullName = new string[30000];
        //protected string[] m_ItemBillableStatus = new string[30000];
        //// axis 10.0 changes
        //protected string[] m_SerialNumber = new string[30000];
        //protected string[] m_LotNumber = new string[30000];
        //// axis 10.0 changes ends
        ////For ItemGroupLineRet
        //protected string[] m_GroupTxnLineID = new string[30000];
        //protected string[] m_ItemGroupRefFullName = new string[30000];

        ////For Sub ItemLineRet
        //protected string[] m_GroupLineTxnLineID = new string[30000];
        //protected string[] m_GroupLineItemRefFullName = new string[30000];
        //protected string[] m_GroupLineDesc = new string[30000];
        //protected decimal?[] m_GroupLineQuantity = new decimal?[30000];
        //protected string[] m_GroupLineUOM = new string[30000];
        //protected decimal?[] m_GroupLineCost = new decimal?[30000];
        //protected decimal?[] m_GroupLineAmount = new decimal?[30000];
        //protected string[] m_GroupLineCustomerRefFullName = new string[30000];
        //protected string[] m_GroupLineClassRefFullName = new string[30000];
        //protected string[] m_GroupLineSalesTaxFullName = new string[30000];
        //protected string[] m_GroupLineBillableStatus = new string[30000];

        //Axis Bug No 144  
        protected string m_LinkRefNumber;
        protected string m_TxnType;
        protected string m_LinkTxnDate;
        protected string m_LinkType;
        protected string m_LinkAmount;
        //End Axis bug no 144
        #endregion
    }
}
