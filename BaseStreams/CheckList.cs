using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseCheckList
    {
        #region Protected Members

        protected string m_TxnId;
        protected string m_AccountRefFullName;
        protected string m_PayeeEntityFullName;
        protected string m_RefNumber;
        protected string m_TxnDate;
        protected decimal? m_Amount;
        protected string m_CurrencyFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_AmountInHomeCurrency;
        protected string m_Memo;
        protected string m_Address1;
        protected string m_Address2;
        protected string m_Address3;
        protected string m_Address4;
        protected string m_Address5;
        protected string m_IsToBePrinted;
        protected string m_IsTaxIncluded;
        protected string m_LinkedTxnID;
        protected string m_LinkedRefNumber;

        //For ExpenseLineAdd
        protected List<string> m_ExpTxnLineID = new List<string>();
        protected List<string> m_ExpAccountFullName = new List<string>();
        protected List<decimal?> m_ExpAmount = new List<decimal?>();
        protected List<string> m_ExpMemo = new List<string>();
        protected List<string> m_ExpCustomerFullname = new List<string>();
        protected List<string> m_ExpClassFullName = new List<string>();
        protected List<string> m_ExpSalesTaxCodeFullName = new List<string>();
        protected List<string> m_ExpBillableStatus = new List<string>();
        protected List<string> m_ExpSalesRepRefFullName = new List<string>();
      
        
        //For ItemLineRet
        protected List<string> m_TxnLineID = new List<string>();
        protected List<string> m_ItemFullName = new List<string>();
        protected List<string> m_Desc = new List<string>();
        protected List<decimal?> m_Quantity = new List<decimal?>();
        protected List<string> m_UOM = new List<string>();
        protected List<decimal?> m_Cost = new List<decimal?>();
        protected List<decimal?> m_ItemAmount = new List<decimal?>();
        protected List<string> m_ItemCustomerFullName = new List<string>();
        protected List<string> m_ItemClassName = new List<string>();
        protected List<string> m_ItemSalesTaxCodeFullName = new List<string>();
        protected List<string> m_ItemBillableStatus = new List<string>();
        protected List<string> m_ItemSalesRepRefFullName = new List<string>();

        // axis 10.0 changes
        protected List<string> m_SerialNumber = new List<string>();
        protected List<string> m_LotNumber = new List<string>();
        // axis 10.0 changes ends
        //For ItemGroupLineRet
        protected List<string> m_GroupTxnLineId = new List<string>();
        protected List<string> m_ItemgroupFullName = new List<string>();
        
        //For Sub ItemGroupLineRet
        protected List<string> m_GroupLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected List<decimal?> m_GroupLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected List<decimal?> m_GroupLineCost = new List<decimal?>();
        protected List<decimal?> m_GroupLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineCustomerFullName = new List<string>();
        protected List<string> m_GroupLineClassName = new List<string>();
        protected List<string> m_GroupLineSalesTaxCodeFullName = new List<string>();
        protected List<string> m_GroupLineBillableStatus = new List<string>();
        protected List<string> m_GroupLineSalesRepRefFullName = new List<string>();

        #endregion
    }
}
