﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class provides the members of bill payment credit card.
    /// </summary>
    public class BaseBillPaymentCreditCardList
    {
        #region Protected Members
        protected string m_TxnId;
        protected string m_TxnNumber;
        protected string m_PayEntityRefFullName;
        protected string m_APAccountRefFullName;
        protected string m_TxnDate;
        protected string m_CreditCardAccountRefFullName;
        protected string m_Amount;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_AmountInHomeCurrency;
        protected string m_RefNumber;
        protected string m_Memo;
        //AppliedToTxnRet collection
        protected string[] m_ATRTxnID = new string[30000];
        protected string[] m_ATRRefNumber = new string[30000];
        //Axis Bug no. #292
        protected string[] m_ATRAmount;
        //End Axis Bug no. #292
        #endregion
    }
}
