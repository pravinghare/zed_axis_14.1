// ===============================================================================
// 
// NonInventoryItemList.cs
//
// This file contains the implementations of the NonInventory Item Base Class Members. 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Marissa Fernandes
// Date :27-06-2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for NonInventory Item List
    /// </summary>
    /// <summary>
    /// This method provide members of NonInventory items.
    /// </summary>
    public class BaseNonInventoryItemList
    {
        #region Private Members
        //Private members of NonInventory Itemlist.
        protected string m_Name;
        protected string m_ListId;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_DataExtName;
        protected string m_FullName;
        protected string m_BarCodeValue; //Axis10.2
        protected string m_ParentRefFullName;
        protected string m_Sublevel;
        protected string m_SublevelFullName;
        protected string m_ManufacturerPartNumber;
        protected string m_UnitOfMeasureSetRefFullName;
        protected string m_SalesTaxCodeRefFullName;
        protected string m_Desc;
        protected string m_Price;
        protected string m_AccountRefFullName;
        protected string m_SalesDesc;
        protected string m_SalesPrice;
        protected string m_IncomeAccountRefFullName;
        protected string m_PurchaseDesc;
        protected string m_PurchaseCost;
        protected string m_ExpenseAccountRefFullName;
        protected string m_PrefVendorRefFullName;
        protected string m_DatExtValue;
        protected string m_DatExtType;
        #endregion
    }
}
