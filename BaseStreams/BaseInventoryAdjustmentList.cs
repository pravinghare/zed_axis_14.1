﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Streams
{
    public class BaseInventoryAdjustmentList
    {
        //        InventoryAdjustmentRet
        //TxnID AccountRef InventorySiteRef
        //TxnDate 
        //RefNumber 
        //CustomerRef
        //ClassRef
        //Memo 
        //ExternalGUID 
        //InventoryAdjustmentLineRet
        //TxnLineID ItemRef SerialNumber  LotNumber 
        //InventorySiteLocationRef
        //QuantityDifference
        //ValueDifference

        protected string m_TxnId;
        protected string m_AccountRefFullName;
        protected string m_InventorySiteRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_CustomerRefFullName;
        protected string m_ClassRefFullName;
        protected string m_Memo;
        protected string m_ExternalGUID;

        protected List<string> m_InvenAdjLineTxnLineID = new List<string>();
        protected List<string> m_InvenAdjLineItemRef = new List<string>();
        protected List<string> m_InvenAdjLineSerialNumber = new List<string>();
        protected List<string> m_InvenAdjLineLotNumber = new List<string>();
        protected List<string> m_InvenAdjLineInvSiteLocRef = new List<string>();
        protected List<string> m_InvenAdjLineQuantityDifference = new List<string>();
        protected List<string> m_InvenAdjLineValueDifference = new List<string>();


    }
}
