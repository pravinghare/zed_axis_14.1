﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    //This class Provides members for Time Entry.
    public class BaseTimeEntry
    {
        #region Protected Members

        protected string m_TxnID;
        protected string m_TxnNumber;
        protected string m_TxnDate;
        protected string m_EntityFullName;
        protected string m_CustomerFullName;
        protected string m_ItemServiceFullName;
        protected string m_Duration;
        protected string m_ClassFullName;
        protected string m_PayrollItemWageFullName;
        protected string m_Notes;
        protected string m_BillableStatus;        

        //ItemServiceQuery ItemServicePrice
        protected decimal? m_ServiceItemPrice;

        #endregion
    }
}
