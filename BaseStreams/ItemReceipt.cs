using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseItemReceipt
    {
        #region Protected Members

        protected string m_purchaseOrdNumber;
        protected string m_supplier;
        protected string m_dateReceived;
        protected string m_product;
        protected int m_totalQunatityRec;
        protected string m_description;
        protected string m_remarks;

        #endregion
    }
}
