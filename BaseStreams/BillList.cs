using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class is used to declare Members of CheckList.
    /// </summary>
    public class BaseBillList
    {
        #region Protected Members

        protected string m_TxnID;
        protected string m_VendorFullName;
        protected string m_APAccountRefFullName;
        protected string m_TxnDate;
        protected string m_DueDate;
        protected decimal? m_AmountDue;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_AmountDueInHomeCurrency;
        protected string m_RefNumber;
        protected string m_TermsRefFullName;
        protected string m_Memo;
        protected string m_IsTaxIncluded;
        protected string m_SalesTaxCodeRefFullName;
        protected string m_IsPaid;
        protected string m_LinkedTxnID;
        protected decimal? m_OpenAmount;

        //For ExpenseLineRet,repeat
        protected List<string> m_ExpTxnLineID = new List<string>();
        protected List<string> m_AccountRefFullName = new List<string>();
        protected  List<decimal?> m_ExpAmount = new List<decimal?>();
        protected List<string> m_ExpMemo = new List<string>();
        protected List<string> m_ExpCustomerRefFullName = new List<string>();
        protected List<string> m_ExpClassRefFullName = new List<string>();
        protected List<string> m_ExpSalesTaxFullName = new List<string>();
        protected List<string> m_BillableStatus = new List<string>();
        protected List<string> m_ExpSalesRepRefFullName = new List<string>();


        //For ItemLineRet, repeat
        protected List<string> m_ItemTxnLineID = new List<string>();
        protected List<string> m_ItemRefFullName = new List<string>();
        protected List<string> m_Desc = new List<string>();
        protected  List<decimal?> m_Quantity = new List<decimal?>();
        protected List<string> m_UOM = new List<string>();
        protected  List<decimal?> m_Cost = new List<decimal?>();
        protected  List<decimal?> m_ItemAmount = new List<decimal?>();
        protected List<string> m_ItemCustomerRefFullName = new List<string>();
        protected List<string> m_ItemClassRefFullName = new List<string>();
        protected List<string> m_ItemSalesTaxFullName = new List<string>();
        protected List<string> m_ItemBillableStatus = new List<string>();
        protected List<string> m_ItemSalesRepRefFullName = new List<string>();
       
        
        // axis 10.0 changes
        protected List<string> m_SerialNumber = new List<string>();
        protected List<string> m_LotNumber = new List<string>();
        // axis 10.0 changes ends
        //For ItemGroupLineRet
        protected List<string> m_GroupTxnLineID = new List<string>();
        protected List<string> m_ItemGroupRefFullName = new List<string>();
       
        //For Sub ItemLineRet
        protected List<string> m_GroupLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemRefFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected  List<decimal?> m_GroupLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected  List<decimal?> m_GroupLineCost = new List<decimal?>();
        protected  List<decimal?> m_GroupLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineCustomerRefFullName = new List<string>();
        protected List<string> m_GroupLineClassRefFullName = new List<string>();
        protected List<string> m_GroupLineSalesTaxFullName = new List<string>();
        protected List<string> m_GroupLineBillableStatus = new List<string>();
        protected List<string> m_GroupLineSalesRepRefFullName = new List<string>();
        

         //Axis Bug No 144  
        protected string m_LinkRefNumber;
        protected string m_TxnType;
        protected string m_LinkTxnDate;
        protected string m_LinkType;
        protected string m_LinkAmount;
        //End Axis bug no 144
        #endregion
    }
}
