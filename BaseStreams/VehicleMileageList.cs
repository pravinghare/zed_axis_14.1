// ===============================================================================
// 
// VehicleMilage List.cs
//
// This file contains the implementations of the VehicleMilage Base Class Members. 
// Developed By : 
// Date : 29/1/2016
// Modified By : 
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace Streams
{
    /// <summary>
    /// Base class for VehicleMileage list.
    /// </summary>
    /// <summary>
    /// This class provides members of VehicleMileage list.
    /// </summary>
    public class BaseVehicleMileageList
    {
        #region protected Members
        //Adding protected member of Mileage.
        protected string m_TxnID = string.Empty;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;

        protected string[] m_TxnLineID = new string[10000];
        protected string m_VehicleRef = string.Empty;
        protected string m_VehicleRefFullName = string.Empty;
        protected string m_CustomerRef = string.Empty;
        protected string m_CustomerRefFullName = string.Empty;
        protected string m_ItemRef = string.Empty;
        protected string m_ItemRefFullName = string.Empty;
        protected string m_ClassRef = string.Empty;
        protected string m_ClassRefFullName= string.Empty;
        protected string m_TripStartDate = string.Empty;
        protected string m_TripEndDate = string.Empty;
        protected string m_OdometerStart = string.Empty;
        protected string m_OdometerEnd = string.Empty;
        protected string m_TotalMiles = string.Empty;
        protected string m_Notes = string.Empty;
        protected string m_BillableStatus = string.Empty;
       
        protected string m_StandardMileageRate= string.Empty;
        protected string m_StandardMileageTotalAmount = string.Empty;
        protected string m_BillableRate = string.Empty;
        protected string m_BillableAmount = string.Empty;
        
        #endregion
    }
}
