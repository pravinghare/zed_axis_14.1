// ===============================================================================
// 
// CustomerList.cs
//
// This file contains the implementations of the Customer Base Class Members. 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Customer list.
    /// </summary>
    /// <summary>
    /// This class provides members of Customer list.
    /// </summary>
    public class BaseCustomerList
    {
        #region Private Members
        //Adding Private member of Customer.
        protected char m_actionIndicator;
        protected string m_customerName;
        protected string m_customerListId;
        protected string m_companyName;
        protected string m_contactName;
        protected string m_phone;
        protected string m_addressLine1;
        protected string m_addressLine2;
        protected string m_addressLine4;
        protected string m_addressLine5;
        protected string m_city;
        protected string m_state;
        protected string m_postalCode;
        protected string m_country;
        protected string m_Note;

        //Axis 6.0 changes
        protected string m_CustomerFullName;
        protected string m_CustomerIsActive;
        protected string m_ParentRefFullName;
        protected string m_Sublevel;
        protected string m_Salutation;
        protected string m_FirstName;
        protected string m_MiddleName;
        protected string m_LastName;
        protected string m_addressLine3;
        protected string m_ShipAddr1;
        protected string m_ShipAddr2;
        protected string m_ShipCity;
        protected string m_ShipState;
        protected string m_ShipPostalCode;
        protected string m_ShipCountry;
        protected string m_ShipNote;
        protected string m_CustomerTypeRefFullName;
        protected string m_TermsRefFullName;
        protected string m_SalesRepRefFullName;
        protected string m_Balance;
        protected string m_TotalBalance;
        protected string m_SalesTaxCodeRefFullName;
        protected string m_ItemSalesTaxRefFullName;

        //Axis 10.0
        //protected string m_CreditCardNumber;
        //protected int m_ExpirationMonth;
        //protected int m_ExpirationYear;
        //protected string m_NameOnCard;
        //protected string m_CreditCardAddress;
        //protected string m_CreditCardPostalCode;

        protected string m_JobStatus;
        protected string m_JobStartDate;
        protected string m_JobProjectedEndDate;
        protected string m_JobEndDate;
        protected string m_JobTypeRefFullName;
        protected string m_Notes;
        protected string m_PriceLevelRefFullName;
        protected string m_CurrencyRefFullName;

        protected string m_AltPhone;
        protected string m_emailAddress;
        protected string m_Fax;
        protected string m_AltContact;
        protected string m_ResaleNumber;
        protected string m_AccountNumber;
        protected string m_CreditLimit;
        protected string m_PreferredPaymentMethodRefFullName;

        // bug 1428 Customer list export missing fields for CreditCardInfo

        protected string m_CreditCardNumber;
        protected int m_ExpirationMonth;
        protected int m_ExpirationYear;
        protected string m_NameOnCard;
        protected string m_CreditCardAddress;
        protected string m_CreditCardPostalCode;

        // 


        #endregion
       
    }
}
