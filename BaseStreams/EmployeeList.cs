using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Employee list.
    /// </summary>
    /// <summary>
    /// This class provides members of Employee list.
    /// </summary>
    public class BaseEmployeeList
    {
        #region Protected Members
        //Adding private member of Invoices.

        protected string[] m_ListID=new string[30000];
        protected string m_Name;
        protected string m_IsActive;
        protected string m_Salutation;
        protected string m_FirstName;
        protected string m_MiddleName;
        protected string m_LastName;
        protected string m_EmployeeAddress;
        protected string m_EmployeeAddress1;
        protected string m_EmployeeAddress2;
        protected string m_City;
        protected string m_State;
        //protected string m_Country;
        protected string m_PostalCode;
        protected string m_PrintAs;
        protected string m_Phone;
        protected string m_Mobile;
        protected string m_Pager;
        protected string m_PagerPin;
        protected string m_AltPhone;
        protected string m_Fax;
        protected string m_SSN;
        protected string m_Email;
        protected string m_EmployeeType;
        protected string m_Gender;
        protected string m_HiredDate;
        protected string m_ReleasedDate;
        protected string m_BirthDate;
        protected string m_AccountNumber;
        protected string m_Notes;

        protected string m_BillingRateFullName;
        protected string m_PayPeriod;
        protected decimal m_Rate;
        protected string m_ClassFullName;
        protected string m_ClearEarnings;
        protected string m_PayrollItemWageFullName;


        protected string m_IsUsingTimeDataToCreatePaychecks;
        protected string m_UseTimeDataToCreatePaychecks;

        protected string m_SickHours;
        protected string m_HoursAvailable;
        protected string m_AccrualPeriod;
        protected string m_HoursAccrued;
        protected string m_MaximumHours;
        protected string m_IsResettingHoursEachNewYear;
        protected string m_HoursUsed;
        protected string m_AccrualStartDate;



        protected string m_VacationHours;
        protected string m_VHoursAvailable;
        protected string m_VAccrualPeriod;
        protected string m_VHoursAccrued;
        protected string m_VMaximumHours;
        protected string m_VIsResettingHoursEachNewYear;
        protected string m_VHoursUsed;
        protected string m_VAccrualStartDate;

        protected string m_ExternalGUID;

        protected string m_OwnerID;
        protected string m_DataExtName;
        protected string m_DataExtType;

        
        #endregion


    }
}
