﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataProcessingBlocks
{
    //This class Provides Members of JournalEntry.
    public class BaseJournalEntry
    {
        #region Protected Methods

        protected string m_TxnId;
        protected string m_TxnNumber;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_IsAdjustment;
        protected string m_IsHomeCurrencyAdjustment;
        protected string m_IsAmountEnteredinHomeCurrency;
        protected string m_CurrencyFullName;
        protected decimal? m_ExchangeRate;
        
        //JournalDebitLine
        protected List<string> m_DLineTxnLineId = new List<string>();
        protected List<string> m_DLineAccountFullName = new List<string>();
        protected List<decimal?> m_DLineAmount = new List<decimal?>();
        protected List<string> m_DLineMemo = new List<string>();
        protected List<string> m_DLineEntityFullName = new List<string>();
        protected List<string> m_DLineClassFullName = new List<string>();
        protected List<string> m_DLineBillableStatus = new List<string>();
        protected List<string> m_DLineItemSalesTaxRefFullName = new List<string>();

        //JournalCreditLine
        protected List<string> m_CLineTxnlineId = new List<string>();
        protected List<string> m_ClineAccountFullName = new List<string>();
        protected List<decimal?> m_CLineAmount = new List<decimal?>();
        protected List<string> m_CLineMemo = new List<string>();
        protected List<string> m_CLineEntityFullName = new List<string>();
        protected List<string> m_CLineClassFullName = new List<string>();
        protected List<string> m_CLineBillableStatus = new List<string>();
        protected List<string> m_CLineItemSalesTaxRefFullName = new List<string>();

        #endregion
    }
}
