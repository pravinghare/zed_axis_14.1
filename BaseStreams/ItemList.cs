// ===============================================================================
// 
// ItemList.cs
//
// This file contains the implementations of the Item Base Class Members. 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Marissa Fernandes
// Date : 06-06-2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Item List
    /// </summary>
    /// <summary>
    /// This method provide members of items.
    /// </summary>
    public  class BaseItemList
    {
        #region Private Members
        //Private members of Itemlist.
        protected string m_Name;
        protected string m_ListId;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_DataExtName;

        protected string m_BarCodeValue; //Axis10.2

        protected char m_actionIndicator;
        protected string m_itemnumber;
        protected string m_itemname;
        protected string m_supplieritemnumber;
        protected string m_basesellingprice;
        protected string m_sellunitmeasure = "Each";
        //protected string m_unitmeasure = "????";
        protected string m_unitmeasure = "EACH";//For client bug no.559
        //protected string m_customfield1 = "9300657009995";       
        protected string m_customfield1;


        protected string m_ParentRefFullName;
        protected string m_SublevelFullName;
        protected string m_ManufacturerPartNumber;
        protected string m_UnitOfMeasureSetRefFullName;
        protected string m_SalesTaxCodeRefFullName;
      
        protected string m_IncomeAccountRefFullName;
       
        protected string m_PurchaseCost;
        protected string m_COGSAccountRefFullName;
        protected string m_PrefVendorRefFullName;
        protected string m_AssetAccountRefFullName;
        protected string m_ReorderPoint;
        protected string m_QuantityOnHand;
        protected string m_AverageCost;
        protected string m_QuantityOnOrder;
        protected string m_QuantityOnSalesOrder;
        protected string m_isactive;

        //Improvement::564
        protected string m_FullyQualifiedName;
        protected string m_SalesTaxIncluded;
        protected string m_PurchaseTaxIncluded;

        #endregion
    }
}
