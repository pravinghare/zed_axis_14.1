// ===============================================================================
// 
// ItemServiceList.cs
//
// This file contains the implementations of the Item Service Base Class Members. 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Marissa Fernandes
// Date : 27-06-2013
// ==============================================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This class is used for define Item Service members list.
    /// </summary>
    public class BaseItemServiceList
    {
        #region Private Members
        
        protected string m_ListId;
        protected string m_TimeCreated;
        protected string m_TimeModified;
       
        protected string m_Name;
        protected string m_FullName;
        protected string m_BarCodeValue; //Axis10.2
        protected string m_ParentRefFullName;
        protected string m_Sublevel;
     
        protected string m_UnitOfMeasureSetRefFullName;
        protected string m_IsTaxIncluded;
        protected string m_SalesTaxCodeRefFullName;
        protected string m_Desc;
        protected string m_Price;
        protected string m_AccountRefFullName;
        protected string m_SalesDesc;
        protected string m_SalesPrice;
        protected string m_SalesPricePercentage;
        protected string m_SalesAccountRefFullName;
        protected string m_IncomeAccountRefFullName;
        protected string m_PurchaseDesc;
        protected string m_PurchaseCost;
        protected string m_PurchaseTaxCodeRefFullName;
        protected string m_ExpenseAccountRefFullName;
        protected string m_PrefVendorRefFullName;
        protected string m_DataExtName;
        protected string m_DatExtValue;
        protected string m_DatExtType;

        #endregion
    }
}
