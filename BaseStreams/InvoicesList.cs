using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Streams
{
    /// <summary>
    /// This class provides members of Invoices.
    /// </summary>
    public class BaseInvoicesList
    {
        #region Protected Members
        //Adding private member of Invoices.
        //protected string m_CustomerListID;
        protected string m_TxnID;       
        protected string m_TxnNumber;
        protected string m_CustomerFullName;
        protected string m_CustomerListID;
        protected string m_ClassFullName;
        protected string m_ARAccountFullName;
        protected string m_TemplateFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_BillAddress;
        protected string m_BillAddress1;
        protected string m_BillAddress2;
        protected string m_BillAddress3;
        protected string m_BillAddress4;
        protected string m_City;
        protected string m_State;        
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_ShipAddress;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipAddress4;

        // Bug No. 398
        protected string m_ShipCity;
        protected string m_ShipState;
        protected string m_ShipPostalCode;

        //Axis 10.0
        protected string m_ShipAddress5;

        protected string m_ShipCountry;
        protected string m_IsPending;
        protected string m_IsFinanceCharge;
        protected string m_PONumber;
        protected string m_TermsFullName;
        protected string m_DueDate;
        protected string m_SalesRepFullName;
        protected string m_FOB;
        protected string m_ShipDate;
        protected string m_ShipMethodFullName;
        protected string m_Memo;
        protected string m_ItemSalesTaxFullName;
        protected string m_SalesTaxPercentage;
        protected decimal m_AppliedAmount;
        protected decimal m_BalanceRemaining;
        protected string m_CurrencyFullName;
        protected decimal m_ExchangeRate;
        protected decimal m_BalanceRemainingInHomeCurrency;
        protected string m_IsPaid;
        protected string m_CustomerMsgFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_IsTaxIncluded;
        protected string m_CustomerSalesTaxCodeFullName;
        protected decimal m_SuggestedDiscountAmount;
        protected string m_SuggestedDiscountDate;
        protected string m_LinkedTxnID;
        //For bug#1496
        protected string m_Other;
        protected List<string> m_Other1 = new List<string>();
        protected List<string> m_Other2 = new List<string>();
       
        //for export InvoiceLineRet;    
        protected List<string> m_QBTxnLineNumber = new List<string>();
        protected List<string> m_QBitemFullName = new List<string>();
        protected List<string> m_Desc = new List<string>();
        protected List<decimal?> m_QBquantity = new List<decimal?>();
        protected List<string> m_UOM = new List<string>();
        protected List<string> m_OverrideUOMFullName = new List<string>();
        protected List<string> m_ClassRefFullName = new List<string>();
        protected List<decimal?> m_Rate = new List<decimal?>();
        protected List<decimal?> m_Amount = new List<decimal?>();
        protected List<string> m_InventorySiteRefFullName = new List<string>();
        protected List<string> m_ServiceDate = new List<string>();
        protected List<string> m_SalesTaxCodeFullName = new List<string>();
        // axis 10.0 changes
        protected List<string> m_SerialNumber = new List<string>();
        protected List<string> m_LotNumber = new List<string>();
        // axis 10.0 changes ends
        //for export InvoiceLineGroupRet;
        protected List<string> m_GroupTxnLineID = new List<string>();
        protected List<string> m_ItemGroupFullName = new List<string>();
        protected List<string> m_GroupDesc = new List<string>();
        protected List<decimal?> m_GroupQuantity = new List<decimal?>();
        protected List<string> m_GroupUOM = new List<string>();
        protected List<string> m_IsPrintItemsInGroup = new List<string>();

        //Sub InvoiceLineRet;
        protected List<string> m_GroupQBTxnLineNumber = new List<string>();
        protected List<string> m_GroupLineItemFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected List<decimal?> m_GroupLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected List<string> m_GroupLineOverrideUOM = new List<string>();
        protected List<string> m_GroupLineClassRefFullName = new List<string>();
        protected List<decimal?> m_GroupLineRate = new List<decimal?>();
        protected List<decimal?> m_GroupLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineServiceDate = new List<string>();
        protected List<string> m_GroupLineSalesTaxCodeFullName = new List<string>();
        ///616
        //protected List<string> m_LineOther1 = new List<string>();
        protected List<string> m_LineOther1 = new List<string>();
        //protected List<string> m_LineOther2 = new List<string>();
        protected List<string> m_LineOther2 = new List<string>();
        
        protected Collection<Streams.QBItemDetails> m_ItemDetails;
      
        #endregion
    }
}
