using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Streams
{
    /// <summary>
    /// This class provide members of Credit Memo.
    /// </summary>
    public class BaseCreditMemoList
    {
        #region Protected Methods

        protected string m_TxnID;
        protected string m_CustomerRefFullName;
        protected string m_ClassRefFullName;
        protected string m_ARAccountRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_BillAddress;
        protected string m_BillAddress1;
        protected string m_BillAddress2;
        protected string m_BillAddress3;
        protected string m_BillAddress4;
        protected string m_Country;
        protected string m_ShipAddress;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipAddress4;
        protected string m_ShipCountry;
        protected string m_IsPending;
        protected string m_PONumber;
        protected string m_TermsRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;
        protected string m_FOB;
        protected string m_Shipdate;
        protected string m_ShipMethodRefFullName;
        protected string m_ItemSalesTaxRefFullName;
        protected string m_SalesTaxPercentage;
        protected decimal? m_TotalAmount;
        protected decimal? m_CreditRemaining;
        protected string m_CurrencyRefFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_CreditRemainingInHomeCurrency;
        protected string m_Memo;
        protected string m_CustomerMsgRefFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_IsTaxIncluded;
        protected string m_CustomerSalesTaxCodeRefFullName;
        protected string m_Other;
        protected string m_LinkedTxnID;

        //CreditMemoLineRet
        protected List<string> m_TxnLineID = new List<string>();
        protected List<string> m_ItemRefFullName = new List<string>();
        protected List<string> m_desc = new List<string>();
        protected List<decimal?> m_Quantity = new List<decimal?>();
        protected List<string> m_UOM = new List<string>();
        protected List<decimal?> m_Rate = new List<decimal?>();
        protected List<decimal?> m_Amount = new List<decimal?>();
        protected List<string> m_InventorySiteRefFullName = new List<string>();
        protected List<string> m_ServiceDate = new List<string>();
        protected List<string> m_SalesTaxCodeRefFullName = new List<string>();
        // axis 10.0 changes
        protected List<string> m_SerialNumber = new List<string>();
        protected List<string> m_LotNumber = new List<string>();
        // axis 10.0 changes ends
        //CreditMemoLineGroupRet
        protected List<string> m_GroupTxnLineID = new List<string>();
        protected List<string> m_GroupItemFullName = new List<string>();        

        //Sub CreditMemoLineRet
        protected List<string> m_GroupLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected List<decimal?> m_GroupLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected List<decimal?> m_GroupLineRate = new List<decimal?>();
        protected List<decimal?> m_GroupLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineServiceDate = new List<string>();
        protected List<string> m_GroupLineSalesTaxCodeFullName = new List<string>();

        //other fields
       
        protected string m_Other1;
        protected string m_Other2;

        protected List<string> m_GroupLineOther1 = new List<string>();
        protected List<string> m_GroupLineOther2 = new List<string>();

        protected List<string> m_Custom = new List<string>();
        protected Collection<QBItemDetails> m_QBItemsDetails;

        #endregion
    }
}
