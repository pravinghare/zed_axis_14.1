// ===============================================================================
// Axis 9.0
// BaseEstimateList.cs
//
// This file contains the implementations of the Estimate Base Class Members. 
//
// Added By : Akanksha.
// ===============================================================================


using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseEstimateList
    {

        #region Protected Members
     
        protected string m_TxnId;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected int m_TxnNumber;
        protected string m_customerRefFullName;
        protected string m_customerListID;
        protected string m_ClassRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_billAddr1;
        protected string m_billAddr2;
        protected string m_billAddr3;
        protected string m_billAddr4;
        protected string m_billAddr5;
        protected string m_billCity;
        protected string m_billState;
        protected string m_billPostalCode;
        protected string m_billCountry;
        protected string m_billNote;
        protected string m_billAddrBlock1;
        protected string m_billAddrBlock2;
        protected string m_billAddrBlock3;
        protected string m_billAddrBlock4;
        protected string m_billAddrBlock5;
        protected string m_shipAddress1;
        protected string m_shipAddress2;
        protected string m_shipAddress3;
        protected string m_shipAddress4;
        protected string m_shipAddress5;
        protected string m_shipAddressCity;
        protected string m_shipAddressState;
        protected string m_shipAddressPostalCode;
        protected string m_shipAddressCountry;
        protected string m_shipAddressNote;
        protected string m_shipAddrBlock1;
        protected string m_shipAddrBlock2;
        protected string m_shipAddrBlock3;
        protected string m_shipAddrBlock4;
        protected string m_shipAddrBlock5;
        protected string m_IsActive;
        protected string m_poNumber;
        protected string m_TermsRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;
        protected string m_FOB;
        protected string m_SubTotal;
        protected string m_ItemSalesTaxRefFullName;
        protected string m_SalesTaxPercentage;
        protected string m_SalesTaxTotal;
        protected string m_TotalAmount;
        protected string m_CurrencyRefFullName;
        protected string m_ExchangeRate;
        protected string m_TotalAmountInHomeCurrency;
        protected string m_memo;
        protected string m_CustomerMsgRefFullName;
        protected string m_IsToBeEmailed;
        protected string m_CustomerSalesTaxCodeRefFullName;
        protected string m_Other;
        protected string m_Other1;
        protected string m_Other2;
        protected string m_externalGUID;
        protected string m_txnType;
        protected string m_linkType;
        protected decimal m_amount;
        protected string m_ownerId;
        protected string m_extName;
        protected string m_dataExtType;
      //  protected string m_dataExtValue;
        protected string m_isTaxIncluded;




        //For EstimateLineRet,repeat
        //Bug 11(Axis 10.1 Changes).
        protected List<string> m_QBTxnLineNumber = new List<string>();
        protected List<string> m_EstItemRefFullName = new List<string>();
        protected List<string> m_EstDesc = new List<string>();
        protected List<decimal?> m_EstQuantity = new List<decimal?>();
        protected List<string> m_EstUOM = new List<string>();
        protected List<string> m_EstOverrideUOMSetRefFullName = new List<string>();
        protected List<decimal?> m_EstRate = new List<decimal?>();
        protected List<string> m_EstClassRefFullName = new List<string>();
        protected List<decimal?> m_EstAmouunt = new List<decimal?>();
        protected List<string> m_EstInventorySiteRefFullName = new List<string>();
        protected List<string> m_EstSalesTaxCodeRefFullName = new List<string>();
        protected List<decimal?> m_EstMarkUpRate = new List<decimal?>();
        protected List<decimal?> m_EstMarkUpRatePercent = new List<decimal?>();
        protected List<string> m_EstOther1 = new List<string>();
        protected List<string> m_EstOther2 = new List<string>();

        //For EstimateLineGroupRet,repeat
        protected List<string> m_EstGroupTxnLineID = new List<string>();
        protected List<string> m_EstGroupItemRefFullName = new List<string>();
        protected List<string> m_EstGroupDesc = new List<string>();
        protected List<decimal?> m_EstGroupQuantity = new List<decimal?>();
        protected List<string> m_EstGroupUOM = new List<string>();
        protected List<string> m_EstGroupOverrideUOMSetRefFullName = new List<string>();
        protected List<bool> m_EstGroupIsPrintItemInGroup;
        protected List<decimal?> m_EstGroupTotalAmount = new List<decimal?>();

        //For  Sub EstimateLineRet,repeat
        protected List<string> m_EstGroupLineTxnLineID = new List<string>();
        protected List<string> m_EstGroupLineItemRefFullName = new List<string>();
        protected List<string> m_EstGroupLineDesc = new List<string>();
        protected List<decimal?> m_EstGroupLineQuantity = new List<decimal?>();
        protected List<string> m_EstGroupLineUOM = new List<string>();
        protected List<string> m_EstGroupLineOverrideUOMSetRefFullName = new List<string>();
        protected List<decimal?> m_EstGroupLineRate = new List<decimal?>();
        protected List<string> m_EstGroupLineClassRefFullName = new List<string>();
        protected List<decimal?> m_EstGroupLineAmouunt = new List<decimal?>();
        protected List<string> m_EstGroupLineInventorySiteRefFullName = new List<string>();
        protected List<string> m_EstGroupLineSalesTaxCodeRefFullName = new List<string>();
        protected List<decimal?> m_EstGroupLineMarkUpRate = new List<decimal?>();
        protected List<decimal?> m_EstGroupLineMarkUpRatePercent = new List<decimal?>();
        protected List<string> m_EstGroupLineOther1 = new List<string>();
        protected List<string> m_EstGroupLineOther2 = new List<string>();



       

                
        #endregion
    }
}
