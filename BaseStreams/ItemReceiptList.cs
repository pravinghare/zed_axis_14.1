﻿// ===============================================================================
// 
// item Receipt List.cs
//
// This file contains the implementations of the Item ReceiptList Base Class Members. 
// Developed By : 
// Date : 10/2/2016
// Modified By : 
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseItemReceiptList
    {
        #region protected Members
        //Adding protected member of Mileage.
        protected string m_TxnID;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_TxnNumber;
        protected string m_VenderRef;
        protected string m_VenderRefFullName;
        protected string m_APAccountRef;
        protected string m_APAccountRefFullName;
        protected string m_LiabilityAccount;
        protected string m_LiabilityAccountRefFullName;
        protected string m_TxnDate;
        protected string m_TotalAmount;
        protected string m_CurrencyRef;
        protected string m_CurrencyRefFullName;
        protected string m_ExchangeRate;
        protected string m_TotalAmountInHomeCurrency;
        protected string m_RefNumber;
        protected string m_Memo;
        protected string m_ExternalGUID;
       // protected string m_OpenAmount;
        protected List<string> m_DataExtOwnerID = new List<string>();
        protected List<string> m_DataExtName = new List<string>();
        protected List<string> m_DataExtType = new List<string>();
        protected List<string> m_DataExtValue = new List<string>();
        

        //Linked transaction
        protected string m_LinkedTxnTxnID;
        protected string m_LinkedTxnTxnDate;
        protected string m_LinkedTxnTxnType;
        protected string m_LinkedTxnRefNumber;
        protected string m_LinkedTxnLinkType ;
        protected string m_LinkedTxnAmount;
        
        //Expense Line Ret

        //For ExpenseLineRet,repeat
        protected List<string> m_ExpensesLineTxnLineID = new List<string>();
        protected List<string> m_ExpensesLineAccountRefFullName = new List<string>();
        protected List<decimal?> m_ExpensesLineAmount = new List<decimal?>();
        protected List<string> m_ExpensesLineMemo = new List<string>();
        protected List<string> m_ExpensesLineCustomerRefFullName = new List<string>();
        protected List<string> m_ExpensesLineClassRefFullName = new List<string>();
        protected List<string> m_ExpensesLineSalesTaxFullName = new List<string>();
        protected List<string> m_ExpensesLineBillableStatus = new List<string>();
        protected List<string> m_ExpensesLineSalesRepRefFullName = new List<string>();
        protected List<string> m_ExpensesLineDataExtOwnerID = new List<string>();
        protected List<string> m_ExpensesLineDataExtDataExtName = new List<string>();
        protected List<string> m_ExpensesLineDataExtDataExtType = new List<string>();
        protected List<string> m_ExpensesLineDataExtDataExtValue = new List<string>();



        //For ItemLineRet, repeat
        protected List<string> m_ItemLineRet = new List<string>();
        protected List<string> m_ItemLineTxnLineID = new List<string>();
        protected List<string> m_ItemLineRefFullName = new List<string>();
        protected List<string> m_ItemLineInventorySiteLocationRefFullName = new List<string>();
        protected List<string> m_ItemLineSerialNumber = new List<string>();
        protected List<string> m_ItemLineLotNumber = new List<string>();
        protected List<string> m_ItemLineDesc = new List<string>();
        protected List<decimal?> m_ItemLineQuantity = new List<decimal?>();
        protected List<string> m_ItemLineUOM = new List<string>();
        protected List<string> m_ItemLineOverrideUOMSetRefFullName = new List<string>();
        protected List<decimal?> m_ItemLineCost = new List<decimal?>();
        protected List<decimal?> m_ItemLineAmount = new List<decimal?>();
        protected List<string> m_ItemLineCustomerRefFullName = new List<string>();
        protected List<string> m_ItemLineClassRefFullName = new List<string>();
        protected List<string> m_ItemLineSalesRepRefFullName = new List<string>();
        protected List<string> m_ItemLineBillableStatus = new List<string>();
        protected List<string> m_ItemLineDataExtOwnerID = new List<string>();
        protected List<string> m_ItemLineDataExtDataExtName = new List<string>();
        protected List<string> m_ItemLineDataExtDataExtType = new List<string>();
        protected List<string> m_ItemLineDataExtDataExtValue = new List<string>();
        // axis 10.0 changes


        //For ItemGroupLineRet

        protected List<string> m_ItemGroupLineRet = new List<string>();
        
        protected List<string> m_GroupLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemGroupRefFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected List<string> m_GroupLineQuantity = new List<string>();
        protected List<string> m_GroupLineUnitOfMeasure = new List<string>();
        protected List<string> m_GroupLineOverrideUOMSetRefFullName = new List<string>();
        protected List<decimal?> m_GroupLineTotalAmount = new List<decimal?>();

        ////For Sub ItemLineRet inside ItemGroupLineRet
        //protected List<string> m_GroupLineItemLineTxnLineID = new List<string>();
        //protected List<string> m_GroupLineItemLineItemGroupRefFullName = new List<string>();
        //protected List<string> m_GroupLineItemLineDesc = new List<string>();
        //protected List<decimal?> m_GroupLineItemLineQuantity = new List<decimal?>();
        //protected List<string> m_GroupLineItemLineUOM = new List<string>();
        
        //protected List<string> m_GroupLineCustomerRefFullName = new List<string>();
        //protected List<string> m_GroupLineClassRefFullName = new List<string>();
        //protected List<string> m_GroupLineSalesTaxFullName = new List<string>();
        //protected List<string> m_GroupLineBillableStatus = new List<string>();

        // groupline -->  itemLine repeated
        protected List<string> m_GroupLineItemLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemLineItemRefFullName = new List<string>();
        protected List<string> m_GroupLineItemLineInventorySiteLocationRefFullName = new List<string>();
        protected List<string> m_GroupLineItemLineSerialNumber = new List<string>();
        protected List<string> m_GroupLineItemLineLotNumber = new List<string>();
        protected List<string> m_GroupLineItemLineDesc = new List<string>();
        protected List<decimal?> m_GroupLineItemLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineItemLineUOM = new List<string>();
        protected List<string> m_GroupLineItemLineOverrideUOMSetRefFullName = new List<string>();
        protected List<decimal?> m_GroupLineItemLineCost = new List<decimal?>();
        protected List<decimal?> m_GroupLineItemLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineItemLineCustomerRefFullName = new List<string>();
        protected List<string> m_GroupLineItemLineClassRefFullName = new List<string>();
        protected List<string> m_GroupLineItemLineSalesRepRefFullName = new List<string>();
        protected List<string> m_GroupLineItemLineBillableStatus = new List<string>();
        protected List<string> m_GroupLineItemLineDataExtOwnerID = new List<string>();
        protected List<string> m_GroupLineItemLineDataExtDataExtName = new List<string>();
        protected List<string> m_GroupLineItemLineDataExtDataExtType = new List<string>();
        protected List<string> m_GroupLineItemLineDataExtDataExtValue = new List<string>();
        
        #endregion
    }
}
