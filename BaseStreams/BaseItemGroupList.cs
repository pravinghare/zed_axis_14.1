﻿// ===============================================================================
// 
// item Receipt List.cs
//
// This file contains the implementations of the Item ReceiptList Base Class Members. 
// Developed By : 
// Date : 10/2/2016
// Modified By : 
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    public class BaseItemGroupList
    {
        #region protected Members
        //Adding protected member of ItemGroupRet

        protected string[] m_ItemGroupRet = new string[30000];
        protected string m_ListID;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected string m_Name;
        protected string m_BarCodeValue;
        protected string m_IsActive;
        protected string m_ItemDesc;

        //UnitOfMesssureSetRef

        protected string m_UnitOfMessureSetRefFullName;

        protected string m_IsPrintItemsInGroup;
        protected string m_SpecialItemType;
        protected string m_ExternalGUID;

        //For ItemGroupLine
        protected string m_ItemgroupLine;
        protected string m_ItemRef;
        protected string[] m_ItemGroupLine = new string[30000];
        protected string[] m_ItemGroupLineItemRefListID = new string[30000];
        protected string[] m_ItemGroupLineItemRefFullName = new string[30000];
        protected string[] m_ItemGroupLineQuantity = new string[30000];
        protected string[] m_ItemGroupLineUnitOfMeasure = new string[30000];

        
        //for DataExtRet
        protected string[] m_DataExtOwnerID = new string[30000];
        protected string[] m_DataExtName = new string[30000];
        protected string[] m_DataExtType = new string[30000];
        protected string[] m_DataExtValue = new string[30000];
      
      
        protected string m_UnitOfMeasure;
        protected string m_Quantity;
        protected string m_FullName;
        protected string m_ItemGroupRef;
        protected string m_DataExtRet;

        #endregion
    }
}
