﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This Class is used to provide members of Price Level.
    /// </summary>
    public class BasePriceLevelList
    {
        #region Protected Members

        //For PriceLevelRet
        protected string m_ListID;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected string m_Name;
        protected string m_IsActive;
        protected string m_PriceLevelType;
        protected string m_PriceLevelFixedPercentage;

        //For PriceLevelPerItemRet
        protected string[] m_LineID = new string[30000];
        protected string[] m_ItemRefFullName = new string[30000];
        protected decimal?[] m_CustomPrice = new decimal?[30000];
        protected string[] m_CustomPricePer = new string[30000];
        protected string m_CurrencyRefFullName;

        #endregion

    }
}
