﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Streams
{
    public class BaseVendorCreditList
    {
        #region Protected members

        protected string m_TxnId;
        protected string m_VendorRefFullName;
        protected string m_APAccountFullName;
        protected string m_TxnDate;
        protected decimal? m_CreditAmount;
        protected string m_CurrencyFullName;
        protected decimal? m_ExchangeRate;
        protected decimal? m_CreditAmountInHomeCurrency;
        protected string m_RefNumber;
        protected string m_Memo;
        protected string m_ExternalGUID;

        //For ExpenseLineRet
        protected List<string> m_ExpTxnLineID = new List<string>();
        protected List<string> m_ExpAccountFullName = new List<string>();
        protected List<decimal?> m_ExpAmount = new List<decimal?>();
        protected List<string> m_ExpMemo = new List<string>();
        protected List<string> m_ExpCustomerFullname = new List<string>();
        protected List<string> m_ExpClassFullName = new List<string>();
        protected List<string> m_ExpBillableStatus = new List<string>();
        protected List<string> m_ExpSalesRepRefFullName = new List<string>();


        //For ItemLineRet 
        protected List<string> m_TxnLineID = new List<string>();
        protected List<string> m_ItemFullName = new List<string>();
        protected List<string> m_Desc = new List<string>();
        protected List<decimal?> m_Quantity = new List<decimal?>();
        protected List<string> m_UOM = new List<string>();
        protected List<decimal?> m_Cost = new List<decimal?>();
        protected List<decimal?> m_ItemAmount = new List<decimal?>();
        protected List<string> m_ItemCustomerFullName = new List<string>();
        protected List<string> m_ItemClassName = new List<string>();
        protected List<string> m_ItemBillableStatus = new List<string>();
        protected List<string> m_ItemSalesRepRefFullName = new List<string>();


        // axis 10.0 changes
        protected List<string> m_SerialNumber = new List<string>();
        protected List<string> m_LotNumber = new List<string>();
        // axis 10.0 changes ends
        //For group ItemLineRet
        protected List<string> m_GroupTxnLineId = new List<string>();
        protected List<string> m_ItemgroupFullName = new List<string>();

        //For sub ItemLineRet
        protected List<string> m_GroupLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected List<decimal?> m_GroupLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected List<decimal?> m_GroupLineCost = new List<decimal?>();
        protected List<decimal?> m_GroupLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineCustomerFullName = new List<string>();
        protected List<string> m_GroupLineClassName = new List<string>();
        protected List<string> m_GroupLineSalesRepRefFullName = new List<string>();
        protected List<string> m_GroupLineBillableStatus = new List<string>();


        #endregion
    }
}
