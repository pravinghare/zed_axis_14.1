using System;
using System.Collections.Generic;
using System.Text;
using Streams;
using System.Xml.Serialization;
using System.Collections;
using QuickBookEntities;
using System.Collections.ObjectModel;
using FinancialEntities;

namespace Streams
{

    public class SalesOrder
    {
        #region Private Members
        private string m_TxnDate;
        private string m_RefNumber;
        private CustomerRef m_CustomerRef;
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        //private PaymentMethodRef m_PaymentMethodRef;
        private string m_ShipDate;
        //private ShipMethodRef m_ShipMethodRef;
        private string m_Memo;
        private string m_Other;
        private Collection<FinancialEntities.SalesOrderLineAdd> m_SalesOrderLineAdd = new Collection<FinancialEntities.SalesOrderLineAdd>();
        #endregion

        #region Public Properties

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        /// <summary>
        /// Get or Set TxnDate of SalesOrder.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }

            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        /// <summary>
        /// Get or Set Ref number of Sales Order.
        /// </summary>
        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }


        /// <summary>
        /// Get or Set ShipAddress of SalesOrder.
        /// </summary>
        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }


        /// <summary>
        /// Get or Set Payment Method Full Name.
        /// </summary>
        //public PaymentMethodRef PaymentMethodRef
        //{
        //    get
        //    {
        //        return m_PaymentMethodRef;
        //    }
        //    set
        //    {
        //        m_PaymentMethodRef = value;
        //    }
        //}

        /// <summary>
        /// Get or Set Ship Date of Sales Order.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string ShipDate
        {

            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_ShipDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_ShipDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_ShipDate = value;
            }
        }

        /// <summary>
        /// Get or Set Ship Method Full Name.
        /// </summary>
        //public ShipMethodRef ShipMethodRef
        //{
        //    get { return m_ShipMethodRef; }
        //    set { m_ShipMethodRef = value; }
        //}

        /// <summary>
        /// Get or Set Memo of Sales Order.
        /// </summary>
        public string Memo
        {
            get
            {
                return m_Memo;
            }
            set
            {
                m_Memo = value;
            }
        }

        /// <summary>
        /// Get or Set Others of Sales Order.
        /// </summary>
        public string Other
        {
            get { return m_Other; }
            set { m_Other = value; }
        }

        /// <summary>
        /// Get or Set Sales Receitp Line Add.
        /// </summary>
        [XmlArray("SalesOrderLineAddREM")]
        public Collection<FinancialEntities.SalesOrderLineAdd> SalesOrderLineAdd
        {
            get { return m_SalesOrderLineAdd; }
            set { m_SalesOrderLineAdd = value; }
        }

        #endregion

        #region  Constructor

        public SalesOrder()
        {
            //m_vendorRef = new VendorRef();
            m_ShipAddress = new Collection<ShipAddress>();
            //m_PaymentMethodRef = new PaymentMethodRef();
            //m_ShipMethodRef = new ShipMethodRef();
            m_CustomerRef = new CustomerRef();
            m_SalesOrderLineAdd = new Collection<FinancialEntities.SalesOrderLineAdd>();
        }

        #endregion
    }
}
