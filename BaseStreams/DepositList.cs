// ===============================================================================
// 
// DepositList.cs
//
// This file contains the implementations of the Deposit Base Class Members. 
// Developed By : Mrudul Ganpule.
// Date : 
// Modified By : Mrudul Ganpule.
// Date : 
// ==============================================================================
using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Deposit list.
    /// </summary>
    /// <summary>
    /// This class provides members of Deposit list.
    /// </summary>
    public class BaseDepositList
    {
        #region Private Members
        //Adding Private member of Deposit.
       
        protected string m_TxnID = string.Empty;
        protected string m_TimeCreated = string.Empty;
        protected string m_TimeModified = string.Empty;
        protected string m_editsequence = string.Empty;
        protected string m_txndate = string.Empty;
        protected string m_DepositToAccountRefFullName = string.Empty;
        protected string m_CurrencyRefFullName = string.Empty;
        protected string[] m_AccountRefFullName = new string[10000];
        protected string m_Memo = string.Empty;
        protected string[] m_EntityRefListID = new string[10000];
        protected string[] m_EntityRefFullName = new string[10000];
        protected decimal?[] m_CheckNumber = new decimal?[10000];
        protected string[] m_PaymentMethodRefFullName = new string[10000];
        protected string[] m_ClassRefFullName = new string[10000];
        protected string m_DepositTotal = string.Empty;
        protected string m_ExchangeRate = string.Empty;
        protected string m_DepositTotalInHomeCurrency = string.Empty;
        protected string[] m_TxnLineID = new string[10000];
        protected string[] m_DepositTxnLineID = new string[10000];
        protected string[] m_QBitemFullName = new string[10000];
        protected string[] m_cashbackmemo = new string[10000];
        protected decimal?[] m_Amount = new decimal?[10000];
        protected decimal?[] m_depositAmount = new decimal?[10000];  
        #endregion       
    }
}
