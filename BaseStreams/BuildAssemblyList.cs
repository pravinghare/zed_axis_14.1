﻿using QuickBookEntities;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace Streams
{
    public class BaseBuildAssemblyList
    {
        #region Protected Members
        protected string m_TxnID;
        protected string m_TxnNumber;
        protected string m_ItemInventoryAssemblyRefFullName;
         protected string m_TxnDate;
         protected string m_RefNumber;
        protected bool m_IsPending;

        protected decimal? m_QuantityToBuild;
        protected string m_QuantityCanBuild;
        protected string m_QuantityOnHand;
        protected string m_QuantityOnSalesOrder;
        protected string m_ComponentItemLineRet;
        protected string []m_ItemRefFullName;
        protected string []m_ItemRefDesc;
        protected string []m_ItemRefQuantityOnHand;
        protected string []m_ItemRefQuantityNeeded;
        protected string [] m_ItemRefInventorySiteLocationRef;
        protected string [] m_ItemRefInventorySiteRef;
        protected string [] m_ItemRefLotNumber;
        protected string [] m_ItemRefSerialNumber;

        protected string []m_FullName;
        protected string []m_Name;
        protected string[] m_TxnLineID;
        protected string m_Memo; 
        protected string m_InventorySiteRefFullName;
        protected string m_InventorySiteLocationRef;
        protected string m_SerialNumber; 
        protected string m_LotNumber; 

        #endregion
    }

    
       
        
       
       
}
