using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This Class Provide Members of SalesReceipt.
    /// </summary>
    public class BaseSalesReceiptList
    {
        #region Protected Methods

        protected string m_TxnID;        
        protected string m_TxnNumber;
        protected string m_CustomerListID;
        protected string m_CustomerRefFullName;
        protected string m_ClassRefFullName;
        protected string m_TemplateRefFullName;
        protected string m_TxnDate;
        protected string m_RefNumber;
        protected string m_BillAddress;
        protected string m_BillAddress1;
        protected string m_BillAddress2;
        protected string m_BillAddress3;
        protected string m_BillAddress4;
        protected string m_City;
        protected string m_State;
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_ShipAddress;
        protected string m_ShipAddress1;
        protected string m_ShipAddress2;
        protected string m_ShipAddress3;
        protected string m_ShipAddress4;
        protected string m_ShipCountry;
        protected string m_IsPending;
        protected string m_CheckNumber;
        protected string m_PaymentMethodRefFullName;
        protected string m_DueDate;
        protected string m_SalesRepRefFullName;
        protected string m_Shipdate;
        protected string m_ShipMethodRefFullName;
        protected string m_FOB;
        protected string m_ItemSalesTaxFullName;
        protected string m_SalesTaxPercentage;
        protected decimal? m_MainTotalAmount;
        protected string m_CurrencyFullName;
        protected string m_Memo;
        protected string m_CutomerMsgFullName;
        protected string m_IsToBePrinted;
        protected string m_IsToBeEmailed;
        protected string m_IsTaxIncluded;
        protected string m_CustomerSalesTaxFullName;
        protected string m_DepositToAccountFullName;     
        protected string m_Other;
        
        
        //SalesReceiptLineRet
        protected List<string> m_TxnLineID = new List<string>();
        protected List<string> m_ItemFullName = new List<string>();
        protected List<string> m_Desc = new List<string>();
        protected  List<decimal?> m_Quantity = new List<decimal?>();
        protected List<string> m_UOM = new List<string>();
        protected List<string> m_OverrideUOMFullName = new List<string>();
        protected  List<decimal?> m_Rate = new List<decimal?>();
        protected List<string> m_LineClassRefFullName = new List<string>();
        protected  List<decimal?> m_Amount = new List<decimal?>();
        protected List<string> m_InventorySiteRefFullName = new List<string>();
        protected List<string> m_ServiceDate = new List<string>();
        protected List<string> m_SalesTaxCodeFullName = new List<string>();
        protected List<string> m_Other1 = new List<string>();
        protected List<string> m_Other2 = new List<string>();
        // axis 10.0 changes
        protected List<string> m_SerialNumber = new List<string>();
        protected List<string> m_LotNumber = new List<string>();
        // axis 10.0 changes ends

        //SalesReceiptLineGroupRet
        protected List<string> m_GroupTxnLineID = new List<string>();
        protected List<string> m_ItemGroupFullName = new List<string>();
        protected List<string> m_GroupDesc = new List<string>();
        protected  List<decimal?> m_GroupQuantity = new List<decimal?>();
        protected List<string> m_IsPrintItemsInGroup = new List<string>();

        //Sub SalesReceiptLineRet
        protected List<string> m_GroupLineTxnLineID = new List<string>();
        protected List<string> m_GroupLineItemFullName = new List<string>();
        protected List<string> m_GroupLineDesc = new List<string>();
        protected  List<decimal?> m_GroupLineQuantity = new List<decimal?>();
        protected List<string> m_GroupLineUOM = new List<string>();
        protected List<string> m_GroupLineOverrideUOM = new List<string>();
        protected  List<decimal?> m_GroupLineRate = new List<decimal?>();
        protected  List<decimal?> m_GroupLineAmount = new List<decimal?>();
        protected List<string> m_GroupLineServiceDate = new List<string>();
        protected List<string> m_GroupLineSalesTaxCodeFullName = new List<string>();
        
        #endregion
    }
}
