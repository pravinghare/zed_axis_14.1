﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Interop.QBXMLRP2Lib;
using System.Xml;
using System.Windows.Forms;
using DataProcessingBlocks;
using System.IO;
using EDI.Constant;
using System.ComponentModel;


namespace Streams
{
  public class BaseInventorySiteList
    {
        protected string m_ListID;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected string m_Name;
        protected string m_IsActive;
        protected string m_ParentSiteFullName;
        protected string m_IsDefaultSite;
        protected string m_SiteDesc;
        protected string m_Contact;
        protected string m_Phone;
        protected string m_Fax;
        protected string m_Email;
        protected string m_SiteAddr1;
        protected string m_SiteAddr2;
        protected string m_SiteAddr3;
        protected string m_SiteAddr4;
        protected string m_SiteAddr5;
        protected string m_SiteCity;
        protected string m_SiteState;
        protected string m_SitePostalCode;
        protected string m_SiteCountry;
        protected string m_SiteAddressBlockAddr1;
        protected string m_SiteAddressBlockAddr2;
        protected string m_SiteAddressBlockAddr3;
        protected string m_SiteAddressBlockAddr4;
        protected string m_SiteAddressBlockAddr5;
        protected string m_Sublevel;
    }
}
