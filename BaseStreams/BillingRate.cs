﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// This Class is used to provide members of Billing Rate.
    /// </summary>
    public class BaseBillingRate
    {
        #region Protected Members

        //For BillingRateRet
        protected string m_ListID;
        protected string m_TimeCreated;
        protected string m_TimeModified;
        protected string m_EditSequence;
        protected string m_Name;
        protected string m_BillingRateType;
        protected string m_FixedBillingRate;

        //For BillingRatePerItemRet
        protected string[] m_LineID = new string[30000];
        protected string[] m_ItemRefFullName = new string[30000];
        protected decimal?[] m_CustomRate = new decimal?[30000];
        protected string[] m_CustomRatePercent = new string[30000];
      
        #endregion
    }
}
