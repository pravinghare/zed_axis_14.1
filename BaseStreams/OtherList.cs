using System;
using System.Collections.Generic;
using System.Text;

namespace Streams
{
    /// <summary>
    /// Base class for Other list.
    /// </summary>
    /// <summary>
    /// This class provides members of Other list.
    /// </summary>
    public class BaseOtherList
    {
        #region Private members


        protected string[] m_ListID = new string[30000];
        protected string m_Name;
        protected string m_IsActive;
        protected string m_CompanyName;
        protected string m_Salutation;
        protected string m_FirstName;
        protected string m_MiddleName;
        protected string m_LastName;
        protected string m_OtherNameAddress;
        protected string m_OtherNameAddress1;
        protected string m_OtherNameAddress2;
        protected string m_OtherNameAddress3;
        protected string m_OtherNameAddress4;
        protected string m_OtherNameAddress5;
        protected string m_City;
        protected string m_State;
        protected string m_PostalCode;
        protected string m_Country;
        protected string m_Note;
        protected string m_Phone;
        protected string m_AltPhone;
        protected string m_Fax;
        protected string m_Email;
        protected string m_Contact;
        protected string m_AltContact;
        
        protected string m_AccountNumber;
        protected string m_Notes;
        protected string m_ExternalGUID;
        protected string m_OwnerID;
        //protected string m_DataExtName;
        //protected string m_DataExtType;



        #endregion
    }
}
