﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("BillingRateQBEntry", Namespace = "", IsNullable = false)]
    public class BillingRateQBEntry
    {
        #region  Private Member Variable

        private string m_Name;
        private string m_FixedBillingRate;
        private Collection<BillingRatePerItem> m_BillingRatePerItemAdd = new Collection<BillingRatePerItem>();
        #endregion

        #region Constructors
        public BillingRateQBEntry()
        {

        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return this.m_Name; }
            set { this.m_Name = value; }
        }

        public string FixedBillingRate
        {
            get { return m_FixedBillingRate; }
            set { m_FixedBillingRate = value; }
        }

        [XmlArray("BillingRatePerItemREM")]
        public Collection<BillingRatePerItem> BillingRatePerItem
        {
            get { return m_BillingRatePerItemAdd; }
            set { m_BillingRatePerItemAdd = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used for Adding new Billing Rate into QuickBooks.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillingRateQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);
            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillingRateAddRq aggregate and fill in field values for it
            System.Xml.XmlElement BillingRateAddRq = requestXmlDoc.CreateElement("BillingRateAddRq");
            inner.AppendChild(BillingRateAddRq);

            //Create BillingRateAdd aggregate and fill in field values for it
            System.Xml.XmlElement BillingRateAdd = requestXmlDoc.CreateElement("BillingRateAdd");

            BillingRateAddRq.AppendChild(BillingRateAdd);
            requestXML = requestXML.Replace("<BillingRatePerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</BillingRatePerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<BillingRatePerItemREM />", string.Empty);
            requestXML = requestXML.Replace("<BillingRatePerItemREM/>", string.Empty);
            BillingRateAdd.InnerXml = requestXML;
            
            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillingRateAddRq/BillingRateAdd"))
            {
                if (oNode.SelectSingleNode("Name") != null)
                {
                    requeststring = oNode.SelectSingleNode("Name").InnerText.ToString();
                }
            }

            if (requeststring != string.Empty)
                BillingRateAddRq.SetAttribute("requestID", "Name :" + requeststring + " RowNumber (By BillingRateName) : " + rowcount.ToString());
            else
                BillingRateAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillingRateAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillingRateAddRs/BillingRateRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBillingRate(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
    }

    public class BillingRateQBEntryCollection : Collection<BillingRateQBEntry>
    {
        /// <summary>
        /// This method is used for validate given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public BillingRateQBEntry FindBillingRateEntry(string name)
        {
            foreach (BillingRateQBEntry item in this)
            {
                if (item.Name == name)
                {
                    return item;
                }
            }
            return null;
        }
    }
    /// <summary>
    ///This class for Declaring properties
    /// </summary>
    [XmlRootAttribute("BillingRatePerItem", Namespace = "", IsNullable = false)]
    public class BillingRatePerItem
    {
        private ItemRef m_ItemRef;
        private string m_CustomRate;
        private string m_CustomRatePercent;
        private string m_AdjustPercentage;
        private string m_AdjustBillingRateRelativeTo;

        public BillingRatePerItem()
        { }


        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string CustomRate
        {
            get { return this.m_CustomRate; }
            set { this.m_CustomRate = value; }
        }

        public string CustomRatePercent
        {
            get { return this.m_CustomRatePercent; }
            set { this.m_CustomRatePercent = value; }
        }

        public string AdjustPercentage
        {
            get { return this.m_AdjustPercentage; }
            set { this.m_AdjustPercentage = value; }
        }

        public string AdjustBillingRateRelativeTo
        {
            get { return this.m_AdjustBillingRateRelativeTo; }
            set { this.m_AdjustBillingRateRelativeTo = value; }
        }
    }

    public enum AdjustBillingRateRelative
    {
        StandardRate,
        CurrentCustomRate
    }
}
