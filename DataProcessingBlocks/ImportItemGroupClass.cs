using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemGroupClass
    {
        private static ImportItemGroupClass m_ImportItemGroupClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemGroupClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemGroup class
        /// </summary>
        /// <returns></returns>
        public static ImportItemGroupClass GetInstance()
        {
            if (m_ImportItemGroupClass == null)
                m_ImportItemGroupClass = new ImportItemGroupClass();
            return m_ImportItemGroupClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemGroup and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemGroup QuickBooks collection </returns>
        public DataProcessingBlocks.ItemGroupQBEntryCollection ImportItemGroupData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemGroup Entry collections.
            DataProcessingBlocks.ItemGroupQBEntryCollection coll = new ItemGroupQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemGroup Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemGroupDt = new DateTime();
                    string datevalue = string.Empty;



                    if (dt.Columns.Contains("Name"))
                    {

                        //ItemGroup Validation
                        DataProcessingBlocks.ItemGroupQBEntry ItemGroup = new ItemGroupQBEntry();
                        ItemGroup = coll.FindItemGroupEntry(dr["Name"].ToString());

                        if (ItemGroup == null)
                        {
                            ItemGroup = new ItemGroupQBEntry();

                            if (dt.Columns.Contains("Name"))
                            {
                                #region Validations of Name
                                if (dr["Name"].ToString() != string.Empty)
                                {
                                    if (dr["Name"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemGroup.Name = dr["Name"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemGroup.Name = dr["Name"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemGroup.Name = dr["Name"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemGroup.Name = dr["Name"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //P Axis 13.1 : issue 651
                            if (dt.Columns.Contains("BarCodeValue"))
                            {
                                #region Validations of BarCodeValue
                                if (dr["BarCodeValue"].ToString() != string.Empty)
                                {
                                    if (dr["BarCodeValue"].ToString().Length > 50)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BarCodeValue (" + dr["BarCodeValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemGroup.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                                if (ItemGroup.BarCode.BarCodeValue == null)
                                                    ItemGroup = null;
                                                else
                                                    ItemGroup.BarCode = new BarCode(dr["BarCodeValue"].ToString().Substring(0, 50));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemGroup.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                                if (ItemGroup.BarCode.BarCodeValue == null)
                                                    ItemGroup = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemGroup.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        ItemGroup.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                    }
                                }
                                #endregion
                            }

                         
                            //P Axis 13.1 : issue 651 END
                            if (dt.Columns.Contains("IsActive"))
                            {
                                #region Validations of IsActive
                                if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsActive"].ToString(), out result))
                                    {
                                        ItemGroup.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsActive"].ToString().ToLower() == "true")
                                        {
                                            ItemGroup.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsActive"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemGroup.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemGroup.IsActive = dr["IsActive"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemGroup.IsActive = dr["IsActive"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemGroup.IsActive = dr["IsActive"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemDesc"))
                            {
                                #region Validations of ItemDesc
                                if (dr["ItemDesc"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDesc (" + dr["ItemDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemGroup.ItemDesc = dr["ItemDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemGroup.ItemDesc = dr["ItemDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemGroup.ItemDesc = dr["ItemDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemGroup.ItemDesc = dr["ItemDesc"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("UnitOfMeasureSetFullName"))
                            {
                                #region Validations of UnitOfMeasureSet FullName
                                if (dr["UnitOfMeasureSetFullName"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasureSetFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasureSet Full Name (" + dr["UnitOfMeasureSetFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemGroup.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                                if (ItemGroup.UnitOfMeasureSetRef.FullName == null)
                                                    ItemGroup.UnitOfMeasureSetRef.FullName = null;
                                                else
                                                    ItemGroup.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemGroup.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                                if (ItemGroup.UnitOfMeasureSetRef.FullName == null)
                                                    ItemGroup.UnitOfMeasureSetRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemGroup.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                            if (ItemGroup.UnitOfMeasureSetRef.FullName == null)
                                                ItemGroup.UnitOfMeasureSetRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemGroup.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                        if (ItemGroup.UnitOfMeasureSetRef.FullName == null)
                                            ItemGroup.UnitOfMeasureSetRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsPrintItemsInGroup"))
                            {
                                #region Validations of IsPrintItemsInGroup
                                if (dr["IsPrintItemsInGroup"].ToString() != "<None>" || dr["IsPrintItemsInGroup"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsPrintItemsInGroup"].ToString(), out result))
                                    {
                                        ItemGroup.IsPrintItemsInGroup = Convert.ToInt32(dr["IsPrintItemsInGroup"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsPrintItemsInGroup"].ToString().ToLower() == "true")
                                        {
                                            ItemGroup.IsPrintItemsInGroup = dr["IsPrintItemsInGroup"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsPrintItemsInGroup"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemGroup.IsPrintItemsInGroup = dr["IsPrintItemsInGroup"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsPrintItemsInGroup (" + dr["IsPrintItemsInGroup"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemGroup.IsPrintItemsInGroup = dr["IsPrintItemsInGroup"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemGroup.IsPrintItemsInGroup = dr["IsPrintItemsInGroup"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemGroup.IsPrintItemsInGroup = dr["IsPrintItemsInGroup"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            #region Item Group LineItem
                            ItemGroupLine itemGroupLineItem = new ItemGroupLine();

                            if (dt.Columns.Contains("ItemGroupLineItemFullName"))
                            {
                                #region Validations of ItemGroupLineItem FullName
                                if (dr["ItemGroupLineItemFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineItemFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineItem FullName (" + dr["ItemGroupLineItemFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                                if (itemGroupLineItem.ItemRef.FullName == null)
                                                    itemGroupLineItem.ItemRef.FullName = null;
                                                else
                                                    itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                                if (itemGroupLineItem.ItemRef.FullName == null)
                                                    itemGroupLineItem.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                            if (itemGroupLineItem.ItemRef.FullName == null)
                                                itemGroupLineItem.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                        if (itemGroupLineItem.ItemRef.FullName == null)
                                            itemGroupLineItem.ItemRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupLineQuantity"))
                            {
                                #region Validations of ItemGroupLine Quantity
                                if (dr["ItemGroupLineQuantity"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineQuantity"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLine Quantity (" + dr["ItemGroupLineQuantity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString().Substring(0, 100);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemGroupLineUnitOfMeasure"))
                            {
                                #region Validations of ItemGroupLine UnitOfMeasure
                                if (dr["ItemGroupLineUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineUnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLine UnitOfMeasure (" + dr["ItemGroupLineUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (itemGroupLineItem.ItemRef != null || itemGroupLineItem.UnitOfMeasure != null || itemGroupLineItem.Quantity != null)
                                ItemGroup.ItemGroupLine.Add(itemGroupLineItem);

                            #endregion

                            coll.Add(ItemGroup);
                        }
                        else
                        {
                            #region Item Group Line Item
                            ItemGroupLine itemGroupLineItem = new ItemGroupLine();

                            if (dt.Columns.Contains("ItemGroupLineItemFullName"))
                            {
                                #region Validations of ItemGroupLineItem FullName
                                if (dr["ItemGroupLineItemFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineItemFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineItem FullName (" + dr["ItemGroupLineItemFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                                if (itemGroupLineItem.ItemRef.FullName == null)
                                                    itemGroupLineItem.ItemRef.FullName = null;
                                                else
                                                    itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                                if (itemGroupLineItem.ItemRef.FullName == null)
                                                    itemGroupLineItem.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                            if (itemGroupLineItem.ItemRef.FullName == null)
                                                itemGroupLineItem.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupLineItem.ItemRef = new ItemRef(dr["ItemGroupLineItemFullName"].ToString());
                                        if (itemGroupLineItem.ItemRef.FullName == null)
                                            itemGroupLineItem.ItemRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupLineQuantity"))
                            {
                                #region Validations of ItemGroupLine Quantity
                                if (dr["ItemGroupLineQuantity"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineQuantity"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLine Quantity (" + dr["ItemGroupLineQuantity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString().Substring(0, 100);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupLineItem.Quantity = dr["ItemGroupLineQuantity"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemGroupLineUnitOfMeasure"))
                            {
                                #region Validations of ItemGroupLine UnitOfMeasure
                                if (dr["ItemGroupLineUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineUnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLine UnitOfMeasure (" + dr["ItemGroupLineUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupLineItem.UnitOfMeasure = dr["ItemGroupLineUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (itemGroupLineItem.ItemRef != null || itemGroupLineItem.UnitOfMeasure != null || itemGroupLineItem.Quantity != null)
                                ItemGroup.ItemGroupLine.Add(itemGroupLineItem);

                            #endregion
                        }
                    }
                    else
                    {

                    }

                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }

    }
}
