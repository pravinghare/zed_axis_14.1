using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("ItemInventoryAssemblyQBEntry", Namespace = "", IsNullable = false)]
    public class ItemInventoryAssemblyQBEntry
    {
        #region Private Member Variable

        private string m_Name;
        private BarCode m_BarCode;
        private string m_IsActive;
        private ParentRef m_ParentRef;
        private UnitOfMeasureSetRef m_UnitOfMeasureSetRef;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_SalesDesc;
        private string m_SalesPrice;
        private IncomeAccountRef m_IncomeAccountRef;
        private string m_PurchaseDesc;
        private string m_PurchaseCost;
        private PurchaseTaxCodeRef m_PurchaseTaxCodeRef;
        private COGSAccountRef m_COGSAccountRef;
        private PrefVendorRef m_PrefVendorRef;
        private AssetAccountRef m_AssetAccountRef;

        private string m_BuildPoint;
        private string m_QuantityOnHand;
        private string m_TotalValue;
        private string m_InventoryDate;
        private Collection<ItemInventoryAssemblyLine> m_ItemInventoryAssemblyLine = new Collection<ItemInventoryAssemblyLine>();
       
        
        #region bug 476 axis 12.0
        //bug 476 axis 12.0
        private ClassRef m_ClassRef;
        private string m_Max;
        private string m_ManufacturerPartNumber;
        #endregion

        #endregion

        #region Constructor
        public ItemInventoryAssemblyQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public BarCode BarCode
        {
            get { return m_BarCode; }
            set { m_BarCode = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }


        //bug 476 axis 12.0
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public ParentRef ParentRef
        {
            get { return m_ParentRef; }
            set { m_ParentRef = value; }
        }

        //bug 476 axis 12.0
        public string ManufacturerPartNumber
        {
            get { return m_ManufacturerPartNumber; }
            set { m_ManufacturerPartNumber = value; }
        }
     
       
                
        public UnitOfMeasureSetRef UnitOfMeasureSetRef
        {
            get { return m_UnitOfMeasureSetRef; }
            set { m_UnitOfMeasureSetRef = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }

        public string SalesDesc
        {
            get { return m_SalesDesc; }
            set { m_SalesDesc = value; }
        }

        public string SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }

        public IncomeAccountRef IncomeAccountRef
        {
            get { return m_IncomeAccountRef; }
            set { m_IncomeAccountRef = value; }
        }

        public string PurchaseDesc
        {
            get { return m_PurchaseDesc; }
            set { m_PurchaseDesc = value; }
        }

        public string PurchaseCost
        {
            get { return m_PurchaseCost; }
            set { m_PurchaseCost = value; }
        }

        public PurchaseTaxCodeRef PurchaseTaxCodeRef
        {
            get { return m_PurchaseTaxCodeRef; }
            set { m_PurchaseTaxCodeRef = value; }
        }

        public COGSAccountRef COGSAccountRef
        {
            get { return m_COGSAccountRef; }
            set { m_COGSAccountRef = value; }
        }

        public PrefVendorRef PrefVendorRef
        {
            get { return m_PrefVendorRef; }
            set { m_PrefVendorRef = value; }
        }

        public AssetAccountRef AssetAccountRef
        {
            get { return m_AssetAccountRef; }
            set { m_AssetAccountRef = value; }
        }

        public string BuildPoint
        {
            get { return m_BuildPoint; }
            set { m_BuildPoint = value; }
        }

        //bug 476 axis 12.0
        public string Max
        {
            get { return m_Max; }
            set { m_Max = value; }
        }


        public string QuantityOnHand
        {
            get { return m_QuantityOnHand; }
            set { m_QuantityOnHand = value; }
        }

        public string TotalValue
        {
            get { return m_TotalValue; }
            set { m_TotalValue = value; }
        }

        public string InventoryDate
        {
            get { return m_InventoryDate; }
            set { m_InventoryDate = value; }
        }

        [XmlArray("ItemInventoryAssemblyLineREM")]
        public Collection<ItemInventoryAssemblyLine> ItemInventoryAssemblyLine
        {
            get { return m_ItemInventoryAssemblyLine; }
            set { m_ItemInventoryAssemblyLine = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemInventoryAssemblyQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemInventoryAssemblyAddRq = requestXmlDoc.CreateElement("ItemInventoryAssemblyAddRq");
            inner.AppendChild(ItemInventoryAssemblyAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement ItemInventoryAssemblyAdd = requestXmlDoc.CreateElement("ItemInventoryAssemblyAdd");

            ItemInventoryAssemblyAddRq.AppendChild(ItemInventoryAssemblyAdd);


            requestXML = requestXML.Replace("<ItemInventoryAssemblyLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemInventoryAssemblyLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemInventoryAssemblyLineREM>", string.Empty);

            ItemInventoryAssemblyAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;



            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAssemblyAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAssemblyAddRs/ItemInventoryAssemblyRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemInventoryAssembly(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating ItemInventoryAssemblyQBEntry information
        /// of existing ItemInventoryAssemblyQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateItemInventoryAssemblyInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemInventoryAssemblyQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemInventoryAssemblyQBEntryModRq = requestXmlDoc.CreateElement("ItemInventoryAssemblyModRq");
            inner.AppendChild(ItemInventoryAssemblyQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement ItemInventoryAssemblyMod = requestXmlDoc.CreateElement("ItemInventoryAssemblyMod");
            ItemInventoryAssemblyQBEntryModRq.AppendChild(ItemInventoryAssemblyMod);


            requestXML = requestXML.Replace("<ItemInventoryAssemblyLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemInventoryAssemblyLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemInventoryAssemblyLineREM>", string.Empty);

            ItemInventoryAssemblyMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemInventoryAssemblyModRq/ItemInventoryAssemblyMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemInventoryAssemblyModRq/ItemInventoryAssemblyMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemInventoryAssemblyModRq/ItemInventoryAssemblyMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAssemblyModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAssemblyModRs/ItemInventoryAssemblyRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemInventoryAssembly(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }



    public class ItemInventoryAssemblyQBEntryCollection : Collection<ItemInventoryAssemblyQBEntry>
    {

        /// <summary>
        /// This method is used for checking whether the item name is exist or not.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public ItemInventoryAssemblyQBEntry FindItemInventoryAssemblyEntry(string name)
        {
            foreach (ItemInventoryAssemblyQBEntry item in this)
            {
                if (item.Name == name)
                {
                    return item;
                }
            }
            return null;
        }

    }
    /// <summary>
    /// Adding new line 
    /// </summary>
    [XmlRootAttribute("ItemInventoryAssemblyLine", Namespace = "", IsNullable = false)]
    public class ItemInventoryAssemblyLine
    {
        #region Private Members

        private ItemInventoryRef m_ItemInventoryRef;
        private string m_Quantity;

        #endregion

        #region Constructor

        public ItemInventoryAssemblyLine()
        { }
        
        #endregion

        #region Public Properties

        public ItemInventoryRef ItemInventoryRef
        {
            get { return m_ItemInventoryRef; }
            set { m_ItemInventoryRef = value; }
        }

        public string Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        #endregion
    }

}
