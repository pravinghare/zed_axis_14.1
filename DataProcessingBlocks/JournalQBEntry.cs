// ==============================================================================================
// 
// JournalQBEntry.cs
//
// This file contains the implementations of the Journal Entry private members , 
// Properties, Constructors and Methods for QuickBooks Journal Imports.
//         JournalQBEntry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 Mapping fields
// (IsHomeCurrencyAdjustment,IsAmountsEnteredInHomeCurrency,CurrencyRef and ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Collections;
using System.Xml;
using EDI.Constant;


namespace DataProcessingBlocks
{

    [XmlRootAttribute("JournalQBEntry", Namespace = "", IsNullable = false)]
    public class JournalQBEntry
    {
        #region Private Member variables

        private string m_TxnDate;
        private string m_RefNumber;
        private string m_IsAdjustment;
        private string m_IsHomeCurrencyAdjustment;
        private string m_IsAmountsEnteredInHomeCurrency;
        private CurrencyRef m_CurrencyRef;
        private string m_ExchangeRate;
        private Collection<JournalDebitLine> m_JournalDebitLine = new Collection<JournalDebitLine>();
        private Collection<JournalCreditLine> m_JournalCreditLine = new Collection<JournalCreditLine>();
        private DateTime m_JournalDate;
        private decimal m_CrAmount = 0;
        private decimal m_DrAmount = 0;
        private bool m_IsBalance = false;
        private string m_IsBillable;
        private string m_Memo;
     

        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        public JournalQBEntry()
        {
            //Put constructor code here    
        }

        #endregion

        #region Public Properties
        /// <summary>
        /// Get or Set the transaction date field of journal entry
        /// </summary>

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                {
                    return null;
                }
                else
                    return Convert.ToString(this.m_TxnDate);
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        /// <summary>
        /// Get or set RefNumber field of Journal entry.
        /// </summary>
        public string RefNumber
        {
            get
            {
                return this.m_RefNumber;
            }
            set
            {
                this.m_RefNumber = value;
            }
        }

        /// <summary>
        /// Get or set IsAdjustment field of Journal Entry.
        /// </summary>
        [XmlElement(DataType = "string")]
        public string IsAdjustment
        {
            get
            {
                return this.m_IsAdjustment;
            }
            set
            {
                this.m_IsAdjustment = value;
            }
        }

        /// <summary>
        /// Get or Set IsHomeCurrencyAdjustment.
        /// Possible Set values : 0,1,true,false
        /// </summary>
        public string IsHomeCurrencyAdjustment
        {
            get
            {
                return this.m_IsHomeCurrencyAdjustment;
            }
            set
            {
                this.m_IsHomeCurrencyAdjustment = value;
            }
        }

        /// <summary>
        /// Get or Set IsAmountsEnteredInHomeCurrency.
        /// Possible Set Values :0,1,true,false
        /// </summary>
        public string IsAmountsEnteredInHomeCurrency
        {
            get
            {
                return this.m_IsAmountsEnteredInHomeCurrency;
            }
            set
            {
                this.m_IsAmountsEnteredInHomeCurrency = value;
            }
        }

        /// <summary>
        /// Get or Set Currency Fullname.
        /// </summary>
        public CurrencyRef CurrencyRef
        {
            get
            {
                return this.m_CurrencyRef;
            }
            set
            {
                this.m_CurrencyRef = value;
            }
        }

        /// <summary>
        /// Get or Set ExchangeRate value.
        /// </summary>
        public string ExchangeRate
        {
            get
            {
                return this.m_ExchangeRate;
            }
            set
            {
                this.m_ExchangeRate = value;
            }
        }

        [XmlArray("JournalDebitLineREM")]
        public Collection<JournalDebitLine> JournalDebitLine
        {
            get { return this.m_JournalDebitLine; }
            set
            {
                this.m_JournalDebitLine = value;
            }
        }
        [XmlArray("JournalCreditLineREM")]
        public Collection<JournalCreditLine> JournalCreditLine
        {
            get { return this.m_JournalCreditLine; }
            set
            {
                this.m_JournalCreditLine = value;
            }
        }

        [XmlIgnoreAttribute()]
        public DateTime JournalDate
        {
            get { return this.m_JournalDate; }
            set { this.m_JournalDate = value; }
        }

        [XmlIgnoreAttribute()]
        public decimal CrAmount
        {
            get { return this.m_CrAmount; }
            set { this.m_CrAmount = value; }
        }

        [XmlIgnoreAttribute()]
        public decimal DrAmount
        {
            get { return this.m_DrAmount; }
            set { this.m_DrAmount = value; }
        }

        [XmlIgnoreAttribute()]
        public bool IsVarifyBalance
        {
            get { return this.m_IsBalance; }
            set { this.m_IsBalance = value; }
        }


        public string IsBillable
        {
            get { return this.m_IsBillable; }
            set { this.m_IsBillable = value; }
        }


        public string Memo
        {
            get { return this.m_Memo; }
            set { this.m_Memo = value; }
        }
        #endregion
        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="appname"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string appname)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.JournalQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }


            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }
            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement JournalEntryAddRq = requestXmlDoc.CreateElement("JournalEntryAddRq");
            inner.AppendChild(JournalEntryAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement JournalEntryAdd = requestXmlDoc.CreateElement("JournalEntryAdd");

            JournalEntryAddRq.AppendChild(JournalEntryAdd);


            requestXML = requestXML.Replace("<JournalDebitLineREM>", string.Empty);
            requestXML = requestXML.Replace("<JournalDebitLineREM >", string.Empty);
            requestXML = requestXML.Replace("</JournalDebitLineREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalDebitLineREM >", string.Empty);
            requestXML = requestXML.Replace("<JournalCreditLineREM>", string.Empty);
            requestXML = requestXML.Replace("<JournalCreditLineREM >", string.Empty);
            requestXML = requestXML.Replace("</JournalCreditLineREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalCreditLineREM >", string.Empty);
            requestXML = requestXML.Replace("<JournalCreditLineREM />", string.Empty);
            requestXML = requestXML.Replace("<JournalDebitLineREM />", string.Empty);
            JournalEntryAdd.InnerXml = requestXML;//.Replace("<JournalDebitLineREM>", string.Empty).Replace("</JournalDebitLineREM>", string.Empty).Replace("<JournalCreditLineREM>", string.Empty).Replace("</JournalCreditLineREM >", string.Empty);

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/JournalEntryAddRq/JournalEntryAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                JournalEntryAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                JournalEntryAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;           
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(appname, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                    
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                    
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {

                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryAddRs/JournalEntryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofJournal(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        /// <summary>
        /// This method is used for getting existing Journals ref no .
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Journal Entry Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public Hashtable CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            Hashtable JournalTable = new Hashtable();
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryQueryRq aggregate and fill in field values for it
            XmlElement JournalEntryQueryRq = requestXmlDoc.CreateElement("JournalEntryQueryRq");
            inner.AppendChild(JournalEntryQueryRq);

            //Create Refno aggregate and fill in field values for it
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            JournalEntryQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            //XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            //IncludeRetElement.InnerText = "TxnID";
            //JournalEntryQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return JournalTable;
            }

            if (resp == string.Empty)
            {
                return JournalTable;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no price level exists.
                    return JournalTable;

                }
                else
                    if (resp.Contains("statusSeverity=\"Warn\""))
                    {
                        //Returning means there is no Journal exists.
                        return JournalTable;
                    }
                    else
                    {
                        //Getting listid and Edit Sequence details of Journal.
                        string listID = string.Empty;
                        string editSequence = string.Empty;
                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryQueryRs/JournalEntryRet"))
                        {
                            //Get ListID of price Level.
                            if (oNode.SelectSingleNode("TxnID") != null)
                            {
                                listID = oNode.SelectSingleNode("TxnID").InnerText.ToString();
                            }
                            //Get Edit Sequence of Price Level.
                            if (oNode.SelectSingleNode("EditSequence") != null)
                            {
                                editSequence = oNode.SelectSingleNode("EditSequence").InnerText.ToString();
                            }
                        }
                        if (listID != string.Empty && editSequence != string.Empty)
                        {
                            JournalTable.Add(listID, editSequence);
                        }
                        return JournalTable;
                    }
            }
        }

        /// <summary>
        /// This method is used for updating price level information
        /// of existing Journal with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateJournalInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.JournalQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalModRq aggregate and fill in field values for it
            System.Xml.XmlElement JournalEntryModRq = requestXmlDoc.CreateElement("JournalEntryModRq");
            inner.AppendChild(JournalEntryModRq);

            //Create PriceLevelMod aggregate and fill in field values for it
            System.Xml.XmlElement JournalEntryMod = requestXmlDoc.CreateElement("JournalEntryMod");
            JournalEntryModRq.AppendChild(JournalEntryMod);

            requestXML = requestXML.Replace("<JournalEntryPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalEntryPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<JournalDebitLineREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalDebitLineREM>", string.Empty);
            requestXML = requestXML.Replace("<JournalCreditLineREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalCreditLineREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<JournalCreditLine>", "<JournalLineMod><TxnLineID>-1</TxnLineID><JournalLineType>Credit</JournalLineType>");
            requestXML = requestXML.Replace("</JournalCreditLine>", "</JournalLineMod>");
            requestXML = requestXML.Replace("<JournalDebitLine>", "<JournalLineMod><TxnLineID>-1</TxnLineID><JournalLineType>Debit</JournalLineType>");
            requestXML = requestXML.Replace("</JournalDebitLine>", "</JournalLineMod>");
            //requestXML = requestXML.Replace("<AccountRef>", "<TxnLineID>-1</TxnLineID><AccountRef>");

            JournalEntryMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/JournalEntryAddRq/JournalEntryAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                JournalEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By JournalEntryRefNumber) : " + rowcount.ToString());
            else
                JournalEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;


            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/JournalEntryModRq/JournalEntryMod").FirstChild;

                //Create ListID aggregate and fill in field values for it
                System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
                //ListID.InnerText = listID;
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/JournalEntryModRq/JournalEntryMod").InsertBefore(ListID, firstChild).InnerText = listID;
                //PriceLevelMod.AppendChild(ListID).InnerText = listID;
                //Create EditSequnce aggregate and fill in field values for it
                System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
                //EditSequence.InnerText = editSequence;
                //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/JournalEntryModRq/JournalEntryMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;


            string responseFile = string.Empty;
            string resp = string.Empty;

            try
            {

                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryModRs/JournalEntryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofJournal(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

//Bug No.412
        /// <summary>
        /// This method is used to append data to corresponding transaction
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendJournalInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.JournalQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalModRq aggregate and fill in field values for it
            System.Xml.XmlElement JournalEntryModRq = requestXmlDoc.CreateElement("JournalEntryModRq");
            inner.AppendChild(JournalEntryModRq);

            //Create PriceLevelMod aggregate and fill in field values for it
            System.Xml.XmlElement JournalEntryMod = requestXmlDoc.CreateElement("JournalEntryMod");
            JournalEntryModRq.AppendChild(JournalEntryMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.

            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></JournalLineMod><JournalLineMod>";
            }

            for (int i = 0; i < request.Length; i++)
            {
                if (Listcnt != 0)
                {
                    if (subStringCnt == 1)
                    {
                        // subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></JournalLineMod><JournalLineMod><TxnLineID>-1</TxnLineID><ItemRef>");
                        subResultString = request[i].Replace("<AccountRef>", addString + "<TxnLineID>-1</TxnLineID><JournalLineType>");
                        subStringCnt++;
                    }
                    else
                    {
                        subResultString = request[i].Replace("<AccountRef>", "<TxnLineID>-1</TxnLineID><JournalLineType>");
                    }
                }
                else
                {
                    subResultString = request[i].Replace("<AccountRef>", "<TxnLineID>-1</TxnLineID><JournalLineType>");
                }

                if (stringCnt == request.Length)
                {
                    resultString += subResultString;
                }
                else
                {
                    resultString += subResultString + "</AccountRef>";
                }
                stringCnt++;
                Listcnt--;
            }
            requestXML = resultString;


            requestXML = requestXML.Replace("<JournalEntryPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalEntryPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<JournalDebitLineREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalDebitLineREM>", string.Empty);
            requestXML = requestXML.Replace("<JournalCreditLineREM>", string.Empty);
            requestXML = requestXML.Replace("</JournalCreditLineREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<JournalCreditLine>", "<JournalLineMod><TxnLineID>-1</TxnLineID><JournalLineType>Credit</JournalLineType>");
            requestXML = requestXML.Replace("</JournalCreditLine>", "</JournalLineMod>");
            requestXML = requestXML.Replace("<JournalDebitLine>", "<JournalLineMod><TxnLineID>-1</TxnLineID><JournalLineType>Debit</JournalLineType>");
            requestXML = requestXML.Replace("</JournalDebitLine>", "</JournalLineMod>");
            //requestXML = requestXML.Replace("<AccountRef>", "<TxnLineID>-1</TxnLineID><AccountRef>");

            JournalEntryMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/JournalEntryAddRq/JournalEntryAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                JournalEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By JournalEntryRefNumber) : " + rowcount.ToString());
            else
                JournalEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;


            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/JournalEntryModRq/JournalEntryMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/JournalEntryModRq/JournalEntryMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/JournalEntryModRq/JournalEntryMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;


            string responseFile = string.Empty;
            string resp = string.Empty;

            try
            {

                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/JournalEntryModRs/JournalEntryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofJournal(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
    }


    public class JournalQBEntryCollection : Collection<JournalQBEntry>
    {
        /// <summary>
        /// This method is used for getting existing journal date, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public JournalQBEntry FindJournalEntry(DateTime date)
        {
            foreach (JournalQBEntry item in this)
            {
                if (item.JournalDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
       /// <summary>
        /// This method is used for getting existing journal refnumber, If not exists then it return null.
       /// </summary>
       /// <param name="refNumber"></param>
       /// <returns></returns>
        public JournalQBEntry FindJournalEntry(string refNumber)
        {
            foreach (JournalQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
       /// <summary>
        /// This method is used for getting existing journal date and refnumber, If not exists then it return null.
       /// </summary>
       /// <param name="date"></param>
       /// <param name="refNumber"></param>
       /// <returns></returns>
        public JournalQBEntry FindJournalEntry(DateTime date, string refNumber)
        {
            foreach (JournalQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.JournalDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }
    /// <summary>
    /// This class is for adding new line for Journal Debit
    /// </summary>
    [XmlRootAttribute("JournalDebitLine", Namespace = "", IsNullable = false)]
    public class JournalDebitLine
    {
        private string m_TxnLineID;
        private AccountRef m_AccountRef;
        private string m_Ammount;
        private string m_Memo;
        private EntityRef m_EntityRef;
        private ClassRef m_ClassRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_BillableStatus;

        public JournalDebitLine()
        { }

        public string TxnLineID
        {
            get { return this.m_TxnLineID; }
            set { this.m_TxnLineID = value; }
        }
        public AccountRef AccountRef
        {
            get { return this.m_AccountRef; }
            set { this.m_AccountRef = value; }
        }

        public string Amount
        {
            get { return this.m_Ammount; }
            set { this.m_Ammount = value; }
        }
        
        public string Memo
        {
            get { return this.m_Memo; }
            set { this.m_Memo = value; }
        }

        public EntityRef EntityRef
        {
            get { return this.m_EntityRef; }
            set { this.m_EntityRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return this.m_ItemSalesTaxRef; }
            set { this.m_ItemSalesTaxRef = value; }
        }

        public string BillableStatus
        {
            get { return this.m_BillableStatus; }
            set { this.m_BillableStatus = value; }
        }
         
    }

    /// <summary>
    /// This class is for adding new line for Journal credit
    /// </summary>
    [XmlRootAttribute("JournalCreditLine", Namespace = "", IsNullable = false)]
    public class JournalCreditLine
    {
        private string m_TxnLineID;
        private AccountRef m_AccountRef;
        private string m_Ammount;
        private string m_Memo;
        private EntityRef m_EntityRef;
        private ClassRef m_ClassRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_BillableStatus;

        public JournalCreditLine()
        { }

        public string TxnLineID
        {
            get { return this.m_TxnLineID; }
            set { this.m_TxnLineID = value; }
        }
        public AccountRef AccountRef
        {
            get { return this.m_AccountRef; }
            set { this.m_AccountRef = value; }
        }

        
        public string Amount
        {
            get { return this.m_Ammount; }
            set { this.m_Ammount =  value; }
        }
        
        public string Memo
        {
            get { return this.m_Memo; }
            set { this.m_Memo = value; }
        }

        public EntityRef EntityRef
        {
            get { return this.m_EntityRef; }
            set { this.m_EntityRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return this.m_ItemSalesTaxRef; }
            set { this.m_ItemSalesTaxRef = value; }
        }

        public string BillableStatus
        {
            get { return this.m_BillableStatus; }
            set { this.m_BillableStatus = value; }
        }
         
    }
    
}
