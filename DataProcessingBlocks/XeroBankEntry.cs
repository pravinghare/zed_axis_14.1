﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using System.Linq;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Api;
using DataProcessingBlocks.XeroConnection;
using System.Threading.Tasks;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("XeroBankEntry", Namespace = "", IsNullable = false)]
    public class XeroBankEntry
    {
        #region Constructor
        public XeroBankEntry()
        {
        }
        #endregion

        #region Public Properties

        public BankTransaction bank
        {
            get;
            set;
        }
        public BankTransactions banks
        {
            get;
            set;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// This Function is used to check whether iven bank transcation is present in xero.
        /// </summary>
        /// <param name="bankname"></param>
        /// <returns></returns>
        public async Task<bool> CheckAndGetNameExistsInXeroAsync(string bankname)
        {
            try
            {
                BankTransactions bankTransactionsResponse = new BankTransactions();
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                string query = "BankAccount.Code == \"" + bankname + "\"";
                var response =  res.GetBankTransactionsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, null, query, null, null, null);
                response.Wait();
                bankTransactionsResponse = response.Result;
                if (bankTransactionsResponse._BankTransactions.Count > 0)
                {
                    TransactionImporter.TransactionImporter.refnoflag = true;
                    CommonUtilities.GetInstance().existTxnId = (bankTransactionsResponse._BankTransactions[0].BankTransactionID ?? Guid.Empty);
                }
                else
                {
                    CommonUtilities.GetInstance().existTxnId = Guid.Empty;
                    TransactionImporter.TransactionImporter.refnoflag = false;
                }
            }
            catch (Exception ex)
            {

            }

            return TransactionImporter.TransactionImporter.refnoflag;
        }


        /// <summary>
        /// This function is used to import bank transcation in xero.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public async Task<bool> ExportToXeroAsync(string statusMessage, string requestText, int rowcount, string txnid, string accountid)
        {
            bool reponseFlag = false;
            statusMessage = string.Empty;
            BankTransactions bankResponse = new BankTransactions();

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            //try
            //{
            //    DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.XeroBankEntry>.Save(this, fileName);
            //}
            //catch
            //{
            //    statusMessage += "\n ";
            //    statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
            //    return false;
            //}

            //System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            //requestXmlDoc.Load(fileName);
            //string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            //System.IO.File.Delete(fileName);

            string resp = string.Empty;
            string responseFile = string.Empty;
            
            try
            {
                CommonUtilities.GetInstance().Type = "Bank";
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();

                BankTransactions xeroBank = new BankTransactions();
                xeroBank._BankTransactions = new List<BankTransaction>();
                xeroBank._BankTransactions.Add(bank);

                //requestText = ModelSerializer.Serialize(bank);
                //requestXmlDoc.LoadXml(requestText);
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);

                if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == false)
                {
                    var response = res.CreateBankTransactionAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, bank);
                    response.Wait();
                    bankResponse = response.Result;
                    if (bankResponse._BankTransactions.Count > 0)
                    {
                        txnid = bankResponse._BankTransactions[0].BankTransactionID.ToString();
                        accountid = bankResponse._BankTransactions[0].BankAccount.AccountID.ToString();
                        reponseFlag = true;
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += bankResponse._BankTransactions[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
                else if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == true)
                {
                    if (TransactionImporter.TransactionImporter.refnoflag == true)
                    {
                        var response =  res.UpdateBankTransactionAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, CommonUtilities.GetInstance().existTxnId, xeroBank);
                        response.Wait();
                        bankResponse = response.Result;
                        reponseFlag = true;
                    }
                    else
                    {
                        var response =  res.CreateBankTransactionAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, bank);
                        response.Wait();
                        bankResponse = response.Result;
                        reponseFlag = true;
                    }

                    if (bankResponse._BankTransactions != null)
                    {
                        txnid = bankResponse._BankTransactions[0].BankTransactionID.ToString();
                        accountid = bankResponse._BankTransactions[0].BankAccount.AccountID.ToString();
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += bankResponse._BankTransactions[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                //CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
            }

            CommonUtilities.GetInstance().xeroMessage = statusMessage;
            CommonUtilities.GetInstance().xeroTxnId = txnid;
            CommonUtilities.GetInstance().xeroBankAccountTxnId = accountid;

            return reponseFlag;
        }

        #endregion
    }
}
