﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using System.Linq;
using Xero.NetStandard.OAuth2.Model;

namespace DataProcessingBlocks
{
    class ImportXeroInvoiceClass
    {
        private static ImportXeroInvoiceClass m_ImportXeroInvoiceClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportXeroInvoiceClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportXeroInvoiceClass GetInstance()
        {
            if (m_ImportXeroInvoiceClass == null)
                m_ImportXeroInvoiceClass = new ImportXeroInvoiceClass();
            return m_ImportXeroInvoiceClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Journal QuickBooks collection </returns>


        public Invoices ImportInvoiceXeroData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Customer Entry collections.

            Invoices invoicelist = new Invoices();
            Xero.NetStandard.OAuth2.Model.Invoice invoice = new Xero.NetStandard.OAuth2.Model.Invoice();

            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;

            DateTime InvoiceDt = new DateTime();
            string datevalue = string.Empty;
            string[] date_formats = { "d-M-yyyy", "d/M/yyyy", "d-MM-yyyy", "d/MM/yyyy", "dd-M-yyyy", "dd/M/yyyy", "dd-MM-yyyy", "dd/MM/yyyy",  //for date-month-year
                                      "M-d-yyyy","M/d/yyyy", "MM-d-yyyy","MM/d/yyyy", "M-dd-yyyy", "M/dd/yyyy", "MM-dd-yyyy", "MM/dd/yyyy",    //for month-date-year
                                      "yyyy-M-d", "yyyy/M/d", "yyyy-MM-d", "yyyy/MM/d", "yyyy-M-dd", "yyyy/M/dd", "yyyy-MM-dd", "yyyy/MM/dd"   //for year-month-date
                                    };


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();

            #region For Invoice Entry

            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string inno = "";

                    if (dt.Columns.Contains("InvoiceNumber"))
                    {
                        #region Validations of Invoice Number
                        if (dr["InvoiceNumber"].ToString() != string.Empty)
                        {
                            if (dr["InvoiceNumber"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This InvoiceNumber ( " + dr["InvoiceNumber"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        inno = dr["InvoiceNumber"].ToString().Substring(0, 100);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        inno = dr["InvoiceNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    inno = dr["InvoiceNumber"].ToString();
                                }
                            }
                            else
                            {
                                inno = dr["InvoiceNumber"].ToString();
                            }
                        }


                        #endregion
                    }

                    if (invoice.InvoiceNumber != null && invoice.InvoiceNumber.Equals(inno))
                    {
                        invoice.InvoiceNumber = inno;
                    }
                    else
                    {
                        invoice = new Xero.NetStandard.OAuth2.Model.Invoice();
                        invoice.InvoiceNumber = inno;
                        invoice.LineItems = new List<LineItem>();
                        if (invoicelist._Invoices == null)
                        {
                            invoicelist._Invoices = new List<Xero.NetStandard.OAuth2.Model.Invoice>();
                        }
                        invoicelist._Invoices.Add(invoice);
                    }

                    Contact con = new Contact();

                    if (dt.Columns.Contains("ContactName"))
                    {
                        #region Validations of ContactName
                        if (dr["ContactName"].ToString() != string.Empty)
                        {
                            if (dr["ContactName"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ContactName ( " + dr["ContactName"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        con.Name = dr["ContactName"].ToString().Substring(0, 100);
                                        invoice.Contact = con;

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        con.Name = dr["ContactName"].ToString();
                                        invoice.Contact = con;
                                    }
                                }
                                else
                                {
                                    con.Name = dr["ContactName"].ToString();
                                    invoice.Contact = con;
                                }
                            }
                            else
                            {
                                con.Name = dr["ContactName"].ToString();
                                invoice.Contact = con;
                            }
                        }
                        #endregion
                    }

                    LineItem lineitem = new LineItem();

                    if (dt.Columns.Contains("LineItemsDescription"))
                    {
                        #region Validations of LineItemsDescription
                        if (dr["LineItemsDescription"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsDescription"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsDescription ( " + dr["LineItemsDescription"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        lineitem.Description = dr["LineItemsDescription"].ToString().Substring(0, 25);
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        lineitem.Description = dr["LineItemsDescription"].ToString();
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    lineitem.Description = dr["LineItemsDescription"].ToString();
                                    //invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                lineitem.Description = dr["LineItemsDescription"].ToString();
                                // invoice.LineItems.Add(lineitem);
                            }
                        }


                        #endregion
                    }

                    if (dt.Columns.Contains("LineItemsQuantity"))
                    {
                        #region Validations of LineItemsQuantity
                        double qty = 0;
                        if (dr["LineItemsQuantity"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsQuantity"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsQuantity ( " + dr["LineItemsQuantity"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        qty = Convert.ToDouble(dr["LineItemsQuantity"].ToString().Substring(0, 25));
                                        lineitem.Quantity = (double?)qty;
                                        invoice.LineItems.Add(lineitem);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        qty = Convert.ToDouble(dr["LineItemsQuantity"].ToString());
                                        lineitem.Quantity = (double?)qty;
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    qty = Convert.ToDouble(dr["LineItemsQuantity"].ToString());
                                    lineitem.Quantity = (double?)qty;
                                    // invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                qty = Convert.ToDouble(dr["LineItemsQuantity"].ToString());
                                lineitem.Quantity = (double?)qty;
                                //invoice.LineItems.Add(lineitem);
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineItemsUnitAmount"))
                    {
                        #region Validations of LineItemsUnitAmount
                        double unitamt = 0;
                        if (dr["LineItemsUnitAmount"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsUnitAmount"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsUnitAmount ( " + dr["LineItemsUnitAmount"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        unitamt = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString().Substring(0, 25));
                                        lineitem.UnitAmount = (double?)unitamt;
                                        invoice.LineItems.Add(lineitem);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        unitamt = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString());
                                        lineitem.UnitAmount = (double?)unitamt;
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    unitamt = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString());
                                    lineitem.UnitAmount = (double?)unitamt;
                                    // invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                unitamt = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString());
                                lineitem.UnitAmount = (double?)unitamt;
                                //invoice.LineItems.Add(lineitem);
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineItemsItemCode"))
                    {
                        #region Validations of LineItemsItemCode
                        if (dr["LineItemsItemCode"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsItemCode"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsItemCode ( " + dr["LineItemsItemCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        lineitem.ItemCode = dr["LineItemsItemCode"].ToString().Substring(0, 100);
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        lineitem.ItemCode = dr["LineItemsItemCode"].ToString();
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    lineitem.ItemCode = dr["LineItemsItemCode"].ToString();
                                    //invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                lineitem.ItemCode = dr["LineItemsItemCode"].ToString();
                                //invoice.LineItems.Add(lineitem);
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("LineItemsAccountCode"))
                    {
                        #region Validations of LineItemsAccountCode
                        if (dr["LineItemsAccountCode"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsAccountCode"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsAccountCode ( " + dr["LineItemsAccountCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        lineitem.AccountCode = dr["LineItemsAccountCode"].ToString().Substring(0, 25);

                                        //invoice.LineItems.Add(lineitem);


                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        lineitem.AccountCode = dr["LineItemsAccountCode"].ToString();
                                        //invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    lineitem.AccountCode = dr["LineItemsAccountCode"].ToString();
                                    // invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                lineitem.AccountCode = dr["LineItemsAccountCode"].ToString();
                                //invoice.LineItems.Add(lineitem);
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineItemsTaxType"))
                    {
                        #region Validations of LineItemsTaxType
                        if (dr["LineItemsTaxType"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsTaxType"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsTaxType ( " + dr["LineItemsTaxType"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        lineitem.TaxType = dr["LineItemsTaxType"].ToString().Substring(0, 25);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        lineitem.TaxType = dr["LineItemsTaxType"].ToString();
                                    }
                                }
                                else
                                {
                                    lineitem.TaxType = dr["LineItemsTaxType"].ToString();
                                }
                            }
                            else
                            {
                                lineitem.TaxType = dr["LineItemsTaxType"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineItemsTaxAmount"))
                    {
                        #region Validations of LineItemsTaxAmount
                        double taxamt = 0;
                        if (dr["LineItemsTaxAmount"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsTaxAmount"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsTaxAmount ( " + dr["LineItemsTaxAmount"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        taxamt = Convert.ToDouble(dr["LineItemsTaxAmount"].ToString().Substring(0, 25));
                                        lineitem.TaxAmount = taxamt;
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        taxamt = Convert.ToDouble(dr["LineItemsTaxAmount"].ToString());
                                        lineitem.TaxAmount = taxamt;
                                        //invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    taxamt = Convert.ToDouble(dr["LineItemsTaxAmount"].ToString());
                                    lineitem.TaxAmount = taxamt;
                                    //invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                taxamt = Convert.ToDouble(dr["LineItemsTaxAmount"].ToString());
                                lineitem.TaxAmount = taxamt;
                                // invoice.LineItems.Add(lineitem);
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineItemsLineAmount"))
                    {
                        #region Validations of LineItemsLineAmount
                        decimal lineamt = 0;
                        if (dr["LineItemsLineAmount"].ToString() != string.Empty)
                        {
                            if (dr["LineItemsLineAmount"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineItemsLineAmount ( " + dr["LineItemsLineAmount"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString().Substring(0, 25));
                                        lineitem.LineAmount = (double?)lineamt;
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString());
                                        lineitem.LineAmount = (double?)lineamt;
                                        // invoice.LineItems.Add(lineitem);
                                    }
                                }
                                else
                                {
                                    lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString());
                                    lineitem.LineAmount = (double?)lineamt;
                                    // invoice.LineItems.Add(lineitem);
                                }
                            }
                            else
                            {
                                lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString());
                                lineitem.LineAmount = (double?)lineamt;
                                //invoice.LineItems.Add(lineitem);
                            }
                        }


                        #endregion
                    }

                    //Axis 141 
                    if (dt.Columns.Contains("TrackingName1"))
                    {
                        #region Validations of TrackingName1
                        Guid trackingCategoryGuid1 = new Guid();
                        string trackingCategoryName1 = "";
                        string optionName1 = "";

                        if (dr["TrackingName1"].ToString() != string.Empty)
                        {
                            if (dr["TrackingName1"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TrackingName1 ( " + dr["TrackingName1"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        trackingCategoryName1 = dr["TrackingName1"].ToString();
                                        if (dr["TrackingOption1"].ToString() != string.Empty)
                                        {
                                            optionName1 = dr["TrackingOption1"].ToString();
                                        }
                                        lineitem.Tracking.Add(new LineItemTracking()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid1,
                                            Name = trackingCategoryName1,
                                            Option = optionName1
                                        });
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        trackingCategoryName1 = dr["TrackingName1"].ToString();
                                        if (dr["TrackingOption1"].ToString() != string.Empty)
                                        {
                                            optionName1 = dr["TrackingOption1"].ToString();
                                        }
                                        lineitem.Tracking.Add(new LineItemTracking()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid1,
                                            Name = trackingCategoryName1,
                                            Option = optionName1
                                        });
                                    }
                                }
                                else
                                {
                                    trackingCategoryName1 = dr["TrackingName1"].ToString();
                                    if (dr["TrackingOption1"].ToString() != string.Empty)
                                    {
                                        optionName1 = dr["TrackingOption1"].ToString();
                                    }
                                    lineitem.Tracking.Add(new LineItemTracking()
                                    {
                                        TrackingCategoryID = trackingCategoryGuid1,
                                        Name = trackingCategoryName1,
                                        Option = optionName1
                                    });
                                }
                            }
                            else
                            {
                                trackingCategoryName1 = dr["TrackingName1"].ToString();
                                if (dr["TrackingOption1"].ToString() != string.Empty)
                                {
                                    optionName1 = dr["TrackingOption1"].ToString();
                                }
                                lineitem.Tracking.Add(new LineItemTracking()
                                {
                                    TrackingCategoryID = trackingCategoryGuid1,
                                    Name = trackingCategoryName1,
                                    Option = optionName1
                                });
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("TrackingName2"))
                    {
                        #region Validations of TrackingName2
                        Guid trackingCategoryGuid2 = new Guid();
                        string trackingCategoryName2 = "";
                        string optionName2 = "";

                        if (dr["TrackingName2"].ToString() != string.Empty)
                        {
                            if (dr["TrackingName2"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TrackingName2 ( " + dr["TrackingName2"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        trackingCategoryName2 = dr["TrackingName2"].ToString();
                                        if (dr["TrackingOption2"].ToString() != string.Empty)
                                        {
                                            optionName2 = dr["TrackingOption2"].ToString();
                                        }
                                        lineitem.Tracking.Add(new LineItemTracking()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid2,
                                            Name = trackingCategoryName2,
                                            Option = optionName2
                                        });
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        trackingCategoryName2 = dr["TrackingName2"].ToString();
                                        if (dr["TrackingOption2"].ToString() != string.Empty)
                                        {
                                            optionName2 = dr["TrackingOption2"].ToString();
                                        }
                                        lineitem.Tracking.Add(new LineItemTracking()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid2,
                                            Name = trackingCategoryName2,
                                            Option = optionName2
                                        });
                                    }
                                }
                                else
                                {
                                    trackingCategoryName2 = dr["TrackingName2"].ToString();
                                    if (dr["TrackingOption2"].ToString() != string.Empty)
                                    {
                                        optionName2 = dr["TrackingOption2"].ToString();
                                    }
                                    lineitem.Tracking.Add(new LineItemTracking()
                                    {
                                        TrackingCategoryID = trackingCategoryGuid2,
                                        Name = trackingCategoryName2,
                                        Option = optionName2
                                    });
                                }
                            }
                            else
                            {
                                trackingCategoryName2 = dr["TrackingName2"].ToString();
                                if (dr["TrackingOption2"].ToString() != string.Empty)
                                {
                                    optionName2 = dr["TrackingOption2"].ToString();
                                }
                                lineitem.Tracking.Add(new LineItemTracking()
                                {
                                    TrackingCategoryID = trackingCategoryGuid2,
                                    Name = trackingCategoryName2,
                                    Option = optionName2
                                });
                            }
                        }


                        #endregion
                    }
                    //Axis 141 ends

                    invoice.LineItems.Add(lineitem);

                    if (dt.Columns.Contains("Date"))
                    {
                        #region Validation of Date
                        if (dr["Date"].ToString() != "<None>" || dr["Date"].ToString() != string.Empty)
                        {
                            datevalue = dr["Date"].ToString();
                            datevalue = datevalue.Trim();
                            if (datevalue.Contains(" "))//time present
                            {
                                int pos = datevalue.IndexOf(" ");
                                datevalue = datevalue.Substring(0, pos);
                            }
                            DateTime temp_date = new DateTime();
                            bool flg1 = false;


                            for (int i = 0; i < date_formats.Length; i++)
                            {
                                flg1 = DateTime.TryParseExact(datevalue, date_formats[i], CultureInfo.InvariantCulture, DateTimeStyles.None, out temp_date);
                                if (flg1 == true)
                                {
                                    break;
                                }
                            }
                            if (flg1 == true)
                            {
                                InvoiceDt = new DateTime(temp_date.Year, temp_date.Month, temp_date.Day);
                                invoice.Date = InvoiceDt;
                            }
                            else
                            {
                                DateTime dttest = DateTime.Today;
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Date (" + datevalue + ") is not valid for this mapping.Please Enter Date in format of (yyyy-MM-dd'T'HH':'mm':'ss).If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        invoice.Date = dttest;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        invoice.Date = dttest;
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("DueDate"))
                    {
                        #region Validation of DueDate
                        if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                        {

                            datevalue = dr["DueDate"].ToString();
                            datevalue = datevalue.Trim();
                            if (datevalue.Contains(" "))//time present
                            {
                                int pos = datevalue.IndexOf(" ");
                                datevalue = datevalue.Substring(0, pos);
                            }
                            DateTime temp_date = new DateTime();
                            bool flg1 = false;


                            for (int i = 0; i < date_formats.Length; i++)
                            {
                                flg1 = DateTime.TryParseExact(datevalue, date_formats[i], CultureInfo.InvariantCulture, DateTimeStyles.None, out temp_date);
                                if (flg1 == true)
                                {
                                    break;
                                }
                            }
                            if (flg1 == true)
                            {
                                InvoiceDt = new DateTime(temp_date.Year, temp_date.Month, temp_date.Day);
                                invoice.DueDate = InvoiceDt;
                            }
                            else
                            {
                                DateTime dttest = DateTime.Today;
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Date (" + datevalue + ") is not valid for this mapping.Please Enter Date in format of (yyyy-MM-dd'T'HH':'mm':'ss).If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        invoice.DueDate = dttest;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        invoice.DueDate = dttest;

                                    }
                                }
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineAmountTypes"))
                    {
                        string valid_line = dr["LineAmountTypes"].ToString().ToLower().Trim();

                        #region valdation for LineAmountTypes
                        if (valid_line != string.Empty)
                        {
                            if (valid_line.Equals("exclusive"))
                            {
                                invoice.LineAmountTypes = LineAmountTypes.Exclusive;
                            }
                            else if (valid_line.Equals("inclusive"))
                            {
                                invoice.LineAmountTypes = LineAmountTypes.Inclusive;
                            }
                            else if (valid_line.Equals("notax"))
                            {
                                invoice.LineAmountTypes = LineAmountTypes.NoTax;
                            }
                            else
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineAmountTypes value ( " + dr["LineAmountTypes"].ToString() + " )  is invalid .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        invoice.LineAmountTypes = LineAmountTypes.Exclusive;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        invoice.LineAmountTypes = LineAmountTypes.Exclusive;
                                    }
                                }
                                else
                                {
                                    invoice.LineAmountTypes = LineAmountTypes.Exclusive;
                                }

                            }
                        }

                        #endregion
                    }
                    if (dt.Columns.Contains("Refernces"))
                    {
                        #region Validations of Reference
                        if (dr["Refernces"].ToString() != string.Empty)
                        {
                            if (dr["Refernces"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Reference ( " + dr["Refernces"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        invoice.Reference = dr["Refernces"].ToString().Substring(0, 100);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        invoice.Reference = dr["Refernces"].ToString();
                                    }
                                }
                                else
                                {
                                    invoice.Reference = dr["Refernces"].ToString();
                                }
                            }
                            else
                            {
                                invoice.Reference = dr["Refernces"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Url"))
                    {
                        #region Validations of Url
                        if (dr["Url"].ToString() != string.Empty)
                        {
                            if (dr["Url"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Url ( " + dr["Url"].ToString() + " ) is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        invoice.Url = dr["Url"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        invoice.Url = dr["Url"].ToString();
                                    }
                                }
                                else
                                {
                                    invoice.Url = dr["Url"].ToString();
                                }
                            }
                            else
                            {
                                invoice.Url = dr["Url"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("CurrencyCode"))
                    {
                        #region Validations of CurrencyRate
                        if (dr["CurrencyCode"].ToString() != string.Empty && dr["CurrencyCode"].ToString() != null)
                        {
                            string invoiceStatus = string.Empty;
                            if (dr["CurrencyCode"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CurrencyCode ( " + dr["CurrencyCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string currencyCode = dr["CurrencyCode"].ToString().Substring(0, 100);
                                        invoiceStatus = currencyCode.ToUpper();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string currencyCode = dr["CurrencyCode"].ToString();
                                        invoiceStatus = currencyCode.ToUpper();
                                    }
                                }
                                else
                                {
                                    string currencyCode = dr["CurrencyCode"].ToString();
                                    invoiceStatus = currencyCode.ToUpper();
                                }
                            }
                            else
                            {
                                string currencyCode = dr["CurrencyCode"].ToString();
                                invoiceStatus = currencyCode.ToUpper();
                            }

                            bool checkFlag = Enum.IsDefined(typeof(CurrencyCode), invoiceStatus);
                            if (checkFlag)
                            {
                                invoice.CurrencyCode = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), invoiceStatus);
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Status"))
                    {
                        #region Validations of Status
                        if (dr["Status"].ToString() != string.Empty && dr["Status"].ToString() != null)
                        {
                            string invoiceStatus = string.Empty;
                            if (dr["Status"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Status (" + dr["Status"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string status = dr["Status"].ToString().Substring(0, 25);
                                        invoiceStatus = status.ToUpper();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string status = dr["Status"].ToString();
                                        invoiceStatus = status.ToUpper();
                                    }
                                }
                                else
                                {
                                    string status = dr["Status"].ToString();
                                    invoiceStatus = status.ToUpper();
                                }
                            }
                            else
                            {
                                string status = dr["Status"].ToString();
                                invoiceStatus = status.ToUpper();
                            }

                            bool checkFlag = Enum.IsDefined(typeof(Xero.NetStandard.OAuth2.Model.Invoice.StatusEnum), invoiceStatus);
                            if (checkFlag)
                            {
                                invoice.Status = (Xero.NetStandard.OAuth2.Model.Invoice.StatusEnum)Enum.Parse(typeof(Xero.NetStandard.OAuth2.Model.Invoice.StatusEnum), invoiceStatus);
                            }
                        }
                        #endregion
                    }

                    //tocheck
                    //if (dt.Columns.Contains("SubTotal"))
                    //{
                    //    #region Validations of Subtotal
                    //    if (dr["SubTotal"].ToString() != string.Empty)
                    //    {
                    //        if (dr["SubTotal"].ToString().Length > 100)
                    //        {
                    //            if (isIgnoreAll == false)
                    //            {
                    //                string strMessages = "This SubTotal ( " + dr["SubTotal"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                    //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                    //                if (Convert.ToString(result) == "Cancel")
                    //                {
                    //                    continue;
                    //                }
                    //                if (Convert.ToString(result) == "No")
                    //                {
                    //                    return null;
                    //                }
                    //                if (Convert.ToString(result) == "Ignore")
                    //                {
                    //                    invoice.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString().Substring(0, 25));

                    //                }
                    //                if (Convert.ToString(result) == "Abort")
                    //                {
                    //                    isIgnoreAll = true;
                    //                    invoice.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString());

                    //                }
                    //            }
                    //            else
                    //            {
                    //                invoice.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString());

                    //            }
                    //        }
                    //        else
                    //        {
                    //            invoice.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString());

                    //        }
                    //    }
                    //    #endregion
                    //}
                    //if (dt.Columns.Contains("TotalTax"))
                    //{
                    //    #region Validations of TotalTax
                    //    if (dr["TotalTax"].ToString() != string.Empty)
                    //    {
                    //        if (dr["TotalTax"].ToString().Length > 100)
                    //        {
                    //            if (isIgnoreAll == false)
                    //            {
                    //                string strMessages = "This TotalTax ( " + dr["TotalTax"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                    //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                    //                if (Convert.ToString(result) == "Cancel")
                    //                {
                    //                    continue;
                    //                }
                    //                if (Convert.ToString(result) == "No")
                    //                {
                    //                    return null;
                    //                }
                    //                if (Convert.ToString(result) == "Ignore")
                    //                {
                    //                    invoice.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString().Substring(0, 25));

                    //                }
                    //                if (Convert.ToString(result) == "Abort")
                    //                {
                    //                    isIgnoreAll = true;
                    //                    invoice.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString());

                    //                }
                    //            }
                    //            else
                    //            {
                    //                invoice.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString());

                    //            }
                    //        }
                    //        else
                    //        {
                    //            invoice.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString());

                    //        }
                    //    }
                    //    #endregion
                    //}
                    //if (dt.Columns.Contains("Total"))
                    //{
                    //    #region Validations of Total
                    //    if (dr["Total"].ToString() != string.Empty)
                    //    {
                    //        if (dr["Total"].ToString().Length > 100)
                    //        {
                    //            if (isIgnoreAll == false)
                    //            {
                    //                string strMessages = "This Total ( " + dr["Total"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                    //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                    //                if (Convert.ToString(result) == "Cancel")
                    //                {
                    //                    continue;
                    //                }
                    //                if (Convert.ToString(result) == "No")
                    //                {
                    //                    return null;
                    //                }
                    //                if (Convert.ToString(result) == "Ignore")
                    //                {
                    //                    invoice.Total = Convert.ToDouble(dr["Total"].ToString().Substring(0, 25));

                    //                }
                    //                if (Convert.ToString(result) == "Abort")
                    //                {
                    //                    isIgnoreAll = true;
                    //                    invoice.Total = Convert.ToDouble(dr["Total"].ToString());
                    //                }
                    //            }
                    //            else
                    //            {
                    //                invoice.Total = Convert.ToDouble(dr["Total"].ToString());
                    //            }
                    //        }
                    //        else
                    //        {
                    //            invoice.Total = Convert.ToDouble(dr["Total"].ToString());
                    //        }
                    //    }
                    //    #endregion
                    //}
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return invoicelist;
        }
    }
}
