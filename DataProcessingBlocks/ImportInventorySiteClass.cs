﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDI.Constant;
using System.Data;
using System.ComponentModel;
using System.Windows.Forms;
using QuickBookEntities;

namespace DataProcessingBlocks
{
    public class ImportInventorySiteClass
    {

        private int cnt;
        private static ImportInventorySiteClass m_ImportInventorySiteClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constuctor
        public ImportInventorySiteClass()
        {

        }
        #endregion

        /// <summary>
        /// Create an instance of Import Invoice class
        /// </summary>
        /// <returns></returns>
        public static ImportInventorySiteClass GetInstance()
        {
            if (m_ImportInventorySiteClass == null)
                m_ImportInventorySiteClass = new ImportInventorySiteClass();
            return m_ImportInventorySiteClass;
        }


        /// <summary>
        /// This method is used for validating import data and create inventory site request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Invoice QuickBooks collection </returns>
        public DataProcessingBlocks.InventorySiteQBEntryCollection ImportInventorySiteData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of Invoice Entry collections.
            DataProcessingBlocks.InventorySiteQBEntryCollection coll = new InventorySiteQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For InventorySite Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex) { }

                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;

                    #region Add Inventory 
                    DataProcessingBlocks.InventorySiteQBEntry InventorySite = new InventorySiteQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations for InventorySite
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name ( " + dr["Name"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string Name = dr["Name"].ToString().Substring(0, 31);
                                        InventorySite.Name = Name;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string Name = dr["Name"].ToString();
                                        InventorySite.Name = Name;
                                    }
                                }
                                else
                                {
                                    string Name = dr["Name"].ToString();
                                    InventorySite.Name = Name;
                                }
                            }
                            else
                            {
                                string Name = dr["Name"].ToString();
                                InventorySite.Name = Name;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsPending
                        if (dr["IsActive"].ToString() != "<None>")
                        {
                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                InventorySite.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    InventorySite.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        InventorySite.IsActive = dr["IsActive"].ToString().ToLower(); ;
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(results) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            InventorySite.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventorySite.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventorySite.IsActive = dr["IsActive"].ToString();
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ParentSiteRefFullName"))
                    {
                        #region Validations of Terms Full name
                        if (dr["ParentSiteRefFullName"].ToString() != string.Empty)
                        {
                            string strCust = dr["ParentSiteRefFullName"].ToString();
                            if (strCust.Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ParentSiteRef FullName is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        InventorySite.ParentSiteRef = new QuickBookEntities.ParentSiteRef(dr["ParentSiteRefFullName"].ToString());
                                        if (InventorySite.ParentSiteRef.FullName == null)
                                        {
                                            InventorySite.ParentSiteRef.FullName = null;
                                        }
                                        else
                                        {
                                            InventorySite.ParentSiteRef = new QuickBookEntities.ParentSiteRef(dr["ParentSiteRefFullName"].ToString().Substring(0, 31));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        InventorySite.ParentSiteRef = new QuickBookEntities.ParentSiteRef(dr["ParentSiteRefFullName"].ToString());
                                        if (InventorySite.ParentSiteRef.FullName == null)
                                        {
                                            InventorySite.ParentSiteRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    InventorySite.ParentSiteRef = new QuickBookEntities.ParentSiteRef(dr["ParentSiteRefFullName"].ToString());
                                    if (InventorySite.ParentSiteRef.FullName == null)
                                    {
                                        InventorySite.ParentSiteRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                InventorySite.ParentSiteRef = new QuickBookEntities.ParentSiteRef(dr["ParentSiteRefFullName"].ToString());

                                if (InventorySite.ParentSiteRef.FullName == null)
                                {
                                    InventorySite.ParentSiteRef.FullName = null;
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteDesc"))
                    {
                        #region Validations for SiteDesc
                        if (dr["SiteDesc"].ToString() != string.Empty)
                        {
                            if (dr["SiteDesc"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SiteDesc ( " + dr["SiteDesc"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string SiteDesc = dr["SiteDesc"].ToString().Substring(0, 100);
                                        InventorySite.SiteDesc = SiteDesc;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string SiteDesc = dr["SiteDesc"].ToString();
                                        InventorySite.SiteDesc = SiteDesc;
                                    }
                                }
                                else
                                {
                                    string SiteDesc = dr["SiteDesc"].ToString();
                                    InventorySite.SiteDesc = SiteDesc;
                                }
                            }
                            else
                            {
                                string SiteDesc = dr["SiteDesc"].ToString();
                                InventorySite.SiteDesc = SiteDesc;
                            }
                        }

                        #endregion

                    }
                    if (dt.Columns.Contains("Contact"))
                    {
                        #region Validations of Contact
                        if (dr["Contact"].ToString() != string.Empty)
                        {
                            if (dr["Contact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Contact (" + dr["Contact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        InventorySite.Contact = dr["Contact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        InventorySite.Contact = dr["Contact"].ToString();
                                    }
                                }
                                else
                                {
                                    InventorySite.Contact = dr["Contact"].ToString();
                                }
                            }
                            else
                            {
                                InventorySite.Contact = dr["Contact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        InventorySite.Phone = dr["Phone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        InventorySite.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    InventorySite.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                InventorySite.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        InventorySite.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        InventorySite.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    InventorySite.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                InventorySite.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 1023)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        InventorySite.Email = dr["Email"].ToString().Substring(0, 1023);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        InventorySite.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    InventorySite.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                InventorySite.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    QuickBookEntities.SiteAddress SiteAddresItem = new SiteAddress();

                    if (dt.Columns.Contains("SiteAddr1"))
                    {
                        #region Validations of SiteAddress1
                        if (dr["SiteAddr1"].ToString() != string.Empty)
                        {
                            if (dr["SiteAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Add1 (" + dr["SiteAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.Addr1 = dr["SiteAddr1"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.Addr1 = dr["SiteAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.Addr1 = dr["SiteAddr1"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.Addr1 = dr["SiteAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteAddr2"))
                    {
                        #region Validations of SiteAddress2
                        if (dr["SiteAddr2"].ToString() != string.Empty)
                        {
                            if (dr["SiteAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Add2 (" + dr["SiteAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.Addr2 = dr["SiteAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.Addr2 = dr["SiteAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.Addr2 = dr["SiteAddr2"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.Addr2 = dr["SiteAddr2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteAddr3"))
                    {
                        #region Validations of SiteAddress3
                        if (dr["SiteAddr3"].ToString() != string.Empty)
                        {
                            if (dr["SiteAddr3"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Add3 (" + dr["SiteAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.Addr3 = dr["SiteAddr3"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.Addr3 = dr["SiteAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.Addr3 = dr["SiteAddr3"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.Addr3 = dr["SiteAddr3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteAddr4"))
                    {
                        #region Validations of SiteAddress4
                        if (dr["SiteAddr4"].ToString() != string.Empty)
                        {
                            if (dr["SiteAddr4"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Add4 (" + dr["SiteAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.Addr4 = dr["SiteAddr4"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.Addr4 = dr["SiteAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.Addr4 = dr["SiteAddr4"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.Addr4 = dr["SiteAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteAddr5"))
                    {
                        #region Validations of SiteAddress5
                        if (dr["SiteAddr5"].ToString() != string.Empty)
                        {
                            if (dr["SiteAddr5"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Add5 (" + dr["SiteAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.Addr5 = dr["SiteAddr5"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.Addr5 = dr["SiteAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.Addr5 = dr["SiteAddr5"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.Addr5 = dr["SiteAddr5"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteCity"))
                    {
                        #region Validations of SiteCity
                        if (dr["SiteCity"].ToString() != string.Empty)
                        {
                            if (dr["SiteCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site City (" + dr["SiteCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.City = dr["SiteCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.City = dr["SiteCity"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.City = dr["SiteCity"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.City = dr["SiteCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteState"))
                    {
                        #region Validations of Bill State
                        if (dr["SiteState"].ToString() != string.Empty)
                        {
                            if (dr["SiteState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site State (" + dr["SiteState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.State = dr["SiteState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.State = dr["SiteState"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.State = dr["SiteState"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.State = dr["SiteState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SitePostalCode"))
                    {
                        #region Validations of SitePostalCode
                        if (dr["SitePostalCode"].ToString() != string.Empty)
                        {
                            if (dr["SitePostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Postal Code (" + dr["SitePostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.PostalCode = dr["SitePostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.PostalCode = dr["SitePostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.PostalCode = dr["SitePostalCode"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.PostalCode = dr["SitePostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SiteCountry"))
                    {
                        #region Validations of SiteCountry
                        if (dr["SiteCountry"].ToString() != string.Empty)
                        {
                            if (dr["SiteCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Site Country (" + dr["SiteCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SiteAddresItem.Country = dr["SiteCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SiteAddresItem.Country = dr["SiteCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    SiteAddresItem.Country = dr["SiteCountry"].ToString();
                                }
                            }
                            else
                            {
                                SiteAddresItem.Country = dr["SiteCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (SiteAddresItem.Addr1 != null || SiteAddresItem.Addr2 != null || SiteAddresItem.Addr3 != null || SiteAddresItem.Addr4 != null || SiteAddresItem.Addr5 != null
                        || SiteAddresItem.City != null || SiteAddresItem.Country != null || SiteAddresItem.PostalCode != null || SiteAddresItem.State != null)
                        InventorySite.SiteAddress.Add(SiteAddresItem);

                    coll.Add(InventorySite);
                    #endregion
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return coll;
        }
    }
}
