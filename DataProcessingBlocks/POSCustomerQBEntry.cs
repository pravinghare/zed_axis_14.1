﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using Interop.QBPOSXMLRPLIB;
using DataProcessingBlocks;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("POSCustomerQBEntry", Namespace = "", IsNullable = false)]
    public class POSCustomerQBEntry
    {
       #region Private Member Variable

        private string m_CustomerFullName;
        private string m_CompanyName;
        private string m_CustomerID;
        private string m_CustomerDiscPercent;      
        private string m_CustomerDiscType;
        private string m_CustomerType;
        private string m_Email;
        private string m_IsOkToEMail;
        private string m_FirstName;
        private string m_IsAcceptingChecks;
        private string m_IsUsingChargeAccount;
        private string m_IsUsingWithQB;
        private string m_IsRewardsMember;
        private string m_IsNoShipToBilling;
        private string m_LastName;
        private string m_Notes;
        private string m_Phone;
        private string m_AltPhone;
        private string m_Contact;
        private string m_AltContact;
        private string m_PriceLevelNumber;
        private string m_Salutation;
        private string m_StoreExchangeStatus;
        private string m_TaxCategory;
        private Collection<BillAddress> m_BillAddress = new Collection<BillAddress>();
        private string m_DefaultShipAddress;
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();

        #endregion

        #region Constructor
        public POSCustomerQBEntry()
        {

           
        }
        #endregion

        #region Public Properties              
    
        public string CustomerFullName
        {
            get { return m_CustomerFullName; }
            set { m_CustomerFullName = value; }
        }
        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }

        public string CustomerID
        {
            get { return m_CustomerID; }
            set { m_CustomerID = value; }
        }
       
        public string CustomerDiscPercent
        {
            get { return m_CustomerDiscPercent; }
            set { m_CustomerDiscPercent = value; }
        }

        public string CustomerDiscType
        {
            get { return m_CustomerDiscType; }
            set { m_CustomerDiscType = value; }
        }

        public string CustomerType
        {
            get { return m_CustomerType; }
            set { m_CustomerType = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string IsOkToEMail
        {
            get { return m_IsOkToEMail; }
            set { m_IsOkToEMail = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        public string IsAcceptingChecks
        {
            get { return m_IsAcceptingChecks; }
            set { m_IsAcceptingChecks = value; }
        }

        public string IsUsingChargeAccount
        {
            get { return m_IsUsingChargeAccount; }
            set { m_IsUsingChargeAccount = value; }
        }

        public string IsUsingWithQB
        {
            get { return m_IsUsingWithQB; }
            set { m_IsUsingWithQB = value; }
        }

        public string IsRewardsMember
        {
            get { return m_IsRewardsMember; }
            set { m_IsRewardsMember = value; }
        }

        public string IsNoShipToBilling
        {
            get { return m_IsNoShipToBilling; }
            set { m_IsNoShipToBilling = value; }
        }

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string Phone2
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }

        public string Phone3
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string Phone4
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

        public string PriceLevelNumber
        {
            get { return m_PriceLevelNumber; }
            set { m_PriceLevelNumber = value; }
        }

        public string Salutation
        {
            get { return m_Salutation; }
            set { m_Salutation = value; }
        }
        
        public string TaxCategory
        {
            get { return m_TaxCategory; }
            set { m_TaxCategory = value; }
        }

        [XmlArray("BillAddressREM")]
        public Collection<BillAddress> BillAddress
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        public string DefaultShipAddress
        {
            get { return m_DefaultShipAddress; }
            set { m_DefaultShipAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        #endregion

        #region  Public Methods
        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.POSCustomerQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement customerAddRq = inputXMLDoc.CreateElement("CustomerAddRq");
            qbXMLMsgsRq.AppendChild(customerAddRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement customerAdd = inputXMLDoc.CreateElement("CustomerAdd");
            customerAddRq.AppendChild(customerAdd);


            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<CustomerFullName />", string.Empty);
            requestXML = requestXML.Replace("<CustomerFullName>", string.Empty);
            requestXML = requestXML.Replace("</CustomerFullName>", string.Empty);

            customerAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/CustomerAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                statusMessage += s + ".\n";
                            }
                            if (requesterror != string.Empty)
                            {
                                statusMessage += "The Error location is " + requesterror;
                            }
                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                        else if (statusSeverity == "Info")
                        {
                            foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/CustomerAddRs/CustomerRet"))
                            {
                                CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                            }
                        }
                    }

                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }
            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofCustomer(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }         
        

        //update pos customer

        /// <summary>
       ///This method is used for updating customer information
        /// of existing customer with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="listID"></param>
        /// <returns></returns>
        public bool UpdateCustomerInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string listID)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.POSCustomerQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument inputXMLDoc = new System.Xml.XmlDocument();
            inputXMLDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)inputXMLDoc.DocumentElement).InnerXml;

            inputXMLDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);


            inputXMLDoc.AppendChild(inputXMLDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            inputXMLDoc.AppendChild(inputXMLDoc.CreateProcessingInstruction("qbposxml", "version=\"3.0\""));
            XmlElement qbXML = inputXMLDoc.CreateElement("QBPOSXML");
            inputXMLDoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = inputXMLDoc.CreateElement("QBPOSXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement customerAddRq = inputXMLDoc.CreateElement("CustomerModRq");
            qbXMLMsgsRq.AppendChild(customerAddRq);
            //  customerAddRq.SetAttribute("requestID", "1");
            XmlElement customerAdd = inputXMLDoc.CreateElement("CustomerMod");
            customerAddRq.AppendChild(customerAdd);
           

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<CustomerFullName />", string.Empty);
            requestXML = requestXML.Replace("<CustomerFullName>", string.Empty);
            requestXML = requestXML.Replace("</CustomerFullName>", string.Empty);

            

            customerAdd.InnerXml = requestXML;

            requestText = inputXMLDoc.OuterXml;


            XmlNode firstChild = inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/CustomerModRq/CustomerMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = inputXMLDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/CustomerModRq/CustomerMod").InsertBefore(ListID, firstChild).InnerText = listID;

            ////Create CustomerID aggregate and fill in field values for it
            //System.Xml.XmlElement CustomerID = inputXMLDoc.CreateElement("CustomerID");            
            //inputXMLDoc.SelectSingleNode("/QBPOSXML/QBPOSXMLMsgsRq/CustomerModRq/CustomerMod").InsertBefore(CustomerID, ListID).InnerText = custID;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(inputXMLDoc);
                if (TransactionImporter.TransactionImporter.rdbQBPOSbutton == true)
                {
                   
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, inputXMLDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, inputXMLDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/CustomerModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        TransactionImporter.TransactionImporter.testFlag = true;
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                statusMessage += s + ".\n";
                            }
                            if (requesterror != string.Empty)
                            {
                                statusMessage += "The Error location is " + requesterror;
                            }

                            TransactionImporter.TransactionImporter.testFlag = false;
                        }
                        else if (statusSeverity == "Info")
                        {
                            foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/CustomerModRs/CustomerRet"))
                            {
                                CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                            }
                        }
                    }

                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().POSValidMessageofCustomer(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
        
    }


    public class POSCustomerQBEntryCollection : Collection<POSCustomerQBEntry>
    {

    }

    public enum CustomerDiscType
    {
        None,
        PriceLevel,
        Percentage 
    }


    //public enum PriceLevelNumber
    //{
    //    PriceLevelNumber1 = 1,
    //    PriceLevelNumber2 = 2,
    //    PriceLevelNumber3 = 3,
    //    PriceLevelNumbe4 = 4,
    //    PriceLevelNumber5 = 5
    //}

  public enum StoreExchangeStatus
  {
      Modified,
      Sent, 
      Acknowledged
  }
   
}
