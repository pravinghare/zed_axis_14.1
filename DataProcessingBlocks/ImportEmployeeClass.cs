using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    public class ImportEmployeeClass
    {
        private static ImportEmployeeClass m_ImportEmployeeClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;
        string[] payrollItemwages;

        #region Constructor

        public ImportEmployeeClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import Employee class
        /// </summary>
        /// <returns></returns>
        public static ImportEmployeeClass GetInstance()
        {
            if (m_ImportEmployeeClass == null)
                m_ImportEmployeeClass = new ImportEmployeeClass();
            return m_ImportEmployeeClass;
        }


        /// <summary>
        /// This method is used for validating import data and create Employee and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Employee QuickBooks collection </returns>
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public DataProcessingBlocks.EmployeeQBEntryCollection ImportEmployeeData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            
            //Create an instance of Employee Entry collections.
            DataProcessingBlocks.EmployeeQBEntryCollection coll = new EmployeeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Employee Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime EmployeeDt = new DateTime();
                    string datevalue = string.Empty;

                    //Employee Validation
                    DataProcessingBlocks.EmployeeQBEntry Employee = new EmployeeQBEntry();

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                Employee.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    Employee.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Employee.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Employee.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Employee.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Salutation"))
                    {
                        #region Validations of Salutation
                        if (dr["Salutation"].ToString() != string.Empty)
                        {
                            if (dr["Salutation"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Salutation (" + dr["Salutation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Salutation = dr["Salutation"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Salutation = dr["Salutation"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Salutation = dr["Salutation"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Salutation = dr["Salutation"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FirstName"))
                    {
                        #region Validations of FirstName
                        if (dr["FirstName"].ToString() != string.Empty)
                        {
                            if (dr["FirstName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.FirstName = dr["FirstName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            else
                            {
                                Employee.FirstName = dr["FirstName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MiddleName"))
                    {
                        #region Validations of MiddleName
                        if (dr["MiddleName"].ToString() != string.Empty)
                        {
                            if (dr["MiddleName"].ToString().Length > 5)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.MiddleName = dr["MiddleName"].ToString().Substring(0, 5);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            else
                            {
                                Employee.MiddleName = dr["MiddleName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LastName"))
                    {
                        #region Validations of LastName
                        if (dr["LastName"].ToString() != string.Empty)
                        {
                            if (dr["LastName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LastName (" + dr["LastName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.LastName = dr["LastName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.LastName = dr["LastName"].ToString();
                                }
                            }
                            else
                            {
                                Employee.LastName = dr["LastName"].ToString();
                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 611
                    if (dt.Columns.Contains("SupervisorFullName"))
                    {
                        #region Validations of SupervisorFullName
                        if (dr["SupervisorFullName"].ToString() != string.Empty)
                        {
                            if (dr["SupervisorFullName"].ToString().Length > 400)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Supervisor FullName (" + dr["SupervisorFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.SupervisorRef = new SupervisorRef(dr["SupervisorFullName"].ToString());
                                        if (Employee.SupervisorRef.FullName == null)
                                            Employee.SupervisorRef.FullName = null;
                                        else
                                            Employee.SupervisorRef = new SupervisorRef(dr["SupervisorFullName"].ToString().Substring(0, 400));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.SupervisorRef = new SupervisorRef(dr["SupervisorFullName"].ToString());
                                        if (Employee.SupervisorRef.FullName == null)
                                            Employee.SupervisorRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Employee.SupervisorRef = new SupervisorRef(dr["SupervisorFullName"].ToString());
                                    if (Employee.SupervisorRef.FullName == null)
                                        Employee.SupervisorRef.FullName = null;
                                }
                            }
                            else
                            {
                                Employee.SupervisorRef = new SupervisorRef(dr["SupervisorFullName"].ToString());
                                if (Employee.SupervisorRef.FullName == null)
                                    Employee.SupervisorRef.FullName = null;
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Department"))
                    {
                        #region Validations of Department
                        if (dr["Department"].ToString() != string.Empty)
                        {
                            if (dr["Department"].ToString().Length > 250)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Department (" + dr["Department"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Department = dr["Department"].ToString().Substring(0, 250);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Department = dr["Department"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Department = dr["Department"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Department = dr["Department"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Description"))
                    {
                        #region Validations of Description
                        if (dr["Description"].ToString() != string.Empty)
                        {
                            if (dr["Description"].ToString().Length > 600)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Description (" + dr["Description"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Description = dr["Description"].ToString().Substring(0, 600);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Description = dr["Description"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Description = dr["Description"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Description = dr["Description"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("TargetBonus"))
                    {
                        #region Validations of TargetBonus
                        if (dr["TargetBonus"].ToString() != string.Empty)
                        {
                            if (dr["TargetBonus"].ToString().Length > 150)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TargetBonus (" + dr["TargetBonus"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.TargetBonus = dr["TargetBonus"].ToString().Substring(0, 150);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.TargetBonus = dr["TargetBonus"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.TargetBonus = dr["TargetBonus"].ToString();
                                }
                            }
                            else
                            {
                                Employee.TargetBonus = dr["TargetBonus"].ToString();
                            }
                        }
                        #endregion
                    }

                    QuickBookEntities.EmployeeAddress EmployeeAddressItem = new EmployeeAddress();
                    if (dt.Columns.Contains("EmployeeAddr1"))
                    {
                        #region Validations of Employee Addr1
                        if (dr["EmployeeAddr1"].ToString() != string.Empty)
                        {
                            if (dr["EmployeeAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Employee Add1 (" + dr["EmployeeAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeeAddressItem.Addr1 = dr["EmployeeAddr1"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeeAddressItem.Addr1 = dr["EmployeeAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    EmployeeAddressItem.Addr1 = dr["EmployeeAddr1"].ToString();
                                }
                            }
                            else
                            {
                                EmployeeAddressItem.Addr1 = dr["EmployeeAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeeAddr2"))
                    {
                        #region Validations of Employee Addr2
                        if (dr["EmployeeAddr2"].ToString() != string.Empty)
                        {
                            if (dr["EmployeeAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Employee Add2 (" + dr["EmployeeAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeeAddressItem.Addr2 = dr["EmployeeAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeeAddressItem.Addr2 = dr["EmployeeAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    EmployeeAddressItem.Addr2 = dr["EmployeeAddr2"].ToString();
                                }
                            }
                            else
                            {
                                EmployeeAddressItem.Addr2 = dr["EmployeeAddr2"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("EmployeeCity"))
                    {
                        #region Validations of Employee City
                        if (dr["EmployeeCity"].ToString() != string.Empty)
                        {
                            if (dr["EmployeeCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Employee City (" + dr["EmployeeCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeeAddressItem.City = dr["EmployeeCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeeAddressItem.City = dr["EmployeeCity"].ToString();
                                    }
                                }
                                else
                                    EmployeeAddressItem.City = dr["EmployeeCity"].ToString();
                            }
                            else
                            {
                                EmployeeAddressItem.City = dr["EmployeeCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeeState"))
                    {
                        #region Validations of Employee State
                        if (dr["EmployeeState"].ToString() != string.Empty)
                        {
                            if (dr["EmployeeState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Employee State (" + dr["EmployeeState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeeAddressItem.State = dr["EmployeeState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeeAddressItem.State = dr["EmployeeState"].ToString();
                                    }
                                }
                                else
                                {
                                    EmployeeAddressItem.State = dr["EmployeeState"].ToString();
                                }
                            }
                            else
                            {
                                EmployeeAddressItem.State = dr["EmployeeState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePostalCode"))
                    {
                        #region Validations of Employee Postal Code
                        if (dr["EmployeePostalCode"].ToString() != string.Empty)
                        {
                            if (dr["EmployeePostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Employee Postal Code (" + dr["EmployeePostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeeAddressItem.PostalCode = dr["EmployeePostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeeAddressItem.PostalCode = dr["EmployeePostalCode"].ToString();
                                    }
                                }
                                else
                                    EmployeeAddressItem.PostalCode = dr["EmployeePostalCode"].ToString();
                            }
                            else
                            {
                                EmployeeAddressItem.PostalCode = dr["EmployeePostalCode"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (EmployeeAddressItem.Addr1 != null || EmployeeAddressItem.Addr2 != null || EmployeeAddressItem.City != null || EmployeeAddressItem.PostalCode != null || EmployeeAddressItem.State != null)
                        Employee.EmployeeAddress.Add(EmployeeAddressItem);

                    if (dt.Columns.Contains("PrintAs"))
                    {
                        #region Validations of PrintAs
                        if (dr["PrintAs"].ToString() != string.Empty)
                        {
                            if (dr["PrintAs"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PrintAs (" + dr["PrintAs"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.PrintAs = dr["PrintAs"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.PrintAs = dr["PrintAs"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.PrintAs = dr["PrintAs"].ToString();
                                }
                            }
                            else
                            {
                                Employee.PrintAs = dr["PrintAs"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Phone = dr["Phone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Mobile"))
                    {
                        #region Validations of Mobile
                        if (dr["Mobile"].ToString() != string.Empty)
                        {
                            if (dr["Mobile"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Mobile (" + dr["Mobile"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Mobile = dr["Mobile"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Mobile = dr["Mobile"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Mobile = dr["Mobile"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Mobile = dr["Mobile"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Pager"))
                    {
                        #region Validations of Pager
                        if (dr["Pager"].ToString() != string.Empty)
                        {
                            if (dr["Pager"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Pager (" + dr["Pager"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Pager = dr["Pager"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Pager = dr["Pager"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Pager = dr["Pager"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Pager = dr["Pager"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PagerPIN"))
                    {
                        #region Validations of PagerPIN
                        if (dr["PagerPIN"].ToString() != string.Empty)
                        {
                            if (dr["PagerPIN"].ToString().Length > 10)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PagerPIN (" + dr["PagerPIN"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.PagerPIN = dr["PagerPIN"].ToString().Substring(0, 10);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.PagerPIN = dr["PagerPIN"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.PagerPIN = dr["PagerPIN"].ToString();
                                }
                            }
                            else
                            {
                                Employee.PagerPIN = dr["PagerPIN"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AltPhone"))
                    {
                        #region Validations of AltPhone
                        if (dr["AltPhone"].ToString() != string.Empty)
                        {
                            if (dr["AltPhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltPhone (" + dr["AltPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.AltPhone = dr["AltPhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.AltPhone = dr["AltPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.AltPhone = dr["AltPhone"].ToString();
                                }
                            }
                            else
                            {
                                Employee.AltPhone = dr["AltPhone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SSN"))
                    {
                        #region Validations of SSN
                        if (dr["SSN"].ToString() != string.Empty)
                        {
                            if (dr["SSN"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SSN (" + dr["SSN"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.SSN = dr["SSN"].ToString().Substring(0, 15);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.SSN = dr["SSN"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.SSN = dr["SSN"].ToString();
                                }
                            }
                            else
                            {
                                Employee.SSN = dr["SSN"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 1023)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Email = dr["Email"].ToString().Substring(0, 1023);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                Employee.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 611
                    EmergencyContacts EmergencyContactsItem = new EmergencyContacts();

                    PrimaryContact PrimaryContactItem = new PrimaryContact();
                    if (dt.Columns.Contains("EmergencyPrimaryContactName"))
                    {
                        #region Validations of EmergencyPrimaryContactName
                        if (dr["EmergencyPrimaryContactName"].ToString() != string.Empty)
                        {
                            if (dr["EmergencyPrimaryContactName"].ToString().Length > 250)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmergencyPrimaryContactName (" + dr["EmergencyPrimaryContactName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        PrimaryContactItem.ContactName = dr["EmergencyPrimaryContactName"].ToString().Substring(0, 250);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        PrimaryContactItem.ContactName = dr["EmergencyPrimaryContactName"].ToString();
                                    }
                                }
                                else
                                {
                                    PrimaryContactItem.ContactName = dr["EmergencyPrimaryContactName"].ToString();
                                }
                            }
                            else
                            {
                                PrimaryContactItem.ContactName = dr["EmergencyPrimaryContactName"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("EmergencyPrimaryContactValue"))
                    {
                        #region Validations of EmergencyPrimaryContactValue
                        if (dr["EmergencyPrimaryContactValue"].ToString() != string.Empty)
                        {
                            if (dr["EmergencyPrimaryContactValue"].ToString().Length > 250)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmergencyPrimaryContactValue (" + dr["EmergencyPrimaryContactValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        PrimaryContactItem.ContactValue = dr["EmergencyPrimaryContactValue"].ToString().Substring(0, 250);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        PrimaryContactItem.ContactValue = dr["EmergencyPrimaryContactValue"].ToString();
                                    }
                                }
                                else
                                {
                                    PrimaryContactItem.ContactValue = dr["EmergencyPrimaryContactValue"].ToString();
                                }
                            }
                            else
                            {
                                PrimaryContactItem.ContactValue = dr["EmergencyPrimaryContactValue"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("EmergencyPrimaryContactRelation"))
                    {
                        #region Validations of EmergencyPrimaryContactRelation
                        if (dr["EmergencyPrimaryContactRelation"].ToString() != string.Empty)
                        {
                            try
                            {
                                PrimaryContactItem.Relation = Convert.ToString((DataProcessingBlocks.Relation)Enum.Parse(typeof(DataProcessingBlocks.Relation), dr["EmergencyPrimaryContactRelation"].ToString(), true));
                            }
                            catch
                            {
                                PrimaryContactItem.Relation = dr["EmergencyPrimaryContactRelation"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (PrimaryContactItem.ContactName != null || PrimaryContactItem.ContactValue != null || PrimaryContactItem.Relation != null )
                        EmergencyContactsItem.PrimaryContact.Add(PrimaryContactItem);


                    SecondaryContact SecondaryContactItem = new SecondaryContact();
                    if (dt.Columns.Contains("EmergencySecondaryContactName"))
                    {
                        #region Validations of EmergencySecondaryContactName
                        if (dr["EmergencySecondaryContactName"].ToString() != string.Empty)
                        {
                            if (dr["EmergencySecondaryContactName"].ToString().Length > 250)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmergencySecondaryContactName (" + dr["EmergencySecondaryContactName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SecondaryContactItem.ContactName = dr["EmergencySecondaryContactName"].ToString().Substring(0, 250);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SecondaryContactItem.ContactName = dr["EmergencySecondaryContactName"].ToString();
                                    }
                                }
                                else
                                {
                                    SecondaryContactItem.ContactName = dr["EmergencySecondaryContactName"].ToString();
                                }
                            }
                            else
                            {
                                SecondaryContactItem.ContactName = dr["EmergencySecondaryContactName"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("EmergencySecondaryContactValue"))
                    {
                        #region Validations of EmergencySecondaryContactValue
                        if (dr["EmergencySecondaryContactValue"].ToString() != string.Empty)
                        {
                            if (dr["EmergencySecondaryContactValue"].ToString().Length > 250)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmergencySecondaryContactValue (" + dr["EmergencySecondaryContactValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SecondaryContactItem.ContactValue = dr["EmergencySecondaryContactValue"].ToString().Substring(0, 250);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SecondaryContactItem.ContactValue = dr["EmergencySecondaryContactValue"].ToString();
                                    }
                                }
                                else
                                {
                                    SecondaryContactItem.ContactValue = dr["EmergencySecondaryContactValue"].ToString();
                                }
                            }
                            else
                            {
                                SecondaryContactItem.ContactValue = dr["EmergencySecondaryContactValue"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("EmergencySecondaryContactRelation"))
                    {
                        #region Validations of EmergencySecondaryContactRelation
                        if (dr["EmergencySecondaryContactRelation"].ToString() != string.Empty)
                        {
                            try
                            {
                                SecondaryContactItem.Relation = Convert.ToString((DataProcessingBlocks.Relation)Enum.Parse(typeof(DataProcessingBlocks.Relation), dr["EmergencySecondaryContactRelation"].ToString(), true));
                            }
                            catch
                            {
                                SecondaryContactItem.Relation = dr["EmergencySecondaryContactRelation"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (SecondaryContactItem.ContactName != null || SecondaryContactItem.ContactValue != null || SecondaryContactItem.Relation != null)
                        EmergencyContactsItem.SecondaryContact.Add(SecondaryContactItem);

                    if (EmergencyContactsItem.PrimaryContact != null || EmergencyContactsItem.SecondaryContact != null)
                        Employee.EmergencyContacts.Add(EmergencyContactsItem);



                    if (dt.Columns.Contains("EmployeeType"))
                    {
                        #region Validations of EmployeeType
                        if (dr["EmployeeType"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.EmployeeType = Convert.ToString((DataProcessingBlocks.EmployeeType)Enum.Parse(typeof(DataProcessingBlocks.EmployeeType), dr["EmployeeType"].ToString(), true));
                            }
                            catch
                            {
                                Employee.EmployeeType = dr["EmployeeType"].ToString();
                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 611
                    if (dt.Columns.Contains("PartOrFullTime"))
                    {
                        #region Validations of PartOrFullTime
                        if (dr["PartOrFullTime"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.PartOrFullTime = Convert.ToString((DataProcessingBlocks.PartOrFullTime)Enum.Parse(typeof(DataProcessingBlocks.PartOrFullTime), dr["PartOrFullTime"].ToString(), true));
                            }
                            catch
                            {
                                Employee.PartOrFullTime = dr["PartOrFullTime"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Exempt"))
                    {
                        #region Validations of Exempt
                        if (dr["Exempt"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.Exempt = Convert.ToString((DataProcessingBlocks.Exempt)Enum.Parse(typeof(DataProcessingBlocks.Exempt), dr["Exempt"].ToString(), true));
                            }
                            catch
                            {
                                Employee.Exempt = dr["Exempt"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("KeyEmployee"))
                    {
                        #region Validations of KeyEmployee
                        if (dr["KeyEmployee"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.KeyEmployee = Convert.ToString((DataProcessingBlocks.KeyEmployee)Enum.Parse(typeof(DataProcessingBlocks.KeyEmployee), dr["KeyEmployee"].ToString(), true));
                            }
                            catch
                            {
                                Employee.KeyEmployee = dr["KeyEmployee"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("Gender"))
                    {
                        #region Validations of Gender
                        if (dr["Gender"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.Gender = Convert.ToString((DataProcessingBlocks.Gender)Enum.Parse(typeof(DataProcessingBlocks.Gender), dr["Gender"].ToString(), true));
                            }
                            catch
                            {
                                Employee.Gender = dr["Gender"].ToString();
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("HiredDate"))
                    {
                        #region validations of HiredDate
                        if (dr["HiredDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["HiredDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This HiredDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Employee.HiredDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.HiredDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Employee.HiredDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    Employee.HiredDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                Employee.HiredDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }
                    //P Axis 13.1 : issue 611
                    if (dt.Columns.Contains("OriginalHireDate"))
                    {
                        #region validations of OriginalHireDate
                        if (dr["OriginalHireDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["OriginalHireDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;
                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This OriginalHireDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Employee.OriginalHireDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.OriginalHireDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Employee.OriginalHireDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    Employee.OriginalHireDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                Employee.OriginalHireDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("AdjustedServiceDate"))
                    {
                        #region validations of AdjustedServiceDate
                        if (dr["AdjustedServiceDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["AdjustedServiceDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AdjustedServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Employee.AdjustedServiceDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.AdjustedServiceDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Employee.AdjustedServiceDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    Employee.AdjustedServiceDate = dttest.ToString("yyyy-MM-dd");
                                }
                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                Employee.AdjustedServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("ReleasedDate"))
                    {
                        #region validations of ReleasedDate
                        if (dr["ReleasedDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["ReleasedDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ReleasedDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Employee.ReleasedDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.ReleasedDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Employee.ReleasedDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    Employee.ReleasedDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                Employee.ReleasedDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BirthDate"))
                    {
                        #region validations of BirthDate
                        if (dr["BirthDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["BirthDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BirthDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Employee.BirthDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.BirthDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Employee.BirthDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    Employee.BirthDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                Employee.BirthDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }
                    //P Axis 13.1 : issue 611
                    if (dt.Columns.Contains("USCitizen"))
                    {
                        #region Validations of USCitizen
                        if (dr["USCitizen"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.USCitizen = Convert.ToString((DataProcessingBlocks.USCitizen)Enum.Parse(typeof(DataProcessingBlocks.USCitizen), dr["USCitizen"].ToString(), true));
                            }
                            catch
                            {
                                Employee.USCitizen = dr["USCitizen"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Ethnicity"))
                    {
                        #region Validations of Ethnicity
                        if (dr["Ethnicity"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.Ethnicity = Convert.ToString((DataProcessingBlocks.Ethnicity)Enum.Parse(typeof(DataProcessingBlocks.Ethnicity), dr["Ethnicity"].ToString(), true));
                            }
                            catch
                            {
                                Employee.Ethnicity = dr["Ethnicity"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Disabled"))
                    {
                        #region Validations of Disabled
                        if (dr["Disabled"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.Disabled = Convert.ToString((DataProcessingBlocks.Disabled)Enum.Parse(typeof(DataProcessingBlocks.Disabled), dr["Disabled"].ToString(), true));
                            }
                            catch
                            {
                                Employee.Disabled = dr["Disabled"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("DisabilityDesc"))
                    {
                        #region Validations of DisabilityDesc
                        if (dr["DisabilityDesc"].ToString() != string.Empty)
                        {
                            if (dr["DisabilityDesc"].ToString().Length > 400)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DisabilityDesc (" + dr["DisabilityDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.DisabilityDesc = dr["DisabilityDesc"].ToString().Substring(0, 400);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.DisabilityDesc = dr["DisabilityDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    Employee.DisabilityDesc = dr["DisabilityDesc"].ToString();
                                }
                            }
                            else
                            {
                                Employee.DisabilityDesc = dr["DisabilityDesc"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("OnFile"))
                    {
                        #region Validations of OnFile
                        if (dr["OnFile"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.OnFile = Convert.ToString((DataProcessingBlocks.OnFile)Enum.Parse(typeof(DataProcessingBlocks.OnFile), dr["OnFile"].ToString(), true));
                            }
                            catch
                            {
                                Employee.OnFile = dr["OnFile"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("WorkAuthExpireDate"))
                    {
                        #region validations of WorkAuthExpireDate
                        if (dr["WorkAuthExpireDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["WorkAuthExpireDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This WorkAuthExpireDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Employee.WorkAuthExpireDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Employee.WorkAuthExpireDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Employee.WorkAuthExpireDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    Employee.WorkAuthExpireDate = dttest.ToString("yyyy-MM-dd");
                                }
                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                Employee.WorkAuthExpireDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("USVeteran"))
                    {
                        #region Validations of USVeteran
                        if (dr["USVeteran"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.USVeteran = Convert.ToString((DataProcessingBlocks.USVeteran)Enum.Parse(typeof(DataProcessingBlocks.USVeteran), dr["USVeteran"].ToString(), true));
                            }
                            catch
                            {
                                Employee.USVeteran = dr["USVeteran"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("MilitaryStatus"))
                    {
                        #region Validations of MilitaryStatus
                        if (dr["MilitaryStatus"].ToString() != string.Empty)
                        {
                            try
                            {
                                Employee.MilitaryStatus = Convert.ToString((DataProcessingBlocks.MilitaryStatus)Enum.Parse(typeof(DataProcessingBlocks.MilitaryStatus), dr["MilitaryStatus"].ToString(), true));
                            }
                            catch
                            {
                                Employee.MilitaryStatus = dr["MilitaryStatus"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("AccountNumber"))
                    {
                        #region Validations of Account Number
                        if (dr["AccountNumber"].ToString() != string.Empty)
                        {
                            if (dr["AccountNumber"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Account Number (" + dr["AccountNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.AccountNumber = dr["AccountNumber"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.AccountNumber = dr["AccountNumber"].ToString();
                                    }
                                }
                                else
                                    Employee.AccountNumber = dr["AccountNumber"].ToString();
                            }
                            else
                            {
                                Employee.AccountNumber = dr["AccountNumber"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["JobDNotesesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.Notes = dr["Notes"].ToString().Substring(0, 4095);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                    Employee.Notes = dr["Notes"].ToString();
                            }
                            else
                            {
                                Employee.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillingRateFullName"))
                    {
                        #region Validations of BillingRateRef FullName
                        if (dr["BillingRateFullName"].ToString() != string.Empty)
                        {
                            if (dr["BillingRateFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillingRateRef FullName  (" + dr["BillingRateFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Employee.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                        if (Employee.BillingRateRef.FullName == null)
                                            Employee.BillingRateRef.FullName = null;
                                        else
                                            Employee.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Employee.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                        if (Employee.BillingRateRef.FullName == null)
                                            Employee.BillingRateRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Employee.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                    if (Employee.BillingRateRef.FullName == null)
                                        Employee.BillingRateRef.FullName = null;
                                }
                            }
                            else
                            {
                                Employee.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                if (Employee.BillingRateRef.FullName == null)
                                    Employee.BillingRateRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    EmployeePayrollInfo EmployeePayrollInfoItem = new EmployeePayrollInfo();

                    if (dt.Columns.Contains("EmployeePayrollPayPeriod"))
                    {
                        #region  Validations of EmployeePayrollInfo PayPeriod
                        if (dr["EmployeePayrollPayPeriod"].ToString() != string.Empty)
                        {
                            try
                            {
                                EmployeePayrollInfoItem.PayPeriod = Convert.ToString((DataProcessingBlocks.PayPeriod)Enum.Parse(typeof(DataProcessingBlocks.PayPeriod), dr["EmployeePayrollPayPeriod"].ToString(), true));
                            }
                            catch
                            {
                                EmployeePayrollInfoItem.PayPeriod = dr["EmployeePayrollPayPeriod"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollClassFullName"))
                    {
                        #region Validations of EmployeePayrollInfo ClassRef FullName
                        if (dr["EmployeePayrollClassFullName"].ToString() != string.Empty)
                        {
                            if (dr["EmployeePayrollClassFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollInfo ClassRef FullName  (" + dr["EmployeePayrollClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        EmployeePayrollInfoItem.ClassRef = new ClassRef(dr["EmployeePayrollClassFullName"].ToString());
                                        if (EmployeePayrollInfoItem.ClassRef.FullName == null)
                                            EmployeePayrollInfoItem.ClassRef.FullName = null;
                                        else
                                            EmployeePayrollInfoItem.ClassRef = new ClassRef(dr["EmployeePayrollClassFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        EmployeePayrollInfoItem.ClassRef = new ClassRef(dr["EmployeePayrollClassFullName"].ToString());
                                        if (EmployeePayrollInfoItem.ClassRef.FullName == null)
                                            EmployeePayrollInfoItem.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    EmployeePayrollInfoItem.ClassRef = new ClassRef(dr["EmployeePayrollClassFullName"].ToString());
                                    if (EmployeePayrollInfoItem.ClassRef.FullName == null)
                                        EmployeePayrollInfoItem.ClassRef.FullName = null;
                                }
                            }
                            else
                            {
                                EmployeePayrollInfoItem.ClassRef = new ClassRef(dr["EmployeePayrollClassFullName"].ToString());
                                if (EmployeePayrollInfoItem.ClassRef.FullName == null)
                                    EmployeePayrollInfoItem.ClassRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollClearEarnings"))
                    {
                        #region Validations of EmployeePayrollInfo ClearEarnings
                        if (dr["EmployeePayrollClearEarnings"].ToString() != "<None>" || dr["EmployeePayrollClearEarnings"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["EmployeePayrollClearEarnings"].ToString(), out result))
                            {
                                EmployeePayrollInfoItem.ClearEarnings = Convert.ToInt32(dr["EmployeePayrollClearEarnings"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["EmployeePayrollClearEarnings"].ToString().ToLower() == "true")
                                {
                                    EmployeePayrollInfoItem.ClearEarnings = dr["EmployeePayrollClearEarnings"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["EmployeePayrollClearEarnings"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        EmployeePayrollInfoItem.ClearEarnings = dr["EmployeePayrollClearEarnings"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmployeePayrollInfo ClearEarnings (" + dr["EmployeePayrollClearEarnings"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            EmployeePayrollInfoItem.ClearEarnings = dr["EmployeePayrollClearEarnings"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            EmployeePayrollInfoItem.ClearEarnings = dr["EmployeePayrollClearEarnings"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EmployeePayrollInfoItem.ClearEarnings = dr["EmployeePayrollClearEarnings"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    #region
                    string earningRate = string.Empty; string earningPercentage = string.Empty;

                    if (dt.Columns.Contains("EmployeePayrollEarningsRate"))
                    {
                        #region Validations for EmployeePayrollEarningsRate
                        if (dr["EmployeePayrollEarningsRate"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["EmployeePayrollEarningsRate"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollInfo Earnings Rate ( " + dr["EmployeePayrollEarningsRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        earningRate = dr["EmployeePayrollEarningsRate"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        earningRate = dr["EmployeePayrollEarningsRate"].ToString();
                                    }
                                }
                                else
                                    earningRate = dr["EmployeePayrollEarningsRate"].ToString();
                            }
                            else
                            {
                                earningRate = Math.Round(Convert.ToDecimal(dr["EmployeePayrollEarningsRate"].ToString()), 5).ToString();
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollEarningsRatePercent"))
                    {
                        #region Validations of EmployeePayrollInfo Earnings RatePercent
                        if (dr["EmployeePayrollEarningsRatePercent"].ToString() != string.Empty)
                        {
                            if (dr["EmployeePayrollEarningsRatePercent"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollInfo Earnings RatePercent (" + dr["EmployeePayrollEarningsRatePercent"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        earningPercentage = dr["EmployeePayrollEarningsRatePercent"].ToString().Substring(0, 15);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        earningPercentage = dr["EmployeePayrollEarningsRatePercent"].ToString();
                                    }
                                }
                                else
                                {
                                    earningPercentage = dr["EmployeePayrollEarningsRatePercent"].ToString();
                                }
                            }
                            else
                            {
                                earningPercentage = dr["EmployeePayrollEarningsRatePercent"].ToString();
                            }
                        }
                        #endregion
                    }
                    #endregion

                    //785
                    if (dt.Columns.Contains("EmployeePayrollEarningsPayrollItemWageFullName"))
                    {
                        if (dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString() != string.Empty &&
                      dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString().Contains(";"))
                        {
                            #region
                            Earnings earningsItem = null;

                            string itemWageName = dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString();
                            payrollItemwages = itemWageName.Split(';');
                            if (payrollItemwages.Length > 0)
                            {
                                foreach (string wages in payrollItemwages)
                                {
                                    earningsItem = new Earnings();
                                    earningsItem.PayrollItemWageRef = new PayrollItemWageRef(wages);

                                    if (earningRate != string.Empty)
                                    {
                                        earningsItem.Rate = earningRate;
                                    }
                                    if (earningPercentage != string.Empty)
                                    {
                                        earningsItem.RatePercent = earningPercentage;
                                    }

                                    if (earningsItem.PayrollItemWageRef != null || earningsItem.Rate != null || earningsItem.RatePercent != null)
                                        EmployeePayrollInfoItem.Earnings.Add(earningsItem);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            Earnings earningsItem = new Earnings();
                            if (dt.Columns.Contains("EmployeePayrollEarningsPayrollItemWageFullName"))
                            {
                                #region Validations of EmployeePayrollInfo Earnings PayrollItemWageRef FullName
                                if (dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString() != string.Empty)
                                {
                                    if (dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This EmployeePayrollInfo Earnings PayrollItemWageRef FullName  (" + dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                earningsItem.PayrollItemWageRef = new PayrollItemWageRef(dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString());
                                                if (earningsItem.PayrollItemWageRef.FullName == null)
                                                    earningsItem.PayrollItemWageRef.FullName = null;
                                                else
                                                    earningsItem.PayrollItemWageRef = new PayrollItemWageRef(dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                earningsItem.PayrollItemWageRef = new PayrollItemWageRef(dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString());
                                                if (earningsItem.PayrollItemWageRef.FullName == null)
                                                    earningsItem.PayrollItemWageRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            earningsItem.PayrollItemWageRef = new PayrollItemWageRef(dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString());
                                            if (earningsItem.PayrollItemWageRef.FullName == null)
                                                earningsItem.PayrollItemWageRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        earningsItem.PayrollItemWageRef = new PayrollItemWageRef(dr["EmployeePayrollEarningsPayrollItemWageFullName"].ToString());
                                        if (earningsItem.PayrollItemWageRef.FullName == null)
                                            earningsItem.PayrollItemWageRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("EmployeePayrollEarningsRate"))
                            {
                                if (earningRate != string.Empty)
                                {
                                    earningsItem.Rate = earningRate;
                                }
                            }

                            if (dt.Columns.Contains("EmployeePayrollEarningsRatePercent"))
                            {
                                if (earningPercentage != string.Empty)
                                {
                                    earningsItem.RatePercent = earningPercentage;
                                }
                            }

                            if (earningsItem.PayrollItemWageRef != null || earningsItem.Rate != null || earningsItem.RatePercent != null)
                                EmployeePayrollInfoItem.Earnings.Add(earningsItem);
                        }
                    }
                    else
                    {
                        #region
                        Earnings earningsItem = new Earnings();

                        if (dt.Columns.Contains("EmployeePayrollEarningsRate"))
                        {
                            if (earningRate != string.Empty)
                            {
                                earningsItem.Rate = earningRate;
                            }
                        }

                        if (dt.Columns.Contains("EmployeePayrollEarningsRatePercent"))
                        {
                            if (earningPercentage != string.Empty)
                            {
                                earningsItem.RatePercent = earningPercentage;
                            }
                        }

                        if (earningsItem.Rate != null || earningsItem.RatePercent != null)
                            EmployeePayrollInfoItem.Earnings.Add(earningsItem);

                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollIsUsingTimeDataToCreatePaychecks"))
                    {
                        #region Validations of EmployeePayrollInfo IsUsingTimeDataToCreatePaychecks
                        if (dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString() != "<None>" || dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString(), out result))
                            {
                                EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks = Convert.ToInt32(dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString().ToLower() == "true")
                                {
                                    EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks = dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks = dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmployeePayrollInfo IsUsingTimeDataToCreatePaychecks (" + dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks = dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks = dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks = dr["EmployeePayrollIsUsingTimeDataToCreatePaychecks"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollUseTimeDataToCreatePaychecks"))
                    {
                        #region Validation For EmployeePayrollInfoUseTimeDataToCreatePaychecks
                        if (dr["EmployeePayrollUseTimeDataToCreatePaychecks"].ToString() != string.Empty)
                        {
                            try
                            {
                                EmployeePayrollInfoItem.UseTimeDataToCreatePaychecks = Convert.ToString((DataProcessingBlocks.UseTimeDataToCreatePaychecks)Enum.Parse(typeof(DataProcessingBlocks.UseTimeDataToCreatePaychecks), dr["EmployeePayrollInfoUseTimeDataToCreatePaychecks"].ToString(), true));
                            }
                            catch
                            {
                                EmployeePayrollInfoItem.UseTimeDataToCreatePaychecks = dr["EmployeePayrollUseTimeDataToCreatePaychecks"].ToString();
                            }
                        }
                        #endregion

                    }

                    SickHours sickHoursItem = new SickHours();

                    if (dt.Columns.Contains("EmployeePayrollSickHoursHoursAvailable"))
                    {
                        #region Validations of EmployeePayrollSickHoursHoursAvailable
                        if (dr["EmployeePayrollSickHoursHoursAvailable"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoSickHoursHoursAvailable = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollSickHoursHoursAvailable"].ToString(), out dtt))
                                {
                                    //sickHoursItem.HoursAvailable = dr["EmployeePayrollSickHoursHoursAvailable"].ToString();
                                    strEmployeePayrollInfoSickHoursHoursAvailable = dr["EmployeePayrollSickHoursHoursAvailable"].ToString();
                                    if (strEmployeePayrollInfoSickHoursHoursAvailable.Contains("PT"))
                                    {
                                        sickHoursItem.HoursAvailable = strEmployeePayrollInfoSickHoursHoursAvailable;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoSickHoursHoursAvailable.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursHoursAvailable.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.HoursAvailable = strAssign;
                                        }
                                        else if (strEmployeePayrollInfoSickHoursHoursAvailable.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursHoursAvailable.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.HoursAvailable = strAssign;
                                        }
                                        else
                                        {
                                            sickHoursItem.HoursAvailable = strEmployeePayrollInfoSickHoursHoursAvailable;
                                        }
                                        //sickHoursItem.HoursAvailable = strAssign;
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoSickHoursHoursAvailable = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoSickHoursHoursAvailable.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    sickHoursItem.HoursAvailable = strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollSickHoursHoursAvailable (" + strEmployeePayrollInfoSickHoursHoursAvailable + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        sickHoursItem.HoursAvailable = dr["EmployeePayrollSickHoursHoursAvailable"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        sickHoursItem.HoursAvailable = dr["EmployeePayrollSickHoursHoursAvailable"].ToString();
                                    }
                                }
                                else
                                    sickHoursItem.HoursAvailable = dr["EmployeePayrollSickHoursHoursAvailable"].ToString();

                            }

                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollSickHoursAccrualPeriod"))
                    {
                        #region Validations of EmployeePayrollInfo SickHours AccrualPeriod
                        if (dr["EmployeePayrollSickHoursAccrualPeriod"].ToString() != string.Empty)
                        {
                            try
                            {
                                sickHoursItem.AccrualPeriod = Convert.ToString((DataProcessingBlocks.AccrualPeriod)Enum.Parse(typeof(DataProcessingBlocks.AccrualPeriod), dr["EmployeePayrollSickHoursAccrualPeriod"].ToString(), true));
                            }
                            catch
                            {
                                sickHoursItem.AccrualPeriod = dr["EmployeePayrollSickHoursAccrualPeriod"].ToString();
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollSickHoursHoursAccrued"))
                    {
                        #region Validations of EmployeePayrollSickHoursHoursAccrued
                        if (dr["EmployeePayrollSickHoursHoursAccrued"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoSickHoursHoursAccrued = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollSickHoursHoursAccrued"].ToString(), out dtt))
                                {
                                    //sickHoursItem.HoursAccrued = dr["EmployeePayrollSickHoursHoursAccrued"].ToString();
                                    strEmployeePayrollInfoSickHoursHoursAccrued = dr["EmployeePayrollSickHoursHoursAccrued"].ToString();
                                    if (strEmployeePayrollInfoSickHoursHoursAccrued.Contains("PT"))
                                    {
                                        sickHoursItem.HoursAccrued = strEmployeePayrollInfoSickHoursHoursAccrued;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoSickHoursHoursAccrued.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursHoursAccrued.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.HoursAccrued = strAssign;
                                        }
                                        else if (strEmployeePayrollInfoSickHoursHoursAccrued.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursHoursAccrued.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.HoursAccrued = strAssign;
                                        }
                                        else
                                        {
                                            sickHoursItem.HoursAccrued = strEmployeePayrollInfoSickHoursHoursAccrued;
                                        }
                                        //sickHoursItem.HoursAccrued = strAssign;
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoSickHoursHoursAccrued = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoSickHoursHoursAccrued.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    sickHoursItem.HoursAccrued = strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollSickHoursHoursAccrued (" + strEmployeePayrollInfoSickHoursHoursAccrued + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        sickHoursItem.HoursAccrued = dr["EmployeePayrollSickHoursHoursAccrued"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        sickHoursItem.HoursAccrued = dr["EmployeePayrollSickHoursHoursAccrued"].ToString();
                                    }
                                }
                                else
                                    sickHoursItem.HoursAccrued = dr["EmployeePayrollSickHoursHoursAccrued"].ToString();

                            }

                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollSickHoursMaximumHours"))
                    {
                        #region Validations of EmployeePayrollSickHoursMaximumHours
                        if (dr["EmployeePayrollSickHoursMaximumHours"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoSickHoursMaximumHours = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollSickHoursMaximumHours"].ToString(), out dtt))
                                {
                                    //sickHoursItem.MaximumHours= dr["EmployeePayrollSickHoursMaximumHours"].ToString();
                                    strEmployeePayrollInfoSickHoursMaximumHours = dr["EmployeePayrollSickHoursMaximumHours"].ToString();
                                    if (strEmployeePayrollInfoSickHoursMaximumHours.Contains("PT"))
                                    {
                                        sickHoursItem.MaximumHours= strEmployeePayrollInfoSickHoursMaximumHours;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoSickHoursMaximumHours.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursMaximumHours.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.MaximumHours= strAssign;
                                        }
                                        else if (strEmployeePayrollInfoSickHoursMaximumHours.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursMaximumHours.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.MaximumHours= strAssign;
                                        }
                                        else
                                        {
                                            sickHoursItem.MaximumHours= strEmployeePayrollInfoSickHoursMaximumHours;
                                        }
                                        //sickHoursItem.MaximumHours= strAssign;
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoSickHoursMaximumHours = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoSickHoursMaximumHours.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    sickHoursItem.MaximumHours= strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollSickHoursMaximumHours (" + strEmployeePayrollInfoSickHoursMaximumHours + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        sickHoursItem.MaximumHours= dr["EmployeePayrollSickHoursMaximumHours"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        sickHoursItem.MaximumHours= dr["EmployeePayrollSickHoursMaximumHours"].ToString();
                                    }
                                }
                                else
                                    sickHoursItem.MaximumHours = dr["EmployeePayrollSickHoursMaximumHours"].ToString();

                            }

                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollSickHoursHoursUsed"))
                    {
                        #region Validations of EmployeePayrollSickHoursHoursUsed
                        if (dr["EmployeePayrollSickHoursHoursUsed"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoSickHoursHoursUsed = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollSickHoursHoursUsed"].ToString(), out dtt))
                                {
                                    //sickHoursItem.HoursUsed  = dr["EmployeePayrollSickHoursHoursUsed"].ToString();
                                    strEmployeePayrollInfoSickHoursHoursUsed = dr["EmployeePayrollSickHoursHoursUsed"].ToString();
                                    if (strEmployeePayrollInfoSickHoursHoursUsed.Contains("PT"))
                                    {
                                        sickHoursItem.HoursUsed  = strEmployeePayrollInfoSickHoursHoursUsed;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoSickHoursHoursUsed.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursHoursUsed.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.HoursUsed  = strAssign;
                                        }
                                        else if (strEmployeePayrollInfoSickHoursHoursUsed.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoSickHoursHoursUsed.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            sickHoursItem.HoursUsed  = strAssign;
                                        }
                                        else
                                        {
                                            sickHoursItem.HoursUsed  = strEmployeePayrollInfoSickHoursHoursUsed;
                                        }
                                        //sickHoursItem.HoursUsed  = strAssign;
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoSickHoursHoursUsed = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoSickHoursHoursUsed.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    sickHoursItem.HoursUsed  = strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollSickHoursHoursUsed (" + strEmployeePayrollInfoSickHoursHoursUsed + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        sickHoursItem.HoursUsed  = dr["EmployeePayrollSickHoursHoursUsed"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        sickHoursItem.HoursUsed  = dr["EmployeePayrollSickHoursHoursUsed"].ToString();
                                    }
                                }
                                else
                                    sickHoursItem.HoursUsed = dr["EmployeePayrollSickHoursHoursUsed"].ToString();

                            }

                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollSickHoursIsResettingHoursEachNewYear"))
                    {
                        #region Validations of EmployeePayrollInfo SickHours IsResettingHoursEachNewYear
                        if (dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString() != "<None>" || dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString(), out result))
                            {
                                sickHoursItem.IsResettingHoursEachNewYear = Convert.ToInt32(dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString().ToLower() == "true")
                                {
                                    sickHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        sickHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmployeePayrollInfo SickHours IsResettingHoursEachNewYear (" + dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            sickHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            sickHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        sickHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollSickHoursIsResettingHoursEachNewYear"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollSickHoursAccrualStartDate"))
                    {
                        #region validations of EmployeePayrollInfo SickHours AccrualStartDate
                        if (dr["EmployeePayrollSickHoursAccrualStartDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["EmployeePayrollSickHoursAccrualStartDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmployeePayrollInfo SickHours AccrualStartDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            sickHoursItem.AccrualStartDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            sickHoursItem.AccrualStartDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        sickHoursItem.AccrualStartDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    sickHoursItem.AccrualStartDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                sickHoursItem.AccrualStartDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (sickHoursItem.AccrualPeriod != null || sickHoursItem.AccrualStartDate != null || sickHoursItem.HoursAccrued != null || sickHoursItem.HoursAvailable != null || sickHoursItem.HoursUsed != null || sickHoursItem.IsResettingHoursEachNewYear != null || sickHoursItem.MaximumHours != null)
                        EmployeePayrollInfoItem.SickHours.Add(sickHoursItem);


                    VacationHours vacationHoursItem = new VacationHours();

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursHoursAvailable"))
                    {
                        #region Validations of EmployeePayrollVacationHoursHoursAvailable
                        if (dr["EmployeePayrollVacationHoursHoursAvailable"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoVacationHoursHoursAvailable = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollVacationHoursHoursAvailable"].ToString(), out dtt))
                                {                                    
                                    strEmployeePayrollInfoVacationHoursHoursAvailable = dr["EmployeePayrollVacationHoursHoursAvailable"].ToString();
                                    if (strEmployeePayrollInfoVacationHoursHoursAvailable.Contains("PT"))
                                    {
                                        vacationHoursItem.HoursAvailable = strEmployeePayrollInfoVacationHoursHoursAvailable;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoVacationHoursHoursAvailable.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursHoursAvailable.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.HoursAvailable = strAssign;
                                        }
                                        else if (strEmployeePayrollInfoVacationHoursHoursAvailable.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursHoursAvailable.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.HoursAvailable = strAssign;
                                        }
                                        else
                                        {
                                            vacationHoursItem.HoursAvailable = strEmployeePayrollInfoVacationHoursHoursAvailable;
                                        }
                                       
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoVacationHoursHoursAvailable = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoVacationHoursHoursAvailable.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    vacationHoursItem.HoursAvailable = strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollVacationHoursHoursAvailable (" + strEmployeePayrollInfoVacationHoursHoursAvailable + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vacationHoursItem.HoursAvailable = dr["EmployeePayrollVacationHoursHoursAvailable"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vacationHoursItem.HoursAvailable = dr["EmployeePayrollVacationHoursHoursAvailable"].ToString();
                                    }
                                }
                                else
                                    vacationHoursItem.HoursAvailable = dr["EmployeePayrollVacationHoursHoursAvailable"].ToString();

                            }

                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursAccrualPeriod"))
                    {
                        #region Validations of EmployeePayrollInfo VacationHours AccrualPeriod
                        if (dr["EmployeePayrollVacationHoursAccrualPeriod"].ToString() != string.Empty)
                        {
                            try
                            {
                                vacationHoursItem.AccrualPeriod = Convert.ToString((DataProcessingBlocks.AccrualPeriod)Enum.Parse(typeof(DataProcessingBlocks.AccrualPeriod), dr["EmployeePayrollVacationHoursAccrualPeriod"].ToString(), true));
                            }
                            catch
                            {
                                vacationHoursItem.AccrualPeriod = dr["EmployeePayrollVacationHoursAccrualPeriod"].ToString();
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursHoursAccrued"))
                    {
                        #region Validations of EmployeePayrollVacationHoursHoursAccrued
                        if (dr["EmployeePayrollVacationHoursHoursAccrued"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoVacationHoursHoursAccrued = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollVacationHoursHoursAccrued"].ToString(), out dtt))
                                {
                                   
                                    strEmployeePayrollInfoVacationHoursHoursAccrued = dr["EmployeePayrollVacationHoursHoursAccrued"].ToString();
                                    if (strEmployeePayrollInfoVacationHoursHoursAccrued.Contains("PT"))
                                    {
                                        vacationHoursItem.HoursAccrued= strEmployeePayrollInfoVacationHoursHoursAccrued;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoVacationHoursHoursAccrued.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursHoursAccrued.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.HoursAccrued= strAssign;
                                        }
                                        else if (strEmployeePayrollInfoVacationHoursHoursAccrued.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursHoursAccrued.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.HoursAccrued= strAssign;
                                        }
                                        else
                                        {
                                            vacationHoursItem.HoursAccrued= strEmployeePayrollInfoVacationHoursHoursAccrued;
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoVacationHoursHoursAccrued = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoVacationHoursHoursAccrued.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    vacationHoursItem.HoursAccrued= strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollVacationHoursHoursAccrued (" + strEmployeePayrollInfoVacationHoursHoursAccrued + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vacationHoursItem.HoursAccrued= dr["EmployeePayrollVacationHoursHoursAccrued"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vacationHoursItem.HoursAccrued= dr["EmployeePayrollVacationHoursHoursAccrued"].ToString();
                                    }
                                }
                                else
                                    vacationHoursItem.HoursAccrued= dr["EmployeePayrollVacationHoursHoursAccrued"].ToString();

                            }

                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursMaximumHours"))
                    {
                        #region Validations of EmployeePayrollVacationHoursMaximumHours
                        if (dr["EmployeePayrollVacationHoursMaximumHours"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoVacationHoursMaximumHours = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollVacationHoursMaximumHours"].ToString(), out dtt))
                                {
                                    strEmployeePayrollInfoVacationHoursMaximumHours = dr["EmployeePayrollVacationHoursMaximumHours"].ToString();
                                    if (strEmployeePayrollInfoVacationHoursMaximumHours.Contains("PT"))
                                    {
                                        vacationHoursItem.MaximumHours  = strEmployeePayrollInfoVacationHoursMaximumHours;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoVacationHoursMaximumHours.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursMaximumHours.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.MaximumHours  = strAssign;
                                        }
                                        else if (strEmployeePayrollInfoVacationHoursMaximumHours.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursMaximumHours.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.MaximumHours  = strAssign;
                                        }
                                        else
                                        {
                                            vacationHoursItem.MaximumHours  = strEmployeePayrollInfoVacationHoursMaximumHours;
                                        }
                                        //vacationHoursItem.MaximumHours  = strAssign;
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoVacationHoursMaximumHours = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoVacationHoursMaximumHours.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    vacationHoursItem.MaximumHours  = strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollVacationHoursMaximumHours (" + strEmployeePayrollInfoVacationHoursMaximumHours + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vacationHoursItem.MaximumHours  = dr["EmployeePayrollVacationHoursMaximumHours"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vacationHoursItem.MaximumHours  = dr["EmployeePayrollVacationHoursMaximumHours"].ToString();
                                    }
                                }
                                else
                                    vacationHoursItem.MaximumHours  = dr["EmployeePayrollVacationHoursMaximumHours"].ToString();

                            }

                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursHoursUsed"))
                    {
                        #region Validations of EmployeePayrollVacationHoursHoursUsed
                        if (dr["EmployeePayrollVacationHoursHoursUsed"].ToString() != string.Empty)
                        {
                            string strEmployeePayrollInfoVacationHoursHoursUsed = string.Empty;
                            try
                            {
                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["EmployeePayrollVacationHoursHoursUsed"].ToString(), out dtt))
                                {
                                    //vacationHoursItem.HoursUsed = dr["EmployeePayrollVacationHoursHoursUsed"].ToString();
                                    strEmployeePayrollInfoVacationHoursHoursUsed = dr["EmployeePayrollVacationHoursHoursUsed"].ToString();
                                    if (strEmployeePayrollInfoVacationHoursHoursUsed.Contains("PT"))
                                    {
                                        vacationHoursItem.HoursUsed = strEmployeePayrollInfoVacationHoursHoursUsed;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strEmployeePayrollInfoVacationHoursHoursUsed.Contains(":"))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursHoursUsed.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.HoursUsed = strAssign;
                                        }
                                        else if (strEmployeePayrollInfoVacationHoursHoursUsed.Contains("."))
                                        {
                                            string[] strsep = strEmployeePayrollInfoVacationHoursHoursUsed.Split('.');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            vacationHoursItem.HoursUsed = strAssign;
                                        }
                                        else
                                        {
                                            vacationHoursItem.HoursUsed = strEmployeePayrollInfoVacationHoursHoursUsed;
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    strEmployeePayrollInfoVacationHoursHoursUsed = dtt.ToString("HH:mm:ss");
                                    string strAssign = "PT";
                                    string[] strsep = strEmployeePayrollInfoVacationHoursHoursUsed.Split(':');

                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    vacationHoursItem.HoursUsed = strAssign;
                                }
                            }
                            catch
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This EmployeePayrollVacationHoursHoursUsed (" + strEmployeePayrollInfoVacationHoursHoursUsed + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        vacationHoursItem.HoursUsed = dr["EmployeePayrollVacationHoursHoursUsed"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        vacationHoursItem.HoursUsed = dr["EmployeePayrollVacationHoursHoursUsed"].ToString();
                                    }
                                }
                                else
                                    vacationHoursItem.HoursUsed = dr["EmployeePayrollVacationHoursHoursUsed"].ToString();

                            }

                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursIsResettingHoursEachNewYear"))
                    {
                        #region Validations of EmployeePayrollInfo VacationHours IsResettingHoursEachNewYear
                        if (dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString() != "<None>" || dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString(), out result))
                            {
                                vacationHoursItem.IsResettingHoursEachNewYear = Convert.ToInt32(dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString().ToLower() == "true")
                                {
                                    vacationHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        vacationHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmployeePayrollInfo VacationHours IsResettingHoursEachNewYear (" + dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            vacationHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vacationHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        vacationHoursItem.IsResettingHoursEachNewYear = dr["EmployeePayrollVacationHoursIsResettingHoursEachNewYear"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("EmployeePayrollVacationHoursAccrualStartDate"))
                    {
                        #region validations of EmployeePayrollInfo VacationHours AccrualStartDate
                        if (dr["EmployeePayrollVacationHoursAccrualStartDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["EmployeePayrollVacationHoursAccrualStartDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out EmployeeDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmployeePayrollInfo VacationHours AccrualStartDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            vacationHoursItem.AccrualStartDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            vacationHoursItem.AccrualStartDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        vacationHoursItem.AccrualStartDate = datevalue;
                                    }
                                }
                                else
                                {
                                    EmployeeDt = dttest;
                                    vacationHoursItem.AccrualStartDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                EmployeeDt = Convert.ToDateTime(datevalue);
                                vacationHoursItem.AccrualStartDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (vacationHoursItem.AccrualPeriod != null || vacationHoursItem.AccrualStartDate != null || vacationHoursItem.HoursAccrued != null || vacationHoursItem.HoursAvailable != null || vacationHoursItem.HoursUsed != null || vacationHoursItem.IsResettingHoursEachNewYear != null || vacationHoursItem.MaximumHours != null)
                        EmployeePayrollInfoItem.VacationHours.Add(vacationHoursItem);


                    if (EmployeePayrollInfoItem.ClassRef != null || EmployeePayrollInfoItem.ClearEarnings != null || EmployeePayrollInfoItem.Earnings != null || EmployeePayrollInfoItem.IsUsingTimeDataToCreatePaychecks!=null || EmployeePayrollInfoItem.PayPeriod!=null || EmployeePayrollInfoItem.SickHours!=null || EmployeePayrollInfoItem.UseTimeDataToCreatePaychecks!=null || EmployeePayrollInfoItem.VacationHours!=null)
                    Employee.EmployeePayrollInfo.Add(EmployeePayrollInfoItem);

                    coll.Add(Employee);

                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }

    }
}
