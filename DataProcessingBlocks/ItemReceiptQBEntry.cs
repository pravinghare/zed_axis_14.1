using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("ItemReceiptQBEntry", Namespace = "", IsNullable = false)]
    public class ItemReceiptQBEntry
    {
        #region Private Member Variable

        private VendorRef m_VendorRef;
        private APAccountRef m_APAccountRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private string m_Memo;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_ExchangeRate;
        private string m_LinkToTxnID;
        private Collection<ExpenseLineAdd> m_ExpenseLineAdd = new Collection<ExpenseLineAdd>();
        private Collection<ItemLineAdd> m_ItemLineAdd = new Collection<ItemLineAdd>();
        private Collection<ItemGroupLineAdd> m_ItemGroupLineAdd = new Collection<ItemGroupLineAdd>();
        
        //Improvement::548
        private SalesRepRef m_SalesRepRef;
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

        #endregion

        #region Constructor
        public ItemReceiptQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public VendorRef VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        public APAccountRef APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }



        //Improvement::548
        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }


        

        

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set
            {

                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    m_SerialNumber = value;
                }
            }
        }

        public string LotNumber
        {
            get { return m_LotNumber; }
            set
            {
                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    m_LotNumber = value;
                }
            }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }


        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public string LinkToTxnID
        {
            get { return m_LinkToTxnID; }
            set { m_LinkToTxnID = value; }
        }


        [XmlArray("ExpenseLineREM")]
        public Collection<ExpenseLineAdd> ExpenseLineAdd
        {
            get { return m_ExpenseLineAdd; }
            set { m_ExpenseLineAdd = value; }
        }

        [XmlArray("ItemLineREM")]
        public Collection<ItemLineAdd> ItemLineAdd
        {
            get { return m_ItemLineAdd; }
            set { m_ItemLineAdd = value; }
        }


        [XmlArray("ItemGroupREM")]
        public Collection<ItemGroupLineAdd> ItemGroupLineAdd
        {
            get { return m_ItemGroupLineAdd; }
            set { m_ItemGroupLineAdd = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemReceiptQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptAddRq = requestXmlDoc.CreateElement("ItemReceiptAddRq");
            inner.AppendChild(ItemReceiptAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptAdd = requestXmlDoc.CreateElement("ItemReceiptAdd");

            ItemReceiptAddRq.AppendChild(ItemReceiptAdd);

            requestXML = requestXML.Replace("<ExpenseLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineREM>", string.Empty);

            
            requestXML = requestXML.Replace("<ItemLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<ItemGroupREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemGroupREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemGroupREM>", string.Empty);

            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);
                

            ItemReceiptAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;



            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptAddRs/ItemReceiptRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemReceipt(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating ItemReceiptQBEntry information
        /// of existing ItemReceiptQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateItemReceiptInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemReceiptQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptQBEntryModRq = requestXmlDoc.CreateElement("ItemReceiptModRq");
            inner.AppendChild(ItemReceiptQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptMod = requestXmlDoc.CreateElement("ItemReceiptMod");
            ItemReceiptQBEntryModRq.AppendChild(ItemReceiptMod);


            requestXML = requestXML.Replace("<ExpenseLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineREM>", string.Empty);


            requestXML = requestXML.Replace("<ItemLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineREM>", string.Empty);

            requestXML = requestXML.Replace("<ItemGroupREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemGroupREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemGroupREM>", string.Empty);


            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>","</ExpenseLineMod>");
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd></ExpenseLineAdd>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd />",string.Empty );
            requestXML = requestXML.Replace("<ItemLineAdd></ItemLineAdd>", string.Empty);

            requestXML = requestXML.Replace("<ItemGroupLineAdd>", "<ItemGroupLineMod>");
            requestXML = requestXML.Replace("</ItemGroupLineAdd>", "</ItemGroupLineMod>");
            requestXML = requestXML.Replace("<ItemGroupLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ItemGroupLineAdd></ItemGroupLineAdd>", string.Empty);


            requestXML = requestXML.Replace("<ItemLineMod>", "<ItemLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            ItemReceiptMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptModRs/ItemReceiptRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemReceipt(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

//Bug No.412
        /// <summary>
        /// This method is used to appending data to corresponding transaction id
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendItemReceiptInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence,List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemReceiptQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptQBEntryModRq = requestXmlDoc.CreateElement("ItemReceiptModRq");
            inner.AppendChild(ItemReceiptQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement ItemReceiptMod = requestXmlDoc.CreateElement("ItemReceiptMod");
            ItemReceiptQBEntryModRq.AppendChild(ItemReceiptMod);

            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
            //Axis 706
            List<string> GroupTxnLineID = new List<string>();
            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                if (!txnLineIDList[i].Contains("GroupTxnLineID_"))
                {
                    addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></ItemLineMod><ItemLineMod>";


                    XmlNode ItemReceiptLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod");
                    System.Xml.XmlElement ItemReceiptLineGroupMod = requestXmlDoc.CreateElement("ItemLineMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = txnLineIDList[i];
                    ItemReceiptLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertAfter(ItemReceiptLineGroupMod, ItemReceiptLineMod.ChildNodes[i == 0 ? i : i - 1]);
                }
                else
                {
                    string GroupTxnID = txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty);
                    GroupTxnLineID.Add(txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty));
                    XmlNode ItemReceiptMod1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod");
                    //XmlNode ItemReceiptLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod/ItemReceiptLineMod");
                    System.Xml.XmlElement ItemReceiptLineGroupMod = requestXmlDoc.CreateElement("ItemGroupLineMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = GroupTxnID;
                    ItemReceiptLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertAfter(ItemReceiptLineGroupMod, ItemReceiptMod1.ChildNodes[i == 0 ? i : i - 1]);
                }
            }

            XmlNode ItemReceiptMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod");

            System.Xml.XmlElement ItemReceiptAdd = requestXmlDoc.CreateElement("ItemLineMod");

            //Axis 706 ends



            requestXML = requestXML.Replace("<ExpenseLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineREM>", string.Empty);


            requestXML = requestXML.Replace("<ItemLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineREM>", string.Empty);

            requestXML = requestXML.Replace("<ItemGroupREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemGroupREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemGroupREM>", string.Empty);


            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd></ExpenseLineAdd>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd></ItemLineAdd>", string.Empty);

            requestXML = requestXML.Replace("<ItemGroupLineAdd>", "<ItemGroupLineMod>");
            requestXML = requestXML.Replace("</ItemGroupLineAdd>", "</ItemGroupLineMod>");
            requestXML = requestXML.Replace("<ItemGroupLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ItemGroupLineAdd></ItemGroupLineAdd>", string.Empty);


           // requestXML = requestXML.Replace("<ItemLineMod>", "<ItemLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            //Axis 706
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");

            string[] ItemReceiptLineModFragment;

            int indexItemReceiptLine = requestXML.IndexOf(@"<ItemReceiptLineMod>");
            int indexItemReceiptLineGroup = requestXML.IndexOf(@"<ItemReceiptLineGroupMod>");

            if (indexItemReceiptLineGroup <= 0 || indexItemReceiptLine < indexItemReceiptLineGroup)
            {
                ItemReceiptLineModFragment = requestXML.Split(new string[] { "<ItemLineMod>" }, StringSplitOptions.None);
            }
            else
            {
                ItemReceiptLineModFragment = requestXML.Split(new string[] { "<ItemGroupLineMod>" }, StringSplitOptions.None);
            }

            if (ItemReceiptLineModFragment.Length > 0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = ItemReceiptLineModFragment[0];


                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").FirstChild;


                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertBefore(xfrag1, firstChild1);

                requestXML = requestXML.Replace(ItemReceiptLineModFragment[0], string.Empty);
            }

            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertAfter(xfrag, ItemReceiptMod2.LastChild);
            //Axis 706 ends

            //For add request id to track error message
            string requeststring = string.Empty;

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemReceiptModRq/ItemReceiptMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemReceiptModRs/ItemReceiptRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemReceipt(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        #endregion
    }

    
    /// <summary>
    /// This method for finding item ref number 
    /// </summary>
    public class ItemReceiptQBEntryCollection : Collection<ItemReceiptQBEntry>
    {
        
        public ItemReceiptQBEntry FindItemReceiptEntry(string refNumber)
        {
            foreach (ItemReceiptQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }

    /// <summary>
    /// class for creating new line for item
    /// </summary>
    [XmlRootAttribute("ItemLineAdd", Namespace = "", IsNullable = false)]
    public class ItemLine
    {
        #region Private Member Variable

        private ItemRef m_ItemRef;
        private InventorySiteRef m_InventorySiteRef;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_Desc;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Cost;
        private string m_Amount;
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_BillableStatus;
        private OverrideItemAccountRef m_OverrideItemAccountRef;
        private LinkToTxn m_LinkToTxn;

        //Improvement::548
        private SalesRepRef m_SalesRepRef;
        //private DataExt m_DataExtName;
        //private DataExt m_DataExtValue;
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;
        
        #endregion

        #region Constructor
        public ItemLine()
        { }
        #endregion

        #region Public Properties

        


        public ItemRef ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return m_InventorySiteRef; }
            set { m_InventorySiteRef = value; }
        }


        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public string LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }

        public string Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public string Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }

        public string Cost
        {
            get { return m_Cost; }
            set { m_Cost = value; }
        }

        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }




        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }

        public string BillableStatus
        {
            get { return m_BillableStatus; }
            set { m_BillableStatus = value; }
        }

        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return m_OverrideItemAccountRef; }
            set { m_OverrideItemAccountRef = value; }
        }

        public LinkToTxn LinkToTxn
        {
            get { return m_LinkToTxn; }
            set { m_LinkToTxn = value; }
        }


        //Improvement::548
        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
        
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }

        #endregion

    }
    /// <summary>
    /// class for creating new line for item group
    /// </summary>
    [XmlRootAttribute("ItemGroupLineAdd", Namespace = "", IsNullable = false)]
    public class ItemGroupLineAdd
    {

        #region Private Member Variable

        private ItemGroupRef m_ItemGroupRef;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private InventorySiteRef m_InventorySiteRef;
        
        //Improvement::548
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;


        #endregion

        #region Constructor
        public ItemGroupLineAdd() { }
        #endregion

        #region Public Properties

        




        public ItemGroupRef ItemGroupRef
        {
            get { return m_ItemGroupRef; }
            set { m_ItemGroupRef = value; }
        }

        public string Quantity
        {
            get { return m_Quantity; }
            set { m_Quantity=value;}
        }

        public string UnitOfMeasure
        {
            get { return m_UnitOfMeasure; }
            set { m_UnitOfMeasure = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return m_InventorySiteRef; }
            set { m_InventorySiteRef = value; }
        }

        //Improvement::548
       
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }

        #endregion
    }


}
