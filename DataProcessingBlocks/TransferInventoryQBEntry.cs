
using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("TransferInventoryQBEntry", Namespace = "", IsNullable = false)]
    public class TransferInventoryQBEntry
    {
        #region Private members

        private string m_TxnDate;
        private string m_RefNumber;
        private FromInventorySiteRef m_FromInventorySiteRef;
        private ToInventorySiteRef m_ToInventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        private string m_Memo;
        private string m_ExternalGUID;
        private Collection<TransferInventoryLineAdd> m_TransferInventoryLineAdd = new Collection<TransferInventoryLineAdd>();


        #endregion

        #region Constructor

        public TransferInventoryQBEntry()
        {
        }

        #endregion

        #region Public Properties
       

        [XmlElement(DataType = "string")]
        public string TxnDate
        {

            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public FromInventorySiteRef FromInventorySiteRef
        {
            get { return m_FromInventorySiteRef; }
            set { m_FromInventorySiteRef = value; }
        }

        public ToInventorySiteRef ToInventorySiteRef
        {
            get { return m_ToInventorySiteRef; }
            set { m_ToInventorySiteRef = value; }
        }
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set
            {
                this.m_InventorySiteLocationRef = value;
            }
        }
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        [XmlArray("TransferInventoryLineAddREM")]
        public Collection<TransferInventoryLineAdd> TransferInventoryLineAdd
        {
            get { return m_TransferInventoryLineAdd; }
            set { m_TransferInventoryLineAdd = value; }
        }


        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.TransferInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesOrderEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement TransferInventoryAddRq = requestXmlDoc.CreateElement("TransferInventoryAddRq");
            inner.AppendChild(TransferInventoryAddRq);

            //Create SalesOrderEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement TransferInventoryAdd = requestXmlDoc.CreateElement("TransferInventoryAdd");


            TransferInventoryAddRq.AppendChild(TransferInventoryAdd);

            requestXML = requestXML.Replace("<TransferInventoryLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<TransferInventoryLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</TransferInventoryLineAddREM>", string.Empty);

            TransferInventoryAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EstimateAddRq/TransferInventoryModRq/TransferInventoryMod");
                    node.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    node.RemoveAll();


                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/TransferInventoryAddRq/TransferInventoryAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                TransferInventoryAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                TransferInventoryAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);                   
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/TransferInventoryAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/TransferInventoryAddRs/TransferInventoryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofTransferInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Transfer Inventory Ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Transfer Inventory Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create TransferInventoryAddRq aggregate and fill in field values for it
            XmlElement TransferInventoryAddRq = requestXmlDoc.CreateElement("TransferInventoryAddRq");
            inner.AppendChild(TransferInventoryAddRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            TransferInventoryAddRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            TransferInventoryAddRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
               
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating Transfer Inventory information
        /// of existing Transfer Inventory with listid 
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateTransferInventoryInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.TransferInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create TransferInventoryModRq aggregate and fill in field values for it
            System.Xml.XmlElement TransferInventoryQBEntryModRq = requestXmlDoc.CreateElement("TransferInventoryModRq");
            inner.AppendChild(TransferInventoryQBEntryModRq);

            //Create TransferInventoryMod aggregate and fill in field values for it
            System.Xml.XmlElement TransferInventoryMod = requestXmlDoc.CreateElement("TransferInventoryMod");
            TransferInventoryQBEntryModRq.AppendChild(TransferInventoryMod);

            requestXML = requestXML.Replace("<TransferInventoryLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<TransferInventoryLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</TransferInventoryLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<TransferInventoryAdd>", "<TransferInventoryMod>");
            requestXML = requestXML.Replace("</TransferInventoryAdd>", "</TransferInventoryMod>");
           // requestXML = requestXML.Replace("<TransferInventoryLineAdd", "<TransferInventoryLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("<TransferInventoryLineAdd>", "<TransferInventoryLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("</TransferInventoryLineAdd>", "</TransferInventoryLineMod>");

            TransferInventoryMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EstimateAddRq/TransferInventoryModRq/TransferInventoryMod");
                    node.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    node.RemoveAll();


                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                TransferInventoryQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By TransferInventoryRefNumber) : " + rowcount.ToString());
            else
                TransferInventoryQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //TransferInventoryMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/TransferInventoryModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/TransferInventoryModRs/TransferInventoryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofTransferInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

//Bug No.412
        /// <summary>
        /// Append data to existing list id
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendTransferInventoryInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.TransferInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create TransferInventoryModRq aggregate and fill in field values for it
            System.Xml.XmlElement TransferInventoryQBEntryModRq = requestXmlDoc.CreateElement("TransferInventoryModRq");
            inner.AppendChild(TransferInventoryQBEntryModRq);

            //Create TransferInventoryMod aggregate and fill in field values for it
            System.Xml.XmlElement TransferInventoryMod = requestXmlDoc.CreateElement("TransferInventoryMod");
            TransferInventoryQBEntryModRq.AppendChild(TransferInventoryMod);

            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.

            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = ""; 
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></TransferInventoryLineMod><TransferInventoryLineMod>";
            }
            for (int i = 0; i < request.Length; i++)
            {
                if (Listcnt != 0)
                {
                    if (subStringCnt == 1)
                    {
                        //subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></TransferInventoryMod><TransferInventoryMod><TxnLineID>-1</TxnLineID><ItemRef>");
                        subResultString = request[i].Replace("<ItemRef>", addString + "<TxnLineID>-1</TxnLineID><ItemRef>");
                        subStringCnt++;
                    }
                    else
                    {
                        subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                    }
                }
                else
                {
                    subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                }

                if (stringCnt == request.Length)
                {
                    resultString += subResultString;
                }
                else
                {
                    resultString += subResultString + "</ItemRef>";
                }
                stringCnt++;
                Listcnt--;
            }
            requestXML = resultString;


            requestXML = requestXML.Replace("<TransferInventoryLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<TransferInventoryLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</TransferInventoryLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<TransferInventoryAdd>", "<TransferInventoryMod>");
            requestXML = requestXML.Replace("</TransferInventoryAdd>", "</TransferInventoryMod>");       
            requestXML = requestXML.Replace("</TransferInventoryLineAdd>", "</TransferInventoryLineMod>");

            TransferInventoryMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EstimateAddRq/TransferInventoryModRq/TransferInventoryMod");
                    node.ParentNode.RemoveChild(node);                   
                    node.RemoveAll();


                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                TransferInventoryQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By TransferInventoryRefNumber) : " + rowcount.ToString());
            else
                TransferInventoryQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");           
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod").InsertBefore(ListID, firstChild).InnerText = listID;            
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");           
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/TransferInventoryModRq/TransferInventoryMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/TransferInventoryModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/TransferInventoryModRs/TransferInventoryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofTransferInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        #endregion
    }

    public class TransferInventoryQBEntryCollection : Collection<TransferInventoryQBEntry>
    {
        /// <summary>
        ///  This method is used for getting existing transfer inv refnumber, If not exists then it return null.
        /// </summary>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public TransferInventoryQBEntry FindTransferInventoryQBEntry(string refNumber)
        {
            foreach (TransferInventoryQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }
    /// <summary>
    /// This class for declare variable and properties for adding new line for transfer inventory
    /// </summary>
    [XmlRootAttribute("TransferInventoryLineAdd", Namespace = "", IsNullable = false)]
    public class TransferInventoryLineAdd
    {
        #region Private Member Variables

        private ItemRef m_ItemRef;
        private FromInventorySiteLocationRef m_FromInventorySiteLocationRef;
        private ToInventorySiteLocationRef m_ToInventorySiteLocationRef;
        private string m_QuantityToTransfer;
        //P Axis 13.1 : issue 659
        private string m_SerialNumber;
        private string m_LotNumber;

        #endregion

        #region  Constructors

        public TransferInventoryLineAdd()
        {

        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }
        public FromInventorySiteLocationRef FromInventorySiteLocationRef
        {
            get { return this.m_FromInventorySiteLocationRef; }
            set { this.m_FromInventorySiteLocationRef = value; }
        }
        public ToInventorySiteLocationRef ToInventorySiteLocationRef
        {
            get { return this.m_ToInventorySiteLocationRef; }
            set { this.m_ToInventorySiteLocationRef = value; }
        }

        public string QuantityToTransfer
        {
            get { return m_QuantityToTransfer; }
            set { m_QuantityToTransfer = value; }
        }

        //P Axis 13.1 : issue 659
        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }
        public string LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }
        #endregion
    }
}


