using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Forms;
using System.Collections.Specialized;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class DefaultAccountSettings
    {
        #region Private Members

        
        private string m_fileName;
        private string m_Type;
        private string m_TaxCode;
        private string m_IncomeAccount;
        private string m_COGSAccount;
        private string m_ExpenseAccount;
        private string m_AssetAccount;
        private string m_IsGrossToNet;
        private string m_IsVendorLookup="0";
        //416
        private string m_ALULookup = "0";

        //Axis-565
        private string m_UPCLookup = "0";
       

       // bug 486
        private string m_SKULookup = "0"; 
       
        private string m_Frieght;
        private string m_Insurance;
        private string m_Discount;
        private string m_SalesTax;//bug no. 410
        private static string m_settingsPath = string.Empty;
        private string SettingAppName = "QuickBook";

        #endregion

        #region Public Properties

        public string File
        {
            get { return m_fileName; }
            set { m_fileName = value; }
        }
        
        public string Type
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        public string TaxCode
        {
            get { return m_TaxCode; }
            set { m_TaxCode = value; }
        }

        public string IncomeAccount
        {
            get { return m_IncomeAccount; }
            set { m_IncomeAccount = value; }
        }

        public string COGSAccount
        {
            get { return m_COGSAccount; }
            set { m_COGSAccount = value; }
        }
        public string ExpenseAccount
        {
            get { return m_ExpenseAccount; }
            set { m_ExpenseAccount = value; }
        }
        public string AssetAccount
        {
            get { return m_AssetAccount; }
            set { m_AssetAccount = value; }
        }
		
		public string GrossToNet
        {
            get { return m_IsGrossToNet; }
            set { m_IsGrossToNet = value; }
        }

        public string Frieght
        {
            get { return m_Frieght; }
            set { m_Frieght = value; }
        }

        public string Insurance
        {
            get { return m_Insurance; }
            set { m_Insurance = value; }
        }

        public string Discount
        {
            get { return m_Discount; }
            set { m_Discount = value; }
        }

        public string VendorLookup
        {
            get { return m_IsVendorLookup; }
            set { m_IsVendorLookup = value; }
        }

        public string SalesTax//bug no. 410
        {
            get { return m_SalesTax; }
            set { m_SalesTax = value; }
        }
        //416
        public string ALULookup
        {
            get { return m_ALULookup; }
            set { m_ALULookup = value; }
        }

        //Axis-565
        public string UPCLookup
        {
            get { return m_UPCLookup; }
            set { m_UPCLookup = value; }
        }
        //Axis-565 end

        public string SKULookup
        {
            get { return m_SKULookup; }
            set { m_SKULookup = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// method for save default settings.
        /// </summary>
        public void save()
        {
            if (IsExists(SettingAppName))
            {
                this.Update();
            }
            else
            {
                this.CreateNew();
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// This method is used to create settings.Xml file.
        /// </summary>
        private void CreateNew()
        {
            m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;

            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);

            XmlNode root = (XmlNode)xdoc.DocumentElement;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "ItemSettings")
                {
                    XmlNode settingsAdd = root.ChildNodes.Item(i).AppendChild(xdoc.CreateElement("QuickBook"));
                    settingsAdd.AppendChild(xdoc.CreateElement("File")).InnerText = m_fileName;
                    settingsAdd.AppendChild(xdoc.CreateElement("Type")).InnerText = m_Type;
                    settingsAdd.AppendChild(xdoc.CreateElement("TaxCode")).InnerText = m_TaxCode;
                    settingsAdd.AppendChild(xdoc.CreateElement("IncomeAccount")).InnerText = m_IncomeAccount;
                    settingsAdd.AppendChild(xdoc.CreateElement("COGSAccount")).InnerText = m_COGSAccount;
                    settingsAdd.AppendChild(xdoc.CreateElement("AssetAccount")).InnerText = m_AssetAccount;
                    settingsAdd.AppendChild(xdoc.CreateElement("IsGrossToNet")).InnerText = m_IsGrossToNet;
                    settingsAdd.AppendChild(xdoc.CreateElement("Freight")).InnerText = m_Frieght;
                    settingsAdd.AppendChild(xdoc.CreateElement("Insurance")).InnerText = m_Insurance;
                    settingsAdd.AppendChild(xdoc.CreateElement("Discount")).InnerText = m_Discount;
                    settingsAdd.AppendChild(xdoc.CreateElement("SalesTax")).InnerText = m_SalesTax;//bug no. 410
                }               
               
            }
            xdoc.Save(m_settingsPath);
        }

        /// <summary>
        /// This method update the Settings.xml file if QuickBook node already exist.
        /// </summary>
        private void Update()
        {
            m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;

            System.Xml.XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);

            XmlNode root = (XmlNode)xdoc.DocumentElement;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "ItemSettings")
                {
                    XmlNodeList ItemSettingsXml = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in ItemSettingsXml)
                    {
                        //Chekcing Current AppName(QuickBook).
                        if (node.Name == SettingAppName)
                        {
                            foreach (XmlNode childNodes in node.ChildNodes)
                            {
                                //Update node where filename matches.
                                if (childNodes.Name == "File")
                                {
                                    if (!IsFileExist(childNodes))
                                    {
                                        break;
                                    }
                                }
                                if (childNodes.Name == "Type")
                                {
                                    childNodes.InnerText = m_Type;
                                }
                                if (childNodes.Name == "TaxCode")
                                {
                                    childNodes.InnerText = m_TaxCode;
                                }
                                if (childNodes.Name == "IncomeAccount")
                                {
                                    childNodes.InnerText = m_IncomeAccount;
                                }
                                if (childNodes.Name == "COGSAccount")
                                {
                                    childNodes.InnerText = m_COGSAccount;
                                }
                                if (childNodes.Name == "AssetAccount")
                                {
                                    childNodes.InnerText = m_AssetAccount;
                                }
                                if (childNodes.Name == "IsGrossToNet")
                                {
                                    childNodes.InnerText = m_IsGrossToNet;
                                }
                                if (childNodes.Name == "Freight")
                                {
                                    childNodes.InnerText = m_Frieght;
                                }
                                if (childNodes.Name == "Insurance")
                                {
                                    childNodes.InnerText = m_Insurance;
                                }
                                if (childNodes.Name == "Discount")
                                {
                                    childNodes.InnerText = m_Discount;
                                }
                                if (childNodes.Name == "SalesTax")//bug no. 410
                                {
                                    childNodes.InnerText = m_SalesTax;
                                }
                            }                           
                        }

                    }
                   // break;
                }
                
               
            }
            xdoc.Save(m_settingsPath);
        }
      

        /// <summary>
        /// Check whether AppName Exist in Current settings.
        /// </summary>
        /// <param name="settingsAppName"></param>
        /// <returns></returns>
        private bool IsExists(string settingsAppName)
        {
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
           
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);

            XmlNode root = (XmlNode)xdoc.DocumentElement;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "ItemSettings")
                {
                    XmlNodeList ItemSettingsXml = root.ChildNodes.Item(i).ChildNodes;

                    foreach (XmlNode node in ItemSettingsXml)
                    {
                        if (node.Name == settingsAppName)
                        {
                            //Check whether settings for same filename is present, if not then return false.
                            foreach (XmlNode childNode in node.ChildNodes)
                            {
                                if (childNode.Name == "File")
                                {
                                    if (IsFileExist(childNode)) //If filename exist then return true else continue for other childnode.
                                    {
                                        return true;
                                    }
                                }
                            }
                           
                        }
                    }
                  
                }
            }
            return false;
        }

        /// <summary>
        /// Check whether settings for connnected file is present.
        /// </summary>
        /// <returns></returns>
        private bool IsFileExist(XmlNode fileNode)
        {
            if (fileNode.InnerText == CommonUtilities.GetInstance().CompanyFile)
                    return true;
            
            return false;

        }

        /// <summary>
        /// This method is used for getting Default Account and Settings Values.
        /// </summary>
        /// <returns></returns>
        public DefaultAccountSettings GetDefaultAccountSettings()
        {
            DefaultAccountSettings DefaultSettings = null;

            DefaultSettings = new DefaultAccountSettings();
           
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;

            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);

            XmlNode root = (XmlNode)xdoc.DocumentElement;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "ItemSettings")
                {
                    XmlNodeList ItemSettingsXml = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in ItemSettingsXml)
                    {
                        if (node.Name == SettingAppName)
                        {
                            if (DefaultSettings == null)
                            {
                                DefaultSettings = new DefaultAccountSettings();
                            }
                            foreach (XmlNode childNodes in node.ChildNodes)
                            {
                                if (childNodes.Name == "File")
                                {
                                    if (!IsFileExist(childNodes))
                                    {
                                        break;
                                    }
                                }
                                if (childNodes.Name == "Type")
                                {
                                    DefaultSettings.Type = childNodes.InnerText;
                                }
                                if (childNodes.Name == "TaxCode")
                                {
                                    DefaultSettings.TaxCode = childNodes.InnerText;
                                }
                                if (childNodes.Name == "IncomeAccount")
                                {
                                    DefaultSettings.IncomeAccount = childNodes.InnerText;
                                }
                                if (childNodes.Name == "COGSAccount")
                                {
                                    DefaultSettings.COGSAccount = childNodes.InnerText;
                                }
                                if (childNodes.Name == "AssetAccount")
                                {
                                    DefaultSettings.AssetAccount = childNodes.InnerText;
                                }
                                if (childNodes.Name == "IsGrossToNet")
                                {
                                    DefaultSettings.GrossToNet = childNodes.InnerText;
                                }
                                if (childNodes.Name == "Freight")
                                {
                                    DefaultSettings.Frieght = childNodes.InnerText;
                                }
                                if (childNodes.Name == "Insurance")
                                {
                                    DefaultSettings.Insurance = childNodes.InnerText;
                                }
                                if (childNodes.Name == "Discount")
                                {
                                    DefaultSettings.Discount = childNodes.InnerText;
                                }
                                if (childNodes.Name == "SalesTax")//bug no. 410
                                {
                                    DefaultSettings.SalesTax = childNodes.InnerText;
                                }
                                if (childNodes.Name == "ALULookup")
                                {
                                    DefaultSettings.ALULookup = childNodes.InnerText;
                                }
                                //Axis-565
                                if (childNodes.Name == "UPCLookup")
                                {
                                    DefaultSettings.UPCLookup = childNodes.InnerText;
                                }
                            }
                        }                       
                    }
                    break;
                }
            }
            return DefaultSettings;            
        }
        
        #endregion
    }
}
