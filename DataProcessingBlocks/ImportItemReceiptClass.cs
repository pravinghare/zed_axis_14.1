using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemReceiptClass
    {
        private static ImportItemReceiptClass m_ImportItemReceiptClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemReceiptClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemReceipt class
        /// </summary>
        /// <returns></returns>
        public static ImportItemReceiptClass GetInstance()
        {
            if (m_ImportItemReceiptClass == null)
                m_ImportItemReceiptClass = new ImportItemReceiptClass();
            return m_ImportItemReceiptClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemReceipt and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemReceipt QuickBooks collection </returns>
        public DataProcessingBlocks.ItemReceiptQBEntryCollection ImportItemReceiptData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemReceipt Entry collections.
            DataProcessingBlocks.ItemReceiptQBEntryCollection coll = new ItemReceiptQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemReceipt Entry

            DateTime ItemReceiptDt = new DateTime();
            string datevalue = string.Empty;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    if (dt.Columns.Contains("RefNumber"))
                    {

                        //ItemReceipt Validation
                        DataProcessingBlocks.ItemReceiptQBEntry ItemReceipt = new ItemReceiptQBEntry();
                        ItemReceipt = coll.FindItemReceiptEntry(dr["RefNumber"].ToString());

                        if (ItemReceipt == null)
                        {
                            #region For New RefNumber

                            ItemReceipt = new ItemReceiptQBEntry();

                            if (dt.Columns.Contains("VendorFullName"))
                            {
                                #region Validations of Vendor FullName
                                if (dr["VendorFullName"].ToString() != string.Empty)
                                {
                                    if (dr["VendorFullName"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor FullName (" + dr["VendorFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                                if (ItemReceipt.VendorRef.FullName == null)
                                                    ItemReceipt.VendorRef.FullName = null;
                                                else
                                                    ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString().Substring(0, 41));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                                if (ItemReceipt.VendorRef.FullName == null)
                                                    ItemReceipt.VendorRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                            if (ItemReceipt.VendorRef.FullName == null)
                                                ItemReceipt.VendorRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                        if (ItemReceipt.VendorRef.FullName == null)
                                            ItemReceipt.VendorRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("APAccountFullName"))
                            {
                                #region Validations of APAccount FullName
                                if (dr["APAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["APAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccount FullName (" + dr["APAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                                if (ItemReceipt.APAccountRef.FullName == null)
                                                    ItemReceipt.APAccountRef.FullName = null;
                                                else
                                                    ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                                if (ItemReceipt.APAccountRef.FullName == null)
                                                    ItemReceipt.APAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                            if (ItemReceipt.APAccountRef.FullName == null)
                                                ItemReceipt.APAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                        if (ItemReceipt.APAccountRef.FullName == null)
                                            ItemReceipt.APAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out ItemReceiptDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemReceipt.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemReceipt.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                ItemReceipt.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ItemReceiptDt = dttest;
                                            ItemReceipt.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        ItemReceiptDt = Convert.ToDateTime(datevalue);
                                        ItemReceipt.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of RefNumber
                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    if (dr["RefNumber"].ToString().Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This RefNumber (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.RefNumber = dr["RefNumber"].ToString().Substring(0, 20);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.RefNumber = dr["RefNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations of Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo (" + dr["Memo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.Memo = dr["Memo"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.Memo = dr["Memo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.Memo = dr["Memo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.Memo = dr["Memo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsTaxInclude"))
                            {
                                #region Validations of IsTaxInclude
                                if (dr["IsTaxInclude"].ToString() != "<None>" || dr["IsTaxInclude"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxInclude"].ToString(), out result))
                                    {
                                        ItemReceipt.IsTaxIncluded = Convert.ToInt32(dr["IsTaxInclude"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxInclude"].ToString().ToLower() == "true")
                                        {
                                            ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxInclude"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxInclude (" + dr["IsTaxInclude"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode FullName
                                if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTaxCode FullName (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                                    ItemReceipt.SalesTaxCodeRef.FullName = null;
                                                else
                                                    ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                                    ItemReceipt.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                                ItemReceipt.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                            ItemReceipt.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                            ItemReceipt.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                    else
                                    {
                                        ItemReceipt.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("LinkToTxnID"))
                            {
                                #region Validations for LinkToTxnID
                                if (dr["LinkToTxnID"].ToString() != string.Empty)
                                {
                                    string strLinkToTxnID = dr["LinkToTxnID"].ToString();
                                    ItemReceipt.LinkToTxnID = strLinkToTxnID;

                                }

                                #endregion
                            }

                            ExpenseLineAdd ExpenseLineAddItem = new ExpenseLineAdd();

                            if (dt.Columns.Contains("ExpenseLineAddAccountFullName"))
                            {
                                #region Validations of ExpenseLineAdd Account FullName
                                if (dr["ExpenseLineAddAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddAccount FullName (" + dr["ExpenseLineAddAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                                if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                    ExpenseLineAddItem.AccountRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                                if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                    ExpenseLineAddItem.AccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                            if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                ExpenseLineAddItem.AccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                        if (ExpenseLineAddItem.AccountRef.FullName == null)
                                            ExpenseLineAddItem.AccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddAmount"))
                            {
                                #region Validations for ExpenseLineAddAmount
                                if (dr["ExpenseLineAddAmount"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["ExpenseLineAddAmount"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddAmount ( " + dr["ExpenseLineAddAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ExpenseLineAddAmount"].ToString();
                                                ExpenseLineAddItem.Amount = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ExpenseLineAddAmount"].ToString();
                                                ExpenseLineAddItem.Amount = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ExpenseLineAddAmount"].ToString();
                                            ExpenseLineAddItem.Amount = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["ExpenseLineAddAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ExpenseLineAddItem.Amount = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ExpenseLineAddItem.Amount == null)
                                            {
                                                ExpenseLineAddItem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseLineAddAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseLineAddAmount"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddMemo"))
                            {
                                #region Validations of Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAdd Memo (" + dr["ExpenseLineAddMemo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddCustomerFullName"))
                            {
                                #region Validations of ExpenseLineAdd Customer FullName
                                if (dr["ExpenseLineAddCustomerFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddCustomerFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddCustomer FullName (" + dr["ExpenseLineAddCustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                                if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                    ExpenseLineAddItem.CustomerRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString().Substring(0, 209));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                                if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                    ExpenseLineAddItem.CustomerRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                            if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                ExpenseLineAddItem.CustomerRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                        if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                            ExpenseLineAddItem.CustomerRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddClassFullName"))
                            {
                                #region Validations of ExpenseLineAdd Class FullName
                                if (dr["ExpenseLineAddClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddClassFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddClass FullName (" + dr["ExpenseLineAddClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                                if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                    ExpenseLineAddItem.ClassRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                                if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                    ExpenseLineAddItem.ClassRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                            if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                ExpenseLineAddItem.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                        if (ExpenseLineAddItem.ClassRef.FullName == null)
                                            ExpenseLineAddItem.ClassRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddSalesTaxCodeFullName"))
                            {
                                #region Validations of ExpenseLineAdd SalesTaxCode FullName
                                if (dr["ExpenseLineAddSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddSalesTaxCodeFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddSalesTaxCode FullName (" + dr["ExpenseLineAddSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                    ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                    ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                        if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                            ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddBillableStatus"))
                            {
                                #region Validations of ExpenseLineAdd BillableStatus
                                if (dr["ExpenseLineAddBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ExpenseLineAddItem.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseLineAddBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ExpenseLineAddItem.BillableStatus = dr["ExpenseLineAddBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                                {
                                                    ExpenseLineAddItem.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                                {
                                                    ExpenseLineAddItem.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                            {
                                                ExpenseLineAddItem.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                        if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                        {
                                            ExpenseLineAddItem.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                            //        if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                            //        {
                            //            ExpenseLineAddItem.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpenseLineAddItem.DataExt1 == null)
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt1.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpenseLineAddItem.DataExt1 == null)
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpenseLineAddItem.DataExt1 == null)
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpenseLineAddItem.DataExt1 == null)
                                            ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                        if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                        {
                                            ExpenseLineAddItem.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ExpenseLineAddItem.DataExt1 == null)
                                        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                    else
                                        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, ExpenseLineAddItem.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                    if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                    {
                                        ExpenseLineAddItem.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                            //        if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                            //        {
                            //            ExpenseLineAddItem.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpenseLineAddItem.DataExt2 == null)
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt2.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpenseLineAddItem.DataExt2 == null)
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpenseLineAddItem.DataExt2 == null)
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpenseLineAddItem.DataExt2 == null)
                                            ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                        if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                        {
                                            ExpenseLineAddItem.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ExpenseLineAddItem.DataExt2 == null)
                                        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                    else
                                        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, ExpenseLineAddItem.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                    if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                    {
                                        ExpenseLineAddItem.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (ExpenseLineAddItem.AccountRef != null || ExpenseLineAddItem.Amount != null || ExpenseLineAddItem.BillableStatus != null || ExpenseLineAddItem.ClassRef != null || ExpenseLineAddItem.CustomerRef != null || ExpenseLineAddItem.Memo != null || ExpenseLineAddItem.SalesTaxCodeRef != null || ExpenseLineAddItem.SalesRepRef != null)
                                ItemReceipt.ExpenseLineAdd.Add(ExpenseLineAddItem);



                            ItemLineAdd ItemLineItem = new ItemLineAdd();

                            if (dt.Columns.Contains("ItemLineAddItemFullName"))
                            {
                                #region Validations of ItemLineAdd Item FullName
                                if (dr["ItemLineAddItemFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddItemFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd ItemFullName (" + dr["ItemLineAddItemFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                                if (ItemLineItem.ItemRef.FullName == null)
                                                    ItemLineItem.ItemRef.FullName = null;
                                                else
                                                    ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                                if (ItemLineItem.ItemRef.FullName == null)
                                                    ItemLineItem.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                            if (ItemLineItem.ItemRef.FullName == null)
                                                ItemLineItem.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                        if (ItemLineItem.ItemRef.FullName == null)
                                            ItemLineItem.ItemRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddInventorySiteFullName"))
                            {
                                #region Validations of ItemLineAdd Item FullName
                                if (dr["ItemLineAddInventorySiteFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddInventorySiteFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAddInventorySite FullName (" + dr["ItemLineAddInventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                                if (ItemLineItem.InventorySiteRef.FullName == null)
                                                    ItemLineItem.InventorySiteRef.FullName = null;
                                                else
                                                    ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                                if (ItemLineItem.InventorySiteRef.FullName == null)
                                                    ItemLineItem.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                            if (ItemLineItem.InventorySiteRef.FullName == null)
                                                ItemLineItem.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                        if (ItemLineItem.InventorySiteRef.FullName == null)
                                            ItemLineItem.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddInventorySiteLocationFullName"))
                            {
                                #region Validations of ItemLineAdd InventorySiteLocation FullName
                                if (dr["ItemLineAddInventorySiteLocationFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddInventorySiteLocationFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd InventorySiteLocation FullName  (" + dr["ItemLineAddInventorySiteLocationFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLineAdd SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                                if (ItemLineItem.SerialNumber == null)
                                                    ItemLineItem.SerialNumber = null;
                                                else
                                                    ItemLineItem.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                                if (ItemLineItem.SerialNumber == null)
                                                    ItemLineItem.SerialNumber = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                            if (ItemLineItem.SerialNumber == null)
                                                ItemLineItem.SerialNumber = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                        if (ItemLineItem.SerialNumber == null)
                                            ItemLineItem.SerialNumber = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLineAdd LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                                if (ItemLineItem.LotNumber == null)
                                                    ItemLineItem.LotNumber = null;
                                                else
                                                    ItemLineItem.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                                if (ItemLineItem.LotNumber == null)
                                                    ItemLineItem.LotNumber = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                            if (ItemLineItem.LotNumber == null)
                                                ItemLineItem.LotNumber = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                        if (ItemLineItem.LotNumber == null)
                                            ItemLineItem.LotNumber = null;
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemLineAddDesc"))
                            {
                                #region Validations of ItemLineAddDesc
                                if (dr["ItemLineAddDesc"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd Desc (" + dr["ItemLineAddDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemLineAddQuantity"))
                            {
                                #region Validations for ItemLineAddQuantity
                                if (dr["ItemLineAddQuantity"].ToString() != string.Empty)
                                {
                                    string strItemLineAddQuantity = dr["ItemLineAddQuantity"].ToString();
                                    ItemLineItem.Quantity = strItemLineAddQuantity;

                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddUnitOfMeasure"))
                            {
                                #region Validations of ItemLineAddUnitOfMeasure
                                if (dr["ItemLineAddUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddUnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd UnitOfMeasure (" + dr["ItemLineAddUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddCost"))
                            {
                                #region Validations for ItemLineAddCost
                                if (dr["ItemLineAddCost"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["ItemLineAddCost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAddCost ( " + dr["ItemLineAddCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ItemLineAddCost"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ItemLineAddCost"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ItemLineAddCost"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["ItemLineAddCost"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLineItem.Cost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLineItem.Cost == null)
                                            {
                                                //axis 10.0 change decimal poit from 2 to 5
                                                //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                                ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            //axis 10.0 change decimal poit from 2 to 5
                                            //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                            ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddAmount"))
                            {
                                #region Validations for ItemLineAddAmount
                                if (dr["ItemLineAddAmount"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["ItemLineAddAmount"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAddAmount ( " + dr["ItemLineAddAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ItemLineAddAmount"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ItemLineAddAmount"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ItemLineAddAmount"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["ItemLineAddAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLineItem.Cost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLineItem.Cost == null)
                                            {
                                                //axis 10.0 change decimal poit from 2 to 5
                                                // ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                                ItemLineItem.Cost = string.Format("{0:000000.000000}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            //axis 10.0 change decimal poit from 2 to 5
                                            //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                            ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddCustomerFullName"))
                            {
                                #region Validations of ItemLineAdd Customer FullName
                                if (dr["ItemLineAddCustomerFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddCustomerFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd Customer FullName (" + dr["ItemLineAddCustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                                if (ItemLineItem.CustomerRef.FullName == null)
                                                    ItemLineItem.CustomerRef.FullName = null;
                                                else
                                                    ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString().Substring(0, 209));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                                if (ItemLineItem.CustomerRef.FullName == null)
                                                    ItemLineItem.CustomerRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                            if (ItemLineItem.CustomerRef.FullName == null)
                                                ItemLineItem.CustomerRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                        if (ItemLineItem.CustomerRef.FullName == null)
                                            ItemLineItem.CustomerRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddClassFullName"))
                            {
                                #region Validations of ItemLineAdd Class FullName
                                if (dr["ItemLineAddClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddClassFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd Class FullName (" + dr["ItemLineAddClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                                if (ItemLineItem.ClassRef.FullName == null)
                                                    ItemLineItem.ClassRef.FullName = null;
                                                else
                                                    ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                                if (ItemLineItem.ClassRef.FullName == null)
                                                    ItemLineItem.ClassRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                            if (ItemLineItem.ClassRef.FullName == null)
                                                ItemLineItem.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                        if (ItemLineItem.ClassRef.FullName == null)
                                            ItemLineItem.ClassRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddSalesTaxCodeFullName"))
                            {
                                #region Validations of ItemLineAdd SalesTaxCode FullName
                                if (dr["ItemLineAddSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddSalesTaxCodeFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd SalesTaxCode FullName (" + dr["ItemLineAddSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                                if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                    ItemLineItem.SalesTaxCodeRef.FullName = null;
                                                else
                                                    ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                                if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                    ItemLineItem.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                            if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                ItemLineItem.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                        if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                            ItemLineItem.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddBillableStatus"))
                            {
                                #region Validations of ItemLineAddBillableStatus
                                if (dr["ItemLineAddBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemLineItem.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemLineAddBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemLineItem.BillableStatus = dr["ItemLineAddBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddOverrideItemAccountFullName"))
                            {
                                #region Validations of ItemLineAdd OverrideItemAccount FullName
                                if (dr["ItemLineAddOverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddOverrideItemAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd OverrideItemAccount FullName (" + dr["ItemLineAddOverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                                if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                    ItemLineItem.OverrideItemAccountRef.FullName = null;
                                                else
                                                    ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                                if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                    ItemLineItem.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                            if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                ItemLineItem.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                        if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                            ItemLineItem.OverrideItemAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddLinkToTxnTxnID"))
                            {
                                #region Validations for ItemLineAdd LinkToTxn TxnID

                                if (dr["ItemLineAddLinkToTxnTxnID"].ToString() != string.Empty)
                                {
                                    ItemLineItem.LinkToTxn = new LinkToTxn(dr["ItemLineAddLinkToTxnTxnID"].ToString(), string.Empty);
                                }

                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                            {
                                #region Validations of ItemSalesRepRef Full name
                                if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLineItem.SalesRepRef.FullName == null)
                                                {
                                                    ItemLineItem.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLineItem.SalesRepRef.FullName == null)
                                                {
                                                    ItemLineItem.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLineItem.SalesRepRef.FullName == null)
                                            {
                                                ItemLineItem.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                        if (ItemLineItem.SalesRepRef.FullName == null)
                                        {
                                            ItemLineItem.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ItemLineItem.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                            //        if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                            //        {
                            //            ItemLineItem.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLineItem.DataExt1 == null)
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt1.DataExtValue);
                                                if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLineItem.DataExt1 == null)
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                                if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLineItem.DataExt1 == null)
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                            if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLineItem.DataExt1 == null)
                                            ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                        if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                        {
                                            ItemLineItem.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ItemLineItem.DataExt1 == null)
                                        ItemLineItem.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                    else
                                        ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, ItemLineItem.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                    if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                    {
                                        ItemLineItem.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ItemLineItem.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                            //        if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                            //        {
                            //            ItemLineItem.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLineItem.DataExt2 == null)
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt2.DataExtValue);
                                                if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLineItem.DataExt2 == null)
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                                if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLineItem.DataExt2 == null)
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                            if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLineItem.DataExt2 == null)
                                            ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                        if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                        {
                                            ItemLineItem.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ItemLineItem.DataExt2 == null)
                                        ItemLineItem.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                    else
                                        ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, ItemLineItem.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                    if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                    {
                                        ItemLineItem.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("ItemLineAddLinkToTxnTxnLineID"))
                            {
                                #region Validations for ItemLineAdd LinkToTxn TxnLineID
                                if (dr["ItemLineAddLinkToTxnTxnLineID"].ToString() != string.Empty)
                                {
                                    //string strItemLineAddLinkToTxnTxnLineID = dr["ItemLineAddLinkToTxnTxnLineID"].ToString();
                                    //ItemLineItem.LinkToTxn.TxnLineID = strItemLineAddLinkToTxnTxnLineID;
                                    ItemLineItem.LinkToTxn = new LinkToTxn(string.Empty, dr["ItemLineAddLinkToTxnTxnLineID"].ToString());

                                }

                                #endregion
                            }

                            if (ItemLineItem.Amount != null || ItemLineItem.BillableStatus != null || ItemLineItem.ClassRef != null || ItemLineItem.Cost != null || ItemLineItem.CustomerRef != null || ItemLineItem.Desc != null || ItemLineItem.InventorySiteRef != null || ItemLineItem.ItemRef != null || ItemLineItem.LinkToTxn != null || ItemLineItem.SalesRepRef != null || ItemLineItem.DataExt1.DataExtName != null || ItemLineItem.DataExt1.DataExtValue != null || ItemLineItem.OverrideItemAccountRef != null || ItemLineItem.Quantity != null || ItemLineItem.SalesTaxCodeRef != null || ItemLineItem.UnitOfMeasure != null)
                                ItemReceipt.ItemLineAdd.Add(ItemLineItem);




                            ItemGroupLineAdd itemGroupItem = new ItemGroupLineAdd();


                            if (dt.Columns.Contains("ItemGroupLineAddItemGroupFullName"))
                            {
                                #region Validations of ItemGroupLineAdd ItemGroup FullName
                                if (dr["ItemGroupLineAddItemGroupFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddItemGroupFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAdd ItemGroup FullName (" + dr["ItemGroupLineAddItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                                if (itemGroupItem.ItemGroupRef.FullName == null)
                                                    itemGroupItem.ItemGroupRef.FullName = null;
                                                else
                                                    itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                                if (itemGroupItem.ItemGroupRef.FullName == null)
                                                    itemGroupItem.ItemGroupRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                            if (itemGroupItem.ItemGroupRef.FullName == null)
                                                itemGroupItem.ItemGroupRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                        if (itemGroupItem.ItemGroupRef.FullName == null)
                                            itemGroupItem.ItemGroupRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupLineAddQuantity"))
                            {
                                #region Validations for ItemGroupLineAdd Quantity
                                if (dr["ItemGroupLineAddQuantity"].ToString() != string.Empty)
                                {
                                    string strItemGroupLineAddQuantity = dr["ItemGroupLineAddQuantity"].ToString();
                                    itemGroupItem.Quantity = strItemGroupLineAddQuantity;

                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupLineAddUnitOfMeasure"))
                            {
                                #region Validations of ItemGroupLineAdd UnitOfMeasure
                                if (dr["ItemGroupLineAddUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddUnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAdd UnitOfMeasure (" + dr["ItemGroupLineAddUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemGroupLineAddInventorySiteFullName"))
                            {
                                #region Validations of ItemGroupLineAdd InventorySite FullName
                                if (dr["ItemGroupLineAddInventorySiteFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddInventorySiteFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAdd InventorySite FullName (" + dr["ItemGroupLineAddInventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                                if (itemGroupItem.InventorySiteRef.FullName == null)
                                                    itemGroupItem.InventorySiteRef.FullName = null;
                                                else
                                                    itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                                if (itemGroupItem.InventorySiteRef.FullName == null)
                                                    itemGroupItem.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                            if (itemGroupItem.InventorySiteRef.FullName == null)
                                                itemGroupItem.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                        if (itemGroupItem.InventorySiteRef.FullName == null)
                                            itemGroupItem.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemGroupLineAddOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemGroupLineAddOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        itemGroupItem.DataExt1 = DataExt.GetInstance(dr["ItemGroupLineAddOwnerID1"].ToString(), null, null);
                            //        if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                            //        {
                            //            itemGroupItem.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemGroupLineAddDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAddDataExtName1 (" + dr["ItemGroupLineAddDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (itemGroupItem.DataExt1 == null)
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString().Substring(0, 31), itemGroupItem.DataExt1.DataExtValue);
                                                if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (itemGroupItem.DataExt1 == null)
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                                if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (itemGroupItem.DataExt1 == null)
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                            if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (itemGroupItem.DataExt1 == null)
                                            itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                        else
                                            itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                        if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                        {
                                            itemGroupItem.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion
                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemGroupLineAddDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (itemGroupItem.DataExt1 == null)
                                        itemGroupItem.DataExt1 = DataExt.GetInstance(null, null, dr["ItemGroupLineAddDataExtValue1"].ToString());
                                    else
                                        itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, itemGroupItem.DataExt1.DataExtName, dr["ItemGroupLineAddDataExtValue1"].ToString());
                                    if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                    {
                                        itemGroupItem.DataExt1 = null;
                                    }

                                }

                                #endregion
                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemGroupLineAddOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemGroupLineAddOwnerID2"].ToString() != string.Empty)
                            //    {
                            //        itemGroupItem.DataExt2 = DataExt.GetInstance(dr["ItemGroupLineAddOwnerID2"].ToString(), null, null);
                            //        if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                            //        {
                            //            itemGroupItem.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemGroupLineAddDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAddDataExtName2 (" + dr["ItemGroupLineAddDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (itemGroupItem.DataExt2 == null)
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString().Substring(0, 31), itemGroupItem.DataExt2.DataExtValue);
                                                if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (itemGroupItem.DataExt2 == null)
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                                if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (itemGroupItem.DataExt2 == null)
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                            if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (itemGroupItem.DataExt2 == null)
                                            itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                        else
                                            itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                        if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                        {
                                            itemGroupItem.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemGroupLineAddDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (itemGroupItem.DataExt2 == null)
                                        itemGroupItem.DataExt2 = DataExt.GetInstance(null, null, dr["ItemGroupLineAddDataExtValue2"].ToString());
                                    else
                                        itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, itemGroupItem.DataExt2.DataExtName, dr["ItemGroupLineAddDataExtValue2"].ToString());
                                    if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                    {
                                        itemGroupItem.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }


                            if (itemGroupItem.InventorySiteRef != null || itemGroupItem.ItemGroupRef != null || itemGroupItem.Quantity != null || itemGroupItem.UnitOfMeasure != null /* || itemGroupItem.DataExt.DataExtName != null || itemGroupItem.DataExt.DataExtValue != null*/)
                                ItemReceipt.ItemGroupLineAdd.Add(itemGroupItem);


                            coll.Add(ItemReceipt);
                            #endregion
                        }
                        else
                        {
                            #region for existing Ref Number

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion


                            ExpenseLineAdd ExpenseLineAddItem = new ExpenseLineAdd();

                            #region Expense Line Add



                            if (dt.Columns.Contains("ExpenseLineAddAccountFullName"))
                            {
                                #region Validations of ExpenseLineAdd Account FullName
                                if (dr["ExpenseLineAddAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddAccount FullName (" + dr["ExpenseLineAddAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                                if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                    ExpenseLineAddItem.AccountRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                                if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                    ExpenseLineAddItem.AccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                            if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                ExpenseLineAddItem.AccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                        if (ExpenseLineAddItem.AccountRef.FullName == null)
                                            ExpenseLineAddItem.AccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddAmount"))
                            {
                                #region Validations for ExpenseLineAddAmount
                                if (dr["ExpenseLineAddAmount"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["ExpenseLineAddAmount"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddAmount ( " + dr["ExpenseLineAddAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ExpenseLineAddAmount"].ToString();
                                                ExpenseLineAddItem.Amount = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ExpenseLineAddAmount"].ToString();
                                                ExpenseLineAddItem.Amount = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ExpenseLineAddAmount"].ToString();
                                            ExpenseLineAddItem.Amount = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["ExpenseLineAddAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ExpenseLineAddItem.Amount = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ExpenseLineAddItem.Amount == null)
                                            {
                                                ExpenseLineAddItem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseLineAddAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseLineAddAmount"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddMemo"))
                            {
                                #region Validations of Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAdd Memo (" + dr["ExpenseLineAddMemo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddCustomerFullName"))
                            {
                                #region Validations of ExpenseLineAdd Customer FullName
                                if (dr["ExpenseLineAddCustomerFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddCustomerFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddCustomer FullName (" + dr["ExpenseLineAddCustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                                if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                    ExpenseLineAddItem.CustomerRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString().Substring(0, 209));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                                if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                    ExpenseLineAddItem.CustomerRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                            if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                ExpenseLineAddItem.CustomerRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                        if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                            ExpenseLineAddItem.CustomerRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddClassFullName"))
                            {
                                #region Validations of ExpenseLineAdd Class FullName
                                if (dr["ExpenseLineAddClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddClassFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddClass FullName (" + dr["ExpenseLineAddClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                                if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                    ExpenseLineAddItem.ClassRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                                if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                    ExpenseLineAddItem.ClassRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                            if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                ExpenseLineAddItem.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                        if (ExpenseLineAddItem.ClassRef.FullName == null)
                                            ExpenseLineAddItem.ClassRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddSalesTaxCodeFullName"))
                            {
                                #region Validations of ExpenseLineAdd SalesTaxCode FullName
                                if (dr["ExpenseLineAddSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseLineAddSalesTaxCodeFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseLineAddSalesTaxCode FullName (" + dr["ExpenseLineAddSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                    ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                                else
                                                    ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                    ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                        if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                            ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseLineAddBillableStatus"))
                            {
                                #region Validations of ExpenseLineAdd BillableStatus
                                if (dr["ExpenseLineAddBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ExpenseLineAddItem.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseLineAddBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ExpenseLineAddItem.BillableStatus = dr["ExpenseLineAddBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                                {
                                                    ExpenseLineAddItem.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                                {
                                                    ExpenseLineAddItem.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                            {
                                                ExpenseLineAddItem.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                        if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                        {
                                            ExpenseLineAddItem.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                            //        if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                            //        {
                            //            ExpenseLineAddItem.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpenseLineAddItem.DataExt1 == null)
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt1.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpenseLineAddItem.DataExt1 == null)
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpenseLineAddItem.DataExt1 == null)
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpenseLineAddItem.DataExt1 == null)
                                            ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                        if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                        {
                                            ExpenseLineAddItem.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ExpenseLineAddItem.DataExt1 == null)
                                        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                    else
                                        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, ExpenseLineAddItem.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                    if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                    {
                                        ExpenseLineAddItem.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                            //        if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                            //        {
                            //            ExpenseLineAddItem.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpenseLineAddItem.DataExt2 == null)
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt2.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpenseLineAddItem.DataExt2 == null)
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                                if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                                {
                                                    ExpenseLineAddItem.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpenseLineAddItem.DataExt2 == null)
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpenseLineAddItem.DataExt2 == null)
                                            ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                        if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                        {
                                            ExpenseLineAddItem.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ExpenseLineAddItem.DataExt2 == null)
                                        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                    else
                                        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, ExpenseLineAddItem.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                    if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                    {
                                        ExpenseLineAddItem.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (ExpenseLineAddItem.AccountRef != null || ExpenseLineAddItem.Amount != null || ExpenseLineAddItem.BillableStatus != null || ExpenseLineAddItem.ClassRef != null || ExpenseLineAddItem.CustomerRef != null || ExpenseLineAddItem.Memo != null || ExpenseLineAddItem.SalesTaxCodeRef != null || ExpenseLineAddItem.SalesRepRef != null)
                                ItemReceipt.ExpenseLineAdd.Add(ExpenseLineAddItem);

                            #endregion



                            ItemLineAdd ItemLineItem = new ItemLineAdd();

                            #region Item Line Add

                            if (dt.Columns.Contains("ItemLineAddItemFullName"))
                            {
                                #region Validations of ItemLineAdd Item FullName
                                if (dr["ItemLineAddItemFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddItemFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd ItemFullName (" + dr["ItemLineAddItemFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                                if (ItemLineItem.ItemRef.FullName == null)
                                                    ItemLineItem.ItemRef.FullName = null;
                                                else
                                                    ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                                if (ItemLineItem.ItemRef.FullName == null)
                                                    ItemLineItem.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                            if (ItemLineItem.ItemRef.FullName == null)
                                                ItemLineItem.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                        if (ItemLineItem.ItemRef.FullName == null)
                                            ItemLineItem.ItemRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddInventorySiteLocationFullName"))
                            {
                                #region Validations of ItemLineAdd InventorySiteLocation FullName
                                if (dr["ItemLineAddInventorySiteLocationFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddInventorySiteLocationFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd InventorySiteLocation FullName  (" + dr["ItemLineAddInventorySiteLocationFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLineAdd SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                                if (ItemLineItem.SerialNumber == null)
                                                    ItemLineItem.SerialNumber = null;
                                                else
                                                    ItemLineItem.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                                if (ItemLineItem.SerialNumber == null)
                                                    ItemLineItem.SerialNumber = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                            if (ItemLineItem.SerialNumber == null)
                                                ItemLineItem.SerialNumber = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                        if (ItemLineItem.SerialNumber == null)
                                            ItemLineItem.SerialNumber = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLineAdd LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                                if (ItemLineItem.LotNumber == null)
                                                    ItemLineItem.LotNumber = null;
                                                else
                                                    ItemLineItem.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                                if (ItemLineItem.LotNumber == null)
                                                    ItemLineItem.LotNumber = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                            if (ItemLineItem.LotNumber == null)
                                                ItemLineItem.LotNumber = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                        if (ItemLineItem.LotNumber == null)
                                            ItemLineItem.LotNumber = null;
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemLineAddInventorySiteFullName"))
                            {
                                #region Validations of ItemLineAdd Item FullName
                                if (dr["ItemLineAddInventorySiteFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddInventorySiteFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAddInventorySite FullName (" + dr["ItemLineAddInventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                                if (ItemLineItem.InventorySiteRef.FullName == null)
                                                    ItemLineItem.InventorySiteRef.FullName = null;
                                                else
                                                    ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                                if (ItemLineItem.InventorySiteRef.FullName == null)
                                                    ItemLineItem.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                            if (ItemLineItem.InventorySiteRef.FullName == null)
                                                ItemLineItem.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                        if (ItemLineItem.InventorySiteRef.FullName == null)
                                            ItemLineItem.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddDesc"))
                            {
                                #region Validations of ItemLineAddDesc
                                if (dr["ItemLineAddDesc"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd Desc (" + dr["ItemLineAddDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("ItemLineAddQuantity"))
                            {
                                #region Validations for ItemLineAddQuantity
                                if (dr["ItemLineAddQuantity"].ToString() != string.Empty)
                                {
                                    string strItemLineAddQuantity = dr["ItemLineAddQuantity"].ToString();
                                    ItemLineItem.Quantity = strItemLineAddQuantity;

                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddUnitOfMeasure"))
                            {
                                #region Validations of ItemLineAddUnitOfMeasure
                                if (dr["ItemLineAddUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddUnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd UnitOfMeasure (" + dr["ItemLineAddUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddCost"))
                            {
                                #region Validations for ItemLineAddCost
                                if (dr["ItemLineAddCost"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["ItemLineAddCost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAddCost ( " + dr["ItemLineAddCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ItemLineAddCost"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ItemLineAddCost"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ItemLineAddCost"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["ItemLineAddCost"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLineItem.Cost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLineItem.Cost == null)
                                            {
                                                //axis 10.0 change decimal poit from 2 to 5
                                                //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                                ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            //axis 10.0 change decimal poit from 2 to 5
                                            //  ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                            ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddAmount"))
                            {
                                #region Validations for ItemLineAddAmount
                                if (dr["ItemLineAddAmount"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["ItemLineAddAmount"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAddAmount ( " + dr["ItemLineAddAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ItemLineAddAmount"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ItemLineAddAmount"].ToString();
                                                ItemLineItem.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ItemLineAddAmount"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["ItemLineAddAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLineItem.Cost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLineItem.Cost == null)
                                            {
                                                //axis 10.0 change decimal poit from 2 to 5
                                                //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                                ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            //axis 10.0 change decimal poit from 2 to 5
                                            //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                            ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddCustomerFullName"))
                            {
                                #region Validations of ItemLineAdd Customer FullName
                                if (dr["ItemLineAddCustomerFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddCustomerFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd Customer FullName (" + dr["ItemLineAddCustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                                if (ItemLineItem.CustomerRef.FullName == null)
                                                    ItemLineItem.CustomerRef.FullName = null;
                                                else
                                                    ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString().Substring(0, 209));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                                if (ItemLineItem.CustomerRef.FullName == null)
                                                    ItemLineItem.CustomerRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                            if (ItemLineItem.CustomerRef.FullName == null)
                                                ItemLineItem.CustomerRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                        if (ItemLineItem.CustomerRef.FullName == null)
                                            ItemLineItem.CustomerRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddClassFullName"))
                            {
                                #region Validations of ItemLineAdd Class FullName
                                if (dr["ItemLineAddClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddClassFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd Class FullName (" + dr["ItemLineAddClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                                if (ItemLineItem.ClassRef.FullName == null)
                                                    ItemLineItem.ClassRef.FullName = null;
                                                else
                                                    ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                                if (ItemLineItem.ClassRef.FullName == null)
                                                    ItemLineItem.ClassRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                            if (ItemLineItem.ClassRef.FullName == null)
                                                ItemLineItem.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                        if (ItemLineItem.ClassRef.FullName == null)
                                            ItemLineItem.ClassRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddSalesTaxCodeFullName"))
                            {
                                #region Validations of ItemLineAdd SalesTaxCode FullName
                                if (dr["ItemLineAddSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddSalesTaxCodeFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd SalesTaxCode FullName (" + dr["ItemLineAddSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                                if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                    ItemLineItem.SalesTaxCodeRef.FullName = null;
                                                else
                                                    ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                                if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                    ItemLineItem.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                            if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                ItemLineItem.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                        if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                            ItemLineItem.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddBillableStatus"))
                            {
                                #region Validations of ItemLineAddBillableStatus
                                if (dr["ItemLineAddBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemLineItem.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemLineAddBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemLineItem.BillableStatus = dr["ItemLineAddBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddOverrideItemAccountFullName"))
                            {
                                #region Validations of ItemLineAdd OverrideItemAccount FullName
                                if (dr["ItemLineAddOverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemLineAddOverrideItemAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemLineAdd OverrideItemAccount FullName (" + dr["ItemLineAddOverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                                if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                    ItemLineItem.OverrideItemAccountRef.FullName = null;
                                                else
                                                    ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                                if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                    ItemLineItem.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                            if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                ItemLineItem.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                        if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                            ItemLineItem.OverrideItemAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemLineAddLinkToTxnTxnID"))
                            {
                                #region Validations for ItemLineAdd LinkToTxn TxnID

                                if (dr["ItemLineAddLinkToTxnTxnID"].ToString() != string.Empty)
                                {
                                    ItemLineItem.LinkToTxn = new LinkToTxn(dr["ItemLineAddLinkToTxnTxnID"].ToString(), string.Empty);
                                }

                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                            {
                                #region Validations of ItemSalesRepRef Full name
                                if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLineItem.SalesRepRef.FullName == null)
                                                {
                                                    ItemLineItem.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLineItem.SalesRepRef.FullName == null)
                                                {
                                                    ItemLineItem.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLineItem.SalesRepRef.FullName == null)
                                            {
                                                ItemLineItem.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                        if (ItemLineItem.SalesRepRef.FullName == null)
                                        {
                                            ItemLineItem.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ItemLineItem.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                            //        if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                            //        {
                            //            ItemLineItem.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLineItem.DataExt1 == null)
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt1.DataExtValue);
                                                if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLineItem.DataExt1 == null)
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                                if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLineItem.DataExt1 == null)
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                            if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLineItem.DataExt1 == null)
                                            ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                        if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                        {
                                            ItemLineItem.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ItemLineItem.DataExt1 == null)
                                        ItemLineItem.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                    else
                                        ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, ItemLineItem.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                    if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                    {
                                        ItemLineItem.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ItemLineItem.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                            //        if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                            //        {
                            //            ItemLineItem.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLineItem.DataExt2 == null)
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt2.DataExtValue);
                                                if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLineItem.DataExt2 == null)
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                                if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLineItem.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLineItem.DataExt2 == null)
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                            if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLineItem.DataExt2 == null)
                                            ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                        if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                        {
                                            ItemLineItem.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ItemLineItem.DataExt2 == null)
                                        ItemLineItem.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                    else
                                        ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, ItemLineItem.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                    if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                    {
                                        ItemLineItem.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }


                            if (dt.Columns.Contains("ItemLineAddLinkToTxnTxnLineID"))
                            {
                                #region Validations for ItemLineAdd LinkToTxn TxnLineID
                                if (dr["ItemLineAddLinkToTxnTxnLineID"].ToString() != string.Empty)
                                {
                                    ItemLineItem.LinkToTxn = new LinkToTxn(string.Empty, dr["ItemLineAddLinkToTxnTxnLineID"].ToString());

                                }

                                #endregion
                            }

                            if (ItemLineItem.Amount != null || ItemLineItem.BillableStatus != null || ItemLineItem.ClassRef != null || ItemLineItem.Cost != null || ItemLineItem.CustomerRef != null || ItemLineItem.Desc != null || ItemLineItem.InventorySiteRef != null || ItemLineItem.ItemRef != null || ItemLineItem.LinkToTxn != null || ItemLineItem.OverrideItemAccountRef != null || ItemLineItem.Quantity != null || ItemLineItem.SalesTaxCodeRef != null || ItemLineItem.SalesRepRef != null || ItemLineItem.DataExt1.DataExtName != null || ItemLineItem.DataExt1.DataExtValue != null || ItemLineItem.UnitOfMeasure != null)
                                ItemReceipt.ItemLineAdd.Add(ItemLineItem);

                            #endregion



                            ItemGroupLineAdd itemGroupItem = new ItemGroupLineAdd();

                            #region Item Group Add

                            if (dt.Columns.Contains("ItemGroupLineAddItemGroupFullName"))
                            {
                                #region Validations of ItemGroupLineAdd ItemGroup FullName
                                if (dr["ItemGroupLineAddItemGroupFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddItemGroupFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAdd ItemGroup FullName (" + dr["ItemGroupLineAddItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                                if (itemGroupItem.ItemGroupRef.FullName == null)
                                                    itemGroupItem.ItemGroupRef.FullName = null;
                                                else
                                                    itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                                if (itemGroupItem.ItemGroupRef.FullName == null)
                                                    itemGroupItem.ItemGroupRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                            if (itemGroupItem.ItemGroupRef.FullName == null)
                                                itemGroupItem.ItemGroupRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                        if (itemGroupItem.ItemGroupRef.FullName == null)
                                            itemGroupItem.ItemGroupRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupLineAddQuantity"))
                            {
                                #region Validations for ItemGroupLineAdd Quantity
                                if (dr["ItemGroupLineAddQuantity"].ToString() != string.Empty)
                                {
                                    string strItemGroupLineAddQuantity = dr["ItemGroupLineAddQuantity"].ToString();
                                    itemGroupItem.Quantity = strItemGroupLineAddQuantity;

                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupLineAddUnitOfMeasure"))
                            {
                                #region Validations of ItemGroupLineAdd UnitOfMeasure
                                if (dr["ItemGroupLineAddUnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddUnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAdd UnitOfMeasure (" + dr["ItemGroupLineAddUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                    }
                                }
                                #endregion
                            }



                            if (dt.Columns.Contains("ItemGroupLineAddInventorySiteFullName"))
                            {
                                #region Validations of ItemGroupLineAdd InventorySite FullName
                                if (dr["ItemGroupLineAddInventorySiteFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddInventorySiteFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAdd InventorySite FullName (" + dr["ItemGroupLineAddInventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                                if (itemGroupItem.InventorySiteRef.FullName == null)
                                                    itemGroupItem.InventorySiteRef.FullName = null;
                                                else
                                                    itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                                if (itemGroupItem.InventorySiteRef.FullName == null)
                                                    itemGroupItem.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                            if (itemGroupItem.InventorySiteRef.FullName == null)
                                                itemGroupItem.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                        if (itemGroupItem.InventorySiteRef.FullName == null)
                                            itemGroupItem.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemGroupLineAddOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemGroupLineAddOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        itemGroupItem.DataExt1 = DataExt.GetInstance(dr["ItemGroupLineAddOwnerID1"].ToString(), null, null);
                            //        if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                            //        {
                            //            itemGroupItem.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemGroupLineAddDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAddDataExtName1 (" + dr["ItemGroupLineAddDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (itemGroupItem.DataExt1 == null)
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString().Substring(0, 31), itemGroupItem.DataExt1.DataExtValue);
                                                if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (itemGroupItem.DataExt1 == null)
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                                if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (itemGroupItem.DataExt1 == null)
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                            if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (itemGroupItem.DataExt1 == null)
                                            itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                        else
                                            itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                        if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                        {
                                            itemGroupItem.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemGroupLineAddDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (itemGroupItem.DataExt1 == null)
                                        itemGroupItem.DataExt1 = DataExt.GetInstance(null, null, dr["ItemGroupLineAddDataExtValue1"].ToString());
                                    else
                                        itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, itemGroupItem.DataExt1.DataExtName, dr["ItemGroupLineAddDataExtValue1"].ToString());
                                    if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                    {
                                        itemGroupItem.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemGroupLineAddOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemGroupLineAddOwnerID2"].ToString() != string.Empty)
                            //    {
                            //        itemGroupItem.DataExt2 = DataExt.GetInstance(dr["ItemGroupLineAddOwnerID2"].ToString(), null, null);
                            //        if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                            //        {
                            //            itemGroupItem.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemGroupLineAddDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupLineAddDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupLineAddDataExtName2 (" + dr["ItemGroupLineAddDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (itemGroupItem.DataExt2 == null)
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString().Substring(0, 31), itemGroupItem.DataExt2.DataExtValue);
                                                if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (itemGroupItem.DataExt2 == null)
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                                else
                                                    itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                                if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                                {
                                                    itemGroupItem.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (itemGroupItem.DataExt2 == null)
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                            if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (itemGroupItem.DataExt2 == null)
                                            itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                        else
                                            itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                        if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                        {
                                            itemGroupItem.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemGroupLineAddDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemGroupLineAddDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (itemGroupItem.DataExt2 == null)
                                        itemGroupItem.DataExt2 = DataExt.GetInstance(null, null, dr["ItemGroupLineAddDataExtValue2"].ToString());
                                    else
                                        itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, itemGroupItem.DataExt2.DataExtName, dr["ItemGroupLineAddDataExtValue2"].ToString());
                                    if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                    {
                                        itemGroupItem.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }
                            //Axis 622
                            if (itemGroupItem.InventorySiteRef != null || itemGroupItem.ItemGroupRef != null || itemGroupItem.Quantity != null || itemGroupItem.UnitOfMeasure != null || (itemGroupItem.DataExt1 != null && itemGroupItem.DataExt1.DataExtName != null) || (itemGroupItem.DataExt1 != null && itemGroupItem.DataExt1.DataExtValue != null))
                                ItemReceipt.ItemGroupLineAdd.Add(itemGroupItem);
                            //Axis 622 end
                            #endregion


                            #endregion
                        }
                    }
                    else
                    {
                        #region without adding ref number

                        ItemReceiptQBEntry ItemReceipt = new ItemReceiptQBEntry();

                        if (dt.Columns.Contains("VendorFullName"))
                        {
                            #region Validations of Vendor FullName
                            if (dr["VendorFullName"].ToString() != string.Empty)
                            {
                                if (dr["VendorFullName"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor FullName (" + dr["VendorFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                            if (ItemReceipt.VendorRef.FullName == null)
                                                ItemReceipt.VendorRef.FullName = null;
                                            else
                                                ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString().Substring(0, 41));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                            if (ItemReceipt.VendorRef.FullName == null)
                                                ItemReceipt.VendorRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                        if (ItemReceipt.VendorRef.FullName == null)
                                            ItemReceipt.VendorRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemReceipt.VendorRef = new VendorRef(dr["VendorFullName"].ToString());
                                    if (ItemReceipt.VendorRef.FullName == null)
                                        ItemReceipt.VendorRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("APAccountFullName"))
                        {
                            #region Validations of APAccount FullName
                            if (dr["APAccountFullName"].ToString() != string.Empty)
                            {
                                if (dr["APAccountFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This APAccount FullName (" + dr["APAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                            if (ItemReceipt.APAccountRef.FullName == null)
                                                ItemReceipt.APAccountRef.FullName = null;
                                            else
                                                ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                            if (ItemReceipt.APAccountRef.FullName == null)
                                                ItemReceipt.APAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                        if (ItemReceipt.APAccountRef.FullName == null)
                                            ItemReceipt.APAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemReceipt.APAccountRef = new APAccountRef(dr["APAccountFullName"].ToString());
                                    if (ItemReceipt.APAccountRef.FullName == null)
                                        ItemReceipt.APAccountRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out ItemReceiptDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemReceipt.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceiptDt = dttest;
                                        ItemReceipt.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    ItemReceiptDt = Convert.ToDateTime(datevalue);
                                    ItemReceipt.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of RefNumber
                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                if (dr["RefNumber"].ToString().Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This RefNumber (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemReceipt.RefNumber = dr["RefNumber"].ToString().Substring(0, 20);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemReceipt.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.RefNumber = dr["RefNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemReceipt.RefNumber = dr["RefNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations of Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo (" + dr["Memo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemReceipt.Memo = dr["Memo"].ToString().Substring(0, 4095);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemReceipt.Memo = dr["Memo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.Memo = dr["Memo"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemReceipt.Memo = dr["Memo"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("IsTaxInclude"))
                        {
                            #region Validations of IsTaxInclude
                            if (dr["IsTaxInclude"].ToString() != "<None>" || dr["IsTaxInclude"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsTaxInclude"].ToString(), out result))
                                {
                                    ItemReceipt.IsTaxIncluded = Convert.ToInt32(dr["IsTaxInclude"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsTaxInclude"].ToString().ToLower() == "true")
                                    {
                                        ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsTaxInclude"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsTaxInclude (" + dr["IsTaxInclude"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemReceipt.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesTaxCodeFullName"))
                        {
                            #region Validations of SalesTaxCode FullName
                            if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTaxCode FullName (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                                ItemReceipt.SalesTaxCodeRef.FullName = null;
                                            else
                                                ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                                ItemReceipt.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                            ItemReceipt.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemReceipt.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                    if (ItemReceipt.SalesTaxCodeRef.FullName == null)
                                        ItemReceipt.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        #region Checking and setting SalesTaxCode

                        if (defaultSettings == null)
                        {
                            CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return null;

                        }
                        //string IsTaxable = string.Empty;
                        string TaxRateValue = string.Empty;
                        string ItemSaleTaxFullName = string.Empty;
                        //if default settings contain checkBoxGrossToNet checked.
                        if (defaultSettings.GrossToNet == "1")
                        {
                            if (dt.Columns.Contains("SalesTaxCodeFullName"))
                            {
                                if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string FullName = dr["SalesTaxCodeFullName"].ToString();
                                    //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                    ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                }
                            }
                        }
                        #endregion

                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemReceipt.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemReceipt.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                        ItemReceipt.ExchangeRate = dr["ExchangeRate"].ToString();
                                }
                                else
                                {
                                    ItemReceipt.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("LinkToTxnID"))
                        {
                            #region Validations for LinkToTxnID
                            if (dr["LinkToTxnID"].ToString() != string.Empty)
                            {
                                string strLinkToTxnID = dr["LinkToTxnID"].ToString();
                                ItemReceipt.LinkToTxnID = strLinkToTxnID;

                            }

                            #endregion
                        }

                        ExpenseLineAdd ExpenseLineAddItem = new ExpenseLineAdd();

                        #region Expense Line Add



                        if (dt.Columns.Contains("ExpenseLineAddAccountFullName"))
                        {
                            #region Validations of ExpenseLineAdd Account FullName
                            if (dr["ExpenseLineAddAccountFullName"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseLineAddAccountFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseLineAddAccount FullName (" + dr["ExpenseLineAddAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                            if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                ExpenseLineAddItem.AccountRef.FullName = null;
                                            else
                                                ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                            if (ExpenseLineAddItem.AccountRef.FullName == null)
                                                ExpenseLineAddItem.AccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                        if (ExpenseLineAddItem.AccountRef.FullName == null)
                                            ExpenseLineAddItem.AccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ExpenseLineAddItem.AccountRef = new AccountRef(dr["ExpenseLineAddAccountFullName"].ToString());
                                    if (ExpenseLineAddItem.AccountRef.FullName == null)
                                        ExpenseLineAddItem.AccountRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ExpenseLineAddAmount"))
                        {
                            #region Validations for ExpenseLineAddAmount
                            if (dr["ExpenseLineAddAmount"].ToString() != string.Empty)
                            {
                                //decimal amount;
                                decimal cost = 0;
                                if (!decimal.TryParse(dr["ExpenseLineAddAmount"].ToString(), out cost))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseLineAddAmount ( " + dr["ExpenseLineAddAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["ExpenseLineAddAmount"].ToString();
                                            ExpenseLineAddItem.Amount = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["ExpenseLineAddAmount"].ToString();
                                            ExpenseLineAddItem.Amount = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["ExpenseLineAddAmount"].ToString();
                                        ExpenseLineAddItem.Amount = strRate;
                                    }
                                }
                                else
                                {

                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                        {
                                            if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                            {
                                                decimal Cost = Convert.ToDecimal(dr["ExpenseLineAddAmount"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    cost = Cost / (1 + (TaxRate / 100));
                                                }

                                                ExpenseLineAddItem.Amount = Convert.ToString(Math.Round(cost, 5));
                                            }
                                        }
                                        //Check if ItemLine.Amount is null
                                        if (ExpenseLineAddItem.Amount == null)
                                        {
                                            ExpenseLineAddItem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseLineAddAmount"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseLineAddAmount"].ToString()));
                                    }

                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ExpenseLineAddMemo"))
                        {
                            #region Validations of Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseLineAdd Memo (" + dr["ExpenseLineAddMemo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString().Substring(0, 4095);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                    }
                                }
                                else
                                {
                                    ExpenseLineAddItem.Memo = dr["ExpenseLineAddMemo"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ExpenseLineAddCustomerFullName"))
                        {
                            #region Validations of ExpenseLineAdd Customer FullName
                            if (dr["ExpenseLineAddCustomerFullName"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseLineAddCustomerFullName"].ToString().Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseLineAddCustomer FullName (" + dr["ExpenseLineAddCustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                            if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                ExpenseLineAddItem.CustomerRef.FullName = null;
                                            else
                                                ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString().Substring(0, 209));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                            if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                                ExpenseLineAddItem.CustomerRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                        if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                            ExpenseLineAddItem.CustomerRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ExpenseLineAddItem.CustomerRef = new CustomerRef(dr["ExpenseLineAddCustomerFullName"].ToString());
                                    if (ExpenseLineAddItem.CustomerRef.FullName == null)
                                        ExpenseLineAddItem.CustomerRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ExpenseLineAddClassFullName"))
                        {
                            #region Validations of ExpenseLineAdd Class FullName
                            if (dr["ExpenseLineAddClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseLineAddClassFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseLineAddClass FullName (" + dr["ExpenseLineAddClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                            if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                ExpenseLineAddItem.ClassRef.FullName = null;
                                            else
                                                ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                            if (ExpenseLineAddItem.ClassRef.FullName == null)
                                                ExpenseLineAddItem.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                        if (ExpenseLineAddItem.ClassRef.FullName == null)
                                            ExpenseLineAddItem.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ExpenseLineAddItem.ClassRef = new ClassRef(dr["ExpenseLineAddClassFullName"].ToString());
                                    if (ExpenseLineAddItem.ClassRef.FullName == null)
                                        ExpenseLineAddItem.ClassRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ExpenseLineAddSalesTaxCodeFullName"))
                        {
                            #region Validations of ExpenseLineAdd SalesTaxCode FullName
                            if (dr["ExpenseLineAddSalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseLineAddSalesTaxCodeFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseLineAddSalesTaxCode FullName (" + dr["ExpenseLineAddSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                            else
                                                ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                                ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                        if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                            ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ExpenseLineAddItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseLineAddSalesTaxCodeFullName"].ToString());
                                    if (ExpenseLineAddItem.SalesTaxCodeRef.FullName == null)
                                        ExpenseLineAddItem.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ExpenseLineAddBillableStatus"))
                        {
                            #region Validations of ExpenseLineAdd BillableStatus
                            if (dr["ExpenseLineAddBillableStatus"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ExpenseLineAddItem.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseLineAddBillableStatus"].ToString(), true));
                                }
                                catch
                                {
                                    ExpenseLineAddItem.BillableStatus = dr["ExpenseLineAddBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Improvement::548
                        if (dt.Columns.Contains("SalesRepRefFullName"))
                        {
                            #region Validations of SalesRepRef Full name
                            if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["SalesRepRefFullName"].ToString();
                                if (strSales.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                            {
                                                ExpenseLineAddItem.SalesRepRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                            {
                                                ExpenseLineAddItem.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                        if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                        {
                                            ExpenseLineAddItem.SalesRepRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    ExpenseLineAddItem.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                    if (ExpenseLineAddItem.SalesRepRef.FullName == null)
                                    {
                                        ExpenseLineAddItem.SalesRepRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("OwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["OwnerID1"].ToString() != string.Empty)
                        //    {
                        //        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                        //        if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                        //        {
                        //            ExpenseLineAddItem.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}

                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ExpenseLineAddItem.DataExt1 == null)
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt1.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ExpenseLineAddItem.DataExt1 == null)
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpenseLineAddItem.DataExt1 == null)
                                            ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                        if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                        {
                                            ExpenseLineAddItem.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ExpenseLineAddItem.DataExt1 == null)
                                        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                    else
                                        ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpenseLineAddItem.DataExt1.DataExtValue);
                                    if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                    {
                                        ExpenseLineAddItem.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue1"].ToString() != string.Empty)
                            {
                                if (ExpenseLineAddItem.DataExt1 == null)
                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                else
                                    ExpenseLineAddItem.DataExt1 = DataExt.GetInstance(ExpenseLineAddItem.DataExt1.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt1.OwnerID, ExpenseLineAddItem.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                if (ExpenseLineAddItem.DataExt1.OwnerID == null && ExpenseLineAddItem.DataExt1.DataExtName == null && ExpenseLineAddItem.DataExt1.DataExtValue == null)
                                {
                                    ExpenseLineAddItem.DataExt1 = null;
                                }

                            }

                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("OwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["OwnerID2"].ToString() != string.Empty)
                        //    {
                        //        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                        //        if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                        //        {
                        //            ExpenseLineAddItem.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ExpenseLineAddItem.DataExt2 == null)
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt2.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ExpenseLineAddItem.DataExt2 == null)
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                            if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                            {
                                                ExpenseLineAddItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpenseLineAddItem.DataExt2 == null)
                                            ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                        if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                        {
                                            ExpenseLineAddItem.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ExpenseLineAddItem.DataExt2 == null)
                                        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                    else
                                        ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpenseLineAddItem.DataExt2.DataExtValue);
                                    if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                    {
                                        ExpenseLineAddItem.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue2"].ToString() != string.Empty)
                            {
                                if (ExpenseLineAddItem.DataExt2 == null)
                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                else
                                    ExpenseLineAddItem.DataExt2 = DataExt.GetInstance(ExpenseLineAddItem.DataExt2.OwnerID == null ? "0" : ExpenseLineAddItem.DataExt2.OwnerID, ExpenseLineAddItem.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                if (ExpenseLineAddItem.DataExt2.OwnerID == null && ExpenseLineAddItem.DataExt2.DataExtName == null && ExpenseLineAddItem.DataExt2.DataExtValue == null)
                                {
                                    ExpenseLineAddItem.DataExt2 = null;
                                }

                            }

                            #endregion

                        }

                        if (ExpenseLineAddItem.AccountRef != null || ExpenseLineAddItem.Amount != null || ExpenseLineAddItem.BillableStatus != null || ExpenseLineAddItem.ClassRef != null || ExpenseLineAddItem.CustomerRef != null || ExpenseLineAddItem.Memo != null || ExpenseLineAddItem.SalesTaxCodeRef != null || ExpenseLineAddItem.SalesRepRef != null)
                            ItemReceipt.ExpenseLineAdd.Add(ExpenseLineAddItem);

                        #endregion


                        ItemLineAdd ItemLineItem = new ItemLineAdd();

                        #region Item Line
                        if (dt.Columns.Contains("ItemLineAddItemFullName"))
                        {
                            #region Validations of ItemLineAdd Item FullName
                            if (dr["ItemLineAddItemFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddItemFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd ItemFullName (" + dr["ItemLineAddItemFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                            if (ItemLineItem.ItemRef.FullName == null)
                                                ItemLineItem.ItemRef.FullName = null;
                                            else
                                                ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                            if (ItemLineItem.ItemRef.FullName == null)
                                                ItemLineItem.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                        if (ItemLineItem.ItemRef.FullName == null)
                                            ItemLineItem.ItemRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.ItemRef = new ItemRef(dr["ItemLineAddItemFullName"].ToString());
                                    if (ItemLineItem.ItemRef.FullName == null)
                                        ItemLineItem.ItemRef.FullName = null;
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("ItemLineAddInventorySiteFullName"))
                        {
                            #region Validations of ItemLineAdd Item FullName
                            if (dr["ItemLineAddInventorySiteFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddInventorySiteFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAddInventorySite FullName (" + dr["ItemLineAddInventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                            if (ItemLineItem.InventorySiteRef.FullName == null)
                                                ItemLineItem.InventorySiteRef.FullName = null;
                                            else
                                                ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                            if (ItemLineItem.InventorySiteRef.FullName == null)
                                                ItemLineItem.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                        if (ItemLineItem.InventorySiteRef.FullName == null)
                                            ItemLineItem.InventorySiteRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.InventorySiteRef = new InventorySiteRef(dr["ItemLineAddInventorySiteFullName"].ToString());
                                    if (ItemLineItem.InventorySiteRef.FullName == null)
                                        ItemLineItem.InventorySiteRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddInventorySiteLocationFullName"))
                        {
                            #region Validations of ItemLineAdd InventorySiteLocation FullName
                            if (dr["ItemLineAddInventorySiteLocationFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddInventorySiteLocationFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd InventorySiteLocation FullName  (" + dr["ItemLineAddInventorySiteLocationFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());
                                    }
                                }
                                else
                                {
                                    ItemLineItem.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemLineAddInventorySiteLocationFullName"].ToString());
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of ItemLineAdd SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SerialNumber"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                            if (ItemLineItem.SerialNumber == null)
                                                ItemLineItem.SerialNumber = null;
                                            else
                                                ItemLineItem.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                            if (ItemLineItem.SerialNumber == null)
                                                ItemLineItem.SerialNumber = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                        if (ItemLineItem.SerialNumber == null)
                                            ItemLineItem.SerialNumber = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.SerialNumber = dr["SerialNumber"].ToString();
                                    if (ItemLineItem.SerialNumber == null)
                                        ItemLineItem.SerialNumber = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LotNumber"))
                        {
                            #region Validations of ItemLineAdd LotNumber
                            if (dr["LotNumber"].ToString() != string.Empty)
                            {
                                if (dr["LotNumber"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                            if (ItemLineItem.LotNumber == null)
                                                ItemLineItem.LotNumber = null;
                                            else
                                                ItemLineItem.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                            if (ItemLineItem.LotNumber == null)
                                                ItemLineItem.LotNumber = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                        if (ItemLineItem.LotNumber == null)
                                            ItemLineItem.LotNumber = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.LotNumber = dr["LotNumber"].ToString();
                                    if (ItemLineItem.LotNumber == null)
                                        ItemLineItem.LotNumber = null;
                                }
                            }
                            #endregion
                        }



                        if (dt.Columns.Contains("ItemLineAddDesc"))
                        {
                            #region Validations of ItemLineAddDesc
                            if (dr["ItemLineAddDesc"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddDesc"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd Desc (" + dr["ItemLineAddDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString().Substring(0, 4095);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemLineItem.Desc = dr["ItemLineAddDesc"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("ItemLineAddQuantity"))
                        {
                            #region Validations for ItemLineAddQuantity
                            if (dr["ItemLineAddQuantity"].ToString() != string.Empty)
                            {
                                string strItemLineAddQuantity = dr["ItemLineAddQuantity"].ToString();
                                ItemLineItem.Quantity = strItemLineAddQuantity;

                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddUnitOfMeasure"))
                        {
                            #region Validations of ItemLineAddUnitOfMeasure
                            if (dr["ItemLineAddUnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddUnitOfMeasure"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd UnitOfMeasure (" + dr["ItemLineAddUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString().Substring(0, 31);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemLineItem.UnitOfMeasure = dr["ItemLineAddUnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddCost"))
                        {
                            #region Validations for ItemLineAddCost
                            if (dr["ItemLineAddCost"].ToString() != string.Empty)
                            {
                                //decimal amount;
                                decimal cost = 0;
                                if (!decimal.TryParse(dr["ItemLineAddCost"].ToString(), out cost))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAddCost ( " + dr["ItemLineAddCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["ItemLineAddCost"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["ItemLineAddCost"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["ItemLineAddCost"].ToString();
                                        ItemLineItem.Cost = strRate;
                                    }
                                }
                                else
                                {

                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                        {
                                            if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                            {
                                                decimal Cost = Convert.ToDecimal(dr["ItemLineAddCost"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    cost = Cost / (1 + (TaxRate / 100));
                                                }

                                                ItemLineItem.Cost = Convert.ToString(Math.Round(cost, 5));
                                            }
                                        }
                                        //Check if ItemLine.Amount is null
                                        if (ItemLineItem.Cost == null)
                                        {
                                            //axis 10.0 change decimal poit from 2 to 5
                                            // ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                            ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        //axis 10.0 change decimal poit from 2 to 5
                                        // ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                        ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddCost"].ToString()));
                                    }

                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddAmount"))
                        {
                            #region Validations for ItemLineAddAmount
                            if (dr["ItemLineAddAmount"].ToString() != string.Empty)
                            {
                                //decimal amount;
                                decimal cost = 0;
                                if (!decimal.TryParse(dr["ItemLineAddAmount"].ToString(), out cost))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAddAmount ( " + dr["ItemLineAddAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["ItemLineAddAmount"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["ItemLineAddAmount"].ToString();
                                            ItemLineItem.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["ItemLineAddAmount"].ToString();
                                        ItemLineItem.Cost = strRate;
                                    }
                                }
                                else
                                {

                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && ItemReceipt.IsTaxIncluded != null && ItemReceipt.IsTaxIncluded != string.Empty)
                                        {
                                            if (ItemReceipt.IsTaxIncluded == "true" || ItemReceipt.IsTaxIncluded == "1")
                                            {
                                                decimal Cost = Convert.ToDecimal(dr["ItemLineAddAmount"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    cost = Cost / (1 + (TaxRate / 100));
                                                }

                                                ItemLineItem.Cost = Convert.ToString(Math.Round(cost, 5));
                                            }
                                        }
                                        //Check if ItemLine.Amount is null
                                        if (ItemLineItem.Cost == null)
                                        {
                                            //axis 10.0 change decimal poit from 2 to 5
                                            // ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                            ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        //axis 10.0 change decimal poit from 2 to 5
                                        //ItemLineItem.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                        ItemLineItem.Cost = string.Format("{0:000000.00000}", Convert.ToDouble(dr["ItemLineAddAmount"].ToString()));
                                    }

                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddCustomerFullName"))
                        {
                            #region Validations of ItemLineAdd Customer FullName
                            if (dr["ItemLineAddCustomerFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddCustomerFullName"].ToString().Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd Customer FullName (" + dr["ItemLineAddCustomerFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                            if (ItemLineItem.CustomerRef.FullName == null)
                                                ItemLineItem.CustomerRef.FullName = null;
                                            else
                                                ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString().Substring(0, 209));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                            if (ItemLineItem.CustomerRef.FullName == null)
                                                ItemLineItem.CustomerRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                        if (ItemLineItem.CustomerRef.FullName == null)
                                            ItemLineItem.CustomerRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.CustomerRef = new CustomerRef(dr["ItemLineAddCustomerFullName"].ToString());
                                    if (ItemLineItem.CustomerRef.FullName == null)
                                        ItemLineItem.CustomerRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddClassFullName"))
                        {
                            #region Validations of ItemLineAdd Class FullName
                            if (dr["ItemLineAddClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddClassFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd Class FullName (" + dr["ItemLineAddClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                            if (ItemLineItem.ClassRef.FullName == null)
                                                ItemLineItem.ClassRef.FullName = null;
                                            else
                                                ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                            if (ItemLineItem.ClassRef.FullName == null)
                                                ItemLineItem.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                        if (ItemLineItem.ClassRef.FullName == null)
                                            ItemLineItem.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.ClassRef = new ClassRef(dr["ItemLineAddClassFullName"].ToString());
                                    if (ItemLineItem.ClassRef.FullName == null)
                                        ItemLineItem.ClassRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddSalesTaxCodeFullName"))
                        {
                            #region Validations of ItemLineAdd SalesTaxCode FullName
                            if (dr["ItemLineAddSalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddSalesTaxCodeFullName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd SalesTaxCode FullName (" + dr["ItemLineAddSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                            if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                ItemLineItem.SalesTaxCodeRef.FullName = null;
                                            else
                                                ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                            if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                                ItemLineItem.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                        if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                            ItemLineItem.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemLineAddSalesTaxCodeFullName"].ToString());
                                    if (ItemLineItem.SalesTaxCodeRef.FullName == null)
                                        ItemLineItem.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddBillableStatus"))
                        {
                            #region Validations of ItemLineAddBillableStatus
                            if (dr["ItemLineAddBillableStatus"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemLineItem.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemLineAddBillableStatus"].ToString(), true));
                                }
                                catch
                                {
                                    ItemLineItem.BillableStatus = dr["ItemLineAddBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddOverrideItemAccountFullName"))
                        {
                            #region Validations of ItemLineAdd OverrideItemAccount FullName
                            if (dr["ItemLineAddOverrideItemAccountFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemLineAddOverrideItemAccountFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemLineAdd OverrideItemAccount FullName (" + dr["ItemLineAddOverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                            if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                ItemLineItem.OverrideItemAccountRef.FullName = null;
                                            else
                                                ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                            if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                                ItemLineItem.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                        if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                            ItemLineItem.OverrideItemAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemLineItem.OverrideItemAccountRef = new OverrideItemAccountRef(dr["ItemLineAddOverrideItemAccountFullName"].ToString());
                                    if (ItemLineItem.OverrideItemAccountRef.FullName == null)
                                        ItemLineItem.OverrideItemAccountRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemLineAddLinkToTxnTxnID"))
                        {
                            #region Validations for ItemLineAdd LinkToTxn TxnID

                            if (dr["ItemLineAddLinkToTxnTxnID"].ToString() != string.Empty)
                            {
                                ItemLineItem.LinkToTxn = new LinkToTxn(dr["ItemLineAddLinkToTxnTxnID"].ToString(), string.Empty);
                            }

                            #endregion
                        }

                        //Improvement::548
                        if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                        {
                            #region Validations of ItemSalesRepRef Full name
                            if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                if (strSales.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLineItem.SalesRepRef.FullName == null)
                                            {
                                                ItemLineItem.SalesRepRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLineItem.SalesRepRef.FullName == null)
                                            {
                                                ItemLineItem.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                        if (ItemLineItem.SalesRepRef.FullName == null)
                                        {
                                            ItemLineItem.SalesRepRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    ItemLineItem.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                    if (ItemLineItem.SalesRepRef.FullName == null)
                                    {
                                        ItemLineItem.SalesRepRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemOwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                        //    {
                        //        ItemLineItem.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                        //        if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                        //        {
                        //            ItemLineItem.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemDataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["ItemDataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ItemLineItem.DataExt1 == null)
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt1.DataExtValue);
                                            if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ItemLineItem.DataExt1 == null)
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                            if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLineItem.DataExt1 == null)
                                            ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                        if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                        {
                                            ItemLineItem.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ItemLineItem.DataExt1 == null)
                                        ItemLineItem.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                    else
                                        ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLineItem.DataExt1.DataExtValue);
                                    if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                    {
                                        ItemLineItem.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                            {
                                if (ItemLineItem.DataExt1 == null)
                                    ItemLineItem.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                else
                                    ItemLineItem.DataExt1 = DataExt.GetInstance(ItemLineItem.DataExt1.OwnerID == null ? "0" : ItemLineItem.DataExt1.OwnerID, ItemLineItem.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                if (ItemLineItem.DataExt1.OwnerID == null && ItemLineItem.DataExt1.DataExtName == null && ItemLineItem.DataExt1.DataExtValue == null)
                                {
                                    ItemLineItem.DataExt1 = null;
                                }

                            }

                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemOwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                        //    {
                        //        ItemLineItem.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                        //        if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                        //        {
                        //            ItemLineItem.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemDataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["ItemDataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ItemLineItem.DataExt2 == null)
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ExpenseLineAddItem.DataExt2.DataExtValue);
                                            if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ItemLineItem.DataExt2 == null)
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                            if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                            {
                                                ItemLineItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLineItem.DataExt2 == null)
                                            ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                        if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                        {
                                            ItemLineItem.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ItemLineItem.DataExt2 == null)
                                        ItemLineItem.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                    else
                                        ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLineItem.DataExt2.DataExtValue);
                                    if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                    {
                                        ItemLineItem.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                            {
                                if (ItemLineItem.DataExt2 == null)
                                    ItemLineItem.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                else
                                    ItemLineItem.DataExt2 = DataExt.GetInstance(ItemLineItem.DataExt2.OwnerID == null ? "0" : ItemLineItem.DataExt2.OwnerID, ItemLineItem.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                if (ItemLineItem.DataExt2.OwnerID == null && ItemLineItem.DataExt2.DataExtName == null && ItemLineItem.DataExt2.DataExtValue == null)
                                {
                                    ItemLineItem.DataExt2 = null;
                                }

                            }

                            #endregion

                        }


                        if (dt.Columns.Contains("ItemLineAddLinkToTxnTxnLineID"))
                        {
                            #region Validations for ItemLineAdd LinkToTxn TxnLineID
                            if (dr["ItemLineAddLinkToTxnTxnLineID"].ToString() != string.Empty)
                            {
                                ItemLineItem.LinkToTxn = new LinkToTxn(string.Empty, dr["ItemLineAddLinkToTxnTxnLineID"].ToString());

                            }

                            #endregion
                        }

                        if (ItemLineItem.Amount != null || ItemLineItem.BillableStatus != null || ItemLineItem.ClassRef != null || ItemLineItem.Cost != null || ItemLineItem.CustomerRef != null || ItemLineItem.Desc != null || ItemLineItem.InventorySiteRef != null || ItemLineItem.ItemRef != null || ItemLineItem.LinkToTxn != null || ItemLineItem.SalesRepRef != null || ItemLineItem.DataExt1.DataExtName != null || ItemLineItem.DataExt1.DataExtValue != null || ItemLineItem.OverrideItemAccountRef != null || ItemLineItem.Quantity != null || ItemLineItem.SalesTaxCodeRef != null || ItemLineItem.SalesRepRef != null || ItemLineItem.DataExt1.DataExtName != null || ItemLineItem.DataExt1.DataExtValue != null || ItemLineItem.UnitOfMeasure != null)
                            ItemReceipt.ItemLineAdd.Add(ItemLineItem);

                        #endregion



                        ItemGroupLineAdd itemGroupItem = new ItemGroupLineAdd();

                        #region Item Group 

                        if (dt.Columns.Contains("ItemGroupLineAddItemGroupFullName"))
                        {
                            #region Validations of ItemGroupLineAdd ItemGroup FullName
                            if (dr["ItemGroupLineAddItemGroupFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemGroupLineAddItemGroupFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemGroupLineAdd ItemGroup FullName (" + dr["ItemGroupLineAddItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                            if (itemGroupItem.ItemGroupRef.FullName == null)
                                                itemGroupItem.ItemGroupRef.FullName = null;
                                            else
                                                itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                            if (itemGroupItem.ItemGroupRef.FullName == null)
                                                itemGroupItem.ItemGroupRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                        if (itemGroupItem.ItemGroupRef.FullName == null)
                                            itemGroupItem.ItemGroupRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    itemGroupItem.ItemGroupRef = new ItemGroupRef(dr["ItemGroupLineAddItemGroupFullName"].ToString());
                                    if (itemGroupItem.ItemGroupRef.FullName == null)
                                        itemGroupItem.ItemGroupRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ItemGroupLineAddQuantity"))
                        {
                            #region Validations for ItemGroupLineAdd Quantity
                            if (dr["ItemGroupLineAddQuantity"].ToString() != string.Empty)
                            {
                                string strItemGroupLineAddQuantity = dr["ItemGroupLineAddQuantity"].ToString();
                                itemGroupItem.Quantity = strItemGroupLineAddQuantity;

                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ItemGroupLineAddUnitOfMeasure"))
                        {
                            #region Validations of ItemGroupLineAdd UnitOfMeasure
                            if (dr["ItemGroupLineAddUnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["ItemGroupLineAddUnitOfMeasure"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemGroupLineAdd UnitOfMeasure (" + dr["ItemGroupLineAddUnitOfMeasure"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString().Substring(0, 31);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    itemGroupItem.UnitOfMeasure = dr["ItemGroupLineAddUnitOfMeasure"].ToString();
                                }
                            }
                            #endregion
                        }



                        if (dt.Columns.Contains("ItemGroupLineAddInventorySiteFullName"))
                        {
                            #region Validations of ItemGroupLineAdd InventorySite FullName
                            if (dr["ItemGroupLineAddInventorySiteFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemGroupLineAddInventorySiteFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemGroupLineAdd InventorySite FullName (" + dr["ItemGroupLineAddInventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                            if (itemGroupItem.InventorySiteRef.FullName == null)
                                                itemGroupItem.InventorySiteRef.FullName = null;
                                            else
                                                itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                            if (itemGroupItem.InventorySiteRef.FullName == null)
                                                itemGroupItem.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                        if (itemGroupItem.InventorySiteRef.FullName == null)
                                            itemGroupItem.InventorySiteRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    itemGroupItem.InventorySiteRef = new InventorySiteRef(dr["ItemGroupLineAddInventorySiteFullName"].ToString());
                                    if (itemGroupItem.InventorySiteRef.FullName == null)
                                        itemGroupItem.InventorySiteRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemGroupLineAddOwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemGroupLineAddOwnerID1"].ToString() != string.Empty)
                        //    {
                        //        itemGroupItem.DataExt1 = DataExt.GetInstance(dr["ItemGroupLineAddOwnerID1"].ToString(), null, null);
                        //        if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                        //        {
                        //            itemGroupItem.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemGroupLineAddDataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemGroupLineAddDataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["ItemGroupLineAddDataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemGroupLineAddDataExtName1 (" + dr["ItemGroupLineAddDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (itemGroupItem.DataExt1 == null)
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString().Substring(0, 31), itemGroupItem.DataExt1.DataExtValue);
                                            if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (itemGroupItem.DataExt1 == null)
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                            if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (itemGroupItem.DataExt1 == null)
                                            itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                        else
                                            itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                        if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                        {
                                            itemGroupItem.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (itemGroupItem.DataExt1 == null)
                                        itemGroupItem.DataExt1 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName1"].ToString(), null);
                                    else
                                        itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, dr["ItemGroupLineAddDataExtName1"].ToString(), itemGroupItem.DataExt1.DataExtValue);
                                    if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                    {
                                        itemGroupItem.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemGroupLineAddDataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemGroupLineAddDataExtValue1"].ToString() != string.Empty)
                            {
                                if (itemGroupItem.DataExt1 == null)
                                    itemGroupItem.DataExt1 = DataExt.GetInstance(null, null, dr["ItemGroupLineAddDataExtValue1"].ToString());
                                else
                                    itemGroupItem.DataExt1 = DataExt.GetInstance(itemGroupItem.DataExt1.OwnerID == null ? "0" : itemGroupItem.DataExt1.OwnerID, itemGroupItem.DataExt1.DataExtName, dr["ItemGroupLineAddDataExtValue1"].ToString());
                                if (itemGroupItem.DataExt1.OwnerID == null && itemGroupItem.DataExt1.DataExtName == null && itemGroupItem.DataExt1.DataExtValue == null)
                                {
                                    itemGroupItem.DataExt1 = null;
                                }

                            }

                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemGroupLineAddOwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemGroupLineAddOwnerID2"].ToString() != string.Empty)
                        //    {
                        //        itemGroupItem.DataExt2 = DataExt.GetInstance(dr["ItemGroupLineAddOwnerID2"].ToString(), null, null);
                        //        if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                        //        {
                        //            itemGroupItem.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemGroupLineAddDataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemGroupLineAddDataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["ItemGroupLineAddDataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemGroupLineAddDataExtName2 (" + dr["ItemGroupLineAddDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (itemGroupItem.DataExt2 == null)
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString().Substring(0, 31), itemGroupItem.DataExt2.DataExtValue);
                                            if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (itemGroupItem.DataExt2 == null)
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                            else
                                                itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                            if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                            {
                                                itemGroupItem.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (itemGroupItem.DataExt2 == null)
                                            itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                        else
                                            itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                        if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                        {
                                            itemGroupItem.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (itemGroupItem.DataExt2 == null)
                                        itemGroupItem.DataExt2 = DataExt.GetInstance(null, dr["ItemGroupLineAddDataExtName2"].ToString(), null);
                                    else
                                        itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, dr["ItemGroupLineAddDataExtName2"].ToString(), itemGroupItem.DataExt2.DataExtValue);
                                    if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                    {
                                        itemGroupItem.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemGroupLineAddDataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemGroupLineAddDataExtValue2"].ToString() != string.Empty)
                            {
                                if (itemGroupItem.DataExt2 == null)
                                    itemGroupItem.DataExt2 = DataExt.GetInstance(null, null, dr["ItemGroupLineAddDataExtValue2"].ToString());
                                else
                                    itemGroupItem.DataExt2 = DataExt.GetInstance(itemGroupItem.DataExt2.OwnerID == null ? "0" : itemGroupItem.DataExt2.OwnerID, itemGroupItem.DataExt2.DataExtName, dr["ItemGroupLineAddDataExtValue2"].ToString());
                                if (itemGroupItem.DataExt2.OwnerID == null && itemGroupItem.DataExt2.DataExtName == null && itemGroupItem.DataExt2.DataExtValue == null)
                                {
                                    itemGroupItem.DataExt2 = null;
                                }

                            }

                            #endregion

                        }
                        //Axis 697
                        //if (itemGroupItem.InventorySiteRef != null || itemGroupItem.ItemGroupRef != null || itemGroupItem.Quantity != null || itemGroupItem.UnitOfMeasure != null || itemGroupItem.DataExt1.DataExtName != null || itemGroupItem.DataExt1.DataExtValue != null)
                        if (itemGroupItem.InventorySiteRef != null || itemGroupItem.ItemGroupRef != null || itemGroupItem.Quantity != null || itemGroupItem.UnitOfMeasure != null || (itemGroupItem.DataExt1 != null && itemGroupItem.DataExt1.DataExtName != null) || (itemGroupItem.DataExt1 != null && itemGroupItem.DataExt1.DataExtValue != null))
                            ItemReceipt.ItemGroupLineAdd.Add(itemGroupItem);
                        #endregion

                        coll.Add(ItemReceipt);

                        #endregion
                    }

                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }
    }
}
