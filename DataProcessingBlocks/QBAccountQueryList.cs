using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using Interop.QBXMLRP2Lib;
using Streams;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Data;
using System.Threading.Tasks;

namespace DataProcessingBlocks
{
    public class QBAccountQueryList
    {
        #region Private Members
        private string m_FullName;
        #endregion

        #region Properties
        public string FullName
        {
            get { return m_FullName; }
            set { m_FullName = value; }
        }
        #endregion
    }

    /// <summary>
    /// Collection class used for Getting AccountQueryList from QuickBook.
    /// </summary>
    public class QBAccountQueryListCollection : Collection<QBAccountQueryList>
    {
        string XmlFileData = string.Empty;

        /// <summary>
        /// This method is used to get IncomeAccountList from QuickBook by passing IncludeRetElement.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateIncomeAccountQuery(string QBFileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = xmlDoc.CreateElement("AccountQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");      
                      
            XmlElement accountType = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountType).InnerText = "Income";

            //IncludeRetElement to get only name of Account type in response from QuickBook.
            XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            queryRequest.AppendChild(includeRetElement).InnerText = "FullName";

            string ticket = string.Empty;
            string responseList = string.Empty;

            if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }
                catch { }
            }
            else 
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, xmlDoc.OuterXml);
                //ticket = CommonUtilities.GetInstance().ticketvalue;
            }
           // responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
           
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForIncomeAccountList = new XmlDocument();
                XmlDocForIncomeAccountList.LoadXml(responseList);
                XmlNodeList IncomeAccountList = XmlDocForIncomeAccountList.GetElementsByTagName("AccountRet");
                foreach (XmlNode IncomeAccountNode in IncomeAccountList)
                {
                    QBAccountQueryList IncomeAccountQueryList = new QBAccountQueryList();
                    foreach (XmlNode node in IncomeAccountNode.ChildNodes)
                    {
                        if (node.Name.Equals("FullName"))
                        {
                            IncomeAccountQueryList.FullName = node.InnerText;
                        }
                    }
                    this.Add(IncomeAccountQueryList);
                }
            }
            return this;
        }


        public async System.Threading.Tasks.Task<Collection<QBAccountQueryList>> PopulateIncomeAccountOnline()
        {
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            QBAccountQueryList accountList = new QBAccountQueryList();
            QueryService<Account> customerQueryService = new QueryService<Account>(serviceContext);
            ReadOnlyCollection<Intuit.Ipp.Data.Account> dataItem;
            List<string> accList = new List<string>();
            bool status = true;
            if (CommonUtilities.GetInstance().ItemType == "Inventory")
            {
                //Bug 471 change  in query
                dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * from Account STARTPOSITION 1 MAXRESULTS 1000"));

               // dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * From Account"));

                foreach (var account in dataItem)
                {
                    if (account.Active == status && account.AccountSubType == Intuit.Ipp.Data.AccountSubTypeEnum.SalesOfProductIncome.ToString())
                    {
                        accountList.FullName = account.FullyQualifiedName.ToString();

                        this.Add(accountList);
                        accountList = new QBAccountQueryList();
                    }
                }

            }
            else 
            {
                //Bug 471 change  in query
                dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * from Account STARTPOSITION 1 MAXRESULTS 1000"));

                //dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * From Account"));

                foreach (var account in dataItem)
                {
                    if (account.Active == status && account.AccountType == Intuit.Ipp.Data.AccountTypeEnum.Income)
                    {
                        accountList.FullName = account.FullyQualifiedName.ToString();

                        this.Add(accountList);
                        accountList = new QBAccountQueryList();
                    }
                }

            }
            return this;
        }

        /// <summary>
        /// This method is used to get COGSAccountList from QuickBook by passing IncludeRetElement.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateCOGSAccountQuery(string QBFileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = xmlDoc.CreateElement("AccountQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement accountType = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountType).InnerText = "CostOfGoodsSold";

            //IncludeRetElement to get only name of Account type in response from QuickBook.
            XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            queryRequest.AppendChild(includeRetElement).InnerText = "FullName";

            string ticket = string.Empty;
            string responseList = string.Empty;

            if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }
                catch { }
            }
            else
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, xmlDoc.OuterXml);
               // ticket = CommonUtilities.GetInstance().ticketvalue;
            }
         //   responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForCOGSAccountList = new XmlDocument();
                XmlDocForCOGSAccountList.LoadXml(responseList);
                XmlNodeList COGSAccountList = XmlDocForCOGSAccountList.GetElementsByTagName("AccountRet");
                foreach (XmlNode COGSAccountNode in COGSAccountList)
                {
                    QBAccountQueryList COGSAccountQueryList = new QBAccountQueryList();
                    foreach (XmlNode node in COGSAccountNode.ChildNodes)
                    {
                        if (node.Name.Equals("FullName"))
                        {
                            COGSAccountQueryList.FullName = node.InnerText;
                        }
                    }
                    this.Add(COGSAccountQueryList);
                }
            }
            return this;
        }


        public async System.Threading.Tasks.Task<Collection<QBAccountQueryList>> PopulateCOGSAccountOnline()
        {
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            QBAccountQueryList accountList = new QBAccountQueryList();
            QueryService<Account> customerQueryService = new QueryService<Account>(serviceContext);
            ReadOnlyCollection<Intuit.Ipp.Data.Account> dataItem;
            List<string> accList = new List<string>();
            bool status = true;

            //Bug 471 change  in query
            dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * from Account STARTPOSITION 1 MAXRESULTS 1000"));


               // dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * From Account"));

                foreach (var account in dataItem)
                {
                    if (account.Active == status && account.AccountType == Intuit.Ipp.Data.AccountTypeEnum.CostofGoodsSold)
                    {
                        accountList.FullName = account.FullyQualifiedName.ToString();

                        this.Add(accountList);
                        accountList = new QBAccountQueryList();
                    }
                }

            
            return this;
        }
        /// <summary>
        /// Axis 10.0 changes
        /// This method is used to get ExpenseAccountList from QuickBook by passing IncludeRetElement.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateExpenseAccountQuery(string QBFileName)
        {
            List<string> ExpenseList = new List<string>();

            XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = xmlDoc.CreateElement("AccountQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            XmlElement accountExpense = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountExpense).InnerText = "Expense";

            XmlElement accountIncome = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountIncome).InnerText = "Income";

            //656
            XmlElement accountCOGS = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountCOGS).InnerText = "CostOfGoodsSold";

            XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            queryRequest.AppendChild(includeRetElement).InnerText = "FullName";

            
            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = ticket;
                }
                catch { }
            }
            else
            {
                ticket = CommonUtilities.GetInstance().ticketvalue;
            }
            responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, xmlDoc.OuterXml);
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForExpenseAccountList = new XmlDocument();
                XmlDocForExpenseAccountList.LoadXml(responseList);
                XmlNodeList ExpenseAccountList = XmlDocForExpenseAccountList.GetElementsByTagName("AccountRet");
                foreach (XmlNode ExpenseAccountNode in ExpenseAccountList)
                {
                    QBAccountQueryList ExpenseAccountQueryList = new QBAccountQueryList();
                    foreach (XmlNode node in ExpenseAccountNode.ChildNodes)
                    {
                        if (node.Name.Equals("FullName"))
                        {
                            ExpenseAccountQueryList.FullName = node.InnerText;
                            ExpenseList.Add(node.InnerText.ToString());
                        }
                    }
                    this.Add(ExpenseAccountQueryList);
                }
                CommonUtilities.GetInstance().ExpenseAccountList = ExpenseList;
            }
            return this;
        }


        public async System.Threading.Tasks.Task<Collection<QBAccountQueryList>> PopulateExpenseAccountOnline()
        {
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            QBAccountQueryList accountList = new QBAccountQueryList();
            QueryService<Account> customerQueryService = new QueryService<Account>(serviceContext);
            ReadOnlyCollection<Intuit.Ipp.Data.Account> dataItem;
            List<string> accList = new List<string>();
            bool status = true;

            //Bug 471 change  in query
            dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * from Account STARTPOSITION 1 MAXRESULTS 1000"));
           // dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * From Account"));

            foreach (var account in dataItem)
            {
                if (account.Active == status && account.AccountType == Intuit.Ipp.Data.AccountTypeEnum.Expense)
                {
                    accountList.FullName = account.FullyQualifiedName.ToString();

                    this.Add(accountList);
                    accountList = new QBAccountQueryList();
                }
            }


            return this;
        }


        // Axis 10.0 changes ends

        /// <summary>
        /// This method is used to get AssetAccountList from QuickBook by passing IncludeRetElement.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateAssetAccountQuery(string QBFileName)
        {
            XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = xmlDoc.CreateElement("AccountQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            //AccountType is optional, may repeat(create same node with different values).
            XmlElement accountFixedType = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountFixedType).InnerText = "FixedAsset";
            
            XmlElement accountOtherType = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountOtherType).InnerText = "OtherAsset";
            
            XmlElement accountOtherCurrentType = xmlDoc.CreateElement("AccountType");
            queryRequest.AppendChild(accountOtherCurrentType).InnerText = "OtherCurrentAsset";

            //IncludeRetElement to get only name of Account type in response from QuickBook.
            XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            queryRequest.AppendChild(includeRetElement).InnerText = "FullName";

            string ticket = string.Empty;
            string responseList = string.Empty;

            if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }
                catch { }
            }
            else
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, xmlDoc.OuterXml);
                //ticket = CommonUtilities.GetInstance().ticketvalue;
            }
            //responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForCOGSAccountList = new XmlDocument();
                XmlDocForCOGSAccountList.LoadXml(responseList);
                XmlNodeList COGSAccountList = XmlDocForCOGSAccountList.GetElementsByTagName("AccountRet");
                foreach (XmlNode COGSAccountNode in COGSAccountList)
                {
                    QBAccountQueryList COGSAccountQueryList = new QBAccountQueryList();
                    foreach (XmlNode node in COGSAccountNode.ChildNodes)
                    {
                        if (node.Name.Equals("FullName"))
                        {
                            COGSAccountQueryList.FullName = node.InnerText;
                        }
                    }
                    this.Add(COGSAccountQueryList);
                }
            }
            return this;              
        }


        public async System.Threading.Tasks.Task<Collection<QBAccountQueryList>> PopulateAssetAccountOnline()
        {
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            QBAccountQueryList accountList = new QBAccountQueryList();
            QueryService<Account> customerQueryService = new QueryService<Account>(serviceContext);
            ReadOnlyCollection<Intuit.Ipp.Data.Account> dataItem;
            List<string> accList = new List<string>();
            bool status = true;
             //Bug 471 change  in query
            dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * from Account STARTPOSITION 1 MAXRESULTS 1000"));
           // dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(customerQueryService.ExecuteIdsQuery("Select * From Account"));
             foreach (var account in dataItem)
            {
                if (account.Active == status && account.AccountType == Intuit.Ipp.Data.AccountTypeEnum.OtherCurrentAsset)
                {
                    accountList.FullName = account.FullyQualifiedName.ToString();

                    this.Add(accountList);
                    accountList = new QBAccountQueryList();
                }
            }

            return this;
        }

        /// <summary>
        /// Get list of item  by request type and ret.
        /// </summary>
        /// <param name="QbFileName"></param>
        /// <param name="queryRequest"></param>
        /// <param name="queryRet"></param>
        private void GetItemType(string QBFileName, string currentqueryRequest, string queryRet)
        {
         XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = xmlDoc.CreateElement(currentqueryRequest);
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            //IncludeRetElement to get only name of service items from QuickBook.
            XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            queryRequest.AppendChild(includeRetElement).InnerText = "FullName";

            string ticket = string.Empty;
            string responseList = string.Empty;

            if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }
                catch { }
            }
            else
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, xmlDoc.OuterXml);
               // ticket = CommonUtilities.GetInstance().ticketvalue;               
            }
           // responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForServiceItemList = new XmlDocument();
                XmlDocForServiceItemList.LoadXml(responseList);
                XmlNodeList serviceItemList = XmlDocForServiceItemList.GetElementsByTagName(queryRet);
                foreach (XmlNode serviceItemNode in serviceItemList)
                {
                    QBAccountQueryList serviceItemQueryList = new QBAccountQueryList();
                    foreach (XmlNode node in serviceItemNode.ChildNodes)
                    {
                        if (node.Name.Equals("FullName"))
                        {
                            serviceItemQueryList.FullName = node.InnerText;
                        }
                    }
                    this.Add(serviceItemQueryList);
                }
            }
        }

        /// <summary>
        /// This method is used to get Service item list from QuickBook.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateServiceAndOtherChargeItemType(string QBFileName)
        {   
            //Add none value to 0th position.
            QBAccountQueryList serviceItemQueryList = new QBAccountQueryList();
            serviceItemQueryList.FullName = "<None>";
            this.Add(serviceItemQueryList);
             
            #region Get Service Item

            GetItemType(QBFileName, "ItemServiceQueryRq", "ItemServiceRet");

            #endregion

            #region Get Other Charge item

            GetItemType(QBFileName, "ItemOtherChargeQueryRq", "ItemOtherChargeRet");

            #endregion

            return this;              
        }


        public async System.Threading.Tasks.Task<Collection<QBAccountQueryList>> PopulateFrieghtOnline()
        {
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            QBAccountQueryList accountList = new QBAccountQueryList();
            QueryService<Item> customerQueryService = new QueryService<Item>(serviceContext);
            ReadOnlyCollection<Intuit.Ipp.Data.Item> dataItem;
            List<string> accList = new List<string>();
            bool status = true;


            dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.Item>(customerQueryService.ExecuteIdsQuery("Select * From Item"));

            foreach (var account in dataItem)
            {
                if (account.Active == status && account.Type == Intuit.Ipp.Data.ItemTypeEnum.NonInventory)
                {
                    accountList.FullName = account.Name.ToString();

                    this.Add(accountList);
                    accountList = new QBAccountQueryList();
                }
            }

            return this;
        }

   

        /// <summary>
        /// This method is used to get discount item list from QuickBook.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateDiscountItemType(string QBFileName)
        {
            //Add none value to 0th position.
            QBAccountQueryList serviceItemQueryList = new QBAccountQueryList();
            serviceItemQueryList.FullName = "<None>";
            this.Add(serviceItemQueryList);

            GetItemType(QBFileName, "ItemDiscountQueryRq", "ItemDiscountRet");
            return this;
        }

        //following method is aded for Sales Tax filed in shopping cart-bug no. 410
        /// <summary>
        /// This method is used to get SalesTax item list from QuickBook.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBAccountQueryList> PopulateSalesTaxItemType(string QBFileName)
        {
            ////Add none value to 0th position.
            //QBAccountQueryList serviceItemQueryList = new QBAccountQueryList();
            //serviceItemQueryList.FullName = "<None>";
            //this.Add(serviceItemQueryList);

            //GetItemType(QBFileName, "ItemSalesTaxQueryRq", "ItemSalesTaxQueryRet");
            //return this;

            XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = xmlDoc.CreateElement("ItemSalesTaxQueryRq");
            qbXMLMsgsRq.AppendChild(queryRequest);
            queryRequest.SetAttribute("requestID", "1");

            ////AccountType is optional, may repeat(create same node with different values).
            //XmlElement accountFixedType = xmlDoc.CreateElement("AccountType");
            //queryRequest.AppendChild(accountFixedType).InnerText = "FixedAsset";

            //XmlElement accountOtherType = xmlDoc.CreateElement("AccountType");
            //queryRequest.AppendChild(accountOtherType).InnerText = "OtherAsset";

            //XmlElement accountOtherCurrentType = xmlDoc.CreateElement("AccountType");
            //queryRequest.AppendChild(accountOtherCurrentType).InnerText = "OtherCurrentAsset";

            ////IncludeRetElement to get only name of Account type in response from QuickBook.
            //XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            //queryRequest.AppendChild(includeRetElement).InnerText = "FullName";

            string ticket = string.Empty;
            string responseList = string.Empty;

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }
                catch { }
            }
            else
            {
                responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, xmlDoc.OuterXml);
                //ticket = CommonUtilities.GetInstance().ticketvalue;
            }
            //responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForSalesTaxList = new XmlDocument();
                XmlDocForSalesTaxList.LoadXml(responseList);
                XmlNodeList SalesTaxList = XmlDocForSalesTaxList.GetElementsByTagName("ItemSalesTaxRet");
                foreach (XmlNode SalesTaxNode in SalesTaxList)
                {
                    QBAccountQueryList COGSAccountQueryList = new QBAccountQueryList();
                    foreach (XmlNode node in SalesTaxNode.ChildNodes)
                    {
                        if (node.Name.Equals("Name"))
                        {
                            COGSAccountQueryList.FullName = node.InnerText;
                        }
                    }
                    this.Add(COGSAccountQueryList);
                }
            }
            return this;
        }


        /// <summary>
        /// Return all account from quickbook
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public List<string> GetAllAccountList(string QBFileName)
        {
            string accountXmlList = QBCommonUtilities.GetListFromQuickBook("AccountQueryRq", QBFileName);

            List<string> accountList = new List<string>();

            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(accountXmlList);
            XmlFileData = accountXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)System.Windows.Forms.Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList AccountNodeList = outputXMLDoc.GetElementsByTagName(QBXmlAccountList.AccountRet.ToString());

            foreach (XmlNode accountNodes in AccountNodeList)
            {
                if (bkWorker.CancellationPending != true)
                {
                    QBAccountQueryList account = new QBAccountQueryList();
                    foreach (XmlNode node in accountNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlAccountList.FullName.ToString()))
                        {
                            if (!accountList.Contains(node.InnerText))
                                accountList.Add(node.InnerText);
                        }
                    }
                }
            }

            return accountList;

        }

       

        public System.Data.DataTable GetAllAccountNameType(string QBFileName)
        {
            string accountXmlList = QBCommonUtilities.GetListFromQuickBook("AccountQueryRq", QBFileName);


            XmlDocument outputXMLDoc = new XmlDocument();

            outputXMLDoc.LoadXml(accountXmlList);
            XmlFileData = accountXmlList;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;

            XmlNodeList AccountNodeList = outputXMLDoc.GetElementsByTagName(QBXmlAccountList.AccountRet.ToString());

            System.Data.DataTable dtAcc = new System.Data.DataTable();
            System.Data.DataColumn dcAccountName = new System.Data.DataColumn("FullName");
            System.Data.DataColumn dcAccountType = new System.Data.DataColumn("AccountType");
            dtAcc.Columns.Add(dcAccountName);
            dtAcc.Columns.Add(dcAccountType);
            object fullName = null;

            foreach (XmlNode accountNodes in AccountNodeList)
            {
                fullName = null;
                if (bkWorker.CancellationPending != true)
                {
                    QBAccountQueryList account = new QBAccountQueryList();
                    object[] objAccount = new object[2];
                    foreach (XmlNode node in accountNodes.ChildNodes)
                    {
                        if (node.Name.Equals(QBXmlAccountList.FullName.ToString()))
                        {
                            //objAccount[0] = (object)node.InnerText;
                            fullName = (object)node.InnerText;
                        }
                        if (node.Name.Equals(QBXmlAccountList.AccountType.ToString()))
                        {
                            if (node.InnerText.Equals("Bank") || node.InnerText.Equals("CreditCard"))
                            {
                                objAccount[0] = fullName;
                                objAccount[1] = (object)node.InnerText;
                                dtAcc.LoadDataRow(objAccount, true);
                            }
                        }
                    }
                    //dtAcc.LoadDataRow(objAccount, true);
                }
            }

            return dtAcc;
        }
    }
}
