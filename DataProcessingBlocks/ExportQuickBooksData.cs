﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using DataProcessingBlocks;
using Streams;
using System.IO;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace DataProcessingBlocks
{
    public class ExportQuickBooksData
    {
        #region Private Members

        private string m_filePath;
        private string m_exportMappingfilePath;
        private DataTable m_exportData;
        private static ExportQuickBooksData m_ExportQuickBooksData;
        private string m_fileDataContent;
        private string m_FileExportMappingXmlData;
        private string m_moduleName;

        #endregion

        #region Constructor

        public ExportQuickBooksData()
        {
 
        }

        #endregion

        #region Properties
        /// <summary>
        /// This property is used for getting and setting dataTable from collection class
        /// </summary>
        public DataTable ExportDataTable
        {
            get { return m_exportData; }
            set { m_exportData = value; }
        }

        /// <summary>
        /// Get or Set file path name.
        /// </summary>
        public string ExportFilePath
        {
            get { return m_filePath; }
            set { m_filePath = value; }
        }

        /// <summary>
        /// Get or Set Export Mapping file path name.
        /// </summary>
        public string ExportMappingFilePath
        {
            get { return m_exportMappingfilePath; }
            set { m_exportMappingfilePath = value; }
        }

        /// <summary>
        /// Get or set the filedata for xml.
        /// </summary>
        public string FileXmlData
        {
            get { return m_fileDataContent; }
            set { m_fileDataContent = value; }
        }

        /// <summary>
        /// Get or set the filedata for xml.
        /// </summary>
        public string FileExportMappingXmlData
        {
            get { return m_FileExportMappingXmlData; }
            set { m_FileExportMappingXmlData = value; }
        }

        public string module
        {
            get { return m_moduleName; }
            set { m_moduleName = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// create class instance
        /// </summary>
        /// <returns></returns>
        public static ExportQuickBooksData GetInstance()
        {
            if (m_ExportQuickBooksData == null)
                m_ExportQuickBooksData = new ExportQuickBooksData();
            return m_ExportQuickBooksData;
        }
        /// <summary>
        ///  export mapping file
        /// </summary>
        public void ExportMappingFile()
        {
            if (string.IsNullOrEmpty(ExportQuickBooksData.GetInstance().ExportMappingFilePath))
            {
                MessageBox.Show("Please select file to export data", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;

                string extension = string.Empty;

                string XmlData = string.Empty;
                XmlData = FileExportMappingXmlData;

                extension = Path.GetExtension(m_exportMappingfilePath);

                if (extension == ".xml")
                {
                    //Exporting Data to Xml file(.xml).
                    ExportXmlFile(XmlData, m_exportMappingfilePath);
                    
                }
            }
        }
        /// <summary>
        /// export data into corresponding file 
        /// </summary>
        public void Export()
        {
            try
            {
                if (string.IsNullOrEmpty(ExportFilePath))
                {
                    MessageBox.Show("Please select file to export data", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;
                string extension = string.Empty;
                string XmlData = string.Empty;
                ExcellUtility excel = new ExcellUtility();
                DataSet ds = null;
                ds = new DataSet();
                DataTable dt=new DataTable();
                dt = ExportDataTable;
                XmlData = FileXmlData;
                //Converting an dataTable to dataset.
                try
                {
                    ds.Tables.Add(dt);
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message.ToString());
                    CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                }
                //Getting an Extension of File Path.
                extension = Path.GetExtension(m_filePath);

                if (extension == ".xls")
                {
                    //Exporting Data to Excel Sheet(.xls).
                   
                    if (DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName != null)
                    {
                        excel.ExportToMultipleSheetsExcel(ds, m_filePath);
                    }
                    else
                    {
                        excel.ExportToExcel(ds, m_filePath);
                    }
                }
                else if (extension == ".txt")
                {
                    //Exporting Data to Text File.
                    ExportTextFile(dt, m_filePath);
                }
                else if (extension == ".xlsx")
                {
                    //Exporting Data to Excel Sheet(.xlsx).
                    if (DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName != null)
                    {
                        excel.ExportToMultipleSheetsExcel(ds, m_filePath);
                    }
                    else
                    {
                        excel.ExportToExcel2007(ds, m_filePath);
                    }
                }
                else if (extension == ".xml")
                {
                    //Exporting Data to Xml file(.xml).
                    ExportXmlFile(XmlData, m_filePath);
                }
                else if (extension == ".csv")
                {
                    //Exporting Data to Xml file(.xml).
                    if(module == "Import")
                    {
                        excel.ExportDataToCsvFile(dt, m_filePath);
                    }
                    if (module == "Export")
                    {
                        excel.ExportToCsvFile(dt, m_filePath);
                    }
                }
                Cursor.Current = Cursors.Default;
               // MessageBox.Show("Data Successfully Exported.", "Zed Axis", MessageBoxButtons.OK);
                ds.Tables.Remove(dt);
                ds = null;
            }

            catch (COMException ex)
            {                
                switch (ex.ErrorCode.ToString())
                {
                    //for office not installed.
                    case EDI.Constant.Constants.officeErrorcode:
                        {
                            if (Convert.ToString(ex.ErrorCode) == EDI.Constant.Constants.officeErrorcode)
                                MessageBox.Show("Microsoft Office is not Installed on your System.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }
                    //for office requires installation.
                    case EDI.Constant.Constants.officeErrorcode1:
                        {
                            if (Convert.ToString(ex.ErrorCode) == EDI.Constant.Constants.officeErrorcode1)
                                MessageBox.Show("Microsoft Office requires Activation.", "Zed Axis",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                            break;
                        }
             
                    default:
                        MessageBox.Show("Unable to Write Excel File" + "<<" + ex.Message + ">>");
                        break;
                }            
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return;
            }
        }

        /// <summary>
        /// This method is used for Exporting data to Tab Delimited Text File.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="FilePath"></param>
        private void ExportTextFile(DataTable dt, string FilePath)
        {
            int i = 0;
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(FilePath, false);
                //Writng the columns Name.
                for (i = 0; i < dt.Columns.Count - 1; i++)
                {
                    sw.Write(dt.Columns[i].ColumnName + "\t");
                }
                sw.Write(dt.Columns[i].ColumnName);
                sw.WriteLine();

                //Writing the Data. 
                foreach (DataRow row in dt.Rows)
                {
                    //Creating an Object Array.
                    object[] array = row.ItemArray;

                    for (i = 0; i < array.Length - 1; i++)
                    {
                        sw.Write(array[i].ToString() + "\t");
                    }
                    sw.Write(array[i].ToString());
                    sw.WriteLine();
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Invalid Operation : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// This method is used to export the data as xml file.
        /// </summary>
        /// <param name="FileData"></param>
        /// <param name="FilePath"></param>
        private void ExportXmlFile(string FileData, string FilePath)
        {
             StreamWriter sw = null;
             try
             {
                 sw = new StreamWriter(FilePath, false);
                 sw.Write(FileData);
                 sw.Close();
             }
             catch (UnauthorizedAccessException ex)
             {
                 MessageBox.Show("Unauthorized Access : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 CommonUtilities.WriteErrorLog(ex.Message.ToString());
                 CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
             }
             catch (System.Security.SecurityException ex)
             {
                 MessageBox.Show("Security Error : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 CommonUtilities.WriteErrorLog(ex.Message.ToString());
                 CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
             }
             catch (Exception ex)
             {
                 MessageBox.Show("Invalid Operation : \n" + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                 CommonUtilities.WriteErrorLog(ex.Message.ToString());
                 CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
             }

        }

        /// <summary>
        /// Axis 11.0 -131 Exports errors from import summary to an xls file
        /// </summary>
        public void ExportImportErrors(DataSet ds, string matchColumn)
        {
            try
            {
                if (matchColumn != null)
                {
                    ExcellUtility excel = new ExcellUtility();
                    excel.exportErrors(ds, m_filePath, matchColumn);
                }
                return;
            }
            catch (COMException ex)
            {
                switch (ex.ErrorCode.ToString())
                {
                    //for office not installed.
                    case EDI.Constant.Constants.officeErrorcode:
                        {
                            if (Convert.ToString(ex.ErrorCode) == EDI.Constant.Constants.officeErrorcode)
                                MessageBox.Show("Microsoft Office is not Installed on your System.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }
                    //for office requires installation.
                    case EDI.Constant.Constants.officeErrorcode1:
                        {
                            if (Convert.ToString(ex.ErrorCode) == EDI.Constant.Constants.officeErrorcode1)
                                MessageBox.Show("Microsoft Office requires Activation.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }

                    default:
                        MessageBox.Show("Unable to Write Excel File" + "<<" + ex.Message + ">>");
                        break;
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return;
            }
        }
        #endregion
    }
}

  
