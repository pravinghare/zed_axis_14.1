using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("ItemSubtotalQBEntry", Namespace = "", IsNullable = false)]
    public class ItemSubtotalQBEntry
    {
        #region Private Member Variable

        private string m_Name;
        private string m_IsActive;
        private string m_ItemDesc;
        
        #endregion

        #region Constructor
        public ItemSubtotalQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string ItemDesc
        {
            get { return m_ItemDesc; }
            set { m_ItemDesc = value; }
        }


        

        #endregion

        #region Public Methods
        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemSubtotalQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemSubtotalAddRq = requestXmlDoc.CreateElement("ItemSubtotalAddRq");
            inner.AppendChild(ItemSubtotalAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement ItemSubtotalAdd = requestXmlDoc.CreateElement("ItemSubtotalAdd");

            ItemSubtotalAddRq.AppendChild(ItemSubtotalAdd);

            
            ItemSubtotalAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;



            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemSubtotalAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemSubtotalAddRs/ItemSubtotalRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemSubtotal(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating ItemSubtotalQBEntry information
        /// of existing ItemSubtotalQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateItemSubtotalInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemSubtotalQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemSubtotalQBEntryModRq = requestXmlDoc.CreateElement("ItemSubtotalModRq");
            inner.AppendChild(ItemSubtotalQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement ItemSubtotalMod = requestXmlDoc.CreateElement("ItemSubtotalMod");
            ItemSubtotalQBEntryModRq.AppendChild(ItemSubtotalMod);


            
            ItemSubtotalMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemSubtotalModRq/ItemSubtotalMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemSubtotalModRq/ItemSubtotalMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemSubtotalModRq/ItemSubtotalMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemSubtotalModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemSubtotalModRs/ItemSubtotalRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemSubtotal(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    
    public class ItemSubtotalQBEntryCollection : Collection<ItemSubtotalQBEntry>
    {

    }

}
