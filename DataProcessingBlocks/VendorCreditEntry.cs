// ==============================================================================================
// 
// VendorCreditQBEntry.cs
//
// This file contains the implementations of the Vendor Credit Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Vendor Credit QB Entry Imports.
//         Vendor Credit Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping fields (IsTaxIncluded,SalesTaxCodeRef and ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using DataProcessingBlocks;
using EDI.Constant;

namespace DataProcessingBlocks.BillModule
{
    [XmlRootAttribute("VendorCreditEntry", Namespace = "", IsNullable = false)]
    public class VendorCreditEntry
    {
        #region  Private Member Variable

        private VendorRef m_VendorRef;
        private APAccountRef m_APAccountRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private string m_Memo;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_ExchangeRate;

        private Collection<ExpenseLineAdd> m_ExpenseLineAdd = new Collection<ExpenseLineAdd>();
        private Collection<ItemLineAdd> m_ItemLineAdd = new Collection<ItemLineAdd>();

        private DateTime m_VendorCreditDate;


        #endregion

        #region Constructors

        public VendorCreditEntry()
        {

        }

        #endregion

        #region Public Properties

        public VendorRef VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        public APAccountRef APAccountRef
        {
            get { return this.m_APAccountRef; }
            set { this.m_APAccountRef = value; }

        }


        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }


        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }


        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime VendorCreditDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_VendorCreditDate) <= DateTime.MinValue)
                        return this.m_VendorCreditDate;
                    else

                        return this.m_VendorCreditDate;
                }
                catch
                {
                    return this.m_VendorCreditDate;
                }
            }
            set { this.m_VendorCreditDate = value; }
        }

        [XmlArray("ExpenseLineAddREM")]
        public Collection<ExpenseLineAdd> ExpenseLineAdd
        {
            get { return m_ExpenseLineAdd; }
            set { m_ExpenseLineAdd = value; }
        }

        [XmlArray("ItemLineAddREM")]
        public Collection<ItemLineAdd> ItemLineAdd
        {
            get { return m_ItemLineAdd; }
            set { m_ItemLineAdd = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillModule.VendorCreditEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
          
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create vendorcreditAddRq aggregate and fill in field values for it
            System.Xml.XmlElement VendorCreditAddRq = requestXmlDoc.CreateElement("VendorCreditAddRq");
            inner.AppendChild(VendorCreditAddRq);

            //Create vendorcreditentryAdd aggregate and fill in field values for it
            System.Xml.XmlElement VendorCreditAdd = requestXmlDoc.CreateElement("VendorCreditAdd");
            

            VendorCreditAddRq.AppendChild(VendorCreditAdd);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
		    requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            VendorCreditAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorCreditAddRq/VendorCreditAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                VendorCreditAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                VendorCreditAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorCreditAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorCreditAddRs/VendorCreditRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofVendorCredit(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Vendor Credit ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Vendor Credit Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create VendorCreditQueryRq aggregate and fill in field values for it
            XmlElement VendorCreditQueryRq = requestXmlDoc.CreateElement("VendorCreditQueryRq");
            inner.AppendChild(VendorCreditQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            VendorCreditQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            VendorCreditQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating VendorCredit information
        /// of existing VendorCredit with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateVendorCreditInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillModule.VendorCreditEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement VendorCreditQBEntryModRq = requestXmlDoc.CreateElement("VendorCreditModRq");
            inner.AppendChild(VendorCreditQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement VendorCreditMod = requestXmlDoc.CreateElement("VendorCreditMod");
            VendorCreditQBEntryModRq.AppendChild(VendorCreditMod);

            requestXML = requestXML.Replace("<VendorCreditPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorCreditPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);
              

            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorCreditLineAdd>","<VendorCreditLineMod>");
            requestXML = requestXML.Replace("</VendorCreditLineAdd>","</VendorCreditLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
		    requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            VendorCreditMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorCreditAddRq/VendorCreditAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                VendorCreditQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By VendorCreditRefNumber) : " + rowcount.ToString());
            else
                VendorCreditQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorCreditModRq/VendorCreditMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
          
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorCreditModRq/VendorCreditMod").InsertBefore(ListID, firstChild).InnerText = listID;
          
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
         
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorCreditModRq/VendorCreditMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket); 
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorCreditModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorCreditModRs/VendorCreditRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofVendorCredit(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
//Bug No.412
        /// <summary>
        /// append data to corresponding listid
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendVendorCreditInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillModule.VendorCreditEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement VendorCreditQBEntryModRq = requestXmlDoc.CreateElement("VendorCreditModRq");
            inner.AppendChild(VendorCreditQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement VendorCreditMod = requestXmlDoc.CreateElement("VendorCreditMod");
            VendorCreditQBEntryModRq.AppendChild(VendorCreditMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
           
            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.

            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></VendorCreditLineMod><VendorCreditLineMod>";
            }

            for (int i = 0; i < request.Length; i++)
            {
                if (Listcnt != 0)
                {                  
                    subResultString = request[i].Replace("<ItemRef>", addString + "<TxnLineID>-1</TxnLineID><ItemRef>");
                    subStringCnt++;
                }
                else
                {
                    subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                }

                if (stringCnt == request.Length)
                {
                    resultString += subResultString;
                }
                else
                {
                    resultString += subResultString + "</ItemRef>";
                }
                stringCnt++;
                Listcnt--;
            }
            requestXML = resultString;


            requestXML = requestXML.Replace("<VendorCreditPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorCreditPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);


            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorCreditLineAdd>", "<VendorCreditLineMod>");
            requestXML = requestXML.Replace("</VendorCreditLineAdd>", "</VendorCreditLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");         
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
			requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            VendorCreditMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorCreditAddRq/VendorCreditAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                VendorCreditQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By VendorCreditRefNumber) : " + rowcount.ToString());
            else
                VendorCreditQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorCreditModRq/VendorCreditMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
         
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorCreditModRq/VendorCreditMod").InsertBefore(ListID, firstChild).InnerText = listID;
           
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
          
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorCreditModRq/VendorCreditMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorCreditModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorCreditModRs/VendorCreditRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofVendorCredit(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
    }

    public class VendorCreditEntryCollection : Collection<VendorCreditEntry>
    {
        /// <summary>
        /// check date present or not
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public VendorCreditEntry FindVendorCreditEntry(DateTime date)
        {
            foreach (VendorCreditEntry item in this)
            {
                if (item.VendorCreditDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
       /// <summary>
        /// check ref no present or not
       /// </summary>
       /// <param name="refNumber"></param>
       /// <returns></returns>
        public VendorCreditEntry FindVendorCreditEntry(string refNumber)
        {
            foreach (VendorCreditEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
        /// <summary>
        /// check date and ref no present or not
        /// </summary>
        /// <param name="date"></param>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public VendorCreditEntry FindVendorCreditEntry(DateTime date, string refNumber)
        {
            foreach (VendorCreditEntry item in this)
            {
                if (item.RefNumber == refNumber && item.VendorCreditDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }

    /// <summary>
    /// adding expense line
    /// </summary>
    [XmlRootAttribute("ExpenseLineAdd", Namespace = "", IsNullable = false)]
    public class ExpenseLineAdd
    {
        private AccountRef m_AccountRef;
        private string m_Amount;
        private string m_Memo;
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_BillableStatus;

        //Improvement::548
        private SalesRepRef m_SalesRepRef;
        
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

        public ExpenseLineAdd()
        { }


        public AccountRef AccountRef
        {
            get
            {
                return m_AccountRef;
            }
            set
            {
                m_AccountRef = value;
            }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public string Memo
        {
            get
            {
                return this.m_Memo;
            }
            set
            {
                this.m_Memo = value;
            }
        }

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public string BillableStatus
        {
            get
            {
                return this.m_BillableStatus;
            }
            set
            {
                this.m_BillableStatus = value;
            }
        }

        //Improvement::548
        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
       
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }

    }

    /// <summary>
    /// adding item line
    /// </summary>
    [XmlRootAttribute("ItemLineAdd", Namespace = "", IsNullable = false)]
    public class ItemLineAdd
    {
        private ItemRef m_ItemRef;
        private InventorySiteRef m_InventorySiteRef;
        private string m_Description;
        private string m_Quantity;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_UnitOfMeasure;
        private string m_Cost;
        private string m_Amount;

        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_BillableStatus;

        private SalesRepRef m_SalesRepRef;
        private OverrideItemAccountRef m_OverrideItemAccount;
        
        //Improvement::548
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

       

        public ItemLineAdd()
        { }

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }
        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }
        public string Desc
        {
            get { return this.m_Description; }
            set { this.m_Description = value; }
        }

        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set
            {
                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    m_SerialNumber = value;
                }
            }
        }

        public string LotNumber
        {
            get { return m_LotNumber; }
            set
            {
                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    m_LotNumber = value;
                }
            }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        public string Cost
        {
            get { return this.m_Cost; }
            set { this.m_Cost = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public string BillableStatus
        {
            get { return this.m_BillableStatus; }
            set { this.m_BillableStatus = value; }
        }

        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }


        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }
		 public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideItemAccount; }
            set { this.m_OverrideItemAccount = value; }
        }

    }

    public enum BillableStatus
    {
        Billable,
        NotBillable,
        HasBeenBilled
    }
}
