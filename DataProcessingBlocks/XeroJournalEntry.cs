﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using System.Linq;
using Xero.NetStandard.OAuth2.Model;
using DataProcessingBlocks.XeroConnection;
using Xero.NetStandard.OAuth2.Api;
using System.Threading.Tasks;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("XeroJournalEntry", Namespace = "", IsNullable = false)]
    public class XeroJournalEntry
    {

        #region Constructor
        public XeroJournalEntry()
        {
        }
        #endregion

        #region Public Properties
        public ManualJournal journal
        {
            get;
            set;
        }
        public ManualJournals jon
        {
            get;
            set;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// This funcion is used to check whether given journal si present in xero.
        /// </summary>
        /// <param name="narration"></param>
        /// <returns></returns>
        public async Task<bool> CheckAndGetNameExistsInXeroAsync(string narration)
        {
            try
            {
                ManualJournals journalResponse = new ManualJournals();
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                string query = "Narration == \"" + narration + "\"";
                var response = res.GetManualJournalsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, null, query, null, null);
                response.Wait();
                journalResponse = response.Result;
                if (journalResponse._ManualJournals.Count > 0)
                {
                    TransactionImporter.TransactionImporter.refnoflag = true;
                    CommonUtilities.GetInstance().existTxnId = (journalResponse._ManualJournals[0].ManualJournalID ?? Guid.Empty);
                }
                else
                {
                    CommonUtilities.GetInstance().existTxnId = Guid.Empty;
                    TransactionImporter.TransactionImporter.refnoflag = false;
                }
            }
            catch (Exception ex)
            {

            }
            return TransactionImporter.TransactionImporter.refnoflag;
        }

        /// <summary>
        ///  This funtion is used to import jounal in xero.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public async Task<bool> ExportToXeroAsync(string statusMessage, string requestText, string txnid, int rowcount)
        {
            bool reponseFlag = false;
            ManualJournals journalResponse = new ManualJournals();
            DataProcessingBlocks.XeroJournalEntry Contact = new XeroJournalEntry();
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            //try
            //{
            //    DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.XeroJournalEntry>.Save(this, fileName);
            //}
            //catch
            //{
            //    statusMessage += "\n ";
            //    statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
            //    return false;
            //}

            //System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            //requestXmlDoc.Load(fileName);
            //string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            //System.IO.File.Delete(fileName);


            string resp = string.Empty;
            string responseFile = string.Empty;
            try
            {
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                CommonUtilities.GetInstance().Type = "Journal";

                ManualJournals xeroJournals = new ManualJournals();
                xeroJournals._ManualJournals = new List<ManualJournal>();
                xeroJournals._ManualJournals.Add(journal);

                //requestText = ModelSerializer.Serialize(journal);
                //requestXmlDoc.LoadXml(requestText);
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);

                if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == false)
                {
                    var response =  res.CreateManualJournalAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, journal);
                    response.Wait();
                    journalResponse = response.Result;
                    if (journalResponse._ManualJournals.Count > 0)
                    {
                        txnid = journalResponse._ManualJournals[0].ManualJournalID.ToString();
                        reponseFlag = true;
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += journalResponse._ManualJournals[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
                else if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == true)
                {
                    if (TransactionImporter.TransactionImporter.refnoflag == true)
                    {
                        var response =  res.UpdateManualJournalAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, CommonUtilities.GetInstance().existTxnId, xeroJournals);
                        response.Wait();
                        journalResponse = response.Result;
                        reponseFlag = true;
                    }
                    else
                    {
                        var response =  res.CreateManualJournalAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, journal);
                        response.Wait();
                        journalResponse = response.Result;
                        reponseFlag = true;
                    }

                    if (journalResponse._ManualJournals.Count > 0)
                    {
                        txnid = journalResponse._ManualJournals[0].ManualJournalID.ToString();
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += journalResponse._ManualJournals[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                // CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
            }

            //TransactionImporter.TransactionImporter.testFlag = true;

            CommonUtilities.GetInstance().xeroMessage = statusMessage;
            CommonUtilities.GetInstance().xeroTxnId = txnid;
            TransactionImporter.TransactionImporter.testFlag = reponseFlag;
            return TransactionImporter.TransactionImporter.testFlag;
        }
        #endregion
    }
}
