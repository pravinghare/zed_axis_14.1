﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportPurchaseOrderClass
    {
        #region Private Members

        public static ImportPurchaseOrderClass m_ImportPurchaseOrderClass;
        public bool isIgnoreAll = false;

        #endregion

        #region Constructor

        public ImportPurchaseOrderClass()
        {

        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Create an instance of Import Purchase Order class.
        /// </summary>
        /// <returns></returns>
        public static ImportPurchaseOrderClass GetInstance()
        {
            if (m_ImportPurchaseOrderClass == null)
                m_ImportPurchaseOrderClass = new ImportPurchaseOrderClass();
            return m_ImportPurchaseOrderClass;
        }


        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Purchase Order QuickBooks collection </returns>
        public DataProcessingBlocks.PurchaseOrderQBEntryCollection ImportPurchaseOrderData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of Purchase Order Entry collections.
            DataProcessingBlocks.PurchaseOrderQBEntryCollection coll = new PurchaseOrderQBEntryCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                //Bug 1434

                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    #region Checking null Values
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    #endregion

                    if (dt.Columns.Contains("PurchaseOrderRefNumber"))
                    {

                        #region Add PurchaseOrder Ref Number

                        PurchaseOrderQBEntry PurchaseOrder = new PurchaseOrderQBEntry();
                        PurchaseOrder = coll.FindPurchaseOrderEntry(dr["PurchaseOrderRefNumber"].ToString());

                        if (PurchaseOrder == null)
                        {
                            PurchaseOrder = new PurchaseOrderQBEntry();
                            DateTime PurchaseOrderDt = new DateTime();

                            string datevalue = string.Empty;
                            if (dt.Columns.Contains("VendorRefFullName"))
                            {
                                #region Validations of Vendor Full name
                                if (dr["VendorRefFullName"].ToString() != string.Empty)
                                {

                                    if (dr["VendorRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                                if (PurchaseOrder.VendorRef.FullName == null)
                                                {
                                                    PurchaseOrder.VendorRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                                if (PurchaseOrder.VendorRef.FullName == null)
                                                {
                                                    PurchaseOrder.VendorRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                            if (PurchaseOrder.VendorRef.FullName == null)
                                            {
                                                PurchaseOrder.VendorRef = null;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                        if (PurchaseOrder.VendorRef.FullName == null)
                                        {
                                            PurchaseOrder.VendorRef = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            //Axis 617 
                            if (dt.Columns.Contains("Currency"))
                            {
                                #region Validations of Currency Full name
                                if (dr["Currency"].ToString() != string.Empty)
                                {
                                    string strCust = dr["Currency"].ToString();
                                    if (strCust.Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (PurchaseOrder.CurrencyRef.FullName == null)
                                                {
                                                    PurchaseOrder.CurrencyRef.FullName = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (PurchaseOrder.CurrencyRef.FullName == null)
                                                {
                                                    PurchaseOrder.CurrencyRef.FullName = null;
                                                }
                                            }


                                        }
                                        else
                                        {
                                            PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (PurchaseOrder.CurrencyRef.FullName == null)
                                            {
                                                PurchaseOrder.CurrencyRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (PurchaseOrder.CurrencyRef.FullName == null)
                                        {
                                            PurchaseOrder.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis 617 ends
                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                                {
                                                    PurchaseOrder.ClassRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                                {
                                                    PurchaseOrder.ClassRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                            {
                                                PurchaseOrder.ClassRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                        if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                        {
                                            PurchaseOrder.ClassRef = null;
                                        }
                                    }
                                }
                                #endregion

                            }


                            if (dt.Columns.Contains("InventorySiteRefFullName"))
                            {
                                #region Validations of Inventory Site Ref Full name
                                if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (PurchaseOrder.InventorySiteRef.FullName == null)
                                                {
                                                    PurchaseOrder.InventorySiteRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (PurchaseOrder.InventorySiteRef.FullName == null)
                                                {
                                                    PurchaseOrder.InventorySiteRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (PurchaseOrder.InventorySiteRef.FullName == null)
                                            {
                                                PurchaseOrder.InventorySiteRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (PurchaseOrder.InventorySiteRef.FullName == null)
                                        {
                                            PurchaseOrder.InventorySiteRef = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipToEntityRefFullName"))
                            {
                                #region Validations of ShipToEntity Full name
                                if (dr["ShipToEntityRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipToEntityRefFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipToEntity full name (" + dr["ShipToEntityRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                                if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                                    PurchaseOrder.ShipToEntityRef = null;
                                                else
                                                    PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString().Substring(0, 209));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                                if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                                    PurchaseOrder.ShipToEntityRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                            if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                                PurchaseOrder.ShipToEntityRef = null;

                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                        if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                            PurchaseOrder.ShipToEntityRef = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TemplateRefFullName"))
                            {
                                #region Validations of Template Full name
                                if (dr["TemplateRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["TemplateRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Template full name (" + dr["TemplateRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (PurchaseOrder.TemplateRef.FullName == null)
                                                    PurchaseOrder.TemplateRef = null;
                                                else
                                                    PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (PurchaseOrder.TemplateRef.FullName == null)
                                                    PurchaseOrder.TemplateRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (PurchaseOrder.TemplateRef.FullName == null)
                                                PurchaseOrder.TemplateRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                        if (PurchaseOrder.TemplateRef.FullName == null)
                                            PurchaseOrder.TemplateRef = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out PurchaseOrderDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrder.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderDt = dttest;
                                            PurchaseOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        PurchaseOrderDt = Convert.ToDateTime(datevalue);
                                        PurchaseOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("PurchaseOrderRefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    PurchaseOrder.PurchaseOrderDate = PurchaseOrderDt;

                                if (dr["PurchaseOrderRefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["PurchaseOrderRefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["PurchaseOrderRefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString().Substring(0, 11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString();
                                        }

                                    }
                                    else
                                        PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString();
                                }
                                #endregion
                            }

                            QuickBookEntities.VendorAddress VendorAddressItem = new VendorAddress();
                            if (dt.Columns.Contains("VendorAddr1"))
                            {
                                #region Validations of Vendor Addr1
                                if (dr["VendorAddr1"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Add1 (" + dr["VendorAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("VendorAddr2"))
                            {
                                #region Validations of Vendor Addr2
                                if (dr["VendorAddr2"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Add2 (" + dr["VendorAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("VendorAddr3"))
                            {
                                #region Validations of Vendor Addr3
                                if (dr["VendorAddr3"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Add3 (" + dr["VendorAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("VendorAddr4"))
                            {
                                #region Validations of Vendor Addr4
                                if (dr["VendorAddr4"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Add4 (" + dr["VendorAddr4"].ToString() + ") is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("VendorAddr5"))
                            {
                                #region Validations of Vendor Addr5
                                if (dr["VendorAddr5"].ToString() != string.Empty)
                                {
                                    if (dr["VendorAddr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Add5 (" + dr["VendorAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("VendorCity"))
                            {
                                #region Validations of Vendor City
                                if (dr["VendorCity"].ToString() != string.Empty)
                                {
                                    if (dr["VendorCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor City (" + dr["VendorCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.City = dr["VendorCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.City = dr["VendorCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.City = dr["VendorCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.City = dr["VendorCity"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("VendorState"))
                            {
                                #region Validations of Vendor State
                                if (dr["VendorState"].ToString() != string.Empty)
                                {
                                    if (dr["VendorState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor State (" + dr["VendorState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.State = dr["VendorState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.State = dr["VendorState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.State = dr["VendorState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.State = dr["VendorState"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("VendorPostalCode"))
                            {
                                #region Validations of Vendor Postal Code
                                if (dr["VendorPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["VendorPostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Postal Code (" + dr["VendorPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("VendorCountry"))
                            {
                                #region Validations of Vendor Country
                                if (dr["VendorCountry"].ToString() != string.Empty)
                                {
                                    if (dr["VendorCountry"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Country (" + dr["VendorCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Country = dr["VendorCountry"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("VendorNote"))
                            {
                                #region Validations of Vendor Note
                                if (dr["VendorNote"].ToString() != string.Empty)
                                {
                                    if (dr["VendorNote"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Vendor Note (" + dr["VendorNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                VendorAddressItem.Note = dr["VendorNote"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                VendorAddressItem.Note = dr["VendorNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            VendorAddressItem.Note = dr["VendorNote"].ToString();
                                        }

                                    }
                                    else
                                    {
                                        VendorAddressItem.Note = dr["VendorNote"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (VendorAddressItem.Addr1 != null || VendorAddressItem.Addr2 != null || VendorAddressItem.Addr3 != null || VendorAddressItem.Addr4 != null || VendorAddressItem.Addr5 != null
                                || VendorAddressItem.City != null || VendorAddressItem.Country != null || VendorAddressItem.PostalCode != null || VendorAddressItem.State != null || VendorAddressItem.Note != null)
                                PurchaseOrder.VendorAddress.Add(VendorAddressItem);

                            if (dt.Columns.Contains("Phone"))
                            {
                                #region Validations of Phone
                                if (dr["Phone"].ToString() != string.Empty)
                                {
                                    if (dr["Phone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Phone = dr["Phone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Phone = dr["Phone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Phone = dr["Phone"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Fax"))
                            {
                                #region Validations of Phone
                                if (dr["Fax"].ToString() != string.Empty)
                                {
                                    if (dr["Fax"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Fax = dr["Fax"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Fax = dr["Fax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Fax = dr["Fax"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Email"))
                            {
                                #region Validations of Phone
                                if (dr["Email"].ToString() != string.Empty)
                                {
                                    if (dr["Email"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Email = dr["Email"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Email = dr["Email"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Email = dr["Email"].ToString();
                                    }
                                }
                                #endregion
                            }
                            QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();
                            if (dt.Columns.Contains("ShipAddr1"))
                            {
                                #region Validations of Ship Addr1
                                if (dr["ShipAddr1"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr2"))
                            {
                                #region Validations of Ship Addr2
                                if (dr["ShipAddr2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr3"))
                            {
                                #region Validations of Ship Addr3
                                if (dr["ShipAddr3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipAddr4"))
                            {
                                #region Validations of Ship Addr4
                                if (dr["ShipAddr4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr5"))
                            {
                                #region Validations of Ship Addr5
                                if (dr["ShipAddr5"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipCity"))
                            {
                                #region Validations of Ship City
                                if (dr["ShipCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.City = dr["ShipCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipState"))
                            {
                                #region Validations of Ship State
                                if (dr["ShipState"].ToString() != string.Empty)
                                {
                                    if (dr["ShipState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.State = dr["ShipState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipPostalCode"))
                            {
                                #region Validations of Ship Postal Code
                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipCountry"))
                            {
                                #region Validations of Ship Country
                                if (dr["ShipCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCountry"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipNote"))
                            {
                                #region Validations of Ship Note
                                if (dr["ShipNote"].ToString() != string.Empty)
                                {
                                    if (dr["ShipNote"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Note = dr["ShipNote"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Note = dr["ShipNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                              || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                                PurchaseOrder.ShipAddress.Add(ShipAddressItem);


                            if (dt.Columns.Contains("TermsRefFullName"))
                            {
                                #region Validations of Terms Full name
                                if (dr["TermsRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["TermsRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TermsRef full name (" + dr["TermsRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                                if (PurchaseOrder.TermsRef.FullName == null)
                                                    PurchaseOrder.TermsRef = null;
                                                else
                                                    PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                                if (PurchaseOrder.TermsRef.FullName == null)
                                                    PurchaseOrder.TermsRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                            if (PurchaseOrder.TermsRef.FullName == null)
                                                PurchaseOrder.TermsRef = null;
                                        }
                                    }

                                    else
                                    {
                                        PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                        if (PurchaseOrder.TermsRef.FullName == null)
                                            PurchaseOrder.TermsRef = null;
                                    }
                                }
                                #endregion

                            }

                            DateTime NewDueDt = new DateTime();
                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region validations of DueDate
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    string duevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(duevalue, out NewDueDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrder.DueDate = duevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.DueDate = duevalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.DueDate = duevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            DateTime NewExpectedDt = new DateTime();
                            if (dt.Columns.Contains("ExpectedDate"))
                            {
                                #region validations of ExpectedDate
                                if (dr["ExpectedDate"].ToString() != "<None>" || dr["ExpectedDate"].ToString() != string.Empty)
                                {
                                    string expvalue = dr["ExpectedDate"].ToString();
                                    if (!DateTime.TryParse(expvalue, out NewExpectedDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(expvalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ExpectedDate (" + expvalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrder.ExpectedDate = expvalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.ExpectedDate = expvalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.ExpectedDate = expvalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.ExpectedDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        PurchaseOrder.ExpectedDate = DateTime.Parse(expvalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipMethodRefFullName"))
                            {
                                #region Validations of ShipMethodRef Full name
                                if (dr["ShipMethodRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ShipMethodRefFullName"].ToString().Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipMethodRef full name (" + dr["ShipMethodRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                                if (PurchaseOrder.ShipMethodRef.FullName == null)
                                                    PurchaseOrder.ShipMethodRef = null;
                                                else
                                                    PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString().Substring(0, 15));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                                if (PurchaseOrder.ShipMethodRef.FullName == null)
                                                    PurchaseOrder.ShipMethodRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                            if (PurchaseOrder.ShipMethodRef.FullName == null)
                                                PurchaseOrder.ShipMethodRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                        if (PurchaseOrder.ShipMethodRef.FullName == null)
                                            PurchaseOrder.ShipMethodRef = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("FOB"))
                            {
                                #region Validations of FOB
                                if (dr["FOB"].ToString() != string.Empty)
                                {
                                    if (dr["FOB"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FOB (" + dr["FOB"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.FOB = dr["FOB"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.FOB = dr["FOB"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.FOB = dr["FOB"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.FOB = dr["FOB"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Memo = dr["Memo"].ToString().Substring(0, 4000);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Memo = dr["Memo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Memo = dr["Memo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Memo = dr["Memo"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("VendorMsg"))
                            {
                                #region Validations for VendorMsg
                                if (dr["VendorMsg"].ToString() != string.Empty)
                                {
                                    if (dr["VendorMsg"].ToString().Length > 99)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This VendorMsg ( " + dr["VendorMsg"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString().Substring(0, 99);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("IsToBePrinted"))
                            {
                                #region Validations of IsToBePrinted
                                if (dr["IsToBePrinted"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                    {
                                        PurchaseOrder.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                        {
                                            PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsToBeEmailed"))
                            {
                                #region Validations of IsToBeEmailed
                                if (dr["IsToBeEmailed"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBeEmailed"].ToString(), out result))
                                    {
                                        PurchaseOrder.IsToBeEmailed = Convert.ToInt32(dr["IsToBeEmailed"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBeEmailed"].ToString().ToLower() == "true")
                                        {
                                            PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBeEmailed"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBeEmailed (" + dr["IsToBeEmailed"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsTaxIncluded"))
                            {
                                #region Validations of IsTaxIncluded
                                if (dr["IsTaxIncluded"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                    {
                                        PurchaseOrder.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                            {
                                #region Validations of SalesTaxCodeRefFullName
                                if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["SalesTaxCodeRefFullName"].ToString();
                                    if (strCust.Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTaxCodeRefFullName (" + dr["SalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                                    PurchaseOrder.SalesTaxCodeRef = null;
                                                else
                                                    PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString().Substring(0, 25));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                                    PurchaseOrder.SalesTaxCodeRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                            if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrder.SalesTaxCodeRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                        if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                            PurchaseOrder.SalesTaxCodeRef = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Other1"))
                            {
                                #region Validations for Other1
                                if (dr["Other1"].ToString() != string.Empty)
                                {
                                    if (dr["Other1"].ToString().Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Other1 = dr["Other1"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Other1 = dr["Other1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Other1 = dr["Other1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Other1 = dr["Other1"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Other2"))
                            {
                                #region Validations for Other2
                                if (dr["Other2"].ToString() != string.Empty)
                                {
                                    if (dr["Other2"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.Other2 = dr["Other2"].ToString().Substring(0, 29);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.Other2 = dr["Other2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.Other2 = dr["Other2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Other2 = dr["Other2"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ExchangeRate"].ToString();
                                                PurchaseOrder.ExchangeRate = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["ExchangeRate"].ToString();
                                                PurchaseOrder.ExchangeRate = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["ExchangeRate"].ToString();
                                            PurchaseOrder.ExchangeRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                    }
                                }

                                #endregion

                            }

                            #region Adding Purchase Order Line

                            DataProcessingBlocks.PurchaseOrderLineAdd PurchaseOrderLine = new PurchaseOrderLineAdd();

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }

                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;

                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("PurchaseOrdLineSalesTaxCodeName"))
                                {
                                    if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["PurchaseOrdLineSalesTaxCodeName"].ToString();

                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion


                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of item Full name

                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (PurchaseOrderLine.ItemRef.FullName == null)
                                                    PurchaseOrderLine.ItemRef = null;
                                                else
                                                    PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (PurchaseOrderLine.ItemRef.FullName == null)
                                                    PurchaseOrderLine.ItemRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (PurchaseOrderLine.ItemRef.FullName == null)
                                                PurchaseOrderLine.ItemRef = null;
                                        }

                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (PurchaseOrderLine.ItemRef.FullName == null)
                                            PurchaseOrderLine.ItemRef = null;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ManufacturerPartNumber"))
                            {
                                #region Validations for ManufacturerPartNumber

                                if (dr["ManufacturerPartNumber"].ToString() != string.Empty)
                                {
                                    if (dr["ManufacturerPartNumber"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ManufacturerPartNumber ( " + dr["ManufacturerPartNumber"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Desc = dr["Description"].ToString().Substring(0, 4095);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Desc = dr["Description"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Desc = dr["Description"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Desc = dr["Description"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    PurchaseOrderLine.Quantity = dr["Quantity"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Rate"))
                            {
                                #region Validations for Rate
                                if (dr["Rate"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;

                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && PurchaseOrder.IsTaxIncluded != null && PurchaseOrder.IsTaxIncluded != string.Empty)
                                            {
                                                if (PurchaseOrder.IsTaxIncluded == "true" || PurchaseOrder.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {

                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    PurchaseOrderLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (PurchaseOrderLine.Rate == null)
                                            {
                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }

                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("PurchaseOrdLineClassName"))
                            {
                                #region Validations of Purchase Order Line Class Full name
                                if (dr["PurchaseOrdLineClassName"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrdLineClassName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order line Class name (" + dr["PurchaseOrdLineClassName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                                if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                                {
                                                    PurchaseOrderLine.ClassRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                                if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                                {
                                                    PurchaseOrderLine.ClassRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                            if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                            {
                                                PurchaseOrderLine.ClassRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                        if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                        {
                                            PurchaseOrderLine.ClassRef = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && PurchaseOrder.IsTaxIncluded != null && PurchaseOrder.IsTaxIncluded != string.Empty)
                                            {
                                                if (PurchaseOrder.IsTaxIncluded == "true" || PurchaseOrder.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    PurchaseOrderLine.Amount = string.Format("{0:000000.00}", amount);
                                                }
                                            }
                                            //Check if EstLine.Amount is null
                                            if (PurchaseOrderLine.Amount == null)
                                            {
                                                PurchaseOrderLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }
                            // AXIS-137 Purchase Order import . InventorySiteLocation error
                            if (dt.Columns.Contains("InventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                    PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                    PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                            PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("CustomerRefFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer name (" + dr["CustomerRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (PurchaseOrderLine.CustomerRef.FullName == null)
                                                {
                                                    PurchaseOrderLine.CustomerRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (PurchaseOrderLine.CustomerRef.FullName == null)
                                                {
                                                    PurchaseOrderLine.CustomerRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (PurchaseOrderLine.CustomerRef.FullName == null)
                                            {
                                                PurchaseOrderLine.CustomerRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (PurchaseOrderLine.CustomerRef.FullName == null)
                                        {
                                            PurchaseOrderLine.CustomerRef = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            DateTime NewServiceDt = new DateTime();
                            if (dt.Columns.Contains("ServiceDate"))
                            {
                                #region validations of ServiceDate
                                if (dr["ServiceDate"].ToString() != "<None>" || dr["ServiceDate"].ToString() != string.Empty)
                                {
                                    string expvalue = dr["ServiceDate"].ToString();
                                    if (!DateTime.TryParse(expvalue, out NewServiceDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(expvalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ServiceDate (" + expvalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrderLine.ServiceDate = expvalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrderLine.ServiceDate = expvalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrderLine.ServiceDate = expvalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ServiceDate = DateTime.Parse(expvalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("PurchaseOrdLineSalesTaxCodeName"))
                            {
                                #region Validations of sales tax code Full name
                                if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This sales tax code name (" + dr["PurchaseOrdLineSalesTaxCodeName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                                if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                    PurchaseOrderLine.SalesTaxCodeRef = null;
                                                else
                                                {
                                                    PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                                if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                    PurchaseOrderLine.SalesTaxCodeRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                            if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrderLine.SalesTaxCodeRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                        if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                            PurchaseOrderLine.SalesTaxCodeRef = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of Override Item Account Full name
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["OverrideItemAccountFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Override Item Account name (" + dr["OverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                    PurchaseOrderLine.OverrideItemAccountRef = null;
                                                else
                                                    PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                    PurchaseOrderLine.OverrideItemAccountRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                PurchaseOrderLine.OverrideItemAccountRef = null;

                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                            PurchaseOrderLine.OverrideItemAccountRef = null;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("POLineOther1"))
                            {
                                #region Validations for Other1

                                if (dr["POLineOther1"].ToString() != string.Empty)
                                {
                                    if (dr["POLineOther1"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Line Other1 ( " + dr["POLineOther1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString().Substring(0, 29);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("POLineOther2"))
                            {
                                #region Validations for Other2

                                if (dr["POLineOther2"].ToString() != string.Empty)
                                {
                                    if (dr["POLineOther2"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Line Other2 ( " + dr["POLineOther2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString().Substring(0, 29);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                    }
                                }

                                #endregion
                            }
                            DataExt.Dispose();
                            if (dt.Columns.Contains("OwnerID"))
                            {
                                #region Validations of OwnerID
                                if (dr["OwnerID"].ToString() != string.Empty)
                                {
                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(dr["OwnerID"].ToString(), null, null);
                                    if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                    {
                                        PurchaseOrderLine.DataExt = null;
                                    }

                                }

                                #endregion

                            }
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName (" + dr["DataExtName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (PurchaseOrderLine.DataExt == null)
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                                else
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString().Substring(0, 31), PurchaseOrderLine.DataExt.DataExtValue);
                                                if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                                {
                                                    PurchaseOrderLine.DataExt = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (PurchaseOrderLine.DataExt == null)
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                                else
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                                if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                                {
                                                    PurchaseOrderLine.DataExt = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (PurchaseOrderLine.DataExt == null)
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                            else
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                            if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                            {
                                                PurchaseOrderLine.DataExt = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseOrderLine.DataExt == null)
                                            PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                        else
                                            PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                        if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                        {
                                            PurchaseOrderLine.DataExt = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue"].ToString() != string.Empty)
                                {
                                    if (PurchaseOrderLine.DataExt == null)
                                        PurchaseOrderLine.DataExt = DataExt.GetInstance(null, null, dr["DataExtValue"].ToString());
                                    else
                                        PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, PurchaseOrderLine.DataExt.DataExtName, dr["DataExtValue"].ToString());
                                    if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                    {
                                        PurchaseOrderLine.DataExt = null;
                                    }

                                }

                                #endregion

                            }
                            PurchaseOrder.PurchaseOrderLineAdd.Add(PurchaseOrderLine);

                            #endregion

                            coll.Add(PurchaseOrder);
                        }
                        else
                        {
                            #region Adding Purchase Order Line

                            DataProcessingBlocks.PurchaseOrderLineAdd PurchaseOrderLine = new PurchaseOrderLineAdd();

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }

                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("PurchaseOrdLineSalesTaxCodeName"))
                                {
                                    if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["PurchaseOrdLineSalesTaxCodeName"].ToString();

                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion


                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of item Full name

                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (PurchaseOrderLine.ItemRef.FullName == null)
                                                    PurchaseOrderLine.ItemRef = null;
                                                else
                                                    PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (PurchaseOrderLine.ItemRef.FullName == null)
                                                    PurchaseOrderLine.ItemRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (PurchaseOrderLine.ItemRef.FullName == null)
                                                PurchaseOrderLine.ItemRef = null;
                                        }

                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (PurchaseOrderLine.ItemRef.FullName == null)
                                            PurchaseOrderLine.ItemRef = null;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ManufacturerPartNumber"))
                            {
                                #region Validations for ManufacturerPartNumber

                                if (dr["ManufacturerPartNumber"].ToString() != string.Empty)
                                {
                                    if (dr["ManufacturerPartNumber"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ManufacturerPartNumber ( " + dr["ManufacturerPartNumber"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Desc = dr["Description"].ToString().Substring(0, 4095);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Desc = dr["Description"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Desc = dr["Description"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Desc = dr["Description"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    PurchaseOrderLine.Quantity = dr["Quantity"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Rate"))
                            {
                                #region Validations for Rate
                                if (dr["Rate"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && PurchaseOrder.IsTaxIncluded != null && PurchaseOrder.IsTaxIncluded != string.Empty)
                                            {
                                                if (PurchaseOrder.IsTaxIncluded == "true" || PurchaseOrder.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    PurchaseOrderLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (PurchaseOrderLine.Rate == null)
                                            {
                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }

                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("PurchaseOrdLineClassName"))
                            {
                                #region Validations of Purchase Order Line Class Full name
                                if (dr["PurchaseOrdLineClassName"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrdLineClassName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order line Class name (" + dr["PurchaseOrdLineClassName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                                if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                                {
                                                    PurchaseOrderLine.ClassRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                                if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                                {
                                                    PurchaseOrderLine.ClassRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                            if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                            {
                                                PurchaseOrderLine.ClassRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                        if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                        {
                                            PurchaseOrderLine.ClassRef = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && PurchaseOrder.IsTaxIncluded != null && PurchaseOrder.IsTaxIncluded != string.Empty)
                                            {
                                                if (PurchaseOrder.IsTaxIncluded == "true" || PurchaseOrder.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    PurchaseOrderLine.Amount = string.Format("{0:000000.00}", amount);
                                                }
                                            }
                                            //Check if EstLine.Amount is null
                                            if (PurchaseOrderLine.Amount == null)
                                            {
                                                PurchaseOrderLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }
                            //AXIS-137 Purchase Order import . InventorySiteLocation error
                            if (dt.Columns.Contains("InventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                    PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                    PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                            PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("CustomerRefFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer name (" + dr["CustomerRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (PurchaseOrderLine.CustomerRef.FullName == null)
                                                {
                                                    PurchaseOrderLine.CustomerRef = null;
                                                }
                                                else
                                                {
                                                    PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (PurchaseOrderLine.CustomerRef.FullName == null)
                                                {
                                                    PurchaseOrderLine.CustomerRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (PurchaseOrderLine.CustomerRef.FullName == null)
                                            {
                                                PurchaseOrderLine.CustomerRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (PurchaseOrderLine.CustomerRef.FullName == null)
                                        {
                                            PurchaseOrderLine.CustomerRef = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            DateTime NewServiceDt = new DateTime();
                            if (dt.Columns.Contains("ServiceDate"))
                            {
                                #region validations of ServiceDate
                                if (dr["ServiceDate"].ToString() != "<None>" || dr["ServiceDate"].ToString() != string.Empty)
                                {
                                    string expvalue = dr["ServiceDate"].ToString();
                                    if (!DateTime.TryParse(expvalue, out NewServiceDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(expvalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ServiceDate (" + expvalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    PurchaseOrderLine.ServiceDate = expvalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    PurchaseOrderLine.ServiceDate = expvalue;
                                                }
                                            }
                                            else
                                            {
                                                PurchaseOrderLine.ServiceDate = expvalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ServiceDate = DateTime.Parse(expvalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("PurchaseOrdLineSalesTaxCodeName"))
                            {
                                #region Validations of sales tax code Full name
                                if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This sales tax code name (" + dr["PurchaseOrdLineSalesTaxCodeName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                                if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                    PurchaseOrderLine.SalesTaxCodeRef = null;
                                                else
                                                    PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                                if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                    PurchaseOrderLine.SalesTaxCodeRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                            if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrderLine.SalesTaxCodeRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                        if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                            PurchaseOrderLine.SalesTaxCodeRef = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of Override Item Account Full name
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["OverrideItemAccountFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Override Item Account name (" + dr["OverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                    PurchaseOrderLine.OverrideItemAccountRef = null;
                                                else
                                                    PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                    PurchaseOrderLine.OverrideItemAccountRef = null;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                PurchaseOrderLine.OverrideItemAccountRef = null;

                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                            PurchaseOrderLine.OverrideItemAccountRef = null;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("POLineOther1"))
                            {
                                #region Validations for Other1

                                if (dr["POLineOther1"].ToString() != string.Empty)
                                {
                                    if (dr["POLineOther1"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Line Other1 ( " + dr["POLineOther1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString().Substring(0, 29);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("POLineOther2"))
                            {
                                #region Validations for Other2

                                if (dr["POLineOther2"].ToString() != string.Empty)
                                {
                                    if (dr["POLineOther2"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Purchase Order Line Other2 ( " + dr["POLineOther2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString().Substring(0, 29);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                    }
                                }

                                #endregion
                            }
                            DataExt.Dispose();
                            if (dt.Columns.Contains("OwnerID"))
                            {
                                #region Validations of OwnerID
                                if (dr["OwnerID"].ToString() != string.Empty)
                                {
                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(dr["OwnerID"].ToString(), null, null);
                                    if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                    {
                                        PurchaseOrderLine.DataExt = null;
                                    }

                                }

                                #endregion

                            }
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName (" + dr["DataExtName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (PurchaseOrderLine.DataExt == null)
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                                else
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString().Substring(0, 31), PurchaseOrderLine.DataExt.DataExtValue);
                                                if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                                {
                                                    PurchaseOrderLine.DataExt = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (PurchaseOrderLine.DataExt == null)
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                                else
                                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                                if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                                {
                                                    PurchaseOrderLine.DataExt = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (PurchaseOrderLine.DataExt == null)
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                            else
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                            if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                            {
                                                PurchaseOrderLine.DataExt = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseOrderLine.DataExt == null)
                                            PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                        else
                                            PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                        if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                        {
                                            PurchaseOrderLine.DataExt = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue"].ToString() != string.Empty)
                                {
                                    if (PurchaseOrderLine.DataExt == null)
                                        PurchaseOrderLine.DataExt = DataExt.GetInstance(null, null, dr["DataExtValue"].ToString());
                                    else
                                        PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, PurchaseOrderLine.DataExt.DataExtName, dr["DataExtValue"].ToString());
                                    if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                    {
                                        PurchaseOrderLine.DataExt = null;
                                    }

                                }

                                #endregion

                            }

                            PurchaseOrder.PurchaseOrderLineAdd.Add(PurchaseOrderLine);

                            #endregion

                        }
                        #endregion
                    }
                    else
                    {
                        #region Without Adding Ref Number
                        PurchaseOrderQBEntry PurchaseOrder = new PurchaseOrderQBEntry();
                        DateTime PurchaseOrderDt = new DateTime();

                        string datevalue = string.Empty;
                        if (dt.Columns.Contains("VendorRefFullName"))
                        {
                            #region Validations of Vendor Full name
                            if (dr["VendorRefFullName"].ToString() != string.Empty)
                            {

                                if (dr["VendorRefFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                            if (PurchaseOrder.VendorRef.FullName == null)
                                            {
                                                PurchaseOrder.VendorRef = null;
                                            }
                                            else
                                            {
                                                PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                            if (PurchaseOrder.VendorRef.FullName == null)
                                            {
                                                PurchaseOrder.VendorRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                        if (PurchaseOrder.VendorRef.FullName == null)
                                        {
                                            PurchaseOrder.VendorRef = null;
                                        }
                                    }

                                }
                                else
                                {
                                    PurchaseOrder.VendorRef = new VendorRef(dr["VendorRefFullName"].ToString());
                                    if (PurchaseOrder.VendorRef.FullName == null)
                                    {
                                        PurchaseOrder.VendorRef = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        //Axis 617 
                        if (dt.Columns.Contains("Currency"))
                        {
                            #region Validations of Currency Full name
                            if (dr["Currency"].ToString() != string.Empty)
                            {
                                string strCust = dr["Currency"].ToString();
                                if (strCust.Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (PurchaseOrder.CurrencyRef.FullName == null)
                                            {
                                                PurchaseOrder.CurrencyRef.FullName = null;
                                            }
                                            else
                                            {
                                                PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (PurchaseOrder.CurrencyRef.FullName == null)
                                            {
                                                PurchaseOrder.CurrencyRef.FullName = null;
                                            }
                                        }


                                    }
                                    else
                                    {
                                        PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        if (PurchaseOrder.CurrencyRef.FullName == null)
                                        {
                                            PurchaseOrder.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (PurchaseOrder.CurrencyRef.FullName == null)
                                    {
                                        PurchaseOrder.CurrencyRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis 617 ends
                        if (dt.Columns.Contains("ClassRefFullName"))
                        {
                            #region Validations of Class Full name
                            if (dr["ClassRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ClassRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                            {
                                                PurchaseOrder.ClassRef = null;
                                            }
                                            else
                                            {
                                                PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                            {
                                                PurchaseOrder.ClassRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                        if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                        {
                                            PurchaseOrder.ClassRef = null;
                                        }
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                    if (PurchaseOrder.ClassRef.FullName == null && PurchaseOrder.ClassRef.ListID == null)
                                    {
                                        PurchaseOrder.ClassRef = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("InventorySiteRefFullName"))
                        {
                            #region Validations of Inventory Site Ref Full name
                            if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Class name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (PurchaseOrder.InventorySiteRef.FullName == null)
                                            {
                                                PurchaseOrder.InventorySiteRef = null;
                                            }
                                            else
                                            {
                                                PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 31));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (PurchaseOrder.InventorySiteRef.FullName == null)
                                            {
                                                PurchaseOrder.InventorySiteRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (PurchaseOrder.InventorySiteRef.FullName == null)
                                        {
                                            PurchaseOrder.InventorySiteRef = null;
                                        }
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                    if (PurchaseOrder.InventorySiteRef.FullName == null)
                                    {
                                        PurchaseOrder.InventorySiteRef = null;
                                    }
                                }
                            }
                            #endregion

                        }


                        if (dt.Columns.Contains("ShipToEntityRefFullName"))
                        {
                            #region Validations of ShipToEntity Full name
                            if (dr["ShipToEntityRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ShipToEntityRefFullName"].ToString().Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipToEntity full name (" + dr["ShipToEntityRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                            if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                                PurchaseOrder.ShipToEntityRef = null;
                                            else
                                                PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString().Substring(0, 209));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                            if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                                PurchaseOrder.ShipToEntityRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                        if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                            PurchaseOrder.ShipToEntityRef = null;

                                    }
                                }
                                else
                                {
                                    PurchaseOrder.ShipToEntityRef = new ShipToEntityRef(dr["ShipToEntityRefFullName"].ToString());
                                    if (PurchaseOrder.ShipToEntityRef.FullName == null)
                                        PurchaseOrder.ShipToEntityRef = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TemplateRefFullName"))
                        {
                            #region Validations of Template Full name
                            if (dr["TemplateRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["TemplateRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Template full name (" + dr["TemplateRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (PurchaseOrder.TemplateRef.FullName == null)
                                                PurchaseOrder.TemplateRef = null;
                                            else
                                                PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (PurchaseOrder.TemplateRef.FullName == null)
                                                PurchaseOrder.TemplateRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                        if (PurchaseOrder.TemplateRef.FullName == null)
                                            PurchaseOrder.TemplateRef = null;
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                    if (PurchaseOrder.TemplateRef.FullName == null)
                                        PurchaseOrder.TemplateRef = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out PurchaseOrderDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderDt = dttest;
                                        PurchaseOrder.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    PurchaseOrderDt = Convert.ToDateTime(datevalue);
                                    PurchaseOrder.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("PurchaseOrderRefNumber"))
                        {
                            #region Validations of Ref Number
                            if (datevalue != string.Empty)
                                PurchaseOrder.PurchaseOrderDate = PurchaseOrderDt;

                            if (dr["PurchaseOrderRefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["PurchaseOrderRefNumber"].ToString();
                                if (strRefNum.Length > 11)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["PurchaseOrderRefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString().Substring(0, 11);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString();
                                    }

                                }
                                else
                                    PurchaseOrder.RefNumber = dr["PurchaseOrderRefNumber"].ToString();
                            }
                            #endregion
                        }

                        QuickBookEntities.VendorAddress VendorAddressItem = new VendorAddress();
                        if (dt.Columns.Contains("VendorAddr1"))
                        {
                            #region Validations of Vendor Addr1
                            if (dr["VendorAddr1"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddr1"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Add1 (" + dr["VendorAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("VendorAddr2"))
                        {
                            #region Validations of Vendor Addr2
                            if (dr["VendorAddr2"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddr2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Add2 (" + dr["VendorAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("VendorAddr3"))
                        {
                            #region Validations of Vendor Addr3
                            if (dr["VendorAddr3"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddr3"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Add3 (" + dr["VendorAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("VendorAddr4"))
                        {
                            #region Validations of Vendor Addr4
                            if (dr["VendorAddr4"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddr4"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Add4 (" + dr["VendorAddr4"].ToString() + ") is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("VendorAddr5"))
                        {
                            #region Validations of Vendor Addr5
                            if (dr["VendorAddr5"].ToString() != string.Empty)
                            {
                                if (dr["VendorAddr5"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Add5 (" + dr["VendorAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("VendorCity"))
                        {
                            #region Validations of Vendor City
                            if (dr["VendorCity"].ToString() != string.Empty)
                            {
                                if (dr["VendorCity"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor City (" + dr["VendorCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.City = dr["VendorCity"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.City = dr["VendorCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.City = dr["VendorCity"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.City = dr["VendorCity"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("VendorState"))
                        {
                            #region Validations of Vendor State
                            if (dr["VendorState"].ToString() != string.Empty)
                            {
                                if (dr["VendorState"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor State (" + dr["VendorState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.State = dr["VendorState"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.State = dr["VendorState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.State = dr["VendorState"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.State = dr["VendorState"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("VendorPostalCode"))
                        {
                            #region Validations of Vendor Postal Code
                            if (dr["VendorPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["VendorPostalCode"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Postal Code (" + dr["VendorPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString().Substring(0, 13);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("VendorCountry"))
                        {
                            #region Validations of Vendor Country
                            if (dr["VendorCountry"].ToString() != string.Empty)
                            {
                                if (dr["VendorCountry"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Country (" + dr["VendorCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Country = dr["VendorCountry"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("VendorNote"))
                        {
                            #region Validations of Vendor Note
                            if (dr["VendorNote"].ToString() != string.Empty)
                            {
                                if (dr["VendorNote"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Vendor Note (" + dr["VendorNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            VendorAddressItem.Note = dr["VendorNote"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            VendorAddressItem.Note = dr["VendorNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        VendorAddressItem.Note = dr["VendorNote"].ToString();
                                    }

                                }
                                else
                                {
                                    VendorAddressItem.Note = dr["VendorNote"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (VendorAddressItem.Addr1 != null || VendorAddressItem.Addr2 != null || VendorAddressItem.Addr3 != null || VendorAddressItem.Addr4 != null || VendorAddressItem.Addr5 != null
                            || VendorAddressItem.City != null || VendorAddressItem.Country != null || VendorAddressItem.PostalCode != null || VendorAddressItem.State != null || VendorAddressItem.Note != null)
                            PurchaseOrder.VendorAddress.Add(VendorAddressItem);

                        if (dt.Columns.Contains("Phone"))
                        {
                            #region Validations of Phone
                            if (dr["Phone"].ToString() != string.Empty)
                            {
                                if (dr["Phone"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Phone = dr["Phone"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Phone = dr["Phone"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Fax"))
                        {
                            #region Validations of Phone
                            if (dr["Fax"].ToString() != string.Empty)
                            {
                                if (dr["Fax"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Fax = dr["Fax"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Fax = dr["Fax"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Email"))
                        {
                            #region Validations of Email
                            if (dr["Email"].ToString() != string.Empty)
                            {
                                if (dr["Email"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Email = dr["Email"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Email = dr["Email"].ToString();
                                }
                            }
                            #endregion
                        }
                        QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();
                        if (dt.Columns.Contains("ShipAddr1"))
                        {
                            #region Validations of Ship Addr1
                            if (dr["ShipAddr1"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr1"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipAddr2"))
                        {
                            #region Validations of Ship Addr2
                            if (dr["ShipAddr2"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipAddr3"))
                        {
                            #region Validations of Ship Addr3
                            if (dr["ShipAddr3"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr3"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipAddr4"))
                        {
                            #region Validations of Ship Addr4
                            if (dr["ShipAddr4"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr4"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipAddr5"))
                        {
                            #region Validations of Ship Addr5
                            if (dr["ShipAddr5"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr5"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipCity"))
                        {
                            #region Validations of Ship City
                            if (dr["ShipCity"].ToString() != string.Empty)
                            {
                                if (dr["ShipCity"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.City = dr["ShipCity"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipState"))
                        {
                            #region Validations of Ship State
                            if (dr["ShipState"].ToString() != string.Empty)
                            {
                                if (dr["ShipState"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.State = dr["ShipState"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipPostalCode"))
                        {
                            #region Validations of Ship Postal Code
                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipPostalCode"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 13);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipCountry"))
                        {
                            #region Validations of Ship Country
                            if (dr["ShipCountry"].ToString() != string.Empty)
                            {
                                if (dr["ShipCountry"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipNote"))
                        {
                            #region Validations of Ship Note
                            if (dr["ShipNote"].ToString() != string.Empty)
                            {
                                if (dr["ShipNote"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Note = dr["ShipNote"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Note = dr["ShipNote"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                          || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                            PurchaseOrder.ShipAddress.Add(ShipAddressItem);


                        if (dt.Columns.Contains("TermsRefFullName"))
                        {
                            #region Validations of Terms Full name
                            if (dr["TermsRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["TermsRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TermsRef full name (" + dr["TermsRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                            if (PurchaseOrder.TermsRef.FullName == null)
                                                PurchaseOrder.TermsRef = null;
                                            else
                                                PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                            if (PurchaseOrder.TermsRef.FullName == null)
                                                PurchaseOrder.TermsRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                        if (PurchaseOrder.TermsRef.FullName == null)
                                            PurchaseOrder.TermsRef = null;
                                    }
                                }

                                else
                                {
                                    PurchaseOrder.TermsRef = new TermsRef(dr["TermsRefFullName"].ToString());
                                    if (PurchaseOrder.TermsRef.FullName == null)
                                        PurchaseOrder.TermsRef = null;
                                }
                            }
                            #endregion

                        }

                        DateTime NewDueDt = new DateTime();
                        if (dt.Columns.Contains("DueDate"))
                        {
                            #region validations of DueDate
                            if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                            {
                                string duevalue = dr["DueDate"].ToString();
                                if (!DateTime.TryParse(duevalue, out NewDueDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.DueDate = duevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.DueDate = duevalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.DueDate = duevalue;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.DueDate = dttest.ToString("yyyy-MM-dd");
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        DateTime NewExpectedDt = new DateTime();
                        if (dt.Columns.Contains("ExpectedDate"))
                        {
                            #region validations of ExpectedDate
                            if (dr["ExpectedDate"].ToString() != "<None>" || dr["ExpectedDate"].ToString() != string.Empty)
                            {
                                string expvalue = dr["ExpectedDate"].ToString();
                                if (!DateTime.TryParse(expvalue, out NewExpectedDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(expvalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpectedDate (" + expvalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrder.ExpectedDate = expvalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.ExpectedDate = expvalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.ExpectedDate = expvalue;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ExpectedDate = dttest.ToString("yyyy-MM-dd");
                                    }


                                }
                                else
                                {
                                    PurchaseOrder.ExpectedDate = DateTime.Parse(expvalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipMethodRefFullName"))
                        {
                            #region Validations of ShipMethodRef Full name
                            if (dr["ShipMethodRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ShipMethodRefFullName"].ToString().Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ShipMethodRef full name (" + dr["ShipMethodRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                            if (PurchaseOrder.ShipMethodRef.FullName == null)
                                                PurchaseOrder.ShipMethodRef = null;
                                            else
                                                PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString().Substring(0, 15));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                            if (PurchaseOrder.ShipMethodRef.FullName == null)
                                                PurchaseOrder.ShipMethodRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                        if (PurchaseOrder.ShipMethodRef.FullName == null)
                                            PurchaseOrder.ShipMethodRef = null;
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                    if (PurchaseOrder.ShipMethodRef.FullName == null)
                                        PurchaseOrder.ShipMethodRef = null;
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("FOB"))
                        {
                            #region Validations of FOB
                            if (dr["FOB"].ToString() != string.Empty)
                            {
                                if (dr["FOB"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FOB (" + dr["FOB"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.FOB = dr["FOB"].ToString().Substring(0, 13);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.FOB = dr["FOB"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.FOB = dr["FOB"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.FOB = dr["FOB"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Memo = dr["Memo"].ToString().Substring(0, 4000);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Memo = dr["Memo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Memo = dr["Memo"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Memo = dr["Memo"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("VendorMsg"))
                        {
                            #region Validations for VendorMsg
                            if (dr["VendorMsg"].ToString() != string.Empty)
                            {
                                if (dr["VendorMsg"].ToString().Length > 99)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This VendorMsg ( " + dr["VendorMsg"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString().Substring(0, 99);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.VendorMsg = dr["VendorMsg"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("IsToBePrinted"))
                        {
                            #region Validations of IsToBePrinted
                            if (dr["IsToBePrinted"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                {
                                    PurchaseOrder.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                    {
                                        PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("IsToBeEmailed"))
                        {
                            #region Validations of IsToBeEmailed
                            if (dr["IsToBeEmailed"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsToBeEmailed"].ToString(), out result))
                                {
                                    PurchaseOrder.IsToBeEmailed = Convert.ToInt32(dr["IsToBeEmailed"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsToBeEmailed"].ToString().ToLower() == "true")
                                    {
                                        PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsToBeEmailed"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsToBeEmailed (" + dr["IsToBeEmailed"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("IsTaxIncluded"))
                        {
                            #region Validations of IsTaxIncluded
                            if (dr["IsTaxIncluded"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                {
                                    PurchaseOrder.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                    {
                                        PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrder.IsTaxIncluded = dr["IsTaxIncluded"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                        {
                            #region Validations of SalesTaxCodeRefFullName
                            if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["SalesTaxCodeRefFullName"].ToString();
                                if (strCust.Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTaxCodeRefFullName (" + dr["SalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                            if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrder.SalesTaxCodeRef = null;
                                            else
                                                PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString().Substring(0, 25));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                            if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrder.SalesTaxCodeRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                        if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                            PurchaseOrder.SalesTaxCodeRef = null;
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                    if (PurchaseOrder.SalesTaxCodeRef.FullName == null)
                                        PurchaseOrder.SalesTaxCodeRef = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Other1"))
                        {
                            #region Validations for Other1
                            if (dr["Other1"].ToString() != string.Empty)
                            {
                                if (dr["Other1"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Other1 = dr["Other1"].ToString().Substring(0, 25);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Other1 = dr["Other1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Other1 = dr["Other1"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Other1 = dr["Other1"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Other2"))
                        {
                            #region Validations for Other2
                            if (dr["Other2"].ToString() != string.Empty)
                            {
                                if (dr["Other2"].ToString().Length > 29)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrder.Other2 = dr["Other2"].ToString().Substring(0, 29);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrder.Other2 = dr["Other2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrder.Other2 = dr["Other2"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.Other2 = dr["Other2"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["ExchangeRate"].ToString();
                                            PurchaseOrder.ExchangeRate = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["ExchangeRate"].ToString();
                                            PurchaseOrder.ExchangeRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["ExchangeRate"].ToString();
                                        PurchaseOrder.ExchangeRate = strRate;
                                    }
                                }
                                else
                                {
                                    PurchaseOrder.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                }
                            }

                            #endregion

                        }

                        #region Adding Purchase Order Line

                        DataProcessingBlocks.PurchaseOrderLineAdd PurchaseOrderLine = new PurchaseOrderLineAdd();

                        #region Checking and setting SalesTaxCode

                        if (defaultSettings == null)
                        {
                            CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return null;

                        }
                        //string IsTaxable = string.Empty;
                        string TaxRateValue = string.Empty;
                        string ItemSaleTaxFullName = string.Empty;
                        //if default settings contain checkBoxGrossToNet checked.
                        if (defaultSettings.GrossToNet == "1")
                        {
                            if (dt.Columns.Contains("PurchaseOrdLineSalesTaxCodeName"))
                            {
                                if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString() != string.Empty)
                                {
                                    string FullName = dr["PurchaseOrdLineSalesTaxCodeName"].ToString();
                                    //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                    ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                }
                            }
                        }
                        #endregion


                        if (dt.Columns.Contains("ItemRefFullName"))
                        {
                            #region Validations of item Full name

                            if (dr["ItemRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemRefFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (PurchaseOrderLine.ItemRef.FullName == null)
                                                PurchaseOrderLine.ItemRef = null;
                                            else
                                                PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (PurchaseOrderLine.ItemRef.FullName == null)
                                                PurchaseOrderLine.ItemRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (PurchaseOrderLine.ItemRef.FullName == null)
                                            PurchaseOrderLine.ItemRef = null;
                                    }

                                }
                                else
                                {
                                    PurchaseOrderLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    if (PurchaseOrderLine.ItemRef.FullName == null)
                                        PurchaseOrderLine.ItemRef = null;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ManufacturerPartNumber"))
                        {
                            #region Validations for ManufacturerPartNumber

                            if (dr["ManufacturerPartNumber"].ToString() != string.Empty)
                            {
                                if (dr["ManufacturerPartNumber"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ManufacturerPartNumber ( " + dr["ManufacturerPartNumber"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("Description"))
                        {
                            #region Validations for Description
                            if (dr["Description"].ToString() != string.Empty)
                            {
                                if (dr["Description"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.Desc = dr["Description"].ToString().Substring(0, 4095);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.Desc = dr["Description"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Desc = dr["Description"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.Desc = dr["Description"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Quantity"))
                        {
                            #region Validations for Quantity
                            if (dr["Quantity"].ToString() != string.Empty)
                            {
                                PurchaseOrderLine.Quantity = dr["Quantity"].ToString();
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations for UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.UnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("Rate"))
                        {
                            #region Validations for Rate
                            if (dr["Rate"].ToString() != string.Empty)
                            {
                                decimal rate = 0;
                                //decimal amount;
                                if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && PurchaseOrder.IsTaxIncluded != null && PurchaseOrder.IsTaxIncluded != string.Empty)
                                        {
                                            if (PurchaseOrder.IsTaxIncluded == "true" || PurchaseOrder.IsTaxIncluded == "1")
                                            {
                                                decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    rate = Rate / (1 + (TaxRate / 100));
                                                }

                                                PurchaseOrderLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                            }
                                        }
                                        //Check if InvoiceLine.Rate is null
                                        if (PurchaseOrderLine.Rate == null)
                                        {
                                            PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                    }

                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("PurchaseOrdLineClassName"))
                        {
                            #region Validations of Purchase Order Line Class Full name
                            if (dr["PurchaseOrdLineClassName"].ToString() != string.Empty)
                            {
                                if (dr["PurchaseOrdLineClassName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order line Class name (" + dr["PurchaseOrdLineClassName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                            if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                            {
                                                PurchaseOrderLine.ClassRef = null;
                                            }
                                            else
                                            {
                                                PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                            if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                            {
                                                PurchaseOrderLine.ClassRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                        if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                        {
                                            PurchaseOrderLine.ClassRef = null;
                                        }
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.ClassRef = new ClassRef(string.Empty, dr["PurchaseOrdLineClassName"].ToString());
                                    if (PurchaseOrderLine.ClassRef.FullName == null && PurchaseOrderLine.ClassRef.ListID == null)
                                    {
                                        PurchaseOrderLine.ClassRef = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("Amount"))
                        {
                            #region Validations for Amount
                            if (dr["Amount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Amount = dr["Amount"].ToString();
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && PurchaseOrder.IsTaxIncluded != null && PurchaseOrder.IsTaxIncluded != string.Empty)
                                        {
                                            if (PurchaseOrder.IsTaxIncluded == "true" || PurchaseOrder.IsTaxIncluded == "1")
                                            {
                                                decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    amount = Amount / (1 + (TaxRate / 100));
                                                }

                                                PurchaseOrderLine.Amount = string.Format("{0:000000.00}", amount);
                                            }
                                        }
                                        //Check if EstLine.Amount is null
                                        if (PurchaseOrderLine.Amount == null)
                                        {
                                            PurchaseOrderLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                    }

                                }
                            }

                            #endregion
                        }
                        //AXIS-137 Purchase Order import . InventorySiteLocation error
                        if (dt.Columns.Contains("InventorySiteLocationRef"))
                        {
                            #region Validations of InventorySiteLocationRef

                            if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                            {
                                if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                                PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                            PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                    if (PurchaseOrderLine.InventorySiteLocationRef.FullName == null)
                                        PurchaseOrderLine.InventorySiteLocationRef.FullName = null;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("CustomerRefFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["CustomerRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["CustomerRefFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Customer name (" + dr["CustomerRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (PurchaseOrderLine.CustomerRef.FullName == null)
                                            {
                                                PurchaseOrderLine.CustomerRef = null;
                                            }
                                            else
                                            {
                                                PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (PurchaseOrderLine.CustomerRef.FullName == null)
                                            {
                                                PurchaseOrderLine.CustomerRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (PurchaseOrderLine.CustomerRef.FullName == null)
                                        {
                                            PurchaseOrderLine.CustomerRef = null;
                                        }
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                    if (PurchaseOrderLine.CustomerRef.FullName == null)
                                    {
                                        PurchaseOrderLine.CustomerRef = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        DateTime NewServiceDt = new DateTime();
                        if (dt.Columns.Contains("ServiceDate"))
                        {
                            #region validations of ServiceDate
                            if (dr["ServiceDate"].ToString() != "<None>" || dr["ServiceDate"].ToString() != string.Empty)
                            {
                                string expvalue = dr["ServiceDate"].ToString();
                                if (!DateTime.TryParse(expvalue, out NewServiceDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(expvalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ServiceDate (" + expvalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                PurchaseOrderLine.ServiceDate = expvalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                PurchaseOrderLine.ServiceDate = expvalue;
                                            }
                                        }
                                        else
                                        {
                                            PurchaseOrderLine.ServiceDate = expvalue;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                    }


                                }
                                else
                                {
                                    PurchaseOrderLine.ServiceDate = DateTime.Parse(expvalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("PurchaseOrdLineSalesTaxCodeName"))
                        {
                            #region Validations of sales tax code Full name
                            if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString() != string.Empty)
                            {
                                if (dr["PurchaseOrdLineSalesTaxCodeName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This sales tax code name (" + dr["PurchaseOrdLineSalesTaxCodeName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                            if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrderLine.SalesTaxCodeRef = null;
                                            else
                                                PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString().Substring(0, 3));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                            if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                                PurchaseOrderLine.SalesTaxCodeRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                        if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                            PurchaseOrderLine.SalesTaxCodeRef = null;
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["PurchaseOrdLineSalesTaxCodeName"].ToString());
                                    if (PurchaseOrderLine.SalesTaxCodeRef.FullName == null)
                                        PurchaseOrderLine.SalesTaxCodeRef = null;
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("OverrideItemAccountFullName"))
                        {
                            #region Validations of Override Item Account Full name
                            if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                            {
                                if (dr["OverrideItemAccountFullName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Override Item Account name (" + dr["OverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                PurchaseOrderLine.OverrideItemAccountRef = null;
                                            else
                                                PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0, 3));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                                PurchaseOrderLine.OverrideItemAccountRef = null;
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                            PurchaseOrderLine.OverrideItemAccountRef = null;

                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                    if (PurchaseOrderLine.OverrideItemAccountRef.FullName == null)
                                        PurchaseOrderLine.OverrideItemAccountRef = null;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("POLineOther1"))
                        {
                            #region Validations for Other1

                            if (dr["POLineOther1"].ToString() != string.Empty)
                            {
                                if (dr["POLineOther1"].ToString().Length > 29)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Line Other1 ( " + dr["POLineOther1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString().Substring(0, 29);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.Other1 = dr["POLineOther1"].ToString();
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("POLineOther2"))
                        {
                            #region Validations for Other2

                            if (dr["POLineOther2"].ToString() != string.Empty)
                            {
                                if (dr["POLineOther2"].ToString().Length > 29)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Purchase Order Line Other2 ( " + dr["POLineOther2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString().Substring(0, 29);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                    }
                                }
                                else
                                {
                                    PurchaseOrderLine.Other2 = dr["POLineOther2"].ToString();
                                }
                            }

                            #endregion
                        }
                        DataExt.Dispose();
                        if (dt.Columns.Contains("OwnerID"))
                        {
                            #region Validations of OwnerID
                            if (dr["OwnerID"].ToString() != string.Empty)
                            {
                                PurchaseOrderLine.DataExt = DataExt.GetInstance(dr["OwnerID"].ToString(), null, null);
                                if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                {
                                    PurchaseOrderLine.DataExt = null;
                                }

                            }

                            #endregion

                        }
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName (" + dr["DataExtName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (PurchaseOrderLine.DataExt == null)
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                            else
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString().Substring(0, 31), PurchaseOrderLine.DataExt.DataExtValue);
                                            if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                            {
                                                PurchaseOrderLine.DataExt = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (PurchaseOrderLine.DataExt == null)
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                            else
                                                PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                            if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                            {
                                                PurchaseOrderLine.DataExt = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseOrderLine.DataExt == null)
                                            PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                        else
                                            PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                        if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                        {
                                            PurchaseOrderLine.DataExt = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (PurchaseOrderLine.DataExt == null)
                                        PurchaseOrderLine.DataExt = DataExt.GetInstance(null, dr["DataExtName"].ToString(), null);
                                    else
                                        PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, dr["DataExtName"].ToString(), PurchaseOrderLine.DataExt.DataExtValue);
                                    if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                    {
                                        PurchaseOrderLine.DataExt = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue"].ToString() != string.Empty)
                            {
                                if (PurchaseOrderLine.DataExt == null)
                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(null, null, dr["DataExtValue"].ToString());
                                else
                                    PurchaseOrderLine.DataExt = DataExt.GetInstance(PurchaseOrderLine.DataExt.OwnerID, PurchaseOrderLine.DataExt.DataExtName, dr["DataExtValue"].ToString());
                                if (PurchaseOrderLine.DataExt.OwnerID == null && PurchaseOrderLine.DataExt.DataExtName == null && PurchaseOrderLine.DataExt.DataExtValue == null)
                                {
                                    PurchaseOrderLine.DataExt = null;
                                }

                            }

                            #endregion

                        }

                        PurchaseOrder.PurchaseOrderLineAdd.Add(PurchaseOrderLine);

                        #endregion

                        coll.Add(PurchaseOrder);
                        #endregion

                    }
                }
                else
                {
                    return null;
                }
            }

            #endregion

            #region Customer,Item and Account Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {


                    }
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                        if (dt.Columns.Contains("VendorRefFullName"))
                        {
                            if (dr["VendorRefFullName"].ToString() != string.Empty)
                            {
                                //Axis 617 
                                string[] CurrencyArr = new string[10];
                                if (dt.Columns.Contains("Currency"))
                                {
                                    if (dr["Currency"].ToString() != string.Empty)
                                    {
                                        string Currency = dr["Currency"].ToString();

                                        CurrencyArr[0] = dr["Currency"].ToString();
                                    }
                                }
                                //Axis 617 ends

                                #region Set Vendor Query
                                XmlDocument pxmldoc = new XmlDocument();
                                pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                pxmldoc.AppendChild(qbXML);
                                XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                qbXML.AppendChild(qbXMLMsgsRq);
                                qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                XmlElement VendorQueryRq = pxmldoc.CreateElement("VendorQueryRq");
                                qbXMLMsgsRq.AppendChild(VendorQueryRq);
                                VendorQueryRq.SetAttribute("requestID", "1");
                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                FullName.InnerText = dr["VendorRefFullName"].ToString();
                                VendorQueryRq.AppendChild(FullName);

                                string pinput = pxmldoc.OuterXml;

                                string resp = string.Empty;
                                try
                                {
                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                    }

                                    else
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                }


                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                finally
                                {
                                    if (resp != string.Empty)
                                    {

                                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                        outputXMLDoc.LoadXml(resp);
                                        string statusSeverity = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorQueryRs"))
                                        {
                                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                        }
                                        outputXMLDoc.RemoveAll();
                                        if (statusSeverity == "Error" || statusSeverity == "Warn")
                                        {
                                            #region Vendor Add Query
                                            XmlDocument xmldocadd = new XmlDocument();
                                            xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                            xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                            XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                            xmldocadd.AppendChild(qbXMLcust);
                                            XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                            qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                            qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                            XmlElement VendorAddRq = xmldocadd.CreateElement("VendorAddRq");
                                            qbXMLMsgsRqcust.AppendChild(VendorAddRq);
                                            VendorAddRq.SetAttribute("requestID", "1");
                                            XmlElement VendorAdd = xmldocadd.CreateElement("VendorAdd");
                                            VendorAddRq.AppendChild(VendorAdd);
                                            XmlElement Name = xmldocadd.CreateElement("Name");
                                            Name.InnerText = dr["VendorRefFullName"].ToString();
                                            VendorAdd.AppendChild(Name);

                                            #region Adding Vendor Address.

                                            if ((dt.Columns.Contains("VendorAddr1") || dt.Columns.Contains("VendorAddr2")) || (dt.Columns.Contains("VendorAddr3") || dt.Columns.Contains("VendorAddr4")) || (dt.Columns.Contains("VendorAddr5") || dt.Columns.Contains("VendorCity")) ||
                                              (dt.Columns.Contains("VendorState") || dt.Columns.Contains("VendorPostalCode")) || (dt.Columns.Contains("VendorCountry") || dt.Columns.Contains("VendorNote") || dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email")))
                                            {
                                                XmlElement vendorAddress = xmldocadd.CreateElement("VendorAddress");
                                                VendorAdd.AppendChild(vendorAddress);
                                                if (dt.Columns.Contains("VendorAddr1"))
                                                {

                                                    if (dr["VendorAddr1"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorAdd1 = xmldocadd.CreateElement("Addr1");
                                                        VendorAdd1.InnerText = dr["VendorAddr1"].ToString();
                                                        vendorAddress.AppendChild(VendorAdd1);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorAddr2"))
                                                {
                                                    if (dr["VendorAddr2"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorAdd2 = xmldocadd.CreateElement("Addr2");
                                                        VendorAdd2.InnerText = dr["VendorAddr2"].ToString();
                                                        vendorAddress.AppendChild(VendorAdd2);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorAddr3"))
                                                {
                                                    if (dr["VendorAddr3"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorAdd3 = xmldocadd.CreateElement("Addr3");
                                                        VendorAdd3.InnerText = dr["VendorAddr3"].ToString();
                                                        vendorAddress.AppendChild(VendorAdd3);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorAddr4"))
                                                {
                                                    if (dr["VendorAddr4"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorAdd4 = xmldocadd.CreateElement("Addr4");
                                                        VendorAdd4.InnerText = dr["VendorAddr4"].ToString();
                                                        vendorAddress.AppendChild(VendorAdd4);
                                                    }

                                                }
                                                if (dt.Columns.Contains("VendorAddr5"))
                                                {
                                                    if (dr["VendorAddr5"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorAdd5 = xmldocadd.CreateElement("Addr5");
                                                        VendorAdd5.InnerText = dr["VendorAddr5"].ToString();
                                                        vendorAddress.AppendChild(VendorAdd5);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorCity"))
                                                {
                                                    if (dr["VendorCity"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorCity = xmldocadd.CreateElement("City");
                                                        VendorCity.InnerText = dr["VendorCity"].ToString();
                                                        vendorAddress.AppendChild(VendorCity);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorState"))
                                                {
                                                    if (dr["VendorState"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorState = xmldocadd.CreateElement("State");
                                                        VendorState.InnerText = dr["VendorState"].ToString();
                                                        vendorAddress.AppendChild(VendorState);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorPostalCode"))
                                                {
                                                    if (dr["VendorPostalCode"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorPostalCode = xmldocadd.CreateElement("PostalCode");
                                                        VendorPostalCode.InnerText = dr["VendorPostalCode"].ToString();
                                                        vendorAddress.AppendChild(VendorPostalCode);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorCountry"))
                                                {
                                                    if (dr["VendorCountry"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorCountry = xmldocadd.CreateElement("Country");
                                                        VendorCountry.InnerText = dr["VendorCountry"].ToString();
                                                        vendorAddress.AppendChild(VendorCountry);
                                                    }
                                                }
                                                if (dt.Columns.Contains("VendorNote"))
                                                {
                                                    if (dr["VendorNote"].ToString() != string.Empty)
                                                    {
                                                        XmlElement VendorNote = xmldocadd.CreateElement("Note");
                                                        VendorNote.InnerText = dr["VendorNote"].ToString();
                                                        vendorAddress.AppendChild(VendorNote);
                                                    }
                                                }
                                                if (dt.Columns.Contains("Phone"))
                                                {
                                                    if (dr["Phone"].ToString() != string.Empty)
                                                    {
                                                        XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                        Phone.InnerText = dr["Phone"].ToString();
                                                        VendorAdd.AppendChild(Phone);
                                                    }
                                                }
                                                if (dt.Columns.Contains("Fax"))
                                                {
                                                    if (dr["Fax"].ToString() != string.Empty)
                                                    {
                                                        XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                        Fax.InnerText = dr["Fax"].ToString();
                                                        VendorAdd.AppendChild(Fax);
                                                    }
                                                }
                                                if (dt.Columns.Contains("Email"))
                                                {
                                                    if (dr["Email"].ToString() != string.Empty)
                                                    {
                                                        XmlElement Email = xmldocadd.CreateElement("Email");
                                                        Email.InnerText = dr["Email"].ToString();
                                                        VendorAdd.AppendChild(Email);
                                                    }
                                                }

                                            }

                                            #endregion

                                            //Axis 617 
                                            #region Currency of Customer.  
                                            if (CurrencyArr[0] != null && CurrencyArr[0] != "")
                                            {
                                                XmlElement CurrencyRef = xmldocadd.CreateElement("CurrencyRef");
                                                XmlElement VendorFullName = xmldocadd.CreateElement("FullName");
                                                VendorFullName.InnerText = CurrencyArr[0];
                                                CurrencyRef.AppendChild(VendorFullName);
                                                VendorAdd.AppendChild(CurrencyRef);
                                            }
                                            #endregion
                                            //Axis 617 ends

                                            string custinput = xmldocadd.OuterXml;
                                            string respcust = string.Empty;
                                            try
                                            {
                                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                {
                                                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                    respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                }

                                                else
                                                    respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonUtilities.WriteErrorLog(ex.Message);
                                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            }
                                            finally
                                            {
                                                if (respcust != string.Empty)
                                                {
                                                    System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                    outputcustXMLDoc.LoadXml(respcust);
                                                    foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorAddRs"))
                                                    {
                                                        string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                        if (statusSeveritycust == "Error")
                                                        {
                                                            string msg = "New Supplier or Vendor could not be created into QuickBooks \n ";
                                                            msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(msg);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(msg);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                        }
                                    }

                                }

                                #endregion
                            }

                        }

                        if (dt.Columns.Contains("CustomerRefFullName"))
                        {
                            if (dr["CustomerRefFullName"].ToString() != string.Empty)
                            {
                                string customerName = dr["CustomerRefFullName"].ToString();
                                string[] arr = new string[15];
                                if (customerName.Contains(":"))
                                {
                                    arr = customerName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["CustomerRefFullName"].ToString();
                                }
                                #region Set Customer Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                        qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                        CustomerQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    CustomerQueryRq.AppendChild(FullName);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            CustomerQueryRq.AppendChild(FullName);
                                        }

                                        string pinput = pxmldoc.OuterXml;

                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {

                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                                }
                                                outputXMLDoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                {
                                                    #region Customer Add Query

                                                    XmlDocument xmldocadd = new XmlDocument();
                                                    xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                    xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                    xmldocadd.AppendChild(qbXMLcust);
                                                    XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                    qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                    qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                    XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                    qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                    CustomerAddRq.SetAttribute("requestID", "1");
                                                    XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                    CustomerAddRq.AppendChild(CustomerAdd);
                                                    XmlElement Name = xmldocadd.CreateElement("Name");
                                                    Name.InnerText = arr[i];
                                                    CustomerAdd.AppendChild(Name);

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                                CustomerAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }
                                                    #region Adding Bill Address of Customer.
                                                    if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                    {
                                                        XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                        CustomerAdd.AppendChild(BillAddress);
                                                        if (dt.Columns.Contains("BillAddr1"))
                                                        {

                                                            if (dr["BillAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                                BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                                BillAddress.AppendChild(BillAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr2"))
                                                        {
                                                            if (dr["BillAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                                BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                                BillAddress.AppendChild(BillAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr3"))
                                                        {
                                                            if (dr["BillAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                                BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                                BillAddress.AppendChild(BillAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr4"))
                                                        {
                                                            if (dr["BillAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                                BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                                BillAddress.AppendChild(BillAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr5"))
                                                        {
                                                            if (dr["BillAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                                BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                                BillAddress.AppendChild(BillAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCity"))
                                                        {
                                                            if (dr["BillCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCity = xmldocadd.CreateElement("City");
                                                                BillCity.InnerText = dr["BillCity"].ToString();
                                                                BillAddress.AppendChild(BillCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillState"))
                                                        {
                                                            if (dr["BillState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillState = xmldocadd.CreateElement("State");
                                                                BillState.InnerText = dr["BillState"].ToString();
                                                                BillAddress.AppendChild(BillState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillPostalCode"))
                                                        {
                                                            if (dr["BillPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                                BillAddress.AppendChild(BillPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCountry"))
                                                        {
                                                            if (dr["BillCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                                BillCountry.InnerText = dr["BillCountry"].ToString();
                                                                BillAddress.AppendChild(BillCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillNote"))
                                                        {
                                                            if (dr["BillNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                                BillNote.InnerText = dr["BillNote"].ToString();
                                                                BillAddress.AppendChild(BillNote);
                                                            }
                                                        }
                                                    }


                                                    #endregion

                                                    #region Adding Ship Address of Customer.

                                                    if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                      (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                    {
                                                        XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                        CustomerAdd.AppendChild(ShipAddress);
                                                        if (dt.Columns.Contains("ShipAddr1"))
                                                        {

                                                            if (dr["ShipAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                                ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr2"))
                                                        {
                                                            if (dr["ShipAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                                ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr3"))
                                                        {
                                                            if (dr["ShipAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                                ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr4"))
                                                        {
                                                            if (dr["ShipAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                                ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr5"))
                                                        {
                                                            if (dr["ShipAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                                ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCity"))
                                                        {
                                                            if (dr["ShipCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                                ShipCity.InnerText = dr["ShipCity"].ToString();
                                                                ShipAddress.AppendChild(ShipCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipState"))
                                                        {
                                                            if (dr["ShipState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipState = xmldocadd.CreateElement("State");
                                                                ShipState.InnerText = dr["ShipState"].ToString();
                                                                ShipAddress.AppendChild(ShipState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipPostalCode"))
                                                        {
                                                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                                ShipAddress.AppendChild(ShipPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCountry"))
                                                        {
                                                            if (dr["ShipCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                                ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                                ShipAddress.AppendChild(ShipCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipNote"))
                                                        {
                                                            if (dr["ShipNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                                ShipNote.InnerText = dr["ShipNote"].ToString();
                                                                ShipAddress.AppendChild(ShipNote);
                                                            }
                                                        }
                                                    }



                                                    #endregion

                                                    if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                                    {
                                                        if (dt.Columns.Contains("Phone"))
                                                        {
                                                            if (dr["Phone"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                                Phone.InnerText = dr["Phone"].ToString();
                                                                CustomerAdd.AppendChild(Phone);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Fax"))
                                                        {
                                                            if (dr["Fax"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                                Fax.InnerText = dr["Fax"].ToString();
                                                                CustomerAdd.AppendChild(Fax);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Email"))
                                                        {
                                                            if (dr["Email"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Email = xmldocadd.CreateElement("Email");
                                                                Email.InnerText = dr["Email"].ToString();
                                                                CustomerAdd.AppendChild(Email);
                                                            }
                                                        }
                                                    }

                                                    string custinput = xmldocadd.OuterXml;
                                                    string respcust = string.Empty;
                                                    try
                                                    {
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                        }

                                                        else
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    finally
                                                    {
                                                        if (respcust != string.Empty)
                                                        {
                                                            System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                            outputcustXMLDoc.LoadXml(respcust);
                                                            foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                            {
                                                                string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                                if (statusSeveritycust == "Error")
                                                                {
                                                                    string msg = "New Customer could not be created into QuickBooks \n ";
                                                                    msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";

                                                                    ErrorSummary summary = new ErrorSummary(msg);
                                                                    summary.ShowDialog();
                                                                    CommonUtilities.WriteErrorLog(oNodecust.Attributes["statusMessage"].Value.ToString());
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                            }

                                        }


                                    }

                                }
                                #endregion
                            }
                        }


                        if (dt.Columns.Contains("ItemRefFullName"))
                        {
                            #region
                            if (dr["ItemRefFullName"].ToString() != string.Empty)
                            {
                                string ItemName = dr["ItemRefFullName"].ToString();
                                string[] arr = new string[15];
                                if (ItemName.Contains(":"))
                                {
                                    arr = ItemName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ItemRefFullName"].ToString();
                                }
                                #region Setting SalesTaxCode and IsTaxIncluded
                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;
                                }
                                string TaxRateValue = string.Empty;
                                string IsTaxIncluded = string.Empty;
                                string netRate = string.Empty;
                                string ItemSaleTaxFullName = string.Empty;
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                    {
                                        if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["SalesTaxCodeRefFullName"].ToString();
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                    if (dt.Columns.Contains("IsTaxIncluded"))
                                    {
                                        if (dr["IsTaxIncluded"].ToString() != string.Empty && dr["IsTaxIncluded"].ToString() != "<None>")
                                        {
                                            int result = 0;
                                            if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                            {
                                                IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                            }
                                            else
                                            {
                                                string strvalid = string.Empty;
                                                if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                                {
                                                    IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                                else
                                                {
                                                    IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                            }
                                        }
                                    }
                                    if (dt.Columns.Contains("Rate"))
                                    {
                                        if (dr["Rate"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                            if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                            {
                                                if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                                {
                                                    decimal Rate;
                                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                                    {
                                                        netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                    }
                                                    else
                                                    {
                                                        Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            rate = Rate / (1 + (TaxRate / 100));
                                                        }
                                                        netRate = Convert.ToString(Math.Round(rate, 5));
                                                    }
                                                }
                                            }
                                            if (netRate == string.Empty)
                                            {
                                                netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dt.Columns.Contains("Rate"))
                                    {
                                        if (dr["Rate"].ToString() != string.Empty)
                                        {
                                            netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }
                                #endregion
                                #region Set Item Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        #region Passing Items Query
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                        qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                        ItemQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText = arr[item].Trim() + ":";
                                                        //arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    ItemQueryRq.AppendChild(FullName);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            ItemQueryRq.AppendChild(FullName);
                                        }
                                        string pinput = pxmldoc.OuterXml;
                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {
                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                }
                                                outputXMLDoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                {
                                                    if (defaultSettings.Type == "NonInventoryPart")
                                                    {
                                                        #region Item NonInventory Add Query
                                                        XmlDocument ItemNonInvendoc = new XmlDocument();
                                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                        ItemNonInvendoc.AppendChild(qbXMLINI);
                                                        XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                        qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                        qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                        XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                        qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                        ItemNonInventoryAddRq.SetAttribute("requestID", "1");
                                                        XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                        ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);
                                                        XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                        ININame.InnerText = arr[i];
                                                        ItemNonInventoryAdd.AppendChild(ININame);
                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                    ItemNonInventoryAdd.AppendChild(INIParent);
                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }
                                                        if (defaultSettings.TaxCode != string.Empty)
                                                        {
                                                            if (defaultSettings.TaxCode.Length < 4)
                                                            {
                                                                XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                                ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);
                                                                XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                                            }
                                                        }
                                                        XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                        bool IsPresent = false;
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemNonInvendoc.CreateElement("Desc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                INISalesAndPurchase.AppendChild(ISDesc);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Rate"))
                                                        {
                                                            if (dr["Rate"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISRate = ItemNonInvendoc.CreateElement("Price");
                                                                ISRate.InnerText = netRate;
                                                                INISalesAndPurchase.AppendChild(ISRate);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (defaultSettings.IncomeAccount != string.Empty)
                                                        {
                                                            XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                            INISalesAndPurchase.AppendChild(INIIncomeAccountRef);
                                                            XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                            INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                                            IsPresent = true;
                                                        }
                                                        if (IsPresent == true)
                                                        {
                                                            ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                        }
                                                        string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;
                                                        string respItemNonInvendoc = string.Empty;
                                                        try
                                                        {
                                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                            System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                            outputXML.LoadXml(respItemNonInvendoc);
                                                            string StatusSeverity = string.Empty;
                                                            string statusMessage = string.Empty;
                                                            foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                            {
                                                                StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                                statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                            }
                                                            outputXML.RemoveAll();
                                                            if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                            {
                                                                ErrorSummary summary = new ErrorSummary(statusMessage);
                                                                summary.ShowDialog();
                                                                CommonUtilities.WriteErrorLog(statusMessage);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            CommonUtilities.WriteErrorLog(ex.Message);
                                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                        }
                                                        string strtest2 = respItemNonInvendoc;
                                                        #endregion
                                                    }
                                                    else if (defaultSettings.Type == "Service")
                                                    {
                                                        #region Item Service Add Query
                                                        XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                        ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                        XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                        XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                        qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                        ItemServiceAddRq.SetAttribute("requestID", "1");
                                                        XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                        ItemServiceAddRq.AppendChild(ItemServiceAdd);
                                                        XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                        NameIS.InnerText = arr[i];
                                                        ItemServiceAdd.AppendChild(NameIS);
                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                    ItemServiceAdd.AppendChild(INIParent);
                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }
                                                        if (defaultSettings.TaxCode != string.Empty)
                                                        {
                                                            if (defaultSettings.TaxCode.Length < 4)
                                                            {
                                                                XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                                ItemServiceAdd.AppendChild(INISalesTaxCodeRef);
                                                                XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                                INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                                INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                            }
                                                        }
                                                        XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                        bool IsPresent = false;
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                ISSalesAndPurchase.AppendChild(ISDesc);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Rate"))
                                                        {
                                                            if (dr["Rate"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISRate = ItemServiceAdddoc.CreateElement("Price");
                                                                ISRate.InnerText = netRate;
                                                                ISSalesAndPurchase.AppendChild(ISRate);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (defaultSettings.IncomeAccount != string.Empty)
                                                        {
                                                            XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                            ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);
                                                            XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                            ISIncomeAccountRef.AppendChild(ISFullName);
                                                            IsPresent = true;
                                                        }
                                                        if (IsPresent == true)
                                                        {
                                                            ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                        }
                                                        string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                        string respItemServiceAddinputdoc = string.Empty;
                                                        try
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                            System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                            outputXML.LoadXml(respItemServiceAddinputdoc);
                                                            string StatusSeverity = string.Empty;
                                                            string statusMessage = string.Empty;
                                                            foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                            {
                                                                StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                                statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                            }
                                                            outputXML.RemoveAll();
                                                            if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                            {
                                                                ErrorSummary summary = new ErrorSummary(statusMessage);
                                                                summary.ShowDialog();
                                                                CommonUtilities.WriteErrorLog(statusMessage);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            CommonUtilities.WriteErrorLog(ex.Message);
                                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                        }
                                                        string strTest3 = respItemServiceAddinputdoc;
                                                        #endregion
                                                    }
                                                    else if (defaultSettings.Type == "InventoryPart")
                                                    {
                                                        #region Inventory Add Query
                                                        XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                        ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                        XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                        XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                        qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                        ItemInventoryAddRq.SetAttribute("requestID", "1");
                                                        XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);
                                                        XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                        NameIS.InnerText = arr[i];
                                                        ItemInventoryAdd.AppendChild(NameIS);
                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                    ItemInventoryAdd.AppendChild(INIParent);
                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }
                                                        if (defaultSettings.TaxCode != string.Empty)
                                                        {
                                                            if (defaultSettings.TaxCode.Length < 4)
                                                            {
                                                                XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                                ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);
                                                                XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                ItemInventoryAdd.AppendChild(ISDesc);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Rate"))
                                                        {
                                                            if (dr["Rate"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISRate = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                                ISRate.InnerText = netRate;
                                                                ItemInventoryAdd.AppendChild(ISRate);
                                                            }
                                                        }
                                                        if (defaultSettings.IncomeAccount != string.Empty)
                                                        {
                                                            XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                            ItemInventoryAdd.AppendChild(INIIncomeAccountRef);
                                                            XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                            INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                        }
                                                        if (defaultSettings.COGSAccount != string.Empty)
                                                        {
                                                            XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                            ItemInventoryAdd.AppendChild(INICOGSAccountRef);
                                                            XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                            INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                        }
                                                        if (defaultSettings.AssetAccount != string.Empty)
                                                        {
                                                            XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                            ItemInventoryAdd.AppendChild(INIAssetAccountRef);
                                                            XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                            INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                        }
                                                        string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                        string respItemInventoryAddinputdoc = string.Empty;
                                                        try
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                            System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                            outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                            string StatusSeverity = string.Empty;
                                                            string statusMessage = string.Empty;
                                                            foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                            {
                                                                StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                                statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                            }
                                                            outputXML.RemoveAll();
                                                            if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                            {
                                                                ErrorSummary summary = new ErrorSummary(statusMessage);
                                                                summary.ShowDialog();
                                                                CommonUtilities.WriteErrorLog(statusMessage);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            CommonUtilities.WriteErrorLog(ex.Message);
                                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                        }
                                                        string strTest4 = respItemInventoryAddinputdoc;
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            #endregion
                        }

                    }

                }
            }
            return coll;
            #endregion
        }
        #endregion
    }

}
