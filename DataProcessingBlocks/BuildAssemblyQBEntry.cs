﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using EDI.Constant;
using System.Xml;
using System.Collections.ObjectModel;
using System.Collections;
using QuickBookEntities;

namespace DataProcessingBlocks
{
   [XmlRootAttribute("BuildAssemblyQBEntry", Namespace = "", IsNullable = false)]
   public class BuildAssemblyQBEntry
    {

        #region Private Member Variable
       private InventorySiteLocationRef m_InventorySiteLocationRef;
       private ItemInventoryAssemblyRef m_ItemInventoryAssemblyRef;
       private InventorySiteRef m_InventorySiteRef;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_TxnDate;
        private string m_RefNumber;       
        private string m_Memo;
        private string m_QuantityToBuild;
        private string m_MarkPendingIfRequired;

        #endregion

        #region Constructor
        public BuildAssemblyQBEntry()
        {
        }
        #endregion

        #region Public Properties


        public ItemInventoryAssemblyRef ItemInventoryAssemblyRef
        {
            get { return m_ItemInventoryAssemblyRef; }
            set { m_ItemInventoryAssemblyRef = value; }
        }
        //public string ItemInventoryAssemblyRef
        //{
        //    get { return m_ItemInventoryAssemblyRef; }
        //    set { m_ItemInventoryAssemblyRef = value; }
        //}

        public InventorySiteRef InventorySiteRef
        {
            get { return m_InventorySiteRef; }
            set { m_InventorySiteRef = value; }
        }

        //public string InventorySiteRef
        //{
        //    get { return m_InventorySiteRef; }
        //    set { m_InventorySiteRef = value; }
        //}
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return m_InventorySiteLocationRef; }
            set { m_InventorySiteLocationRef = value; }
        }
        //public string InventorySiteLocationRef
        //{
        //    get { return m_InventorySiteLocationRef; }
        //    set { m_InventorySiteLocationRef = value; }
        //}

        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set { m_SerialNumber = value; }
        }

        public string LotNumber
        {
            get { return m_LotNumber; }
            set { m_LotNumber = value; }
        }

        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string QuantityToBuild
        {
            get { return m_QuantityToBuild; }
            set { m_QuantityToBuild = value; }
        }

        public string MarkPendingIfRequired
        {
            get { return m_MarkPendingIfRequired; }
            set { m_MarkPendingIfRequired = value; }
        }
        #endregion

        #region Public Methods

        /// This method is used to creating xml request for  export data to quickbook
        public bool ExportToQuickBooks1(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BuildAssemblyQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions          
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

           // Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement BuildAssemblyQBEntry = requestXmlDoc.CreateElement("BuildAssemblyQBEntry");
            inner.AppendChild(BuildAssemblyQBEntry);

            //Create JournalEntryAdd aggregate and fill in field values for it
           
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BuildAssemblyQBEntry"))//BuildAssemblyAddRq/BuildAssemblyAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }

            BuildAssemblyQBEntry.InnerXml = requestXML;          

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BuildAssemblyAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BuilAssemblyAddRs/BuilAssemblyRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBuildAssembly(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }



        /// This method is used to creating xml request for  export data to quickbook
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {

                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BuildAssemblyQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement BuildAssemblyAddRq = requestXmlDoc.CreateElement("BuildAssemblyAddRq");
            inner.AppendChild(BuildAssemblyAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement BuildAssemblyAdd = requestXmlDoc.CreateElement("BuildAssemblyAdd");

            BuildAssemblyAddRq.AppendChild(BuildAssemblyAdd);           

            BuildAssemblyAdd.InnerXml = requestXML;

            //For add request id to track error messages
            string requeststring = string.Empty;  
                       
            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BuildAssemblyAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBuildAssembly(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }








       /// <summary>
        /// This method is used for updating VendorQBEntry information
        /// of existing VendorQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateBuildAssemblyInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BuildAssemblyQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement BuildAssemblyQBEntryModRq = requestXmlDoc.CreateElement("BuildAssemblyModRq");
            inner.AppendChild(BuildAssemblyQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement BuildAssemblyMod = requestXmlDoc.CreateElement("BuildAssemblyMod");
            BuildAssemblyQBEntryModRq.AppendChild(BuildAssemblyMod);
           
         
          
            BuildAssemblyMod.InnerXml = requestXML;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BuildAssemblyModRq/BuildAssemblyMod"))
            {
                if (oNode.SelectSingleNode("ItemInventoryAssemblyRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BuildAssemblyModRq/BuildAssemblyMod/ItemInventoryAssemblyRef");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();
                }
            }


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BuildAssemblyModRq/BuildAssemblyMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BuildAssemblyModRq/BuildAssemblyMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BuildAssemblyModRq/BuildAssemblyMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

          
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BuildAssemblyModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BuildAssemblyModRs/BuildAssemblyRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBuildAssembly(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion


    }
   


   public class BuildAssemblyQBEntryCollection : Collection<BuildAssemblyQBEntry>
   {

   }
}
