using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using System.ComponentModel;
using EDI.Constant;
using System.Text.RegularExpressions;
namespace DataProcessingBlocks
{
    public class ImportTimeTrackingClass
    {
        private static ImportTimeTrackingClass m_ImportTimeTrackingClass;
        public bool isIgnoreAll = false;


        #region Constuctor
        public ImportTimeTrackingClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import Time Tracking class
        /// </summary>
        /// <returns></returns>
      
        public static ImportTimeTrackingClass GetInstance()
        {
            if (m_ImportTimeTrackingClass == null)
                m_ImportTimeTrackingClass = new ImportTimeTrackingClass();
            return m_ImportTimeTrackingClass;
        }

        /// <summary>
        /// This method is used for validating import time tracking data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Time Tracking QuickBooks collection </returns>
        
        public DataProcessingBlocks.TimeTrackingAddCollection ImportTimeTrackingData(string QBFileName,DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            DataProcessingBlocks.TimeTrackingAddCollection coll = new TimeTrackingAddCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    DateTime TimeTrackingDt = new DateTime();
                    string datevalue = string.Empty;
                    TimeTrackingAddEntry TimeTracking = new TimeTrackingAddEntry();

                    #region Adding TimeTracking
                    if (dt.Columns.Contains("TxnID"))
                    {
                        #region Validations of TxnID
                        if (dr["TxnID"].ToString() != string.Empty)
                        {
                            string strTxnID = dr["TxnID"].ToString();
                            if (strTxnID.Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TxnID (" + dr["TxnID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.TxnID = dr["TxnID"].ToString().Substring(0, 4000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.TxnID = dr["TxnID"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeTracking.TxnID = dr["TxnID"].ToString();
                                }
                            }
                            else
                            {
                                TimeTracking.TxnID = dr["TxnID"].ToString();
                            }
                        }
                        #endregion

                    }
                    if (dt.Columns.Contains("TxnDate"))
                    {
                        #region validations of TxnDate
                        if (dr["TxnDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["TxnDate"].ToString();
                            DateTime dttest = new DateTime();
                            if (!DateTime.TryParse(datevalue, out TimeTrackingDt))
                            {

                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TimeTracking.TxnDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeTracking.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TimeTracking.TxnDate = datevalue;
                                    }
                                }
                                else
                                {
                                    TimeTrackingDt = dttest;
                                    TimeTracking.TxnDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                TimeTracking.TxnDate = TimeTrackingDt.ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion

                    }
                    if (dt.Columns.Contains("EntityFullName"))
                    {
                        #region Validations of Entity Full name
                        if (dr["EntityFullName"].ToString() != string.Empty)
                        {
                            if (dr["EntityFullName"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Entity name (" + dr["EntityFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.EntityRef = new EntityRef(string.Empty, dr["EntityFullName"].ToString());
                                        if (TimeTracking.EntityRef.FullName == null && TimeTracking.EntityRef.ListID == null)
                                        {
                                            TimeTracking.EntityRef.FullName = null;
                                        }
                                        else
                                        {
                                            TimeTracking.EntityRef = new EntityRef(string.Empty, dr["EntityFullName"].ToString().Substring(0,1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.EntityRef = new EntityRef(string.Empty, dr["EntityFullName"].ToString());
                                        if (TimeTracking.EntityRef.FullName == null && TimeTracking.EntityRef.ListID == null)
                                        {
                                            TimeTracking.EntityRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    TimeTracking.EntityRef = new EntityRef(string.Empty, dr["EntityFullName"].ToString());
                                    if (TimeTracking.EntityRef.FullName == null && TimeTracking.EntityRef.ListID == null)
                                    {
                                        TimeTracking.EntityRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                TimeTracking.EntityRef = new EntityRef(string.Empty, dr["EntityFullName"].ToString());
                                //string strClassFullName = string.Empty;
                                if (TimeTracking.EntityRef.FullName == null && TimeTracking.EntityRef.ListID == null)
                                {
                                    TimeTracking.EntityRef.FullName = null;
                                }
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("CustomerFullName"))
                    {
                        #region Validations of Customer Full name
                        if (dr["CustomerFullName"].ToString() != string.Empty)
                        {
                            string strCust = dr["CustomerFullName"].ToString();
                            if (strCust.Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                        if (TimeTracking.CustomerRef.FullName == null)
                                        {
                                            TimeTracking.CustomerRef.FullName = null;
                                        }
                                        else
                                        {
                                            TimeTracking.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString().Substring(0,1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                        if (TimeTracking.CustomerRef.FullName == null)
                                        {
                                            TimeTracking.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    TimeTracking.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                    if (TimeTracking.CustomerRef.FullName == null)
                                    {
                                        TimeTracking.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                TimeTracking.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                //string strCustomerFullname = string.Empty;
                                if (TimeTracking.CustomerRef.FullName == null)
                                {
                                    TimeTracking.CustomerRef.FullName = null;
                                }
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("ItemServiceFullName"))
                    {
                        #region Validations of ItemService Full name
                        if (dr["ItemServiceFullName"].ToString() != string.Empty)
                        {
                            if (dr["ItemServiceFullName"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemService full name (" + dr["ItemServiceFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.ItemServiceRef = new ItemServiceRef(dr["ItemServiceFullName"].ToString());
                                        if (TimeTracking.ItemServiceRef.FullName == null)
                                            TimeTracking.ItemServiceRef.FullName = null;
                                        else
                                            TimeTracking.ItemServiceRef = new ItemServiceRef(dr["ItemServiceFullName"].ToString().Substring(0,1000));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.ItemServiceRef = new ItemServiceRef(dr["ItemServiceFullName"].ToString());
                                        if (TimeTracking.ItemServiceRef.FullName == null)
                                            TimeTracking.ItemServiceRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    TimeTracking.ItemServiceRef = new ItemServiceRef(dr["ItemServiceFullName"].ToString());
                                    if (TimeTracking.ItemServiceRef.FullName == null)
                                        TimeTracking.ItemServiceRef.FullName = null;
                                }
                            }
                            else
                            {
                                TimeTracking.ItemServiceRef = new ItemServiceRef(dr["ItemServiceFullName"].ToString());
                                if (TimeTracking.ItemServiceRef.FullName == null)
                                    TimeTracking.ItemServiceRef.FullName = null;
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("Duration"))
                    {
                        #region Validations of Duration
                        if (dr["Duration"].ToString() != string.Empty)
                        {
                            string strDuration = string.Empty;
                            string strTime = string.Empty;
                            string value = string.Empty;
                            string time = "";
                            try
                            {

                                strDuration = dr["Duration"].ToString();
                                if (strDuration.Contains("."))
                                {
                                    string[] strsep = strDuration.Split('.');
                                    int temp = Convert.ToInt32(strsep[0]);
                                   

                                    //if (temp > 24)
                                    //{
                                    //    //temp = temp - 24;
                                    //    //  strsep[0] = temp.ToString();
                                    //    time = strsep[0];
                                    //    //strDuration = "0." + strsep[1];
                                    //    strDuration = temp + "." + strsep[1];
                                    //}
                                    //else
                                    //{
                                          strDuration = strsep[0] + "." + strsep[1];
                                //    }
                                    // strDuration = value;
                                    dr["Duration"] = strDuration.ToString();
                                }

                                DateTime dtt = new DateTime();
                                if (!DateTime.TryParse(dr["Duration"].ToString(), out dtt))
                                {
                                    //TimeTracking.Duration = dr["Duration"].ToString();
                                    // strDuration = dr["Duration"].ToString();

                                    if (strDuration.Contains("PT"))
                                    {
                                        TimeTracking.Duration = strDuration;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strDuration.Contains(":"))
                                        {
                                            string[] strsep = strDuration.Split(':');


                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";

                                                strAssign += strsep[1].ToString() + "M";



                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            TimeTracking.Duration = strAssign;
                                        }
                                        else if (strDuration.Contains("."))
                                        {
                                            string[] strsep = strDuration.Split('.');
                                            string minval = "";
                                            decimal convertedmin = 0;
                                            if (strsep[1].Length == 1)
                                            {
                                                minval = strsep[1].ToString();
                                                convertedmin = Convert.ToDecimal(minval) * 6;
                                                strsep[1] = convertedmin.ToString();
                                            }
                                            else
                                            {

                                                minval = strsep[1].ToString().Substring(0, 2);

                                                //   int min = Convert.ToInt32(minval);
                                                //before 613
                                                //decimal min = (Convert.ToDecimal(minval)) / 10;
                                                //613
                                                decimal min = (Convert.ToDecimal(minval)) / 100;

                                                //int min1 = min / 100;
                                                // int convertedmin = ((min * 60) / 100);
                                                //before 613
                                                //convertedmin = (min * 6) / 100;
                                                //613
                                                //convertedmin = (min * 60);
                                                convertedmin = Convert.ToDecimal(Math.Round(min * 60));
                                                


                                                strsep[1] = convertedmin.ToString();
                                                //before 613
                                                //strsep[1] = strsep[1].ToString().Substring(0, 4);
                                                //string dd = strsep[1].ToString().Substring(0, 4);
                                                //strsep[1] = dd;

                                            }
                                            if (strsep.Length == 2)
                                            {
                                              // strAssign += strsep[0].ToString() + "H";
                                                if (time == "")
                                                {

                                                    if (Convert.ToInt32(strsep[0]) > 0)
                                                    {
                                                        strAssign += strsep[0] + "H";
                                                    }
                                                    else
                                                    {
                                                        strAssign += time + "0H";
                                                    }
                                                }
                                                else
                                                {
                                                    strAssign += time + "H";
                                                }
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                              //  strAssign += strsep[0].ToString() + "H";
                                                strAssign += time + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            time = "";
                                            TimeTracking.Duration = strAssign;
                                        }
                                        else
                                        {
                                            TimeTracking.Duration= strAssign + strDuration + "H0M";
                                        }

                                    }
                                }
                                else if (DateTime.TryParse(dr["Duration"].ToString(), out dtt))
                                {
                                    //TimeTracking.Duration = dr["Duration"].ToString();
                                    strDuration = dr["Duration"].ToString();
                                    if (strDuration.Contains("PT"))
                                    {
                                        TimeTracking.Duration = strDuration;
                                    }
                                    else
                                    {
                                        string strAssign = "PT";
                                        if (strDuration.Contains(":"))
                                        {
                                            string[] strsep = strDuration.Split(':');
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            TimeTracking.Duration = strAssign;
                                        }
                                        else if (strDuration.Contains("."))
                                        {
                                            string[] strsep = strDuration.Split('.');
                                            string minval = "";
                                            decimal convertedmin = 0;
                                            if (strsep[1].Length == 1)
                                            {
                                                minval = strsep[1].ToString();
                                                convertedmin = Convert.ToDecimal(minval) * 6;
                                                strsep[1] = convertedmin.ToString();
                                            }
                                            else
                                            {

                                                minval = strsep[1].ToString().Substring(0, 2);

                                                //   int min = Convert.ToInt32(minval);
                                                //before 613
                                                //decimal min = (Convert.ToDecimal(minval)) / 10;
                                                //613
                                                decimal min = (Convert.ToDecimal(minval)) / 100;
                                                //int min1 = min / 100;
                                                // int convertedmin = ((min * 60) / 100);\
                                                //before 613
                                                //convertedmin = (min * 6) / 100;
                                                //613
                                                convertedmin = Convert.ToDecimal(Math.Round(min * 60));
                                                

                                                strsep[1] = convertedmin.ToString();
                                                //before 613
                                                //strsep[1] = strsep[1].ToString().Substring(0, 4);
                                                //string dd = strsep[1].ToString().Substring(0, 4);
                                                //strsep[1] = dd;
                                                
                                            }
                                            
                                            if (strsep.Length == 2)
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                            }
                                            else
                                            {
                                                strAssign += strsep[0].ToString() + "H";
                                                strAssign += strsep[1].ToString() + "M";
                                                strAssign += strsep[2].ToString() + "S";
                                            }
                                            strsep = null;
                                            TimeTracking.Duration = strAssign;
                                        }
                                        else
                                        {
                                            TimeTracking.Duration = strDuration;
                                        }
                                        //TimeTracking.Duration = strAssign;
                                    }

                                }
                                else
                                {

                                    //strTime = dr["Duration"].ToString();
                                    //dtt = DateTime.ParseExact(strTime," dd.MM.yyyy HH:mm:ss",CultureInfo.InvariantCulture);
                                    strDuration = dtt.ToString("HH:mm:ss");
                                    strDuration = strTime;
                                    string strAssign = "PT";
                                    string[] strsep = strDuration.Split(':');


                                    strAssign += strsep[0].ToString() + "H";
                                    strAssign += strsep[1].ToString() + "M";
                                    strAssign += strsep[2].ToString() + "S";
                                    TimeTracking.Duration = strAssign;
                                }
                            }
                            catch
                            {
                                //if (isIgnoreAll == false)
                                //{
                                //    string strMessages = "This Duration (" + strDuration + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                //    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                //    if (Convert.ToString(result) == "Cancel")
                                //    {
                                //        continue;
                                //    }
                                //    if (Convert.ToString(result) == "No")
                                //    {
                                //        return null;
                                //    }
                                //    if (Convert.ToString(result) == "Ignore")
                                //    {
                                //        TimeTracking.Duration = dr["Duration"].ToString();
                                //    }
                                //    if (Convert.ToString(result) == "Abort")
                                //    {
                                //        isIgnoreAll = true;
                                //        TimeTracking.Duration = dr["Duration"].ToString();
                                //    }
                                //}
                                //else
                                //    TimeTracking.Duration = dr["Duration"].ToString();

                            }

                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("ClassFullName"))
                    {
                        #region Validations of Class Full name
                        if (dr["ClassFullName"].ToString() != string.Empty)
                        {
                            if (dr["ClassFullName"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class name (" + dr["ClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.ClassRef = new ClassRef(dr["ClassFullName"].ToString());
                                        if (TimeTracking.ClassRef.FullName == null)
                                        {
                                            TimeTracking.ClassRef.FullName = null;
                                        }
                                        else
                                        {
                                            TimeTracking.ClassRef = new ClassRef(dr["ClassFullName"].ToString().Substring(0,1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.ClassRef = new ClassRef(dr["ClassFullName"].ToString());
                                        if (TimeTracking.ClassRef.FullName == null)
                                        {
                                            TimeTracking.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    TimeTracking.ClassRef = new ClassRef(dr["ClassFullName"].ToString());
                                    if (TimeTracking.ClassRef.FullName == null)
                                    {
                                        TimeTracking.ClassRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                TimeTracking.ClassRef = new ClassRef(dr["ClassFullName"].ToString());
                                //string strClassFullName = string.Empty;
                                if (TimeTracking.ClassRef.FullName == null)
                                {
                                    TimeTracking.ClassRef.FullName = null;
                                }
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("PayrollItemWageFullName"))
                    {
                        #region Validations of PayrollItemWage Full name
                        if (dr["PayrollItemWageFullName"].ToString() != string.Empty)
                        {
                            if (dr["PayrollItemWageFullName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PayrollItemWage name (" + dr["PayrollItemWageFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.PayrollItemWageRef = new PayrollItemWageRef(dr["PayrollItemWageFullName"].ToString());
                                        if (TimeTracking.PayrollItemWageRef.FullName == null)
                                        {
                                            TimeTracking.PayrollItemWageRef.FullName = null;
                                        }
                                        else
                                        {
                                            TimeTracking.PayrollItemWageRef = new PayrollItemWageRef(dr["PayrollItemWageFullName"].ToString().Substring(0,100));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.PayrollItemWageRef = new PayrollItemWageRef(dr["PayrollItemWageFullName"].ToString());
                                        if (TimeTracking.PayrollItemWageRef.FullName == null)
                                        {
                                            TimeTracking.PayrollItemWageRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    TimeTracking.PayrollItemWageRef = new PayrollItemWageRef(dr["PayrollItemWageFullName"].ToString());
                                    if (TimeTracking.PayrollItemWageRef.FullName == null)
                                    {
                                        TimeTracking.PayrollItemWageRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                TimeTracking.PayrollItemWageRef = new PayrollItemWageRef(dr["PayrollItemWageFullName"].ToString());
                                //string strClassFullName = string.Empty;
                                if (TimeTracking.PayrollItemWageRef.FullName == null)
                                {
                                    TimeTracking.PayrollItemWageRef.FullName = null;
                                }
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("Notes"))
                    {

                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            string strNotes = dr["Notes"].ToString();
                            if (strNotes.Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["Notes"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        TimeTracking.Notes = dr["Notes"].ToString().Substring(0,4000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        TimeTracking.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                {
                                    TimeTracking.Notes = dr["Notes"].ToString();
                                }
                            }
                            else
                            {
                                TimeTracking.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillableStatus"))
                    {
                        #region validations of Billable Status
                        if (dr["BillableStatus"].ToString() != string.Empty)
                        {
                            try
                            {
                                TimeTracking.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["BillableStatus"].ToString(), true));
                            }
                            catch
                            {
                                TimeTracking.BillableStatus = dr["BillableStatus"].ToString();
                            }
                        }
                        #endregion
                    }
                  
                    if (dt.Columns.Contains("IsBillable"))
                    {
                        #region Validations of IsBillable
                        if (dr["IsBillable"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsBillable"].ToString(), out result))
                            {
                                TimeTracking.IsBillable = Convert.ToInt32(dr["IsBillable"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsBillable"].ToString().ToLower() == "true")
                                {
                                    TimeTracking.IsBillable = dr["IsBillable"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsBillable"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        TimeTracking.IsBillable = dr["IsBillable"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsBillable (" + dr["IsBillable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(results) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            TimeTracking.IsBillable = dr["IsBillable"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TimeTracking.IsBillable = dr["IsBillable"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        TimeTracking.IsBillable = dr["IsBillable"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    coll.Add(TimeTracking);

                    #endregion
                }
                else
                {
                    return null;
                }

            }
            #endregion

            #region Customer,Item and Account Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                    if (dt.Columns.Contains("CustomerFullName"))
                    {
                        if (dr["CustomerFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Customer Name contains ":"
                            string customerName = dr["CustomerFullName"].ToString();
                            string[] arr1 = new string[15];
                            if (customerName.Contains(":"))
                            {
                                arr1 = customerName.Split(':');
                            }
                            else
                            {
                                arr1[0] = dr["CustomerFullName"].ToString();
                            }

                            for (int i = 0; i < arr1.Length; i++)
                            {
                                if (arr1[i] != null && arr1[i] != string.Empty)
                                {

                                    #region Set Customer Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                    qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                    CustomerQueryRq.SetAttribute("requestID", "1");
                                    XmlElement FullName = pxmldoc.CreateElement("FullName");
                                    //FullName.InnerText = dr["CustomerRefFullName"].ToString();
                                    FullName.InnerText = dr["CustomerFullName"].ToString();
                                    CustomerQueryRq.AppendChild(FullName);
                                    //XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                                    //CustomerQueryRq.AppendChild(ActiveStatus).InnerText = "All";
                                    string pinput = pxmldoc.OuterXml;
                                    //pxmldoc.Save("C://Test.xml");
                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                                        }
                                        else
                                        {
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);

                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {

                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {

                                                #region Customer Add Query

                                                XmlDocument xmldocadd = new XmlDocument();
                                                xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                xmldocadd.AppendChild(qbXMLcust);
                                                XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                CustomerAddRq.SetAttribute("requestID", "1");
                                                XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                CustomerAddRq.AppendChild(CustomerAdd);
                                                XmlElement Name = xmldocadd.CreateElement("Name");
                                                //Name.InnerText = dr["CustomerRefFullName"].ToString();
                                                Name.InnerText = arr1[i];
                                                CustomerAdd.AppendChild(Name);

                                                if (i > 0 && i <= 2)
                                                {
                                                    if (arr1[i] != null && arr1[i] != string.Empty)
                                                    {
                                                        //Adding Parent
                                                        XmlElement custParent = xmldocadd.CreateElement("ParentRef");
                                                        CustomerAdd.AppendChild(custParent);

                                                        if (i == 2)
                                                        {
                                                            XmlElement custChildFullName = xmldocadd.CreateElement("FullName");
                                                            custChildFullName.InnerText = arr1[i - 2] + ":" + arr1[i - 1];
                                                            custParent.AppendChild(custChildFullName);
                                                        }
                                                        else if (i == 1)
                                                        {
                                                            XmlElement custChildFullName = xmldocadd.CreateElement("FullName");
                                                            custChildFullName.InnerText = arr1[i - 1];
                                                            custParent.AppendChild(custChildFullName);
                                                        }

                                                    }
                                                }
                                                #region Adding Bill Address of Customer.
                                                if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                    (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                {
                                                    XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                    CustomerAdd.AppendChild(BillAddress);
                                                    if (dt.Columns.Contains("BillAddr1"))
                                                    {

                                                        if (dr["BillAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                            BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                            BillAddress.AppendChild(BillAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr2"))
                                                    {
                                                        if (dr["BillAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                            BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                            BillAddress.AppendChild(BillAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr3"))
                                                    {
                                                        if (dr["BillAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                            BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                            BillAddress.AppendChild(BillAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr4"))
                                                    {
                                                        if (dr["BillAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                            BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                            BillAddress.AppendChild(BillAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr5"))
                                                    {
                                                        if (dr["BillAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                            BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                            BillAddress.AppendChild(BillAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCity"))
                                                    {
                                                        if (dr["BillCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCity = xmldocadd.CreateElement("City");
                                                            BillCity.InnerText = dr["BillCity"].ToString();
                                                            BillAddress.AppendChild(BillCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillState"))
                                                    {
                                                        if (dr["BillState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillState = xmldocadd.CreateElement("State");
                                                            BillState.InnerText = dr["BillState"].ToString();
                                                            BillAddress.AppendChild(BillState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillPostalCode"))
                                                    {
                                                        if (dr["BillPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                            BillAddress.AppendChild(BillPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCountry"))
                                                    {
                                                        if (dr["BillCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                            BillCountry.InnerText = dr["BillCountry"].ToString();
                                                            BillAddress.AppendChild(BillCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillNote"))
                                                    {
                                                        if (dr["BillNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                            BillNote.InnerText = dr["BillNote"].ToString();
                                                            BillAddress.AppendChild(BillNote);
                                                        }
                                                    }

                                                }

                                                #endregion

                                                #region Adding Ship Address of Customer.

                                                if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                  (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                {
                                                    XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                    CustomerAdd.AppendChild(ShipAddress);
                                                    if (dt.Columns.Contains("ShipAddr1"))
                                                    {

                                                        if (dr["ShipAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                            ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr2"))
                                                    {
                                                        if (dr["ShipAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                            ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr3"))
                                                    {
                                                        if (dr["ShipAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                            ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr4"))
                                                    {
                                                        if (dr["ShipAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                            ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr5"))
                                                    {
                                                        if (dr["ShipAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                            ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCity"))
                                                    {
                                                        if (dr["ShipCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                            ShipCity.InnerText = dr["ShipCity"].ToString();
                                                            ShipAddress.AppendChild(ShipCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipState"))
                                                    {
                                                        if (dr["ShipState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipState = xmldocadd.CreateElement("State");
                                                            ShipState.InnerText = dr["ShipState"].ToString();
                                                            ShipAddress.AppendChild(ShipState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipPostalCode"))
                                                    {
                                                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                            ShipAddress.AppendChild(ShipPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCountry"))
                                                    {
                                                        if (dr["ShipCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                            ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                            ShipAddress.AppendChild(ShipCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipNote"))
                                                    {
                                                        if (dr["ShipNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                            ShipNote.InnerText = dr["ShipNote"].ToString();
                                                            ShipAddress.AppendChild(ShipNote);
                                                        }
                                                    }
                                                }

                                                #endregion

                                                string custinput = xmldocadd.OuterXml;
                                                string respcust = string.Empty;
                                                try
                                                {
                                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                    {
                                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);
                                                    }
                                                    else
                                                    {
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                finally
                                                {
                                                    if (respcust != string.Empty)
                                                    {
                                                        System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                        outputcustXMLDoc.LoadXml(respcust);
                                                        foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                        {
                                                            string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                            if (statusSeveritycust == "Error")
                                                            {
                                                                string msg = "New Customer could not be created into QuickBooks \n ";
                                                                msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                //MessageBox.Show(msg);
                                                                //Task 1435 (Axis 6.0):
                                                                ErrorSummary summary = new ErrorSummary(msg);
                                                                summary.ShowDialog();
                                                                CommonUtilities.WriteErrorLog(msg);
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }
                        }
                    }


                    //version 6.0 code for Employee creation
                    if (dt.Columns.Contains("EntityFullName"))
                    {
                        //Code to check whether Employee Name contains " "
                        
                        string employeeName = dr["EntityFullName"].ToString().Trim();
                        employeeName = Regex.Replace(employeeName,@"\s+"," ");
                        string[] employeeArray = null;

                        if (employeeName.Contains(" "))
                        {
                            employeeArray = employeeName.Split(' ');
                        }
                        else
                        {
                            employeeArray = new string[1];
                            employeeArray[0] = employeeName;
                        }

                        #region Set Employee
                        XmlDocument pxmldoc = new XmlDocument();
                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                        pxmldoc.AppendChild(qbXML);
                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                        qbXML.AppendChild(qbXMLMsgsRq);
                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                        XmlElement EmployeeQueryRq = pxmldoc.CreateElement("EmployeeQueryRq");
                        qbXMLMsgsRq.AppendChild(EmployeeQueryRq);
                        EmployeeQueryRq.SetAttribute("requestID", "1");
                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                        FullName.InnerText = dr["EntityFullName"].ToString();
                        EmployeeQueryRq.AppendChild(FullName);


                        string pinput = pxmldoc.OuterXml;

                        string resp = string.Empty;
                        try
                        {
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                            }
                            else
                            {
                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                            }
                        }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                        }
                        finally
                        {
                            if (resp != string.Empty)
                            {

                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                outputXMLDoc.LoadXml(resp);
                                string statusSeverity = string.Empty;
                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/EmployeeQueryRs"))
                                {
                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                }
                                outputXMLDoc.RemoveAll();
                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                {
                                    #region Employee Add Query
                                    XmlDocument xmldocadd = new XmlDocument();
                                    


                                    //XmlDocument xmldocadd = new XmlDocument();
                                    xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                    xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                    xmldocadd.AppendChild(qbXMLcust);
                                    XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                    qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                    qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                    XmlElement EmployeeAddRq = xmldocadd.CreateElement("EmployeeAddRq");
                                    qbXMLMsgsRqcust.AppendChild(EmployeeAddRq);
                                    EmployeeAddRq.SetAttribute("requestID", "1");
                                    XmlElement EmployeeAdd = xmldocadd.CreateElement("EmployeeAdd");
                                    EmployeeAddRq.AppendChild(EmployeeAdd);
                                    for (int i = 0; i < 3; i++)
                                    {
                                        //For first Name
                                        if (i == 0 )
                                        {
                                            XmlElement Name = xmldocadd.CreateElement("FirstName");
                                            Name.InnerText = employeeArray[i].ToString();
                                            EmployeeAdd.AppendChild(Name);
                                        }

                                        //For Middle Name
                                        if (i == 1 && employeeArray.Length>2)
                                        {
                                            XmlElement Name = xmldocadd.CreateElement("MiddleName");
                                            Name.InnerText = employeeArray[i].ToString();
                                            EmployeeAdd.AppendChild(Name);
                                        }
                                        else if(i==1 && employeeArray.Length>1)
                                        {
                                            XmlElement Name = xmldocadd.CreateElement("LastName");
                                            Name.InnerText = employeeArray[i].ToString();
                                            EmployeeAdd.AppendChild(Name);
                                        }

                                        //for last name
                                        if (i == 2 && employeeArray.Length>2)
                                        {
                                            XmlElement Name = xmldocadd.CreateElement("LastName");
                                            Name.InnerText = employeeArray[i].ToString();
                                            EmployeeAdd.AppendChild(Name);
                                        }
                                    }

                                    XmlElement EmployeePayrollInfo = xmldocadd.CreateElement("EmployeePayrollInfo");
                                    EmployeeAdd.AppendChild(EmployeePayrollInfo);
                                    XmlElement UseTimeDataToCreatePaychecks = xmldocadd.CreateElement("UseTimeDataToCreatePaychecks");
                                    UseTimeDataToCreatePaychecks.InnerText = "DoNotUseTimeData";
                                    EmployeePayrollInfo.AppendChild(UseTimeDataToCreatePaychecks);
                                    string custinput = xmldocadd.OuterXml;
                                    string respcust = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);
                                        }
                                        else
                                        {
                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue,custinput);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (respcust != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                            outputcustXMLDoc.LoadXml(respcust);
                                            foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/EmployeeAddRs"))
                                            {
                                                string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                if (statusSeveritycust == "Error")
                                                {
                                                    CommonUtilities.WriteErrorLog(oNodecust.Attributes["statusMessage"].Value.ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                                    #endregion
                            }

                        }

                        #endregion
                    }



                    //Solution for BUG 633
                    if (dt.Columns.Contains("ItemServiceFullName"))
                    {
                        if (dr["ItemServiceFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemServiceFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemServiceFullName"].ToString();
                            }

                            #region Set Item Query


                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemServiceQueryRq = pxmldoc.CreateElement("ItemServiceQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemServiceQueryRq);
                                    ItemServiceQueryRq.SetAttribute("requestID", "1");

                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");


                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemServiceQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        //FullName.InnerText = dr["ItemServiceFullName"].ToString();
                                        ItemServiceQueryRq.AppendChild(FullName);
                                    }

                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                           CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                                        }
                                        else
                                        {
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {
                                                //statusMessage += "\n ";
                                                //statusMessage += oNode.Attributes["statusMessage"].Value.ToString();

                                                if (defaultSettings.Type == "Service")
                                                {
                                                    #region Item Service Add Query

                                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemServiceFullName"].ToString();
                                                    ItemServiceAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }

                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                        }
                                                    }


                                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                        //Adding IncomeAccount FullName.
                                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                    //ItemServiceAdddoc.Save("C://ItemServiceAdddoc.xml");
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenMultiUser);

                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemServiceAddinput);
                                                        }
                                                        //End Changes

                                                        
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || statusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);                                                            
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }
                }
            }
                else
                {
                    return null;
                }
            }
            #endregion

            return coll;

        }
    }
}