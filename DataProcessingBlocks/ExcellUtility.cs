using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.Linq;
using System.Data;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using EDI.Constant;
using System.Xml;
using TransactionImporter;
using System.Diagnostics;
using Tamir.SharpSsh.java.lang;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Diagnostics.Contracts;
using LumenWorks.Framework.IO.Csv;
using GemBox.Spreadsheet;
using System.Collections;
using LinqToSqlShared;
using CsvFile;
using System.Linq;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;


namespace DataProcessingBlocks
{
    /// <summary>
    /// This Class provides the utility to operate on Excel sheets
    /// </summary>
    public class ExcellUtility
    {
        //615
        internal static List<string[]> trnlst = new List<string[]>();

        TransactionImporter.TransactionImporter ti = new TransactionImporter.TransactionImporter();
        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        #region Private Constants
        private const string connectionStringFormat = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0}; Extended Properties='Excel 8.0; IMEX=1; HDR={1}'";
        //private const string connectionStringFormat = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0}; Extended Properties=Excel 8.0;";
        private const string connectionStringFormatXLSX = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties='Excel 12.0 Xml; IMEX=1; HDR={1}'";
        private const string connectionStringXLS = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source={0}; Extended Properties='Excel 12.0; IMEX=1; HDR={1}'";
        private const string connectionStringCSV = @"Provider=Microsoft.Jet.OLEDB.4.0; Data Source={0};Extended Properties=""text;HDR={1};FMT={2};""";
        private const string tableNames = "TABLE_NAME";
        private const string SELECTQUERY = "SELECT * FROM [{0}$]";
        private const string SELECTFILEQUERY = "SELECT * FROM [{0}]";
        private const string SELECTCOLUMNQUERY = "SELECT TOP 1 * FROM [{0}]";
        //private const string SELECTCOLUMNQUERY = "SELECT * FROM [{0}]";

        private const string COLUMNNAME = "COLUMN_NAME";
        private const string DEFUALTCOLNAME = "Column";
        private const string DefaultError = "The process cannot access the file";

        private const string ERROR_STRING = "The 'Microsoft.ACE.OLEDB.12.0' provider is not registered on the local machine";
        string ParentTag = string.Empty;
        List<string> xmllist = new List<string>();
        
        #endregion

        #region Private Members
        private static ExcellUtility m_ExcellUtility;
        string str;
        public string m_filePath;
        private string m_CurrentSheetname;
      
        //615
        private string m_CurrentIIFtransName;
        private bool m_HDR = false;
        private int m_Datanumber = 0;
        // new change..
        public System.Data.DataTable textDataFromSheet;
        List<object> result = new List<object>();
        private string m_FMT = string.Empty;
        public string Err = string.Empty;
        public static string strXlsSheet;

        public int callDoEventsOn = 1000;

        // Axis 10 .0 
        XmlNodeList nodeList;

        //Axis 12.0
        protected char[] SpecialChars = new char[] { ',', '"', '\r', '\n' };

        // Indexes into SpecialChars for characters with specific meaning
        private const int DelimiterIndex = 0;
        private const int QuoteIndex = 1;

        /// <summary>
        /// Gets/sets the character used for column delimiters.
        /// </summary>
        public char Delimiter
        {
            get { return SpecialChars[DelimiterIndex]; }
            set { SpecialChars[DelimiterIndex] = value; }
        }

        /// <summary>
        /// Gets/sets the character used for column quotes.
        /// </summary>
        public char Quote
        {
            get { return SpecialChars[QuoteIndex]; }
            set { SpecialChars[QuoteIndex] = value; }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets Or Sets the file path on which operations to be done.
        /// </summary>
        /// 
        //615
        public string Name { get; set; }

        //615
        public string CurrentIIFtransName
        {
            get { return this.m_CurrentIIFtransName; }
            set { this.m_CurrentIIFtransName = value; }
        }

        public string FilePath
        {
            get { return this.m_filePath; }
            set { this.m_filePath = value; }
        }

        public string ERR
        {
            get { return this.Err; }
            set { this.Err = value; }
        }
        public bool HDR
        {
            get
            {
                return this.m_HDR;
            }
            set { this.m_HDR = value; }
        }

        public string FMT
        {
            get
            {
                return this.m_FMT;
            }
            set
            {
                this.m_FMT = value;
            }

        }

        public string CurrentSheetName
        {
            get
            {
                return this.m_CurrentSheetname;
            }
            set
            {
                this.m_CurrentSheetname = value;
            }
        }

        public int DataNumber
        {
            get
            {
                return this.m_Datanumber;
            }
            set
            {
                this.m_Datanumber = value;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// ParameterLess constructor for ExcellUtility
        /// </summary>
        public ExcellUtility()
        {

        }

        #endregion

        #region Static Methods
        /// <summary>
        /// Get current instance fo Excell Utility Object.
        /// </summary>
        /// <returns></returns>
        public static ExcellUtility GetInstance()
        {
            if (m_ExcellUtility == null)
                m_ExcellUtility = new ExcellUtility();
            return m_ExcellUtility;
        }

        /// <summary>
        /// Creates new instance of Excell Utility. And makes this as current instance.
        /// </summary>
        /// <returns></returns>
        public static ExcellUtility CreateInstance()
        {
            m_ExcellUtility = new ExcellUtility();
            return m_ExcellUtility;
        }


        /// <summary>
        /// Crreates new instance of the ExcellUtitlity Object with the file path. And makes this as the current instance.
        /// </summary>
        /// <param name="filePath">file path of the excell file.</param>
        /// <returns></returns>
        public static ExcellUtility CreateInstance(string filePath)
        {
            m_ExcellUtility = new ExcellUtility(filePath);
            return m_ExcellUtility;
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Excell utility with the file path.
        /// All the operations will be done on this file
        /// </summary>
        /// <param name="filePath">file Path</param>
        public ExcellUtility(string filePath)
        {
		///615
           // filePath = ti.openFileDialogBrowseFile.FileName;
            this.m_filePath = filePath;
        }

        /// <summary>
        /// This mehtod retrieves the excel sheet names from 
        /// an excel workbook.
        /// </summary>
        /// <returns>String[]</returns>
        public System.Collections.ObjectModel.Collection<string> GetExcelSheetNames()
        {
            if ((System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".iif") && (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".txt"))
            {

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".xml")
                {
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".qif")
                    {
                        if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".qfx")
                        {
                            if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".ofx")
                            {
                                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() != ".qbo")
                                {
                                    //633
                                    if (this.m_filePath != "")
                                    {
                                        try
                                        {
                                            //637
                                            SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");
                                            ExcelFile ef = ExcelFile.Load(this.m_filePath);
                                            StringBuilder sb = new StringBuilder();

                                        System.Collections.ObjectModel.Collection<string> excelSheets = new System.Collections.ObjectModel.Collection<string>();

                                        foreach (ExcelWorksheet sheet in ef.Worksheets)
                                        {
                                            if (sheet.CalculateMaxUsedColumns() != 0)
                                            {

                                                if (sheet.Name != null)
                                                {
                                                    //if (!sheet.Name.ToString().Contains("#"))
                                                    {
                                                        if (!excelSheets.Contains(sheet.Name.ToString().Replace("'", string.Empty).Replace("$", string.Empty)))
                                                            sb.AppendFormat("{0}({1})", sheet.Name, sheet.Name.GetType().Name);
                                                        excelSheets.Add(sheet.Name.ToString().Replace("'", string.Empty).Replace("$", string.Empty));

                                                    }
                                                }
                                            }

                                        }
                                        return excelSheets;
                                    }
                                    catch (OleDbException OleEx)
                                    {
                                        if (OleEx.Message.Contains(ERROR_STRING))
                                        {

                                            //CommonUtilities.WriteErrorLog(OleEx.Message);
                                            //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                                            throw new TIException("Zed Axis ER004");
                                        }
                                        else if (OleEx.ErrorCode == -2147467259)
                                        {
                                            //CommonUtilities.WriteErrorLog(OleEx.Message);
                                            //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                                            throw new TIException("Zed Axis ER003");
                                        }
                                        else
                                        {
                                            //CommonUtilities.WriteErrorLog(OleEx.Message);
                                            //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                                            throw new TIException("Zed Axis ER001");
                                        }

                                        }
                                        catch (Exception ex)
                                        {
                                            //CommonUtilities.WriteErrorLog(ex.Message);
                                            //CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            throw new TIException("Zed Axis ER001");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;

        }


        public System.Collections.ObjectModel.Collection<string> GetIIFSheetNames()
        {
            System.Collections.ObjectModel.Collection<string> trnsTypeList = new System.Collections.ObjectModel.Collection<string>();

            try
            {
           
                TransactionImporter.TransactionImporter tni = new TransactionImporter.TransactionImporter();
           // List<string> trnsTypeList = new List<string>();
             textDataFromSheet = null;
            int transCount = 0;
            int splCount = 0;
            int gemboxused = 0;
            // int transCount = 0;
            OleDbConnection oleDBConn = null;
           
                // string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");

            string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                string exfile = this.m_filePath;
               
                //This is used for IIF file parsing logic. So that we can import those files.

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                    if (this.m_FMT != "Fixed")
                    {
                        textDataFromSheet = new System.Data.DataTable();
                        char[] delimiter = new char[1];
                        if (this.m_FMT == "TabDelimited")
                            delimiter[0] = '\t';
                        if (this.m_FMT == "CSVDelimited")                       
                            delimiter[0] = ',';                        
                        if (this.m_FMT == "Delimited(|)")
                            delimiter[0] = '|';
                        string[] lines = File.ReadAllLines(this.m_filePath);
                        ///
                        for (int CmDel = 0; CmDel < lines.Length; CmDel++)
                        {
                            if (lines[CmDel].StartsWith("!TRNS") || lines[CmDel].StartsWith("\"!TRNS\"") ||
                                lines[CmDel].StartsWith("!ACCNT") || lines[CmDel].StartsWith("\"!ACCNT\"") ||
                                 lines[CmDel].StartsWith("!CUST") || lines[CmDel].StartsWith("\"!CUST\"") ||
                                lines[CmDel].StartsWith("!EMP") || lines[CmDel].StartsWith("\"!EMP\"") ||
                                lines[CmDel].StartsWith("!INVITEM") || lines[CmDel].StartsWith("\"!INVITEM\"") ||
                                lines[CmDel].StartsWith("!OTHERNAME") || lines[CmDel].StartsWith("\"!OTHERNAME\"") ||
                                lines[CmDel].StartsWith("!VEND") || lines[CmDel].StartsWith("\"!VEND\"") ||
                                //Axis 720
                                //lines[CmDel].StartsWith("!TIMERHDR") || lines[CmDel].StartsWith("\"!TIMERHDR\"") ||
                                lines[CmDel].StartsWith("!TIMEACT") || lines[CmDel].StartsWith("\"!TIMEACT\""))
                                //Axis 720 ends
                            {
                                if (lines[CmDel].Contains(","))
                                {
                                    this.m_FMT = "CSVDelimited";
                                    delimiter[0] = ',';
                                }
                                //previous Code
                                //else if (lines[CmDel].StartsWith("!TRNS"))
                                //{
                                //    if (lines[CmDel].Contains("\t"))
                                //    {
                                //        this.m_FMT = "TabDelimited";
                                //        delimiter[0] = '\t';
                                //    }                                    
                                //}
                                //else if (lines[CmDel].StartsWith("!TRNS"))
                                //{
                                //    if (lines[CmDel].Contains("|"))
                                //    {
                                //        this.m_FMT = "Delimited";
                                //        delimiter[0] = '|';
                                //    }
                                //}
                                
                                //New Code
                                 else if (lines[CmDel].Contains("\t"))
                                 {
                                     this.m_FMT = "TabDelimited";
                                     delimiter[0] = '\t';
                                 }
                                                        
                                 else if (lines[CmDel].Contains("|"))
                                 {
                                     this.m_FMT = "Delimited";
                                     delimiter[0] = '|';
                                 }
                            }
                            else
                            {
                                //P Axis 13.1 : issue 683
                                //break;
                            }
                        }

                        string textHeaderData = string.Empty;
                        //int DebitIndex = 0;
                        //int DebitLineIndex = 0;

                        //List contains header data
                        List<string> header_trans = new List<string>();

                        //List contains header data
                        List<string> header_spl = new List<string>();

                        //List contains actual detail TRNS data
                        List<string> detail = new List<string>();

                        //List contains actual detail SPL data
                        List<string> detailSpl = new List<string>();

                        //List contains actual detail TIMEACT data
                        List<string> detailTimeact = new List<string>();
                        bool IsJournal = false;

                        for (int i = 0; i < lines.Length; i++)
                        {

                            List<string> Transcolumns = new List<string>();
                            List<string> Splitcolumns = new List<string>();
                            List<string> Transrows = new List<string>();
                            List<string> Splitrows = new List<string>();
                            List<string> newColumnList = new List<string>();
                            List<string> TrnsData = new List<string>();
                            //string TrnsData = string.Empty;
                            List<string> SplData = new List<string>();
                            //string SplData = string.Empty;

                            //615   
                            //Axis 624
                            if (lines[i].StartsWith("!ACCNT") || lines[i].StartsWith("\"!ACCNT\""))
                            {
                                trnsTypeList.Add("Account");
                            }
                            if (lines[i].StartsWith("!CUST") || lines[i].StartsWith("\"!CUST\""))
                            {
                                trnsTypeList.Add("Customer");
                            }
                            if (lines[i].StartsWith("!EMP") || lines[i].StartsWith("\"!EMP\""))
                            {
                                trnsTypeList.Add("Employee");
                            }
                            if (lines[i].StartsWith("!INVITEM") || lines[i].StartsWith("\"!INVITEM\""))
                            {
                                trnsTypeList.Add("InventoryItem");
                            }
                            if (lines[i].StartsWith("!OTHERNAME") || lines[i].StartsWith("\"!OTHERNAME\""))
                            {
                                trnsTypeList.Add("Othername");
                            }
                            if (lines[i].StartsWith("!VEND") || lines[i].StartsWith("\"!VEND\""))
                            {
                                trnsTypeList.Add("Vendor");
                            }
                            //Axis 720
                            //if (lines[i].StartsWith("!TIMERHDR") || lines[i].StartsWith("\"!TIMERHDR\""))
                            //{
                            //    trnsTypeList.Add("TIMERHDR");
                            //}
                            if (lines[i].StartsWith("!TIMEACT") || lines[i].StartsWith("\"!TIMEACT\""))
                            {
                                trnsTypeList.Add("TIMEACT");
                            }
                            //Axis 720 ends

                            #region TRNS
                            if (lines[i].StartsWith("!TRNS") || lines[i].StartsWith("\"!TRNS\""))
                            {
                                Transcolumns = lines[i].Replace("\"", "").Split(delimiter).ToList();
                                Transcolumns.RemoveAt(0);
                                //Axis 624

                                int transtypeIndex = Transcolumns.IndexOf("TRNSTYPE");
                                if (Transcolumns.Contains("TRANSTYPE"))
                                {
                                    transtypeIndex = Transcolumns.IndexOf("TRANSTYPE");
                                }

                                for (int j = i + 1; j < lines.Length; j++)
                                {
                                    i = j;
                                    if (lines[j].StartsWith("!TRNS") || lines[j].StartsWith("\"!TRNS\""))
                                    {
                                        Transcolumns = lines[j].Split(delimiter).ToList();
                                        Transcolumns.RemoveAt(0);
                                    }
                                    if (lines[j].StartsWith("!SPL") || lines[j].StartsWith("\"!SPL\""))
                                    {
                                        Splitcolumns = lines[j].Split(delimiter).ToList();
                                        Splitcolumns.RemoveAt(0);
                                    }
                                    DataRow Transdr = textDataFromSheet.NewRow();

                                    //Improvement ;; 615
                                    if (lines[j].StartsWith("TRNS") || lines[j].StartsWith("\"TRNS\""))
                                    {
                                        Transrows = lines[j].Split(delimiter).ToList();
                                        Transrows.RemoveAt(0);
                                        string transType = Transrows[transtypeIndex];

                                        if (!trnsTypeList.Contains(transType))
                                        {
                                            trnsTypeList.Add(transType);

                                            ////BindingSource bs = new BindingSource();
                                            ////bs.DataSource = trnsTypeList;
                                            ////tni.comboBoxIIFTransType.DataSource = bs.DataSource;

                                            ////tni.comboBoxIIFTransType.SelectedText = tni.comboBoxIIFTransType.SelectedItem.ToString();
                                            ////tni.comboBoxIIFTransType.DisplayMember = tni.comboBoxIIFTransType.SelectedItem.ToString();
                                            ////tni.comboBoxIIFTransType.ValueMember = "Name";

                                            //new change
                                            //this.comboBoxIIFTransType.SelectedIndexChanged +=
                                            //new System.EventHandler(comboBoxIIFTransType_SelectedIndexChanged);
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                }
            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.ToString());
               
            }
                return trnsTypeList;
        }
        /// <summary>
        /// Gets the data from the file by sheet name.
        /// </summary>
        /// <param name="sheetName">Name of the sheet from which data to be fetched</param>
        /// <returns></returns>
        public System.Data.DataTable GetDataBySheetName(string sheetName)
        {
            
            textDataFromSheet = null;
            int transCount = 0;
            int splCount = 0;
            int gemboxused = 0;
            //628
            // int transCount = 0;
            //OleDbConnection oleDBConn = null;
            try
            {
                this.m_CurrentSheetname = sheetName;

                // string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");

                string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                string exfile = this.m_filePath;
                #region IIF File Parsing
                //This is used for IIF file parsing logic. So that we can import those files.

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                    if (this.m_FMT != "Fixed")
                    {
                        textDataFromSheet = new System.Data.DataTable();
                        char[] delimiter = new char[1];
                        //Main Code
                        //Axis 624
                        string[] lines = File.ReadAllLines(this.m_filePath);

                        for (int CmDel = 0; CmDel < lines.Length; CmDel++)
                        {
                            //issue 709
                            if (lines[CmDel].StartsWith("!TRNS") || lines[CmDel].StartsWith("\"!TRNS\"") ||    
                                lines[CmDel].StartsWith("!ACCNT") || lines[CmDel].StartsWith("\"!ACCNT\"") ||
                                lines[CmDel].StartsWith("!CUST") || lines[CmDel].StartsWith("\"!CUST\"") ||
                                lines[CmDel].StartsWith("!EMP") || lines[CmDel].StartsWith("\"!EMP\"") ||
                                lines[CmDel].StartsWith("!INVITEM") || lines[CmDel].StartsWith("\"!INVITEM\"") ||
                                lines[CmDel].StartsWith("!OTHERNAME") || lines[CmDel].StartsWith("\"!OTHERNAME\"") ||
                                lines[CmDel].StartsWith("!VEND") || lines[CmDel].StartsWith("\"!VEND\"") ||
                                //Axis 720
                                //lines[CmDel].StartsWith("!TIMERHDR") || lines[CmDel].StartsWith("\"!TIMERHDR\"") ||
                                lines[CmDel].StartsWith("!TIMEACT") || lines[CmDel].StartsWith("\"!TIMEACT\"")
                                //Axis 720 ends                            
                                )
                            {
                                if (lines[CmDel].Contains(","))
                                {
                                    this.m_FMT = "CSVDelimited";
                                    delimiter[0] = ',';
                                    break;
                                }

                                //New Code
                                else if (lines[CmDel].Contains("\t"))
                                {
                                    this.m_FMT = "TabDelimited";
                                    delimiter[0] = '\t';
                                    break;
                                }

                                else if (lines[CmDel].Contains("|"))
                                {
                                    this.m_FMT = "Delimited";
                                    delimiter[0] = '|';
                                    break;
                                }
                            }

                        }
                        //Axis 624 End
                        #region
                        //if (this.m_FMT == "TabDelimited")
                        //{
                        //    delimiter[0] = '\t';
                        //    ti.radioButtonTab.Checked = true;
                        //}

                        //if (this.m_FMT == "CSVDelimited")
                        //{
                        //    delimiter[0] = ',';
                        //    ti.radioButtoncomma.Checked = true;
                        //}

                        //if (this.m_FMT == "Delimited(|)")
                        //{
                        //    delimiter[0] = '|';
                        //    ti.radioButtonPipe.Checked = true;
                        //}


                        //596
                        //lines.ToString().Replace("\"", string.Empty);

                        //for (int i = 0; i < lines.Length; i++)
                        //{
                        //    lines[i].ToString().Replace("\"", string.Empty);
                        //}
                        ///615
                        //for (int CmDel = 0; CmDel < lines.Length; CmDel++)
                        //{
                        //    if (lines[CmDel].StartsWith("!TRNS"))
                        //    {
                        //        //New Change::
                        //        //if (this.m_FMT == "TabDelimited")
                        //        //    delimiter[0] = '\t';
                        //        //if (this.m_FMT == "CSVDelimited")
                        //        //    delimiter[0] = ',';
                        //        //if (this.m_FMT == "Delimited(|)")
                        //        //    delimiter[0] = '|';

                        //        if (lines[CmDel].Contains(","))
                        //        {
                        //            this.m_FMT = "CSVDelimited";
                        //            delimiter[0] = ',';
                        //        }
                        //        //New Code
                        //        else if (lines[CmDel].Contains("\t"))
                        //        {
                        //            this.m_FMT = "TabDelimited";
                        //            delimiter[0] = '\t';
                        //        }

                        //        else if (lines[CmDel].Contains("|"))
                        //        {
                        //            this.m_FMT = "Delimited";
                        //            delimiter[0] = '|';
                        //        }
                        //    }
                        //    else { break; }
                        //}
                        #endregion
                        string textHeaderData = string.Empty;
                        int DebitIndex = 0;
                        int DebitLineIndex = 0;

                        //List contains header data
                        List<string> header_trans = new List<string>();

                        //List contains header data
                        List<string> header_spl = new List<string>();

                        //List contains actual detail TRNS data
                        List<string> detail = new List<string>();

                        //List contains actual detail SPL data
                        List<string> detailSpl = new List<string>();

                        //List contains actual detail TIMEACT data
                        List<string> detailTimeact = new List<string>();
                        bool IsJournal = false;

                        for (int i = 0; i < lines.Length; i++)
                        {
                            //Axis 641
                            if (i % callDoEventsOn == 0)
                            {
                                Application.DoEvents();

                            }
                            //Axis 641 End

                            if (!string.IsNullOrEmpty(lines[i].ToString().Trim()))
                            {
                                //Axis 624  issue 709
                                if (lines[i].StartsWith("TRNS") || lines[i].StartsWith("\"TRNS\"") ||
                                lines[i].StartsWith("ACCNT") || lines[i].StartsWith("\"ACCNT\"") ||
                                lines[i].StartsWith("CUST") || lines[i].StartsWith("\"CUST\"") ||
                                lines[i].StartsWith("EMP") || lines[i].StartsWith("\"EMP\"") ||
                                lines[i].StartsWith("INVITEM") || lines[i].StartsWith("\"INVITEM\"") ||
                                lines[i].StartsWith("OTHERNAME") || lines[i].StartsWith("\"OTHERNAME\"") ||
                                lines[i].StartsWith("VEND") || lines[i].StartsWith("\"VEND\"")   ||
                                //Axis 720
                                //lines[i].StartsWith("TIMERHDR") || lines[i].StartsWith("\"TIMERHDR\"") ||
                                lines[i].StartsWith("TIMEACT") || lines[i].StartsWith("\"TIMEACT\"") 
                                //Axis 720 ends                                
                                )
                                {
                                    string transtype = lines[i].ToString();

                                    //596
                                    //lines[i] = lines[i].ToString().Replace("\"", string.Empty);
                                    //lines[i] = lines[i].ToString().Replace("\"", "");

                                    string[] columnHead = transtype.Split(delimiter);

                                    // ///615
                                    //for (int CmDel = 0; CmDel < lines.Length; CmDel++)
                                    //{
                                    //    if (lines[CmDel].StartsWith("!TRNS"))
                                    //    {
                                    //        if (lines[CmDel].Contains(","))
                                    //        {
                                    //            this.m_FMT = "CSVDelimited";
                                    //            delimiter[0] = ',';
                                    //        }
                                    //    }
                                    //    else { break; }
                                    //}

                                  foreach(var arr in columnHead)
                                  {
                                    if (arr.Contains("GENERAL JOURNAL"))
                                    {
                                        IsJournal = true;
                                        break;
                                    }
                                  }
                                }
                            }
                        }

                        //issue 709
                        string listname1 = "";string listname2 = "";
                        if (this.m_CurrentSheetname == "Account")
                        {
                            listname1 = "ACCNT";
                            listname2 = "accnt";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        else if (this.m_CurrentSheetname == "Customer")
                        {
                            listname1 = "CUST";
                            listname2 = "cust";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        else if (this.m_CurrentSheetname == "Employee")
                        {
                            listname1 = "EMP";
                            listname2 = "emp";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        else if (this.m_CurrentSheetname == "InventoryItem")
                        {
                            listname1 = "INVITEM";
                            listname2 = "invetem";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        else if (this.m_CurrentSheetname == "Othername")
                        {
                            listname1 = "OTHERNAME";
                            listname2 = "othername";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        else if (this.m_CurrentSheetname == "Vendor")
                        {
                            listname1 = "VEND";
                            listname2 = "vend";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        //Axis-720
                        //else if (this.m_CurrentSheetname == "TIMERHDR")
                        //{
                        //    listname1 = "TIMERHDR";
                        //    listname2 = "";
                        //    textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        //}
                        else if (this.m_CurrentSheetname == "TIMEACT")
                        {
                            listname1 = "TIMEACT";
                            listname2 = "timeact";
                            textDataFromSheet = GetDataByListName(lines, delimiter, listname1, listname2);
                        }
                        else
                        {
                            for (int i = 0; i < lines.Length; i++)
                            {
                                //Axis 641
                                if (i % callDoEventsOn == 0)
                                {
                                    Application.DoEvents();
                                }
                                //Axis 641 End

                                if (lines[i].StartsWith("!TRNS") || lines[i].StartsWith("\"!TRNS\""))
                            {
                                #region Header data of !TRNS and !SPL
                                //Add header data of TRNS and SPL into List.
                                string col = lines[i].ToString();

                                //596
                                //lines[i] = lines[i].ToString().Replace("\"", string.Empty);
                                //lines[i] = lines[i].ToString().Replace("\"", "");

                                string[] columnHead = col.Replace("\"",string.Empty).Split(delimiter);

                                int tempCol = 0;

                                //P Axis 13.1 : issue 683
                                int temp = 1;
                                //P Axis 13.1 : issue 683 END
                                foreach (string columnHeader in columnHead.ToList())
                                {

                                    if (columnHeader.Trim().ToLower() != "!trns" && columnHeader.Trim().ToLower() != "\"!trns\"")
                                    {
                                        if (columnHeader != "")
                                        {
                                            if (columnHeader.ToLower().Contains("amount") || columnHeader.ToLower().Contains("\"amount\""))
                                            {
                                                if (IsJournal)
                                                {
                                                    if (header_trans.Contains("DEBIT") || header_trans.Contains("\"DEBIT\""))
                                                    {
                                                        header_trans.Add("DEBIT");
                                                        header_trans.Add("CREDIT");
                                                    }
                                                    else
                                                    {
                                                        header_trans.Add("DEBIT");
                                                        DebitIndex = header_trans.Count();
                                                        header_trans.Add("CREDIT");
                                                    }
                                                }
                                                else
                                                {
                                                    header_trans.Add(columnHeader);
                                                }
                                            }
                                            else
                                            {
                                                header_trans.Add(columnHeader);
                                            }
                                            if (lines[i].StartsWith("!TRNS") || lines[i].StartsWith("\"!TRNS\""))
                                                transCount++;
                                            if (lines[i].StartsWith("!SPL") || lines[i].StartsWith("\"!SPL\""))
                                                splCount++;
                                        }
                                        else
                                        {
                                            //Axis 624 p
                                            //int temp = 1;
                                            //Axis 624 p end
                                            foreach (var item in columnHead.ToList())
                                            {

                                                if (!columnHead.Contains("Column" + temp))
                                                {
                                                    header_trans.Add("Column" + temp);
                                                    columnHead[tempCol] = "Column" + temp;
                                                    if (this.m_FMT == "TabDelimited")
                                                        lines[i] = String.Join("\t", columnHead.ToList());

                                                    else if (this.m_FMT == "CSVDelimited")
                                                        lines[i] = String.Join(",", columnHead.ToList());

                                                    else if (this.m_FMT == "Delimited(|)")
                                                        lines[i] = String.Join("|", columnHead.ToList());
                                                    temp++;

                                                    break;
                                                }
                                                else
                                                    temp++;
                                            }
                                        }                                       
                                    }
                                    tempCol++;
                                }
                                // break;
                                //Axis 624 p end
                                #endregion
                            }
                            //else if (lines[i].StartsWith("!TIMEACT"))
                            //{
                            //    #region Header data of !TIMEACT

                            //    //Add header data of TIMEACT into List.
                            //    string[] columnHead = lines[i].Split(delimiter);
                            //    foreach (string columnHeader in columnHead)
                            //    {
                            //        if (columnHeader.Trim().ToLower() != "!timeact")
                            //        {
                            //            header.Add(columnHeader);
                            //        }
                            //    }
                            //    #endregion
                            //}

                        }
                        int temp1 = 0;

                        //if (!IsJournal)
                        //{
                        for (int i = 0; i < lines.Length; i++)
                        {
                            //Axis 641
                            if (i % callDoEventsOn == 0)
                            {
                                Application.DoEvents();

                            }
                            //Axis 641 End
                            if (lines[i].StartsWith("!SPL") || lines[i].StartsWith("\"!SPL\""))
                            {
                                #region Header data of !TRNS and !SPL
                                //Add header data of TRNS and SPL into List.
                                string col = lines[i].ToString();
                                //596
                                //lines[i] = lines[i].ToString().Replace("\"", string.Empty);
                                //lines[i] = lines[i].ToString().Replace("\"", "");
                                string[] columnHead = col.Replace("\"", "").Split(delimiter);

                                int tempCol = 0;
                                foreach (string columnHeader in columnHead)
                                {
                                    if (columnHeader.Trim().ToLower() != "!spl" && columnHeader.Trim().ToLower() != "\"!spl\"")
                                    {
                                        if (columnHeader != "")
                                        {
                                            temp1++;
                                            if (columnHeader.ToLower().Contains("amount") || columnHeader.ToLower().Contains("\"amount\""))
                                            {
                                                if (IsJournal)
                                                {
                                                    if (header_spl.Contains("DEBIT") || header_spl.Contains("\"DEBIT\""))
                                                    {
                                                        DebitLineIndex = temp1;
                                                        header_spl.Add("DEBIT");
                                                        header_spl.Add("CREDIT");
                                                    }
                                                    else
                                                    {
                                                        header_spl.Add("DEBIT");
                                                        DebitIndex = header_spl.Count();
                                                        header_spl.Add("CREDIT");
                                                    }
                                                }
                                                else
                                                {
                                                    header_spl.Add(columnHeader);
                                                }
                                            }
                                            else
                                            {
                                                header_spl.Add(columnHeader);
                                            }
                                            if (lines[i].StartsWith("!TRNS") || lines[i].StartsWith("\"!TRNS\""))
                                                transCount++;
                                            if (lines[i].StartsWith("!SPL") || lines[i].StartsWith("\"!SPL\""))
                                                splCount++;
                                        }
                                        else
                                        {
                                            int temp = 1;
                                            foreach (var item in columnHead.ToList())
                                            {
                                                try
                                                {
                                                    if (!header_spl.Contains("Column" + temp) || !header_spl.Contains("\"Column\"" + temp))
                                                    {
                                                        header_spl.Add("Column" + temp);
                                                        columnHead[tempCol] = "Column" + temp;
                                                        if (this.m_FMT == "TabDelimited")
                                                            lines[i] = String.Join("\t", columnHead.ToList());

                                                        else if (this.m_FMT == "CSVDelimited")
                                                            lines[i] = String.Join(",", columnHead.ToList());
                                                            

                                                       else if (this.m_FMT == "Delimited(|)")
                                                            lines[i] = String.Join("|", columnHead.ToList()); 
                                                        break;
                                                    }
                                                    else
                                                        temp++;
                                                }
                                                catch (Exception)
                                                {
                                                    temp++;
                                                    throw;
                                                }
                                            }

                                        }
                                    }
                                    tempCol++;
                                }
                                #endregion
                            }
                        }
                        
                        //for (int i = 0; i < lines.Length; i++)
                        //{
                        //    if (lines[i].StartsWith("!TIMEACT"))
                        //    {
                        //        #region Header data of !TIMEACT

                        //        //Add header data of TIMEACT into List.
                        //        string[] columnHead = lines[i].Split(delimiter);
                        //        foreach (string columnHeader in columnHead)
                        //        {
                        //            if (columnHeader.Trim().ToLower() != "!timeact")
                        //            {
                        //                header.Add(columnHeader);
                        //            }
                        //        }
                        //        break;
                        //        #endregion
                        //    }
                        //}
                        int tmp = 0;
                        foreach (string head in header_trans)
                        {
                            try
                            {
                                textDataFromSheet.Columns.Add("trans" + head);
                            }
                            catch
                            {
                            }
                        }

                        foreach (string head in header_spl)
                        {

                            try
                            {
                                if (IsJournal)
                                {
                                    textDataFromSheet.Columns.Add("trans" + head);
                                }
                                else
                                {
                                    textDataFromSheet.Columns.Add("spl" + head);
                                }
                            }
                            catch
                            {
                            }
                        }


                        //BUG:::571
                        #region iif parsing 615
                       
                        for (int i = 0; i < lines.Length; i++)
                        {
                            //Axis 641
                            if (i % callDoEventsOn == 0)
                            {
                                Application.DoEvents();

                            }
                            //Axis 641 End

                            List<string> Transcolumns = new List<string>();
                            List<string> Splitcolumns = new List<string>();
                            List<string> Transrows = new List<string>();
                            List<string> Splitrows = new List<string>();
                            List<string> newColumnList = new List<string>();
                            List<string> TrnsData = new List<string>();
                            //615
                            List<string> TrnsSplColumn = new List<string>();
                            //string TrnsData = string.Empty;
                            List<string> SplData = new List<string>();
                            //string SplData = string.Empty;

                                #region trns & spl
                                if (lines[i].StartsWith("!TRNS") || lines[i].StartsWith("\"!TRNS\""))
                                {
                                    //596
                                    //lines[i] = lines[i].ToString().Replace("\"", string.Empty);
                                    //lines[i] = lines[i].ToString().Replace("\"", "");

                                //Transcolumns = lines[i].Replace("\"", string.Empty).Split(delimiter).ToList();

                                Transcolumns = lines[i].Replace("\"", "").Split(delimiter).ToList();

                                Transcolumns.RemoveAt(0);
                                int transtypeIndex = Transcolumns.IndexOf("TRNSTYPE");
                                ///615
                                //DataRow Transdr = textDataFromSheet.NewRow();

                                for (int j = i ; j < lines.Length; j++)
                                {
                                    i = j;
                                    if (lines[j].StartsWith("!TRNS") || lines[j].StartsWith("\"!TRNS\""))
                                    {
                                        //596
                                        //lines[j] = lines[j].Replace("\"", string.Empty);
                                        //lines[j] = lines[j].Replace("\"", "");

                                        //lines[j].IndexOf("!TRNS");
                                        //break;
                                        //Transcolumns = lines[j].Replace("\"", string.Empty).Split(delimiter).ToList();

                                        Transcolumns = lines[j].Replace("\"", "").Split(delimiter).ToList();

                                        Transcolumns.RemoveAt(0);
                                        if (IsJournal)
                                        {
                                            if (Transcolumns.Contains("AMOUNT") || Transcolumns.Contains("\"AMOUNT\""))
                                            {
                                                DebitLineIndex = temp1;
                                                int index = Transcolumns.IndexOf("AMOUNT");
                                                Transcolumns[index] = "DEBIT";
                                            }
                                        }
                                        #region Code for 615
                                        ///Improvement;;615
                                        ///
                                        for (int k = j + 1; k < lines.Length; k++)
                                        {
                                            //596
                                            //lines[k] = lines[k].Replace("\"", string.Empty);

                                            j = k;
                                            if (lines[k].StartsWith("!TRNS") || lines[k].StartsWith("\"!TRNS\""))
                                            {
                                                break;
                                            }
                                            if (lines[k].StartsWith("!SPL") || lines[k].StartsWith("\"!SPL\""))
                                            {
                                                //596
                                                //lines[k] = lines[k].Replace("\"", string.Empty);
                                                //lines[k] = lines[k].Replace("\"", "");

                                                //Splitcolumns = lines[k].Replace("\"", string.Empty).Split(delimiter).ToList();

                                                Splitcolumns = lines[k].Replace("\"", "").Split(delimiter).ToList();
                                                Splitcolumns.RemoveAt(0);

                                                if (IsJournal)
                                                {
                                                    if (Splitcolumns.Contains("AMOUNT") || Splitcolumns.Contains("\"AMOUNT\""))
                                                    {
                                                        int index = Splitcolumns.IndexOf("AMOUNT");
                                                        Splitcolumns[index] = "DEBIT";
                                                    }
                                                }
                                            }
                                            DataRow Transdrs = textDataFromSheet.NewRow();

                                            #region TRNS Validations
                                            if (lines[k].StartsWith("TRNS") || lines[k].StartsWith("\"TRNS\""))
                                            {
                                                //596
                                                //lines[k] = lines[k].Replace("\"", string.Empty);
                                                //lines[k] = lines[k].Replace("\"", "");

                                                //Transrows = lines[k].Replace("\"", string.Empty).Split(delimiter).ToList();

                                                var reg = new Regex("\".*?\"");
                                                var matches = reg.Matches(lines[k]);
                                                //P Axis 13.1 : issue 683
                                                for (int tempReg = 0; tempReg < reg.Matches(lines[k]).Count; tempReg++)
                                                {
                                                    foreach (Match match in reg.Matches(lines[k]))
                                                    {
                                                        if (match.ToString().Contains(",") || match.ToString().Contains("\t") || match.ToString().Contains("|"))
                                                        {
                                                            int startIndex = match.Index;
                                                            int EndIndex = match.Index + match.Length - 1;
                                                            var aStringBuilder = new StringBuilder(lines[k]);
                                                            aStringBuilder.Remove(startIndex, match.Length);
                                                            aStringBuilder.Insert(startIndex, match.ToString().Replace(",", "~~**&&").Replace("|", "@@!!^^").Replace("\t", "$$??##"));
                                                            lines[k] = aStringBuilder.ToString();
                                                            tempReg = 0; //P Axis 13.1 : issue 683
                                                            break;       //P Axis 13.1 : issue 683
                                                        }
                                                    }
                                                }
                                                foreach (var item in matches)
                                                {
                                                    Console.WriteLine(item.ToString());
                                                }
                                                //Axis 624
                                                // Transrows = lines[k].Split(delimiter).ToList();
                                                Transrows = lines[k].Replace("\"", "").Split(delimiter).ToList();
                                                Transrows.RemoveAt(0);
                                                int temp = 0;
                                                try
                                                {
                                                for (int count = 0; count < Transcolumns.Count(); count++)
                                                        {
                                                            if (Transrows.Count < Transcolumns.Count)  ////P Axis 13.2 : issue 708
                                                            {
                                                                if (Transrows.Count <= count)
                                                                {
                                                                    Transrows.Insert(count, "");
                                                                }
                                                            }
                                                    int TranColumnindex = textDataFromSheet.Columns["trans" + Transcolumns[count]].Ordinal;
                                                   
                                                    if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transDEBIT")
                                                    {
                                                        if (Transrows.ElementAt(count).Contains("-"))
                                                        {
                                                            string Credit = Transrows.ElementAt(count);
                                                            Credit = Credit.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                            Transdrs[TranColumnindex+1] = Credit;
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                        temp = 1;
                                                    }
                                                    // New Change of Validation;;
                                                    //Amount ;;
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transAMOUNT")
                                                    {
                                                        if (Transrows.ElementAt(count).Contains("-"))
                                                        {
                                                            string amt = Transrows.ElementAt(count);
                                                            amt = amt.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                            Transdrs[TranColumnindex] = amt;
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }                                                       
                                                    }
                                                    //ToPrint ;; Y = true, N = false
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transTOPRINT")
                                                    {
                                                        if (Transrows.ElementAt(count).ToString() != string.Empty)
                                                        {
                                                            string toprint = Transrows.ElementAt(count);
                                                            toprint = toprint.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                            Transdrs[TranColumnindex] = toprint;
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }
                                                    //NAMEISTAXABLE : Y = true, N = false
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transNAMEISTAXABLE")
                                                    {
                                                        if (Transrows.ElementAt(count).ToString() != string.Empty)
                                                        {
                                                            string Nameistxbl = Transrows.ElementAt(count);
                                                            Nameistxbl = Nameistxbl.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                            Transdrs[TranColumnindex] = Nameistxbl;
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }
                                                    //TAXABLE : Y = true, N = false
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transTAXABLE")
                                                    {
                                                        if (Transrows.ElementAt(count).ToString() != string.Empty)
                                                        {
                                                            string taxable = Transrows.ElementAt(count);
                                                            taxable = taxable.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                            Transdrs[TranColumnindex] = taxable;
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }
                                                    //CLEAR : Y = true, N = false
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transCLEAR")
                                                    {
                                                        if (Transrows.ElementAt(count).ToString() != string.Empty)
                                                        {
                                                            string clear = Transrows.ElementAt(count);
                                                            clear = clear.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                            Transdrs[TranColumnindex] = clear;
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }
                                                    //PAID : Y = true, N = false
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transPAID")
                                                    {
													
                                                            if (Transrows.ElementAt(count) != null)  //Axis 683 
                                                            {
                                                                if (Transrows.ElementAt(count).ToString() != string.Empty)
                                                                {
                                                                    string paid = Transrows.ElementAt(count);
                                                                    paid = paid.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    Transdrs[TranColumnindex] = paid;
                                                                }
                                                                else
                                                                {
                                                                    Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                }
                                                            }
                                                    }
                                                    //DATE :; MM/DD/YY to regional format
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transDATE")
                                                    {
                                                        DateTime NewDt = new DateTime();                                                       
                                                        string dt = Transrows.ElementAt(count).ToString();
                                                        if (dt != string.Empty)
                                                        { 
                                                            if (!DateTime.TryParse(dt, out NewDt))
                                                            {
                                                                DateTime dttest = new DateTime();
                                                                bool IsValid = false;
                                                                try
                                                                {
                                                                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                    IsValid = true;
                                                                }
                                                                catch(Exception ex)
                                                                {
                                                                    IsValid = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                NewDt = Convert.ToDateTime(dt);
                                                                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                            }
                                                            Transdrs[TranColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }

                                                    //DUEDATE : MM/DD/YY to regional format
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transDUEDATE")
                                                    {
                                                        DateTime NewDt = new DateTime();                                                       
                                                        string dt = Transrows.ElementAt(count).ToString();
                                                        if (dt != string.Empty)
                                                        { 
                                                            if (!DateTime.TryParse(dt, out NewDt))
                                                            {
                                                                DateTime dttest = new DateTime();
                                                                bool IsValid = false;

                                                                try
                                                                {
                                                                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                    IsValid = true;
                                                                }
                                                                catch
                                                                {
                                                                    IsValid = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                NewDt = Convert.ToDateTime(dt);
                                                                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                            }
                                                            Transdrs[TranColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }
                                                    //SERVICEDATE : MM/DD/YY to regional format
                                                    else if (textDataFromSheet.Columns[TranColumnindex].ColumnName == "transSERVICEDATE")
                                                    {
                                                        DateTime NewDt = new DateTime();
                                                        string dt = Transrows.ElementAt(count).ToString();
                                                        if (dt != string.Empty)
                                                        {
                                                            if (!DateTime.TryParse(dt, out NewDt))
                                                            {
                                                                DateTime dttest = new DateTime();
                                                                bool IsValid = false;

                                                                try
                                                                {
                                                                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                    IsValid = true;
                                                                }
                                                                catch
                                                                {
                                                                    IsValid = false;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                NewDt = Convert.ToDateTime(dt);
                                                                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                            }
                                                            Transdrs[TranColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                        else
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                        }
                                                    }

                                                    else
                                                    {
                                                        try
                                                        {
                                                            Transdrs[TranColumnindex] = Transrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");

                                                            }
                                                            catch (Exception)
                                                            {
                                                                Transdrs[TranColumnindex] = "";

                                                            }
                                                        }
                                                    }
                                                    if (IsJournal)
                                                    {
                                                        textDataFromSheet.Rows.Add(Transdrs.ItemArray);
                                                    }
                                                    k++;
													
													//P Axis 13.1 : issue 684
                                                    if (lines[k].StartsWith("!SPL"))
                                                    {
                                                        Splitcolumns = lines[k].Replace("\"", " ").Split(delimiter).ToList();
                                                        Splitcolumns.RemoveAt(0);

                                                        if (IsJournal)
                                                        {
                                                            if (Splitcolumns.Contains("AMOUNT"))
                                                            {
                                                                int index = Splitcolumns.IndexOf("AMOUNT");
                                                                Splitcolumns[index] = "DEBIT";
                                                            }
                                                        }
                                                        k++;
                                                    }
                                                    //P Axis 13.1 : issue 684 END

                                                    #endregion TRNS val

                                                    #region SPL Validations
                                                    //var tempvar = lines[k].StartsWith("SPL").ToString();
                                                    for (int SplCount = 0; lines[k].StartsWith("SPL")|| lines[k].StartsWith("\"SPL\""); SplCount++)
                                                    {
                                                        if (lines[k].StartsWith("SPL") || lines[k].StartsWith("\"SPL\""))
                                                        {
                                                            //596
                                                            //lines[k] = lines[k].Replace("\"", string.Empty);
                                                            //lines[k] = lines[k].Replace("\"", "");

                                                            //Splitrows = lines[k].Replace("\"", string.Empty).Split(delimiter).ToList();
                                                            var regSpl = new Regex("\".*?\"");
                                                            foreach (Match match in regSpl.Matches(lines[k]))
                                                            {
                                                                if (match.ToString().Contains(",") || match.ToString().Contains("\t") || match.ToString().Contains("|"))
                                                                {
                                                                    int startIndex = match.Index;
                                                                    int EndIndex = match.Index + match.Length - 1;
                                                                    var aStringBuilder = new StringBuilder(lines[k]);
                                                                    aStringBuilder.Remove(startIndex, match.Length);
                                                                    aStringBuilder.Insert(startIndex, match.ToString().Replace(",", "~~**&&").Replace("|", "@@!!^^").Replace("\t", "$$??##"));
                                                                    lines[k] = aStringBuilder.ToString();
                                                                }

                                                            }
                                                            Splitrows = lines[k].Split(delimiter).ToList();
                                                           
                                                            Splitrows.RemoveAt(0);                                                           
                                                            DataRow dr = textDataFromSheet.NewRow();


                                                            temp = 0;

                                                            //Axis 608 All Spl and Trans Should be in Same Line if Trans type is Journal 
                                                            if (IsJournal)
                                                            {
                                                                dr = textDataFromSheet.NewRow();
                                                                    for (int count = 0; count < Splitcolumns.Count(); count++)
                                                                    {
                                                                        if (Splitrows.Count < Splitcolumns.Count)  ////P Axis 13.2 : issue 708
                                                                        {
                                                                            if (Splitrows.Count <= count)
                                                                            {
                                                                                Splitrows.Insert(count, "");
                                                                            }
                                                                        }
                                                                        int SplColumnindex = textDataFromSheet.Columns["trans" + Splitcolumns[count]].Ordinal;
                                                                        if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transDEBIT")
                                                                        {
                                                                        if (Splitrows.ElementAt(count).Contains("-"))
                                                                        {
                                                                            string Credit = Splitrows.ElementAt(count);
                                                                            Credit = Credit.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", "");
                                                                            dr[SplColumnindex + 1] = Credit;

                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", "").Replace("@@!!^^", "").Replace("$$??##", "\t");
                                                                        }
                                                                        temp = 1;
                                                                    }

                                                                    // New Change of Validation;;
                                                                    //Amount ;;
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transAMOUNT")
                                                                    {
                                                                        if (Splitrows.ElementAt(count).Contains("-"))
                                                                        {
                                                                            string amt = Splitrows.ElementAt(count);
                                                                            amt = amt.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", "");
                                                                            dr[SplColumnindex] = amt;
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", "").Replace("@@!!^^", "").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //ToPrint ;; Y = true, N = false
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transTOPRINT")
                                                                    {
                                                                        if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                        {
                                                                            string toprint = Splitrows.ElementAt(count);
                                                                            toprint = toprint.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                            dr[SplColumnindex] = toprint;
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //NAMEISTAXABLE : Y = true, N = false
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transNAMEISTAXABLE")
                                                                    {
                                                                        if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                        {
                                                                            string Nameistxbl = Splitrows.ElementAt(count);
                                                                            Nameistxbl = Nameistxbl.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                            dr[SplColumnindex] = Nameistxbl;
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //TAXABLE : Y = true, N = false
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transTAXABLE")
                                                                    {
                                                                        if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                        {
                                                                            string taxable = Splitrows.ElementAt(count);
                                                                            taxable = taxable.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                            dr[SplColumnindex] = taxable;
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //CLEAR : Y = true, N = false
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transCLEAR")
                                                                    {
                                                                        if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                        {
                                                                            string clear = Splitrows.ElementAt(count);
                                                                            clear = clear.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                            dr[SplColumnindex] = clear;
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //PAID : Y = true, N = false
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transPAID")
                                                                    {
                                                                        if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                        {
                                                                            string paid = Splitrows.ElementAt(count);
                                                                            paid = paid.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                            dr[SplColumnindex] = paid;
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //DATE :; MM/DD/YY to regional format
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transDATE")
                                                                    {
                                                                        DateTime NewDt = new DateTime();
                                                                        string dt = Splitrows.ElementAt(count).ToString();
                                                                        if (dt != string.Empty)
                                                                        {
                                                                            if (!DateTime.TryParse(dt, out NewDt))
                                                                            {
                                                                                DateTime dttest = new DateTime();
                                                                                bool IsValid = false;

                                                                                try
                                                                                {
                                                                                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                                    IsValid = true;
                                                                                }
                                                                                catch
                                                                                {
                                                                                    IsValid = false;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                NewDt = Convert.ToDateTime(dt);
                                                                                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                                            }
                                                                            dr[SplColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //DUEDATE : MM/DD/YY to regional format
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transDUEDATE")
                                                                    {
                                                                        DateTime NewDt = new DateTime();
                                                                        string dt = Splitrows.ElementAt(count).ToString();
                                                                        if (dt != string.Empty)
                                                                        {
                                                                            if (!DateTime.TryParse(dt, out NewDt))
                                                                            {
                                                                                DateTime dttest = new DateTime();
                                                                                bool IsValid = false;

                                                                                try
                                                                                {
                                                                                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                                    IsValid = true;
                                                                                }
                                                                                catch
                                                                                {
                                                                                    IsValid = false;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                NewDt = Convert.ToDateTime(dt);
                                                                                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                                            }
                                                                            dr[SplColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }
                                                                    //SERVICEDATE : MM/DD/YY to regional format
                                                                    else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "transSERVICEDATE")
                                                                    {
                                                                        DateTime NewDt = new DateTime();
                                                                        string dt = Splitrows.ElementAt(count).ToString();
                                                                        if (dt != string.Empty)
                                                                        {
                                                                            if (!DateTime.TryParse(dt, out NewDt))
                                                                            {
                                                                                DateTime dttest = new DateTime();
                                                                                bool IsValid = false;

                                                                                try
                                                                                {
                                                                                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                                    IsValid = true;
                                                                                }
                                                                                catch
                                                                                {
                                                                                    IsValid = false;
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                NewDt = Convert.ToDateTime(dt);
                                                                                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                                            }
                                                                            dr[SplColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                        else
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        }
                                                                    }

                                                                    else
                                                                    {
                                                                        try
                                                                        {
                                                                            dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");

                                                                        }
                                                                        catch (Exception)
                                                                        {

                                                                            dr[SplColumnindex] = "";

                                                                        }
                                                                    }
                                                                }
                                                                if (IsJournal)
                                                                {
                                                                    textDataFromSheet.Rows.Add(dr.ItemArray);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                for (int p = Transcolumns.Count + 1; p < Transdrs.ItemArray.Length; p++)
                                                                {
                                                                    Transdrs[p] = null;
                                                                }


                                                            dr = Transdrs;
                                                             temp = 0;
                                                            for (int count = 0; count < Splitcolumns.Count(); count++)
                                                                    {
                                                                        if (Splitrows.Count < Splitcolumns.Count)  ////P Axis 13.2 : issue 708
                                                                        {
                                                                            if (Splitrows.Count <= count)
                                                                            {
                                                                                Splitrows.Insert(count, "");
                                                                            }
                                                                        }
                                                                        int SplColumnindex = textDataFromSheet.Columns["spl" + Splitcolumns[count]].Ordinal;
                                                                if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splDEBIT")
                                                                {
                                                                    if (Splitrows.ElementAt(count).Contains("-"))
                                                                    {
                                                                        string Credit = Splitrows.ElementAt(count);
                                                                        Credit = Credit.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", "");
                                                                        dr[SplColumnindex + 1] = Credit;

                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", "").Replace("@@!!^^", "").Replace("$$??##", "\t");
                                                                    }
                                                                    temp = 1;
                                                                }

                                                                    // New Change of Validation;;
                                                                //Amount ;;
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splAMOUNT")
                                                                {
                                                                    if (Splitrows.ElementAt(count).Contains("-"))
                                                                    {
                                                                        string amt = Splitrows.ElementAt(count);
                                                                        amt = amt.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", "");
                                                                        dr[SplColumnindex] = amt;
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", "").Replace("@@!!^^", "").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //ToPrint ;; Y = true, N = false
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splTOPRINT")
                                                                {
                                                                    if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                    {
                                                                        string toprint = Splitrows.ElementAt(count);
                                                                        toprint = toprint.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        dr[SplColumnindex] = toprint;
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //NAMEISTAXABLE : Y = true, N = false
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splNAMEISTAXABLE")
                                                                {
                                                                    if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                    {
                                                                        string Nameistxbl = Splitrows.ElementAt(count);
                                                                        Nameistxbl = Nameistxbl.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        dr[SplColumnindex] = Nameistxbl;
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //TAXABLE : Y = true, N = false
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splTAXABLE")
                                                                {
                                                                    if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                    {
                                                                        string taxable = Splitrows.ElementAt(count);
                                                                        taxable = taxable.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        dr[SplColumnindex] = taxable;
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //CLEAR : Y = true, N = false
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splCLEAR")
                                                                {
                                                                    if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                    {
                                                                        string clear = Splitrows.ElementAt(count);
                                                                        clear = clear.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        dr[SplColumnindex] = clear;
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //PAID : Y = true, N = false
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splPAID")
                                                                {
                                                                    if (Splitrows.ElementAt(count).ToString() != string.Empty)
                                                                    {
                                                                        string paid = Splitrows.ElementAt(count);
                                                                        paid = paid.Replace("Y", "true").Replace("N", "false").Replace("\"", string.Empty).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                        dr[SplColumnindex] = paid;
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //DATE :; MM/DD/YY to regional format
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splDATE")
                                                                {
                                                                    DateTime NewDt = new DateTime();
                                                                    string dt = Splitrows.ElementAt(count).ToString();
                                                                    if (dt != string.Empty)
                                                                    {
                                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                                        {
                                                                            DateTime dttest = new DateTime();
                                                                            bool IsValid = false;

                                                                            try
                                                                            {
                                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                                IsValid = true;
                                                                            }
                                                                            catch
                                                                            {
                                                                                IsValid = false;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            NewDt = Convert.ToDateTime(dt);
                                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                                        }
                                                                        dr[SplColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //DUEDATE : MM/DD/YY to regional format
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splDUEDATE")
                                                                {
                                                                    DateTime NewDt = new DateTime();
                                                                    string dt = Splitrows.ElementAt(count).ToString();
                                                                    if (dt != string.Empty)
                                                                    {
                                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                                        {
                                                                            DateTime dttest = new DateTime();
                                                                            bool IsValid = false;

                                                                            try
                                                                            {
                                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                                IsValid = true;
                                                                            }
                                                                            catch
                                                                            {
                                                                                IsValid = false;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            NewDt = Convert.ToDateTime(dt);
                                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                                        }
                                                                        dr[SplColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }
                                                                //SERVICEDATE : MM/DD/YY to regional format
                                                                else if (textDataFromSheet.Columns[SplColumnindex].ColumnName == "splSERVICEDATE")
                                                                {
                                                                    DateTime NewDt = new DateTime();
                                                                    string dt = Splitrows.ElementAt(count).ToString();
                                                                    if (dt != string.Empty)
                                                                    {
                                                                        if (!DateTime.TryParse(dt, out NewDt))
                                                                        {
                                                                            DateTime dttest = new DateTime();
                                                                            bool IsValid = false;

                                                                            try
                                                                            {
                                                                                dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                                                                                IsValid = true;
                                                                            }
                                                                            catch
                                                                            {
                                                                                IsValid = false;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            NewDt = Convert.ToDateTime(dt);
                                                                            dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                                                                        }
                                                                        dr[SplColumnindex] = dt.Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                    else
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                                    }
                                                                }

                                                                else
                                                                {
                                                                    try
                                                                    {
                                                                        dr[SplColumnindex] = Splitrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");

                                                                    }
                                                                    catch (Exception)
                                                                    {

                                                                        dr[SplColumnindex] = "";

                                                                    }
                                                                }
                                                            }
                                                            }
                                                            k++;
                                                            if (!IsJournal)
                                                            {
                                                                textDataFromSheet.Rows.Add(dr.ItemArray);
                                                            }
                                                        }
                                                    }
                                                #endregion SPL Val
                                                }
                                                catch (Exception ex)
                                                {
                                                    MessageBox.Show(ex.Message);
                                                }
                                            }
                                        }
                                        //imp
                                        break;
                                        #endregion code 615
                                    }
                                    #region Commented code
                                    //if (lines[j].StartsWith("!SPL"))
                                    //{
                                    //    Splitcolumns = lines[j].Split(delimiter).ToList();
                                       
                                    //    Splitcolumns.RemoveAt(0);                                       
                                    //}
                                    //DataRow Transdr = textDataFromSheet.NewRow();
                                   ///
                                   
                                    //if (lines[j].StartsWith("TRNS"))
                                    //{
                                    //    Transrows = lines[j].Split(delimiter).ToList();
                                       
                                    //    Transrows.RemoveAt(0);
                                    //    for (int count = 0; count < Transcolumns.Count(); count++)
                                    //    {
                                    //        int TranColumnindex = textDataFromSheet.Columns["trans" + Transcolumns[count]].Ordinal;
                                    //        Transdr[TranColumnindex] = Transrows.ElementAt(count);
                                    //    }
                                    //    j++;
                                    //    try
                                    //    {
                                    //        //MainCodeee

                                    //        for (int SplCount = 0; SplCount <= lines[j].Equals("SPL").ToString().Count(); SplCount++)
                                    //        {
                                    //            if (lines[j].StartsWith("SPL"))
                                    //            {
                                    //                Splitrows = lines[j].Split(delimiter).ToList();
                                                  
                                    //                Splitrows.RemoveAt(0);
                                    //                //615
                                    //                DataRow dr = textDataFromSheet.NewRow();
                                    //                dr = Transdr;

                                    //                for (int count = 0; count < Splitcolumns.Count(); count++)
                                    //                {
                                    //                    int SplColumnindex = textDataFromSheet.Columns["spl" + Splitcolumns[count]].Ordinal;
                                    //                    Transdr[SplColumnindex] = Splitrows.ElementAt(count);
                                    //                }
                                    //                j++;
                                    //                textDataFromSheet.Rows.Add(dr.ItemArray);
                                                    
                                    //            }
                                                
                                    //        }
                                    //    }
                                    //    catch (Exception)
                                    //    {
                                    //    }
                                    //}
                                    #endregion commented code
                                    }
                                }
                                #endregion

                            }
                        }
                        
                        #endregion iif parsing 615
                        #region Commented code 
                        //for (int i = 0; i < lines.Length; i++)
                        //{
                        //    if (!string.IsNullOrEmpty(lines[i].Trim()))
                        //    {

                        //        try
                        //        {
                        //            string[] temp = new string[1];
                        //            temp[0] = "";

                        //            if (lines[i].StartsWith("TRNS"))
                        //            {
                        //                #region Actual TRNS data

                        //                //Add Actual detail TRNS data into List.
                        //                string col = lines[i].ToString();
                        //                List<string> columnDetail = col.Split(delimiter).ToList();

                        //                // Array.Resize(ref columnDetail, transCount+1);
                        //                if (textDataFromSheet.Columns.Contains("transDEBIT"))
                        //                {
                        //                    columnDetail.InsertRange(DebitIndex + 1, temp);
                        //                }

                        //                foreach (string columnDetData in columnDetail)
                        //                {
                        //                    detail.Add(columnDetData);
                        //                }
                        //                #endregion
                        //            }

                        //            else if (lines[i].StartsWith("SPL") || lines[i].StartsWith("ENDTRNS"))
                        //            {
                        //                #region Actual SPL data

                        //                //Add Actual detail SPL data into List.
                        //                string col = lines[i].ToString();
                        //                List<string> columnDetailSPL = col.Split(delimiter).ToList();

                        //                if (!lines[i].StartsWith("ENDTRNS"))
                        //                {
                        //                    if (textDataFromSheet.Columns.Contains("splDEBIT"))
                        //                    {
                        //                        columnDetailSPL.InsertRange(DebitLineIndex + 1, temp);
                        //                    }
                        //                }

                        //                // string[] columnDetailSPL = col.Split(delimiter);
                        //                //if (lines[i].StartsWith("SPL"))

                        //                // Array.Resize(ref columnDetailSPL, splCount + 1);
                        //                foreach (string columnDetDataSPL in columnDetailSPL)
                        //                {
                        //                    detailSpl.Add(columnDetDataSPL);
                        //                }
                        //                #endregion
                        //            }



                        //            else if (lines[i].StartsWith("TIMEACT"))
                        //            {
                        //                #region Actual TIMEACT data

                        //                //Add Actual detail TIMEACT data into List.
                        //                string[] columnTimeact = lines[i].Split(delimiter);

                        //                foreach (string columnTimeData in columnTimeact)
                        //                {
                        //                    detailTimeact.Add(columnTimeData);
                        //                }
                        //                #endregion
                        //            }
                        //        }
                        //        catch
                        //        {
                        //        }
                        //    }
                        //}
                        #endregion commented code
                        #region old iif parsing logic...
                        //#region Display Header data in preview grid in header line and actual detail data in grid.

                        //#region Display Header Data in header line




                        //#endregion


                        //#region Display Actual detail data of TRNS and SPL
                        //try
                        //{
                        //    string actualData = string.Empty;
                        //    string actualDataSpl = string.Empty;
                        //    string actualSplEndtrns = string.Empty;

                        //    //Get TRNS detail data
                        //    foreach (string data in detail)
                        //    {
                        //        if (data.Equals("TRNS"))
                        //        {
                        //            actualData += "\n";
                        //        }
                        //        actualData += data + "~";

                        //        actualData = actualData.Replace("\"", string.Empty);
                        //    }
                        //    actualData = actualData.Replace("\n\n", "\n");
                        //    //Get SPL detail data
                        //    foreach (string dataSpl in detailSpl)
                        //    {
                        //        if (dataSpl.Equals("SPL") || dataSpl.Equals("ENDTRNS"))
                        //        {
                        //            actualDataSpl += "\n";
                        //        }
                        //        actualDataSpl += dataSpl + "~";

                        //        actualDataSpl = actualDataSpl.Replace("\"", string.Empty);
                        //    }

                        //    actualDataSpl = actualDataSpl.Replace("\n\n", "\n");

                        //    string FinalData = string.Empty;

                        //    string newTrns = string.Empty;
                        //    string newSpl = string.Empty;

                        //    for (int c = 0; c < actualDataSpl.Length; c++)
                        //    {
                        //        int indexSpl = actualDataSpl.IndexOf("SPL");

                        //        for (int t = 0; t < actualData.Length; t++)
                        //        {
                        //            int indexTrns = actualData.IndexOf("TRNS");
                        //            actualData = actualData.Substring(indexTrns);
                        //            if (actualData.Contains("\n"))
                        //            {
                        //                int indexNewLineTrns = actualData.IndexOf("\n");

                        //                //Get first TRNS line
                        //                if (indexTrns >= 1)
                        //                    newTrns = actualData.Substring(indexTrns - 1, indexNewLineTrns);
                        //                else
                        //                    newTrns = actualData.Substring(indexTrns, indexNewLineTrns);
                        //            }
                        //            else
                        //                //If only one TRNS line present then assign actualData to newTrns.
                        //                newTrns = actualData;
                        //            if (actualDataSpl.Contains("ENDTRNS"))
                        //            {

                        //                int indexSPL = actualDataSpl.IndexOf("SPL");
                        //                if (indexSpl < 0)
                        //                {
                        //                    actualDataSpl = actualDataSpl.Remove(0, actualDataSpl.IndexOf("\n"));
                        //                }
                        //                else
                        //                {
                        //                    actualDataSpl = actualDataSpl.Remove(0, indexSPL);

                        //                    int indexEndtrns = actualDataSpl.IndexOf("ENDTRNS");
                        //                    if (indexSpl > 0)
                        //                    {
                        //                        actualSplEndtrns = actualDataSpl.Substring(indexSpl - 1, indexEndtrns);
                        //                    }
                        //                    else
                        //                        actualSplEndtrns = actualDataSpl.Substring(indexSpl, indexEndtrns);
                        //                    // actualSplEndtrns = actualSplEndtrns.Remove(0, actualSplEndtrns.IndexOf("SPL"));
                        //                    // int start = actualSplEndtrns.IndexOf("SPL");


                        //                    for (int s = 0; s < actualSplEndtrns.Length; s++)//actualSplEndtrns.Length
                        //                    {
                        //                        //  actualSplEndtrns = actualSplEndtrns.Replace("\n\n", "\n"); //Bug 537 Sometimes IFF file has More than two empty lines are added then preview mismatches
                        //                        int indexNewSpl = actualSplEndtrns.IndexOf("SPL");//
                        //                        actualSplEndtrns = actualSplEndtrns.Substring(indexNewSpl);

                        //                        indexNewSpl = actualSplEndtrns.IndexOf("SPL");
                        //                        if (actualSplEndtrns.Contains("\n"))
                        //                        {
                        //                            int indexNewLine = actualSplEndtrns.IndexOf("\n");
                        //                            if (indexNewSpl >= 1)
                        //                                newSpl = actualSplEndtrns.Substring(indexNewSpl - 1, indexNewLine);
                        //                            else
                        //                                newSpl = actualSplEndtrns.Substring(indexNewSpl, indexNewLine);
                        //                        }
                        //                        else
                        //                            newSpl = actualSplEndtrns.ToString();
                        //                        //Append SPL data to TRNS data
                        //                        FinalData += newTrns + newSpl;

                        //                        FinalData = FinalData.Remove(FinalData.Length - 1);
                        //                        FinalData = FinalData.TrimEnd();

                        //                        //Remove TRNS, SPL, ENDTRNS words.
                        //                        FinalData = FinalData.Replace("TRNS~", string.Empty).Replace("SPL~", string.Empty).Replace("ENDTRNS~", string.Empty);

                        //                        object[] textRowData = new object[textDataFromSheet.Columns.Count];
                        //                        textRowData = Regex.Split(FinalData, "~");

                        //                        #region Parsing Values


                        //                        if (IsJournal)
                        //                        {
                        //                            //AMOUNT : negative to positive
                        //                            if (textDataFromSheet.Columns.Contains("transDEBIT"))
                        //                            {
                        //                                int index = textDataFromSheet.Columns.IndexOf("transDEBIT");
                        //                                string amt = textRowData[index].ToString().Trim();
                        //                                if (amt != string.Empty)
                        //                                {
                        //                                    if (amt.StartsWith("-"))
                        //                                    {
                        //                                        textRowData[index + 1] = textRowData[index].ToString().Replace("-", string.Empty);
                        //                                        textRowData[index] = "";
                        //                                    }
                        //                                    else
                        //                                    {
                        //                                        textRowData[index] = textRowData[index].ToString().Replace("-", string.Empty);

                        //                                        textRowData[index + 1] = "";
                        //                                    }

                        //                                }

                        //                            }

                        //                            //AMOUNTLine : negative to positive
                        //                            if (textDataFromSheet.Columns.Contains("splDEBIT"))
                        //                            {
                        //                                int index = textDataFromSheet.Columns.IndexOf("splDEBIT");
                        //                                string amt = textRowData[index].ToString().Trim();
                        //                                if (amt != string.Empty)
                        //                                {
                        //                                    if (amt.StartsWith("-"))
                        //                                    {
                        //                                        textRowData[index + 1] = textRowData[index].ToString().Replace("-", string.Empty);
                        //                                        textRowData[index] = "";
                        //                                    }
                        //                                    else
                        //                                    {
                        //                                        textRowData[index] = textRowData[index].ToString().Replace("-", string.Empty);

                        //                                        textRowData[index + 1] = "";
                        //                                    }
                        //                                }
                        //                            }
                        //                        }
                        //                        else
                        //                        {
                        //                            //AMOUNT : negative to positive
                        //                            if (textDataFromSheet.Columns.Contains("AMOUNT"))
                        //                            {
                        //                                int index = textDataFromSheet.Columns.IndexOf("AMOUNT");
                        //                                string amt = textRowData[index].ToString().Trim();
                        //                                if (amt != string.Empty)
                        //                                {
                        //                                    if (amt.StartsWith("-"))
                        //                                    {
                        //                                        textRowData[index] = textRowData[index].ToString().Replace("-", string.Empty);
                        //                                    }
                        //                                }

                        //                            }

                        //                            //AMOUNTLine : negative to positive
                        //                            if (textDataFromSheet.Columns.Contains("AMOUNTLine"))
                        //                            {
                        //                                int index = textDataFromSheet.Columns.IndexOf("AMOUNTLine");
                        //                                string amt = textRowData[index].ToString().Trim();
                        //                                if (amt != string.Empty)
                        //                                {
                        //                                    if (amt.StartsWith("-"))
                        //                                    {
                        //                                        textRowData[index] = textRowData[index].ToString().Replace("-", string.Empty);
                        //                                    }
                        //                                }
                        //                            }
                        //                        }

                        //                        //TOPRINT : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("TOPRINT"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("TOPRINT");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }

                        //                        }

                        //                        //TOPRINTLine : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("TOPRINTLine"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("TOPRINTLine");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }
                        //                        }

                        //                        //NAMEISTAXABLE : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("NAMEISTAXABLE"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("NAMEISTAXABLE");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }
                        //                        }

                        //                        //NAMEISTAXABLELine : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("NAMEISTAXABLELine"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("NAMEISTAXABLELine");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }
                        //                        }

                        //                        //TAXABLE : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("TAXABLE"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("TAXABLE");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }
                        //                        }                        

                        //                        //TAXABLELine : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("TAXABLELine"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("TAXABLELine");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }
                        //                        }

                        //                        //BUG;::571

                        //                        //if (textDataFromSheet.Columns.Contains("DOCNUM"))
                        //                        //{
                        //                        //    int index = textDataFromSheet.Columns.IndexOf("DOCNUM");
                        //                        //    if (textRowData[index].ToString() != string.Empty)
                        //                        //    {
                        //                        //        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                        //    }
                        //                        //}

                        //                        //if (textDataFromSheet.Columns.Contains("DOCNUMLine"))
                        //                        //{
                        //                        //    int index = textDataFromSheet.Columns.IndexOf("DOCNUM");
                        //                        //    if (textRowData[index].ToString() != string.Empty)
                        //                        //    {
                        //                        //        textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                        //    }
                        //                        //}

                        //                        //CLEAR : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("CLEAR"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("CLEAR");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }

                        //                        }

                        //                        //CLEARLine : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("CLEARLine"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("CLEARLine");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }

                        //                        }

                        //                        //PAID : Y = true, N = false
                        //                        if (textDataFromSheet.Columns.Contains("PAID"))
                        //                        {
                        //                            int index = textDataFromSheet.Columns.IndexOf("PAID");
                        //                            if (textRowData[index].ToString() != string.Empty)
                        //                            {
                        //                                textRowData[index] = textRowData[index].ToString().Replace("Y", "true").Replace("N", "false");
                        //                            }

                        //                        }

                        //                        //DATE : MM/DD/YY to regional format
                        //                        if (textDataFromSheet.Columns.Contains("DATE"))
                        //                        {
                        //                            DateTime NewDt = new DateTime();
                        //                            int index = textDataFromSheet.Columns.IndexOf("DATE");
                        //                            string dt = textRowData[index].ToString();
                        //                            if (dt != string.Empty)
                        //                            {
                        //                                if (!DateTime.TryParse(dt, out NewDt))
                        //                                {
                        //                                    DateTime dttest = new DateTime();
                        //                                    bool IsValid = false;

                        //                                    try
                        //                                    {
                        //                                        dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                        //                                        IsValid = true;
                        //                                    }
                        //                                    catch
                        //                                    {
                        //                                        IsValid = false;
                        //                                    }
                        //                                }
                        //                                else
                        //                                {
                        //                                    NewDt = Convert.ToDateTime(dt);
                        //                                    dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                        //                                }
                        //                                textRowData[index] = dt;
                        //                            }

                        //                        }

                        //                        //DATELine : MM/DD/YY to regional format
                        //                        if (textDataFromSheet.Columns.Contains("DATELine"))
                        //                        {
                        //                            DateTime NewDt = new DateTime();
                        //                            int index = textDataFromSheet.Columns.IndexOf("DATELine");
                        //                            string dt = textRowData[index].ToString();
                        //                            if (dt != string.Empty)
                        //                            {
                        //                                if (!DateTime.TryParse(dt, out NewDt))
                        //                                {
                        //                                    DateTime dttest = new DateTime();
                        //                                    bool IsValid = false;

                        //                                    try
                        //                                    {
                        //                                        dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                        //                                        IsValid = true;
                        //                                    }
                        //                                    catch
                        //                                    {
                        //                                        IsValid = false;
                        //                                    }
                        //                                }
                        //                                else
                        //                                {
                        //                                    NewDt = Convert.ToDateTime(dt);
                        //                                    dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                        //                                }
                        //                                textRowData[index] = dt;
                        //                            }

                        //                        }


                        //                        //DUEDATE : MM/DD/YY to regional format
                        //                        if (textDataFromSheet.Columns.Contains("DUEDATE"))
                        //                        {
                        //                            DateTime NewDt = new DateTime();
                        //                            int index = textDataFromSheet.Columns.IndexOf("DUEDATE");
                        //                            string dt = textRowData[index].ToString();
                        //                            if (dt != string.Empty)
                        //                            {
                        //                                if (!DateTime.TryParse(dt, out NewDt))
                        //                                {
                        //                                    DateTime dttest = new DateTime();
                        //                                    bool IsValid = false;

                        //                                    try
                        //                                    {
                        //                                        dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                        //                                        IsValid = true;
                        //                                    }
                        //                                    catch
                        //                                    {
                        //                                        IsValid = false;
                        //                                    }
                        //                                }
                        //                                else
                        //                                {
                        //                                    NewDt = Convert.ToDateTime(dt);
                        //                                    dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                        //                                }
                        //                                textRowData[index] = dt;
                        //                            }
                        //                        }

                        //                        //SERVICEDATE : MM/DD/YY to regional format
                        //                        if (textDataFromSheet.Columns.Contains("SERVICEDATE"))
                        //                        {
                        //                            DateTime NewDt = new DateTime();
                        //                            int index = textDataFromSheet.Columns.IndexOf("SERVICEDATE");
                        //                            string dt = textRowData[index].ToString();
                        //                            if (dt != string.Empty)
                        //                            {
                        //                                if (!DateTime.TryParse(dt, out NewDt))
                        //                                {
                        //                                    DateTime dttest = new DateTime();
                        //                                    bool IsValid = false;

                        //                                    try
                        //                                    {
                        //                                        dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                        //                                        IsValid = true;
                        //                                    }
                        //                                    catch
                        //                                    {
                        //                                        IsValid = false;
                        //                                    }
                        //                                }
                        //                                else
                        //                                {
                        //                                    NewDt = Convert.ToDateTime(dt);
                        //                                    dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                        //                                }
                        //                                textRowData[index] = dt;
                        //                            }
                        //                        }



                        //                        #endregion

                        //                        //LoadDataRow A
                        //                        if (textRowData.Length > textDataFromSheet.Columns.Count)
                        //                        {
                        //                            while (textRowData.Length != textDataFromSheet.Columns.Count)
                        //                            {
                        //                                //object[] textRowData1 = new object[header.ToArray().Length - 1
                        //                                //  textRowData.Length = textRowData.Length - 1;
                        //                                Array.Resize(ref textRowData, textDataFromSheet.Columns.Count);

                        //                            }
                        //                            textDataFromSheet.LoadDataRow(textRowData, true);

                        //                        }
                        //                        else
                        //                        {
                        //                            textDataFromSheet.LoadDataRow(textRowData, true);
                        //                        }

                        //                        FinalData = string.Empty;
                        //                        textRowData = null;
                        //                        //}
                        //                        //Bug 537
                        //                        actualSplEndtrns = actualSplEndtrns.Remove(0, newSpl.Length);

                        //                        //actualSplEndtrns = actualSplEndtrns.Replace(newSpl, string.Empty);


                        //                        //if (actualSplEndtrns == "\n") { actualSplEndtrns = string.Empty; }
                        //                        //actualSplEndtrns = actualSplEndtrns.Replace("\n", string.Empty);
                        //                        actualDataSpl = actualDataSpl.Remove(1, actualDataSpl.IndexOf("SPL"));
                        //                        int indexTile = actualDataSpl.IndexOf("~");
                        //                        actualDataSpl = actualDataSpl.Substring(indexTile + 1);
                        //                        // actualDataSpl = actualDataSpl.Replace(newSpl, string.Empty);
                        //                        //actualData = actualData.Replace(newTrns, string.Empty);

                        //                    }
                        //                }

                        //            }
                        //            actualData = actualData.Remove(0, newTrns.Length);
                        //            if (!actualDataSpl.Contains("SPL"))
                        //            {
                        //                actualDataSpl = string.Empty;
                        //            }
                        //            else
                        //            {
                        //                int indexSPLTile = actualDataSpl.IndexOf("SPL");
                        //                actualDataSpl = actualDataSpl.Substring(indexSPLTile);
                        //            }
                        //        }
                        //    }

                        //#endregion


                            //#region Display Actual detail data of TIMEACT

                            //string actualTimeact = string.Empty;
                            //string newTime = string.Empty;

                            ////Get TIMEACT detail data
                            //foreach (string dataTime in detailTimeact)
                            //{
                            //    if (dataTime.Equals("TIMEACT"))
                            //    {
                            //        actualTimeact += "\n";
                            //    }
                            //    actualTimeact += dataTime + "~";
                            //}

                            //for (int c = 0; c < actualTimeact.Length; c++)
                            //{
                            //    int indexTime = actualTimeact.IndexOf("TIMEACT");

                            //    actualTimeact = actualTimeact.Substring(indexTime);
                            //    if (actualTimeact.Contains("\n"))
                            //    {
                            //        int indexNewLine = actualTimeact.IndexOf("\n");

                            //        newTime = actualTimeact.Substring(indexTime - 1, indexNewLine);
                            //        FinalData += newTime;
                            //        actualTimeact = actualTimeact.Replace(newTime, string.Empty);
                            //    }
                            //    else
                            //    {
                            //        FinalData += actualTimeact;
                            //        actualTimeact = string.Empty;
                            //    }
                            //    FinalData = FinalData.Remove(FinalData.Length - 1);

                            //    FinalData = FinalData.Replace("TIMEACT~", string.Empty);

                            //    object[] textRowData = Regex.Split(FinalData, "~");

                            //    #region Parsing logic for Date

                            //    //DATE : MM/DD/YY to regional format
                            //    if (textDataFromSheet.Columns.Contains("DATE"))
                            //    {
                            //        DateTime NewDt = new DateTime();
                            //        int index = textDataFromSheet.Columns.IndexOf("DATE");
                            //        string dt = textRowData[index].ToString();
                            //        if (dt != string.Empty)
                            //        {
                            //            if (!DateTime.TryParse(dt, out NewDt))
                            //            {
                            //                DateTime dttest = new DateTime();
                            //                bool IsValid = false;

                            //                try
                            //                {
                            //                    dttest = DateTime.ParseExact(dt, "MM/dd/yy", CultureInfo.InvariantCulture);
                            //                    IsValid = true;
                            //                }
                            //                catch
                            //                {
                            //                    IsValid = false;
                            //                }
                            //            }
                            //            else
                            //            {
                            //                NewDt = Convert.ToDateTime(dt);
                            //                dt = DateTime.Parse(dt).ToString("yyyy-MM-dd");
                            //            }
                            //            textRowData[index] = dt;
                            //        }

                            //    }
                            //    #endregion

                            //    //LoadDataRow B
                            //    if (textRowData.Length > textDataFromSheet.Columns.Count)
                            //    {
                            //        while (textRowData.Length != textDataFromSheet.Columns.Count)
                            //        {
                            //            //object[] textRowData1 = new object[header.ToArray().Length - 1
                            //            //  textRowData.Length = textRowData.Length - 1;
                            //            Array.Resize(ref textRowData, textDataFromSheet.Columns.Count);

                            //        }
                            //        textDataFromSheet.LoadDataRow(textRowData, true);

                            //    }
                            //    else
                            //    {
                            //        textDataFromSheet.LoadDataRow(textRowData, true);
                            //    }

                            //    FinalData = string.Empty;
                            //    textRowData = null;
                            //}

                        //}
                        //catch (Exception ex)
                        //{
                        //    MessageBox.Show(ex.ToString());
                        //}
                        //#endregion

                        #endregion old iif parsing code

                        //this.textDataFromSheet.DefaultView.RowFilter = string.Format("transTRNSTYPE = '{0}'", CurrentIIFtransName);

                        //P Axis 13.1 : issue 684
                        CurrentIIFtransName = CurrentIIFtransName.Replace("\"", "");
                        if (textDataFromSheet.Columns.Contains("transTRNSTYPE"))
                        {
                            textDataFromSheet.DefaultView.RowFilter = string.Format("transTRNSTYPE = '{0}'", CurrentIIFtransName);
                        }
                        if (textDataFromSheet.Columns.Contains("transTRANSTYPE"))
                        {
                            textDataFromSheet.DefaultView.RowFilter = string.Format("transTRANSTYPE = '{0}'", CurrentIIFtransName);
                        }
                        //P Axis 13.1 : issue 684 END

                        for (int dr = 0; dr < textDataFromSheet.Rows.Count; dr++)
                        {
                            //P Axis 13.1 : issue 684
                            var myValue = (Object)null;
                            if (textDataFromSheet.Columns.Contains("transTRNSTYPE"))
                            {
                                 myValue = textDataFromSheet.Rows[dr]["transTRNSTYPE"];
                            }
                            if (textDataFromSheet.Columns.Contains("transTRANSTYPE"))
                            {
                                 myValue = textDataFromSheet.Rows[dr]["transTRANSTYPE"];
                            }

                            if (CurrentIIFtransName != null)
                            {
                                string currentiiftrans = CurrentIIFtransName.TrimStart('"').TrimEnd('"');
                                //issue 709
                                if (myValue != null)
                                {
                                    if (myValue.ToString().Trim() != currentiiftrans.ToString().Trim())
                                    {
                                        textDataFromSheet.Rows[dr].Delete();
                                        dr = 0;
                                    }
                                }
                            }
                            //P Axis 13.1 : issue 684 END
                        }
                        /// 615 New change
                        /// Repeating the loop for deleting the first row which not get deleted in first loop
                        for (int dr = 0; dr < textDataFromSheet.Rows.Count; dr++)
                        {
                            //P Axis 13.1 : issue 684 
                            var myValue = (Object)null;
                            if (textDataFromSheet.Columns.Contains("transTRNSTYPE"))
                            {
                                myValue = textDataFromSheet.Rows[dr]["transTRNSTYPE"];
                            }
                            if (textDataFromSheet.Columns.Contains("transTRANSTYPE"))
                            {
                                myValue = textDataFromSheet.Rows[dr]["transTRANSTYPE"];
                            }
                           
                            string currentiiftrans = CurrentIIFtransName.TrimStart('"').TrimEnd('"');
                            //issue 709
                            if (myValue != null)
                            {
                                if (myValue.ToString().Trim() != currentiiftrans.ToString().Trim())
                                {
                                    this.textDataFromSheet.Rows[dr].Delete();
                                    dr = 0;
                                }
                            }
                            //P Axis 13.1 : issue 684 END
                        }
                            return textDataFromSheet;
                    }
                    else
                    {
                        //628

                        // Create connection object by using the preceding connection string.
                        //oleDBConn = new OleDbConnection(connString);
                        //// Open connection with the database.
                        //oleDBConn.Open();

                        //OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);

                        //if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                        //{
                        //    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                        //}
                        System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();
                        //oleDBAdapter.Fill(excellDataFromSheet);

                        excellDataFromSheet = ConvertToDataTable(this.m_filePath);
                        return excellDataFromSheet;
                    }
                    //return textDataFromSheet;

                }
                #endregion
              
                #region
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                {
                    // List<object> result = new List<object>();
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                    if (this.m_FMT != "Fixed")
                    {
                        textDataFromSheet = new System.Data.DataTable();

                        char[] delimiter = new char[1];
                        if (this.m_FMT == "TabDelimited")
                            delimiter[0] = '\t';
                        if (this.m_FMT == "CSVDelimited")
                            delimiter[0] = ',';
                        if (this.m_FMT == "Delimited(|)")
                            delimiter[0] = '|';


                        string strsymbol = "";
                        string[] alllines = File.ReadAllLines(this.m_filePath);
                        List<string> tmp = new List<string>(alllines);

                        if (tmp.Contains(strsymbol))
                        {
                            string newline = strsymbol;
                            int numIdx = Array.IndexOf(alllines, newline);
                            tmp.RemoveAt(numIdx);
                            alllines = tmp.ToArray();
                        }
                        string tempData = string.Empty;

                        for (int cnt = 0; cnt < alllines.Length; cnt++)
                        {

                            if(cnt% callDoEventsOn == 0)
                            {
                                Application.DoEvents();

                            }

                            char[] data = new char[alllines[cnt].Length];
                            data = alllines[cnt].ToCharArray();
                            int count = 0;

                            if (data.Length > 0)
                            {

                                for (int x = 0; x < data.Length; x++)
                                {
                                    if (data[x] == '"')
                                    {
                                        count++;
                                    }
                                }
                                //}
                                if (count % 2 == 0)
                                {
                                    tempData += alllines[cnt] + "~";
                                }
                                else
                                {
                                    tempData += alllines[cnt++] + alllines[cnt] + "~";
                                }
                            }
                        }

                        char[] symbol = new char[1];
                        symbol[0] = '~';
                        string[] lines = tempData.Split(symbol);
                        var col_header = new string[] { };

                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (i == 0)
                            {
                                col_header = lines[i].Split(delimiter);
                                for (int p = 0; p < col_header.Length; p++)
                                {
                                    int no = 2;
                                    string temp_str = col_header[p];
                                    for (int q = p + 1; q < col_header.Length; q++)
                                    {
                                        if (temp_str == col_header[q])
                                        {
                                            col_header[q] = temp_str + " (" + no + ")";
                                            no++;

                                        }

                                    }

                                }
                            }
                        }
                        for (int i = 0; i < lines.Length; i++)
                        {
                            if (i == 0)
                            {

                                //    string[] columns = lines[i].Split(delimiter);
                                //    columnCount = columns.Length;
                                foreach (string column in col_header)
                                {
                                    //try
                                    //{
                                    //if (this.m_HDR)
                                    textDataFromSheet.Columns.Add(column);
                                    //    else
                                    //        textDataFromSheet.Columns.Add();
                                    //}
                                    //catch
                                    //{
                                    //    try
                                    //    {
                                    //        textDataFromSheet.Columns.Add();
                                    //    }
                                    //    catch
                                    //    {
                                    //        textDataFromSheet.Columns.Add();
                                    //    }
                                    //}
                                }
								//628
                                if (this.m_HDR == false)
                                {
                                    for (int p = 0; p < col_header.Length; p++)
                                    {
                                        textDataFromSheet.Columns[p].ColumnName = "Column" + (p + 1);
                                    }
                                }
                                else { continue; }
                                
                                   
                            }
                            #region String Parser Logic

                            //Converts all rows into Object array
                            //This is String parser logic for handling invalid files.
                            if (lines[i] != string.Empty)
                            {
                                //for (int k = 1; k < lines[i]-1; k++)

                                //{
                                string str = lines[i].Replace("\"\"", "^");
                                string middleStr = "";


                                if (str.Contains("\""))
                                {

                                    int index = str.IndexOf("\"") + 1;
                                    int indexaa = str.IndexOf("\",");

                                    if (middleStr == "")
                                    {
                                    }
                                    else
                                    {
                                        middleStr = str.Substring(index, indexaa - index);
                                        str = str.Replace(middleStr, "~");

                                        for (int m = 0; m < middleStr.Length; m++)
                                        {
                                            middleStr = middleStr.Replace("^", "\"");
                                        }
                                    }
                                }

                                //Axis 12 bug 510
                                string[] array = (string[])str.Split(delimiter);
                                //Axis 647
                                if (array.Count() != textDataFromSheet.Columns.Count)
                                {
                                    Array.Resize(ref array, textDataFromSheet.Columns.Count);
                                }
                                //Axis 647 end
                                string strng = "";
                                char[] chrarray = strng.ToCharArray();
                                String str2 = "";
                                int lengtharr = textDataFromSheet.Columns.Count;
                                string[] newarray = new string[lengtharr];

                                for (int j = 0; j < textDataFromSheet.Columns.Count; j++)
                                {
								
									   //Axis 641
                    if (j % callDoEventsOn == 0)
                    {
                        Application.DoEvents();

                    }
                    //Axis 641 End
                                    //Axis 647
                                    if (array[j] != null)
                                    //Axis 647 end
                                    {
                                        if (array[j].Contains("~"))
                                        {
                                            array[j] = middleStr;
                                        }
                                        if (array[j].Equals("^") && array[j].Length == 1)
                                        {
                                            array[j] = "";
                                        }

                                        strng = array[j].ToString();
                                        chrarray = strng.ToCharArray();
                                        for (int z = 0; z < chrarray.Length; z++)
                                        {
                                            if (chrarray[z] == '"' || chrarray[z] == '\"')
                                            {
                                                chrarray[z] = ' ';
                                            }
                                            str2 = new String(chrarray);
                                        }

                                        newarray[j] = str2;
                                        str2 = string.Empty;
                                    }
                                }

                                int lengtharr1 = textDataFromSheet.Columns.Count;

                                string[] objTextFile = new string[lengtharr1];
                                for (int temp = 0; temp < textDataFromSheet.Columns.Count; temp++)
                                {
                                    objTextFile[temp] = newarray[temp];
                                }

                                textDataFromSheet.LoadDataRow(objTextFile, true);
                            }

                        }
                            #endregion
                        return textDataFromSheet;
                    }
                    else
                    {
                        //Bug 628

                        // Create connection object by using the preceding connection string.
                        //oleDBConn = new OleDbConnection(connString);
                        //// Open connection with the database.
                        //oleDBConn.Open();

                        //OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);

                        //if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                        //{
                        //    oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTFILEQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                        //}
                        //System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();
                        //oleDBAdapter.Fill(excellDataFromSheet);


                        System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();
                        //oleDBAdapter.Fill(excellDataFromSheet);

                        excellDataFromSheet = ConvertToDataTable(this.m_filePath);
                        return excellDataFromSheet;
                    }
                    //return textDataFromSheet;

                }
                #endregion
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                {
                    #region Axis 10.2 csv file parsing to get column headers and data
                    textDataFromSheet = new System.Data.DataTable();

                    //bug 505

                    //Cursor = Cursors.WaitCursor;
                    try
                    {
                        //dataGridView1.Rows.Clear();
                        List<string> columns = new List<string>();
                        int cnt = 0;
                        using (var reader = new CsvFileReader(this.m_filePath))
                        {
                            while (reader.ReadRow(columns))
                            {
                                if (cnt != 0)
                                {
                                    textDataFromSheet.LoadDataRow(columns.ToArray(), true);
                                }
                                else if (cnt == 0)
                                {
                                    int i = 0;
                                    var temp_header = columns.ToArray();
                                   
                                    for (int p = 0; p < temp_header.Length; p++)
                                    {
                                        int no = 2;
                                        string temp_str = temp_header[p];
                                        for (int q = p + 1; q < temp_header.Length; q++)
                                        {
                                            if (temp_str.Trim() == temp_header[q].Trim())
                                            {
                                                temp_header[q] = temp_str + " (" + no + ")";
                                                no++;

                                            }

                                        }

                                    }

                                    ////previous code
                                    //for (int c = 0; c < temp_header.Length; c++)
                                    //{
                                    //    textDataFromSheet.Columns.Add(temp_header[c]);
                                    //}

                                    if (cnt == 0)
                                    {
                                        for (int c = 0; c < temp_header.Length; c++)
                                        {
                                            textDataFromSheet.Columns.Add(temp_header[c]);
                                        }
                                        if (this.m_HDR == false)
                                        {
                                            for (int j = 0; j < temp_header.Length; j++)
                                            {
                                                textDataFromSheet.Columns[j].ColumnName = "Column" + (j + 1);
                                            }
                                        }
                                        else
                                        {

                                            cnt++;
                                            continue;
                                        }
                                        if (temp_header[i] != string.Empty)
                                        {                                            
                                                if (i == 0)
                                                {
                                                    textDataFromSheet.LoadDataRow(temp_header.ToArray(), true);
                                                }
                                                else { break; }
                                           
                                        }
                                    }
                                }
                                cnt++;
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format("Error reading from {0}.\r\n\r\n{1}", this.m_filePath, ex.Message));
                    }
                    finally
                    {

                    }
                    return textDataFromSheet;
                    #endregion
                }
                else
                {
                    //if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                    //{


                    #region For Unformated Xls file

                    System.Data.DataTable excellDataFromSheet = new System.Data.DataTable();

                    DataTable dtCloned = new DataTable();

                    gemboxused = 1;
                    //637
                    SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");

                    try
                    {
                        // Axis 12.0 bug 510
                        FileInfo flinfo = new FileInfo(this.m_filePath);
                        string Flname = flinfo.Name;
                        flinfo.GetAccessControl();

                        string extension = flinfo.Extension;
                        ExcelFile ef = ExcelFile.Load(this.m_filePath);
                        foreach (ExcelWorksheet sheet in ef.Worksheets)
                        {
                            if (sheet.Name == sheetName)
                            {
                                int colcount = sheet.CalculateMaxUsedColumns();
                                int usedCols = 0;
                                if (extension.ToLower() == ".ods")
                                {
                                    for (int i = 0; i < colcount; i++)
                                    {
                                        
                                        if (sheet.Rows[0].Cells[i].Value != null)
                                        { usedCols++; }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    usedCols = colcount;
                                }
                                excellDataFromSheet = sheet.CreateDataTable(new CreateDataTableOptions()
                                 {
                                     ColumnHeaders = true,
                                     StartRow = 0,

                                     NumberOfColumns = usedCols,
                                     NumberOfRows = sheet.Rows.Count,
                                     ExtractDataOptions = ExtractDataOptions.StopAtFirstEmptyRow,
                                     Resolution = ColumnTypeResolution.StringInvariantCulture
                                });

                                //bug 543 remove empty columns from datatable
                                int dtcol = 0;

                                int nullcolincr = 0;
                                int excelcol = 0;
                                for (int temp = 0; temp < usedCols; temp++)
                                {
                                    //Axis 641
                                    if (temp % callDoEventsOn == 0)
                                    {
                                        Application.DoEvents();
                                    }
                                    //Axis 641 End


                                    if (sheet.Rows[0].Cells[temp].Value == null)
                                    {
                                        excellDataFromSheet.Columns.RemoveAt(excelcol - nullcolincr);
                                       
                                        if (dtcol != 0)
                                        {
                                            nullcolincr++;
                                            excelcol++;
                                        }
                                    }
                                    else
                                    {
                                        dtcol++;
                                        excelcol++;
                                    }
                                }
                                //637
                                try
                                {
                                    //P Axis 13.1 : issue 693
                                    string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt","MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                                                            "MM/dd/yyyy hh:mm:ss tt","M/d/yyyy h:mm:ss tt","M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
                                                            "M/d/yyyy h:mm", "M/d/yyyy h:mm","M/d/yyyy h:mm tt", "M/d/yyyy h:mm tt","dd/MM/yyyy hh:mm:ss",
                                                            "dd-MM-yyyy hh:mm:ss","dd-MM-yyyy hh:mm:ss tt","MM-dd-yyyy hh:mm:ss tt",
                                                            "MM-dd-yyyy hh:mm:ss",
                                                            "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm", "dd/MM/yyyy hh:mm:ss tt"};
                                 
                                    for (int colindex = 0; colindex < excellDataFromSheet.Columns.Count; colindex++)
                                    {
                                        DateTime dateValue;
                                        for (int rowindex = 0; rowindex < excellDataFromSheet.Rows.Count; rowindex++)
                                        {
                                            string value = excellDataFromSheet.Rows[rowindex][colindex].ToString();
                                            if (DateTime.TryParseExact(value, formats, new CultureInfo("en-US"), DateTimeStyles.None, out dateValue))
                                            {
                                                if (value.Contains("12:00:00") || value.Contains("00:00:00"))
                                                {
                                                    DateTime date = dateValue;
                                                    string shortDate = date.ToShortDateString();
                                                    excellDataFromSheet.Rows[rowindex][colindex] = shortDate;
                                                }
                                            }
                                            else if (value == String.Empty || value == null)
                                            {
                                                rowindex++;
                                            }
                                            else
                                            {
                                                colindex++;
                                                rowindex = -1;
                                            }
                                        }

                                    } 

                                  
								    //P Axis 13.1 : issue 693 END
 

                                    //dtCloned = excellDataFromSheet.Clone();

                                    //for (int i = 0; i < excellDataFromSheet.Columns.Count; i++)
                                    //{
                                    //    dtCloned.Columns[i].DataType = typeof(string);
                                    //}

                                    //foreach (DataRow row in excellDataFromSheet.Rows)
                                    //{
                                    //    dtCloned.ImportRow(row);
                                    //}

                                    //foreach (DataRow row in dtCloned.Rows)
                                    //{
                                    //    for (int i = 0; i < dtCloned.Rows.Count; i++)
                                    //    {

                                    //        for (int j = 0; j < row.ItemArray.Count(); j++)
                                    //        {
                                    //            DateTime dateValue;

                                    //            if (DateTime.TryParseExact(row[j].ToString(), formats, new CultureInfo("en-US"), DateTimeStyles.None, out dateValue))
                                    //            {
                                    //                DateTime date = dateValue;

                                    //                string shortDate = date.ToShortDateString();

                                    //                dtCloned.Rows[i][j] = shortDate;
                                    //            }

                                    //        }
                                    //    }

                                    //}
                                }

                                catch (Exception ex)
                                {
                                //MessageBox.Show(ex.ToString());
                            }
                                

                                //}

                                break;
                            }
                        }
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        string sheetnametest = System.IO.Path.GetFileName(this.m_filePath);
             
                      
                    }
                    catch
                    {

                    }
                    #endregion

                    return excellDataFromSheet;
                }

            }
            catch (OleDbException OleEx)
            {
                if (OleEx.Message.Contains(ERROR_STRING))
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER004");
                }
                else if (OleEx.ErrorCode == -2147467259)
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER003");

                }
                else
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER001");

                }

            }
            catch (Exception ex)
            {
                if (ex.Message.Contains(DefaultError))
                {
                    //CommonUtilities.WriteErrorLog(ex.Message);
                    //CommonUtilities.WriteErrorLog(ex.StackTrace);
                    Err = DefaultError;
                    throw new TIException("Zed Axis MSG037", this.m_filePath);
                }
                else
                {
                    //CommonUtilities.WriteErrorLog(ex.Message);
                    //CommonUtilities.WriteErrorLog(ex.StackTrace);
                    throw new TIException("Zed Axis ER001");
                }

            }
            finally
            {

                try
                {
                    //628
                    //if (oleDBConn != null)
                    //{
                    //    if (oleDBConn.State.ToString() == "Open")
                    //    {
                    //        oleDBConn.Close();
                    //        oleDBConn.Dispose();
                    //    }
                    //}
                }
                catch
                { }
            }
        }
        
        private DataTable GetDataByListName(string[] lines, char[] delimiter,string listname1, string listname2)
        {
            #region
            List<string> header_name = new List<string>();
            try
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    if (lines[i].StartsWith("!" + listname1) || lines[i].StartsWith("\"!" + listname1 +"\""))
                    {
                        #region
                        //Add header data into List.
                        string col = lines[i].ToString();
                        string[] columnHead = col.Replace("\"", string.Empty).Split(delimiter);

                        int tempCol = 0;
                        int temp = 1;
                        foreach (string columnHeader in columnHead.ToList())
                        {
                            if (columnHeader.Trim().ToLower() != ("!"+ listname2) && columnHeader.Trim().ToLower() != "\"!" + listname2 +"\"")
                            {
                                if (columnHeader != "")
                                {
                                    header_name.Add(columnHeader);
                                }
                                else
                                {
                                    foreach (var item in columnHead.ToList())
                                    {
                                        if (!columnHead.Contains("Column" + temp))
                                        {
                                            header_name.Add("Column" + temp);
                                            columnHead[tempCol] = "Column" + temp;
                                            if (this.m_FMT == "TabDelimited")
                                                lines[i] = String.Join("\t", columnHead.ToList());

                                            else if (this.m_FMT == "CSVDelimited")
                                                lines[i] = String.Join(",", columnHead.ToList());

                                            else if (this.m_FMT == "Delimited(|)")
                                                lines[i] = String.Join("|", columnHead.ToList());
                                            temp++;

                                            break;
                                        }
                                        else
                                            temp++;
                                    }
                                }
                            }
                            tempCol++;
                        }
                        #endregion
                    }
                }
                foreach (string head in header_name)
                {
                    textDataFromSheet.Columns.Add(listname2 + head);
                }
                for (int i = 0; i < lines.Length; i++)
                {
                    if (i % callDoEventsOn == 0)
                    {
                        Application.DoEvents();
                    }

                    List<string> Listcolumns = new List<string>();
                    List<string> Listrows = new List<string>();

                    #region accnt
                    if (lines[i].StartsWith("!"+ listname1) || lines[i].StartsWith("\"!"+ listname1 +"\""))
                    {
                        Listcolumns = lines[i].Replace("\"", "").Split(delimiter).ToList();
                        Listcolumns.RemoveAt(0);
                       
                        for (int j = i; j < lines.Length; j++)
                        {
                            i = j;
                            if (lines[j].StartsWith("!"+ listname1) || lines[j].StartsWith("\"!"+ listname1 +"\""))
                            {
                                Listcolumns = lines[j].Replace("\"", "").Split(delimiter).ToList();
                                Listcolumns.RemoveAt(0);
                                for (int k = j + 1; k < lines.Length; k++)
                                {
                                    j = k;
                                    DataRow Listdrs = textDataFromSheet.NewRow();

                                    #region
                                    if (lines[k].StartsWith(listname1) || lines[k].StartsWith("\""+ listname1 +"\""))
                                    {
                                        var reg = new Regex("\".*?\"");
                                        var matches = reg.Matches(lines[k]);
                                        for (int tempReg = 0; tempReg < reg.Matches(lines[k]).Count; tempReg++)
                                        {
                                            foreach (Match match in reg.Matches(lines[k]))
                                            {
                                                if (match.ToString().Contains(",") || match.ToString().Contains("\t") || match.ToString().Contains("|"))
                                                {
                                                    int startIndex = match.Index;
                                                    int EndIndex = match.Index + match.Length - 1;
                                                    var aStringBuilder = new StringBuilder(lines[k]);
                                                    aStringBuilder.Remove(startIndex, match.Length);
                                                    aStringBuilder.Insert(startIndex, match.ToString().Replace(",", "~~**&&").Replace("|", "@@!!^^").Replace("\t", "$$??##"));
                                                    lines[k] = aStringBuilder.ToString();
                                                    tempReg = 0;
                                                    break;
                                                }
                                            }
                                        }
                                        Listrows = lines[k].Replace("\"", "").Split(delimiter).ToList();
                                        Listrows.RemoveAt(0);
                                        try
                                        {
                                            for (int count = 0; count < Listcolumns.Count(); count++)
                                            {
                                                if (Listrows.Count < Listcolumns.Count)
                                                {
                                                    if (Listrows.Count <= count)
                                                    {
                                                        Listrows.Insert(count, "");
                                                    }
                                                }
                                                if (Listrows.ElementAt(count).Contains("-"))
                                                {
                                                    string rowdata = Listrows.ElementAt(count);
                                                    rowdata = rowdata.Replace("-", "").Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                    Listdrs[count] = rowdata;
                                                }
                                                else
                                                {
                                                    Listdrs[count] = Listrows.ElementAt(count).Replace("\"", string.Empty).Replace("~~**&&", ",").Replace("@@!!^^", "|").Replace("$$??##", "\t");
                                                }
                                            }
                                            textDataFromSheet.Rows.Add(Listdrs.ItemArray);
                                            #endregion
                                        }
                                        catch (Exception ex)
                                        {
                                            MessageBox.Show(ex.Message);
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
            }
            return textDataFromSheet;
            #endregion
        }

        //628
        public DataTable ConvertToDataTable(string filePath)
        {
            DataTable tbl = new DataTable();
            string[] lines = System.IO.File.ReadAllLines(filePath);
            char[] delimiter = new char[1];
            if (this.m_FMT == "TabDelimited")
                delimiter[0] = '\t';
            if (this.m_FMT == "CSVDelimited")
                delimiter[0] = ',';
            if (this.m_FMT == "Delimited(|)")
                delimiter[0] = '|';

            var ColumnHeader = lines[0].Split(delimiter);

            for (int col = 0; col < ColumnHeader.Count(); col++)
                tbl.Columns.Add(new DataColumn(ColumnHeader[col]));

            int temp = 1;

           

            foreach (string line in lines)
            {
                if(temp!=1)
                {
                var cols = line.Split(Delimiter);

                DataRow dr = tbl.NewRow();
                for (int cIndex = 0; cIndex < 3; cIndex++)
                {
                    dr[cIndex] = cols[cIndex];
                }

                tbl.Rows.Add(dr);
                }
                temp++;

                //Axis 641
                if (temp % callDoEventsOn == 0)
                {
                    Application.DoEvents();

                }
                //Axis 641 End
            }

            return tbl;
        }

        /// <summary>
        /// Gets the first row data from file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="sheetIndex"></param>
        /// <returns></returns>
        public string[] GetFirstRow(string fileName, int sheetIndex, string trndatatype)
        {
            string[] first_row = null;
            try
            {
                string extension = System.IO.Path.GetExtension(this.m_filePath).ToLower();
                //Axis 12.0  bug 510
                if (extension == ".xlsx" || extension == ".xls" || extension == ".xlsm" || extension == ".ods")
                {
                    // _Application xlApp;
                    // Workbook xlWorkBook;
                    //  Worksheet xlWorkSheet;
                    // Range used_range;
                    //637
                    SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");
                    ExcelFile ef = new ExcelFile();
                    ef = ExcelFile.Load(this.FilePath);
                    //Axis 590
                    if (sheetIndex == -1) sheetIndex = 0;
                    var ws = ef.Worksheets[sheetIndex];
                    ExcelWorksheet sw = ef.Worksheets.Add("Conditional Formatting");
                    int columnCount = ws.CalculateMaxUsedColumns();

                    int usedCols = 0;
                    int row_no = 1, col_no = 0;
                    if (extension == ".ods")
                    {
                        for (int j = 0; j < columnCount; j++)
                        {
                            if (ws.Rows[0].Cells[j].Value != null)
                            { usedCols++; }
                            else
                            {
                                break;
                            }
                        }
                        columnCount = usedCols;
                    }

                    int i = 0;
                    for (col_no = 0; col_no < columnCount; col_no++)
                    {
                        if ((ws.Cells[0, col_no]).Value != null)
                        {
                            usedCols++;
                        }

                    }
                    //  columnCount = usedCols;
                    first_row = new string[usedCols];

                    for (col_no = 0; col_no < columnCount; col_no++)
                    {
                        if ((ws.Cells[0, col_no]).Value != null)
                        {
                            if ((ws.Cells[row_no, col_no]).Value != null)
                            {
                                first_row[i++] = (ws.Cells[row_no, col_no]).Value.ToString();
                            }
                            else
                            {
                                first_row[i++] = "";
                            }
                        }
                    }
                }
                else if (extension == ".txt")
                {
                    StreamReader sr_reader = new StreamReader(this.m_filePath);

                    string line = sr_reader.ReadLine();//This line contains the column names
                    line = sr_reader.ReadLine();//This line contains the first line of the file.

                    char[] delimiter = new char[1];
                    if (this.m_FMT == "TabDelimited")
                        delimiter[0] = '\t';
                    if (this.m_FMT == "CSVDelimited")
                        delimiter[0] = ',';
                    if (this.m_FMT == "Delimited(|)")
                        delimiter[0] = '|';

                    first_row = line.Split(delimiter);

                }
                else if (extension == ".iif")
                {
                    this.textDataFromSheet = GetDataBySheetName(trndatatype);

                    //596
                    //this.textDataFromSheet = textDataFromSheet.ToString().Replace("\"", string.Empty);

                    ArrayList arTables = new ArrayList();
                    int k = 0;
                    ///615 New change
                    //this.textDataFromSheet.DefaultView.RowFilter = string.Format("transTRNSTYPE = '{0}'", CurrentIIFtransName);
                    for (int dr = 0; dr < textDataFromSheet.Rows.Count; dr++)
                    {
                        //P Axis 13.1 : issue 683
                        var myValue = (Object)null;
                        if (textDataFromSheet.Columns.Contains("transTRNSTYPE"))
                        {
                            myValue = textDataFromSheet.Rows[dr]["transTRNSTYPE"];
                        }
                        if (textDataFromSheet.Columns.Contains("transTRANSTYPE"))
                        {
                            myValue = textDataFromSheet.Rows[dr]["transTRANSTYPE"];
                        }
                        string currentiiftrans = CurrentIIFtransName.TrimStart('"').TrimEnd('"');
                        //issue 709
                        if (myValue != null)
                        {
                            if (myValue.ToString().Trim() != currentiiftrans.ToString().Trim())
                            {
                                this.textDataFromSheet.Rows[dr].Delete();
                                dr = 0;
                            }
                        }
                        //P Axis 13.1 : issue 683 END
                    }

                    foreach (DataRow dr in this.textDataFromSheet.Rows)
                    {
                        for (int i = 0; i < dr.ItemArray.Length; i++)
                        {
                            arTables.Add(dr.ItemArray[i].ToString());
                        }
                        first_row = new string[arTables.Count];
                        for (int j = k; j < arTables.Count; j++)
                        {
                            if (arTables[j] != null)
                            {
                                first_row[k++] = arTables[j].ToString();
                            }
                            else
                            {
                                first_row[k++] = "";
                            }
                        }
                        break;
                    }
                }
                
                    //682
                else if (extension == ".csv")
                {
                    this.textDataFromSheet = GetDataBySheetName(this.m_filePath);
                    
                    first_row = new string[this.textDataFromSheet.Columns.Count + 1];
                    int index = 0;
                    first_row[index++] = "";
                    for (int col = 0; col < this.textDataFromSheet.Rows[0].ItemArray.Count(); col++)
                    {                       
                        if (this.textDataFromSheet.Rows[0].ItemArray[col].ToString() != null)
                        {
                            first_row[index++] = this.textDataFromSheet.Rows[0].ItemArray[col].ToString();
                        }
                    }
                   
                }

            }
            catch (Exception e)
            { throw e; }
            return first_row;
        }



        /// <summary>
        /// Get name columns 
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public System.Collections.ObjectModel.Collection<string> GetColumnNames(string sheetName)
        {
            int gembox = 0;
            OleDbConnection oleDBConn = null;
            try
            {
                this.m_CurrentSheetname = sheetName;
                // Connection String. Change the excel file to the file you
                // will search.

                string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                {
                    //connString = string.Format(connectionStringFormatXLSX, this.m_filePath, this.m_HDR ? "YES" : "NO");
                    connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                }
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    sheetName = System.IO.Path.GetFileName(this.m_filePath);
                    connString = string.Format(connectionStringCSV, System.IO.Path.GetDirectoryName(this.m_filePath), this.m_HDR ? "YES" : "NO", this.m_FMT);
                }
                else
                {
                    //if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                    //{
                    //    if (sheetName == string.Empty)
                    //        sheetName = this.CurrentSheetName;
                    //    if (sheetName.Contains("[") == false)
                    //    {
                    //        sheetName = "[" + sheetName + "$]";
                    //    }
                    //    else
                    //    {
                    //        sheetName += "$";
                    //    }
                    //}
                }
                // Create connection object by using the preceding connection string.
                oleDBConn = new OleDbConnection(connString);
                // Open connection with the database.
                string counter = string.Empty;
                try
                {
                    oleDBConn.Open();
                }
                catch
                {
                    counter = "1";
                }

                #region For Unformated Xls file
                //if (counter == "1")
                //{
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                    {

                        //Microsoft.Office.Interop.Excel.Application objExcel = new Microsoft.Office.Interop.Excel.Application();
                        //try
                        //{
                        //    string logDirectory = System.Windows.Forms.Application.StartupPath + "\\LOG";
                        //    try
                        //    {
                        //        if (CommonUtilities.GetInstance().OSName.Contains(EDI.Constant.Constants.xpOSName))
                        //        {
                        //            if (logDirectory.Contains("Program Files (x86)"))
                        //            {
                        //                logDirectory = logDirectory.Replace("Program Files (x86)", EDI.Constant.Constants.xpPath);
                        //            }
                        //            else
                        //            {
                        //                logDirectory = logDirectory.Replace("Program Files", EDI.Constant.Constants.xpPath);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            if (logDirectory.Contains("Program Files (x86)"))
                        //            {
                        //                logDirectory = logDirectory.Replace("Program Files (x86)", "Users\\Public\\Documents");
                        //            }
                        //            else
                        //            {
                        //                logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                        //            }
                        //        }
                        //    }
                        //    catch { }

                        //    if (!System.IO.Directory.Exists(logDirectory))
                        //    {
                        //        System.IO.Directory.CreateDirectory(logDirectory);
                        //    }
                        //    FileInfo flinfo = new FileInfo(this.m_filePath);
                        //    string Flname = flinfo.Name;
                        //    string filenewpath = logDirectory + "\\" + Flname;
                        //    File.Copy(this.m_filePath, filenewpath, true);

                        //    // Microsoft.Office.Interop.Excel.Workbook objWorkBook = objExcel.Workbooks.Open(filenewpath, Type.Missing, true, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                        //    ExcelFile ef = ExcelFile.Load(filenewpath);

                        //    string filePth = logDirectory + "\\test123456.xls";

                        //    try
                        //    {
                        //        if (File.Exists(filePth))
                        //            File.Delete(filePth);
                        //        ef.Save(filePth);
                        //    }
                        //    catch
                        //    { }

                        //    uint _ExcelPID = 0;
                        //    try
                        //    {
                        //        CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                        //    }
                        //    catch { }
                        //    try
                        //    {
                        //        //kill that process
                        //        Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                        //        excel.Kill();
                        //        excel.WaitForExit();
                        //    }
                        //    catch { }


                        //    GC.Collect();
                        //    GC.WaitForPendingFinalizers();
                        //    this.FilePath = filePth;
                        //    this.m_filePath = filePth;
                        //}
                        //catch
                        //{

                        //    uint _ExcelPID = 0;

                        //    try
                        //    {
                        //        CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                        //    }
                        //    catch { }

                        //    try
                        //    {
                        //        //kill that process
                        //        Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                        //        excel.Kill();
                        //        excel.WaitForExit();
                        //    }
                        //    catch { }

                        //    GC.Collect();
                        //    GC.WaitForPendingFinalizers();
                        //}
                        //// string connStringnew = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");                      
                        //string connStringnew = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_filePath + ";Extended Properties=Excel 12.0;";//change for bug no 457 xls and xlsx 
                        //OleDbConnection oleDBConntest = new OleDbConnection(connStringnew);
                        //// Open connection with the database.
                        //if (oleDBConn.State.ToString() != "Open")
                        //{
                        //    oleDBConn.Open();
                        //}
                        //string sheetnametest = this.CurrentSheetName;
                        //if (sheetnametest.Contains("[") == false)
                        //{
                        //    sheetnametest = "[" + sheetnametest + "$]";
                        //}
                        //else
                        //{
                        //    sheetnametest += "$";
                        //}

                        //if (sheetnametest.Contains("$"))
                        //{
                        //    sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        //}
                        //System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetnametest, null });
                        //System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                        //if (dt.Rows.Count == 0)
                        //{
                        //    if (sheetnametest.Contains("[") == false)
                        //    {
                        //        sheetnametest = "[" + sheetnametest + "$]";
                        //    }
                        //    else
                        //    {
                        //        sheetnametest += "$";
                        //    }
                        //    System.Data.DataTable dtt = new System.Data.DataTable();
                        //    if (sheetnametest.Contains("$"))
                        //    {
                        //        sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        //    }

                        //    OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetnametest), oleDBConn);
                        //    oleDBAdapter.Fill(dtt);
                        //    for (int i = 0; i < dtt.Columns.Count; i++)
                        //    {
                        //        columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName.Trim() : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME).Trim());
                        //    }

                        //}
                        //else
                        //{
                        //    for (int i = 0; i < dt.Rows.Count; i++)
                        //    {
                        //        columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString().Trim() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME).Trim());
                        //    }
                        //}
                        //oleDBConn.Close();
                        System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                        System.Data.DataTable dtGetData = GetDataBySheetName(this.CurrentSheetName);
                        for (int i = 0; i < dtGetData.Columns.Count; i++)
                        {
                            columnNames.Add(this.m_HDR ? dtGetData.Columns[i].ColumnName.Trim() : dtGetData.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME).Trim());
                        }
                        return columnNames;
                    }
                //}
                #endregion
                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif")
                {
                    //issue 709
                    System.Data.DataTable dtGetTxtData = GetDataBySheetName(this.m_CurrentSheetname);
                    DataColumnCollection dtColumns = dtGetTxtData.Columns;
                    System.Data.DataTable dtTxtFile = new System.Data.DataTable();
                    for (int j = 0; j < dtColumns.Count; j++)
                    {
                        dtTxtFile.Columns.Add(dtColumns[j].ColumnName);
                    }
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    for (int i = 0; i < dtTxtFile.Columns.Count; i++)
                    {
                        columnNames.Add(this.m_HDR ? dtTxtFile.Columns[i].ColumnName.ToString().Trim() : dtTxtFile.Columns[i].ColumnName.ToString().Replace("F", DEFUALTCOLNAME).Trim());
                    }
                    oleDBConn.Close();
                    return columnNames;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt")
                {
                    System.Data.DataTable dtGetTxtData = GetDataBySheetName(string.Empty);
                    DataColumnCollection dtColumns = dtGetTxtData.Columns;
                    System.Data.DataTable dtTxtFile = new System.Data.DataTable();
                    string[] exceldata = new string[100];

                    for (int j = 0; j < dtColumns.Count; j++)
                    {
                        dtTxtFile.Columns.Add(dtColumns[j].ColumnName);
                    }
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    for (int i = 0; i < dtTxtFile.Columns.Count; i++)
                    {

                        //exceldata[i] = this.m_HDR ? dtTxtFile.Columns[i].ColumnName.ToString() : dtTxtFile.Columns[i].ColumnName.ToString().Replace("F", DEFUALTCOLNAME);
                        columnNames.Add(this.m_HDR ? dtTxtFile.Columns[i].ColumnName.ToString().Trim() : dtTxtFile.Columns[i].ColumnName.ToString().Replace("F", DEFUALTCOLNAME).Trim());
                    }

                    //for (int i = 0; i < exceldata.Length; i++)
                    //  {
                    //     int no = 1;
                    //        for (int j = 1; i < exceldata.Length; j++)
                    //        {

                    //         if (exceldata[i] == exceldata[j])
                    //             {

                    //               exceldata[j] = exceldata[i] + no;
                    //                 no++;

                    //             }

                    //         }

                    //   }
                    //for (int i = 0; i < exceldata.Length; i++)
                    //{
                    //    columnNames.Add(exceldata[i].ToString());
                    //}


                    oleDBConn.Close();
                    return columnNames;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv")
                {
                    //OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTCOLUMNQUERY, System.IO.Path.GetFileName(this.m_filePath)), oleDBConn);
                    // System.Data.DataTable dtTxtFile = new System.Data.DataTable();
                    //oleDBAdapter.Fill(dtTxtFile);                    

                    #region For the bug 1502 (axis 6.0)

                    #endregion
                    //bug 505
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    foreach (DataColumn col in textDataFromSheet.Columns)
                    {
                        columnNames.Add(col.ColumnName.Trim());
                    }

                    return columnNames;
                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods")
                {

                    try
                    {
                        gembox = 1;
                        #region commented code of xlsx file load , do not delete this code , bug no 399
                        //637
                        SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");
                        ExcelFile ef1 = ExcelFile.Load(this.FilePath);

                        StringBuilder sb = new StringBuilder();
                        System.Collections.ObjectModel.Collection<string> excelSheets = new System.Collections.ObjectModel.Collection<string>();
                        //bug 505
                        int colcount = ef1.Worksheets[sheetName].CalculateMaxUsedColumns();
                        FileInfo flinfo = new FileInfo(this.m_filePath);
                        string Flname = flinfo.Name;
                        string extension = flinfo.Extension;

                        // string celValue = "";
                        //excelSheets.Add(celValue);
                        foreach (ExcelWorksheet sheet in ef1.Worksheets)
                        {

                            if (sheet.Name == sheetName)
                            {
                                int usedCols = 0;
                                if (extension.ToLower() == ".ods")
                                {
                                    for (int i = 0; i < colcount; i++)
                                    {
                                        if (sheet.Rows[0].Cells[i].Value != null)
                                        { usedCols++; }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < colcount; i++)
                                    {
                                        if (sheet.Rows[0].Cells[i].Value != null)
                                        {
                                            usedCols++;

                                        }
                                    }
                                    // usedCols = colcount;
                                }




                                //DataTable excellDataFromSheet = sheet.CreateDataTable(new CreateDataTableOptions()
                                //   {
                                //       ColumnHeaders = true,
                                //       StartRow = 0,

                                //       NumberOfColumns = usedCols,
                                //       NumberOfRows = sheet.Rows.Count,
                                //       Resolution = ColumnTypeResolution.Auto,
                                //       ExtractDataOptions = ExtractDataOptions.StopAtFirstEmptyRow,
                                //   });

                                string[] exceldata = new string[usedCols];
                                //for (int i = 0; i < usedCols; i++)
                                //{
                                //    exceldata[i] = excellDataFromSheet.Columns[i].ToString();
                                //}

                                //excellDataFromSheet = null;


                                int k = -1;
                                for (int i = 0; i < colcount; i++)
                                {

                                    if (sheet.Rows[0].Cells[i].Value != null)
                                    {
                                        k++;
                                        exceldata[k] = sheet.Rows[0].Cells[i].Value.ToString();
                                        //P Axis 13.2 : issue 730

                                        //Bug,:::::::543
                                        //exceldata[k] = sheet.Rows[0].Cells[i].Value.ToString().Trim();
                                        //Axis 714
                                        //exceldata[k] = sheet.Rows[0].Cells[i].Value.ToString().Replace(".", "").Replace("#", "").Replace("-", "").Replace("@", "").Replace("$", "").Replace("*", "");
                                        //exceldata[k] = sheet.Rows[0].Cells[i].Value.ToString().Replace(".", "").Replace("-", "").Replace("@", "").Replace("$", "").Replace("*", "");
                                    }
                                    else
                                    {
                                        //int unicode = 65 + i;
                                        //char character = (char)unicode;
                                        //exceldata[i] = character.ToString();
                                    }

                                }
                                for (int i = 0; i < usedCols; i++)
                                {
                                    int no = 2;
                                    for (int j = i + 1; j < usedCols - i; j++)
                                    {
                                        if (exceldata[i] != null && exceldata[j] != null)
                                        {
                                            if (exceldata[i].ToString().Trim() == exceldata[j].ToString().Trim())
                                            {
                                                exceldata[j] = exceldata[j].Trim() + " (" + no + ")";
                                                no++;
                                            }
                                        }

                                    }

                                }
                                for (int i = 0; i < usedCols; i++)
                                {
                                    excelSheets.Add(exceldata[i].ToString().Trim());
                                }
                                //ef1.Save(this.FilePath);

                                // releaseObject(ef1.Worksheets);

                                return excelSheets;

                                // }
                                
                            }



                            // releaseObject(ef1);
                        }
                        #endregion


                        #region code of xlsx file load , replacement of above code ,bug no 399
                        uint _ExcelPID = 0;
                        try
                        {
                            CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                        }
                        catch { }
                        try
                        {
                            //kill that process
                            Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                            excel.Kill();
                            excel.WaitForExit();
                        }
                        catch { }

                        GC.Collect();
                        GC.WaitForPendingFinalizers();


                        // Open connection with the database.
                        if (oleDBConn.State.ToString() != "Open")
                        {
                            oleDBConn.Open();
                        }
                        string sheetnametest = this.CurrentSheetName;
                        if (sheetnametest.Contains("[") == false)
                        {
                            sheetnametest = "[" + sheetnametest + "$]";
                        }
                        else
                        {
                            sheetnametest += "$";
                        }


                        if (sheetnametest.Contains("$"))
                        {
                            sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        }
                        System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetnametest, null });
                        System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                        if (dt.Rows.Count == 0)
                        {
                            if (sheetnametest.Contains("[") == false)
                            {
                                sheetnametest = "[" + sheetnametest + "$]";
                            }
                            else
                            {
                                sheetnametest += "$";
                            }
                            System.Data.DataTable dtt = new System.Data.DataTable();
                            if (sheetnametest.Contains("$"))
                            {
                                sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                            }

                            OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetnametest), oleDBConn);
                            oleDBAdapter.Fill(dtt);
                            for (int i = 0; i < dtt.Columns.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME));
                            }

                        }
                        else
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME));
                            }
                        }
                        oleDBConn.Close();
                        #endregion
                        return columnNames;
                    }
                    catch (Exception ex)
                    {
                        uint _ExcelPID = 0;
                        try
                        {
                            CommonUtilities.GetInstance().WriteProcessIdToFile(_ExcelPID.ToString());
                        }
                        catch { }
                        try
                        {
                            //kill that process
                            Process excel = Process.GetProcessById(Convert.ToInt32(_ExcelPID));
                            excel.Kill();
                            excel.WaitForExit();
                        }
                        catch { }

                        GC.Collect();
                        GC.WaitForPendingFinalizers();


                        // Open connection with the database.
                        if (oleDBConn.State.ToString() != "Open")
                        {
                            oleDBConn.Open();
                        }
                        string sheetnametest = this.CurrentSheetName;
                        if (sheetnametest.Contains("[") == false)
                        {
                            sheetnametest = "[" + sheetnametest + "$]";
                        }
                        else
                        {
                            sheetnametest += "$";
                        }


                        if (sheetnametest.Contains("$"))
                        {
                            sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        }
                        System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetnametest, null });
                        System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                        if (dt.Rows.Count == 0)
                        {
                            if (sheetnametest.Contains("[") == false)
                            {
                                sheetnametest = "[" + sheetnametest + "$]";
                            }
                            else
                            {
                                sheetnametest += "$";
                            }
                            System.Data.DataTable dtt = new System.Data.DataTable();
                            if (sheetnametest.Contains("$"))
                            {
                                sheetnametest = sheetnametest.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                            }

                            OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetnametest), oleDBConn);
                            oleDBAdapter.Fill(dtt);
                            for (int i = 0; i < dtt.Columns.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME));
                            }

                        }
                        else
                        {
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME));
                            }
                        }
                        oleDBConn.Close();
                        return columnNames;
                    }

                    finally
                    {
                        // Clean up.

                        try
                        {
                            if (oleDBConn.State == ConnectionState.Open)
                            {
                                oleDBConn.Close();
                                oleDBConn.Dispose();
                            }
                        }
                        catch (Exception ex)
                        { }
                    }


                }
                else if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xml")
                {
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();

                    if (CommonUtilities.GetInstance().XmlCheckExportFlag.Equals(false))
                    {
                        #region Normal browse xml file file

                        List<string> list = new List<string>();
                        sheetName = System.IO.Path.GetFullPath(this.m_filePath);
                        if (File.Exists(sheetName))
                        {
                            XmlDocument xmlDoc = new XmlDocument();

                            try
                            {
                                xmlDoc.Load(sheetName);
                                XmlNode root = (XmlNode)xmlDoc.DocumentElement;

                                // Axis 10.0 chnages
                                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                {

                                    nodeList = root.ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes;

                                }
                                else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
                                {
                                    nodeList = root.ChildNodes;
                                }
                                //End Of Axis 10.0 Changes.

                                string xmlFileType = Mappings.GetInstance().XMLNodedata;
                                string nodeName = string.Empty;

                                foreach (XmlNode node in nodeList)
                                {
                                    xmllist.RemoveRange(0, xmllist.Count);
                                    list = getXmlElements(node);
                                    foreach (string xmlData in list)
                                    {
                                        columnNames.Add(xmlData);
                                    }
                                }

                            }
                            catch (XmlException)
                            {
                                MessageBox.Show("Error while loading the file.Please check the xml.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                        #endregion
                    }
                    else
                    {
                        #region Exported xml file

                        System.Xml.XmlDocument xdoc = new XmlDocument();

                        xdoc.Load(CommonUtilities.GetInstance().BrowseFileName);

                        XmlNode root = (XmlNode)xdoc.DocumentElement;

                        for (int i = 0; i < root.ChildNodes.Count; i++)
                        {
                            if (root.ChildNodes.Item(i).Name == "UserMappings")
                            {
                                XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes[0].ChildNodes;
                                foreach (XmlNode node in userMappingsXML)
                                {
                                    if (!string.IsNullOrEmpty(node.InnerText) && !node.Name.Equals("name") && !node.Name.Equals("MappingType") && !node.Name.Equals("ImportType"))
                                        columnNames.Add(node.InnerText);
                                }
                            }
                        }

                        #endregion
                    }
                    return columnNames;
                }
                else
                {
                    if (sheetName.Contains("$"))
                    {
                        sheetName = sheetName.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                    }
                    System.Data.DataTable dt = oleDBConn.GetOleDbSchemaTable(OleDbSchemaGuid.Columns, new object[] { null, null, sheetName, null });
                    System.Collections.ObjectModel.Collection<string> columnNames = new System.Collections.ObjectModel.Collection<string>();
                    if (dt.Rows.Count == 0)
                    {
                        System.Data.DataTable dtt = new System.Data.DataTable();
                        if (sheetName.Contains("$"))
                        {
                            sheetName = sheetName.Replace("[", string.Empty).Replace("]", string.Empty).Replace("$", string.Empty);
                        }
                        OleDbDataAdapter oleDBAdapter = new OleDbDataAdapter(string.Format(SELECTQUERY, sheetName), oleDBConn);
                        oleDBAdapter.Fill(dtt);
                        for (int i = 0; i < dtt.Columns.Count; i++)
                        {
                            columnNames.Add(this.m_HDR ? dtt.Columns[i].ColumnName : dtt.Columns[i].ColumnName.Replace("F", DEFUALTCOLNAME));
                        }

                    }
                    else
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            columnNames.Add(this.m_HDR ? dt.Rows[i][COLUMNNAME].ToString() : dt.Rows[i][COLUMNNAME].ToString().Replace("F", DEFUALTCOLNAME));
                        }
                    }

                    return columnNames;
                }
            }
            catch (OleDbException OleEx)
            {
                if (OleEx.Message.Contains(ERROR_STRING))
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER004");
                }
                else if (OleEx.ErrorCode == -2147467259)
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER003");

                }
                else
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER001");

                }

            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                if (Err != string.Empty)
                    throw new TIException("Zed Axis MSG037", this.m_filePath);
                else
                    throw new TIException("Zed Axis ER001");

            }
            finally
            {
                // Clean up.
                if (oleDBConn != null)
                // if (gembox != 1 && oleDBConn != null)
                {
                    oleDBConn.Close();
                    oleDBConn.Dispose();
                }
            }
        }
        /// <summary>
        /// creating xml file
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private List<string> getXmlElements(XmlNode node)
        {
            if (node.ChildNodes.Count > 0 && !node.FirstChild.Name.ToString().Equals("#text"))
            {

                string nodeName = ParentTag + node.Name;
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    if (node.ChildNodes[i].ChildNodes.Count > 0 && !node.ChildNodes[i].FirstChild.Name.ToString().Equals("#text"))
                    {
                        ParentTag = node.Name;
                        xmllist = getXmlElements(node.ChildNodes[i]);
                    }
                    else
                    {
                        ParentTag = string.Empty;
                        xmllist.Add(nodeName + node.ChildNodes[i].Name);
                    }
                }
            }
            else
                xmllist.Add(node.Name);
            return xmllist;
        }


        /// <summary>
        /// export datatable to csv file
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="outputPath"></param>
        //bug 487 Export to csv format
        public void ExportToCsvFile(DataTable dt, string outputPath)
        {
            StringBuilder sb = new StringBuilder();

            string[] columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName).
                                              ToArray();
            sb.AppendLine(string.Join(",", columnNames));

            foreach (DataRow row in dt.Rows)
            {
                string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                ToArray();
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText(outputPath, sb.ToString());
        }

       
        public void ExportDataToCsvFile(DataTable dt, string outputPath)
        {
            StringBuilder sb = new StringBuilder();

            string[] columnNames = dt.Columns.Cast<DataColumn>().
                                              Select(column => column.ColumnName).
                                              ToArray();
            for (int i = 0; i < columnNames.Length; i++)
            {
                if (columnNames[i].Contains(","))
                {
                    sb.Append(String.Format("\"{0}\",", columnNames[i]));
                }
                else
                {
                    sb.Append(String.Format("{0},", columnNames[i]));
                }
            }

            sb.Remove((sb.Length - 1), 1);

            foreach (DataRow row in dt.Rows)
            {
                string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                ToArray();
                sb.Append(Environment.NewLine);
                for (int i = 0; i < fields.Length; i++)
                {
                    if (fields[i].Contains(","))
                    {
                      
                        sb.Append(String.Format("\"{0}\",", fields[i]));
                    }
                    else
                    {
                        sb.Append(String.Format("{0},", fields[i]));
                    }
                }
               sb.Remove((sb.Length - 1), 1);
            }
            File.WriteAllText(outputPath, sb.ToString());
        }

        /// <summary>
        /// export dataset to excel
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="outputPath"></param>
        public void ExportToExcel(DataSet dataSet, string outputPath)
        {
            //#region for try
            try
            {
                //637
                SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");

                ExcelFile ef = new ExcelFile();

                string fileName = Path.GetFileNameWithoutExtension(outputPath);
                ExcelWorksheet ws = ef.Worksheets.Add(fileName);


                //bug 510
                // Copy each DataTable
                foreach (System.Data.DataTable dt in dataSet.Tables)
                {

                    //    // Insert the data from DataTable to the worksheet starting at cell "A1".
                    //    worksheet.InsertDataTable(dataTable,
                    //        new InsertDataTableOptions("A1") { ColumnHeaders = true ,StartColumn=0,StartRow=0});
                    //}
                    //ef.Save(outputPath);

                    // Copy the DataTable to an object array
                    object[,] rawData = new object[dt.Rows.Count + 2, dt.Columns.Count];

                    // Copy the column names to the first row of the object array
                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                    }

                    //GemBoxTry
                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                    }

                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {
                            rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                        }
                    }


                    for (int col = 0; col < dt.Columns.Count; col++)
                    {
                        for (int row = 0; row < dt.Rows.Count; row++)
                        {
                            ws.Cells[row, col].Value = rawData[row, col];
                        }
                    }
                    //ExcelCell cell

                    // Calculate the final column letter
                    string finalColLetter = string.Empty;
                    string colCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    int colCharsetLen = colCharset.Length;

                    if (dt.Columns.Count > colCharsetLen)
                    {
                        finalColLetter = colCharset.Substring(
                            (dt.Columns.Count - 1) / colCharsetLen - 1, 1);
                    }

                    finalColLetter += colCharset.Substring(
                            (dt.Columns.Count - 1) % colCharsetLen, 1);



                    if (dt.TableName == string.Empty)
                        dt.TableName = "Sheet1";
                    // excelSheet.Name = dt.TableName; 
                    ws.Name = dt.TableName;
                    this.CurrentSheetName = dt.TableName;
                    string excelRange = string.Format("A1:{0}{1}",
                    finalColLetter, dt.Rows.Count + 1);
                    // ws.InsertDataTable(dt);
                    ws.InsertDataTable(dt,
                    new InsertDataTableOptions()
                    {
                        ColumnHeaders = false,
                        StartRow = 1,
                        StartColumn = 0

                    });
                    ef.Save(outputPath);
                    releaseObject(ef.Worksheets);
                }
            }
            catch (Exception ex)
            {
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void ExportToMultipleSheetsExcel(DataSet dataSet, string outputPath)
        {
            try
            {
                SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");

                ExcelFile excelFileSheet = ExcelFile.Load(outputPath);
                int newSheetIndex = 0;
                int oldSheetIndex = excelFileSheet.Worksheets.Count - 1;
                // Copy each DataTable

                foreach (ExcelWorksheet sheet in excelFileSheet.Worksheets)
                {
                    if (sheet.Name == DataProcessingBlocks.ExcellUtility.GetInstance().CurrentSheetName)
                    {
                        newSheetIndex = sheet.Index;
                        excelFileSheet.Worksheets.Remove(sheet.Name);
                        ExcelWorksheet worksheet = excelFileSheet.Worksheets.Add(sheet.Name);

                        foreach (System.Data.DataTable dt in dataSet.Tables)
                        {
                            // Copy the DataTable to an object array
                            object[,] rawData = new object[dt.Rows.Count + 2, dt.Columns.Count];

                            // Copy the column names to the first row of the object array
                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                            }

                            //GemBoxTry
                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                            }

                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                for (int row = 0; row < dt.Rows.Count; row++)
                                {
                                    rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                                }
                            }

                            for (int col = 0; col < dt.Columns.Count; col++)
                            {
                                for (int row = 0; row < dt.Rows.Count; row++)
                                {
                                    worksheet.Cells[row, col].Value = rawData[row, col];
                                }
                            }

                            worksheet.InsertDataTable(dt,
                            new InsertDataTableOptions()
                            {
                                ColumnHeaders = false,
                                StartRow = 1,
                                StartColumn = 0

                            });
                            excelFileSheet.Save(outputPath);
                            releaseObject(excelFileSheet.Worksheets);
                        }
                        break;
                    }
                }
                excelFileSheet.Worksheets.Move(oldSheetIndex, newSheetIndex);
                excelFileSheet.Save(outputPath);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            catch (Exception ex)
            {
                DataProcessingBlocks.CommonUtilities.WriteErrorLog(ex.Message);
            }
        }

        /// <summary>
        /// export dataset to excel 2007
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="outputPath"></param>
        public void ExportToExcel2007(DataSet dataSet, string outputPath)
        {
            #region GemBox
            try
            {
                //637
                SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");

                ExcelFile ef = new ExcelFile();

                string fileName = Path.GetFileNameWithoutExtension(outputPath);
                ExcelWorksheet worksheet = ef.Worksheets.Add(fileName);


                // Copy each DataTable
                foreach (System.Data.DataTable dataTable in dataSet.Tables)
                {

                    // Insert the data from DataTable to the worksheet starting at cell "A1".
                    worksheet.InsertDataTable(dataTable,
                        new InsertDataTableOptions("A1") { ColumnHeaders = true });
                }
                ef.Save(outputPath);
                // // Copy the DataTable to an object array
                // object[,] rawData = new object[dt.Rows.Count + 2, dt.Columns.Count];
                // // rawData[0, dt.Columns.Count / 2] = reportTitle;

                // // Copy the column names to the first row of the object array
                // for (int col = 0; col < dt.Columns.Count; col++)
                // {
                //     rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                // }

                // // Copy the values to the object array
                // //for (int col = 0; col < dt.Columns.Count; col++)
                // //{
                // //    for (int row = 0; row < dt.Rows.Count; row++)
                // //    {
                // //        rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                // //    }
                // //}

                // //GemBoxTry
                // for (int col = 0; col < dt.Columns.Count; col++)
                // {
                //     rawData[0, col] = dt.Columns[col].ColumnName.Replace(".", string.Empty);
                // }

                // for (int col = 0; col < dt.Columns.Count; col++)
                // {
                //     for (int row = 0; row < dt.Rows.Count; row++)
                //     {
                //         rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                //     }
                // }


                // for (int col = 0; col < dt.Columns.Count; col++)
                // {
                //     for (int row = 0; row <= dt.Rows.Count; row++)
                //     {
                //         //rawData[row + 1, col] = dt.Rows[row].ItemArray[col];
                //         ws.Cells[row, col].Value = rawData[row, col];
                //     }
                // }

                // // Calculate the final column letter
                // string finalColLetter = string.Empty;
                // string colCharset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                // int colCharsetLen = colCharset.Length;

                // if (dt.Columns.Count > colCharsetLen)
                // {
                //     finalColLetter = colCharset.Substring(
                //         (dt.Columns.Count - 1) / colCharsetLen - 1, 1);
                // }

                // finalColLetter += colCharset.Substring(
                //         (dt.Columns.Count - 1) % colCharsetLen, 1);


                // if (dt.TableName == string.Empty)
                //     dt.TableName = "Sheet1";
                // // excelSheet.Name = dt.TableName; 
                // ws.Name = dt.TableName;
                // this.CurrentSheetName = dt.TableName;

                // //    excelSheet.Columns.NumberFormat = "@";        //For bug no.557
                // // Fast data export to Excel
                // string excelRange = string.Format("A1:{0}{1}",
                //finalColLetter, dt.Rows.Count + 1);

                // //  releaseObject(ef.Worksheets);
                // uint _ExcelPID = 0;
                // ef.Save(outputPath);
                // releaseObject(ef.Worksheets);
                //    }

            }
            catch (Exception ex)
            {
            }
            #endregion
        }

        //For excel interop objects to get released from memory
        private void releaseObject(object obj)
        {
            try
            {
                //System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                //obj = null;

                //bug 480
                if (Marshal.IsComObject(obj))
                {
                    Marshal.ReleaseComObject(obj);

                }
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        /// <summary>
        /// Axis 11.0 -131 Exports errors from import summary to an xls file
        /// </summary>
        public void exportErrors(DataSet dataSet, string outputPath, string matchColumn)
        {
            try
            {
                string errorColumnName = "Ref";
                if (matchColumn.Contains("Name") || matchColumn == "Code" || matchColumn == "Reason" || matchColumn == "Desc1" || matchColumn == "PriceAdjustmentName" || matchColumn == "PriceDiscountName" || matchColumn == "CreatedBy" || matchColumn == "FromStoreNumber")
                {
                    errorColumnName = "Name";
                }
                if (matchColumn.Contains("MergeTo") || matchColumn.Contains("BillingRateName"))
                {
                    errorColumnName = "Name";
                }

                //637
                SpreadsheetInfo.SetLicense("E43Z-FVCZ-CTOM-D1EU");

                ExcelFile ef = new ExcelFile();
                int row = 0, col = 0, index = 0;


                string fileName = Path.GetFileNameWithoutExtension(outputPath);
                ExcelWorksheet ws = ef.Worksheets.Add(fileName);

                ws.Name = "Import Errors";


                System.Data.DataTable importData = dataSet.Tables[0];
                System.Data.DataTable errorData = dataSet.Tables[1];
                index = 0;
                for (col = 0; col < errorData.Columns.Count - 1; col++)
                {
                    ws.Cells[row, col].Value = errorData.Columns[index++].ColumnName;
                }

                index = 0;
                for (int i = 0; i < importData.Columns.Count; i++)
                {
                    ws.Cells[row, col++].Value = importData.Columns[index++].ColumnName;
                }

                row = 1;
                string val1 = "";
                string val2 = "";
                foreach (DataRow import in importData.Rows)
                {
                    foreach (DataRow error in errorData.Rows)
                    {
                        //P Axis 13.1 : issue 666
                        if ((matchColumn.Contains("CreditMemoRefNumber")) || (matchColumn.Contains("EstimateRefNumber")) || (matchColumn.Contains("InventoryAdjustmentRefNumber")) ||
                            (matchColumn.Contains("StatementChargesRefNumber")) || (matchColumn.Contains("SalesRecieptRefNumber")) || (matchColumn.Contains("ReceivePaymentRefNumber")))
                        {
                            if (import["RefNumber"].ToString() != null)
                            {
                                val1 = import["RefNumber"].ToString();
                            }
                        }
                        else
                        {
                            val1 = import[matchColumn].ToString();
                        }
                         val2 = error[errorColumnName].ToString();
                        if (val1 == val2)
                        {
                            index = 0;
                            for (col = 0; col < errorData.Columns.Count - 1; col++)
                            {
                                ws.Cells[row, col].Value = error[index++].ToString();
                            }
                        }
                    }

                    index = 0;
                    for (int i = 0; i < importData.Columns.Count; i++)
                    {
                        ws.Cells[row, col++].Value = import[index++].ToString();
                    }
                    row++;
                }

                ef.Save(outputPath);
                releaseObject(ef.Worksheets);
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// format data using selected mapping by using file extesion.
        /// </summary>
        /// <param name="sheetName"></param>
        /// <returns></returns>
        public System.Data.DataTable GetDataForSelectedMapping(string sheetName)
        {
            //OleDbConnection oleDBConn = null;
            try
            {
                string newfilePath = string.Empty;
                this.m_CurrentSheetname = sheetName;
                // Connection String. Change the excel file to the file you will search.
                string connString = string.Format(connectionStringFormat, this.m_filePath, this.m_HDR ? "YES" : "NO");
                int checkExcel = 0;
                
                System.Data.DataTable TextDatafromSelectedMapping = new System.Data.DataTable();
                DataTable excellDataFromSheet = new DataTable();

                if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".txt" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".iif" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".csv" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm")
                {
                    if (System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsx" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xlsm" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".ods" || System.IO.Path.GetExtension(this.m_filePath).ToLower() == ".xls")
                    {
                        TextDatafromSelectedMapping = GetDataBySheetName(sheetName);
                    }
                    else
                    {
                        TextDatafromSelectedMapping = GetDataBySheetName(sheetName);
                    }

                    excellDataFromSheet = CommonUtilities.GetInstance().SelectedMapping.GetDataTableValue(TextDatafromSelectedMapping);
                    excellDataFromSheet = ApplyConcatFunctionFunction(TextDatafromSelectedMapping, excellDataFromSheet);

                    //Axis 641 sort rows with ref number to add same line rows in same bunch
                    if (excellDataFromSheet.Columns.Contains("RefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "RefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                   else if (excellDataFromSheet.Columns.Contains("InvoiceRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "InvoiceRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("CreditMemoRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "CreditMemoRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("EstimateRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "EstimateRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("SalesOrderRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "SalesOrderRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("SalesReceiptRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "SalesReceiptRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("PurchaseOrderRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "PurchaseOrderRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("PaymentRefNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "PaymentRefNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }
                    else if (excellDataFromSheet.Columns.Contains("DocNumber"))
                    {
                        excellDataFromSheet.DefaultView.Sort = "DocNumber";
                        excellDataFromSheet = excellDataFromSheet.DefaultView.ToTable();
                    }

                    //Axis 641 

                    return excellDataFromSheet;

                }
             //   this.DataNumber = 0;
                return excellDataFromSheet;
            }
            catch (OleDbException OleEx)
            {
                if (OleEx.Message.Contains(ERROR_STRING))
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);
                    throw new TIException("Zed Axis ER004");
                }
                else if (OleEx.ErrorCode == -2147467259)
                {
                    // CommonUtilities.WriteErrorLog(OleEx.Message);
                    // CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER003");
                }
                else
                {
                    //CommonUtilities.WriteErrorLog(OleEx.Message);
                    //CommonUtilities.WriteErrorLog(OleEx.StackTrace);

                    throw new TIException("Zed Axis ER001");
                }

            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);

                throw new TIException("Zed Axis ER001");
            }
            finally
            {
                // Clean up.
                //if (oleDBConn != null)
                //{
                 //   oleDBConn.Close();
                 //   oleDBConn.Dispose();
                }
            }


        
        private DataTable ApplyConcatFunctionFunction(DataTable RowDataTable, DataTable MappedDataTable)
        {
            try
            {
                string m_settingsPath = CommonUtilities.GetInstance().getSettingFilePath();
                XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);
                XmlNode root = (XmlNode)xdoc.DocumentElement;
                string constantColumnList = string.Empty;
                for (int count = 0; count < root.ChildNodes.Count; count++)
                {
                    if (root.ChildNodes.Item(count).Name == "FunctionMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(count).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.SelectSingleNode("./MappingName").InnerText.Equals(DataProcessingBlocks.CommonUtilities.GetInstance().SelectedMapping.Name))
                            {
                                for (int childcount = 0; childcount < node.ChildNodes.Count; childcount++)
                                {
                                    if (!node.ChildNodes[childcount].Name.Equals("MappingName"))
                                    {
                                        if (!node.ChildNodes[childcount].Name.Equals("MappingType"))
                                        {
                                            if (!node.ChildNodes[childcount].Name.Equals("ImportType"))
                                            {
                                                string appliedOnColumn = node.ChildNodes[childcount].Name;
                                                string functionName = node.ChildNodes[childcount].InnerText;
                                                if (!string.IsNullOrEmpty(functionName))
                                                {
                                                    List<string> returnConcatFunctionDetails = CommonUtilities.GetInstance().FetchConcatFunctionDetails(functionName);
                                                    if (returnConcatFunctionDetails.Count > 0)
                                                    {
                                                        for (int i = 0; i < returnConcatFunctionDetails.Count; i++)
                                                        {
                                                            if (returnConcatFunctionDetails[i] != null && returnConcatFunctionDetails[i] != "")
                                                            {
                                                                // Syntax of Expression {Col1}&"delimeterChar"&{Col2}  
                                                                string[] SplitList = returnConcatFunctionDetails[i].Split(new string[] { "&" }, StringSplitOptions.None);
                                                                List<string> ColList = new List<string>();
                                                                List<string> DeliminatorList = new List<string>();
                                                                for (int colIndex = 0; colIndex < SplitList.Length; colIndex++)
                                                                {
                                                                    if (SplitList[colIndex].StartsWith("\"") && SplitList[colIndex].EndsWith("\""))
                                                                    {
                                                                        string Deliminator = SplitList[colIndex].Replace("\"", string.Empty);
                                                                        DeliminatorList.Add(Deliminator);
                                                                    }
                                                                    else if (SplitList[colIndex].StartsWith("{") && SplitList[colIndex].EndsWith("}"))
                                                                    {
                                                                        string ColumnName = SplitList[colIndex].Replace("{", string.Empty).Replace("}", string.Empty).Trim();
                                                                        ColList.Add(ColumnName);
                                                                    }
                                                                }

                                                                bool columnExistsInMapped = false;
                                                                int columnIndex = 0;

                                                                foreach (DataColumn column in MappedDataTable.Columns)
                                                                {
                                                                    if (column.ColumnName == appliedOnColumn)
                                                                    { columnExistsInMapped = true; columnIndex = MappedDataTable.Columns.IndexOf(column); break; }
                                                                }


                                                                if (!columnExistsInMapped)
                                                                {
                                                                    MappedDataTable.Columns.Add(appliedOnColumn);
                                                                    columnIndex = MappedDataTable.Columns.IndexOf(appliedOnColumn);
                                                                    //foreach (DataColumn column in RowDataTable.Columns)
                                                                    //{
                                                                    //    if (column.ColumnName == appliedOnColumn)
                                                                    //    { columnExistsInRow = true; columnIndex = RowDataTable.Columns.IndexOf(column); break; }
                                                                    //}
                                                                }
                                                                for (int rows = 0; rows < RowDataTable.Rows.Count; rows++)
                                                                {
                                                                    string ConcatString = "";
                                                                    int delimiterListIndex = 0; int columnListIndex = 0;
                                                                    for (int index = 0; index < SplitList.Length; index++)
                                                                    {
                                                                        if (SplitList[index].StartsWith("{") && SplitList[index].EndsWith("}"))
                                                                        {
                                                                            if (RowDataTable.Columns.Contains(ColList[columnListIndex]))
                                                                            {
                                                                                if (RowDataTable.Rows[rows][ColList[columnListIndex]].ToString() != null && RowDataTable.Rows[rows][ColList[columnListIndex]].ToString() != "")
                                                                                {
                                                                                    ConcatString += RowDataTable.Rows[rows][ColList[columnListIndex]].ToString();
                                                                                }
                                                                                columnListIndex++;
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            if (SplitList[index].StartsWith("\"") && SplitList[index].EndsWith("\""))
                                                                            {
                                                                                ConcatString += DeliminatorList[delimiterListIndex].ToString();
                                                                                delimiterListIndex++;
                                                                            }
                                                                        }
                                                                    }
                                                                    MappedDataTable.Rows[rows][columnIndex] = ConcatString;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }//end of for loop
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // MessageBox.Show(searchReplaceException.Message.ToString());
            }
            return MappedDataTable;
        }

        #endregion
    }
}












