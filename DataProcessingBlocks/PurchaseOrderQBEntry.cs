﻿// ==============================================================================================
// 
// PurchaseOrderQBEntry.cs
//
// This file contains the implementations of the Purchase Order Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Purchase Order Entry Imports.
//         Purchase Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping fields (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("PurchaseOrderQBEntry", Namespace = "", IsNullable = false)]
    public class PurchaseOrderQBEntry
    {
        #region Private Member Variable

        private VendorRef m_VendorRef;
        //Axis 617 
        private CurrencyRef m_CurrencyRef;
        //Axis 617 ends
        private ClassRef m_ClassRef;
        private InventorySiteRef m_InventorySiteRef;
        private ShipToEntityRef m_ShipToEntityRef;
        private TemplateRef m_TemplateRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private Collection<VendorAddress> m_VendorAddress = new Collection<VendorAddress>();
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        private TermsRef m_TermsRef;
        private string m_DueDate;
        private string m_ExpectedDate;
        private ShipMethodRef m_ShipMethodRef;
        private string m_FOB;
        private string m_Memo;
        private string m_VendorMsg;
        private string m_IsToBePrinted;
        private string m_IsToBeEmailed;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_Other1;
        private string m_Other2;
        private string m_ExchangeRate;
        private Collection<PurchaseOrderLineAdd> m_PurchaseOrderLineAdd = new Collection<PurchaseOrderLineAdd>();
        private DateTime m_PurchaseOrderDate;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;
       
        #endregion

        #region Construtor

        public PurchaseOrderQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public VendorRef VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        //Axis 617 
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        //Axis 617 ends
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
        public InventorySiteRef InventorySiteRef
        {
            get { return m_InventorySiteRef; }
            set { m_InventorySiteRef = value; }
        }
      
        public ShipToEntityRef ShipToEntityRef
        {
            get { return m_ShipToEntityRef; }
            set { m_ShipToEntityRef = value; }
        }

        public TemplateRef TemplateRef
        {
            get { return m_TemplateRef; }
            set { m_TemplateRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {

            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        [XmlArray("VendorAddressREM")]
        public Collection<VendorAddress> VendorAddress
        {
            get { return m_VendorAddress; }
            set { m_VendorAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }


        public TermsRef TermsRef
        {
            get { return m_TermsRef; }
            set { m_TermsRef = value; }
        }
        
        [XmlElement(DataType = "string")]
        public String DueDate
        {
             get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }

            
        }

        [XmlElement(DataType = "string")]
        public String ExpectedDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_ExpectedDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_ExpectedDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_ExpectedDate = value;
            }


        }

        public ShipMethodRef ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string VendorMsg
        {
            get { return m_VendorMsg; }
            set { m_VendorMsg = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }
         
        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }
        
        public string Other1
        {
            get { return m_Other1; }
            set { m_Other1 = value; }
        }

        public string Other2
        {
            get { return m_Other2; }
            set { m_Other2 = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        [XmlArray("PurchaseOrderLineAddREM")]
        public Collection<PurchaseOrderLineAdd> PurchaseOrderLineAdd
        {
            get { return m_PurchaseOrderLineAdd; }
            set { m_PurchaseOrderLineAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime PurchaseOrderDate
        {
            get { return m_PurchaseOrderDate; }
            set { m_PurchaseOrderDate = value; }
        }


        #endregion

        #region Public Methods
        /// <summary>
       /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.PurchaseOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create PurchaseOrderEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderAddRq = requestXmlDoc.CreateElement("PurchaseOrderAddRq");
            inner.AppendChild(PurchaseOrderAddRq);

            //Create PurchaseOrderAdd aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderAdd = requestXmlDoc.CreateElement("PurchaseOrderAdd");

            PurchaseOrderAddRq.AppendChild(PurchaseOrderAdd);

            requestXML = requestXML.Replace("<PurchaseOrderLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<VendorAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            PurchaseOrderAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd/Phone");
                    PurchaseOrderAdd.RemoveChild(childNode);
                }
            }

            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd/CurrencyRef");
                    PurchaseOrderAdd.RemoveChild(childNode);
                }
            }
            //Axis 617 ends

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd/Fax");
                    PurchaseOrderAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd/Email");
                    PurchaseOrderAdd.RemoveChild(childNode);
                }
            }
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderLineAdd"))
            //{
            //    if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
            //    {
            //        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderLineAdd/InventorySiteLocationRef");
            //        if (node != null)
            //        {
            //            PurchaseOrderAdd.RemoveChild(node);
            //        }

            //        // requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
            //        PurchaseOrderAdd.RemoveAll();


            //    }
            //}


            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                PurchaseOrderAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                PurchaseOrderAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());
            


            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }


            if (resp != string.Empty)
            {
                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                outputXMLDoc.LoadXml(resp);
                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PurchaseOrderAddRs"))
                {
                    string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                    if (statusSeverity == "Error")
                    {
                        string requesterror = string.Empty;
                        try
                        {
                            requesterror = oNode.Attributes["requestID"].Value.ToString();
                        }
                        catch
                        { }
                                                                      
                        string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                        foreach (string s in array_msg)
                        {
                            if (s != "")
                            {  statusMessage += s + ".\n";  }
                            if (s == "")
                            {  statusMessage += s;  } 
                        }
                        statusMessage += "The Error location is " + requesterror;

                    }
                }
                foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PurchaseOrderAddRs/PurchaseOrderRet"))
                {
                    CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                }
                CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
            }
                        
            if (resp == string.Empty)
            {
                statusMessage += "\n";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofPurchaseOrder(this);
                statusMessage += "\n";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing PurchaseOrder ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="PurchaseOrder Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create PurchaseOrderQueryRq aggregate and fill in field values for it
            XmlElement PurchaseOrderQueryRq = requestXmlDoc.CreateElement("PurchaseOrderQueryRq");
            inner.AppendChild(PurchaseOrderQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            PurchaseOrderQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            PurchaseOrderQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }


        /// <summary>
        /// This method is used for updating PurchaseOrder information
        /// of existing PurchaseOrder with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdatePurchaseOrderInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.PurchaseOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderQBEntryModRq = requestXmlDoc.CreateElement("PurchaseOrderModRq");
            inner.AppendChild(PurchaseOrderQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderMod = requestXmlDoc.CreateElement("PurchaseOrderMod");
            PurchaseOrderQBEntryModRq.AppendChild(PurchaseOrderMod);

            requestXML = requestXML.Replace("<PurchaseOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<PurchaseOrderLineAdd>", "<PurchaseOrderLineMod>");
            requestXML = requestXML.Replace("</PurchaseOrderLineAdd>", "</PurchaseOrderLineMod>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
            requestXML = requestXML.Replace("<PurchaseOrderLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<PurchaseOrderLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<VendorAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            PurchaseOrderMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/Phone");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd/CurrencyRef");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            //Axis 617 ends
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/Fax");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/Email");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/InventorySiteLocationRef");
                    PurchaseOrderMod.RemoveChild(node);
                      requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    PurchaseOrderMod.RemoveAll();


                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }
            }
            if (requeststring != string.Empty)
                PurchaseOrderQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By PurchaseOrderRefNumber) : " + rowcount.ToString());
            else
                PurchaseOrderQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PurchaseOrderModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PurchaseOrderModRs/PurchaseOrderRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofPurchaseOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
//Bug No.412
        /// <summary>
        /// This method is used appending data to corresponding listid
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendPurchaseOrderInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence,List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.PurchaseOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderQBEntryModRq = requestXmlDoc.CreateElement("PurchaseOrderModRq");
            inner.AppendChild(PurchaseOrderQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement PurchaseOrderMod = requestXmlDoc.CreateElement("PurchaseOrderMod");
            PurchaseOrderQBEntryModRq.AppendChild(PurchaseOrderMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.

            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></PurchaseOrderLineMod><PurchaseOrderLineMod>";
            }
            for (int i = 0; i < request.Length; i++)
            {
                if (Listcnt != 0)
                {
                    if (subStringCnt == 1)
                    {
                        //subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></PurchaseOrderLineMod><PurchaseOrderLineMod><TxnLineID>-1</TxnLineID><ItemRef>");
                        subResultString = request[i].Replace("<ItemRef>", addString + "<TxnLineID>-1</TxnLineID><ItemRef>");
                        subStringCnt++;
                    }
                    else
                    {
                        subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                    }
                }
                else
                {
                    subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                }

                if (stringCnt == request.Length)
                {
                    resultString += subResultString;
                }
                else
                {
                    resultString += subResultString + "</ItemRef>";
                }
                stringCnt++;
                Listcnt--;
            }
            requestXML = resultString;


            requestXML = requestXML.Replace("<PurchaseOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<PurchaseOrderLineAdd>", "<PurchaseOrderLineMod>");
            requestXML = requestXML.Replace("</PurchaseOrderLineAdd>", "</PurchaseOrderLineMod>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
            requestXML = requestXML.Replace("<PurchaseOrderLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<PurchaseOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</PurchaseOrderLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<PurchaseOrderLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<VendorAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            //requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            PurchaseOrderMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/Phone");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderAddRq/PurchaseOrderAdd/CurrencyRef");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            //Axis 617 ends
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/Fax");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/Email");
                    PurchaseOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod/InventorySiteLocationRef");
                    PurchaseOrderMod.RemoveChild(node);
                    requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    PurchaseOrderMod.RemoveAll();


                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }
            }
            if (requeststring != string.Empty)
                PurchaseOrderQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By PurchaseOrderRefNumber) : " + rowcount.ToString());
            else
                PurchaseOrderQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PurchaseOrderModRq/PurchaseOrderMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PurchaseOrderModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PurchaseOrderModRs/PurchaseOrderRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofPurchaseOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }            
        #endregion
    }

    public class PurchaseOrderQBEntryCollection : Collection<PurchaseOrderQBEntry>
    {
        /// <summary>
        ///  This method is used for getting existing PO entry date, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public PurchaseOrderQBEntry FindPurchaseOrderEntry(DateTime date)
        {
            foreach (PurchaseOrderQBEntry item in this)
            {
                if (item.PurchaseOrderDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        /// <summary>
        ///  This method is used for getting existing PO entry ref no, If not exists then it return null.
        /// </summary>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public PurchaseOrderQBEntry FindPurchaseOrderEntry(string refNumber)
        {
            foreach (PurchaseOrderQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
        /// <summary>
        ///  This method is used for getting existing PO entry date and ref no, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public PurchaseOrderQBEntry FindPurchaseOrderEntry(DateTime date, string refNumber)
        {
            foreach (PurchaseOrderQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.PurchaseOrderDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }
    /// <summary>
    /// In this class declared variables and properties 
    /// </summary>
    [XmlRootAttribute("PurchaseOrderLineAdd", Namespace = "", IsNullable = false)]
    public class PurchaseOrderLineAdd
    {
        #region Private Member Variables
        
        private ItemRef m_ItemRef;
        private string m_ManufacturerPartNumber;
        private string m_Desc;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Rate;
        private ClassRef m_ClassRef;
        private string m_Amount;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        private CustomerRef m_CustomerRef;
        private string m_ServiceDate;     
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private OverrideItemAccountRef m_OverrideItemAccountRef;
        private string m_Other1;
        private string m_Other2;
        private DataExt m_DataExt;
        

        #endregion

        #region  Constructors

        public PurchaseOrderLineAdd()
        {

        }

        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string ManufacturerPartNumber
        {
            get { return this.m_ManufacturerPartNumber; }
            set { this.m_ManufacturerPartNumber = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
            }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set
            {
                this.m_InventorySiteLocationRef = value;
            }
        }
        public CustomerRef CustomerRef
        {
            get { return this.m_CustomerRef; }
            set { this.m_CustomerRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string ServiceDate
        {

            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_ServiceDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_ServiceDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_ServiceDate = value;
            }
        }
        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideItemAccountRef; }
            set { this.m_OverrideItemAccountRef = value; }
        }

        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        public string Other2
        {
            get { return this.m_Other2; }
            set { this.m_Other2 = value; }
        }


        public DataExt DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }

        #endregion
    }
}
