using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    class ImportStatementChargeClass
    {
   
        private static ImportStatementChargeClass m_ImportStatementChargeClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportStatementChargeClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import Statement Charge class
        /// </summary>
        /// <returns></returns>
        public static ImportStatementChargeClass GetInstance()
        {
            if (m_ImportStatementChargeClass == null)
                m_ImportStatementChargeClass = new ImportStatementChargeClass();
            return m_ImportStatementChargeClass;
        }


        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>StatementCharge QuickBooks collection </returns>
        public DataProcessingBlocks.ChargeQBEntryCollection ImportStatementChargeData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of Charge Entry collections.
            DataProcessingBlocks.ChargeQBEntryCollection coll= new ChargeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;


            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Statement Charge Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime StatementChargeDt = new DateTime();
                    string datevalue = string.Empty;
                    string datevalue1 = string.Empty;
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Adding ref number

                        DataProcessingBlocks.StatementChargesQBEntry StatementCharge = new StatementChargesQBEntry();
                        StatementCharge =  coll.FindChargeEntry(dr["RefNumber"].ToString());
                        if (StatementCharge == null)
                        {
                            StatementCharge = new StatementChargesQBEntry();
                            if (dt.Columns.Contains("CustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                                if (StatementCharge.CustomerRef.FullName == null)
                                                {
                                                    StatementCharge.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                                if (StatementCharge.CustomerRef.FullName == null)
                                                {
                                                    StatementCharge.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                            if (StatementCharge.CustomerRef.FullName == null)
                                            {
                                                StatementCharge.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                        if (StatementCharge.CustomerRef.FullName == null)
                                        {
                                            StatementCharge.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out StatementChargeDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    StatementCharge.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    StatementCharge.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                StatementCharge.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            StatementChargeDt = dttest;
                                            StatementCharge.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        StatementChargeDt = Convert.ToDateTime(datevalue);
                                        StatementCharge.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number

                                if (datevalue != string.Empty)
                                    StatementCharge.EstimateDate = StatementChargeDt;

                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.RefNumber = dr["RefNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.RefNumber = dr["RefNumber"].ToString();
                                        }

                                    }
                                    else
                                        StatementCharge.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemFullName"))
                            {
                                #region Validations of item Full name

                                if (dr["ItemFullName"].ToString() != string.Empty)
                                {
                                    StatementCharge.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                    if (StatementCharge.ItemRef.FullName == null)
                                        StatementCharge.ItemRef.FullName = null;

                                }
                                #endregion

                            }
                            //validation for InventorySiteRefFullName
                            if (dt.Columns.Contains("InventorySiteFullName"))
                            {
                                #region Validations of Inventory Site Ref Full name
                                if (dr["InventorySiteFullName"].ToString() != string.Empty)
                                {

                                    if (dr["InventorySiteFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref name (" + dr["InventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (StatementCharge.InventorySiteRef.FullName == null)
                                                    StatementCharge.InventorySiteRef.FullName = null;
                                                else
                                                    StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString().Substring(0, 31));

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (StatementCharge.InventorySiteRef.FullName == null)
                                                    StatementCharge.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                            if (StatementCharge.InventorySiteRef.FullName == null)
                                                StatementCharge.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                        if (StatementCharge.InventorySiteRef.FullName == null)
                                            StatementCharge.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion


                            }

                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    StatementCharge.Quantity = strQuantity;
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                                StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion
                            {

                            }

                            if (dt.Columns.Contains("Rate"))
                            {
                                #region Validations for Rate
                                if (dr["Rate"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;                                
                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                StatementCharge.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                StatementCharge.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                            StatementCharge.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {


                                        if (StatementCharge.Rate == null)
                                        {
                                            StatementCharge.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }

                                        else
                                        {
                                            StatementCharge.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                StatementCharge.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["Amount"].ToString();
                                                StatementCharge.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            StatementCharge.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {


                                            if (StatementCharge.Amount == null)
                                            {
                                                StatementCharge.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Desc"))
                            {
                                #region Validations for Description
                                if (dr["Desc"].ToString() != string.Empty)
                                {
                                    if (dr["Desc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Desc"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Desc"].ToString().Substring(0, 4095);
                                                StatementCharge.Desc= strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Desc"].ToString();
                                                StatementCharge.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.Desc = dr["Desc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Desc"].ToString();
                                        StatementCharge.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("ARAccountFullName"))
                            {
                                #region Validations of ARAccount Full Name
                                if (dr["ARAccountFullName"].ToString() != string.Empty)
                                {

                                    if (dr["ARAccountFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ARAccount  name (" + dr["ARAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString());
                                                if (StatementCharge.ARAccountRef.FullName == null)
                                                    StatementCharge.ARAccountRef.FullName = null;
                                                else
                                                    StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString().Substring(0, 31));

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString());
                                                if (StatementCharge.ARAccountRef.FullName == null)
                                                    StatementCharge.ARAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString());
                                            if (StatementCharge.ARAccountRef.FullName == null)
                                                StatementCharge.ARAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.ARAccountRef = new QuickBookEntities.ARAccountRef(dr["ARAccountFullName"].ToString());
                                        if (StatementCharge.ARAccountRef.FullName == null)
                                            StatementCharge.ARAccountRef.FullName = null;
                                    }
                                }
                                #endregion


                            }
                            if (dt.Columns.Contains("ClassFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                                if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                                {
                                                    StatementCharge.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                                if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                                {
                                                    StatementCharge.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                            if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                            {
                                                StatementCharge.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                        if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                        {
                                            StatementCharge.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            DateTime NewBilledDt = new DateTime();

                            if (dt.Columns.Contains("BilledDate"))
                            {
                                #region validations of BilledDate
                                if (dr["BilledDate"].ToString() != string.Empty)
                                {
                                    string billedDateValue = dr["BilledDate"].ToString();
                                    if (!DateTime.TryParse(billedDateValue, out NewBilledDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(billedDateValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This BilledDate (" + billedDateValue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    StatementCharge.BilledDate = billedDateValue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    StatementCharge.BilledDate = billedDateValue;
                                                }
                                            }
                                            else
                                            {
                                                StatementCharge.BilledDate = billedDateValue;
                                            }
                                        }
                                        else
                                        {
                                            NewBilledDt = dttest;
                                            StatementCharge.BilledDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        StatementChargeDt = Convert.ToDateTime(billedDateValue);
                                        StatementCharge.BilledDate = DateTime.Parse(billedDateValue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                           
                            DateTime NewDueDt = new DateTime();
                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region validations of DueDate
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    string duevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(duevalue, out NewDueDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            //DialogResult dgv = MessageBox.Show("TxnDate is not valid for this mapping.","Warning",MessageBoxButtons.);
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    StatementCharge.DueDate = duevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    StatementCharge.DueDate = duevalue;
                                                }
                                            }
                                            else
                                                StatementCharge.DueDate = duevalue;
                                        }
                                        else
                                        {
                                            StatementCharge.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        StatementCharge.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of OverrideItemAccount FullName
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["OverrideItemAccountFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This OverrideItemAccount name (" + dr["OverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString());
                                                if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                                {
                                                    StatementCharge.OverrideItemAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                                {
                                                    StatementCharge.OverrideItemAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString());
                                            if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                            {
                                                StatementCharge.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                        {
                                            StatementCharge.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }


                            


                            coll.Add(StatementCharge);
                        }

                        #endregion
                        }
                        

                        
                        else
                        {
                            StatementChargesQBEntry StatementCharge = new DataProcessingBlocks.StatementChargesQBEntry();

                            #region Without Adding ref number



                            if (dt.Columns.Contains("CustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                                if (StatementCharge.CustomerRef.FullName == null)
                                                {
                                                    StatementCharge.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                                if (StatementCharge.CustomerRef.FullName == null)
                                                {
                                                    StatementCharge.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                            if (StatementCharge.CustomerRef.FullName == null)
                                            {
                                                StatementCharge.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.CustomerRef = new CustomerRef(dr["CustomerFullName"].ToString());
                                        if (StatementCharge.CustomerRef.FullName == null)
                                        {
                                            StatementCharge.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out StatementChargeDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    StatementCharge.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    StatementCharge.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                StatementCharge.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            StatementChargeDt = dttest;
                                            StatementCharge.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        StatementChargeDt = Convert.ToDateTime(datevalue);
                                        StatementCharge.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    StatementCharge.EstimateDate = StatementChargeDt;


                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.RefNumber = dr["RefNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.RefNumber = dr["RefNumber"].ToString();
                                        }

                                    }
                                    else
                                        StatementCharge.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemFullName"))
                            {
                                #region Validations of item Full name

                                if (dr["ItemFullName"].ToString() != string.Empty)
                                {
                                    StatementCharge.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                    if (StatementCharge.ItemRef.FullName == null)
                                        StatementCharge.ItemRef.FullName = null;

                                }
                                #endregion

                            }
                            //validation for InventorySiteRefFullName
                            if (dt.Columns.Contains("InventorySiteFullName"))
                            {
                                #region Validations of Inventory Site Ref Full name
                                if (dr["InventorySiteFullName"].ToString() != string.Empty)
                                {

                                    if (dr["InventorySiteFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref name (" + dr["InventorySiteFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (StatementCharge.InventorySiteRef.FullName == null)
                                                    StatementCharge.InventorySiteRef.FullName = null;
                                                else
                                                    StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString().Substring(0, 31));

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (StatementCharge.InventorySiteRef.FullName == null)
                                                    StatementCharge.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                            if (StatementCharge.InventorySiteRef.FullName == null)
                                                StatementCharge.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                        if (StatementCharge.InventorySiteRef.FullName == null)
                                            StatementCharge.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion


                            }

                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    StatementCharge.Quantity = strQuantity;
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                                StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        StatementCharge.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("Rate"))
                            {
                                #region Validations for Rate
                                if (dr["Rate"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                StatementCharge.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                StatementCharge.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                            StatementCharge.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {


                                        if (StatementCharge.Rate == null)
                                        {
                                            StatementCharge.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }

                                        else
                                        {
                                            StatementCharge.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                StatementCharge.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["Amount"].ToString();
                                                StatementCharge.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            StatementCharge.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {


                                            if (StatementCharge.Amount == null)
                                            {
                                                StatementCharge.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("Desc"))
                            {
                                #region Validations for Description
                                if (dr["Desc"].ToString() != string.Empty)
                                {
                                    if (dr["Desc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Desc"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Desc"].ToString().Substring(0, 4095);
                                                StatementCharge.Desc= strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Desc"].ToString();
                                                StatementCharge.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.Desc = dr["Desc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Desc"].ToString();
                                        StatementCharge.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("ARAccountFullName"))
                            {
                                #region Validations of ARAccount Full Name
                                if (dr["ARAccountFullName"].ToString() != string.Empty)
                                {

                                    if (dr["ARAccountFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ARAccount  name (" + dr["ARAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString());
                                                if (StatementCharge.ARAccountRef.FullName == null)
                                                    StatementCharge.ARAccountRef.FullName = null;
                                                else
                                                    StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString().Substring(0, 31));

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString());
                                                if (StatementCharge.ARAccountRef.FullName == null)
                                                    StatementCharge.ARAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.ARAccountRef = new ARAccountRef(dr["ARAccountFullName"].ToString());
                                            if (StatementCharge.ARAccountRef.FullName == null)
                                                StatementCharge.ARAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.ARAccountRef = new QuickBookEntities.ARAccountRef(dr["ARAccountFullName"].ToString());
                                        if (StatementCharge.ARAccountRef.FullName == null)
                                            StatementCharge.ARAccountRef.FullName = null;
                                    }
                                }
                                #endregion


                            }
                            if (dt.Columns.Contains("ClassFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                                if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                                {
                                                    StatementCharge.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                                if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                                {
                                                    StatementCharge.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                            if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                            {
                                                StatementCharge.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.ClassRef = new ClassRef(string.Empty, dr["ClassFullName"].ToString());
                                        if (StatementCharge.ClassRef.FullName == null && StatementCharge.ClassRef.ListID == null)
                                        {
                                            StatementCharge.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            DateTime NewBilledDt = new DateTime();

                            if (dt.Columns.Contains("BilledDate"))
                            {
                                #region validations of BilledDate
                                if (dr["BilledDate"].ToString() != string.Empty)
                                {
                                     string billedDateValue = dr["BilledDate"].ToString();
                                     if (!DateTime.TryParse(billedDateValue, out NewBilledDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(billedDateValue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This BilledDate (" + billedDateValue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    StatementCharge.BilledDate = billedDateValue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    StatementCharge.BilledDate = billedDateValue;
                                                }
                                            }
                                            else
                                            {
                                                StatementCharge.BilledDate = billedDateValue;
                                            }
                                        }
                                        else
                                        {
                                            NewBilledDt = dttest;
                                            StatementCharge.BilledDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        StatementChargeDt = Convert.ToDateTime(billedDateValue);
                                        StatementCharge.BilledDate = DateTime.Parse(billedDateValue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }                           

                            DateTime NewDueDt = new DateTime();
                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region validations of DueDate
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    string duevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(duevalue, out NewDueDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            //DialogResult dgv = MessageBox.Show("TxnDate is not valid for this mapping.","Warning",MessageBoxButtons.);
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    StatementCharge.DueDate = duevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    StatementCharge.DueDate = duevalue;
                                                }
                                            }
                                            else
                                                StatementCharge.DueDate = duevalue;
                                        }
                                        else
                                        {
                                            StatementCharge.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        StatementCharge.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of OverrideItemAccount FullName
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["OverrideItemAccountFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This OverrideItemAccount name (" + dr["OverrideItemAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                                {
                                                    StatementCharge.OverrideItemAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                                {
                                                    StatementCharge.OverrideItemAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                            {
                                                StatementCharge.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        StatementCharge.OverrideItemAccountRef = new OverrideItemAccountRef( dr["OverrideItemAccountFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (StatementCharge.OverrideItemAccountRef.FullName == null)
                                        {
                                            StatementCharge.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }


                           


                            coll.Add(StatementCharge);

                            #endregion
                        }
                    }


                    else
                    {
                        return null;
                    }
                 #endregion
            }



            #region Customer,Item and Account Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                    if (dt.Columns.Contains("CustomerFullName"))
                    {

                        if (dr["CustomerFullName"].ToString() != string.Empty)
                        {
                            string customerName = dr["CustomerFullName"].ToString();
                            string[] arr = new string[15];
                            if (customerName.Contains(":"))
                            {
                                arr = customerName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["CustomerFullName"].ToString();
                            }
                            #region Set Customer Query
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                    qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                    CustomerQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                CustomerQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        CustomerQueryRq.AppendChild(FullName);
                                    }
                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {

                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {
                                                #region Customer Add Query

                                                XmlDocument xmldocadd = new XmlDocument();
                                                xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                xmldocadd.AppendChild(qbXMLcust);
                                                XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                CustomerAddRq.SetAttribute("requestID", "1");
                                                XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                CustomerAddRq.AppendChild(CustomerAdd);

                                                XmlElement Name = xmldocadd.CreateElement("Name");
                                                Name.InnerText = arr[i];
                                                CustomerAdd.AppendChild(Name);

                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                            CustomerAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }
                                                #region Adding Bill Address of Customer.

                                                if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                    (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                {
                                                    XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                    CustomerAdd.AppendChild(BillAddress);
                                                    if (dt.Columns.Contains("BillAddr1"))
                                                    {

                                                        if (dr["BillAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                            BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                            BillAddress.AppendChild(BillAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr2"))
                                                    {
                                                        if (dr["BillAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                            BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                            BillAddress.AppendChild(BillAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr3"))
                                                    {
                                                        if (dr["BillAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                            BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                            BillAddress.AppendChild(BillAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr4"))
                                                    {
                                                        if (dr["BillAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                            BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                            BillAddress.AppendChild(BillAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr5"))
                                                    {
                                                        if (dr["BillAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                            BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                            BillAddress.AppendChild(BillAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCity"))
                                                    {
                                                        if (dr["BillCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCity = xmldocadd.CreateElement("City");
                                                            BillCity.InnerText = dr["BillCity"].ToString();
                                                            BillAddress.AppendChild(BillCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillState"))
                                                    {
                                                        if (dr["BillState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillState = xmldocadd.CreateElement("State");
                                                            BillState.InnerText = dr["BillState"].ToString();
                                                            BillAddress.AppendChild(BillState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillPostalCode"))
                                                    {
                                                        if (dr["BillPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                            BillAddress.AppendChild(BillPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCountry"))
                                                    {
                                                        if (dr["BillCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                            BillCountry.InnerText = dr["BillCountry"].ToString();
                                                            BillAddress.AppendChild(BillCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillNote"))
                                                    {
                                                        if (dr["BillNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                            BillNote.InnerText = dr["BillNote"].ToString();
                                                            BillAddress.AppendChild(BillNote);
                                                        }
                                                    }
                                                }

                                                #endregion

                                                #region Adding Ship Address of Customer.
                                                if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                   (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                {
                                                    XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                    CustomerAdd.AppendChild(ShipAddress);
                                                    if (dt.Columns.Contains("ShipAddr1"))
                                                    {

                                                        if (dr["ShipAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                            ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr2"))
                                                    {
                                                        if (dr["ShipAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                            ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr3"))
                                                    {
                                                        if (dr["ShipAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                            ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr4"))
                                                    {
                                                        if (dr["ShipAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                            ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr5"))
                                                    {
                                                        if (dr["ShipAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                            ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCity"))
                                                    {
                                                        if (dr["ShipCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                            ShipCity.InnerText = dr["ShipCity"].ToString();
                                                            ShipAddress.AppendChild(ShipCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipState"))
                                                    {
                                                        if (dr["ShipState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipState = xmldocadd.CreateElement("State");
                                                            ShipState.InnerText = dr["ShipState"].ToString();
                                                            ShipAddress.AppendChild(ShipState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipPostalCode"))
                                                    {
                                                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                            ShipAddress.AppendChild(ShipPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCountry"))
                                                    {
                                                        if (dr["ShipCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                            ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                            ShipAddress.AppendChild(ShipCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipNote"))
                                                    {
                                                        if (dr["ShipNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                            ShipNote.InnerText = dr["ShipNote"].ToString();
                                                            ShipAddress.AppendChild(ShipNote);
                                                        }
                                                    }

                                                }


                                                #endregion

                                                if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                                {
                                                    if (dt.Columns.Contains("Phone"))
                                                    {
                                                        if (dr["Phone"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                            Phone.InnerText = dr["Phone"].ToString();
                                                            CustomerAdd.AppendChild(Phone);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Fax"))
                                                    {
                                                        if (dr["Fax"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                            Fax.InnerText = dr["Fax"].ToString();
                                                            CustomerAdd.AppendChild(Fax);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Email"))
                                                    {
                                                        if (dr["Email"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Email = xmldocadd.CreateElement("Email");
                                                            Email.InnerText = dr["Email"].ToString();
                                                            CustomerAdd.AppendChild(Email);
                                                        }
                                                    }
                                                }

                                                string custinput = xmldocadd.OuterXml;
                                                string respcust = string.Empty;

                                                try
                                                {
                                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                    {
                                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                    }

                                                    else
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                }

                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                finally
                                                {
                                                    if (respcust != string.Empty)
                                                    {
                                                        System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                        outputcustXMLDoc.LoadXml(respcust);
                                                        foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                        {
                                                            string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                            if (statusSeveritycust == "Error")
                                                            {
                                                                string msg = "New Customer could not be created into QuickBooks \n ";
                                                                msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                //Task 1435 (Axis 6.0):
                                                                ErrorSummary summary = new ErrorSummary(msg);
                                                                summary.ShowDialog();
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            #endregion
                            }
                        }
                    }                   

                    //Solution for BUG 633
                    if (dt.Columns.Contains("ItemFullName"))
                    {
                        if (dr["ItemFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemFullName"].ToString();
                            }

                            #region Setting SalesTaxCode and IsTaxIncluded

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string IsTaxIncluded = string.Empty;
                            string netRate = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;

                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);


                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }

                                //validate IsTaxInluded value if present
                                if (dt.Columns.Contains("IsTaxIncluded"))
                                {
                                    if (dr["IsTaxIncluded"].ToString() != string.Empty && dr["IsTaxIncluded"].ToString() != "<None>")
                                    {
                                        int result = 0;
                                        if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                        {
                                            IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                            {
                                                IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }

                                    }
                                }

                                //Calculate cost
                                if (dt.Columns.Contains("Rate"))
                                {
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                        {
                                            if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                            {
                                                decimal Rate;
                                                if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                                {
                                                    //Rate = 0;
                                                    netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                }
                                                else
                                                {
                                                    Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    netRate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                        }
                                        if (netRate == string.Empty)
                                        {
                                            netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dt.Columns.Contains("Rate"))
                                {
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                    }
                                }
                            }

                            #endregion

                            #region Set Item Query

                            for (int i = 0; i < arr.Length; i++)
                            {
                                int item = 0;
                                int a = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");


                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");

                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        ItemQueryRq.AppendChild(FullName);
                                    }                                   

                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }





                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {

                                                if (defaultSettings.Type == "NonInventoryPart")
                                                {
                                                    #region Item NonInventory Add Query

                                                    XmlDocument ItemNonInvendoc = new XmlDocument();
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                                    XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                    ItemNonInvendoc.AppendChild(qbXMLINI);
                                                    XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                    qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                    qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                    ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                    ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                    XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                    ININame.InnerText = arr[i];
                                                    //ININame.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemNonInventoryAdd.AppendChild(ININame);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }

                                                    //Adding Tax Code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {

                                                            XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                            ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                                    //Adding Desc And Rate
                                                    //Solution for BUG 631 and 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement INIDesc = ItemNonInvendoc.CreateElement("Desc");
                                                            INIDesc.InnerText = dr["Description"].ToString();
                                                            INISalesAndPurchase.AppendChild(INIDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemNonInvendoc.CreateElement("Price");
                                                            ISRate.InnerText = netRate;
                                                            INISalesAndPurchase.AppendChild(ISRate);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                        INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                        //INIFullName.InnerText = "Sales";
                                                        INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                    }
                                                    string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;
                                                    string respItemNonInvendoc = string.Empty;
                                                    try
                                                    {
                                                        respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemNonInvendoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strtest2 = respItemNonInvendoc;

                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "Service")
                                                {
                                                    #region Item Service Add Query

                                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];                                                  
                                                    ItemServiceAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }
                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                        }
                                                    }


                                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                                    //Adding Desc And Rate
                                                    //Solution for BUG 631 and 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ISSalesAndPurchase.AppendChild(ISDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemServiceAdddoc.CreateElement("Price");
                                                            ISRate.InnerText = netRate;
                                                            ISSalesAndPurchase.AppendChild(ISRate);
                                                            IsPresent = true;
                                                        }
                                                    }

                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                        //Adding IncomeAccount FullName.
                                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                        IsPresent = true;
                                                    }

                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                    //ItemServiceAdddoc.Save("C://ItemServiceAdddoc.xml");
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenMultiUser);

                                                        respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemInventoryAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                ItemInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }

                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    //Adding Desc
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ItemInventoryAdd.AppendChild(ISDesc);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                            ISRate.InnerText = netRate;
                                                            ItemInventoryAdd.AppendChild(ISRate);
                                                        }
                                                    }


                                                    //Adding IncomeAccountRef
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                    }

                                                    //Adding COGSAccountRef
                                                    if (defaultSettings.COGSAccount != string.Empty)
                                                    {
                                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                    }

                                                    //Adding AssetAccountRef
                                                    if (defaultSettings.AssetAccount != string.Empty)
                                                    {
                                                        XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                        XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                        INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                    }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenMultiUser);
                                                        respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest4 = respItemInventoryAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
                else
                {
                    return null;
                }
            }
            #endregion
          
            #endregion

            return coll;
        }
    }
}