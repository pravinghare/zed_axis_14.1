using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemOtherChargeClass
    {
        private static ImportItemOtherChargeClass m_ImportItemOtherChargeClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemOtherChargeClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemOtherCharge class
        /// </summary>
        /// <returns></returns>
        public static ImportItemOtherChargeClass GetInstance()
        {
            if (m_ImportItemOtherChargeClass == null)
                m_ImportItemOtherChargeClass = new ImportItemOtherChargeClass();
            return m_ImportItemOtherChargeClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemOtherCharge and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemOtherCharge QuickBooks collection </returns>
        public DataProcessingBlocks.ItemOtherChargeQBEntryCollection ImportItemOtherChargeData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemOtherCharge Entry collections.
            DataProcessingBlocks.ItemOtherChargeQBEntryCollection coll = new ItemOtherChargeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemOtherCharge Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemOtherChargeDt = new DateTime();
                    string datevalue = string.Empty;

                    //ItemOtherCharge Validation
                    DataProcessingBlocks.ItemOtherChargeQBEntry ItemOtherCharge = new ItemOtherChargeQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemOtherCharge.Name = dr["Name"].ToString().Substring(0, 31);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemOtherCharge.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemOtherCharge.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                ItemOtherCharge.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BarCodeValue"))
                    {
                        #region Validations of BarCodeValue
                        if (dr["BarCodeValue"].ToString() != string.Empty)
                        {
                            if (dr["BarCodeValue"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BarCodeValue (" + dr["BarCodeValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemOtherCharge.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemOtherCharge.BarCode.BarCodeValue == null)
                                            ItemOtherCharge = null;
                                        else
                                            ItemOtherCharge.BarCode = new BarCode(dr["BarCodeValue"].ToString().Substring(0, 50));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemOtherCharge.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemOtherCharge.BarCode.BarCodeValue == null)
                                            ItemOtherCharge = null;
                                    }
                                }
                                else
                                {
                                    ItemOtherCharge.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                }
                            }
                            else
                            {
                                ItemOtherCharge.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                ItemOtherCharge.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    ItemOtherCharge.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        ItemOtherCharge.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemOtherCharge.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemOtherCharge.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemOtherCharge.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 651
                    if (dt.Columns.Contains("ClassRefFullName"))
                    {
                        #region Validations of Class FullName
                        if (dr["ClassRefFullName"].ToString() != string.Empty)
                        {
                            if (dr["ClassRefFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class FullName (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemOtherCharge.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemOtherCharge.ClassRef.FullName == null)
                                            ItemOtherCharge.ClassRef.FullName = null;
                                        else
                                            ItemOtherCharge.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemOtherCharge.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemOtherCharge.ClassRef.FullName == null)
                                            ItemOtherCharge.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemOtherCharge.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                    if (ItemOtherCharge.ClassRef.FullName == null)
                                        ItemOtherCharge.ClassRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemOtherCharge.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                if (ItemOtherCharge.ClassRef.FullName == null)
                                    ItemOtherCharge.ClassRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        #region Validations of ParentFullName
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            if (dr["ParentFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Parent FullName (" + dr["ParentFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemOtherCharge.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (ItemOtherCharge.ParentRef.FullName == null)
                                            ItemOtherCharge.ParentRef.FullName = null;
                                        else
                                            ItemOtherCharge.ParentRef = new ParentRef(dr["ParentFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemOtherCharge.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (ItemOtherCharge.ParentRef.FullName == null)
                                            ItemOtherCharge.ParentRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemOtherCharge.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                    if (ItemOtherCharge.ParentRef.FullName == null)
                                        ItemOtherCharge.ParentRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemOtherCharge.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                if (ItemOtherCharge.ParentRef.FullName == null)
                                    ItemOtherCharge.ParentRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsTaxInclude"))
                    {
                        #region Validations of IsTaxInclude
                        if (dr["IsTaxInclude"].ToString() != "<None>" || dr["IsTaxInclude"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsTaxInclude"].ToString(), out result))
                            {
                                ItemOtherCharge.IsTaxIncluded = Convert.ToInt32(dr["IsTaxInclude"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsTaxInclude"].ToString().ToLower() == "true")
                                {
                                    ItemOtherCharge.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsTaxInclude"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        ItemOtherCharge.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsTaxInclude (" + dr["IsTaxInclude"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemOtherCharge.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemOtherCharge.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemOtherCharge.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesTaxCodeFullName"))
                    {
                        #region Validations of SalesTaxCode FullName
                        if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTaxCode FullName (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemOtherCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemOtherCharge.SalesTaxCodeRef.FullName == null)
                                            ItemOtherCharge.SalesTaxCodeRef.FullName = null;
                                        else
                                            ItemOtherCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemOtherCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemOtherCharge.SalesTaxCodeRef.FullName == null)
                                            ItemOtherCharge.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemOtherCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                    if (ItemOtherCharge.SalesTaxCodeRef.FullName == null)
                                        ItemOtherCharge.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemOtherCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                if (ItemOtherCharge.SalesTaxCodeRef.FullName == null)
                                    ItemOtherCharge.SalesTaxCodeRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    #region Checking and setting SalesTaxCode

                    if (defaultSettings == null)
                    {
                        CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return null;

                    }
                    //string IsTaxable = string.Empty;
                    string TaxRateValue = string.Empty;
                    string ItemSaleTaxFullName = string.Empty;
                    //if default settings contain checkBoxGrossToNet checked.
                    if (defaultSettings.GrossToNet == "1")
                    {
                        if (dt.Columns.Contains("SalesTaxCodeFullName"))
                        {
                            if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                string FullName = dr["SalesTaxCodeFullName"].ToString();
                                //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                            }
                        }
                    }
                    #endregion


                    SalesOrPurchase salesOrPurchaseItem = new SalesOrPurchase();

                    if (dt.Columns.Contains("SalesOrPurchaseDesc"))
                    {
                        #region Validations of SalesOrPurchaseDesc
                        if (dr["SalesOrPurchaseDesc"].ToString() != string.Empty)
                        {
                            if (dr["SalesOrPurchaseDesc"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesOrPurchaseDesc (" + dr["SalesOrPurchaseDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        salesOrPurchaseItem.Desc = dr["SalesOrPurchaseDesc"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        salesOrPurchaseItem.Desc = dr["SalesOrPurchaseDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    salesOrPurchaseItem.Desc = dr["SalesOrPurchaseDesc"].ToString();
                                }
                            }
                            else
                            {
                                salesOrPurchaseItem.Desc = dr["SalesOrPurchaseDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesOrPurchasePrice"))
                    {
                        #region Validations for SalesOrPurchasePrice
                        if (dr["SalesOrPurchasePrice"].ToString() != string.Empty)
                        {
                            //decimal amount;
                            decimal cost = 0;
                            if (!decimal.TryParse(dr["SalesOrPurchasePrice"].ToString(), out cost))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesOrPurchasePrice ( " + dr["SalesOrPurchasePrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string strRate = dr["SalesOrPurchasePrice"].ToString();
                                        salesOrPurchaseItem.Price  = strRate;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string strRate = dr["SalesOrPurchasePrice"].ToString();
                                        salesOrPurchaseItem.Price  = strRate;
                                    }
                                }
                                else
                                {
                                    string strRate = dr["SalesOrPurchasePrice"].ToString();
                                    salesOrPurchaseItem.Price  = strRate;
                                }
                            }
                            else
                            {

                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (TaxRateValue != string.Empty && ItemOtherCharge.IsTaxIncluded != null && ItemOtherCharge.IsTaxIncluded != string.Empty)
                                    {
                                        if (ItemOtherCharge.IsTaxIncluded == "true" || ItemOtherCharge.IsTaxIncluded == "1")
                                        {
                                            decimal Cost = Convert.ToDecimal(dr["SalesOrPurchasePrice"].ToString());
                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                            {
                                                //decimal TaxRate = 10;
                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                cost = Cost / (1 + (TaxRate / 100));
                                            }

                                            salesOrPurchaseItem.Price  = Convert.ToString(Math.Round(cost, 5));
                                        }
                                    }
                                    //Check if ItemLine.Amount is null
                                    if (salesOrPurchaseItem.Price  == null)
                                    {
                                        salesOrPurchaseItem.Price  = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesOrPurchasePrice"].ToString()));
                                    }
                                }
                                else
                                {
                                    salesOrPurchaseItem.Price  = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesOrPurchasePrice"].ToString()));
                                }

                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("SalesOrPurchasePricePercent"))
                    {
                        #region Validations of SalesOrPurchasePrice Percent
                        if (dr["SalesOrPurchasePricePercent"].ToString() != string.Empty)
                        {
                            if (dr["SalesOrPurchasePricePercent"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesOrPurchase PricePercent (" + dr["SalesOrPurchasePricePercent"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        salesOrPurchaseItem.PricePercent = dr["SalesOrPurchasePricePercent"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        salesOrPurchaseItem.PricePercent = dr["SalesOrPurchasePricePercent"].ToString();
                                    }
                                }
                                else
                                {
                                    salesOrPurchaseItem.PricePercent = dr["SalesOrPurchasePricePercent"].ToString();
                                }
                            }
                            else
                            {
                                salesOrPurchaseItem.PricePercent = dr["SalesOrPurchasePricePercent"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesOrPurchaseAccountFullName"))
                    {
                        #region Validations of SalesOrPurchaseAccountFullName
                        if (dr["SalesOrPurchaseAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesOrPurchaseAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesOrPurchase Account FullName (" + dr["SalesOrPurchaseAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        salesOrPurchaseItem.AccountRef = new AccountRef(dr["SalesOrPurchaseAccountFullName"].ToString());
                                        if (salesOrPurchaseItem.AccountRef.FullName == null)
                                            salesOrPurchaseItem.AccountRef.FullName = null;
                                        else
                                            salesOrPurchaseItem.AccountRef = new AccountRef(dr["SalesOrPurchaseAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        salesOrPurchaseItem.AccountRef = new AccountRef(dr["SalesOrPurchaseAccountFullName"].ToString());
                                        if (salesOrPurchaseItem.AccountRef.FullName == null)
                                            salesOrPurchaseItem.AccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    salesOrPurchaseItem.AccountRef = new AccountRef(dr["SalesOrPurchaseAccountFullName"].ToString());
                                    if (salesOrPurchaseItem.AccountRef.FullName == null)
                                        salesOrPurchaseItem.AccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                salesOrPurchaseItem.AccountRef = new AccountRef(dr["SalesOrPurchaseAccountFullName"].ToString());
                                if (salesOrPurchaseItem.AccountRef.FullName == null)
                                    salesOrPurchaseItem.AccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (salesOrPurchaseItem.AccountRef != null || salesOrPurchaseItem.Desc != null || salesOrPurchaseItem.Price != null || salesOrPurchaseItem.PricePercent != null)
                        ItemOtherCharge.SalesOrPurchase.Add(salesOrPurchaseItem);




                    SalesAndPurchase SalesAndPurchaseItem = new SalesAndPurchase();

                    if (dt.Columns.Contains("SalesAndPurchaseSalesDesc"))
                    {
                        #region Validations of SalesAndPurchaseSalesDesc
                        if (dr["SalesAndPurchaseSalesDesc"].ToString() != string.Empty)
                        {
                            if (dr["SalesAndPurchaseSalesDesc"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchaseSalesDesc (" + dr["SalesAndPurchaseSalesDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesAndPurchaseItem.SalesDesc = dr["SalesAndPurchaseSalesDesc"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesAndPurchaseItem.SalesDesc = dr["SalesAndPurchaseSalesDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.SalesDesc = dr["SalesAndPurchaseSalesDesc"].ToString();
                                }
                            }
                            else
                            {
                                SalesAndPurchaseItem.SalesDesc = dr["SalesAndPurchaseSalesDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesAndPurchaseSalesPrice"))
                    {
                        #region Validations for SalesAndPurchaseSalesPrice
                        if (dr["SalesAndPurchaseSalesPrice"].ToString() != string.Empty)
                        {
                            //decimal amount;
                            decimal cost = 0;
                            if (!decimal.TryParse(dr["SalesAndPurchaseSalesPrice"].ToString(), out cost))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchaseSalesPrice ( " + dr["SalesAndPurchaseSalesPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string strRate = dr["SalesAndPurchaseSalesPrice"].ToString();
                                        SalesAndPurchaseItem.SalesPrice = strRate;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string strRate = dr["SalesAndPurchaseSalesPrice"].ToString();
                                        SalesAndPurchaseItem.SalesPrice = strRate;
                                    }
                                }
                                else
                                {
                                    string strRate = dr["SalesAndPurchaseSalesPrice"].ToString();
                                    SalesAndPurchaseItem.SalesPrice = strRate;
                                }
                            }
                            else
                            {

                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (TaxRateValue != string.Empty && ItemOtherCharge.IsTaxIncluded != null && ItemOtherCharge.IsTaxIncluded != string.Empty)
                                    {
                                        if (ItemOtherCharge.IsTaxIncluded == "true" || ItemOtherCharge.IsTaxIncluded == "1")
                                        {
                                            decimal Cost = Convert.ToDecimal(dr["SalesAndPurchaseSalesPrice"].ToString());
                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                            {
                                                //decimal TaxRate = 10;
                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                cost = Cost / (1 + (TaxRate / 100));
                                            }

                                            SalesAndPurchaseItem.SalesPrice = Convert.ToString(Math.Round(cost, 5));
                                        }
                                    }
                                    //Check if ItemLine.Amount is null
                                    if (SalesAndPurchaseItem.SalesPrice == null)
                                    {
                                        SalesAndPurchaseItem.SalesPrice = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesAndPurchaseSalesPrice"].ToString()));
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.SalesPrice = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesAndPurchaseSalesPrice"].ToString()));
                                }

                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("SalesAndPurchaseIncomeAccountFullName"))
                    {
                        #region Validations of SalesAndPurchase IncomeAccount FullName
                        if (dr["SalesAndPurchaseIncomeAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesAndPurchaseIncomeAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchaseIncomeAccountFullName (" + dr["SalesAndPurchaseIncomeAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesAndPurchaseItem.IncomeAccountRef = new IncomeAccountRef(dr["SalesAndPurchaseIncomeAccountFullName"].ToString());
                                        if (SalesAndPurchaseItem.IncomeAccountRef.FullName == null)
                                            SalesAndPurchaseItem.IncomeAccountRef.FullName = null;
                                        else
                                            SalesAndPurchaseItem.IncomeAccountRef = new IncomeAccountRef(dr["SalesAndPurchaseIncomeAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesAndPurchaseItem.IncomeAccountRef = new IncomeAccountRef(dr["SalesAndPurchaseIncomeAccountFullName"].ToString());
                                        if (SalesAndPurchaseItem.IncomeAccountRef.FullName == null)
                                            SalesAndPurchaseItem.IncomeAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.IncomeAccountRef = new IncomeAccountRef(dr["SalesAndPurchaseIncomeAccountFullName"].ToString());
                                    if (SalesAndPurchaseItem.IncomeAccountRef.FullName == null)
                                        SalesAndPurchaseItem.IncomeAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                SalesAndPurchaseItem.IncomeAccountRef = new IncomeAccountRef(dr["SalesAndPurchaseIncomeAccountFullName"].ToString());
                                if (SalesAndPurchaseItem.IncomeAccountRef.FullName == null)
                                    SalesAndPurchaseItem.IncomeAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesAndPurchasePurchaseDesc"))
                    {
                        #region Validations of SalesAndPurchasePurchaseDesc
                        if (dr["SalesAndPurchasePurchaseDesc"].ToString() != string.Empty)
                        {
                            if (dr["SalesAndPurchasePurchaseDesc"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchasePurchaseDesc (" + dr["SalesAndPurchasePurchaseDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesAndPurchaseItem.PurchaseDesc = dr["SalesAndPurchasePurchaseDesc"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesAndPurchaseItem.PurchaseDesc = dr["SalesAndPurchasePurchaseDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.PurchaseDesc = dr["SalesAndPurchasePurchaseDesc"].ToString();
                                }
                            }
                            else
                            {
                                SalesAndPurchaseItem.PurchaseDesc = dr["SalesAndPurchasePurchaseDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesAndPurchasePurchaseCost"))
                    {
                        #region Validations for SalesAndPurchasePurchaseCost
                        if (dr["SalesAndPurchasePurchaseCost"].ToString() != string.Empty)
                        {
                            //decimal amount;
                            decimal cost = 0;
                            if (!decimal.TryParse(dr["SalesAndPurchasePurchaseCost"].ToString(), out cost))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchasePurchaseCost ( " + dr["SalesAndPurchasePurchaseCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string strRate = dr["SalesAndPurchasePurchaseCost"].ToString();
                                        SalesAndPurchaseItem.PurchaseCost = strRate;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string strRate = dr["SalesAndPurchasePurchaseCost"].ToString();
                                        SalesAndPurchaseItem.PurchaseCost = strRate;
                                    }
                                }
                                else
                                {
                                    string strRate = dr["SalesAndPurchasePurchaseCost"].ToString();
                                    SalesAndPurchaseItem.PurchaseCost = strRate;
                                }
                            }
                            else
                            {

                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (TaxRateValue != string.Empty && ItemOtherCharge.IsTaxIncluded != null && ItemOtherCharge.IsTaxIncluded != string.Empty)
                                    {
                                        if (ItemOtherCharge.IsTaxIncluded == "true" || ItemOtherCharge.IsTaxIncluded == "1")
                                        {
                                            decimal Cost = Convert.ToDecimal(dr["SalesAndPurchasePurchaseCost"].ToString());
                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                            {
                                                //decimal TaxRate = 10;
                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                cost = Cost / (1 + (TaxRate / 100));
                                            }

                                            SalesAndPurchaseItem.PurchaseCost = Convert.ToString(Math.Round(cost, 5));
                                        }
                                    }
                                    //Check if ItemLine.Amount is null
                                    if (SalesAndPurchaseItem.PurchaseCost == null)
                                    {
                                        SalesAndPurchaseItem.PurchaseCost = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesAndPurchasePurchaseCost"].ToString()));
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.PurchaseCost = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesAndPurchasePurchaseCost"].ToString()));
                                }

                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("SalesAndPurchasePurchaseTaxCodeFullName"))
                    {
                        #region Validations of SalesAndPurchase PurchaseTaxCode FullName
                        if (dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString().Length > 3)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchasePurchaseTaxCodeFullName (" + dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesAndPurchaseItem.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString());
                                        if (SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName == null)
                                            SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName = null;
                                        else
                                            SalesAndPurchaseItem.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString().Substring(0, 3));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesAndPurchaseItem.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString());
                                        if (SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName == null)
                                            SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString());
                                    if (SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName == null)
                                        SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName = null;
                                }
                            }
                            else
                            {
                                SalesAndPurchaseItem.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["SalesAndPurchasePurchaseTaxCodeFullName"].ToString());
                                if (SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName == null)
                                    SalesAndPurchaseItem.PurchaseTaxCodeRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesAndPurchaseExpenseAccountFullName"))
                    {
                        #region Validations of SalesAndPurchase ExpenseAccount FullName
                        if (dr["SalesAndPurchaseExpenseAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesAndPurchaseExpenseAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchaseExpenseAccountFullName (" + dr["SalesAndPurchaseExpenseAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesAndPurchaseItem.ExpenseAccountRef = new ExpenseAccountRef(dr["SalesAndPurchaseExpenseAccountFullName"].ToString());
                                        if (SalesAndPurchaseItem.ExpenseAccountRef.FullName == null)
                                            SalesAndPurchaseItem.ExpenseAccountRef.FullName = null;
                                        else
                                            SalesAndPurchaseItem.ExpenseAccountRef = new ExpenseAccountRef(dr["SalesAndPurchaseExpenseAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesAndPurchaseItem.ExpenseAccountRef = new ExpenseAccountRef(dr["SalesAndPurchaseExpenseAccountFullName"].ToString());
                                        if (SalesAndPurchaseItem.ExpenseAccountRef.FullName == null)
                                            SalesAndPurchaseItem.ExpenseAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.ExpenseAccountRef = new ExpenseAccountRef(dr["SalesAndPurchaseExpenseAccountFullName"].ToString());
                                    if (SalesAndPurchaseItem.ExpenseAccountRef.FullName == null)
                                        SalesAndPurchaseItem.ExpenseAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                SalesAndPurchaseItem.ExpenseAccountRef = new ExpenseAccountRef(dr["SalesAndPurchaseExpenseAccountFullName"].ToString());
                                if (SalesAndPurchaseItem.ExpenseAccountRef.FullName == null)
                                    SalesAndPurchaseItem.ExpenseAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("SalesAndPurchasePrefVendorFullName"))
                    {
                        #region Validations of SalesAndPurchase PrefVendor FullName
                        if (dr["SalesAndPurchasePrefVendorFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesAndPurchasePrefVendorFullName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesAndPurchasePrefVendorFullName (" + dr["SalesAndPurchasePrefVendorFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        SalesAndPurchaseItem.PrefVendorRef = new PrefVendorRef(dr["SalesAndPurchasePrefVendorFullName"].ToString());
                                        if (SalesAndPurchaseItem.PrefVendorRef.FullName == null)
                                            SalesAndPurchaseItem.PrefVendorRef.FullName = null;
                                        else
                                            SalesAndPurchaseItem.PrefVendorRef = new PrefVendorRef(dr["SalesAndPurchasePrefVendorFullName"].ToString().Substring(0, 41));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        SalesAndPurchaseItem.PrefVendorRef = new PrefVendorRef(dr["SalesAndPurchasePrefVendorFullName"].ToString());
                                        if (SalesAndPurchaseItem.PrefVendorRef.FullName == null)
                                            SalesAndPurchaseItem.PrefVendorRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    SalesAndPurchaseItem.PrefVendorRef = new PrefVendorRef(dr["SalesAndPurchasePrefVendorFullName"].ToString());
                                    if (SalesAndPurchaseItem.PrefVendorRef.FullName == null)
                                        SalesAndPurchaseItem.PrefVendorRef.FullName = null;
                                }
                            }
                            else
                            {
                                SalesAndPurchaseItem.PrefVendorRef = new PrefVendorRef(dr["SalesAndPurchasePrefVendorFullName"].ToString());
                                if (SalesAndPurchaseItem.PrefVendorRef.FullName == null)
                                    SalesAndPurchaseItem.PrefVendorRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (SalesAndPurchaseItem.ExpenseAccountRef != null || SalesAndPurchaseItem.IncomeAccountRef != null || SalesAndPurchaseItem.PrefVendorRef != null || SalesAndPurchaseItem.PurchaseCost != null || SalesAndPurchaseItem.PurchaseDesc != null || SalesAndPurchaseItem.PurchaseTaxCodeRef != null || SalesAndPurchaseItem.SalesDesc != null || SalesAndPurchaseItem.SalesPrice != null)
                        ItemOtherCharge.SalesAndPurchase.Add(SalesAndPurchaseItem);



                    


                    coll.Add(ItemOtherCharge);

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion

            #region Create Parent for item
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            string ItemName = dr["ParentFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ParentFullName"].ToString();
                            }
                            #region Setting SalesTaxCode and IsTaxIncluded
                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;
                            }
                            string TaxRateValue = string.Empty;
                            string IsTaxIncluded = string.Empty;
                            string netRate = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            string AccountFullName = string.Empty;
                            string IncomeAccountFullName = string.Empty;
                            string ExpenseAccountFullName = string.Empty;
                            if (dt.Columns.Contains("SalesOrPurchaseAccountFullName"))
                            {
                                if (dr["SalesOrPurchaseAccountFullName"].ToString() != string.Empty)
                                {
                                    AccountFullName = dr["SalesOrPurchaseAccountFullName"].ToString();
                                }
                            }
                            if (dt.Columns.Contains("SalesAndPurchaseIncomeAccountFullName"))
                            {
                                if (dr["SalesAndPurchaseIncomeAccountFullName"].ToString() != string.Empty)
                                {
                                    IncomeAccountFullName = dr["SalesAndPurchaseIncomeAccountFullName"].ToString();
                                }
                            }
                            if (dt.Columns.Contains("SalesAndPurchaseExpenseAccountFullName"))
                            {
                                if (dr["SalesAndPurchaseExpenseAccountFullName"].ToString() != string.Empty)
                                {
                                    ExpenseAccountFullName = dr["SalesAndPurchaseExpenseAccountFullName"].ToString();
                                }
                            }
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);
                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion
                            #region Set Item Query
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int item = 0;
                                int a = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        ItemQueryRq.AppendChild(FullName);
                                    }
                                    string pinput = pxmldoc.OuterXml;
                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }


                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {
                                                #region Item Inventory Add Query
                                                XmlDocument ItemOtherChargeAdddoc = new XmlDocument();
                                                ItemOtherChargeAdddoc.AppendChild(ItemOtherChargeAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                ItemOtherChargeAdddoc.AppendChild(ItemOtherChargeAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLIS = ItemOtherChargeAdddoc.CreateElement("QBXML");
                                                ItemOtherChargeAdddoc.AppendChild(qbXMLIS);
                                                XmlElement qbXMLMsgsRqIS = ItemOtherChargeAdddoc.CreateElement("QBXMLMsgsRq");
                                                qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                XmlElement ItemOtherChargeAddRq = ItemOtherChargeAdddoc.CreateElement("ItemOtherChargeAddRq");
                                                qbXMLMsgsRqIS.AppendChild(ItemOtherChargeAddRq);
                                                ItemOtherChargeAddRq.SetAttribute("requestID", "1");
                                                XmlElement ItemOtherChargeAdd = ItemOtherChargeAdddoc.CreateElement("ItemOtherChargeAdd");
                                                ItemOtherChargeAddRq.AppendChild(ItemOtherChargeAdd);
                                                XmlElement NameIS = ItemOtherChargeAdddoc.CreateElement("Name");
                                                NameIS.InnerText = arr[i];
                                                ItemOtherChargeAdd.AppendChild(NameIS);
                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = ItemOtherChargeAdddoc.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            XmlElement INIParent = ItemOtherChargeAdddoc.CreateElement("ParentRef");
                                                            ItemOtherChargeAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }
                                                if (defaultSettings.TaxCode != string.Empty)
                                                {
                                                    if (defaultSettings.TaxCode.Length < 4)
                                                    {
                                                        XmlElement INISalesTaxCodeRef = ItemOtherChargeAdddoc.CreateElement("SalesTaxCodeRef");
                                                        ItemOtherChargeAdd.AppendChild(INISalesTaxCodeRef);
                                                        XmlElement INISTCodeRefFullName = ItemOtherChargeAdddoc.CreateElement("FullName");
                                                        INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                        INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                    }
                                                }
                                                if (AccountFullName != string.Empty)
                                                {
                                                    if (defaultSettings.IncomeAccount != string.Empty || IncomeAccountFullName != string.Empty)
                                                    {
                                                        XmlElement INISalesOrPurchase = ItemOtherChargeAdddoc.CreateElement("SalesOrPurchase");
                                                        ItemOtherChargeAdd.AppendChild(INISalesOrPurchase);
                                                        XmlElement ISAccountRef = ItemOtherChargeAdddoc.CreateElement("AccountRef");
                                                        INISalesOrPurchase.AppendChild(ISAccountRef);
                                                        XmlElement ISFullName = ItemOtherChargeAdddoc.CreateElement("FullName");
                                                        if (AccountFullName != string.Empty)
                                                            ISFullName.InnerText = AccountFullName;
                                                        else
                                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISAccountRef.AppendChild(ISFullName);
                                                    }
                                                }
                                                else if (ExpenseAccountFullName != string.Empty || IncomeAccountFullName != string.Empty)
                                                {
                                                    XmlElement INISalesAndPurchase = ItemOtherChargeAdddoc.CreateElement("SalesAndPurchase");
                                                    ItemOtherChargeAdd.AppendChild(INISalesAndPurchase);
                                                    if (defaultSettings.IncomeAccount != string.Empty || IncomeAccountFullName != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemOtherChargeAdddoc.CreateElement("IncomeAccountRef");
                                                        INISalesAndPurchase.AppendChild(ISIncomeAccountRef);
                                                        XmlElement ISFullName = ItemOtherChargeAdddoc.CreateElement("FullName");
                                                        if (IncomeAccountFullName != string.Empty)
                                                            ISFullName.InnerText = IncomeAccountFullName;
                                                        else
                                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                    }
                                                    if (ExpenseAccountFullName != string.Empty)
                                                    {
                                                        XmlElement ISExpenseAccountRef = ItemOtherChargeAdddoc.CreateElement("ExpenseAccountRef");
                                                        INISalesAndPurchase.AppendChild(ISExpenseAccountRef);
                                                        XmlElement ISFullName = ItemOtherChargeAdddoc.CreateElement("FullName");
                                                        if (ExpenseAccountFullName != string.Empty)
                                                            ISFullName.InnerText = ExpenseAccountFullName;
                                                        ISExpenseAccountRef.AppendChild(ISFullName);
                                                    }
                                                }
                                                string ItemOtherChargeAddinput = ItemOtherChargeAdddoc.OuterXml;
                                                string respItemOtherChargeAddinputdoc = string.Empty;
                                                try
                                                {
                                                    respItemOtherChargeAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemOtherChargeAddinput);
                                                    System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                    outputXML.LoadXml(respItemOtherChargeAddinputdoc);
                                                    string StatusSeverity = string.Empty;
                                                    string statusMessage = string.Empty;
                                                    foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemOtherChargeAddRs"))
                                                    {
                                                        StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                        statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    }
                                                    outputXML.RemoveAll();
                                                    if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                    {
                                                        ErrorSummary summary = new ErrorSummary(statusMessage);
                                                        summary.ShowDialog();
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                string strTest3 = respItemOtherChargeAddinputdoc;
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
            return coll;
        }

    }
}
