﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using System.ComponentModel;
using EDI.Constant;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    public class ImportBillingRateClass
    {
        private static ImportBillingRateClass m_ImportBillingRateClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constuctor
        public ImportBillingRateClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import BillingRate class
        /// </summary>
        /// <returns></returns>
        public static ImportBillingRateClass GetInstance()
        {
            if (m_ImportBillingRateClass == null)
                m_ImportBillingRateClass = new ImportBillingRateClass();
            return m_ImportBillingRateClass;
        }

        /// <summary>
        /// This method is used for validating import data and create request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>BillingRate QuickBooks collection </returns>
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public DataProcessingBlocks.BillingRateQBEntryCollection ImportBillingRateData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            DataProcessingBlocks.BillingRateQBEntryCollection coll = new BillingRateQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }

                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }


                    if (dt.Columns.Contains("BillingRateName"))
                    {
                        #region Adding Name Attributes

                        BillingRateQBEntry BillingRate = new BillingRateQBEntry();
                        BillingRate = coll.FindBillingRateEntry(dr["BillingRateName"].ToString());
                        if (BillingRate == null)
                        {
                            BillingRate = new BillingRateQBEntry();
                            if (dt.Columns.Contains("BillingRateName"))
                            {
                                #region Validations of BillingRateName
                                if (dr["BillingRateName"].ToString() != string.Empty)
                                {
                                    string strName = dr["BillingRateName"].ToString();
                                    if (strName.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BillingRateName is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillingRate.Name = dr["BillingRateName"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillingRate.Name = dr["BillingRateName"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillingRate.Name = dr["BillingRateName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillingRate.Name = dr["BillingRateName"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("FixedBillingRate"))
                            {
                                #region Validations for FixedBillingRate
                                if (dr["FixedBillingRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["FixedBillingRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FixedBillingRate ( " + dr["FixedBillingRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["FixedBillingRate"].ToString();
                                                BillingRate.FixedBillingRate = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["FixedBillingRate"].ToString();
                                                BillingRate.FixedBillingRate = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["FixedBillingRate"].ToString();
                                            BillingRate.FixedBillingRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        BillingRate.FixedBillingRate = string.Format("{0:.00000}", Convert.ToDouble(dr["FixedBillingRate"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            DataProcessingBlocks.BillingRatePerItem billingRatePerItem = new BillingRatePerItem();
                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of Item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    billingRatePerItem.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    if (billingRatePerItem.ItemRef.FullName == null)
                                        billingRatePerItem.ItemRef.FullName = null;

                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomRate"))
                            {
                                #region Validations for CustomRate
                                if (dr["CustomRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CustomRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomRate ( " + dr["CustomRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["CustomRate"].ToString();
                                                billingRatePerItem.CustomRate = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["CustomRate"].ToString();
                                                billingRatePerItem.CustomRate = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["CustomRate"].ToString();
                                            billingRatePerItem.CustomRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        billingRatePerItem.CustomRate = string.Format("{0:.00000}", Convert.ToDouble(dr["CustomRate"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomRatePercent"))
                            {
                                #region Validations for CustomRatePercent
                                if (dr["CustomRatePercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CustomRatePercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomRatePercent ( " + dr["CustomRatePercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                                billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                                billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                            }
                                        }
                                        else
                                        {
                                            string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                            billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                        }
                                    }
                                    else
                                    {
                                        billingRatePerItem.CustomRatePercent = string.Format("{0:.00}", Convert.ToDouble(dr["CustomRatePercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("AdjustPercentage"))
                            {
                                #region Validations for AdjustPercentage
                                if (dr["AdjustPercentage"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["AdjustPercentage"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AdjustPercentage ( " + dr["AdjustPercentage"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                                billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                                billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                            }
                                        }
                                        else
                                        {
                                            string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                            billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                        }
                                    }
                                    else
                                    {

                                        billingRatePerItem.AdjustPercentage = string.Format("{0:.00}", Convert.ToDouble(dr["AdjustPercentage"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("AdjustBillingRateRelativeTo"))
                            {
                                #region validations of AdjustBillingRateRelativeTo
                                if (dr["AdjustBillingRateRelativeTo"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        billingRatePerItem.AdjustBillingRateRelativeTo = Convert.ToString((DataProcessingBlocks.AdjustBillingRateRelative)Enum.Parse(typeof(DataProcessingBlocks.AdjustBillingRateRelative), dr["AdjustBillingRateRelativeTo"].ToString(), true));
                                    }
                                    catch
                                    {
                                        billingRatePerItem.AdjustBillingRateRelativeTo = dr["AdjustBillingRateRelativeTo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (billingRatePerItem.ItemRef != null || billingRatePerItem.CustomRate != null ||
                                billingRatePerItem.CustomRatePercent != null || billingRatePerItem.AdjustPercentage != null ||
                                billingRatePerItem.AdjustBillingRateRelativeTo != null)
                            {
                                BillingRate.BillingRatePerItem.Add(billingRatePerItem);
                            }

                            coll.Add(BillingRate);
                        }
                        else
                        {
                            DataProcessingBlocks.BillingRatePerItem billingRatePerItem = new BillingRatePerItem();
                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of Item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    billingRatePerItem.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    if (billingRatePerItem.ItemRef.FullName == null)
                                        billingRatePerItem.ItemRef.FullName = null;

                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomRate"))
                            {
                                #region Validations for CustomRate
                                if (dr["CustomRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CustomRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomRate ( " + dr["CustomRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["CustomRate"].ToString();
                                                billingRatePerItem.CustomRate = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["CustomRate"].ToString();
                                                billingRatePerItem.CustomRate = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["CustomRate"].ToString();
                                            billingRatePerItem.CustomRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        billingRatePerItem.CustomRate = string.Format("{0:.00000}", Convert.ToDouble(dr["CustomRate"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomRatePercent"))
                            {
                                #region Validations for CustomRatePercent
                                if (dr["CustomRatePercent"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CustomRatePercent"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomRatePercent ( " + dr["CustomRatePercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                                billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                                billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                            }
                                        }
                                        else
                                        {
                                            string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                            billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                        }
                                    }
                                    else
                                    {
                                        billingRatePerItem.CustomRatePercent = string.Format("{0:.00}", Convert.ToDouble(dr["CustomRatePercent"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("AdjustPercentage"))
                            {
                                #region Validations for AdjustPercentage
                                if (dr["AdjustPercentage"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["AdjustPercentage"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AdjustPercentage ( " + dr["AdjustPercentage"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                                billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                                billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                            }
                                        }
                                        else
                                        {
                                            string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                            billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                        }
                                    }
                                    else
                                    {

                                        billingRatePerItem.AdjustPercentage = string.Format("{0:.00}", Convert.ToDouble(dr["AdjustPercentage"].ToString()));
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("AdjustBillingRateRelativeTo"))
                            {
                                #region validations of AdjustBillingRateRelativeTo
                                if (dr["AdjustBillingRateRelativeTo"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        billingRatePerItem.AdjustBillingRateRelativeTo = Convert.ToString((DataProcessingBlocks.AdjustBillingRateRelative)Enum.Parse(typeof(DataProcessingBlocks.AdjustBillingRateRelative), dr["AdjustBillingRateRelativeTo"].ToString(), true));
                                    }
                                    catch
                                    {
                                        billingRatePerItem.AdjustBillingRateRelativeTo = dr["AdjustBillingRateRelativeTo"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (billingRatePerItem.ItemRef != null || billingRatePerItem.CustomRate != null ||
                               billingRatePerItem.CustomRatePercent != null || billingRatePerItem.AdjustPercentage != null ||
                               billingRatePerItem.AdjustBillingRateRelativeTo != null)
                            {
                                BillingRate.BillingRatePerItem.Add(billingRatePerItem);
                            }
                            //coll.Add(BillingRate);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Without adding Name attributes

                        BillingRateQBEntry BillingRate = new BillingRateQBEntry();

                        if (dt.Columns.Contains("BillingRateName"))
                        {
                            #region Validations of BillingRateName
                            if (dr["BillingRateName"].ToString() != string.Empty)
                            {
                                string strName = dr["BillingRateName"].ToString();
                                if (strName.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BillingRateName is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillingRate.Name = dr["BillingRateName"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillingRate.Name = dr["BillingRateName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillingRate.Name = dr["BillingRateName"].ToString();
                                    }
                                }
                                else
                                {
                                    BillingRate.Name = dr["BillingRateName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("FixedBillingRate"))
                        {
                            #region Validations for FixedBillingRate
                            if (dr["FixedBillingRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["FixedBillingRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FixedBillingRate ( " + dr["FixedBillingRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["FixedBillingRate"].ToString();
                                            BillingRate.FixedBillingRate = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["FixedBillingRate"].ToString();
                                            BillingRate.FixedBillingRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["FixedBillingRate"].ToString();
                                        BillingRate.FixedBillingRate = strRate;
                                    }
                                }
                                else
                                {
                                    BillingRate.FixedBillingRate = string.Format("{0:.00000}", Convert.ToDouble(dr["FixedBillingRate"].ToString()));
                                }
                            }
                            #endregion
                        }

                        DataProcessingBlocks.BillingRatePerItem billingRatePerItem = new BillingRatePerItem();
                        if (dt.Columns.Contains("ItemRefFullName"))
                        {
                            #region Validations of Item Full name
                            if (dr["ItemRefFullName"].ToString() != string.Empty)
                            {
                                billingRatePerItem.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                if (billingRatePerItem.ItemRef.FullName == null)
                                    billingRatePerItem.ItemRef.FullName = null;

                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CustomRate"))
                        {
                            #region Validations for CustomRate
                            if (dr["CustomRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CustomRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomRate ( " + dr["CustomRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["CustomRate"].ToString();
                                            billingRatePerItem.CustomRate = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["CustomRate"].ToString();
                                            billingRatePerItem.CustomRate = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["CustomRate"].ToString();
                                        billingRatePerItem.CustomRate = strRate;
                                    }
                                }
                                else
                                {
                                    billingRatePerItem.CustomRate = string.Format("{0:.00000}", Convert.ToDouble(dr["CustomRate"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("CustomRatePercent"))
                        {
                            #region Validations for CustomRatePercent
                            if (dr["CustomRatePercent"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CustomRatePercent"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomRatePercent ( " + dr["CustomRatePercent"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                            billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                            billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                        }
                                    }
                                    else
                                    {
                                        string strAdjustPercentage = dr["CustomRatePercent"].ToString();
                                        billingRatePerItem.CustomRatePercent = strAdjustPercentage;
                                    }
                                }
                                else
                                {
                                    billingRatePerItem.CustomRatePercent = string.Format("{0:.00}", Convert.ToDouble(dr["CustomRatePercent"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("AdjustPercentage"))
                        {
                            #region Validations for AdjustPercentage
                            if (dr["AdjustPercentage"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["AdjustPercentage"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AdjustPercentage ( " + dr["AdjustPercentage"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                            billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                            billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                        }
                                    }
                                    else
                                    {
                                        string strAdjustPercentage = dr["AdjustPercentage"].ToString();
                                        billingRatePerItem.AdjustPercentage = strAdjustPercentage;
                                    }
                                }
                                else
                                {

                                    billingRatePerItem.AdjustPercentage = string.Format("{0:.00}", Convert.ToDouble(dr["AdjustPercentage"].ToString()));
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("AdjustBillingRateRelativeTo"))
                        {
                            #region validations of AdjustBillingRateRelativeTo
                            if (dr["AdjustBillingRateRelativeTo"].ToString() != string.Empty)
                            {
                                try
                                {
                                    billingRatePerItem.AdjustBillingRateRelativeTo = Convert.ToString((DataProcessingBlocks.AdjustBillingRateRelative)Enum.Parse(typeof(DataProcessingBlocks.AdjustBillingRateRelative), dr["AdjustBillingRateRelativeTo"].ToString(), true));
                                }
                                catch
                                {
                                    billingRatePerItem.AdjustBillingRateRelativeTo = dr["AdjustBillingRateRelativeTo"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (billingRatePerItem.ItemRef != null || billingRatePerItem.CustomRate != null ||
                                billingRatePerItem.CustomRatePercent != null || billingRatePerItem.AdjustPercentage != null ||
                                billingRatePerItem.AdjustBillingRateRelativeTo != null)
                        {
                            BillingRate.BillingRatePerItem.Add(billingRatePerItem);
                        }

                        coll.Add(BillingRate);
                        #endregion
                    }
                }
                else
                {
                    return null;
                }

            }
            #endregion

            #region Item Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                    if (dt.Columns.Contains("ItemRefFullName"))
                    {
                        if (dr["ItemRefFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemRefFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemRefFullName"].ToString();
                            }
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            return coll;
        }

    }
}
