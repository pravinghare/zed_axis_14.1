﻿using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    public class ListMergeQBEntry
    {
        #region member variables
        private string m_ListMergeType;
        private string m_MergeFrom;
        private string m_MergeTo;
        private string m_SameShipAddrOk;

        #endregion

        #region Construtor

        public ListMergeQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public string ListMergeType
        {
            get { return m_ListMergeType; }
            set { m_ListMergeType = value; }
        }

        public string MergeFrom
        {
            get { return m_MergeFrom; }
            set { m_MergeFrom = value; }
        }

        public string MergeTo
        {
            get { return m_MergeTo; }
            set { m_MergeTo = value; }
        }

        public string SameShipAddrOk
        {
            get { return m_SameShipAddrOk; }
            set { m_SameShipAddrOk = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used for merging list information
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool ListMergeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listIDMergeFrom, string editSqMergeFrom, string listIDMergeTo, string editSqMergeTo, string ListType, string sameShipAddOk)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ListMergeQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);
            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            requestXmlDoc = new System.Xml.XmlDocument();
            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement ListMergeQBEntryRq = requestXmlDoc.CreateElement("ListMergeRq");
            inner.AppendChild(ListMergeQBEntryRq);

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ListMergeRq").FirstChild;
            System.Xml.XmlElement listMergeType = requestXmlDoc.CreateElement("ListMergeType");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ListMergeRq").InsertBefore(listMergeType, firstChild).InnerText = ListType;

            System.Xml.XmlElement mergeFrom = requestXmlDoc.CreateElement("MergeFrom");
            ListMergeQBEntryRq.AppendChild(mergeFrom);
            //Bind Listid
            System.Xml.XmlElement ListIDFrom = requestXmlDoc.CreateElement("ListID");
            XmlText listIdFromText = requestXmlDoc.CreateTextNode(listIDMergeFrom);
            ListIDFrom.AppendChild(listIdFromText);
            mergeFrom.AppendChild(ListIDFrom);
            //Bind EditSquence
            System.Xml.XmlElement EditSequenceFrom = requestXmlDoc.CreateElement("EditSequence");
            XmlText editSequenceFromText = requestXmlDoc.CreateTextNode(editSqMergeFrom);
            EditSequenceFrom.AppendChild(editSequenceFromText);
            mergeFrom.AppendChild(EditSequenceFrom);

            System.Xml.XmlElement mergeTo = requestXmlDoc.CreateElement("MergeTo");
            ListMergeQBEntryRq.AppendChild(mergeTo);
            //Bind Listid
            System.Xml.XmlElement ListIDTo = requestXmlDoc.CreateElement("ListID");
            XmlText listIdToText = requestXmlDoc.CreateTextNode(listIDMergeTo);
            ListIDTo.AppendChild(listIdToText);
            mergeTo.AppendChild(ListIDTo);
            //Bind EditSquence
            System.Xml.XmlElement EditSequenceTo = requestXmlDoc.CreateElement("EditSequence");
            XmlText editSequenceToText = requestXmlDoc.CreateTextNode(editSqMergeTo);
            EditSequenceTo.AppendChild(editSequenceToText);
            mergeTo.AppendChild(EditSequenceTo);

            if (sameShipAddOk == null)
            {
                sameShipAddOk = "";
            }
            if (sameShipAddOk != "")
            {
                System.Xml.XmlElement sameAddress = requestXmlDoc.CreateElement("SameShipAddrOk");
                XmlText sameShipAddrOkText = requestXmlDoc.CreateTextNode(sameShipAddOk);
                sameAddress.AppendChild(sameShipAddrOkText);
                ListMergeQBEntryRq.AppendChild(sameAddress);
            }

            //For add request id to track error message
            string requeststring = string.Empty;
            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ListMergeRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            if (requesterror != string.Empty)
                            {
                                statusMessage += "The Error location is " + requesterror;
                            }
                        }
                    }

                    foreach (System.Xml.XmlNode ListId in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ListMergeRs/MergedTo"))
                    {
                        CommonUtilities.GetInstance().TxnId = ListId["ListID"].InnerText;
                    }
                 
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofMergeList(this, listIDMergeFrom,  editSqMergeFrom,  listIDMergeTo,  editSqMergeTo,  ListType,  sameShipAddOk);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
    }

    public class ListMergeQBEntryCollection : Collection<ListMergeQBEntry>
    {

    }

}
