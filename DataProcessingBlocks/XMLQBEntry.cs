using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using System.Configuration;
using System.IO;
using TransactionImporter;

namespace DataProcessingBlocks
{


    public class XMLQBEntry
    {


        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportXMLToQuickBooks(ref StringBuilder statusMessage, string AppName, string importType, XmlDocument requestXmlDoc)
        {
            string resp = string.Empty;
            string sb = string.Empty;           
            string responseFile = string.Empty;        

            try
            {
                #region commented code For saving Request xml file
                //logDirectory = ConfigurationManager.AppSettings.Get("LOGS");
                //if (logDirectory.StartsWith(Path.DirectorySeparatorChar.ToString()))
                //{
                //    logDirectory = Application.StartupPath + logDirectory;
                //    try
                //    {
                //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                //        else
                //            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                //    }
                //    catch { }
                //}
                //if (!Directory.Exists(logDirectory))
                //{
                //    try
                //    {
                //        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //            logDirectory = logDirectory.Replace("Program Files", Constants.xpPath);
                //        else
                //            logDirectory = logDirectory.Replace("Program Files", "Users\\Public\\Documents");
                //    }
                //    catch { }
                //    Directory.CreateDirectory(logDirectory);
                //}
                //RequestFile = logDirectory.EndsWith(Path.DirectorySeparatorChar.ToString()) ? logDirectory + "Request_" + DateTime.Now.Ticks.ToString() + ".xml" : logDirectory + Path.DirectorySeparatorChar + "Request_" + DateTime.Now.Ticks.ToString() + ".xml";
                //ResponseFile = RequestFile.Replace("Request_", "Response_");
                //try
                //{
                //    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                //    {
                //        RequestFile = RequestFile.Replace("Program Files", Constants.xpPath);
                //        ResponseFile = ResponseFile.Replace("Program Files", Constants.xpPath);
                //    }
                //    else
                //    {
                //        RequestFile = RequestFile.Replace("Program Files", "Users\\Public\\Documents");
                //        ResponseFile = ResponseFile.Replace("Program Files", "Users\\Public\\Documents");
                //    }
                //}
                //catch { }
                //requestXmlDoc.Save(RequestFile);
                #endregion
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);                
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);               
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);                   
                
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + importType + "AddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                        sb += oNode.Attributes["statusMessage"].Value.ToString();
                    }
                    statusMessage.AppendLine();
                    statusMessage.AppendLine();
                    statusMessage.Append(sb);
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }

            if (resp == string.Empty)
            {
                sb += "\n ";
                sb += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofqbXML(requestXmlDoc.OuterXml);
                sb += "\n ";
                statusMessage.AppendLine();
                statusMessage.AppendLine();
                statusMessage.Append(sb);
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;
                }
                else
                    return true;
            }

        }


        /// <summary>
        /// This method is used for getting existing Bill ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="RefNo"></param>
        /// <param name="AppName"></param>
        /// <param name="importType"></param>
        /// <returns></returns>
        public Hashtable checkAndGetXMLRefNoExistInQuickBook(string RefNo, string AppName, string importType)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            Hashtable JournalTable = new Hashtable();
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryQueryRq aggregate and fill in field values for it
            XmlElement JournalEntryQueryRq = requestXmlDoc.CreateElement(importType + "QueryRq");
            inner.AppendChild(JournalEntryQueryRq);

            //Create Refno aggregate and fill in field values for it
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            JournalEntryQueryRq.AppendChild(RefNumber);                     

            string resp = string.Empty;
            try
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    //Sending request for checking Journal entry in QuickBooks.
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    //Getting response of Journal Entry.
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return JournalTable;
            }

            if (resp == string.Empty)
            {
                return JournalTable;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no price level exists.
                    return JournalTable;

                }
                else
                    if (resp.Contains("statusSeverity=\"Warn\""))
                    {
                        //Returning means there is no Journal exists.
                        return JournalTable;
                    }
                    else
                    {
                        //Getting listid and Edit Sequence details of Journal.
                        string listID = string.Empty;
                        string editSequence = string.Empty;
                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + importType + "QueryRs/" + importType + "Ret"))
                        {
                            //Get ListID of price Level.
                            if (oNode.SelectSingleNode("TxnID") != null)
                            {
                                listID = oNode.SelectSingleNode("TxnID").InnerText.ToString();
                            }
                            //Get Edit Sequence of Price Level.
                            if (oNode.SelectSingleNode("EditSequence") != null)
                            {
                                editSequence = oNode.SelectSingleNode("EditSequence").InnerText.ToString();
                            }
                        }
                        if (listID != string.Empty && editSequence != string.Empty)
                        {
                            JournalTable.Add(listID, editSequence);
                        }
                        return JournalTable;
                    }
            }
        }



        /// <summary>
        /// This method is used for updating information
        /// of existing with listid and edit sequence.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="request"></param>
        /// <param name="RowCount"></param>
        /// <param name="p"></param>
        /// <param name="listID"></param>
        /// <param name="editSq"></param>
        /// <returns></returns>
        public bool UpdateXMLInQuickBooks(ref StringBuilder statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, string importType, string FileName)
        {
            string status = string.Empty;

            XmlDocument requestXmlDoc = new XmlDocument();
            requestXmlDoc.Load(FileName);
            XmlNode root = (XmlNode)requestXmlDoc.DocumentElement;

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            //create new object XmlDocumnet
            requestXmlDoc = new XmlDocument();

            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            requestXML = requestXML.Replace("<"+importType+"Add defMacro=\"MACROTYPE\">","<"+importType+"Mod>");     

            requestXML = requestXML.Replace("<"+importType+"Add>", "<"+importType+"Mod>");
            requestXML = requestXML.Replace("</" + importType + "Add>", "</" + importType + "Mod>");

            requestXML = requestXML.Replace("<" + importType + "AddRq>", "<" + importType + "ModRq>");
            requestXML = requestXML.Replace("</" + importType + "AddRq>", "</" + importType + "ModRq>");

            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            //e.q invoicelineAdd to invoicelineMod
            requestXML = requestXML.Replace("<"+importType+"LineAdd>", "<"+importType+"LineMod>");
            requestXML = requestXML.Replace("</"+importType+"LineAdd>", "</"+importType+"LineMod>");

            requestXML = requestXML.Replace("<" + importType + "LineGroupAdd>", "<" + importType + "LineGroupMod>");
            requestXML = requestXML.Replace("</" + importType + "LineGroupAdd>", "</" + importType + "LineGroupMod>");


            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");

            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");
            
            outer.InnerXml = requestXML;

            root = (XmlNode)requestXmlDoc.DocumentElement;
            XmlElement nodeMod = (XmlElement)root.ChildNodes[0].ChildNodes[0].ChildNodes[0];

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod/Phone");
                    nodeMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod/Fax");
                    nodeMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod/Email");
                    nodeMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }

            //For TxnID.Remove LinkToTxn
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod/"+importType+"LineMod"))
            {
                if (oNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod/"+importType+"LineMod/LinkToTxn");
                    node.ParentNode.RemoveChild(node);

                }
            }

            XmlElement nodeModRq = (XmlElement)root.ChildNodes[0].ChildNodes[0];

            if (requeststring != string.Empty)
                nodeModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber  : " + rowcount.ToString());
            else
                nodeModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());



            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod").InsertBefore(ListID, firstChild).InnerText = listID;

            //Create edit sequence aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/"+importType+"ModRq/"+importType+"Mod").InsertAfter(EditSequence, ListID).InnerText = editSequence;


            string resp = string.Empty;

            try
            {
              resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + importType + "ModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                            status += oNode.Attributes["statusMessage"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            status += "\n ";
                            status += "\nThe Error location is " + requesterror + "\n";
                            status += oNode.Attributes["statusMessage"].Value.ToString();
                        }
                    }
                    statusMessage.AppendLine();
                    statusMessage.AppendLine();
                    statusMessage.Append(status);
                }
            }
            if (resp == string.Empty)
            {               
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }     
    }
}
