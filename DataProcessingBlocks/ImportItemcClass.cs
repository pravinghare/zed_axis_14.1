﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using Xero.NetStandard.OAuth2.Model;

namespace DataProcessingBlocks
{
    class ImportItemcClass
    {
        private static ImportItemcClass m_ImportItemcClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemcClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Item class
        /// </summary>
        /// <returns></returns>
        public static ImportItemcClass GetInstance()
        {
            if (m_ImportItemcClass == null)
                m_ImportItemcClass = new ImportItemcClass();
            return m_ImportItemcClass;
        }

        /// <summary>
        /// This method is used for validating import data and create Items.
        /// import data into Xero.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate xml file.</param>
        /// <returns>Item Xero collection </returns>
        public DataProcessingBlocks.ItemXeroEntryCollection ImportItemData(DataTable dt, ref string logDirectory)
        {
            //Create an instance of Customer Entry collections.
            DataProcessingBlocks.ItemXeroEntryCollection coll = new ItemXeroEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            string datevalue = string.Empty;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;


            #region For Item Entry

            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    DataProcessingBlocks.ItemXeroEntry Item = new ItemXeroEntry();
                    //Item Validation

                    if (dt.Columns.Contains("Code"))
                    {
                        #region Validations of Code

                        if (dr["Code"].ToString() != string.Empty)
                        {
                            if (dr["Code"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Code ( " + dr["Code"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Item.Code = dr["Code"].ToString().Substring(0, 100);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Item.Code = dr["Code"].ToString();
                                    }
                                }
                                else
                                {
                                    Item.Code = dr["Code"].ToString();
                                }
                            }
                            else
                            {
                                Item.Code = dr["Code"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Description"))
                    {
                        #region Validations of Description
                        if (dr["Description"].ToString() != string.Empty)
                        {
                            if (dr["Description"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Description (" + dr["Description"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Item.Description = dr["Description"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Item.Description = dr["Description"].ToString();
                                    }
                                }
                                else
                                {
                                    Item.Description = dr["Description"].ToString();
                                }
                            }
                            else
                            {
                                Item.Description = dr["Description"].ToString();
                            }
                        }
                        #endregion
                    }

                    Purchase phn = new Purchase();
                    Purchase sal = new Purchase();

                    if (dt.Columns.Contains("PurchaseDetailsUnitPrice"))
                    {
                        double unitprice = 0;
                        #region Validations for ExchangeRate

                        if (dr["PurchaseDetailsUnitPrice"].ToString() != string.Empty)
                        {

                            decimal amount;
                            if (!decimal.TryParse(dr["PurchaseDetailsUnitPrice"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PurchaseDetailsUnitPrice ( " + dr["ExchangeRate"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        unitprice = Convert.ToDouble(dr["PurchaseDetailsUnitPrice"].ToString());
                                        phn.UnitPrice = (double?)unitprice;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        unitprice = Convert.ToDouble(dr["PurchaseDetailsUnitPrice"].ToString());
                                        phn.UnitPrice = (double?)unitprice;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                }
                                else
                                {
                                    unitprice = Convert.ToDouble(dr["PurchaseDetailsUnitPrice"].ToString());
                                    phn.UnitPrice = (double?)unitprice;
                                }
                            }
                            else
                            {
                                //phn.UnitPrice = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["PurchaseDetailsUnitPrice"].ToString()));
                                unitprice = Convert.ToDouble(dr["PurchaseDetailsUnitPrice"].ToString());
                                phn.UnitPrice = (double?)unitprice;
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("PurchaseDetailsAccountCode"))
                    {
                        #region Validations of LastName
                        if (dr["PurchaseDetailsAccountCode"].ToString() != string.Empty)
                        {
                            if (dr["PurchaseDetailsAccountCode"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PurchaseDetailsAccountCode ( " + dr["PurchaseDetailsAccountCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        phn.AccountCode = dr["PurchaseDetailsAccountCode"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        phn.AccountCode = dr["PurchaseDetailsAccountCode"].ToString();
                                    }
                                }
                                else
                                {
                                    phn.AccountCode = dr["PurchaseDetailsAccountCode"].ToString();
                                }
                            }
                            else
                            {
                                phn.AccountCode = dr["PurchaseDetailsAccountCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (phn.UnitPrice != null || phn.AccountCode != null)
                        Item.PurchaseDetails = phn;

                    if (dt.Columns.Contains("SalesDetailsUnitPrice"))
                    {
                        #region Validations for ExchangeRate
                        double unitprice = 0;
                        if (dr["SalesDetailsUnitPrice"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["SalesDetailsUnitPrice"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesDetailsUnitPrice ( " + dr["SalesDetailsUnitPrice"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        unitprice = Convert.ToDouble(dr["SalesDetailsUnitPrice"].ToString());
                                        sal.UnitPrice = (double?)unitprice;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        unitprice = Convert.ToDouble(dr["SalesDetailsUnitPrice"].ToString());
                                        sal.UnitPrice = (double?)unitprice;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                }
                                else
                                {
                                    unitprice = Convert.ToDouble(dr["SalesDetailsUnitPrice"].ToString());
                                    sal.UnitPrice = (double?)unitprice;
                                }
                            }
                            else
                            {
                                unitprice = Convert.ToDouble(dr["SalesDetailsUnitPrice"].ToString());
                                sal.UnitPrice = (double?)unitprice;
                            }
                        }

                        #endregion
                    }
                    if (dt.Columns.Contains("SalesDetailsAccountCode"))
                    {
                        #region Validations of BankAccountDetails
                        if (dr["SalesDetailsAccountCode"].ToString() != string.Empty)
                        {
                            if (dr["SalesDetailsAccountCode"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesDetailsAccountCode ( " + dr["SalesDetailsAccountCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        sal.AccountCode = dr["SalesDetailsAccountCode"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        sal.AccountCode = dr["SalesDetailsAccountCode"].ToString();
                                    }
                                }
                                else
                                {
                                    sal.AccountCode = dr["SalesDetailsAccountCode"].ToString();
                                }
                            }
                            else
                            {
                                sal.AccountCode = dr["SalesDetailsAccountCode"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (sal.UnitPrice != null || sal.AccountCode != null)
                        Item.SalesDetails = sal;

                    coll.Add(Item);

                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return coll;
        }
    }
}
