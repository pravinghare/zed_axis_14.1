using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemSalesTaxClass
    {
        private static ImportItemSalesTaxClass m_ImportItemSalesTaxClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemSalesTaxClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemSalesTax class
        /// </summary>
        /// <returns></returns>
        public static ImportItemSalesTaxClass GetInstance()
        {
            if (m_ImportItemSalesTaxClass == null)
                m_ImportItemSalesTaxClass = new ImportItemSalesTaxClass();
            return m_ImportItemSalesTaxClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemSalesTax and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemSalesTax QuickBooks collection </returns>
        public DataProcessingBlocks.ItemSalesTaxQBEntryCollection ImportItemSalesTaxData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemSalesTax Entry collections.
            DataProcessingBlocks.ItemSalesTaxQBEntryCollection coll = new ItemSalesTaxQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemSalesTax Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemSalesTaxDt = new DateTime();
                    string datevalue = string.Empty;

                    //ItemSalesTax Validation
                    DataProcessingBlocks.ItemSalesTaxQBEntry ItemSalesTax = new ItemSalesTaxQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemSalesTax.Name = dr["Name"].ToString().Substring(0, 31);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemSalesTax.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemSalesTax.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                ItemSalesTax.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                ItemSalesTax.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    ItemSalesTax.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        ItemSalesTax.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemSalesTax.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemSalesTax.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemSalesTax.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ItemDesc"))
                    {
                        #region Validations of ItemDesc
                        if (dr["ItemDesc"].ToString() != string.Empty)
                        {
                            if (dr["ItemDesc"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemDesc (" + dr["ItemDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemSalesTax.ItemDesc = dr["ItemDesc"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemSalesTax.ItemDesc = dr["ItemDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemSalesTax.ItemDesc = dr["ItemDesc"].ToString();
                                }
                            }
                            else
                            {
                                ItemSalesTax.ItemDesc = dr["ItemDesc"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("TaxRate"))
                    {
                        #region Validations of TaxRate
                        if (dr["TaxRate"].ToString() != string.Empty)
                        {
                            if (dr["TaxRate"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxRate (" + dr["TaxRate"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemSalesTax.TaxRate = dr["TaxRate"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemSalesTax.TaxRate = dr["TaxRate"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemSalesTax.TaxRate = dr["TaxRate"].ToString();
                                }
                            }
                            else
                            {
                                ItemSalesTax.TaxRate = dr["TaxRate"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TaxVendorFullName"))
                    {
                        #region Validations of TaxVendor FullName
                        if (dr["TaxVendorFullName"].ToString() != string.Empty)
                        {
                            if (dr["TaxVendorFullName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxVendor FullName (" + dr["TaxVendorFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemSalesTax.TaxVendorRef = new TaxVendorRef(dr["TaxVendorFullName"].ToString());
                                        if (ItemSalesTax.TaxVendorRef.FullName == null)
                                            ItemSalesTax.TaxVendorRef.FullName = null;
                                        else
                                            ItemSalesTax.TaxVendorRef = new TaxVendorRef(dr["TaxVendorFullName"].ToString().Substring(0, 41));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemSalesTax.TaxVendorRef = new TaxVendorRef(dr["TaxVendorFullName"].ToString());
                                        if (ItemSalesTax.TaxVendorRef.FullName == null)
                                            ItemSalesTax.TaxVendorRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemSalesTax.TaxVendorRef = new TaxVendorRef(dr["TaxVendorFullName"].ToString());
                                    if (ItemSalesTax.TaxVendorRef.FullName == null)
                                        ItemSalesTax.TaxVendorRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemSalesTax.TaxVendorRef = new TaxVendorRef(dr["TaxVendorFullName"].ToString());
                                if (ItemSalesTax.TaxVendorRef.FullName == null)
                                    ItemSalesTax.TaxVendorRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesTaxReturnLineFullName"))
                    {
                        #region Validations of UnitOfMeasureSet FullName
                        if (dr["SalesTaxReturnLineFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesTaxReturnLineFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTaxReturnLine FullName (" + dr["SalesTaxReturnLineFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemSalesTax.SalesTaxReturnLineRef = new SalesTaxReturnLineRef(dr["SalesTaxReturnLineFullName"].ToString());
                                        if (ItemSalesTax.SalesTaxReturnLineRef.FullName == null)
                                            ItemSalesTax.SalesTaxReturnLineRef.FullName = null;
                                        else
                                            ItemSalesTax.SalesTaxReturnLineRef = new SalesTaxReturnLineRef(dr["SalesTaxReturnLineFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemSalesTax.SalesTaxReturnLineRef = new SalesTaxReturnLineRef(dr["SalesTaxReturnLineFullName"].ToString());
                                        if (ItemSalesTax.SalesTaxReturnLineRef.FullName == null)
                                            ItemSalesTax.SalesTaxReturnLineRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemSalesTax.SalesTaxReturnLineRef = new SalesTaxReturnLineRef(dr["SalesTaxReturnLineFullName"].ToString());
                                    if (ItemSalesTax.SalesTaxReturnLineRef.FullName == null)
                                        ItemSalesTax.SalesTaxReturnLineRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemSalesTax.SalesTaxReturnLineRef = new SalesTaxReturnLineRef(dr["SalesTaxReturnLineFullName"].ToString());
                                if (ItemSalesTax.SalesTaxReturnLineRef.FullName == null)
                                    ItemSalesTax.SalesTaxReturnLineRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    

                   
                    coll.Add(ItemSalesTax);

                }
                else
                {
                    return null;
                }
            }
            #endregion
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                        
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }

    }
}
