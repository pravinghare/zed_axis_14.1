﻿using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;


namespace DataProcessingBlocks
{

    public class InventorySiteQBEntry
    {
        #region member variables
        private string m_ListID;
        private string m_Name;
        private string m_IsActive;
        private ParentSiteRef m_ParentSiteRef;
        private string m_SiteDesc;
        private string m_Contact;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;
        private Collection<SiteAddress> m_SiteAddress = new Collection<SiteAddress>();


        #endregion

        #region Construtor

        public InventorySiteQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public string ListID
        {
            get { return m_ListID; }
            set { m_ListID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public ParentSiteRef ParentSiteRef
        {
            get { return m_ParentSiteRef; }
            set { m_ParentSiteRef = value; }
        }

        public string SiteDesc
        {
            get { return m_SiteDesc; }
            set { m_SiteDesc = value; }
        }

        public string Contact
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        [XmlArray("SiteAddressREM")]
        public Collection<SiteAddress> SiteAddress
        {
            get { return m_SiteAddress; }
            set { m_SiteAddress = value; }
        }


        #endregion

        #region Public Methods
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {

                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InventorySiteQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);

            }
            catch (Exception ex)
            {
                // throw;
            }


            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));


            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement InventorySiteAddRq = requestXmlDoc.CreateElement("InventorySiteAddRq");
            inner.AppendChild(InventorySiteAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement InventorySiteAdd = requestXmlDoc.CreateElement("InventorySiteAdd");

            InventorySiteAddRq.AppendChild(InventorySiteAdd);
            requestXML = requestXML.Replace("<SiteAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</SiteAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<SiteAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<Name />", string.Empty);

            InventorySiteAdd.InnerXml = requestXML;

            //For add request id to track error messages
            string requeststring = string.Empty;

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventorySiteAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            if (requesterror != string.Empty)
                            {
                                statusMessage += "The Error location is " + requesterror;
                            }
                        }
                    }

                    string siteName = "";  string parentSiteFullName = ""; string siteListId = "";
                    foreach (System.Xml.XmlNode listId in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventorySiteAddRs/InventorySiteRet"))
                    {
                        siteListId = listId["ListID"].InnerText;
                        siteName = listId["Name"].InnerText;
                        CommonUtilities.GetInstance().TxnId = siteListId;
                    }
                    foreach (System.Xml.XmlNode parentName in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventorySiteAddRs/InventorySiteRet/ParentSiteRef"))
                    {
                        parentSiteFullName = parentName["FullName"].InnerText;
                    }

                    CommonUtilities.GetInstance().listParentClass.Add(new Tuple<string, string>(siteListId, parentSiteFullName));
                    CommonUtilities.GetInstance().listParentWithChild.Add(new Tuple<string, string,string>(siteListId, siteName, parentSiteFullName));

                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                // statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInvoice(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }


        /// <summary>
        /// This method is used for updating Inventorysite  information
        /// of existing Inventorysite with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateInventorySiteInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {

                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InventorySiteQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement InventorySiteQBEntryModRq = requestXmlDoc.CreateElement("InventorySiteModRq");
            inner.AppendChild(InventorySiteQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement InventorySiteMod = requestXmlDoc.CreateElement("InventorySiteMod");
            InventorySiteQBEntryModRq.AppendChild(InventorySiteMod);

            requestXML = requestXML.Replace("<SiteAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</SiteAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<SiteAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ListID />", string.Empty);
            requestXML = requestXML.Replace("<Name />", string.Empty);

            InventorySiteMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
          


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventorySiteModRq/InventorySiteMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventorySiteModRq/InventorySiteMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;

            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventorySiteModRq/InventorySiteMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;
            // requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventorySiteModRq/InventorySiteMod").RemoveChild(requestXmlDoc.SelectSingleNode("Name"));
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {

                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventorySiteModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            if (requesterror != string.Empty)
                            {
                                statusMessage += "The Error location is " + requesterror;
                            }
                        }
                    }

                    string siteName = "";  string parentSiteFullName = ""; string siteListId = "";
                    foreach (System.Xml.XmlNode listId in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventorySiteModRs/InventorySiteRet"))
                    {
                        siteListId = listId["ListID"].InnerText;
                        siteName = listId["Name"].InnerText;
                        CommonUtilities.GetInstance().TxnId = siteListId;
                    }
                    foreach (System.Xml.XmlNode parentName in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventorySiteModRs/InventorySiteRet/ParentSiteRef"))
                    {
                        parentSiteFullName = parentName["FullName"].InnerText;
                    }

                    CommonUtilities.GetInstance().listParentClass.Add(new Tuple<string, string>(siteListId, parentSiteFullName));
                    CommonUtilities.GetInstance().listParentWithChild.Add(new Tuple<string, string, string>(siteListId, siteName, parentSiteFullName));

                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInventorySite(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }


    public class InventorySiteQBEntryCollection : Collection<InventorySiteQBEntry>
    {

    }

}