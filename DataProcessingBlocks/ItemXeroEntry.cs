﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
//using XeroApi;
//using XeroApi.ConsoleApp;
//using XeroApi.Model;
//using XeroApi.Model.Reporting;
//using XeroApi.OAuth;
using System.Linq;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Api;
using DataProcessingBlocks.XeroConnection;
using Newtonsoft.Json;
using Xero.NetStandard.OAuth2.Models;
using System.Threading.Tasks;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("ItemXeroEntry", Namespace = "", IsNullable = false)]
    public class ItemXeroEntry
    {
        #region Private Member Variable
        private string m_Code;
        private string m_Description;
        private Purchase m_PurchaseDetails = new Purchase();
        private Purchase m_SalesDetails = new Purchase();
        #endregion

        #region Constructor
        public ItemXeroEntry()
        {
        }
        #endregion

        #region public property
        public string Code
        {
            get { return this.m_Code; }
            set { this.m_Code = value; }
        }
        public string Description
        {
            get { return this.m_Description; }
            set { this.m_Description = value; }
        }

        //[XmlIgnoreAttribute()]
        public Purchase PurchaseDetails
        {
            get { return m_PurchaseDetails; }
            set { m_PurchaseDetails = value; }
        }
        //[XmlIgnoreAttribute()]
        public Purchase SalesDetails
        {
            get { return m_SalesDetails; }
            set { m_SalesDetails = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// this method is used to fetch item name from xero
        /// </summary>
        /// <param name="itemcode"></param>
        /// <returns></returns>
        public async Task<bool> CheckAndGetNameExistsInXeroAsync(string itemcode)
        {
            try
            {
                Items itemResponse = new Items();
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                string query = "Code == \"" + itemcode + "\"";
                var response =  res.GetItemsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, null, query, null, null);
                response.Wait();
                itemResponse = response.Result;
                if (itemResponse._Items.Count > 0)
                {
                    TransactionImporter.TransactionImporter.refnoflag = true;
                    CommonUtilities.GetInstance().existTxnId = (itemResponse._Items[0].ItemID ?? Guid.Empty);
                }
                else
                {
                    CommonUtilities.GetInstance().existTxnId = Guid.Empty;
                    TransactionImporter.TransactionImporter.refnoflag = false;
                }
            }
            catch (Exception ex)
            {

            }
            return TransactionImporter.TransactionImporter.refnoflag;
        }


        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="txnid"></param>
        /// <returns></returns>
        public async Task<bool> ExportToXeroAsync(string statusMessage, string requestText, int rowcount, string txnid)
        {
            Items itemResponse = new Items();
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            //try
            //{
            //    DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemXeroEntry>.Save(this, fileName);
            //}
            //catch
            //{
            //    statusMessage += "\n ";
            //    statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
            //    return false;
            //}
            //System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            //requestXmlDoc.Load(fileName);
            //string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            //requestXmlDoc = new System.Xml.XmlDocument();
            //System.IO.File.Delete(fileName);

            DataProcessingBlocks.ItemXeroEntry Contact = new ItemXeroEntry();
            Item item = new Item();
            Items items = new Items();

            if (Code != string.Empty)
            {
                item.Code = Code;
            }

            if (Description != string.Empty)
            {
                item.Description = Description;
            }

            if (PurchaseDetails != null)
            {
                item.PurchaseDetails = PurchaseDetails;
            }

            if (SalesDetails != null)
            {
                item.SalesDetails = SalesDetails;
            }


            Items xeroItems = new Items();
            xeroItems._Items = new List<Item>();
            xeroItems._Items.Add(item);

            string resp = string.Empty;
            string responseFile = string.Empty;
            try
            {
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                CommonUtilities.GetInstance().Type = "Item";

                //requestText = ModelSerializer.Serialize(item);
                //requestXmlDoc.LoadXml(requestText);
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);

                if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == false)
                {
                    var response =  res.CreateItemAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, item);
                    response.Wait();
                    itemResponse = response.Result;
                    if (itemResponse._Items.Count > 0)
                    {
                        txnid = itemResponse._Items[0].ItemID.ToString();
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += itemResponse._Items[0].ValidationErrors;
                        statusMessage += "\n ";
                        TransactionImporter.TransactionImporter.testFlag = false;
                    }
                }
                else if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == true)
                {
                    if (TransactionImporter.TransactionImporter.refnoflag == true)
                    {
                        var response =  res.UpdateItemAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, CommonUtilities.GetInstance().existTxnId, xeroItems);
                        response.Wait();
                        itemResponse = response.Result;
                    }
                    else
                    {
                        var response =  res.CreateItemAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, item);
                        response.Wait();
                        itemResponse = response.Result;
                    }
                    if (itemResponse._Items.Count > 0)
                    {
                        txnid = itemResponse._Items[0].ItemID.ToString();
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += itemResponse._Items[0].ValidationErrors;
                        statusMessage += "\n ";
                        TransactionImporter.TransactionImporter.testFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                //CommonUtilities.GetInstance().SaveResponseFile(itemResponse.ToString(), responseFile);
            }

            TransactionImporter.TransactionImporter.testFlag = true;

            CommonUtilities.GetInstance().xeroMessage = statusMessage;
            CommonUtilities.GetInstance().xeroTxnId = txnid;
            return TransactionImporter.TransactionImporter.testFlag;
        }
        #endregion
    }

    public class ItemXeroEntryCollection : Collection<ItemXeroEntry>
    {

    }
}
