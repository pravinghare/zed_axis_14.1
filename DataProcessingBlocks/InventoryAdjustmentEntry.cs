using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using EDI.Constant;
using QuickBookEntities;
using System.Collections;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("InventoryAdjustmentEntry", Namespace = "", IsNullable = false)]
    public class InventoryAdjustmentEntry
    {
        #region Private Member Variables

        private AccountRef m_Accountref;
        private string m_TxnDate;
        private string m_RefNumber;
        private string m_SerialNumber;
        private string m_LotNumber;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private string m_Memo;
        private Collection<DataProcessingBlocks.InventoryAdjustmentLineAdd> m_InventoryAdjustmentLine = new Collection<DataProcessingBlocks.InventoryAdjustmentLineAdd>();

        #endregion

        #region Constructors
        public InventoryAdjustmentEntry()
        {

        }
        #endregion

        #region Public Properties

        public AccountRef AccountRef
        {
            get { return this.m_Accountref; }
            set { this.m_Accountref = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string RefNumber
        {
            get { return this.m_RefNumber; }
            set { this.m_RefNumber = value; }
        }


        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { this.m_SerialNumber = value; }
        }

        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set
            {
                this.m_InventorySiteLocationRef = value;
            }
        }

        public CustomerRef CustomerRef
        {
            get { return this.m_CustomerRef; }
            set { this.m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }


        [XmlArray("InventoryAdjustmentLineREM")]
        public Collection<InventoryAdjustmentLineAdd> InventoryAdjustmentLineAdd
        {
            get { return m_InventoryAdjustmentLine; }
            set { m_InventoryAdjustmentLine = value; }
        }
        #endregion

        #region Public Methods
        //For inventory Adjustment.
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InventoryAdjustmentEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            XmlDocument requestXmlDoc = new XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;
            requestXmlDoc = new XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create the InventorydjustmentRq
            XmlElement InventoryAddRq = requestXmlDoc.CreateElement("InventoryAdjustmentAddRq");
            inner.AppendChild(InventoryAddRq);

            //Create the InventoryAdd
            XmlElement InventoryAdd = requestXmlDoc.CreateElement("InventoryAdjustmentAdd");
            InventoryAddRq.AppendChild(InventoryAdd);

            requestXML = requestXML.Replace("<InventoryAdjustmentLineREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentLineREM>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineREM />", string.Empty);
            requestXML = requestXML.Replace("<TxnLineID />", string.Empty);

            InventoryAdd.InnerXml = requestXML;

            //Add requestId to track error message.
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentAddRq/InventoryAdjustmentAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }
            }
            //bug 496 commented code
            #region bug  496 not required this code bcoz it removes  InventorySiteLocationRef fullname from QBXML
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq//InventoryAdjustmentAddRq/InventoryAdjustmentAdd"))
            //{
            //    if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
            //    {
            //        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq//InventoryAdjustmentAddRq/InventoryAdjustmentAdd/InventorySiteLocationRef");
            //        //bug 496
            //        node.ParentNode.RemoveChild(node);

            //        requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
            //      node.RemoveAll();
            //    }
            //}
            #endregion

            if (requeststring != string.Empty)
                InventoryAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + "Row Number (By RefNumber) : " + rowcount.ToString());
            else
                InventoryAddRq.SetAttribute("requestID", "Row Number :" + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;

            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }





            //If response from QuickBook is empty.
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                //This method is used for checking InventoryAdjustment file or class.
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInventoryAdjustment(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                XmlDocument OutputXmlDoc = new XmlDocument();
                OutputXmlDoc.LoadXml(resp);

                foreach (XmlNode oNode in OutputXmlDoc.SelectNodes("QBXML/QBXMLMsgsRs/InventoryAdjustmentAddRs"))
                {
                    string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                    if (statusSeverity == "Error")
                    {
                        string requesterror = string.Empty;
                        try
                        {
                            requesterror = oNode.Attributes["requestID"].Value.ToString();
                        }
                        catch
                        {
                        }
                        string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                        foreach (string s in array_msg)
                        {
                            if (s != "")
                            { statusMessage += s + ".\n"; }
                            if (s == "")
                            { statusMessage += s; }
                        }
                        statusMessage += "The Error location is " + requesterror;
                    }
                }
                foreach (System.Xml.XmlNode oTxn in OutputXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventoryAdjustmentAddRs/InventoryAdjustmentRet"))
                {
                    CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                }
                CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;
                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Inventory adjustment ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Inventory adjustme Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create InventoryAdjustmentQueryRq aggregate and fill in field values for it
            XmlElement InventoryAdjustmentQueryRq = requestXmlDoc.CreateElement("InventoryAdjustmentQueryRq");
            inner.AppendChild(InventoryAdjustmentQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            InventoryAdjustmentQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            InventoryAdjustmentQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                // responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating InventoryAdjustment information
        /// of existing InventoryAdjustment with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateInventoryAdjustmentInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<Tuple<string, string>> lineTxnIdList = null)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }


                #region Assign TxnLineID to existing lines

                Collection<InventoryAdjustmentLineAdd> InventoryAdjustmentLineTemp = this.InventoryAdjustmentLineAdd;
                this.InventoryAdjustmentLineAdd = new Collection<DataProcessingBlocks.InventoryAdjustmentLineAdd>();
                lineTxnIdList.ForEach(item1 =>
                {

                    foreach (var item in InventoryAdjustmentLineTemp)
                    {
                        if (item.ItemRef != null && item.ItemRef.FullName != null)
                        {

                            if (item.ItemRef.FullName == item1.Item1.ToString())
                            {
                                item.TxnLineID = item1.Item2.ToString();
                                break;
                            }
                        }
                        if (item.TxnLineID == null)
                            item.TxnLineID = "-1";
                    }
                });


                lineTxnIdList.ForEach(item1 =>
                {
                    foreach (var item in InventoryAdjustmentLineTemp)
                    {
                        if (item.ItemRef != null && item.ItemRef.FullName != null && item.TxnLineID != "-1" && item.TxnLineID != null)
                        {

                            if (item.ItemRef.FullName == item1.Item1.ToString())
                            {
                                this.InventoryAdjustmentLineAdd.Add(item);
                                break;
                            }
                        }
                    }
                });

                foreach (var item in InventoryAdjustmentLineTemp)
                {
                    if (item.TxnLineID == "-1" || item.TxnLineID == null)
                    {
                        item.TxnLineID = "-1";
                        this.InventoryAdjustmentLineAdd.Add(item);
                    }
                }
                #endregion

            }
            catch { }
            try
            {
              

                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InventoryAdjustmentEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement InventoryAdjustmentQBEntryModRq = requestXmlDoc.CreateElement("InventoryAdjustmentModRq");
            inner.AppendChild(InventoryAdjustmentQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement InventoryAdjustmentMod = requestXmlDoc.CreateElement("InventoryAdjustmentMod");
            InventoryAdjustmentQBEntryModRq.AppendChild(InventoryAdjustmentMod);

            requestXML = requestXML.Replace("<InventoryAdjustmentPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentLineREM>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineREM />", string.Empty);
            requestXML = requestXML.Replace("<TxnLineID />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAdd>", "<InventoryAdjustmentLineMod>");
            requestXML = requestXML.Replace("</InventoryAdjustmentLineAdd>", "</InventoryAdjustmentLineMod>");

            //  requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            InventoryAdjustmentMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq//InventoryAdjustmentAddRq/InventoryAdjustmentAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq//InventoryAdjustmentAddRq/InventoryAdjustmentAdd/InventorySiteLocationRef");
                    node.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    node.RemoveAll();


                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentAddRq/InventoryAdjustmentAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod"))
            //{
            //    if (oNode.SelectSingleNode("QuantityAdjustment") != null)
            //    {
            //        XmlNodeList childNodeList = requestXmlDoc.GetElementsByTagName("QuantityAdjustment");
            //        //XmlNode childNodeList = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/QuantityAdjustment/QuantityDifference");
            //        foreach (XmlNode childNode in childNodeList)
            //        {
            //            if (!childNode.FirstChild.Name.Equals("QuantityDifference"))
            //                childNode.RemoveAll();
            //        }


            //    }
            //}

            #region  remove NewQuantity,NewValue from request because in mod request NewQuantity,NewValue not allowed 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/QuantityAdjustment"))
            {
                if (oNode.SelectSingleNode("NewQuantity") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/QuantityAdjustment/NewQuantity");
                    oNode.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment"))
            {
                if (oNode.SelectSingleNode("NewQuantity") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment/NewQuantity");
                    oNode.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment"))
            {
                if (oNode.SelectSingleNode("NewValue") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment/NewValue");
                    oNode.RemoveChild(childNode);
                }
            }
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<QuantityAdjustment>", string.Empty).Replace("</QuantityAdjustment>", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<ValueAdjustment>", string.Empty).Replace("</ValueAdjustment>", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<ValueAdjustment />", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<QuantityAdjustment />", string.Empty);


            #endregion

            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<QuantityAdjustment></QuantityAdjustment>", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<ValueAdjustment></ValueAdjustment>", string.Empty);

            //Axis 648 
            try
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod"))
                {
                    if (oNode.SelectSingleNode("QuantityDifference") != null)
                    {
                        XmlNode inventorySiteLocationRef = oNode.SelectSingleNode("InventorySiteLocationRef");
                        XmlNode quantityDifference = oNode.SelectSingleNode("QuantityDifference");
                        oNode.RemoveChild(quantityDifference);
                        oNode.InsertAfter(quantityDifference, inventorySiteLocationRef);
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            //Axis 648 ends

            if (requeststring != string.Empty)
                InventoryAdjustmentQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By InventoryAdjustmentRefNumber) : " + rowcount.ToString());
            else
                InventoryAdjustmentQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            InventoryAdjustmentMod.InnerXml = requestXML;

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventoryAdjustmentModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventoryAdjustmentModRs/InventoryAdjustmentRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInventoryAdjustment(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        //Bug No.412
        /// <summary>
        /// Appending data to existing transaction.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendInventoryAdjustmentInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InventoryAdjustmentEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create InventoryAdjustmentModRq aggregate and fill in field values for it
            System.Xml.XmlElement InventoryAdjustmentQBEntryModRq = requestXmlDoc.CreateElement("InventoryAdjustmentModRq");
            inner.AppendChild(InventoryAdjustmentQBEntryModRq);

            //Create InventoryAdjustmentMod aggregate and fill in field values for it
            System.Xml.XmlElement InventoryAdjustmentMod = requestXmlDoc.CreateElement("InventoryAdjustmentMod");
            InventoryAdjustmentQBEntryModRq.AppendChild(InventoryAdjustmentMod);

            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
            string addString = "";
            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></InventoryAdjustmentLineMod><InventoryAdjustmentLineMod>";

                XmlNode InventoryAdjustmentLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod");
                System.Xml.XmlElement InventoryAdjustmentLineGroupMod = requestXmlDoc.CreateElement("InventoryAdjustmentLineMod");
                System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                TxnLineID.InnerText = txnLineIDList[i];
                InventoryAdjustmentLineGroupMod.AppendChild(TxnLineID);
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertAfter(InventoryAdjustmentLineGroupMod, InventoryAdjustmentLineMod.ChildNodes[i == 0 ? i : i - 1]);
            }

            XmlNode InventoryAdjustmentMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod");

            requestXML = requestXML.Replace("<TxnID>" + listID + "</TxnID>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentAdd>", "<InventoryAdjustmentMod>");
            requestXML = requestXML.Replace("</InventoryAdjustmentAdd>", "</InventoryAdjustmentMod>");
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAdd>", "<InventoryAdjustmentLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("</InventoryAdjustmentLineAdd>", "</InventoryAdjustmentLineMod>");

            requestXML = requestXML.Replace("<InventoryAdjustmentPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineREM>", string.Empty);
            requestXML = requestXML.Replace("</InventoryAdjustmentLineREM>", string.Empty);
            requestXML = requestXML.Replace("<InventoryAdjustmentLineREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            //requestXML = requestXML.Replace("<TxnLineID />", string.Empty);

            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
            requestXML = requestXML.Replace("<InventoryAdjustmentLineAdd>", "<InventoryAdjustmentLineMod>");
            requestXML = requestXML.Replace("</InventoryAdjustmentLineAdd>", "</InventoryAdjustmentLineMod>");


            string[] InventoryAdjustmentModFragment;
            int indexInventoryAdjustmentLine = requestXML.IndexOf(@"<InventoryAdjustmentLineMod>");
            InventoryAdjustmentModFragment = requestXML.Split(new string[] { "<InventoryAdjustmentLineMod>" }, StringSplitOptions.None);
            if (InventoryAdjustmentModFragment.Length > 0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = InventoryAdjustmentModFragment[0];
                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").FirstChild;
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertBefore(xfrag1, firstChild1);
                requestXML = requestXML.Replace(InventoryAdjustmentModFragment[0], string.Empty);
            }

            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertAfter(xfrag, InventoryAdjustmentMod2.LastChild);

            requestText = requestXmlDoc.OuterXml;

            #region  remove NewQuantity,NewValue from request because in mod request NewQuantity,NewValue not allowed 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/QuantityAdjustment"))
            {
                if (oNode.SelectSingleNode("NewQuantity") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/QuantityAdjustment/NewQuantity");
                    oNode.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment"))
            {
                if (oNode.SelectSingleNode("NewQuantity") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment/NewQuantity");
                    oNode.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment"))
            {
                if (oNode.SelectSingleNode("NewValue") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod/InventoryAdjustmentLineMod/ValueAdjustment/NewValue");
                    oNode.RemoveChild(childNode);
                }
            }
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<QuantityAdjustment>", string.Empty).Replace("</QuantityAdjustment>", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<ValueAdjustment>", string.Empty).Replace("</ValueAdjustment>", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<ValueAdjustment />", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<QuantityAdjustment />", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<QuantityAdjustment></QuantityAdjustment>", string.Empty);
            requestXmlDoc.InnerXml = requestXmlDoc.InnerXml.Replace("<ValueAdjustment></ValueAdjustment>", string.Empty);

            #endregion
            
            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InventoryAdjustmentModRq/InventoryAdjustmentMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventoryAdjustmentModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InventoryAdjustmentModRs/InventoryAdjustmentRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInventoryAdjustment(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }
    /// <summary>
    /// This method is used for getting existing Inventory Adjustment refnumber, If not exists then it return null.
    /// </summary>
    public class InventoryAdjustmentEntryCollection : Collection<InventoryAdjustmentEntry>
    {
        public InventoryAdjustmentEntry FindInventoryAdjustmentEntry(string refNumber)
        {
            foreach (InventoryAdjustmentEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
    }

    [XmlRootAttribute("InventoryAdjustmentLine", Namespace = "", IsNullable = false)]
    public class InventoryAdjustmentLineAdd
    {
        private string m_TxnLineID;

        private ItemRef m_ItemRef;
        //private QuantityAdjustment m_QuantityDifference;

        //Changesssss
        private QuickBookEntities.QuantityAdjustment m_QuantityAdjustment;
        private ValueAdjustment m_ValueAdjustment;
        private SerialNumberAdjustment m_SerialNumberAdjustment;
        private LotNumberAdjustment m_LotNumberAdjustment;

        public InventoryAdjustmentLineAdd()
        {

        }
        public string TxnLineID
        {
            get { return this.m_TxnLineID; }
            set { this.m_TxnLineID = value; }
        }
        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }


        public QuickBookEntities.QuantityAdjustment QuantityAdjustment
        {
            get { return this.m_QuantityAdjustment; }
            set { this.m_QuantityAdjustment = value; }
        }

        public ValueAdjustment ValueAdjustment
        {
            get { return this.m_ValueAdjustment; }
            set { this.m_ValueAdjustment = value; }
        }

        public SerialNumberAdjustment SerialNumberAdjustment
        {
            get { return this.m_SerialNumberAdjustment; }
            set { this.m_SerialNumberAdjustment = value; }
        }

        public LotNumberAdjustment LotNumberAdjustment
        {
            get { return this.m_LotNumberAdjustment; }
            set { this.m_LotNumberAdjustment = value; }
        }

    }


}





