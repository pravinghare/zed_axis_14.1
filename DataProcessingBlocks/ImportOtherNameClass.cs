﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    public class ImportOtherNameClass
    {
        private static ImportOtherNameClass m_ImportOtherNameClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor
        public ImportOtherNameClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import OtherName class
        /// </summary>
        /// <returns></returns>
        public static ImportOtherNameClass GetInstance()
        {
            if (m_ImportOtherNameClass == null)
                m_ImportOtherNameClass = new ImportOtherNameClass();
            return m_ImportOtherNameClass;
        }

        /// <summary>
        /// This method is used for validating import data and create othername and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>OtherName QuickBooks collection </returns>
        public DataProcessingBlocks.OtherNameQBEntryCollection ImportOtherNameData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of OtherName Entry collections.
            DataProcessingBlocks.OtherNameQBEntryCollection coll = new OtherNameQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                 
                    string datevalue = string.Empty;

                    //OtherName Validation
                    DataProcessingBlocks.OtherNameQBEntry otherName = new OtherNameQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Name = dr["Name"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                otherName.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {
                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                otherName.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    otherName.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        otherName.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            otherName.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            otherName.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        otherName.IsActive = dr["IsActive"].ToString();
                                    }
                                }
                            }
                        }
                        #endregion
                    }
                   
                    if (dt.Columns.Contains("CompanyName"))
                    {
                        #region Validations of CompanyName
                        if (dr["CompanyName"].ToString() != string.Empty)
                        {
                            if (dr["CompanyName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CompanyName (" + dr["CompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.CompanyName = dr["CompanyName"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.CompanyName = dr["CompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.CompanyName = dr["CompanyName"].ToString();
                                }
                            }
                            else
                            {
                                otherName.CompanyName = dr["CompanyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Salutation"))
                    {
                        #region Validations of Salutation
                        if (dr["Salutation"].ToString() != string.Empty)
                        {
                            if (dr["Salutation"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Salutation (" + dr["Salutation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Salutation = dr["Salutation"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Salutation = dr["Salutation"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.Salutation = dr["Salutation"].ToString();
                                }
                            }
                            else
                            {
                                otherName.Salutation = dr["Salutation"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FirstName"))
                    {
                        #region Validations of FirstName
                        if (dr["FirstName"].ToString() != string.Empty)
                        {
                            if (dr["FirstName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.FirstName = dr["FirstName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            else
                            {
                                otherName.FirstName = dr["FirstName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MiddleName"))
                    {
                        #region Validations of MiddleName
                        if (dr["MiddleName"].ToString() != string.Empty)
                        {
                            if (dr["MiddleName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.MiddleName = dr["MiddleName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            else
                            {
                                otherName.MiddleName = dr["MiddleName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LastName"))
                    {
                        #region Validations of LastName
                        if (dr["LastName"].ToString() != string.Empty)
                        {
                            if (dr["LastName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LastName (" + dr["LastName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.LastName = dr["LastName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.LastName = dr["LastName"].ToString();
                                }
                            }
                            else
                            {
                                otherName.LastName = dr["LastName"].ToString();
                            }
                        }
                        #endregion
                    }

                    QuickBookEntities.OtherNameAddress otherNameAddressItem = new OtherNameAddress();
                    if (dt.Columns.Contains("OtherNameAddr1"))
                    {
                        #region Validations of OtherNameAddr1
                        if (dr["OtherNameAddr1"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Add1 (" + dr["OtherNameAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Addr1 = dr["OtherNameAddr1"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Addr1 = dr["OtherNameAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    otherNameAddressItem.Addr1 = dr["OtherNameAddr1"].ToString();
                                }
                            }
                            else
                            {
                                otherNameAddressItem.Addr1 = dr["OtherNameAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameAddr2"))
                    {
                        #region Validations of OtherNameAddr2
                        if (dr["OtherNameAddr2"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Add2 (" + dr["OtherNameAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Addr2 = dr["OtherNameAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Addr2 = dr["OtherNameAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    otherNameAddressItem.Addr2 = dr["OtherNameAddr2"].ToString();
                                }
                            }
                            else
                            {
                                otherNameAddressItem.Addr2 = dr["OtherNameAddr2"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameAddr3"))
                    {
                        #region Validations of OtherNameAddr3
                        if (dr["OtherNameAddr3"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameAddr3"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Add3 (" + dr["OtherNameAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Addr3 = dr["OtherNameAddr3"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Addr3 = dr["OtherNameAddr3"].ToString();
                                    }
                                }
                                else
                                    otherNameAddressItem.Addr3 = dr["OtherNameAddr3"].ToString();

                            }
                            else
                            {
                                otherNameAddressItem.Addr3 = dr["OtherNameAddr3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameAddr4"))
                    {
                        #region Validations of OtherNameAddr4
                        if (dr["OtherNameAddr4"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameAddr4"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Add4 (" + dr["OtherNameAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Addr4 = dr["OtherNameAddr4"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Addr4 = dr["OtherNameAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    otherNameAddressItem.Addr4 = dr["OtherNameAddr4"].ToString();
                                }
                            }
                            else
                            {
                                otherNameAddressItem.Addr4 = dr["OtherNameAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameAddr5"))
                    {
                        #region Validations of OtherNameAddr5
                        if (dr["OtherNameAddr5"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameAddr5"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Add5 (" + dr["OtherNameAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Addr5 = dr["OtherNameAddr5"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Addr5 = dr["OtherNameAddr5"].ToString();
                                    }
                                }
                                else
                                    otherNameAddressItem.Addr5 = dr["OtherNameAddr5"].ToString();
                            }
                            else
                            {
                                otherNameAddressItem.Addr5 = dr["OtherNameAddr5"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameCity"))
                    {
                        #region Validations of OtherNameCity
                        if (dr["OtherNameCity"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name City (" + dr["OtherNameCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.City = dr["OtherNameCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.City = dr["OtherNameCity"].ToString();
                                    }
                                }
                                else
                                    otherNameAddressItem.City = dr["OtherNameCity"].ToString();
                            }
                            else
                            {
                                otherNameAddressItem.City = dr["OtherNameCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameState"))
                    {
                        #region Validations of OtherNameState
                        if (dr["OtherNameState"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name State (" + dr["OtherNameState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.State = dr["OtherNameState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.State = dr["OtherNameState"].ToString();
                                    }
                                }
                                else
                                {
                                    otherNameAddressItem.State = dr["OtherNameState"].ToString();
                                }
                            }
                            else
                            {
                                otherNameAddressItem.State = dr["OtherNameState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNamePostalCode"))
                    {
                        #region Validations of OtherNamePostalCode
                        if (dr["OtherNamePostalCode"].ToString() != string.Empty)
                        {
                            if (dr["OtherNamePostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Postal Code (" + dr["OtherNamePostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.PostalCode = dr["OtherNamePostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.PostalCode = dr["OtherNamePostalCode"].ToString();
                                    }
                                }
                                else
                                    otherNameAddressItem.PostalCode = dr["OtherNamePostalCode"].ToString();
                            }
                            else
                            {
                                otherNameAddressItem.PostalCode = dr["OtherNamePostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameCountry"))
                    {
                        #region Validations of OtherNameCountry
                        if (dr["OtherNameCountry"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Country (" + dr["OtherNameCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Country = dr["OtherNameCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Country = dr["OtherNameCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    otherNameAddressItem.Country = dr["OtherNameCountry"].ToString();
                                }
                            }
                            else
                            {
                                otherNameAddressItem.Country = dr["OtherNameCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OtherNameNote"))
                    {
                        #region Validations of OtherNameNote
                        if (dr["OtherNameNote"].ToString() != string.Empty)
                        {
                            if (dr["OtherNameNote"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Other Name Note (" + dr["OtherNameNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherNameAddressItem.Note = dr["OtherNameNote"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherNameAddressItem.Note = dr["OtherNameNote"].ToString();
                                    }
                                }
                                else
                                    otherNameAddressItem.Note = dr["OtherNameNote"].ToString();
                            }
                            else
                            {
                                otherNameAddressItem.Note = dr["OtherNameNote"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (otherNameAddressItem.Addr1 != null || otherNameAddressItem.Addr2 != null || otherNameAddressItem.Addr3 != null || otherNameAddressItem.Addr4 != null || otherNameAddressItem.Addr5 != null
                                || otherNameAddressItem.City != null || otherNameAddressItem.Country != null || otherNameAddressItem.PostalCode != null || otherNameAddressItem.State != null || otherNameAddressItem.Note != null)
                        otherName.OtherNameAddress.Add(otherNameAddressItem);


                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Phone = dr["Phone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                otherName.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AltPhone"))
                    {
                        #region Validations of Alt Phone
                        if (dr["AltPhone"].ToString() != string.Empty)
                        {
                            if (dr["AltPhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltPhone (" + dr["AltPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.AltPhone = dr["AltPhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.AltPhone = dr["AltPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.AltPhone = dr["AltPhone"].ToString();
                                }
                            }
                            else
                            {
                                otherName.AltPhone = dr["AltPhone"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                otherName.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 1023)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Email = dr["Email"].ToString().Substring(0, 1023);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                otherName.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Contact"))
                    {
                        #region Validations of Contact
                        if (dr["Contact"].ToString() != string.Empty)
                        {
                            if (dr["Contact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Contact (" + dr["Contact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Contact = dr["Contact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Contact = dr["Contact"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.Contact = dr["Contact"].ToString();
                                }
                            }
                            else
                            {
                                otherName.Contact = dr["Contact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AltContact"))
                    {
                        #region Validations of AltContact
                        if (dr["AltContact"].ToString() != string.Empty)
                        {
                            if (dr["AltContact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltContact (" + dr["AltContact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.AltContact = dr["AltContact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.AltContact = dr["AltContact"].ToString();
                                    }
                                }
                                else
                                {
                                    otherName.AltContact = dr["AltContact"].ToString();
                                }
                            }
                            else
                            {
                                otherName.AltContact = dr["AltContact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AccountNumber"))
                    {
                        #region Validations of Account Number
                        if (dr["AccountNumber"].ToString() != string.Empty)
                        {
                            if (dr["AccountNumber"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Account Number (" + dr["AccountNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.AccountNumber = dr["AccountNumber"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.AccountNumber = dr["AccountNumber"].ToString();
                                    }
                                }
                                else
                                    otherName.AccountNumber = dr["AccountNumber"].ToString();
                            }
                            else
                            {
                                otherName.AccountNumber = dr["AccountNumber"].ToString();
                            }
                        }
                        #endregion
                    }
                   
                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["JobDNotesesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.Notes = dr["Notes"].ToString().Substring(0, 4095);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                    otherName.Notes = dr["Notes"].ToString();
                            }
                            else
                            {
                                otherName.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("ExternalGUID"))
                    {
                        #region Validations of ExternalGUID
                        if (dr["ExternalGUID"].ToString() != string.Empty)
                        {
                            if (dr["ExternalGUID"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ExternalGUID (" + dr["ExternalGUID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        otherName.ExternalGUID = dr["ExternalGUID"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        otherName.ExternalGUID = dr["ExternalGUID"].ToString();
                                    }
                                }
                                else
                                    otherName.ExternalGUID = dr["NotExternalGUIDes"].ToString();
                            }
                            else
                            {
                                otherName.ExternalGUID = dr["ExternalGUID"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    coll.Add(otherName);
                }
                else
                {
                    return null;
                }
            }
            #endregion


            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }


            #endregion

            #endregion

            return coll;
        }

    }
}
