﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using System.Linq;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Api;
using DataProcessingBlocks.XeroConnection;
using Newtonsoft.Json;
using Xero.NetStandard.OAuth2.Models;
using System.Threading.Tasks;


namespace DataProcessingBlocks
{
    [XmlRootAttribute("XeroInvoiceEntry", Namespace = "", IsNullable = false)]
    public class XeroInvoiceEntry
    {
        public bool itemflag = false;

        #region Constructor
        public XeroInvoiceEntry()
        {
        }
        #endregion

        #region Public Properties

        public Invoices invoices
        {
            get;
            set;
        }
        public Invoice invoice
        {
            get;
            set;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Function is to check whether Given Invoice is present in Xero.
        /// </summary>
        /// <param name="invoiceno">Invoice Number given by user.</param>
        /// <returns></returns>
        public async Task<bool> CheckAndGetRefNoExistsInXeroAsync(string invoiceno)
        {
            try
            {
                Invoices invoiceTransactionsResponse = new Invoices();
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                string query = "InvoiceNumber == \"" + invoiceno + "\"";
                var response =  res.GetInvoicesAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, null, query, null, null, null, null, null, null, null, null, null);
                response.Wait();
                invoiceTransactionsResponse = response.Result;
                if (invoiceTransactionsResponse._Invoices.Count > 0)
                {
                    TransactionImporter.TransactionImporter.refnoflag = true;
                    CommonUtilities.GetInstance().existTxnId = (invoiceTransactionsResponse._Invoices[0].InvoiceID ?? Guid.Empty);
                }
                else
                {
                    CommonUtilities.GetInstance().existTxnId = Guid.Empty;
                    TransactionImporter.TransactionImporter.refnoflag = false;
                }
            }
            catch (Exception ex)
            {

            }

            return TransactionImporter.TransactionImporter.refnoflag;
        }

        /// <summary>
        /// Function used to inset invoice in xero.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <returns></returns>
        public async Task<bool> ExportToXeroAsync(string statusMessage, string requestText, int rowcount, string txnid)
        {
            string resp = string.Empty;
            string responseFile = string.Empty;
            Invoices invoiceResponse = new Invoices();
            bool reponseFlag = false;

            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            //try
            //{
            //    DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.XeroInvoiceEntry>.Save(this, fileName);
            //}
            //catch
            //{
            //    statusMessage += "\n ";
            //    statusMessage += "Mapping contains invalid data.Application can not send data to Xero.";
            //    return false;
            //}

            //System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            //requestXmlDoc.Load(fileName);
            //System.IO.File.Delete(fileName);


            Item itemcode = new Item();
            string lineitemcode = string.Empty;
            string itempresentinxero = string.Empty;

            try
            {
                if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.InvoiceCustomer)
                {
                    invoice.Type = Invoice.TypeEnum.ACCREC;
                    CommonUtilities.GetInstance().Type = "Account Receivable";
                }
                else
                {
                    invoice.Type = Invoice.TypeEnum.ACCPAY;
                    CommonUtilities.GetInstance().Type = "Account Payable";
                }


                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                var items =  res.GetItemsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, null, null, null, null);
                items.Wait();
                var itemlist = items.Result;
                int itemExistCount = 0;

                //tocheck
                if (itemlist._Items.Count > 0)
                {
                    foreach (LineItem lineitemnode in invoice.LineItems)
                    {
                        lineitemcode = lineitemnode.ItemCode;

                        foreach (Item itemname in itemlist._Items)
                        {
                            itempresentinxero = itemname.Code;
                            if (lineitemcode != null)
                            {
                                if (lineitemcode.Equals(itempresentinxero))
                                {
                                    itemflag = false;
                                    itemExistCount = 1;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (itemExistCount != 1)
                {
                    itemcode.Code = lineitemcode;
                    var itemResponse = res.CreateItemAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, itemcode);
                    itemResponse.Wait();
                }

                Invoices xeroInvoices= new Invoices();
                xeroInvoices._Invoices = new List<Invoice>();
                xeroInvoices._Invoices.Add(invoice);

                //requestText = ModelSerializer.Serialize(invoice);
                //requestXmlDoc.LoadXml(requestText);
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);

                if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == false)
                {
                    var response = res.CreateInvoiceAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, invoice);
                    response.Wait();
                    invoiceResponse = response.Result;
                    if (invoiceResponse._Invoices.Count > 0)
                    {
                        txnid = invoiceResponse._Invoices[0].InvoiceID.ToString();
                        reponseFlag = true;
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += invoiceResponse._Invoices[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
                else if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == true)
                {
                    if (TransactionImporter.TransactionImporter.refnoflag == true)
                    {
                        var response =  res.UpdateInvoiceAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, CommonUtilities.GetInstance().existTxnId, xeroInvoices);
                        response.Wait();
                        invoiceResponse = response.Result;
                        reponseFlag = true;
                    }
                    else
                    {
                        var response =  res.CreateInvoiceAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, invoice);
                        response.Wait();
                        invoiceResponse = response.Result;
                        reponseFlag = true;
                    }
                    if (invoiceResponse._Invoices.Count > 0)
                    {
                        txnid = invoiceResponse._Invoices[0].InvoiceID.ToString();
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += invoiceResponse._Invoices[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                // CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
            }

            //TransactionImporter.TransactionImporter.testFlag = true;

            CommonUtilities.GetInstance().xeroMessage = statusMessage;
            CommonUtilities.GetInstance().xeroTxnId = txnid;
            return reponseFlag;
        }
        #endregion
    }

    public enum InvoiceStatus
    {
        DRAFT,
        SUBMITTED,
        DELETED,
        AUTHORISED,
        PAID,
        VOIDED,
    }
}
