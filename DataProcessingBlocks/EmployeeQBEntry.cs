using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("EmployeeQBEntry", Namespace = "", IsNullable = false)]
    public class EmployeeQBEntry
    {
        #region Private Member Variable

        private string m_IsActive;
        private string m_Salutation;
        private string m_FirstName;
        private string m_MiddleName;
        private string m_LastName;
        //P Axis 13.1 : issue 611
        private SupervisorRef m_SupervisorRef;
        private string m_Department;
        private string m_Description;
        private string m_TargetBonus;

        private Collection<EmployeeAddress> m_EmployeeAddress = new Collection<EmployeeAddress>();
        private string m_PrintAs;
        private string m_Phone;
        private string m_Mobile; 
        private string m_Pager;
        private string m_PagerPIN;
        private string m_AltPhone; 
        private string m_Fax; 
        private string m_SSN; 
        private string m_Email;
        //P Axis 13.1 : issue 611
        private Collection<EmergencyContacts> m_EmergencyContacts = new Collection<EmergencyContacts>();

        private string m_EmployeeType;
        //P Axis 13.1 : issue 611
        private string m_PartOrFullTime;
        private string m_Exempt;
        private string m_KeyEmployee;

        private string m_Gender; 
        private string m_HiredDate;
        //P Axis 13.1 : issue 611
        private string m_OriginalHireDate;
        private string m_AdjustedServiceDate;

        private string m_ReleasedDate; 
        private string m_BirthDate;
        //P Axis 13.1 : issue 611
        private string m_USCitizen;
        private string m_Ethnicity;
        private string m_Disabled;
        private string m_DisabilityDesc;
        private string m_OnFile;
        private string m_WorkAuthExpireDate;
        private string m_USVeteran;
        private string m_MilitaryStatus;

        private string m_AccountNumber;
        private string m_Notes;
        private BillingRateRef m_BillingRateRef;
        private Collection<EmployeePayrollInfo> m_EmployeePayrollInfo = new Collection<EmployeePayrollInfo>();

        #endregion

        #region Constructor

        public EmployeeQBEntry()
        {
        }

        #endregion

        #region Public Properties

        public string IsActive
        {
            get { return this.m_IsActive; }
            set { this.m_IsActive = value; }
        }

        public string Salutation
        {
            get { return this.m_Salutation; }
            set { this.m_Salutation = value; }
        }

        public string FirstName
        {
            get { return this.m_FirstName; }
            set { this.m_FirstName = value; }
        }

        public string MiddleName
        {
            get { return this.m_MiddleName; }
            set { this.m_MiddleName = value; }
        }

        public string LastName
        {
            get { return this.m_LastName; }
            set { this.m_LastName = value; }
        }
        //P Axis 13.1 : issue 611
        public SupervisorRef SupervisorRef
        {
            get { return m_SupervisorRef; }
            set { m_SupervisorRef = value; }
        }
        public string Department
        {
            get { return this.m_Department; }
            set { this.m_Department = value; }
        }
        public string Description
        {
            get { return this.m_Description; }
            set { this.m_Description = value; }
        }
        public string TargetBonus
        {
            get { return this.m_TargetBonus; }
            set { this.m_TargetBonus = value; }
        }



        [XmlArray("EmployeeAddressREM")]
        public Collection<EmployeeAddress> EmployeeAddress
        {
            get { return this.m_EmployeeAddress; }
            set { this.m_EmployeeAddress = value; }
        }
        public string PrintAs
        {
            get { return this.m_PrintAs; }
            set { this.m_PrintAs = value; }
        }

        public string Phone
        {
            get { return this.m_Phone; }
            set { this.m_Phone = value; }
        }

        public string Mobile
        {
            get { return this.m_Mobile; }
            set { this.m_Mobile = value; }
        }
        public string Pager
        {
            get { return this.m_Pager; }
            set { this.m_Pager = value; }
        }

        public string PagerPIN
        {
            get { return this.m_PagerPIN; }
            set { this.m_PagerPIN = value; }
        }

        public string AltPhone
        {
            get { return this.m_AltPhone; }
            set { this.m_AltPhone = value; }
        }
        public string Fax
        {
            get { return this.m_Fax; }
            set { this.m_Fax = value; }
        }

        public string SSN
        {
            get { return this.m_SSN; }
            set { this.m_SSN = value; }
        }

        public string Email
        {
            get { return this.m_Email; }
            set { this.m_Email = value; }
        }
        //P Axis 13.1 : issue 611
        [XmlArray("EmergencyContactsREM")]
        public Collection<EmergencyContacts> EmergencyContacts
        {
            get { return this.m_EmergencyContacts; }
            set { this.m_EmergencyContacts = value; }
        }

        public string EmployeeType
        {
            get { return this.m_EmployeeType; }
            set { this.m_EmployeeType = value; }
        }
        //P Axis 13.1 : issue 611
        public string PartOrFullTime
        {
            get { return this.m_PartOrFullTime; }
            set { this.m_PartOrFullTime = value; }
        }
        public string Exempt
        {
            get { return this.m_Exempt; }
            set { this.m_Exempt = value; }
        }
        public string KeyEmployee
        {
            get { return this.m_KeyEmployee; }
            set { this.m_KeyEmployee = value; }
        }

        public string Gender
        {
            get { return this.m_Gender; }
            set { this.m_Gender = value; }
        }

        public string HiredDate
        {
            get { return this.m_HiredDate; }
            set { this.m_HiredDate = value; }
        }
        //P Axis 13.1 : issue 611
        public string OriginalHireDate
        {
            get { return this.m_OriginalHireDate; }
            set { this.m_OriginalHireDate = value; }
        }
        public string AdjustedServiceDate
        {
            get { return this.m_AdjustedServiceDate; }
            set { this.m_AdjustedServiceDate = value; }
        }

        public string ReleasedDate
        {
            get { return this.m_ReleasedDate; }
            set { this.m_ReleasedDate = value; }
        }

        public string BirthDate
        {
            get { return this.m_BirthDate; }
            set { this.m_BirthDate = value; }
        }
        //P Axis 13.1 : issue 611
        public string USCitizen
        {
            get { return this.m_USCitizen; }
            set { this.m_USCitizen = value; }
        }
        public string Ethnicity
        {
            get { return this.m_Ethnicity; }
            set { this.m_Ethnicity = value; }
        }
        public string Disabled
        {
            get { return this.m_Disabled; }
            set { this.m_Disabled = value; }
        }
        public string DisabilityDesc
        {
            get { return this.m_DisabilityDesc; }
            set { this.m_DisabilityDesc = value; }
        }
        public string OnFile
        {
            get { return this.m_OnFile; }
            set { this.m_OnFile = value; }
        }
        public string WorkAuthExpireDate
        {
            get { return this.m_WorkAuthExpireDate; }
            set { this.m_WorkAuthExpireDate = value; }
        }
        public string USVeteran
        {
            get { return this.m_USVeteran; }
            set { this.m_USVeteran = value; }
        }
        public string MilitaryStatus
        {
            get { return this.m_MilitaryStatus; }
            set { this.m_MilitaryStatus = value; }
        }

        public string AccountNumber
        {
            get { return this.m_AccountNumber; }
            set { this.m_AccountNumber = value; }
        }

        public string Notes
        {
            get { return this.m_Notes; }
            set { this.m_Notes = value; }
        }

        public BillingRateRef BillingRateRef
        {
            get { return this.m_BillingRateRef; }
            set { this.m_BillingRateRef = value; }
        }

        [XmlArray("EmployeePayrollInfoREM")]
        public Collection<EmployeePayrollInfo> EmployeePayrollInfo
        {
            get { return this.m_EmployeePayrollInfo; }
            set { this.m_EmployeePayrollInfo = value; }
        }

        
        #endregion

        #region Public Methods
/// <summary>
        /// Creating request file for exporting data to quickbook.
/// </summary>
/// <param name="statusMessage"></param>
/// <param name="requestText"></param>
/// <param name="rowcount"></param>
/// <param name="AppName"></param>
/// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.EmployeeQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement EmployeeAddRq = requestXmlDoc.CreateElement("EmployeeAddRq");
            inner.AppendChild(EmployeeAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement EmployeeAdd = requestXmlDoc.CreateElement("EmployeeAdd");

            EmployeeAddRq.AppendChild(EmployeeAdd);

            requestXML = requestXML.Replace("<EmployeeAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<EmployeeAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</EmployeeAddressREM>", string.Empty);


            requestXML = requestXML.Replace("<EmployeePayrollInfoREM />", string.Empty);
            requestXML = requestXML.Replace("<EmployeePayrollInfoREM>", string.Empty);
            requestXML = requestXML.Replace("</EmployeePayrollInfoREM>", string.Empty);


            requestXML = requestXML.Replace("<VacationHoursREM />", string.Empty);
            requestXML = requestXML.Replace("<VacationHoursREM>", string.Empty);
            requestXML = requestXML.Replace("</VacationHoursREM>", string.Empty);


            requestXML = requestXML.Replace("<SickHoursREM />", string.Empty);
            requestXML = requestXML.Replace("<SickHoursREM>", string.Empty);
            requestXML = requestXML.Replace("</SickHoursREM>", string.Empty);


            requestXML = requestXML.Replace("<EarningsREM />", string.Empty);
            requestXML = requestXML.Replace("<EarningsREM>", string.Empty);
            requestXML = requestXML.Replace("</EarningsREM>", string.Empty);

            //P Axis 13.1 : issue 611
            requestXML = requestXML.Replace("<EmergencyContactsREM />", string.Empty);
            requestXML = requestXML.Replace("<EmergencyContactsREM>", string.Empty);
            requestXML = requestXML.Replace("</EmergencyContactsREM>", string.Empty);

            requestXML = requestXML.Replace("<PrimaryContactREM />", string.Empty);
            requestXML = requestXML.Replace("<PrimaryContactREM>", string.Empty);
            requestXML = requestXML.Replace("</PrimaryContactREM>", string.Empty);

            requestXML = requestXML.Replace("<SecondaryContactREM />", string.Empty);
            requestXML = requestXML.Replace("<SecondaryContactREM>", string.Empty);
            requestXML = requestXML.Replace("</SecondaryContactREM>", string.Empty);

            requestXML = requestXML.Replace("<EmergencyContactsMod>", "<EmergencyContacts>");
            requestXML = requestXML.Replace("</EmergencyContactsMod>", "</EmergencyContacts>");

            EmployeeAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
           
            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/EmployeeAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/EmployeeAddRs/EmployeeRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofEmployee(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating EmployeeQBEntry information
        /// of existing EmployeeQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateEmployeeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.EmployeeQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement EmployeeQBEntryModRq = requestXmlDoc.CreateElement("EmployeeModRq");
            inner.AppendChild(EmployeeQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement EmployeeMod = requestXmlDoc.CreateElement("EmployeeMod");
            EmployeeQBEntryModRq.AppendChild(EmployeeMod);



            requestXML = requestXML.Replace("<EmployeeAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<EmployeeAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</EmployeeAddressREM>", string.Empty);


            requestXML = requestXML.Replace("<EmployeePayrollInfoREM />", string.Empty);
            requestXML = requestXML.Replace("<EmployeePayrollInfoREM>", string.Empty);
            requestXML = requestXML.Replace("</EmployeePayrollInfoREM>", string.Empty);


            requestXML = requestXML.Replace("<VacationHoursREM />", string.Empty);
            requestXML = requestXML.Replace("<VacationHoursREM>", string.Empty);
            requestXML = requestXML.Replace("</VacationHoursREM>", string.Empty);


            requestXML = requestXML.Replace("<SickHoursREM />", string.Empty);
            requestXML = requestXML.Replace("<SickHoursREM>", string.Empty);
            requestXML = requestXML.Replace("</SickHoursREM>", string.Empty);


            requestXML = requestXML.Replace("<EarningsREM />", string.Empty);
            requestXML = requestXML.Replace("<EarningsREM>", string.Empty);
            requestXML = requestXML.Replace("</EarningsREM>", string.Empty);


            requestXML = requestXML.Replace("<EmployeePayrollInfo>", "<EmployeePayrollInfoMod>");
            requestXML = requestXML.Replace("</EmployeePayrollInfo>", "</EmployeePayrollInfoMod>");
            requestXML = requestXML.Replace("<EmployeePayrollInfo />", string.Empty);
            requestXML = requestXML.Replace("<EmployeePayrollInfoMod />", string.Empty);



            requestXML = requestXML.Replace("<EmployeePayrollInfoMod></EmployeePayrollInfoMod>", string.Empty);

            //P Axis 13.1 : issue 611
            requestXML = requestXML.Replace("<EmergencyContactsREM />", string.Empty);
            requestXML = requestXML.Replace("<EmergencyContactsREM>", string.Empty);
            requestXML = requestXML.Replace("</EmergencyContactsREM>", string.Empty);

            requestXML = requestXML.Replace("<PrimaryContactREM />", string.Empty);
            requestXML = requestXML.Replace("<PrimaryContactREM>", string.Empty);
            requestXML = requestXML.Replace("</PrimaryContactREM>", string.Empty);

            requestXML = requestXML.Replace("<SecondaryContactREM />", string.Empty);
            requestXML = requestXML.Replace("<SecondaryContactREM>", string.Empty);
            requestXML = requestXML.Replace("</SecondaryContactREM>", string.Empty);

            requestXML = requestXML.Replace("<EmergencyContactsMod>", "<EmergencyContacts>");
            requestXML = requestXML.Replace("</EmergencyContactsMod>", "</EmergencyContacts>");

            EmployeeMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;
            

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EmployeeModRq/EmployeeMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EmployeeModRq/EmployeeMod").InsertBefore(ListID, firstChild).InnerText = listID;
            
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");            
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EmployeeModRq/EmployeeMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/EmployeeModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/EmployeeModRs/EmployeeRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofEmployee(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class EmployeeQBEntryCollection : Collection<EmployeeQBEntry>
    {

    }

    [XmlRootAttribute("EmployeePayrollInfo", Namespace = "", IsNullable = false)]
    public class EmployeePayrollInfo
    {
        #region Private Members

        private string m_PayPeriod;
        private ClassRef m_ClassRef;
        private string m_ClearEarning;
        private Collection<Earnings> m_Earnings = new Collection<Earnings>();
        private string m_IsUsingTimeDataToCreatePaychecks;
        private string m_UseTimeDataToCreatePaychecks;

        private Collection<SickHours> m_SickHours = new Collection<SickHours>();
        private Collection<VacationHours> m_VacationHours = new Collection<VacationHours>();

        #endregion

        public EmployeePayrollInfo()
        {
        }

        #region Public Properties

        public string PayPeriod
        {
            get { return this.m_PayPeriod; }
            set { this.m_PayPeriod = value; }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public string ClearEarnings
        {
            get { return this.m_ClearEarning; }
            set { this.m_ClearEarning = value; }
        }

        [XmlArray("EarningsREM")]
        public Collection<Earnings> Earnings
        {
            get { return this.m_Earnings; }
            set { this.m_Earnings = value; }
        }

        public string IsUsingTimeDataToCreatePaychecks
        {
            get { return this.m_IsUsingTimeDataToCreatePaychecks; }
            set { this.m_IsUsingTimeDataToCreatePaychecks = value; }
        }

        public string UseTimeDataToCreatePaychecks
        {
            get { return this.m_UseTimeDataToCreatePaychecks; }
            set { this.m_UseTimeDataToCreatePaychecks = value; }
        }

        [XmlArray("SickHoursREM")]
        public Collection<SickHours> SickHours
        {
            get { return this.m_SickHours; }
            set { this.m_SickHours = value; }
        }

        [XmlArray("VacationHoursREM")]
        public Collection<VacationHours> VacationHours
        {
            get { return this.m_VacationHours; }
            set { this.m_VacationHours = value; }
        }


        #endregion
    }


    [XmlRootAttribute("SickHours", Namespace = "", IsNullable = false)]
    public class SickHours
    {
        #region Private Members
        private string m_HoursAvailable;
        private string m_AccrualPeriod;
        private string m_HoursAccrued;
        private string m_MaximumHours;
        private string m_IsResettingHoursEachNewYear;
        private string m_HoursUsed;
        private string m_AccrualStartDate;

        #endregion

        #region Constructor

        public SickHours()
        {
        }
        #endregion

        #region Public Properties
        public string HoursAvailable
        {
            get { return this.m_HoursAvailable; }
            set { this.m_HoursAvailable = value; }
        }
        
        public string AccrualPeriod
        {
            get { return this.m_AccrualPeriod; }
            set { this.m_AccrualPeriod = value; }
        }
        
        public string HoursAccrued
        {
            get { return this.m_HoursAccrued; }
            set { this.m_HoursAccrued = value; }
        }
        
        public string MaximumHours
        {
            get { return this.m_MaximumHours; }
            set { this.m_MaximumHours = value; }
        }
        
        public string IsResettingHoursEachNewYear
        {
            get { return this.m_IsResettingHoursEachNewYear; }
            set { this.m_IsResettingHoursEachNewYear = value; }
        }
        
        public string HoursUsed
        {
            get { return this.m_HoursUsed; }
            set { this.m_HoursUsed = value; }
        }
        
        public string AccrualStartDate
        {
            get { return this.m_AccrualStartDate; }
            set { this.m_AccrualStartDate = value; }
        }

        #endregion

    }

    [XmlRootAttribute("VacationHours", Namespace = "", IsNullable = false)]
    public class VacationHours
    {
        #region Private Members
        private string m_HoursAvailable;
        private string m_AccrualPeriod;
        private string m_HoursAccrued;
        private string m_MaximumHours;
        private string m_IsResettingHoursEachNewYear;
        private string m_HoursUsed;
        private string m_AccrualStartDate;
        #endregion

        #region Constructor
        public VacationHours()
        {
        }

        #endregion

        #region Public Properties
        public string HoursAvailable
        {
            get { return this.m_HoursAvailable; }
            set { this.m_HoursAvailable = value; }
        }

        public string AccrualPeriod
        {
            get { return this.m_AccrualPeriod; }
            set { this.m_AccrualPeriod = value; }
        }

        public string HoursAccrued
        {
            get { return this.m_HoursAccrued; }
            set { this.m_HoursAccrued = value; }
        }

        public string MaximumHours
        {
            get { return this.m_MaximumHours; }
            set { this.m_MaximumHours = value; }
        }

        public string IsResettingHoursEachNewYear
        {
            get { return this.m_IsResettingHoursEachNewYear; }
            set { this.m_IsResettingHoursEachNewYear = value; }
        }

        public string HoursUsed
        {
            get { return this.m_HoursUsed; }
            set { this.m_HoursUsed = value; }
        }

        public string AccrualStartDate
        {
            get { return this.m_AccrualStartDate; }
            set { this.m_AccrualStartDate = value; }
        }
        #endregion
    }


    [XmlRootAttribute("Earnings", Namespace = "", IsNullable = false)]
    public class Earnings
    {
        #region Private Members

        private PayrollItemWageRef m_PayrollItemWageRef;
        private string m_Rate;
        private string m_RatePercent;

        #endregion

        #region Constructor

        public Earnings()
        {
        }

        #endregion


        #region Public Properties

        public PayrollItemWageRef PayrollItemWageRef
        {
            get { return this.m_PayrollItemWageRef; }
            set { this.m_PayrollItemWageRef = value; }
        }

        public string Rate
        {
            get { return this.m_Rate; }
            set { this.m_Rate = value; }
        }

        public string RatePercent
        {
            get { return this.m_RatePercent; }
            set { this.m_RatePercent = value; }
        }

        #endregion


    }


    //P Axis 13.1 : issue 611
    [XmlRootAttribute("EmergencyContacts", Namespace = "", IsNullable = false)]
    public class EmergencyContacts
    {
        #region Private Members
        private Collection<PrimaryContact> m_PrimaryContact = new Collection<PrimaryContact>();
        private Collection<SecondaryContact> m_SecondaryContact = new Collection<SecondaryContact>();
        #endregion
        public EmergencyContacts()
        {
        }

        [XmlArray("PrimaryContactREM")]
        public Collection<PrimaryContact> PrimaryContact
        {
            get { return this.m_PrimaryContact; }
            set { this.m_PrimaryContact = value; }
        }

        [XmlArray("SecondaryContactREM")]
        public Collection<SecondaryContact> SecondaryContact
        {
            get { return this.m_SecondaryContact; }
            set { this.m_SecondaryContact = value; }
        }
    }

    [XmlRootAttribute("PrimaryContact", Namespace = "", IsNullable = false)]
    public class PrimaryContact
    {
        #region Private Members
        private string m_ContactName;
        private string m_ContactValue;
        private string m_Relation;
        #endregion

        #region Constructor

        public PrimaryContact()
        {
        }
        #endregion

        #region Public Properties
        public string ContactName
        {
            get { return this.m_ContactName; }
            set { this.m_ContactName = value; }
        }

        public string ContactValue
        {
            get { return this.m_ContactValue; }
            set { this.m_ContactValue = value; }
        }

        public string Relation
        {
            get { return this.m_Relation; }
            set { this.m_Relation = value; }
        }
        #endregion
    }

    [XmlRootAttribute("SecondaryContact", Namespace = "", IsNullable = false)]
    public class SecondaryContact
    {
        #region Private Members
        private string m_ContactName;
        private string m_ContactValue;
        private string m_Relation;
        #endregion

        #region Constructor
        public SecondaryContact()
        {
        }
        #endregion

        #region Public Properties
        public string ContactName
        {
            get { return this.m_ContactName; }
            set { this.m_ContactName = value; }
        }
        public string ContactValue
        {
            get { return this.m_ContactValue; }
            set { this.m_ContactValue = value; }
        }
        public string Relation
        {
            get { return this.m_Relation; }
            set { this.m_Relation = value; }
        }
        #endregion
    }
    //P Axis 13.1 : issue 611 END


    public enum EmployeeType
    {
        Officer,
        Owner,
        Regular,
        Statutory
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum PayPeriod
    {
        Daily,
        Weekly,
        Biweekly,
        Semimonthly,
        Monthly,
        Quaterly,
        Yearly
    }

    public enum UseTimeDataToCreatePaychecks
    {
        NotSet,
        UseTimeData,
        DoNotUseTimeData
    }

    public enum AccrualPeriod
    {
        BeginningOfYear,
        EveryHourOnPaycheck,
        EveryPaycheck
    }

    //P Axis 13.1 : issue 611
    public enum PartOrFullTime
    {
        PartTime,
        FullTime
    }
    public enum Exempt
    {
        Exempt,
        NonExempt
    }
    public enum KeyEmployee
    {
        Yes,
        No
    }
    public enum USCitizen
    {
        Yes,
        No
    }
    public enum Ethnicity
    {
        AmericianIndian,
        Asian,
        Black,
        Hawaiian,
        Hispanic,
        White,
        TwoOrMoreRaces
    }
    public enum Disabled
    {
        Yes,
        No
    }
    public enum OnFile
    {
        Yes,
        No
    }
    public enum USVeteran
    {
        Yes,
        No
    }
    public enum MilitaryStatus
    {
        Active,
        Reserve
    }
    public enum Relation
    {
        Spouse,
        Partner,
        Mother,
        Father,
        Sister,
        Brother,
        Son,
        Daughter,
        Friend,
        Other
    }
    //P Axis 13.1 : issue 611 END
}
