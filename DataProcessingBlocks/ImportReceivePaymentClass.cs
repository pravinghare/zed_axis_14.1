using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using System.ComponentModel;
using EDI.Constant;
using System.Collections;

namespace DataProcessingBlocks
{
    public class ImportReceivePaymentClass
    {
        private static ImportReceivePaymentClass m_ImportReceivePaymentClass;
        public bool isIgnoreAll = false;

        #region Constuctor
        public ImportReceivePaymentClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import Receive Payment class
        /// </summary>
        /// <returns></returns>
        public static ImportReceivePaymentClass GetInstance()
        {
            if (m_ImportReceivePaymentClass == null)
                m_ImportReceivePaymentClass = new ImportReceivePaymentClass();
            return m_ImportReceivePaymentClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Receive Payment QuickBooks collection </returns>
        public DataProcessingBlocks.ReceivePaymentQBEntryCollection ImportReceivePaymentData(DataTable dt, ref string logDirectory)
        {
            DataProcessingBlocks.ReceivePaymentQBEntryCollection coll = new ReceivePaymentQBEntryCollection();
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Add ReceivePayment Ref Number

                        ReceivePaymentQBEntry ReceivePayment = new ReceivePaymentQBEntry();
                        ReceivePayment = coll.FindReceivePaymentEntry(dr["RefNumber"].ToString());
                        if (ReceivePayment == null)
                        {
                            #region for ReceivePayment null
                            ReceivePayment = new ReceivePaymentQBEntry();
                            DateTime ReceiveDt = new DateTime();
                            string datevalue = string.Empty;
                            if (dt.Columns.Contains("CustomerRefFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CustomerRefFullName"].ToString();
                                    if (strCust.Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (ReceivePayment.CustomerRef.FullName == null)
                                                {
                                                    ReceivePayment.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 209));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (ReceivePayment.CustomerRef.FullName == null)
                                                {
                                                    ReceivePayment.CustomerRef.FullName = null;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (ReceivePayment.CustomerRef.FullName == null)
                                            {
                                                ReceivePayment.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (ReceivePayment.CustomerRef.FullName == null)
                                        {
                                            ReceivePayment.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out ReceiveDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ReceivePayment.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ReceivePayment.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                ReceivePayment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ReceiveDt = dttest;
                                            ReceivePayment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        ReceiveDt = Convert.ToDateTime(datevalue);
                                        ReceivePayment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    ReceivePayment.ReceivePaymentDate = ReceiveDt;

                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 20)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.RefNumber = dr["RefNumber"].ToString().Substring(0, 20);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.RefNumber = dr["RefNumber"].ToString();
                                        }


                                    }
                                    else
                                        ReceivePayment.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            //for version 6.0 , Apply to ApplyToReceivePaymentRef
                            if (dt.Columns.Contains("ApplyToInvoiceRef"))
                            {

                            }

                            if (dt.Columns.Contains("ARAccountRefFullName"))
                            {
                                #region Validations of ARAccountRef Full name
                                if (dr["ARAccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ARAccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ARAccountRef full name (" + dr["ARAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                                if (ReceivePayment.ARAccountRef.FullName == null)
                                                    ReceivePayment.ARAccountRef.FullName = null;
                                                else
                                                    ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                                if (ReceivePayment.ARAccountRef.FullName == null)
                                                    ReceivePayment.ARAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                            if (ReceivePayment.ARAccountRef.FullName == null)
                                                ReceivePayment.ARAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                        if (ReceivePayment.ARAccountRef.FullName == null)
                                            ReceivePayment.ARAccountRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TotalAmount"))
                            {
                                #region Validations for TotalAmount
                                if (dr["TotalAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["TotalAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalAmount ( " + dr["TotalAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["TotalAmount"].ToString();
                                                ReceivePayment.TotalAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["TotalAmount"].ToString();
                                                ReceivePayment.TotalAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["TotalAmount"].ToString();
                                            ReceivePayment.TotalAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.TotalAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["TotalAmount"].ToString())));
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("PaymentMethodRefFullName"))
                            {
                                #region Validations of PaymentMethodRef
                                if (dr["PaymentMethodRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["PaymentMethodRefFullName"].ToString();
                                    if (strCust.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentMethodRefFullNumber (" + dr["PaymentMethodRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                                if (ReceivePayment.PaymentMethodRef.FullName == null)
                                                    ReceivePayment.PaymentMethodRef.FullName = null;
                                                else
                                                    ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                                if (ReceivePayment.PaymentMethodRef.FullName == null)
                                                    ReceivePayment.PaymentMethodRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (ReceivePayment.PaymentMethodRef.FullName == null)
                                                ReceivePayment.PaymentMethodRef.FullName = null;

                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                        if (ReceivePayment.PaymentMethodRef.FullName == null)
                                            ReceivePayment.PaymentMethodRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("DepositToAccountRefFullName"))
                            {
                                #region Validations of DipositToAccountRef Full name
                                if (dr["DepositToAccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositToAccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositToAccountRef FullName (" + dr["DepositToAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                                if (ReceivePayment.DepositToAccountRef.FullName == null)
                                                {
                                                    ReceivePayment.DepositToAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                                if (ReceivePayment.DepositToAccountRef.FullName == null)
                                                {
                                                    ReceivePayment.DepositToAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                            if (ReceivePayment.DepositToAccountRef.FullName == null)
                                            {
                                                ReceivePayment.DepositToAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                        if (ReceivePayment.DepositToAccountRef.FullName == null)
                                        {
                                            ReceivePayment.DepositToAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0, 4095);
                                                ReceivePayment.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                ReceivePayment.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            ReceivePayment.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        ReceivePayment.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("IsAutoApply"))
                            {
                                #region Validations of IsAutoApply
                                if (dr["IsAutoApply"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsAutoApply"].ToString(), out result))
                                    {
                                        ReceivePayment.IsAutoApply = Convert.ToInt32(dr["IsAutoApply"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsAutoApply"].ToString().ToLower() == "true")
                                        {
                                            ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsAutoApply"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsAutoApply(" + dr["IsAutoApply"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            #region Adding AppliedToTxnAdd Line
                            DataProcessingBlocks.AppliedToTxnAdd AppliedToTxn = new AppliedToTxnAdd();

                            if (dt.Columns.Contains("AppliedToId"))
                            {
                                #region Validations for AppliedToId
                                if (dr["AppliedToId"].ToString() != string.Empty)
                                {
                                    if (dr["AppliedToId"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AppliedToId ( " + dr["AppliedToId"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["AppliedToId"].ToString();
                                                AppliedToTxn.TxnID = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["AppliedToId"].ToString();
                                                AppliedToTxn.TxnID = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["AppliedToId"].ToString();
                                            AppliedToTxn.TxnID = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["AppliedToId"].ToString();
                                        AppliedToTxn.TxnID = strDesc;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ApplyToInvoiceRef"))
                            {
                                #region Validations for ApplyToInvoiceRef
                                if (dr["ApplyToInvoiceRef"].ToString() != string.Empty)
                                {
                                    Hashtable invoiceId = new Hashtable();

                                    //to check whether ref number already exsist
                                    invoiceId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("Invoice", dr["ApplyToInvoiceRef"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                    if (invoiceId.Count == 0)
                                    {
                                        //if no txnid get
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG125"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return null;
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in invoiceId)
                                        {
                                            AppliedToTxn.TxnID = de.Value.ToString();
                                        }
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("DiscountAccountRefFullName"))
                            {
                                #region Validations of item DiscountAccountRef
                                if (dr["DiscountAccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["DiscountAccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAccountRef (" + dr["DiscountAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                    AppliedToTxn.DiscountAccountRef.FullName = null;
                                                else
                                                    AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                    AppliedToTxn.DiscountAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                AppliedToTxn.DiscountAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                        if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                            AppliedToTxn.DiscountAccountRef.FullName = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("PaymentAmount"))
                            {
                                #region Validations for PaymentAmount
                                if (dr["PaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentAmount ( " + dr["PaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["PaymentAmount"].ToString();
                                                AppliedToTxn.PaymentAmount = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["PaymentAmount"].ToString();
                                                AppliedToTxn.PaymentAmount = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["PaymentAmount"].ToString();
                                            AppliedToTxn.PaymentAmount = strDesc;
                                        }
                                    }
                                    else
                                    {

                                        AppliedToTxn.PaymentAmount = string.Format("{0:000000.00}", Convert.ToDouble(dr["PaymentAmount"].ToString()));
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("DiscountAmount"))
                            {
                                #region Validations for DiscountAmount
                                if (dr["DiscountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAmount ( " + dr["DiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                AppliedToTxn.DiscountAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                AppliedToTxn.DiscountAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            AppliedToTxn.DiscountAmount = strAmount;
                                        }
                                    }
                                    else
                                    {

                                        AppliedToTxn.DiscountAmount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountAmount"].ToString()));

                                    }
                                }

                                #endregion

                            }

                            DataProcessingBlocks.SetCredit setCreditLine = new SetCredit();

                            if (dt.Columns.Contains("SetCreditMemoRef"))
                            {
                                #region Validations for SetCreditMemoRef
                                if (dr["SetCreditMemoRef"].ToString() != string.Empty)
                                {
                                    Hashtable creditMemoTxnId = new Hashtable();

                                    //to check whether ref number already exsist
                                    creditMemoTxnId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForSetCreditMemoRef("CreditMemo", dr["SetCreditMemoRef"].ToString(), dr["CustomerRefFullName"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                    if (creditMemoTxnId.Count == 0)
                                    {
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG125"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return null;
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in creditMemoTxnId)
                                        {
                                            setCreditLine.CreditTxnID = de.Value.ToString();
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SetCreditAmount"))
                            {
                                #region Validations for SetCreditAmount

                                if (dr["SetCreditAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SetCreditAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SetCreditAmount ( " + dr["SetCreditAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["SetCreditAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["SetCreditAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["SetCreditAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        setCreditLine.AppliedAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["SetCreditAmount"].ToString())));
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SetCreditOverride"))
                            {
                                #region Validations of SetCreditOverride
                                if (dr["SetCreditOverride"].ToString() != "<None>")
                                {
                                    int result = 0;
                                    if (int.TryParse(dr["SetCreditOverride"].ToString(), out result))
                                    {
                                        setCreditLine.Override = Convert.ToInt32(dr["SetCreditOverride"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["SetCreditOverride"].ToString().ToLower() == "true")
                                        {
                                            setCreditLine.Override = dr["SetCreditOverride"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["SetCreditOverride"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                setCreditLine.Override = dr["SetCreditOverride"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SetCreditOverride (" + dr["SetCreditOverride"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountClassRefFullName"))
                            {
                                #region Validations of DiscountClassRefFullName
                                if (dr["DiscountClassRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["DiscountClassRefFullName"].ToString();
                                    if (strTerms.Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountClassRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountClassRef.FullName == null)
                                                {
                                                    AppliedToTxn.DiscountClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountClassRef.FullName == null)
                                                {
                                                    AppliedToTxn.DiscountClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountClassRef.FullName == null)
                                            {
                                                AppliedToTxn.DiscountClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());

                                        if (AppliedToTxn.DiscountClassRef.FullName == null)
                                        {
                                            AppliedToTxn.DiscountClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (AppliedToTxn.TxnID != null || AppliedToTxn.DiscountAmount != null ||
                                AppliedToTxn.PaymentAmount != null || AppliedToTxn.DiscountAccountRef != null || AppliedToTxn.DiscountClassRef != null)
                            {
                                if (setCreditLine.CreditTxnID != null || setCreditLine.AppliedAmount != null || setCreditLine.Override != null)
                                {
                                    AppliedToTxn.SetCredit.Add(setCreditLine);
                                }
                                ReceivePayment.AppliedToTxnAdd.Add(AppliedToTxn);
                            }


                            #endregion

                            coll.Add(ReceivePayment);
                            #endregion
                        }
                        else
                        {
                            #region not null ReceivePayment
                            #region Adding AppliedToTxnAdd Line

                            DataProcessingBlocks.AppliedToTxnAdd AppliedToTxn = new AppliedToTxnAdd();

                            if (dt.Columns.Contains("AppliedToId"))
                            {
                                #region Validations for AppliedToId
                                if (dr["AppliedToId"].ToString() != string.Empty)
                                {
                                    if (dr["AppliedToId"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AppliedToId ( " + dr["AppliedToId"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["AppliedToId"].ToString();
                                                AppliedToTxn.TxnID = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["AppliedToId"].ToString();
                                                AppliedToTxn.TxnID = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["AppliedToId"].ToString();
                                            AppliedToTxn.TxnID = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["AppliedToId"].ToString();
                                        AppliedToTxn.TxnID = strDesc;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ApplyToInvoiceRef"))
                            {
                                #region Validations for ApplyToInvoiceRef
                                if (dr["ApplyToInvoiceRef"].ToString() != string.Empty)
                                {
                                    Hashtable invoiceId = new Hashtable();

                                    //to check whether ref number already exsist
                                    invoiceId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("Invoice", dr["ApplyToInvoiceRef"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                    if (invoiceId.Count == 0)
                                    {
                                        //if no txnid get
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG125"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return null;
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in invoiceId)
                                        {
                                            AppliedToTxn.TxnID = de.Value.ToString();
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountAccountRefFullName"))
                            {
                                #region Validations of item DiscountAccountRef
                                if (dr["DiscountAccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["DiscountAccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAccountRef (" + dr["DiscountAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                    AppliedToTxn.DiscountAccountRef.FullName = null;
                                                else
                                                    AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                    AppliedToTxn.DiscountAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                AppliedToTxn.DiscountAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                        if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                            AppliedToTxn.DiscountAccountRef.FullName = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("PaymentAmount"))
                            {
                                #region Validations for PaymentAmount
                                if (dr["PaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentAmount ( " + dr["PaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["PaymentAmount"].ToString();
                                                AppliedToTxn.PaymentAmount = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["PaymentAmount"].ToString();
                                                AppliedToTxn.PaymentAmount = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["PaymentAmount"].ToString();
                                            AppliedToTxn.PaymentAmount = strDesc;
                                        }
                                    }
                                    else
                                    {

                                        AppliedToTxn.PaymentAmount = string.Format("{0:000000.00}", Convert.ToDouble(dr["PaymentAmount"].ToString()));
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("DiscountAmount"))
                            {
                                #region Validations for DiscountAmount
                                if (dr["DiscountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAmount ( " + dr["DiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                AppliedToTxn.DiscountAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                AppliedToTxn.DiscountAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            AppliedToTxn.DiscountAmount = strAmount;
                                        }
                                    }
                                    else
                                    {

                                        AppliedToTxn.DiscountAmount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountAmount"].ToString()));

                                    }
                                }

                                #endregion

                            }

                            DataProcessingBlocks.SetCredit setCreditLine = new SetCredit();

                            if (dt.Columns.Contains("SetCreditMemoRef"))
                            {
                                #region Validations for SetCreditMemoRef
                                if (dr["SetCreditMemoRef"].ToString() != string.Empty)
                                {
                                    Hashtable creditMemoTxnId = new Hashtable();

                                    //to check whether ref number already exsist
                                    creditMemoTxnId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForSetCreditMemoRef("CreditMemo", dr["SetCreditMemoRef"].ToString(), dr["CustomerRefFullName"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                    if (creditMemoTxnId.Count == 0)
                                    {
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG125"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return null;
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in creditMemoTxnId)
                                        {
                                            setCreditLine.CreditTxnID = de.Value.ToString();
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SetCreditAmount"))
                            {
                                #region Validations for SetCreditAmount

                                if (dr["SetCreditAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["SetCreditAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SetCreditAmount ( " + dr["SetCreditAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["SetCreditAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["SetCreditAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["SetCreditAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        setCreditLine.AppliedAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["SetCreditAmount"].ToString())));
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SetCreditOverride"))
                            {
                                #region Validations of SetCreditOverride
                                if (dr["SetCreditOverride"].ToString() != "<None>")
                                {
                                    int result = 0;
                                    if (int.TryParse(dr["SetCreditOverride"].ToString(), out result))
                                    {
                                        setCreditLine.Override = Convert.ToInt32(dr["SetCreditOverride"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["SetCreditOverride"].ToString().ToLower() == "true")
                                        {
                                            setCreditLine.Override = dr["SetCreditOverride"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["SetCreditOverride"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                setCreditLine.Override = dr["SetCreditOverride"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SetCreditOverride (" + dr["SetCreditOverride"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountClassRefFullName"))
                            {
                                #region Validations of DiscountClassRefFullName
                                if (dr["DiscountClassRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["DiscountClassRefFullName"].ToString();
                                    if (strTerms.Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountClassRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountClassRef.FullName == null)
                                                {
                                                    AppliedToTxn.DiscountClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                                if (AppliedToTxn.DiscountClassRef.FullName == null)
                                                {
                                                    AppliedToTxn.DiscountClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountClassRef.FullName == null)
                                            {
                                                AppliedToTxn.DiscountClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());

                                        if (AppliedToTxn.DiscountClassRef.FullName == null)
                                        {
                                            AppliedToTxn.DiscountClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (AppliedToTxn.TxnID != null || AppliedToTxn.DiscountAmount != null ||
                                AppliedToTxn.PaymentAmount != null || AppliedToTxn.DiscountAccountRef != null || AppliedToTxn.DiscountClassRef != null)
                            {
                                if (setCreditLine.CreditTxnID != null || setCreditLine.AppliedAmount != null || setCreditLine.Override != null)
                                {
                                    AppliedToTxn.SetCredit.Add(setCreditLine);
                                }
                                ReceivePayment.AppliedToTxnAdd.Add(AppliedToTxn);
                            }
                            #endregion
                            #endregion
                        }

                        #endregion
                    }
                    else
                    {
                        #region Without Adding Ref Number
                        ReceivePaymentQBEntry ReceivePayment = new ReceivePaymentQBEntry();

                        DateTime ReceiveDt = new DateTime();
                        string datevalue = string.Empty;
                        if (dt.Columns.Contains("CustomerRefFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["CustomerRefFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["CustomerRefFullName"].ToString();
                                if (strCust.Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (ReceivePayment.CustomerRef.FullName == null)
                                            {
                                                ReceivePayment.CustomerRef.FullName = null;
                                            }
                                            else
                                            {
                                                ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 209));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            isIgnoreAll = true;
                                            ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (ReceivePayment.CustomerRef.FullName == null)
                                            {
                                                ReceivePayment.CustomerRef.FullName = null;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (ReceivePayment.CustomerRef.FullName == null)
                                        {
                                            ReceivePayment.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ReceivePayment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                    if (ReceivePayment.CustomerRef.FullName == null)
                                    {
                                        ReceivePayment.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out ReceiveDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ReceivePayment.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        ReceiveDt = dttest;
                                        ReceivePayment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    ReceiveDt = Convert.ToDateTime(datevalue);
                                    ReceivePayment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref Number
                            if (datevalue != string.Empty)
                                ReceivePayment.ReceivePaymentDate = ReceiveDt;

                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["RefNumber"].ToString();
                                if (strRefNum.Length > 20)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ReceivePayment.RefNumber = dr["RefNumber"].ToString().Substring(0, 20);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ReceivePayment.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.RefNumber = dr["RefNumber"].ToString();
                                    }


                                }
                                else
                                    ReceivePayment.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }

                        //for version 6.0 , Apply to ApplyToReceivePaymentRef
                        if (dt.Columns.Contains("ApplyToInvoiceRef"))
                        {

                        }

                        if (dt.Columns.Contains("ARAccountRefFullName"))
                        {
                            #region Validations of ARAccountRef Full name
                            if (dr["ARAccountRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ARAccountRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ARAccountRef full name (" + dr["ARAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                            if (ReceivePayment.ARAccountRef.FullName == null)
                                                ReceivePayment.ARAccountRef.FullName = null;
                                            else
                                                ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                            if (ReceivePayment.ARAccountRef.FullName == null)
                                                ReceivePayment.ARAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                        if (ReceivePayment.ARAccountRef.FullName == null)
                                            ReceivePayment.ARAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ReceivePayment.ARAccountRef = new ARAccountRef(dr["ARAccountRefFullName"].ToString());
                                    if (ReceivePayment.ARAccountRef.FullName == null)
                                        ReceivePayment.ARAccountRef.FullName = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TotalAmount"))
                        {
                            #region Validations for TotalAmount
                            if (dr["TotalAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["TotalAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalAmount ( " + dr["TotalAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["TotalAmount"].ToString();
                                            ReceivePayment.TotalAmount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["TotalAmount"].ToString();
                                            ReceivePayment.TotalAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["TotalAmount"].ToString();
                                        ReceivePayment.TotalAmount = strAmount;
                                    }
                                }
                                else
                                {
                                    ReceivePayment.TotalAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["TotalAmount"].ToString())));
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ReceivePayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ReceivePayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    ReceivePayment.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("PaymentMethodRefFullName"))
                        {
                            #region Validations of PaymentMethodRef
                            if (dr["PaymentMethodRefFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["PaymentMethodRefFullName"].ToString();
                                if (strCust.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentMethodRefFullNumber (" + dr["PaymentMethodRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (ReceivePayment.PaymentMethodRef.FullName == null)
                                                ReceivePayment.PaymentMethodRef.FullName = null;
                                            else
                                                ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (ReceivePayment.PaymentMethodRef.FullName == null)
                                                ReceivePayment.PaymentMethodRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                        if (ReceivePayment.PaymentMethodRef.FullName == null)
                                            ReceivePayment.PaymentMethodRef.FullName = null;

                                    }
                                }
                                else
                                {
                                    ReceivePayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                    if (ReceivePayment.PaymentMethodRef.FullName == null)
                                        ReceivePayment.PaymentMethodRef.FullName = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("DepositToAccountRefFullName"))
                        {
                            #region Validations of DipositToAccountRef Full name
                            if (dr["DepositToAccountRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["DepositToAccountRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositToAccountRef FullName (" + dr["DepositToAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                            if (ReceivePayment.DepositToAccountRef.FullName == null)
                                            {
                                                ReceivePayment.DepositToAccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                            if (ReceivePayment.DepositToAccountRef.FullName == null)
                                            {
                                                ReceivePayment.DepositToAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                        if (ReceivePayment.DepositToAccountRef.FullName == null)
                                        {
                                            ReceivePayment.DepositToAccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ReceivePayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                    if (ReceivePayment.DepositToAccountRef.FullName == null)
                                    {
                                        ReceivePayment.DepositToAccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString().Substring(0, 4095);
                                            ReceivePayment.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            ReceivePayment.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        ReceivePayment.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    ReceivePayment.Memo = strMemo;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("IsAutoApply"))
                        {
                            #region Validations of IsAutoApply
                            if (dr["IsAutoApply"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsAutoApply"].ToString(), out result))
                                {
                                    ReceivePayment.IsAutoApply = Convert.ToInt32(dr["IsAutoApply"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsAutoApply"].ToString().ToLower() == "true")
                                    {
                                        ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsAutoApply"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsAutoApply(" + dr["IsAutoApply"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ReceivePayment.IsAutoApply = dr["IsAutoApply"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        #region Adding AppliedToTxnAdd Line

                        DataProcessingBlocks.AppliedToTxnAdd AppliedToTxn = new AppliedToTxnAdd();

                        if (dt.Columns.Contains("AppliedToId"))
                        {
                            #region Validations for AppliedToId
                            if (dr["AppliedToId"].ToString() != string.Empty)
                            {
                                if (dr["AppliedToId"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AppliedToId ( " + dr["AppliedToId"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["AppliedToId"].ToString();
                                            AppliedToTxn.TxnID = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["AppliedToId"].ToString();
                                            AppliedToTxn.TxnID = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["AppliedToId"].ToString();
                                        AppliedToTxn.TxnID = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["AppliedToId"].ToString();
                                    AppliedToTxn.TxnID = strDesc;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ApplyToInvoiceRef"))
                        {
                            #region Validations for ApplyToInvoiceRef
                            if (dr["ApplyToInvoiceRef"].ToString() != string.Empty)
                            {
                                Hashtable invoiceId = new Hashtable();

                                //to check whether ref number already exsist
                                invoiceId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("Invoice", dr["ApplyToInvoiceRef"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                if (invoiceId.Count == 0)
                                {
                                    //if no txnid get
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG125"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return null;
                                }
                                else
                                {
                                    foreach (DictionaryEntry de in invoiceId)
                                    {
                                        AppliedToTxn.TxnID = de.Value.ToString();
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("DiscountAccountRefFullName"))
                        {
                            #region Validations of item DiscountAccountRef
                            if (dr["DiscountAccountRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["DiscountAccountRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountAccountRef (" + dr["DiscountAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                AppliedToTxn.DiscountAccountRef.FullName = null;
                                            else
                                                AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                                AppliedToTxn.DiscountAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                        if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                            AppliedToTxn.DiscountAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    AppliedToTxn.DiscountAccountRef = new DiscountAccountRef(string.Empty, dr["DiscountAccountRefFullName"].ToString());
                                    if (AppliedToTxn.DiscountAccountRef.FullName == null)
                                        AppliedToTxn.DiscountAccountRef.FullName = null;
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("PaymentAmount"))
                        {
                            #region Validations for PaymentAmount
                            if (dr["PaymentAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["PaymentAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentAmount ( " + dr["PaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["PaymentAmount"].ToString();
                                            AppliedToTxn.PaymentAmount = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["PaymentAmount"].ToString();
                                            AppliedToTxn.PaymentAmount = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["PaymentAmount"].ToString();
                                        AppliedToTxn.PaymentAmount = strDesc;
                                    }
                                }
                                else
                                {

                                    AppliedToTxn.PaymentAmount = string.Format("{0:000000.00}", Convert.ToDouble(dr["PaymentAmount"].ToString()));
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("DiscountAmount"))
                        {
                            #region Validations for DiscountAmount
                            if (dr["DiscountAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DiscountAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountAmount ( " + dr["DiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            AppliedToTxn.DiscountAmount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            AppliedToTxn.DiscountAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["DiscountAmount"].ToString();
                                        AppliedToTxn.DiscountAmount = strAmount;
                                    }
                                }
                                else
                                {

                                    AppliedToTxn.DiscountAmount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountAmount"].ToString()));

                                }
                            }

                            #endregion

                        }

                        DataProcessingBlocks.SetCredit setCreditLine = new SetCredit();

                        if (dt.Columns.Contains("SetCreditMemoRef"))
                        {
                            #region Validations for SetCreditMemoRef
                            if (dr["SetCreditMemoRef"].ToString() != string.Empty)
                            {
                                Hashtable creditMemoTxnId = new Hashtable();

                                //to check whether ref number already exsist
                                creditMemoTxnId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForSetCreditMemoRef("CreditMemo", dr["SetCreditMemoRef"].ToString(), dr["CustomerRefFullName"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                if (creditMemoTxnId.Count == 0)
                                {
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG125"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return null;
                                }
                                else
                                {
                                    foreach (DictionaryEntry de in creditMemoTxnId)
                                    {
                                        setCreditLine.CreditTxnID = de.Value.ToString();
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SetCreditAmount"))
                        {
                            #region Validations for SetCreditAmount

                            if (dr["SetCreditAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["SetCreditAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SetCreditAmount ( " + dr["SetCreditAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["SetCreditAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["SetCreditAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["SetCreditAmount"].ToString();
                                        setCreditLine.AppliedAmount = strAmount;
                                    }
                                }
                                else
                                {
                                    setCreditLine.AppliedAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["SetCreditAmount"].ToString())));
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SetCreditOverride"))
                        {
                            #region Validations of SetCreditOverride
                            if (dr["SetCreditOverride"].ToString() != "<None>")
                            {
                                int result = 0;
                                if (int.TryParse(dr["SetCreditOverride"].ToString(), out result))
                                {
                                    setCreditLine.Override = Convert.ToInt32(dr["SetCreditOverride"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["SetCreditOverride"].ToString().ToLower() == "true")
                                    {
                                        setCreditLine.Override = dr["SetCreditOverride"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["SetCreditOverride"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            setCreditLine.Override = dr["SetCreditOverride"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SetCreditOverride (" + dr["SetCreditOverride"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            setCreditLine.Override = dr["SetCreditOverride"].ToString();
                                        }
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("DiscountClassRefFullName"))
                        {
                            #region Validations of DiscountClassRefFullName
                            if (dr["DiscountClassRefFullName"].ToString() != string.Empty)
                            {
                                string strTerms = dr["DiscountClassRefFullName"].ToString();
                                if (strTerms.Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountClassRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountClassRef.FullName == null)
                                            {
                                                AppliedToTxn.DiscountClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString().Substring(0, 159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                            if (AppliedToTxn.DiscountClassRef.FullName == null)
                                            {
                                                AppliedToTxn.DiscountClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());
                                        if (AppliedToTxn.DiscountClassRef.FullName == null)
                                        {
                                            AppliedToTxn.DiscountClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    AppliedToTxn.DiscountClassRef = new DiscountClassRef(dr["DiscountClassRefFullName"].ToString());

                                    if (AppliedToTxn.DiscountClassRef.FullName == null)
                                    {
                                        AppliedToTxn.DiscountClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (AppliedToTxn.TxnID != null || AppliedToTxn.DiscountAmount != null ||
                                AppliedToTxn.PaymentAmount != null || AppliedToTxn.DiscountAccountRef != null ||
                                AppliedToTxn.DiscountClassRef != null)
                        {
                            if (setCreditLine.CreditTxnID != null || setCreditLine.AppliedAmount != null || setCreditLine.Override != null)
                            {
                                AppliedToTxn.SetCredit.Add(setCreditLine);
                            }
                            ReceivePayment.AppliedToTxnAdd.Add(AppliedToTxn);
                        }
                        #endregion

                        coll.Add(ReceivePayment);
                        #endregion
                    }
                }
                else
                { return null; }

            }
            #endregion

            #region Customer and Account Requests
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                        if (dt.Columns.Contains("CustomerRefFullName"))
                        {
                            if (dr["CustomerRefFullName"].ToString() != string.Empty)
                            {
                                string customerName = dr["CustomerRefFullName"].ToString();
                                string[] arr = new string[15];
                                if (customerName.Contains(":"))
                                {
                                    arr = customerName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["CustomerRefFullName"].ToString();
                                }
                                #region Set Customer Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                        qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                        CustomerQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    CustomerQueryRq.AppendChild(FullName);
                                                }
                                            }
                                            else
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                FullName.InnerText = arr[i];
                                                CustomerQueryRq.AppendChild(FullName);
                                            }

                                            string pinput = pxmldoc.OuterXml;

                                            string resp = string.Empty;
                                            try
                                            {

                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonUtilities.WriteErrorLog(ex.Message);
                                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            }
                                            finally
                                            {
                                                if (resp != string.Empty)
                                                {

                                                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                    outputXMLDoc.LoadXml(resp);
                                                    string statusSeverity = string.Empty;
                                                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                                    {
                                                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                                    }
                                                    outputXMLDoc.RemoveAll();
                                                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                    {
                                                        //statusMessage += "\n ";
                                                        //statusMessage += oNode.Attributes["statusMessage"].Value.ToString();

                                                        #region Customer Add Query

                                                        XmlDocument xmldocadd = new XmlDocument();
                                                        xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                        xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                        xmldocadd.AppendChild(qbXMLcust);
                                                        XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                        qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                        qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                        XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                        qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                        CustomerAddRq.SetAttribute("requestID", "1");
                                                        XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                        CustomerAddRq.AppendChild(CustomerAdd);
                                                        XmlElement Name = xmldocadd.CreateElement("Name");
                                                        Name.InnerText = arr[i];
                                                        CustomerAdd.AppendChild(Name);

                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                                    CustomerAdd.AppendChild(INIParent);
                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }
                                                        #region Adding Bill Address of Customer.

                                                        if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                            (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                        {
                                                            XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                            CustomerAdd.AppendChild(BillAddress);
                                                            if (dt.Columns.Contains("BillAddr1"))
                                                            {

                                                                if (dr["BillAddr1"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                                    BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                                    BillAddress.AppendChild(BillAdd1);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillAddr2"))
                                                            {
                                                                if (dr["BillAddr2"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                                    BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                                    BillAddress.AppendChild(BillAdd2);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillAddr3"))
                                                            {
                                                                if (dr["BillAddr3"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                                    BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                                    BillAddress.AppendChild(BillAdd3);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillAddr4"))
                                                            {
                                                                if (dr["BillAddr4"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                                    BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                                    BillAddress.AppendChild(BillAdd4);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillAddr5"))
                                                            {
                                                                if (dr["BillAddr5"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                                    BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                                    BillAddress.AppendChild(BillAdd5);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillCity"))
                                                            {
                                                                if (dr["BillCity"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillCity = xmldocadd.CreateElement("City");
                                                                    BillCity.InnerText = dr["BillCity"].ToString();
                                                                    BillAddress.AppendChild(BillCity);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillState"))
                                                            {
                                                                if (dr["BillState"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillState = xmldocadd.CreateElement("State");
                                                                    BillState.InnerText = dr["BillState"].ToString();
                                                                    BillAddress.AppendChild(BillState);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillPostalCode"))
                                                            {
                                                                if (dr["BillPostalCode"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                    BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                                    BillAddress.AppendChild(BillPostalCode);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillCountry"))
                                                            {
                                                                if (dr["BillCountry"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                                    BillCountry.InnerText = dr["BillCountry"].ToString();
                                                                    BillAddress.AppendChild(BillCountry);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("BillNote"))
                                                            {
                                                                if (dr["BillNote"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                                    BillNote.InnerText = dr["BillNote"].ToString();
                                                                    BillAddress.AppendChild(BillNote);
                                                                }
                                                            }

                                                        }

                                                        #endregion

                                                        #region Adding Ship Address of Customer.
                                                        if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                            (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                        {
                                                            XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                            CustomerAdd.AppendChild(ShipAddress);
                                                            if (dt.Columns.Contains("ShipAddr1"))
                                                            {

                                                                if (dr["ShipAddr1"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                                    ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                                    ShipAddress.AppendChild(ShipAdd1);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipAddr2"))
                                                            {
                                                                if (dr["ShipAddr2"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                                    ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                                    ShipAddress.AppendChild(ShipAdd2);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipAddr3"))
                                                            {
                                                                if (dr["ShipAddr3"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                                    ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                                    ShipAddress.AppendChild(ShipAdd3);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipAddr4"))
                                                            {
                                                                if (dr["ShipAddr4"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                                    ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                                    ShipAddress.AppendChild(ShipAdd4);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipAddr5"))
                                                            {
                                                                if (dr["ShipAddr5"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                                    ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                                    ShipAddress.AppendChild(ShipAdd5);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipCity"))
                                                            {
                                                                if (dr["ShipCity"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                                    ShipCity.InnerText = dr["ShipCity"].ToString();
                                                                    ShipAddress.AppendChild(ShipCity);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipState"))
                                                            {
                                                                if (dr["ShipState"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipState = xmldocadd.CreateElement("State");
                                                                    ShipState.InnerText = dr["ShipState"].ToString();
                                                                    ShipAddress.AppendChild(ShipState);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipPostalCode"))
                                                            {
                                                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                    ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                                    ShipAddress.AppendChild(ShipPostalCode);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipCountry"))
                                                            {
                                                                if (dr["ShipCountry"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                                    ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                                    ShipAddress.AppendChild(ShipCountry);
                                                                }
                                                            }
                                                            if (dt.Columns.Contains("ShipNote"))
                                                            {
                                                                if (dr["ShipNote"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                                    ShipNote.InnerText = dr["ShipNote"].ToString();
                                                                    ShipAddress.AppendChild(ShipNote);
                                                                }
                                                            }

                                                        }
                                                        #endregion

                                                        string custinput = xmldocadd.OuterXml;
                                                        string respcust = string.Empty;
                                                        try
                                                        {
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            CommonUtilities.WriteErrorLog(ex.Message);
                                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                        }
                                                        finally
                                                        {
                                                            if (respcust != string.Empty)
                                                            {
                                                                System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                                outputcustXMLDoc.LoadXml(respcust);
                                                                foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                                {
                                                                    string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                                    if (statusSeveritycust == "Error")
                                                                    {
                                                                        string msg = "New Customer could not be created into QuickBooks \n ";
                                                                        msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";

                                                                        ErrorSummary summary = new ErrorSummary(msg);
                                                                        summary.ShowDialog();
                                                                        CommonUtilities.WriteErrorLog(msg);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        #endregion

                                                    }
                                                }

                                            }

                                        }

                                    }
                                }
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }


                }
                #endregion

            }
            return coll;
        }
    }
}
