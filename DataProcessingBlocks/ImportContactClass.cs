﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    class ImportContactClass
    {
        private static ImportContactClass m_ImportContactClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportContactClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import contact class
        /// </summary>
        /// <returns></returns>
        public static ImportContactClass GetInstance()
        {
            if (m_ImportContactClass == null)
                m_ImportContactClass = new ImportContactClass();
            return m_ImportContactClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Customer QuickBooks collection </returns>
        [MethodImpl(MethodImplOptions.NoOptimization)]
        public DataProcessingBlocks.ContactsXeroEntryCollection ImportContactData(DataTable dt, ref string logDirectory)
        {

            //Create an instance of Customer Entry collections.
            DataProcessingBlocks.ContactsXeroEntryCollection coll = new ContactsXeroEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
          
            Guid contactid = new Guid();
           
            string datevalue = string.Empty;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Contacts Entry

            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }


                    //Contacts Validation
                    DataProcessingBlocks.ContactsXeroEntry Contact = new ContactsXeroEntry();

                        if (dt.Columns.Contains("ContactID"))
                        {
                            #region Validations for ContactID
                            if (dr["ContactID"].ToString() != string.Empty)
                            {
                                                      
                               string strcontid = dr["ContactID"].ToString();
                              
                               if (!Guid.TryParse(strcontid, out contactid))
                               {
                                bool IsValid = false;
                                Guid id = new Guid();

                                try
                                {
                                    id = Guid.Parse(strcontid);
                                    strcontid = id.ToString("D");
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ContactID ( " + dr["ContactID"].ToString() + " ) is not valid this mapping .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        
                                    }
                                    else
                                    {
                                        strcontid = dr["ContactID"].ToString();
                                        Contact.ContactID = Guid.Parse(strcontid);
                                    }
                                }
                                else
                                {
                                    strcontid = dr["ContactID"].ToString();
                                    Contact.ContactID = Guid.Parse(strcontid);


                                }

                               
                            }
                            else
                            {
                                strcontid = dr["ContactID"].ToString();
                                Contact.ContactID = Guid.Parse(strcontid);
                            }
                        }

                        #endregion
                        }

                        if (dt.Columns.Contains("ContactStatus"))
                        {

                             #region Validations of ContactStatus
                        if (dr["ContactStatus"].ToString() != string.Empty)
                        {
                            try
                            {
                                string name = Convert.ToString((DataProcessingBlocks.ContactStatus)Enum.Parse(typeof(DataProcessingBlocks.ContactStatus), dr["ContactStatus"].ToString(), true));
                                Contact.ContactStatus = name.ToUpper();
                            }
                            catch
                            {
                                Contact.ContactStatus = dr["ContactStatus"].ToString();
                            }
                        }
                        #endregion

                        }
                        if (dt.Columns.Contains("Name"))
                        {
                            #region Validations of Name
                            if (dr["Name"].ToString() != string.Empty)
                            {
                                if (dr["Name"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Name ( " + dr["Name"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.Name = dr["Name"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.Name = dr["Name"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.Name = dr["Name"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("FirstName"))
                        {
                            #region Validations of FirstName
                            if (dr["FirstName"].ToString() != string.Empty)
                            {
                                if (dr["FirstName"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.FirstName = dr["FirstName"].ToString().Substring(0, 25);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.FirstName = dr["FirstName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LastName"))
                        {
                            #region Validations of LastName
                            if (dr["LastName"].ToString() != string.Empty)
                            {
                                if (dr["LastName"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LastName (" + dr["LastName"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.LastName = dr["LastName"].ToString().Substring(0, 25);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.LastName = dr["LastName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.LastName = dr["LastName"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("EmailAddress"))
                        {
                            #region Validations of LastName
                            if (dr["EmailAddress"].ToString() != string.Empty)
                            {
                                if (dr["EmailAddress"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This EmailAddress ( " + dr["EmailAddress"].ToString() + " ) is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.EmailAddress = dr["EmailAddress"].ToString().Substring(0, 25);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.EmailAddress = dr["EmailAddress"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.EmailAddress = dr["EmailAddress"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.EmailAddress = dr["EmailAddress"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SkypeUserName"))
                        {
                            #region Validations of LastName
                            if (dr["SkypeUserName"].ToString() != string.Empty)
                            {
                                if (dr["SkypeUserName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SkypeUserName ( " + dr["SkypeUserName"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.SkypeUserName = dr["SkypeUserName"].ToString().Substring(0, 25);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.SkypeUserName = dr["SkypeUserName"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.SkypeUserName = dr["SkypeUserName"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.SkypeUserName = dr["SkypeUserName"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BankAccountDetails"))
                        {
                            #region Validations of BankAccountDetails
                            if (dr["BankAccountDetails"].ToString() != string.Empty)
                            {
                                if (dr["BankAccountDetails"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BankAccountDetails ( " + dr["BankAccountDetails"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.BankAccountDetails = dr["BankAccountDetails"].ToString().Substring(0, 25);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.BankAccountDetails = dr["BankAccountDetails"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.BankAccountDetails = dr["BankAccountDetails"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.BankAccountDetails = dr["BankAccountDetails"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TaxNumber"))
                        {
                            #region Validations of LastName
                            if (dr["TaxNumber"].ToString() != string.Empty)
                            {
                                if (dr["TaxNumber"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TaxNumber ( " + dr["TaxNumber"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.TaxNumber = dr["TaxNumber"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.TaxNumber = dr["TaxNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.TaxNumber = dr["TaxNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.TaxNumber = dr["TaxNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AccountsReceivableTaxType"))
                        {
                            #region Validations of LastName
                            if (dr["AccountsReceivableTaxType"].ToString() != string.Empty)
                            {
                                if (dr["AccountsReceivableTaxType"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AccountsReceivableTaxType ( " + dr["AccountsReceivableTaxType"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.AccountsReceivableTaxType = dr["AccountsReceivableTaxType"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.AccountsReceivableTaxType = dr["AccountsReceivableTaxType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.AccountsReceivableTaxType = dr["AccountsReceivableTaxType"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.AccountsReceivableTaxType = dr["AccountsReceivableTaxType"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AccountsPayableTaxType"))
                        {
                            #region Validations of LastName
                            if (dr["AccountsPayableTaxType"].ToString() != string.Empty)
                            {
                                if (dr["AccountsPayableTaxType"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AccountsPayableTaxType ( " + dr["AccountsPayableTaxType"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.AccountsPayableTaxType = dr["AccountsPayableTaxType"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.AccountsPayableTaxType = dr["AccountsPayableTaxType"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.AccountsPayableTaxType = dr["AccountsPayableTaxType"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.AccountsPayableTaxType = dr["AccountsPayableTaxType"].ToString();
                                }
                            }
                            #endregion
                        }

                        QuickBookEntities.Addresses AddressesItem = new  QuickBookEntities.Addresses();
                        if (dt.Columns.Contains("AddressType"))
                        {
                            #region Validations of AddressType
                            if (dr["AddressType"].ToString() != string.Empty)
                            {
                                try
                                {
                                    string name = Convert.ToString((DataProcessingBlocks.AddressType)Enum.Parse(typeof(DataProcessingBlocks.AddressType), dr["AddressType"].ToString(), true));
                                    AddressesItem.AddressType = name.ToUpper();
                                }
                                catch
                                {
                                    AddressesItem.AddressType = dr["AddressType"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("AddressLine1"))
                        {
                            #region Validations of AddressLine1
                            if (dr["AddressLine1"].ToString() != string.Empty)
                            {
                                if (dr["AddressLine1"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AddressLine1 (" + dr["AddressLine1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.Addr1 = dr["AddressLine1"].ToString().Substring(0, 41);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.Addr1 = dr["AddressLine1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AddressesItem.Addr1 = dr["AddressLine1"].ToString();
                                    }
                                }
                                else
                                {
                                    AddressesItem.Addr1 = dr["AddressLine1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AddressLine2"))
                        {
                            #region Validations of Bill Addr2
                            if (dr["AddressLine2"].ToString() != string.Empty)
                            {
                                if (dr["AddressLine2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AddressLine2 (" + dr["AddressLine2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.Addr2 = dr["AddressLine2"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.Addr2 = dr["AddressLine2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AddressesItem.Addr2 = dr["AddressLine2"].ToString();
                                    }
                                }
                                else
                                {
                                    AddressesItem.Addr2 = dr["AddressLine2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AddressLine3"))
                        {
                            #region Validations of AddressLine3
                            if (dr["AddressLine3"].ToString() != string.Empty)
                            {
                                if (dr["AddressLine3"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AddressLine3 (" + dr["AddressLine3"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.Addr3 = dr["AddressLine3"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.Addr3 = dr["AddressLine3"].ToString();
                                        }
                                    }
                                    else
                                        AddressesItem.Addr3 = dr["AddressLine3"].ToString();

                                }
                                else
                                {
                                    AddressesItem.Addr3 = dr["AddressLine3"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AddressLine4"))
                        {
                            #region Validations of AddressLine4
                            if (dr["AddressLine4"].ToString() != string.Empty)
                            {
                                if (dr["AddressLine4"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AddressLine4 (" + dr["AddressLine4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.Addr4 = dr["AddressLine4"].ToString().Substring(0, 41);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.Addr4 = dr["AddressLine4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AddressesItem.Addr4 = dr["AddressLine4"].ToString();
                                    }
                                }
                                else
                                {
                                    AddressesItem.Addr4 = dr["AddressLine4"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("City"))
                        {
                            #region Validations of City
                            if (dr["City"].ToString() != string.Empty)
                            {
                                if (dr["City"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This City (" + dr["City"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.City = dr["City"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.City = dr["City"].ToString();
                                        }
                                    }
                                    else
                                        AddressesItem.City = dr["City"].ToString();
                                }
                                else
                                {
                                    AddressesItem.City = dr["City"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Region"))
                        {
                            #region Validations of Region
                            if (dr["Region"].ToString() != string.Empty)
                            {
                                if (dr["Region"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Region (" + dr["Region"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.Region = dr["Region"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.Region = dr["Region"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AddressesItem.Region = dr["Region"].ToString();
                                    }
                                }
                                else
                                {
                                    AddressesItem.Region = dr["Region"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PostalCode"))
                        {
                            #region Validations of Postal Code
                            if (dr["PostalCode"].ToString() != string.Empty)
                            {
                                if (dr["PostalCode"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This  Postal Code (" + dr["PostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.PostalCode = dr["PostalCode"].ToString().Substring(0, 13);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.PostalCode = dr["PostalCode"].ToString();
                                        }
                                    }
                                    else
                                        AddressesItem.PostalCode = dr["PostalCode"].ToString();
                                }
                                else
                                {
                                    AddressesItem.PostalCode = dr["PostalCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Country"))
                        {
                            #region Validations of  Country
                            if (dr["Country"].ToString() != string.Empty)
                            {
                                if (dr["Country"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Country (" + dr["Country"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.Country = dr["BillCountry"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.Country = dr["Country"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        AddressesItem.Country = dr["Country"].ToString();
                                    }
                                }
                                else
                                {
                                    AddressesItem.Country = dr["Country"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AttentionTo"))
                        {
                            #region Validations of AttentionTo
                            if (dr["AttentionTo"].ToString() != string.Empty)
                            {
                                if (dr["AttentionTo"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AttentionTo (" + dr["AttentionTo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            AddressesItem.AttentionTo = dr["AttentionTo"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            AddressesItem.AttentionTo = dr["AttentionTo"].ToString();
                                        }
                                    }
                                    else
                                        AddressesItem.AttentionTo = dr["AttentionTo"].ToString();
                                }
                                else
                                {
                                    AddressesItem.AttentionTo = dr["AttentionTo"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (AddressesItem.AddressType != null || AddressesItem.Addr1 != null || AddressesItem.Addr2 != null || AddressesItem.Addr3 != null || AddressesItem.Addr4 != null || AddressesItem.City != null
                                    || AddressesItem.Region != null || AddressesItem.PostalCode != null || AddressesItem.Country != null || AddressesItem.AttentionTo != null)
                            Contact.Addresses.Add(AddressesItem);

                        QuickBookEntities.Phones PhoneItem = new QuickBookEntities.Phones();

                        if (dt.Columns.Contains("PhoneType"))
                        {
                            #region Validations of PhoneType
                            if (dr["PhoneType"].ToString() != string.Empty)
                            {
                                try
                                {
                                    string name = Convert.ToString((DataProcessingBlocks.PhoneType)Enum.Parse(typeof(DataProcessingBlocks.PhoneType), dr["PhoneType"].ToString(), true));
                                    PhoneItem.PhoneType = name.ToUpper();
                                }
                                catch
                                {
                                    PhoneItem.PhoneType = dr["PhoneType"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PhoneNumber"))
                        {
                            #region Validations of Ship Addr2
                            if (dr["PhoneNumber"].ToString() != string.Empty)
                            {
                                if (dr["PhoneNumber"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PhoneNumber (" + dr["PhoneNumber"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PhoneItem.PhoneNumber = dr["PhoneNumber"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            PhoneItem.PhoneNumber = dr["PhoneNumber"].ToString();
                                        }
                                    }
                                    else
                                        PhoneItem.PhoneNumber = dr["PhoneNumber"].ToString();
                                }
                                else
                                {
                                    PhoneItem.PhoneNumber = dr["PhoneNumber"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("PhoneAreaCode"))
                        {
                            #region Validations of Ship Addr3
                            if (dr["PhoneAreaCode"].ToString() != string.Empty)
                            {
                                if (dr["PhoneAreaCode"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PhoneAreaCode (" + dr["PhoneAreaCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PhoneItem.PhoneAreaCode = dr["PhoneAreaCode"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PhoneItem.PhoneAreaCode = dr["PhoneAreaCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        PhoneItem.PhoneAreaCode = dr["PhoneAreaCode"].ToString();
                                    }
                                }
                                else
                                {
                                    PhoneItem.PhoneAreaCode = dr["PhoneAreaCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PhoneCountryCode"))
                        {
                            #region Validations of Ship Addr4
                            if (dr["PhoneCountryCode"].ToString() != string.Empty)
                            {
                                if (dr["PhoneCountryCode"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PhoneCountryCode (" + dr["PhoneCountryCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            PhoneItem.PhoneCountryCode = dr["PhoneCountryCode"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            PhoneItem.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                                        }
                                    }
                                    else
                                        PhoneItem.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                                }
                                else
                                {
                                    PhoneItem.PhoneCountryCode = dr["PhoneCountryCode"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (PhoneItem.PhoneType != null || PhoneItem.PhoneNumber != null || PhoneItem.PhoneAreaCode != null || PhoneItem.PhoneCountryCode != null)
                            Contact.Phones.Add(PhoneItem);

                     

                        if (dt.Columns.Contains("IsSupplier"))
                        {
                            #region Validations of IsSupplier
                            if (dr["IsSupplier"].ToString() != "<None>" || dr["IsSupplier"].ToString() != string.Empty)
                            {

                                bool result = false;
                                if (bool.TryParse(dr["IsSupplier"].ToString(), out result))
                                {
                                   Contact.IsSupplier = Convert.ToBoolean(dr["IsSupplier"].ToString().ToLower());
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsSupplier"].ToString().ToLower() == "true")
                                    {
                                        Contact.IsSupplier = Convert.ToBoolean(dr["IsSupplier"].ToString().ToLower());
                                    }
                                    else
                                    {
                                        if (dr["IsSupplier"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Contact.IsSupplier = Convert.ToBoolean(dr["IsSupplier"].ToString().ToLower());
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsSupplier (" + dr["IsSupplier"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Contact.IsSupplier = Convert.ToBoolean(dr["IsSupplier"].ToString().ToLower());
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Contact.IsSupplier = Convert.ToBoolean(dr["IsSupplier"].ToString().ToLower());
                                            }
                                        }
                                        else
                                        {
                                            Contact.IsSupplier = Convert.ToBoolean(dr["IsSupplier"].ToString().ToLower());
                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("IsCustomer"))
                        {
                            #region Validations of IsCustomer
                            if (dr["IsCustomer"].ToString() != "<None>" || dr["IsCustomer"].ToString() != string.Empty)
                            {

                                bool result = false;
                                if (bool.TryParse(dr["IsCustomer"].ToString(), out result))
                                {
                                  Contact.IsCustomer = Convert.ToBoolean(dr["IsCustomer"].ToString().ToLower());
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsCustomer"].ToString().ToLower() == "true")
                                    {
                                        Contact.IsCustomer = Convert.ToBoolean(dr["IsCustomer"].ToString().ToLower());
                                    }
                                    else
                                    {
                                        if (dr["IsCustomer"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Contact.IsCustomer = Convert.ToBoolean(dr["IsCustomer"].ToString().ToLower());                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsCustomer (" + dr["IsCustomer"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Contact.IsCustomer = Convert.ToBoolean(dr["IsCustomer"].ToString());
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Contact.IsCustomer = Convert.ToBoolean(dr["IsCustomer"].ToString());
                                            }
                                        }
                                        else
                                        {
                                            Contact.IsCustomer = Convert.ToBoolean(dr["IsCustomer"].ToString());
                                        }
                                    }

                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("DefaultCurrency"))
                        {
                            #region Validations of DefaultCurrency
                            if (dr["DefaultCurrency"].ToString() != string.Empty)
                            {
                                if (dr["DefaultCurrency"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DefaultCurrency ( " + dr["DefaultCurrency"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Contact.DefaultCurrency = dr["DefaultCurrency"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Contact.DefaultCurrency = dr["DefaultCurrency"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Contact.DefaultCurrency = dr["DefaultCurrency"].ToString();
                                    }
                                }
                                else
                                {
                                    Contact.DefaultCurrency = dr["DefaultCurrency"].ToString();
                                }
                            }
                            #endregion
                        }

                        coll.Add(Contact);

                    }
                    else
                    {
                        return null;
                    }
                }
            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return coll;
        }

        private DateTime DateTime(string p)
        {
            throw new NotImplementedException();
        }     
      } 
  }

