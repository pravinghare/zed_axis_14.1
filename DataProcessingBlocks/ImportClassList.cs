﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;


namespace DataProcessingBlocks
{
    public class ImportClassList
    {
        private static ImportClassList m_ImportClassList;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor
        public ImportClassList()
        {

        }
        #endregion

        /// <summary>
        /// Create an instance of Import class list
        /// </summary>
        /// <returns></returns>
        public static ImportClassList GetInstance()
        {
            if (m_ImportClassList == null)
                m_ImportClassList = new ImportClassList();
            return m_ImportClassList;
        }

        /// <summary>
        /// This method is used for validating import data and create class request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Class QuickBooks collection </returns>
       
        public DataProcessingBlocks.ClassQBEntryCollection ImportClassData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of Class Entry collections.
            DataProcessingBlocks.ClassQBEntryCollection coll = new ClassQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                   
                    string datevalue = string.Empty;

                    //Class Validation
                    DataProcessingBlocks.ClassQBEntry classList = new ClassQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        classList.Name = dr["Name"].ToString().Substring(0, 100);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        classList.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    classList.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                classList.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                classList.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    classList.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        classList.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            classList.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            classList.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        classList.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        #region Validations of Parent FullName
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            if (dr["ParentFullName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Parent Full Name (" + dr["ParentFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        classList.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (classList.ParentRef.FullName == null)
                                            classList.ParentRef.FullName = null;
                                        else
                                            classList.ParentRef = new ParentRef(dr["ParentFullName"].ToString().Substring(0, 100));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        classList.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (classList.ParentRef.FullName == null)
                                            classList.ParentRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    classList.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                    if (classList.ParentRef.FullName == null)
                                        classList.ParentRef.FullName = null;
                                }
                            }
                            else
                            {
                                classList.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                if (classList.ParentRef.FullName == null)
                                    classList.ParentRef.FullName = null;
                            }
                        }
                        #endregion
                    }
                   
                    coll.Add(classList);
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            string className = dr["ParentFullName"].ToString();
                            string[] arr = new string[15];
                            if (className.Contains(":"))
                            {
                                arr = className.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ParentFullName"].ToString();
                            }
                        }
                    }
                }
            }

            #endregion

            #endregion

            return coll;
        }

    }
}
