using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Specialized;
using System.Xml;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using EDI.Constant;
using System.IO;
using System.Xml.Xsl;
using System.Data;
using System.Deployment;
using System.Reflection;
using System.Configuration;


namespace DataProcessingBlocks
{
    /// <summary>
    /// This class defines user mappings of QuickBooks Fields with the data Coloumn to import.
    /// </summary>
    public class UserMappings
    {
        #region Private Members
        private string m_Name;
        private NameValueCollection m_Mappings = new NameValueCollection();
        private NameValueCollection m_ConstantMappings = new NameValueCollection();
        private NameValueCollection m_ConstantMappingsXML = new NameValueCollection();
        private static string m_settingsPath = string.Empty;
        private string m_Type = string.Empty;
        private string m_MappingType = string.Empty;
        private const string SELECTQUERY = "SELECT * FROM [{0}$]";
        DefaultAccountSettings defaultAccountSettings;
        public static string assembly_version;
        //Axis 8.0
        private NameValueCollection m_FunctionMappings = new NameValueCollection();
        #endregion

        #region Properties

        public string ImType
        {
            get { return m_Type; }
            set { m_Type = value; }
        }

        public string MappingType
        {
            get
            { return m_MappingType; }
            set
            { m_MappingType = value; }
        }

        public string Name
        {
            get { return this.m_Name; }
            set { this.m_Name = value; }
        }

        public NameValueCollection Mappings
        {
            get { return this.m_Mappings; }
            set
            {
                this.m_Mappings = value;
            }
        }

        public NameValueCollection ConstantMappings
        {
            get { return this.m_ConstantMappings; }
            set
            {
                this.m_ConstantMappings = value;
            }
        }

        public NameValueCollection ConstantMappingsXML
        {
            get { return this.m_ConstantMappingsXML; }
            set
            {
                this.m_ConstantMappingsXML = value;
            }
        }
        //Get Column and Data list in Select query when mapping is with header.
        public NameValueCollection FunctionMappings
        {
            get { return this.m_FunctionMappings; }
            set
            {
                this.m_FunctionMappings = value;
            }
        }

        #endregion

        /// <summary>
        /// Constuctor
        /// </summary>
        public UserMappings()
        {
            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
            {
                #region QuickBooks Import Types

                ImportType importType = CommonUtilities.GetInstance().SelectedImportType;
                switch (importType)
                {
                    case ImportType.JournalEntry:
                        AddJournalTag();
                        break;
                    case ImportType.Bill:
                        AddBillTag();
                        break;
                    case ImportType.BillPaymentCreditCard:
                        addBillPaymentCreditCardTag();
                        break;
                    case ImportType.BillPaymentCheck:
                        addBillPaymentCheckTag();
                        break;
                    case ImportType.Check:
                        addCheckTag();
                        break;
                    case ImportType.CreditCardCredit:
                        addCreditCardCreditTag();
                        break;
                    case ImportType.CreditCardCharge:
                        addCreditCardChargeTag();
                        break;
                    case ImportType.CreditMemo:
                        addCreditMemo();
                        break;
                    case ImportType.ReceivePayment:
                        addReceivePaymentTag();
                        break;
                    case ImportType.Deposit:
                        addDepositTag();
                        break;
                    case ImportType.Estimate:
                        addEstimateTag();
                        break;
                    case ImportType.Invoice:
                        addInvoiceTag();
                        break;
                    case ImportType.SalesOrder:
                        addSalesOrderTag();
                        break;
                    case ImportType.SalesReceipt:
                        addSalesReceiptTag();
                        break;
                    case ImportType.TimeTracking:
                        addTimeTracking();
                        break;
                    case ImportType.VendorCredits:
                        addVenderCredit();
                        break;
                    case ImportType.PriceLevel:
                        addPriceLevel();
                        break;
                    case ImportType.InventoryAdjustment:
                        AddInventoryAdjustmentTag();
                        break;
                    case ImportType.PurchaseOrder:
                        addPurchaseOrderTag();
                        break;
                    case ImportType.TransferInventory:
                        addTransferInventoryTag();
                        break;
                    case ImportType.CustomFieldList:
                        addDataExtTag();
                        break;
                    case ImportType.Account:
                        addAccountTag();
                        break;
                    case ImportType.BillingRate:
                        addBillingRateTag();
                        break;
                    case ImportType.BuildAssembly:
                        addBuildAssemblyTag();
                        break;
                    case ImportType.Charge:
                        addChargeTag();
                        break;
                    case ImportType.Class:
                        addClassTag();
                        break;
                    case ImportType.Currency:
                        addCurrencyTag();
                        break;
                    case ImportType.Customer:
                        addCustomerTag();
                        break;
                    case ImportType.CustomerMsg:
                        addCustomerMsgTag();
                        break;
                    case ImportType.CustomerType:
                        addCustomerTypeTag();
                        break;
                    case ImportType.DataExtDef:
                        addDataExtDefTag();
                        break;
                    case ImportType.DateDriventTerms:
                        addDateDrivenTermsTag();
                        break;
                    case ImportType.Employee:
                        addEmployeeTag();
                        break;
                    case ImportType.ItemDiscount:
                        addItemDiscountTag();
                        break;
                    case ImportType.ItemFixedAsset:
                        addItemFixedAssetTag();
                        break;
                    case ImportType.ItemGroup:
                        addItemGroupTag();
                        break;
                    case ImportType.ItemInventory:
                        addItemInventoryTag();
                        break;
                    case ImportType.ItemInventoryAssembly:
                        addItemInventoryAssemblyTag();
                        break;
                    case ImportType.ItemNonInventory:
                        addItemNonInventoryTag();
                        break;
                    case ImportType.ItemOtherCharge:
                        addItemOtherChargeTag();
                        break;
                    case ImportType.ItemPayment:
                        addItemPaymentTag();
                        break;
                    case ImportType.ItemReceipt:
                        addItemReceiptTag();
                        break;
                    case ImportType.ItemSalesTax:
                        addItemSalesTaxTag();
                        break;
                    case ImportType.ItemSalesTaxGroup:
                        addItemSalesTaxGroupTag();
                        break;
                    case ImportType.ItemService:
                        addItemServiceTag();
                        break;
                    case ImportType.ItemSubtotal:
                        addItemSubtotalTag();
                        break;
                    case ImportType.JobType:
                        addJobTypeTag();
                        break;
                    case ImportType.ListDisplay:
                        addListDisplayTag();
                        break;
                    case ImportType.OtherName:
                        addOtherNameTag();
                        break;
                    case ImportType.PaymentMethod:
                        addPaymentMethodTag();
                        break;
                    case ImportType.PayrollItemWage:
                        addPayrollItemWageTag();
                        break;
                    case ImportType.SalesRep:
                        addSalesRepTag();
                        break;
                    case ImportType.SalesTaxCode:
                        addSalesTaxCodeTag();
                        break;
                    case ImportType.ShipMethod:
                        addShipMethodTag();
                        break;
                    case ImportType.SpecialAccount:
                        addSpecialAccountTag();
                        break;
                    case ImportType.SpecialItem:
                        addSpecialItemTag();
                        break;
                    case ImportType.StandardTerms:
                        addStandardTermsTag();
                        break;
                    case ImportType.ToDo:
                        addToDoTag();
                        break;
                    case ImportType.TxnDisplay:
                        addTxnDisplayTag();
                        break;
                    case ImportType.UnitOfMeasureSet:
                        addUnitOfMeasureSetTag();
                        break;
                    case ImportType.Vehicle:
                        addVehicleTag();
                        break;
                    case ImportType.VehicleMileage:
                        addVehicleMileageTag();
                        break;
                    case ImportType.Vendor:
                        addVendorTag();
                        break;
                    case ImportType.VendorType:
                        addVendorTypeTag();
                        break;
                    case ImportType.WorkersCompCode:
                        addWorkersCompCodeTag();
                        break;
                    case ImportType.StatementCharges:
                        addStatementChargesTag();
                        break;
                    case ImportType.Transfer:
                        addBankTransfersTag();
                        break;
                    case ImportType.InventorySite:
                        addInventorySiteTag();
                        break;
                    case ImportType.ListMerge:
                        addListMergeTag();
                        break;
                    case ImportType.ClearedStatus:
                        addClearedStatusTag();
                        break;
                }

                #endregion
            }
           
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
            {
                #region Xero Import Types
                XeroImportType importType = CommonUtilities.GetInstance().SelectedXeroImportType;
                switch (importType)
                {
                    case XeroImportType.Contacts:
                        addContactsTag();
                        break;
                    case XeroImportType.Items:
                        addItemsTag();
                        break;
                    case XeroImportType.Journal:
                        addJournalTag();
                        break;
                    case XeroImportType.BankReceive:
                        addBankTag();
                        break;
                    case XeroImportType.BankSpend:
                        addBankTag();
                        break;
                    case XeroImportType.InvoiceSupplier:
                        addXeroInvoiceTag();
                        break;
                    case XeroImportType.InvoiceCustomer:
                        addXeroInvoiceTag();
                        break;
                }


                #endregion
            }
                // axis 11 pos
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
            {
                #region QuickBooks Import Types

                POSImportType  importType = CommonUtilities.GetInstance().SelectedPOSImportType;
                switch (importType)
                {
                    case POSImportType.Customer:
                         addPOSCustomerTag();
                         break;
                    case POSImportType.Employee:
                         addPOSEmployeeTag();
                         break;
                    case POSImportType.ItemInventory:
                         addPOSItemInventoryTag();
                         break;
                    case POSImportType.PriceDiscount:
                         addPOSPriceDiscountTag();
                         break;
                    case POSImportType.PriceAdjustment:
                         addPOSPriceAdjustmentTag();
                         break;
                    case POSImportType.CustomFieldList:
                         addPOSDataExtTag();
                         break;
                    case POSImportType.SalesOrder:
                         addPOSSalesOrderTag();
                         break;
                    case POSImportType.SalesReceipt:
                         addPOSSalesReceiptTag();
                         break;
                    case POSImportType.PurchaseOrder:
                         addPOSPurchaseOrderTag();
                         break;
                    case POSImportType.TimeEntry:
                         addPOSTimeEntryTag();
                         break;
                    case POSImportType.Voucher:
                         addPOSVoucherTag();
                         break;
                    case POSImportType.InventoryCostAdjustment:
                         addPOSInventoryCostAdjustmentTag();
                         break;
                    case POSImportType.InventoryQtyAdjustment:
                         addPOSInventoryQtyAdjustmentTag();
                         break;
                    case POSImportType.TransferSlip:
                         addPOSTransferSlipTag();
                         break;

                }

                #endregion
            }

            //axis 11.1 Online
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
            {
                #region QuickBooks Import Types

                OnlineImportType  importType = CommonUtilities.GetInstance().SelectedOnlineImportType;
                switch (importType)
                {                   
                    case OnlineImportType.SalesReceipt:
                        addOnlineSalesReceiptTag();
                        break;
                    case OnlineImportType.RefundReceipt:
                        addOnlineRefundReceiptTag();
                        break;

                    case OnlineImportType.Invoice:
                        addOnlineInvoiceTag();
                        break;

                    case OnlineImportType.Bill:
                        addOnlineBillTag();
                        break;

                    case OnlineImportType.JournalEntry:
                        addOnlineJournalEntryTag();
                        break;

                    case OnlineImportType.TimeActivity:
                        addOnlineTimeActivityTag();
                        break;   

                    case OnlineImportType.PurchaseOrder:
                        addOnlinePurchaseOrderTag();
                        break;
                    case OnlineImportType.CashPurchase:
                        addOnlinePurchaseTag();
                        break;
                    case OnlineImportType.CheckPurchase:
                        addOnlinePurchaseTag();
                        break;
                    case OnlineImportType.CreditCardPurchase:
                        addOnlinePurchaseTag();
                        break;
                    case OnlineImportType.Estimate:
                        addOnlineEstimateTag();
                        break;
	                case OnlineImportType.Payment:
                        addOnlinePaymentTag();
                        break;
                    case OnlineImportType.VendorCredit:
                        addOnlineVendorCreditTag();
                        break;
						  case OnlineImportType.BillPayment:
                       addOnlineBillPaymentTag();
                     break;
					  case OnlineImportType.CreditMemo:
                        addOnlineCreditMemoTag();
                        break;
                    case OnlineImportType.Employee:
                        addOnlineEmployeeTag();
                        break;
                    case OnlineImportType.Items:  //bug no. 404
                        AddOnlineItemsTag();
                        break;
				     case OnlineImportType.Customer:
                        addOnlineCustomerTag();
                        break;
						///Axis 11.4 Bug No 436
                    case OnlineImportType.Deposit:
                        addOnlineDepositTag();
                        break;
                    case OnlineImportType.Account:
                        addOnlineAccountTag();
                        break;
                        //592
                    case OnlineImportType.Transfer:
                        addOnlineTransferTag();
                        break;
                    //591
                    case OnlineImportType.Vendor:
                        addOnlineVendorTag();
                        break;
                    case OnlineImportType.Attachment:
                        addOnlineAttachmentTag();
                        break;

                }

                #endregion
            }


        }
        private void AddMappingFieldsToCollection(NameValueCollection MappingFields)
        {
            this.m_Mappings.Add(MappingFields);
            this.m_ConstantMappings.Add(MappingFields);
            this.m_ConstantMappingsXML.Add(MappingFields);
            this.m_FunctionMappings.Add(MappingFields);
        }
        //Improvement no 502
        private void addOnlineRefundReceiptTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);
            MappingFields.Add("TxnStatus", string.Empty);
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineSalesItemRefName", string.Empty);
            MappingFields.Add("LineSalesItemClassRef", string.Empty);
            MappingFields.Add("LineSalesItemUnitPrice", string.Empty);
            MappingFields.Add("LineSalesItemQty", string.Empty);
            MappingFields.Add("LineSalesItemTaxCodeRefValue", string.Empty);
            MappingFields.Add("LineSalesItemServiceDate", string.Empty);
            MappingFields.Add("LineDiscountAmount", string.Empty);
            MappingFields.Add("LineDiscountPercentBased", string.Empty);
            MappingFields.Add("LineDiscountPercent", string.Empty);
            MappingFields.Add("LineDiscountAccountRefName", string.Empty);
            MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            MappingFields.Add("CustomerFullyQualifiedName", string.Empty);
            MappingFields.Add("CustomerMemo", string.Empty);
            MappingFields.Add("BillAddrLine1", string.Empty);
            MappingFields.Add("BillAddrLine2", string.Empty);
            MappingFields.Add("BillAddrLine3", string.Empty);
            MappingFields.Add("BillAddrCity", string.Empty);
            MappingFields.Add("BillAddrCountry", string.Empty);
            MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
            MappingFields.Add("BillAddrPostalCode", string.Empty);
            MappingFields.Add("BillAddrNote", string.Empty);
            MappingFields.Add("BillAddrLine4", string.Empty);
            MappingFields.Add("BillAddrLine5", string.Empty);
            MappingFields.Add("BillAddrLat", string.Empty);
            MappingFields.Add("BillAddrLong", string.Empty);
            MappingFields.Add("ShipAddrLine1", string.Empty);
            MappingFields.Add("ShipAddrLine2", string.Empty);
            MappingFields.Add("ShipAddrLine3", string.Empty);
            MappingFields.Add("ShipAddrCity", string.Empty);
            MappingFields.Add("ShipAddrCountry", string.Empty);
            MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
            MappingFields.Add("ShipAddrPostalCode", string.Empty);
            MappingFields.Add("ShipAddrNote", string.Empty);
            MappingFields.Add("ShipAddrLine4", string.Empty);
            MappingFields.Add("ShipAddrLine5", string.Empty);
            MappingFields.Add("ShipAddrLat", string.Empty);
            MappingFields.Add("ShipAddrLong", string.Empty);
            MappingFields.Add("ClassRef", string.Empty);
            MappingFields.Add("ShipMethodRef", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("TrackingNum", string.Empty);
            MappingFields.Add("PrintStatus", string.Empty);
            MappingFields.Add("EmailStatus", string.Empty);
            MappingFields.Add("BillEmail", string.Empty);
            MappingFields.Add("Shipping", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("ShippingTaxCode", string.Empty);
            }
            MappingFields.Add("PaymentRefNumber", string.Empty);
            MappingFields.Add("PaymentMethodRef", string.Empty);
            MappingFields.Add("DepositToAccountRef", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("ApplyTaxAfterDiscount", string.Empty);
            }
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }

            AddMappingFieldsToCollection(MappingFields);

        }

        #region QuickBooks Transactions Import Types

        //bug 477
        private void addInventorySiteTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //user mappings
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentSiteRefFullName", string.Empty);
            MappingFields.Add("SiteDesc", string.Empty);
            MappingFields.Add("Contact", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("SiteAddr1", string.Empty);
            MappingFields.Add("SiteAddr2", string.Empty);
            MappingFields.Add("SiteAddr3", string.Empty);
            MappingFields.Add("SiteAddr4", string.Empty);
            MappingFields.Add("SiteAddr5", string.Empty);
            MappingFields.Add("SiteCity", string.Empty);
            MappingFields.Add("SiteState", string.Empty);
            MappingFields.Add("SitePostalCode", string.Empty);
            MappingFields.Add("SiteCountry", string.Empty);

            AddMappingFieldsToCollection(MappingFields);



        }

        private void addListMergeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //user mappings
            MappingFields.Add("ListMergeType", string.Empty);
            MappingFields.Add("MergeFrom", string.Empty);
            MappingFields.Add("MergeTo", string.Empty);
            MappingFields.Add("SameShipAddrOk", string.Empty);


            AddMappingFieldsToCollection(MappingFields);

        }

        private void addClearedStatusTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //user mappings
            MappingFields.Add("TxnID", string.Empty);
            MappingFields.Add("TxnLineID", string.Empty);
            MappingFields.Add("ClearedStatus", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private NameValueCollection AddressTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("BillAddr1", string.Empty);
            MappingFields.Add("BillAddr2", string.Empty);
            MappingFields.Add("BillAddr3", string.Empty);
            MappingFields.Add("BillAddr4", string.Empty);
            MappingFields.Add("BillAddr5", string.Empty);
            MappingFields.Add("BillCity", string.Empty);
            MappingFields.Add("BillState", string.Empty);
            MappingFields.Add("BillPostalCode", string.Empty);
            MappingFields.Add("BillCountry", string.Empty);
            MappingFields.Add("BillNote", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);

            MappingFields.Add("ShipAddr1", string.Empty);
            MappingFields.Add("ShipAddr2", string.Empty);
            MappingFields.Add("ShipAddr3", string.Empty);
            MappingFields.Add("ShipAddr4", string.Empty);
            MappingFields.Add("ShipAddr5", string.Empty);
            MappingFields.Add("ShipCity", string.Empty);
            MappingFields.Add("ShipState", string.Empty);
            MappingFields.Add("ShipPostalCode", string.Empty);
            MappingFields.Add("ShipCountry", string.Empty);
            MappingFields.Add("ShipNote", string.Empty);
            return MappingFields;
        }
        

        private NameValueCollection PurchaseOrderAddressTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("VendorAddr1", string.Empty);
            MappingFields.Add("VendorAddr2", string.Empty);
            MappingFields.Add("VendorAddr3", string.Empty);
            MappingFields.Add("VendorAddr4", string.Empty);
            MappingFields.Add("VendorAddr5", string.Empty);
            MappingFields.Add("VendorCity", string.Empty);
            MappingFields.Add("VendorState", string.Empty);
            MappingFields.Add("VendorPostalCode", string.Empty);
            MappingFields.Add("VendorCountry", string.Empty);
            MappingFields.Add("VendorNote", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);

            MappingFields.Add("ShipAddr1", string.Empty);
            MappingFields.Add("ShipAddr2", string.Empty);
            MappingFields.Add("ShipAddr3", string.Empty);
            MappingFields.Add("ShipAddr4", string.Empty);
            MappingFields.Add("ShipAddr5", string.Empty);
            MappingFields.Add("ShipCity", string.Empty);
            MappingFields.Add("ShipState", string.Empty);
            MappingFields.Add("ShipPostalCode", string.Empty);
            MappingFields.Add("ShipCountry", string.Empty);
            MappingFields.Add("ShipNote", string.Empty);
            return MappingFields;
        }

        private void addDepositTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("TxnID", string.Empty);           
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("DepositToAccountRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("CashBackAccountRefFullName", string.Empty);
            MappingFields.Add("CashBackMemo", string.Empty);
            MappingFields.Add("CashBackAmount", string.Empty);
            MappingFields.Add("CurrencyRefFullName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("AccountRefFullName", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("EntityRefFullName", string.Empty);
            MappingFields.Add("CheckNumber", string.Empty);
            MappingFields.Add("PaymentMethodRefFullName", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("DepositLineMemo", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addInvoiceTag()
        {
            //user mapping
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("CustomerRefFullName", string.Empty);
            MappingFields.Add("Currency", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("AccountRef", string.Empty);
            MappingFields.Add("TemplateRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("InvoiceRefNumber", string.Empty);
            //add apply to invoiceref tag

            MappingFields.Add(this.AddressTag());

            MappingFields.Add("IsPending", string.Empty);
            MappingFields.Add("IsFinanceCharge", string.Empty);
            MappingFields.Add("ItemSalesTaxRefFullName", string.Empty);
            MappingFields.Add("ShipMethodRefFullName", string.Empty);
            MappingFields.Add("PONumber", string.Empty);
            MappingFields.Add("TermsRefFullName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            MappingFields.Add("FOB", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("CustomerMsgRefFullName", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("IsToBeEmailed", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("CustomerSalesTaxCodeRefFullName", string.Empty);
            MappingFields.Add("Other", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);


            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("TxnID", string.Empty);
            MappingFields.Add("TxnLineID", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("InvoiceLineClassFullName", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            //New Feature:: 601
            MappingFields.Add("PriceLevelRefFullName", string.Empty);

            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            MappingFields.Add("ServiceDate", string.Empty);
            MappingFields.Add("SalesTaxCodeRefFullName", string.Empty);
            //Axis 10.0
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
            {
                MappingFields.Add("IsTaxable", string.Empty);
            }
            MappingFields.Add("Other1", string.Empty);
            MappingFields.Add("Other2", string.Empty);


            //Axis bug no. 331          
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);

            ///bug 443
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);  
            //End Axis bug no. 331         

            //new changes in invoice FOR 6.0
            MappingFields.Add("ItemGroupFullName", string.Empty);
            MappingFields.Add("ItemGroupQuantity", string.Empty);
            MappingFields.Add("ItemGroupUOM", string.Empty);

            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("SalesTax", string.Empty); //bug no. 410
            //Axis 10.0
            MappingFields.Add("LinkToSalesOrder", string.Empty);
            //Axis 10.2
            MappingFields.Add("InventorySiteLocationRef", string.Empty);

            //Axis bug no. 331    
            MappingFields.Add("ItemGroupCustomFieldName1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName2", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue2", string.Empty);

            ///443
            MappingFields.Add("ItemGroupCustomFieldName3", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue3", string.Empty);
            //End Axis bug no. 331

            //Axis 10.0

            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
            {
                MappingFields.Add("DiscountLineAmount", string.Empty);
                MappingFields.Add("DiscountLineIsTaxable", string.Empty);
                MappingFields.Add("DiscountLineAccountRefFullName", string.Empty);
                MappingFields.Add("ShippingLineAmount", string.Empty);
                MappingFields.Add("ShippingLineAccountRefFullName", string.Empty);

            }
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                MappingFields.Add("SerialNumber", string.Empty);
                MappingFields.Add("LotNumber", string.Empty);
            }

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCreditMemo()
        {
            //user mappings
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("CustomerRefFullName", string.Empty);
            ///11.4 bug 442
            MappingFields.Add("Currency", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ARAccountRefFullName", string.Empty);
            MappingFields.Add("TemplateRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("CreditMemoRefNumber", string.Empty);
            this.AddressTag();

            MappingFields.Add("IsPending", string.Empty);

            MappingFields.Add("PONumber", string.Empty);
            MappingFields.Add("TermsRefFullName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            MappingFields.Add("FOB", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("ShipMethodRefFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);

            MappingFields.Add("CustomerMsgRefFullName", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("IsToBeEmailed", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);

            MappingFields.Add("CustomerSalesTaxCodeRefFullName", string.Empty);
            //Axis Bug NO.233
            MappingFields.Add("Other", string.Empty);
            MappingFields.Add("Other1", string.Empty);
            MappingFields.Add("Other2", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);

            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);

            //New Feature:: 601
            MappingFields.Add("PriceLevelRefFullName", string.Empty);
            //Axis 723
            MappingFields.Add("LineClassRef", string.Empty);
            //Axis 723 ends

            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("SalesTaxCodeRefFullName", string.Empty);            
            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("SalesTax", string.Empty); //bug no. 410

            //Axis bug no. 331          
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);

            ///bug 443
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);
            //End Axis bug no. 331             


            // Axis 10.0
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
            {
                MappingFields.Add("DiscountLineAmount", string.Empty);
                MappingFields.Add("DiscountLineIsTaxable", string.Empty);
                MappingFields.Add("DiscountLineAccountRefFullName", string.Empty);
                MappingFields.Add("ShippingLineAmount", string.Empty);
                MappingFields.Add("ShippingLineAccountRefFullName", string.Empty);
            }
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addReceivePaymentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("CustomerRefFullName", string.Empty);
            MappingFields.Add("ARAccountRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("ApplyToInvoiceRef", string.Empty);
            MappingFields.Add("TotalAmount", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("PaymentMethodRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("DepositToAccountRefFullName", string.Empty);
            MappingFields.Add("IsAutoApply", string.Empty);
            MappingFields.Add("AppliedToId", string.Empty);
            MappingFields.Add("PaymentAmount", string.Empty);
            MappingFields.Add("DiscountAmount", string.Empty);
            MappingFields.Add("DiscountAccountRefFullName", string.Empty);
            MappingFields.Add("SetCreditMemoRef", string.Empty);
            MappingFields.Add("SetCreditAmount", string.Empty);
            MappingFields.Add("SetCreditOverride", string.Empty);
            MappingFields.Add("DiscountClassRefFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addEstimateTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("CustomerRefFullName", string.Empty);
            ///11.4 bug 442
            MappingFields.Add("Currency", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("TemplateRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("EstimateRefNumber", string.Empty);
            this.AddressTag();
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("PONumber", string.Empty);

            MappingFields.Add("TermsRefFullName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            MappingFields.Add("FOB", string.Empty);
            MappingFields.Add("ItemSalesTaxRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("CustomerMsgRefFullName", string.Empty);
            MappingFields.Add("IsToBeEmailed", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("CustomerSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);

            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("EstimateUnitOfMeasure", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("EstLineClassRefFullName", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("MarkupRate", string.Empty);
            MappingFields.Add("MarkupRatePercent", string.Empty);

            //New Feature:: 601
            MappingFields.Add("PriceLevelRefFullName", string.Empty);
            //Axis bug no. 331          
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);

            ///bug 443
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);
            //End Axis bug no. 331         

            //new changes in Estimate FOR 10.2(Axis -92)
            MappingFields.Add("ItemGroupFullName", string.Empty);
            MappingFields.Add("ItemGroupQuantity", string.Empty);
            MappingFields.Add("ItemGroupUOM", string.Empty);
            MappingFields.Add("ItemGroupInventorySiteFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRefFullName", string.Empty);

            ///Bug no 443
            MappingFields.Add("ItemGroupCustomFieldName1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName2", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue2", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName3", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue3", string.Empty);
            //End Changes.

            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("SalesTax", string.Empty); //bug no. 410
            MappingFields.Add("Other", string.Empty);
            MappingFields.Add("Other1", string.Empty);
            MappingFields.Add("Other2", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addSalesOrderTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("CustomerRefFullName", string.Empty);
            ///11.4 bug 442
            MappingFields.Add("Currency", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("TemplateRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("SalesOrderRefNumber", string.Empty);
            MappingFields.Add("TermsRefFullName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add(this.AddressTag());
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxRefFullName", string.Empty);
            MappingFields.Add("FOB", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("ShipMethodRefFullName", string.Empty);
            MappingFields.Add("PONumber", string.Empty);
            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("SalesOrderLineClassName", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            //601
            MappingFields.Add("PriceLevelRefFullName", string.Empty);
            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("SalesTaxCodeRefFullName", string.Empty);
            MappingFields.Add("IsManuallyClosed", string.Empty);
           
            MappingFields.Add("CustomerMsgRefFullName", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("IsToBeEmailed", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            //Axis Bug no. #96
            MappingFields.Add("Other", string.Empty);
            //End Axis Bug no. #96
            MappingFields.Add("CustomerSalesTaxCodeRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("Other1", string.Empty);
            MappingFields.Add("Other2", string.Empty);

            //Axis bug no. 331          
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);

            ///bug 443
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);
            //End Axis bug no. 331         

            //add Freight, Insurance, discount.
            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("SalesTax", string.Empty); //bug no. 410
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);         

            //Axis 10.2
            MappingFields.Add("ItemGroupFullName", string.Empty);
            MappingFields.Add("ItemGroupQuantity", string.Empty);
            MappingFields.Add("ItemGroupUoM", string.Empty);
            MappingFields.Add("ItemGroupInventorySiteFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRefFullName", string.Empty);
            //Axis bug no. 331          
            MappingFields.Add("ItemGroupCustomFieldName1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName2", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue2", string.Empty);
            ///
            MappingFields.Add("ItemGroupCustomFieldName3", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue3", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addSalesReceiptTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            //user mappings
            MappingFields.Add("CustomerRefFullName", string.Empty);
            ///11.4 bug 442
            MappingFields.Add("Currency", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("TemplateRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("SalesReceiptRefNumber", string.Empty);
            MappingFields.Add(this.AddressTag());

            MappingFields.Add("IsPending", string.Empty);
            MappingFields.Add("CheckNumber", string.Empty);
            MappingFields.Add("PaymentMethodRefFullName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("ShipMethodRefFullName", string.Empty);
            MappingFields.Add("FOB", string.Empty);
            MappingFields.Add("ItemSalesTaxRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("CustomerMsgRefFullName", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("IsToBeEmailed", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("CustomerSalesTaxCodeRefFullName", string.Empty);
            MappingFields.Add("DepositToAccountRefFullName", string.Empty);
            MappingFields.Add("Other", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("Other1", string.Empty);
            MappingFields.Add("Other2", string.Empty);
            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("SalesReceiptLineClassName", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("PriceLevelRefFullName", string.Empty);

            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);
            MappingFields.Add("SalesTaxCodeRefFullName", string.Empty);

            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);

            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);
            MappingFields.Add("ItemGroupFullName", string.Empty);
            MappingFields.Add("ItemGroupQuantity", string.Empty);
            MappingFields.Add("ItemGroupUoM", string.Empty);
            MappingFields.Add("ItemGroupInventorySiteFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRefFullName", string.Empty);
            MappingFields.Add("ItemGroupSerialNumber", string.Empty);
            MappingFields.Add("ItemGroupLotNumber", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue1", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName2", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue2", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldName3", string.Empty);
            MappingFields.Add("ItemGroupCustomFieldValue3", string.Empty);  

            //Axis 10.0
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
            {
                MappingFields.Add("IsTaxable", string.Empty);
            }
            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("SalesTax", string.Empty); //bug no. 410
            //Axis 10.0
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
            {
                MappingFields.Add("DiscountLineAmount", string.Empty);
                //MappingFields.Add("DiscountLineRatePercent", string.Empty);
                MappingFields.Add("DiscountLineIsTaxable", string.Empty);
                MappingFields.Add("DiscountLineAccountRefFullName", string.Empty);
                MappingFields.Add("ShippingLineAmount", string.Empty);
                MappingFields.Add("ShippingLineAccountRefFullName", string.Empty);
            }

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addTimeTracking()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("TxnID", string.Empty);
           
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("EntityFullName", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("ItemServiceFullName", string.Empty);
            MappingFields.Add("Duration", string.Empty);
            MappingFields.Add("ClassFullName", string.Empty);
            MappingFields.Add("PayrollItemWageFullName", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("BillableStatus", string.Empty);
            MappingFields.Add("IsBillable", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addVenderCredit()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("VendorFullName", string.Empty);
            MappingFields.Add("APAccountFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("VendorCreditSalesTaxCodeName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("AccountFullName", string.Empty);
            MappingFields.Add("ExpenseAmount", string.Empty);
            MappingFields.Add("ExpenseMemo", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("ClassFullName", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("BillableStatus", string.Empty);

            //Improvement::548
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            //MappingFields.Add("OwnerID1", string.Empty);
            MappingFields.Add("DataExtName1", string.Empty);
            MappingFields.Add("DataExtValue1", string.Empty);
            //MappingFields.Add("OwnerID2", string.Empty);
            MappingFields.Add("DataExtName2", string.Empty);
            MappingFields.Add("DataExtValue2", string.Empty);

            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("InventorySiteFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("ItemAmount", string.Empty);
            MappingFields.Add("ItemCustomerFullName", string.Empty);
            MappingFields.Add("ItemClassFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemBillableStatus", string.Empty);
            
            //Improvement::548
            MappingFields.Add("ItemSalesRepRefFullName", string.Empty);
            //MappingFields.Add("ItemOwnerID1", string.Empty);
            MappingFields.Add("ItemDataExtName1", string.Empty);
            MappingFields.Add("ItemDataExtValue1", string.Empty);
            //MappingFields.Add("ItemOwnerID2", string.Empty);
            MappingFields.Add("ItemDataExtName2", string.Empty);
            MappingFields.Add("ItemDataExtValue2", string.Empty);

            MappingFields.Add("OverrideItemAccountFullName", string.Empty);
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCreditCardChargeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("AccountFullName", string.Empty);
            MappingFields.Add("PayeeEntityFullName", string.Empty);
            //Axis 617
            MappingFields.Add("Currency", string.Empty);
            //Axis 617 ends
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            //MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);

            MappingFields.Add("ExpenseAccountFullName", string.Empty);
            MappingFields.Add("ExpenseAmount", string.Empty);
            MappingFields.Add("ExpenseMemo", string.Empty);
            MappingFields.Add("ExpenseCustomerFullName", string.Empty);
            MappingFields.Add("ExpenseClassFullName", string.Empty);
            MappingFields.Add("ExpenseSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ExpenseBillableStatus", string.Empty);

            //Improvement::548
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            //MappingFields.Add("OwnerID1", string.Empty);
            MappingFields.Add("DataExtName1", string.Empty);
            MappingFields.Add("DataExtValue1", string.Empty);
            //MappingFields.Add("OwnerID2", string.Empty);
            MappingFields.Add("DataExtName2", string.Empty);
            MappingFields.Add("DataExtValue2", string.Empty);

            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("InventorySiteFullName", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("ItemAmount", string.Empty);
            MappingFields.Add("ItemCustomerFullName", string.Empty);
            MappingFields.Add("ItemClassFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemBillableStatus", string.Empty);
            MappingFields.Add("OverrideItemAccountFullName", string.Empty);
            
            //Improvement::548
            MappingFields.Add("ItemSalesRepRefFullName", string.Empty);
            //MappingFields.Add("ItemOwnerID1", string.Empty);
            MappingFields.Add("ItemDataExtName1", string.Empty);
            MappingFields.Add("ItemDataExtValue1", string.Empty);
            //MappingFields.Add("ItemOwnerID2", string.Empty);
            MappingFields.Add("ItemDataExtName2", string.Empty);
            MappingFields.Add("ItemDataExtValue2", string.Empty);

            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);

            //Adding constant values into mappings for text file
            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCheckTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("AccountFullName", string.Empty);
            MappingFields.Add("PayeeEntityFullName", string.Empty);
            //Axis 617 
            MappingFields.Add("Currency", string.Empty);
            //Axis 617 ends
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("Addr1", string.Empty);
            MappingFields.Add("Addr2", string.Empty);
            MappingFields.Add("Addr3", string.Empty);
            MappingFields.Add("Addr4", string.Empty);
            MappingFields.Add("Addr5", string.Empty);
            MappingFields.Add("City", string.Empty);
            MappingFields.Add("State", string.Empty);
            MappingFields.Add("PostalCode", string.Empty);
            MappingFields.Add("Country", string.Empty);
            MappingFields.Add("Note", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
           
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("ApplyCheckToTxnID", string.Empty);
            MappingFields.Add("ApplyCheckToTxnAmount", string.Empty);

            MappingFields.Add("ExpenseAccountFullName", string.Empty);
            MappingFields.Add("ExpenseAmount", string.Empty);
            MappingFields.Add("ExpenseMemo", string.Empty);
            MappingFields.Add("ExpenseCustomerFullName", string.Empty);
            MappingFields.Add("ExpenseClassFullName", string.Empty);
            MappingFields.Add("ExpenseSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ExpenseBillableStatus", string.Empty);
            
            //Improvement::548
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            //Improvement::548
            //MappingFields.Add("OwnerID1", string.Empty);
            MappingFields.Add("DataExtName1", string.Empty);
            MappingFields.Add("DataExtValue1", string.Empty);
            //MappingFields.Add("OwnerID2", string.Empty);
            MappingFields.Add("DataExtName2", string.Empty);
            MappingFields.Add("DataExtValue2", string.Empty);

            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("ItemInventorySiteFullName", string.Empty);
            MappingFields.Add("ItemInventorySiteLocationFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("ItemAmount", string.Empty);
            MappingFields.Add("ItemCustomerFullName", string.Empty);
            MappingFields.Add("ItemClassFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemBillableStatus", string.Empty);
            
            //Improvement:: 548
            MappingFields.Add("ItemSalesRepRefFullName", string.Empty);
            //MappingFields.Add("ItemOwnerID1", string.Empty);
            MappingFields.Add("ItemDataExtName1", string.Empty);
            MappingFields.Add("ItemDataExtValue1", string.Empty);
            //MappingFields.Add("itemOwnerID2", string.Empty);
            MappingFields.Add("ItemDataExtName2", string.Empty);
            MappingFields.Add("ItemDataExtValue2", string.Empty);

            MappingFields.Add("OverrideItemAccountFullName", string.Empty);
            MappingFields.Add("Max", string.Empty);

            //Axis 10.0
            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "AU" || CommonUtilities.GetInstance().CountryVersion == "UK" || CommonUtilities.GetInstance().CountryVersion == "CA")
            {
                MappingFields.Add("SalesTax", string.Empty); //bug no. 410
            }
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCreditCardCreditTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("AccountFullName", string.Empty);
            MappingFields.Add("PayeeEntityFullName", string.Empty);
            //Axis 617
            MappingFields.Add("Currency", string.Empty);
            //Axis 617 ends
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("SalesTaxCodeRefFullName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);

            MappingFields.Add("ExpenseAccountFullName", string.Empty);
            MappingFields.Add("ExpenseAmount", string.Empty);
            MappingFields.Add("ExpenseMemo", string.Empty);
            MappingFields.Add("ExpenseCustomerFullName", string.Empty);
            MappingFields.Add("ExpenseClassFullName", string.Empty);
            MappingFields.Add("ExpenseSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ExpenseBillableStatus", string.Empty);

            //Improvement::548
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            //MappingFields.Add("OwnerID1", string.Empty);
            MappingFields.Add("DataExtName1", string.Empty);
            MappingFields.Add("DataExtValue1", string.Empty);
            //MappingFields.Add("OwnerID2", string.Empty);
            MappingFields.Add("DataExtName2", string.Empty);
            MappingFields.Add("DataExtValue2", string.Empty);

            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("ItemAmount", string.Empty);
            MappingFields.Add("ItemCustomerFullName", string.Empty);
            MappingFields.Add("ItemClassFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemBillableStatus", string.Empty);
            MappingFields.Add("OverrideItemAccountFullName", string.Empty);
               
            //Improvement::548
            MappingFields.Add("ItemSalesRepRefFullName", string.Empty);
            //MappingFields.Add("ItemOwnerID1", string.Empty);
            MappingFields.Add("ItemDataExtName1", string.Empty);
            MappingFields.Add("ItemDataExtValue1", string.Empty);
            //MappingFields.Add("ItemOwnerID2", string.Empty);
            MappingFields.Add("ItemDataExtName2", string.Empty);
            MappingFields.Add("ItemDataExtValue2", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addBillPaymentCreditCardTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("PayeeEntityRefFullName", string.Empty);
            MappingFields.Add("APAccountRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("CreditCardAccountRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("ApplyToBillRef", string.Empty);
            MappingFields.Add("PaymentAmount", string.Empty);
            MappingFields.Add("DiscountAmount", string.Empty);
            MappingFields.Add("DiscountAccountRefFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addBillPaymentCheckTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("PayeeEntityRefFullName", string.Empty);
            MappingFields.Add("APAccountRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("BankAccountRefFullName", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);

            //new changes version 6.0
            MappingFields.Add("ApplyToBillRef", string.Empty);

            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            //changes in version 6.0
            MappingFields.Add("AppliedToTxnID", string.Empty);
            MappingFields.Add("PaymentAmount", string.Empty);
            MappingFields.Add("DiscountAmount", string.Empty);
            MappingFields.Add("DiscountAccountRefFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void AddBillTag()
        {

         NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("VendorFullName", string.Empty);
            //Axis 617 
            MappingFields.Add("Currency", string.Empty);
            //Axis 617 ends 
            MappingFields.Add("APAccountFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("TermsFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("LinkToTxnID", string.Empty);
            MappingFields.Add("AccountFullName", string.Empty);
            MappingFields.Add("ExpenseAmount", string.Empty);
            MappingFields.Add("ExpenseMemo", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("ClassFullName", string.Empty);
            MappingFields.Add("ExpenseSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("BillableStatus", string.Empty);
            //Improvement::548
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            //Improvement::548
            //BillMappingFields.Add("OwnerID1", string.Empty);
            MappingFields.Add("DataExtName1", string.Empty);
            MappingFields.Add("DataExtValue1", string.Empty);
            //BillMappingFields.Add("OwnerID2", string.Empty);
            MappingFields.Add("DataExtName2", string.Empty);
            MappingFields.Add("DataExtValue2", string.Empty);


            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("ItemInventorySiteFullName", string.Empty);
            MappingFields.Add("ItemInventorySiteLocationFullName", string.Empty);
            MappingFields.Add("TxnID", string.Empty);
            MappingFields.Add("TxnLineID", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("ItemAmount", string.Empty);
            MappingFields.Add("ItemCustomerFullName", string.Empty);
            MappingFields.Add("ItemClassFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemBillableStatus", string.Empty);
            MappingFields.Add("ItemSalesRepRefFullName", string.Empty);
            MappingFields.Add("ItemDataExtName1", string.Empty);
            MappingFields.Add("ItemDataExtValue1", string.Empty);
            MappingFields.Add("ItemDataExtName2", string.Empty);
            MappingFields.Add("ItemDataExtValue2", string.Empty);

            MappingFields.Add("OverrideItemAccountFullName", string.Empty);

            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("Insurance", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "AU" || CommonUtilities.GetInstance().CountryVersion == "UK" || CommonUtilities.GetInstance().CountryVersion == "CA")
            {
                MappingFields.Add("SalesTax", string.Empty); //bug no. 410
            }
            MappingFields.Add("LinkToPurchaseOrder", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void AddJournalTag()
        {

            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("IsAdjustment", string.Empty);
            MappingFields.Add("IsHomeCurrencyAdjustment", string.Empty);
            MappingFields.Add("IsAmountsEnteredInHomeCurrency", string.Empty);
            MappingFields.Add("CurrencyFullName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("Debit", string.Empty);
            MappingFields.Add("Credit", string.Empty);
            MappingFields.Add("AccountFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("EntityRefFullName", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxRefFullName", string.Empty);
            MappingFields.Add("BillableStatus", string.Empty);
            MappingFields.Add("IsBillable", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPriceLevel()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("CustomPrice", string.Empty);        
            MappingFields.Add("AdjustPercentage", string.Empty);
            MappingFields.Add("AdjustRelativeTo", string.Empty);
            MappingFields.Add("CurrencyRefFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void AddInventoryAdjustmentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("AccountRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            
            MappingFields.Add("CustomerRefFullName", string.Empty);
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("QuantityAdjNewQuantity", string.Empty);
            MappingFields.Add("QuantityAdjDifference", string.Empty);
            MappingFields.Add("QuantityAdjSerialNumber", string.Empty);
            MappingFields.Add("QuantityAdjLotNumber", string.Empty);
            MappingFields.Add("QuantityAdjInventorySiteLocationRef", string.Empty);


            MappingFields.Add("ValueAdjNewQuantity", string.Empty);
            MappingFields.Add("ValueAdjQuantityDifference", string.Empty);
            MappingFields.Add("ValueAdjNewValue", string.Empty);
            MappingFields.Add("ValueAdjValueDifference", string.Empty);

            MappingFields.Add("SerialNumberAdjAddSerialNumber", string.Empty);
            MappingFields.Add("SerialNumberAdjRemoveSerialNumber", string.Empty);
            MappingFields.Add("SerialNumberAdjInventorySiteLocationRef", string.Empty);


            MappingFields.Add("LotNumberAdjLotNumber", string.Empty);
            MappingFields.Add("LotNumberAdjCountAdjustment", string.Empty);
            MappingFields.Add("LotNumberAdjInventorySiteLocationRef", string.Empty);


            AddMappingFieldsToCollection(MappingFields);
        }

        private void addAccountTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("AccountType", string.Empty);
            MappingFields.Add("AccountNumber", string.Empty);
            MappingFields.Add("BankNumber", string.Empty);
            MappingFields.Add("Desc", string.Empty);
            MappingFields.Add("OpenBalance", string.Empty);
            MappingFields.Add("OpenBalanceDate", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("TaxLineID", string.Empty);
            MappingFields.Add("CurrencyFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addBillingRateTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("BillingRateName", string.Empty);
            MappingFields.Add("FixedBillingRate", string.Empty);
            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("CustomRate", string.Empty);
            MappingFields.Add("CustomRatePercent", string.Empty);
            MappingFields.Add("AdjustPercentage", string.Empty);
            MappingFields.Add("AdjustBillingRateRelativeTo", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        //402
        private void addBuildAssemblyTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("ItemInventoryAssemblyRef", string.Empty);
            MappingFields.Add("InventorySiteRef", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("QuantityToBuild", string.Empty);
            MappingFields.Add("MarkPendingIfRequired", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCustomerMsgTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCustomerTypeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }
  

        private void addDataExtDefTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("OwnerID", string.Empty);
            MappingFields.Add("DataExtName", string.Empty);
            MappingFields.Add("DataExtType", string.Empty);
            MappingFields.Add("AssignToObject", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addWorkersCompCodeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("Desc", string.Empty);
            MappingFields.Add("RateEntryRate", string.Empty);
            MappingFields.Add("RateEntryEffectiveDate", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        //Axis 13 Changes

        private void addBankTransfersTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("TransferFromAccountRef", string.Empty);
            MappingFields.Add("TransferToAccountRef", string.Empty);
            MappingFields.Add("ClassRef", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("Memo", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addStatementChargesTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("InventorySiteFullName", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("Desc", string.Empty);
            MappingFields.Add("ARAccountFullName", string.Empty);
            MappingFields.Add("ClassFullName", string.Empty);
            MappingFields.Add("BilledDate", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("OverrideItemAccountFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addVendorTypeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addVendorTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("CompanyName", string.Empty);
            MappingFields.Add("Salutation", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("MiddleName", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("VendorAddr1", string.Empty);
            MappingFields.Add("VendorAddr2", string.Empty);
            MappingFields.Add("VendorAddr3", string.Empty);
            MappingFields.Add("VendorAddr4", string.Empty);
            MappingFields.Add("VendorAddr5", string.Empty);
            MappingFields.Add("VendorCity", string.Empty);
            MappingFields.Add("VendorState", string.Empty);
            MappingFields.Add("VendorPostalCode", string.Empty);
            MappingFields.Add("VendorCountry", string.Empty);
            MappingFields.Add("VendorNote", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("AltPhone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("Contact", string.Empty);
            MappingFields.Add("AltContact", string.Empty);
            MappingFields.Add("NameOnCheck", string.Empty);
            MappingFields.Add("AccountNumber", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("VendorTypeFullName", string.Empty);
            MappingFields.Add("TermsFullName", string.Empty);
            MappingFields.Add("CreditLimit", string.Empty);
            MappingFields.Add("VendorTaxIdent", string.Empty);
            MappingFields.Add("IsVendorEligibleFor1099", string.Empty);
            MappingFields.Add("OpenBalance", string.Empty);
            MappingFields.Add("OpenBalanceDate", string.Empty);
            MappingFields.Add("BillingRateFullName", string.Empty);
            MappingFields.Add("ExternalGUID", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesTaxCountry", string.Empty);
            MappingFields.Add("IsSalesTaxAgency", string.Empty);
            MappingFields.Add("SalesTaxReturnFullName", string.Empty);
            MappingFields.Add("TaxRegistrationNumber", string.Empty);
            MappingFields.Add("ReportingPeriod", string.Empty);
            MappingFields.Add("IsTaxTrackedOnPurchases", string.Empty);
            MappingFields.Add("TaxOnPurchasesAccountFullName", string.Empty);
            MappingFields.Add("IsTaxTrackedOnSales", string.Empty);
            MappingFields.Add("TaxOnSalesAccountFullName", string.Empty);
            MappingFields.Add("IsTaxOnTax", string.Empty);
            MappingFields.Add("PrefillAccountFullName", string.Empty);
            MappingFields.Add("CurrencyFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addVehicleMileageTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("VehicleFullName", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("ClassFullName", string.Empty);
            MappingFields.Add("TripStartDate", string.Empty);
            MappingFields.Add("TripEndDate", string.Empty);
            MappingFields.Add("OdometerStart", string.Empty);
            MappingFields.Add("OdometerEnd", string.Empty);
            MappingFields.Add("TotalMiles", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("BillableStatus", string.Empty);


            AddMappingFieldsToCollection(MappingFields);
        }


        private void addVehicleTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("Desc", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addUnitOfMeasureSetTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("UnitOfMeasureType", string.Empty);
            MappingFields.Add("BaseUnitName", string.Empty);
            MappingFields.Add("BaseUnitAbbreviation", string.Empty);
            MappingFields.Add("RelatedUnitName", string.Empty);
            MappingFields.Add("RelatedUnitAbbreviation", string.Empty);
            MappingFields.Add("RelatedUnitConversionRatio", string.Empty);
            MappingFields.Add("DefaultUnitUnitUsedFor", string.Empty);
            MappingFields.Add("DefaultUnitUnit", string.Empty);

            //Adding Constants values ot mapping for text file
            AddMappingFieldsToCollection(MappingFields);
        }

        private void addTxnDisplayTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("TxnDisplayAddType", string.Empty);
            MappingFields.Add("EntityFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addToDoTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("IsDone", string.Empty);
            MappingFields.Add("ReminderDate", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }


        private void addStandardTermsTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("StdDueDays", string.Empty);
            MappingFields.Add("StdDiscountDays", string.Empty);
            MappingFields.Add("DiscountPct", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addSpecialItemTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("SpecialItemType", string.Empty);
            MappingFields.Add("ExternalGUID", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }


        private void addSpecialAccountTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("SpecialAccountType", string.Empty);
            MappingFields.Add("CurrencyFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addShipMethodTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addSalesTaxCodeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("IsTaxable", string.Empty);
            MappingFields.Add("Desc", string.Empty);
            MappingFields.Add("ItemPurchaseTaxFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addSalesRepTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Initial", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("SalesRepEntityFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addPayrollItemWageTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("WageType", string.Empty);
            MappingFields.Add("ExpenseAccountFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addPaymentMethodTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("PaymentMethodType", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addOtherNameTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("CompanyName", string.Empty);
            MappingFields.Add("Salutation", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("MiddleName", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("OtherNameAddr1", string.Empty);
            MappingFields.Add("OtherNameAddr2", string.Empty);
            MappingFields.Add("OtherNameAddr3", string.Empty);
            MappingFields.Add("OtherNameAddr4", string.Empty);
            MappingFields.Add("OtherNameAddr5", string.Empty);
            MappingFields.Add("OtherNameCity", string.Empty);
            MappingFields.Add("OtherNameState", string.Empty);
            MappingFields.Add("OtherNamePostalCode", string.Empty);
            MappingFields.Add("OtherNameCountry", string.Empty);
            MappingFields.Add("OtherNameNote", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("AltPhone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("Contact", string.Empty);
            MappingFields.Add("AltContact", string.Empty);
            MappingFields.Add("AccountNumber", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("ExternalGUID", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addListDisplayTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("ListDisplayAddType", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addJobTypeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        #region QuickBook Item

        private void addItemSubtotalTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ItemDesc", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }


        private void addItemServiceTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty); //AXIS 10.2 Available for QBSDK 12.0 
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("UnitOfMeasureSetFullName", string.Empty);
            MappingFields.Add("IsTaxInclude", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesOrPurchaseDesc", string.Empty);
            MappingFields.Add("SalesOrPurchasePrice", string.Empty);
            MappingFields.Add("SalesOrPurchasePricePercent", string.Empty);
            MappingFields.Add("SalesOrPurchaseAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchaseSalesDesc", string.Empty);
            MappingFields.Add("SalesAndPurchaseSalesPrice", string.Empty);
            MappingFields.Add("SalesAndPurchaseIncomeAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseDesc", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseCost", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesAndPurchaseExpenseAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchasePredVendorFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemSalesTaxGroupTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ItemDesc", string.Empty);           
            MappingFields.Add("ItemSalesTaxFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemSalesTaxTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ItemDesc", string.Empty);
            MappingFields.Add("TaxRate", string.Empty);
            MappingFields.Add("TaxVendorFullName", string.Empty);
            MappingFields.Add("SalesTaxReturnLineFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemReceiptTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("VendorFullName", string.Empty);
            MappingFields.Add("APAccountFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("IsTaxInclude", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("LinkToTxnID", string.Empty);

            //Improvement::548
            MappingFields.Add("SalesRepRefFullName", string.Empty);
            //MappingFields.Add("OwnerID1", string.Empty);
            MappingFields.Add("DataExtName1", string.Empty);
            MappingFields.Add("DataExtValue1", string.Empty);
            //MappingFields.Add("OwnerID2", string.Empty);
            MappingFields.Add("DataExtName2", string.Empty);
            MappingFields.Add("DataExtValue2", string.Empty);
        
            MappingFields.Add("ItemLineAddItemFullName", string.Empty);
            MappingFields.Add("ItemLineAddInventorySiteFullName", string.Empty);
            MappingFields.Add("ItemLineAddInventorySiteLocationFullName", string.Empty);
            MappingFields.Add("ItemLineAddDesc", string.Empty);
            MappingFields.Add("ItemLineAddQuantity", string.Empty);
            MappingFields.Add("ItemLineAddUnitOfMeasure", string.Empty);
            MappingFields.Add("ItemLineAddCost", string.Empty);
            MappingFields.Add("ItemLineAddAmount", string.Empty);
            MappingFields.Add("ItemLineAddCustomerFullName", string.Empty);
            MappingFields.Add("ItemLineAddClassFullName", string.Empty);
            MappingFields.Add("ItemLineAddSalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemLineAddBillableStatus", string.Empty);
            MappingFields.Add("ItemLineAddOverrideItemAccountFullName", string.Empty);
            MappingFields.Add("ItemLineAddLinkToTxnTxnID", string.Empty);

            //Improvement::548
            MappingFields.Add("ItemSalesRepRefFullName", string.Empty);
            //MappingFields.Add("ItemOwnerID1", string.Empty);
            MappingFields.Add("ItemDataExtName1", string.Empty);
            MappingFields.Add("ItemDataExtValue1", string.Empty);
            //MappingFields.Add("ItemOwnerID2", string.Empty);
            MappingFields.Add("ItemDataExtName2", string.Empty);
            MappingFields.Add("ItemDataExtValue2", string.Empty);
        
            MappingFields.Add("ItemLineAddLinkToTxnTxnLineID", string.Empty);
            MappingFields.Add("ItemGroupLineAddItemGroupFullName", string.Empty);
            MappingFields.Add("ItemGroupLineAddQuantity", string.Empty);
            MappingFields.Add("ItemGroupLineAddUnitOfMeasure", string.Empty);
            MappingFields.Add("ItemGroupLineAddInventorySiteFullName", string.Empty);
            
            //Improvement::548
            //MappingFields.Add("ItemGroupLineAddOwnerID1", string.Empty);
            MappingFields.Add("ItemGroupLineAddDataExtName1", string.Empty);
            MappingFields.Add("ItemGroupLineAddDataExtValue1", string.Empty);
            //MappingFields.Add("ItemGroupLineAddOwnerID1", string.Empty);
            MappingFields.Add("ItemGroupLineAddDataExtName2", string.Empty);
            MappingFields.Add("ItemGroupLineAddDataExtValue2", string.Empty);

            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemPaymentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ItemDesc", string.Empty);
            MappingFields.Add("DepositToAccountFullName", string.Empty);
            MappingFields.Add("PaymentMethodFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemOtherChargeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("IsTaxInclude", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesOrPurchaseDesc", string.Empty);
            MappingFields.Add("SalesOrPurchasePrice", string.Empty);
            MappingFields.Add("SalesOrPurchasePricePercent", string.Empty);
            MappingFields.Add("SalesOrPurchaseAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchaseSalesDesc", string.Empty);
            MappingFields.Add("SalesAndPurchaseSalesPrice", string.Empty);
            MappingFields.Add("SalesAndPurchaseIncomeAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseDesc", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseCost", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesAndPurchaseExpenseAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchasePrefVendorFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemNonInventoryTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("ManufacturerPartNumber", string.Empty);
            MappingFields.Add("UnitOfMeasureSetFullName", string.Empty);
            MappingFields.Add("IsTaxInclude", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesOrPurchaseDesc", string.Empty);
            MappingFields.Add("SalesOrPurchasePrice", string.Empty);
            MappingFields.Add("SalesOrPurchasePricePercent", string.Empty);
            MappingFields.Add("SalesOrPurchaseAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchaseSalesDesc", string.Empty);
            MappingFields.Add("SalesAndPurchaseSalesPrice", string.Empty);
            MappingFields.Add("SalesAndPurchaseIncomeAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseDesc", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseCost", string.Empty);
            MappingFields.Add("SalesAndPurchasePurchaseTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesAndPurchaseExpenseAccountFullName", string.Empty);
            MappingFields.Add("SalesAndPurchasePrefVendorFullName", string.Empty);


            AddMappingFieldsToCollection(MappingFields);
        }
        private void addItemInventoryAssemblyTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //bug 476 axis12.0
            MappingFields.Add("ClassRefFullName", string.Empty);

            MappingFields.Add("ParentFullName", string.Empty);
            //bug 476 axis12.0
            MappingFields.Add("ManufacturerPartNumber", string.Empty);

            MappingFields.Add("UnitOfMeasureSetFullName", string.Empty);
            MappingFields.Add("IsTaxInclude", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesDesc", string.Empty);
            MappingFields.Add("SalesPrice", string.Empty);
            MappingFields.Add("IncomeAccountFullName", string.Empty);
            MappingFields.Add("PurchaseDesc", string.Empty);
            MappingFields.Add("PurchaseCost", string.Empty);
            MappingFields.Add("PurchaseTaxCodeFullName", string.Empty);
            MappingFields.Add("COGSAccountFullName", string.Empty);
            MappingFields.Add("PrefVendorFullName", string.Empty);
            MappingFields.Add("AssetAccountFullName", string.Empty);
            MappingFields.Add("BuildPoint", string.Empty);
            //bug 476 axis12.0
            MappingFields.Add("Max", string.Empty);

            MappingFields.Add("QuantityOnHand", string.Empty);
            MappingFields.Add("TotalValue", string.Empty);
            MappingFields.Add("InventoryDate", string.Empty);
            MappingFields.Add("ItemInventoryAssemblyLineItemInventoryFullName", string.Empty);
            MappingFields.Add("ItemInventoryAssemblyLineQuantity", string.Empty);


            AddMappingFieldsToCollection(MappingFields);

        }

        private void addItemInventoryTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("ManufacturerPartNumber", string.Empty);
            MappingFields.Add("UnitOfMeasureSetFullName", string.Empty);
            MappingFields.Add("IsTaxInclude", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("SalesDesc", string.Empty);
            MappingFields.Add("SalesPrice", string.Empty);
            MappingFields.Add("IncomeAccountFullName", string.Empty);
            MappingFields.Add("PurchaseDesc", string.Empty);
            MappingFields.Add("PurchaseCost", string.Empty);
            MappingFields.Add("PurchaseTaxCodeFullName", string.Empty);
            MappingFields.Add("COGSAccountFullName", string.Empty);
            MappingFields.Add("PrefVendorFullName", string.Empty);
            MappingFields.Add("AssetAccountFullName", string.Empty);
            MappingFields.Add("ReorderPoint", string.Empty);
            MappingFields.Add("Max", string.Empty);
            MappingFields.Add("QuantityOnHand", string.Empty);
            MappingFields.Add("TotalValue", string.Empty);
            MappingFields.Add("InventoryDate", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemGroupTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("Class", string.Empty);

            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ItemDesc", string.Empty);
            MappingFields.Add("UnitOfMeasureSetFullName", string.Empty);
            MappingFields.Add("IsPrintItemsInGroup", string.Empty);            
            MappingFields.Add("ItemGroupLineItemFullName", string.Empty);
            MappingFields.Add("ItemGroupLineQuantity", string.Empty);
            MappingFields.Add("ItemGroupLineUnitOfMeasure", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemFixedAssetTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("AcquiredAs", string.Empty);
            MappingFields.Add("PurchaseDesc", string.Empty);
            MappingFields.Add("PurchaseDate", string.Empty);
            MappingFields.Add("PurchaseCost", string.Empty);
            MappingFields.Add("VendorOrPayeeName", string.Empty);
            MappingFields.Add("AssetAccountFullName", string.Empty);
            MappingFields.Add("FixedAssetSalesInfoSalesDesc", string.Empty);
            MappingFields.Add("FixedAssetSalesInfoSalesDate", string.Empty);
            MappingFields.Add("FixedAssetSalesInfoSalesPrice", string.Empty);
            MappingFields.Add("FixedAssetSalesInfoSalesExpense", string.Empty);
            MappingFields.Add("AssetDesc", string.Empty);
            MappingFields.Add("Location", string.Empty);
            MappingFields.Add("PONumber", string.Empty);
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("WarrantyExpDate", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("AssetNumber", string.Empty);
            MappingFields.Add("CostBasis", string.Empty);
            MappingFields.Add("YearEndAccumulatedDepreciation", string.Empty);
            MappingFields.Add("YearEndBookValue", string.Empty);


            AddMappingFieldsToCollection(MappingFields);
        }

        private void addItemDiscountTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("BarCodeValue", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            //P Axis 13.1 : issue 651
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("ItemDesc", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("DiscountRate", string.Empty);
            MappingFields.Add("DiscountRatePercent", string.Empty);
            MappingFields.Add("AccountFullName", string.Empty);


            AddMappingFieldsToCollection(MappingFields);
        }

        #endregion

        private void addEmployeeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("Salutation", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("MiddleName", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("SupervisorFullName", string.Empty);
            MappingFields.Add("Department", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("TargetBonus", string.Empty);

            MappingFields.Add("EmployeeAddr1", string.Empty);
            MappingFields.Add("EmployeeAddr2", string.Empty);
            MappingFields.Add("EmployeeCity", string.Empty);
            MappingFields.Add("EmployeeState", string.Empty);
            MappingFields.Add("EmployeePostalCode", string.Empty);
            MappingFields.Add("PrintAs", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Mobile", string.Empty);
            MappingFields.Add("Pager", string.Empty);
            MappingFields.Add("PagerPIN", string.Empty);
            MappingFields.Add("AltPhone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("SSN", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("EmergencyPrimaryContactName", string.Empty);
            MappingFields.Add("EmergencyPrimaryContactValue", string.Empty);
            MappingFields.Add("EmergencyPrimaryContactRelation", string.Empty);
            MappingFields.Add("EmergencySecondaryContactName", string.Empty);
            MappingFields.Add("EmergencySecondaryContactValue", string.Empty);
            MappingFields.Add("EmergencySecondaryContactRelation", string.Empty);

            MappingFields.Add("EmployeeType", string.Empty);
            MappingFields.Add("PartOrFullTime", string.Empty);
            MappingFields.Add("Exempt", string.Empty);
            MappingFields.Add("KeyEmployee", string.Empty);

            MappingFields.Add("Gender", string.Empty);
            MappingFields.Add("HiredDate", string.Empty);
            MappingFields.Add("OriginalHireDate", string.Empty);
            MappingFields.Add("AdjustedServiceDate", string.Empty);

            MappingFields.Add("ReleasedDate", string.Empty);
            MappingFields.Add("BirthDate", string.Empty);
            MappingFields.Add("USCitizen", string.Empty);
            MappingFields.Add("Ethnicity", string.Empty);
            MappingFields.Add("Disabled", string.Empty);
            MappingFields.Add("DisabilityDesc", string.Empty);
            MappingFields.Add("OnFile", string.Empty);
            MappingFields.Add("WorkAuthExpireDate", string.Empty);
            MappingFields.Add("USVeteran", string.Empty);
            MappingFields.Add("MilitaryStatus", string.Empty);

            MappingFields.Add("AccountNumber", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("BillingRateFullName", string.Empty);
            MappingFields.Add("EmployeePayrollPayPeriod", string.Empty);
            MappingFields.Add("EmployeePayrollClassFullName", string.Empty);
            MappingFields.Add("EmployeePayrollClearEarnings", string.Empty);
            MappingFields.Add("EmployeePayrollEarningsPayrollItemWageFullName", string.Empty);
            MappingFields.Add("EmployeePayrollEarningsRate", string.Empty);
            MappingFields.Add("EmployeePayrollEarningsRatePercent", string.Empty);
            MappingFields.Add("EmployeePayrollIsUsingTimeDataToCreatePaychecks", string.Empty);
            MappingFields.Add("EmployeePayrollUseTimeDataToCreatePaychecks", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursHoursAvailable", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursAccrualPeriod", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursHoursAccrued", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursMaximumHours", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursIsResettingHoursEachNewYear", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursHoursUsed", string.Empty);
            MappingFields.Add("EmployeePayrollSickHoursAccrualStartDate", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursHoursAvailable", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursAccrualPeriod", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursHoursAccrued", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursMaximumHours", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursIsResettingHoursEachNewYear", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursHoursUsed", string.Empty);
            MappingFields.Add("EmployeePayrollVacationHoursAccrualStartDate", string.Empty);

            AddMappingFieldsToCollection(MappingFields);


        }

        private void addDateDrivenTermsTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("DayOfMonthDue", string.Empty);
            MappingFields.Add("DueOfMonthDays", string.Empty);
            MappingFields.Add("DiscountDayOfMonth", string.Empty);
            MappingFields.Add("DiscountPct", string.Empty);
            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCustomerTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);
            MappingFields.Add("CompanyName", string.Empty);
            MappingFields.Add("Salutation", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("MiddleName", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("JobTitle", string.Empty);          
            MappingFields.Add("BillAddr1", string.Empty);
            MappingFields.Add("BillAddr2", string.Empty);
            MappingFields.Add("BillAddr3", string.Empty);
            MappingFields.Add("BillAddr4", string.Empty);
            MappingFields.Add("BillAddr5", string.Empty);
            MappingFields.Add("BillCity", string.Empty);
            MappingFields.Add("BillState", string.Empty);
            MappingFields.Add("BillPostalCode", string.Empty);
            MappingFields.Add("BillCountry", string.Empty);
            MappingFields.Add("BillNote", string.Empty);
            MappingFields.Add("ShipAddr1", string.Empty);
            MappingFields.Add("ShipAddr2", string.Empty);
            MappingFields.Add("ShipAddr3", string.Empty);
            MappingFields.Add("ShipAddr4", string.Empty);
            MappingFields.Add("ShipAddr5", string.Empty);
            MappingFields.Add("ShipCity", string.Empty);
            MappingFields.Add("ShipState", string.Empty);
            MappingFields.Add("ShipPostalCode", string.Empty);
            MappingFields.Add("ShipCountry", string.Empty);
            MappingFields.Add("ShipNote", string.Empty);
            MappingFields.Add("ShipToName", string.Empty);
            MappingFields.Add("ShipToAddr1", string.Empty);
            MappingFields.Add("ShipToAddr2", string.Empty);
            MappingFields.Add("ShipToAddr3", string.Empty);
            MappingFields.Add("ShipToAddr4", string.Empty);
            MappingFields.Add("ShipToAddr5", string.Empty);
            MappingFields.Add("ShipToCity", string.Empty);
            MappingFields.Add("ShipToState", string.Empty);
            MappingFields.Add("ShipToPostalCode", string.Empty);
            MappingFields.Add("ShipToCountry", string.Empty);
            MappingFields.Add("ShipToNote", string.Empty);
            MappingFields.Add("DefaultShipTo", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("AltPhone", string.Empty);
            MappingFields.Add("Fax", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("Cc", string.Empty);
            MappingFields.Add("Contact", string.Empty);
            MappingFields.Add("AltContact", string.Empty);
            MappingFields.Add("ContactName", string.Empty);
            MappingFields.Add("ContactValue", string.Empty);
            MappingFields.Add("CustomerTypeFullName", string.Empty);
            MappingFields.Add("TermFullName", string.Empty);
            MappingFields.Add("SalesRepFullName", string.Empty);
            MappingFields.Add("SalesTaxCodeFullName", string.Empty);
            MappingFields.Add("ItemSalesTaxFullName", string.Empty);
            MappingFields.Add("ReSaleNumber", string.Empty);
            MappingFields.Add("AccountNumber", string.Empty);
            MappingFields.Add("CreditLimit", string.Empty);
            MappingFields.Add("PreferredPaymentMethodFullName", string.Empty);
            MappingFields.Add("CreditCardInfoCreditCardNumber", string.Empty);
            MappingFields.Add("CreditCardInfoExpirationMonth", string.Empty);
            MappingFields.Add("CreditCardInfoExpirationYear", string.Empty);
            MappingFields.Add("CreditCardInfoNameOnCard", string.Empty);
            MappingFields.Add("CreditCardInfoCreditCardAddress", string.Empty);
            MappingFields.Add("CreditCardInfoCreditCardPostalCode", string.Empty);
            MappingFields.Add("JobStatus", string.Empty);
            MappingFields.Add("JobStartDate", string.Empty);
            MappingFields.Add("JobProjectedEndDate", string.Empty);
            MappingFields.Add("JobEndDate", string.Empty);
            MappingFields.Add("JobDesc", string.Empty);
            MappingFields.Add("JobTypeFullName", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("AdditionalNotes", string.Empty);
            MappingFields.Add("PreferredDeliveryMethod", string.Empty);
            MappingFields.Add("PriceLevelFullName", string.Empty);
            MappingFields.Add("CurrencyFullName", string.Empty);
            AddMappingFieldsToCollection(MappingFields);
        }

        private void addCurrencyTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("CurrencyCode", string.Empty);
            MappingFields.Add("CurrencyFormatThousandSeparator", string.Empty);
            MappingFields.Add("CurrencyFormatThousandSeparatorGrouping", string.Empty);
            MappingFields.Add("CurrencyFormatDecimalPlaces", string.Empty);
            MappingFields.Add("CurrencyFormatDecimalSeparator", string.Empty);
            AddMappingFieldsToCollection(MappingFields);
        }
        private void addClassTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("IsActive", string.Empty);
            MappingFields.Add("ParentFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }
        private void addChargeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("ItemFullName", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("Desc", string.Empty);
            MappingFields.Add("ARAccountFullName", string.Empty);
            MappingFields.Add("ClassFullName", string.Empty);
            MappingFields.Add("BilledDate", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("OverrideItemAccountFullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }

        private void addPurchaseOrderTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            //user mappings
            MappingFields.Add("VendorRefFullName", string.Empty);
            //Axis 617 
            MappingFields.Add("Currency", string.Empty);
            //Axis 617 ends
            MappingFields.Add("ClassRefFullName", string.Empty);
            MappingFields.Add("InventorySiteRefFullName", string.Empty);
            MappingFields.Add("ShipToEntityRefFullName", string.Empty);
            MappingFields.Add("TemplateRefFullName", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PurchaseOrderRefNumber", string.Empty);
            MappingFields.Add(this.PurchaseOrderAddressTag());
          //  this.PurchaseOrderAddressTag();
            MappingFields.Add("TermsRefFullName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("ExpectedDate", string.Empty);
            MappingFields.Add("ShipMethodRefFullName", string.Empty);
            MappingFields.Add("FOB", string.Empty);
            MappingFields.Add("Memo", string.Empty);
            MappingFields.Add("VendorMsg", string.Empty);
            MappingFields.Add("IsToBePrinted", string.Empty);
            MappingFields.Add("IsToBeEmailed", string.Empty);
            MappingFields.Add("IsTaxIncluded", string.Empty);
            MappingFields.Add("SalesTaxCodeRefFullName", string.Empty);
            MappingFields.Add("Other1", string.Empty);
            MappingFields.Add("Other2", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);

            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("ManufacturerPartNumber", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("Quantity", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("Rate", string.Empty);
            MappingFields.Add("PurchaseOrdLineClassName", string.Empty);
            MappingFields.Add("Amount", string.Empty);
            MappingFields.Add("InventorySiteLocationRef", string.Empty);
            MappingFields.Add("CustomerRefFullName", string.Empty);
            MappingFields.Add("ServiceDate", string.Empty);
            MappingFields.Add("PurchaseOrdLineSalesTaxCodeName", string.Empty);
            MappingFields.Add("OverrideItemAccountFullName", string.Empty);
            MappingFields.Add("POLineOther1", string.Empty);
            MappingFields.Add("POLineOther2", string.Empty);
            MappingFields.Add("OwnerID", string.Empty);
            MappingFields.Add("DataExtName", string.Empty);
            MappingFields.Add("DataExtValue", string.Empty);

            AddMappingFieldsToCollection(MappingFields);


        }

        //Axis 8.0
        private void addDataExtTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            //user Mapping
            MappingFields.Add("DataExtName", string.Empty);
            MappingFields.Add("ListDataExtType", string.Empty);
            MappingFields.Add("FullName", string.Empty);
            MappingFields.Add("DataExtValue", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        //Axis 7 Changes
        /// <summary>
        /// Transfer Inventory add tag.
        /// </summary>
        private void addTransferInventoryTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //user mappings
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("FromInventorySiteRefFullName", string.Empty);
            MappingFields.Add("ToInventorySiteRefFullName", string.Empty);
            MappingFields.Add("FromInventorySiteLocationRef", string.Empty);
            MappingFields.Add("ToInventorySiteLocationRef", string.Empty);          
            MappingFields.Add("Memo", string.Empty);           
            MappingFields.Add("ItemRefFullName", string.Empty);
            MappingFields.Add("QuantityToTransfer", string.Empty);
            //P Axis 13.1 : issue 659
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("LotNumber", string.Empty);

            AddMappingFieldsToCollection(MappingFields);
        }


     

        #endregion
        //axis 11 pos
        #region  POS Transactions Import Types

        private void addPOSCustomerTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("CompanyName", string.Empty);
            MappingFields.Add("CustomerID", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("CustomerDiscPercent", string.Empty);
            MappingFields.Add("CustomerDiscType", string.Empty);
            MappingFields.Add("CustomerType", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("IsOkToEMail", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("IsAcceptingChecks", string.Empty);
            MappingFields.Add("IsUsingChargeAccount", string.Empty);
            MappingFields.Add("IsUsingWithQB", string.Empty);
            MappingFields.Add("IsRewardsMember", string.Empty);
            MappingFields.Add("IsNoShipToBilling", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Phone2", string.Empty);
            MappingFields.Add("Phone3", string.Empty);
            MappingFields.Add("Phone4", string.Empty);
            MappingFields.Add("PriceLevelNumber", string.Empty);
            MappingFields.Add("Salutation", string.Empty);
            MappingFields.Add("TaxCategory", string.Empty);
            MappingFields.Add("BillCity", string.Empty);
            MappingFields.Add("BillCountry", string.Empty);
            MappingFields.Add("BillPostalCode", string.Empty);
            MappingFields.Add("BillState", string.Empty);
            MappingFields.Add("BillStreet", string.Empty);
            MappingFields.Add("BillStreet2", string.Empty);
            MappingFields.Add("DefaultShipAddress", string.Empty);
            MappingFields.Add("ShipAddressName", string.Empty);
            MappingFields.Add("ShipCompanyName", string.Empty);
            MappingFields.Add("ShipFullName", string.Empty);
            MappingFields.Add("ShipCity", string.Empty);
            MappingFields.Add("ShipCountry", string.Empty);
            MappingFields.Add("ShipPostalCode", string.Empty);
            MappingFields.Add("ShipState", string.Empty);
            MappingFields.Add("ShipStreet", string.Empty);
            MappingFields.Add("ShipStreet2", string.Empty);
            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSEmployeeTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            //user mappings
            MappingFields.Add("City", string.Empty);
            MappingFields.Add("CommissionPercent", string.Empty);
            MappingFields.Add("Country", string.Empty);
            MappingFields.Add("Email", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("IsTrackingHours", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("LoginName", string.Empty);
            MappingFields.Add("Notes", string.Empty);
            MappingFields.Add("Phone", string.Empty);
            MappingFields.Add("Phone2", string.Empty);
            MappingFields.Add("Phone3", string.Empty);
            MappingFields.Add("Phone4", string.Empty);
            MappingFields.Add("PostalCode", string.Empty);
            MappingFields.Add("State", string.Empty);
            MappingFields.Add("Street", string.Empty);
            MappingFields.Add("Street2", string.Empty);
            MappingFields.Add("SecurityGroup", string.Empty);
            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSItemInventoryTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("ALU", string.Empty);
            MappingFields.Add("Attribute", string.Empty);
            MappingFields.Add("COGSAccount", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("Department", string.Empty);
            MappingFields.Add("Desc1", string.Empty);
            MappingFields.Add("Desc2", string.Empty);
            MappingFields.Add("IncomeAccount", string.Empty);
            MappingFields.Add("IsEligibleForCommission", string.Empty);
            MappingFields.Add("IsPrintingTags", string.Empty);
            MappingFields.Add("IsUnorderable", string.Empty);
            MappingFields.Add("IsEligibleForRewards", string.Empty);
            MappingFields.Add("IsWebItem", string.Empty);
            MappingFields.Add("ItemType", string.Empty);
            MappingFields.Add("MarginPercent", string.Empty);
            MappingFields.Add("MarkupPercent", string.Empty);
            MappingFields.Add("MSRP", string.Empty);
            MappingFields.Add("OnHandStore01", string.Empty);
            MappingFields.Add("OnHandStore02", string.Empty);
            MappingFields.Add("OnHandStore03", string.Empty);
            MappingFields.Add("OnHandStore04", string.Empty);
            MappingFields.Add("OnHandStore05", string.Empty);
            MappingFields.Add("OnHandStore06", string.Empty);
            MappingFields.Add("OnHandStore07", string.Empty);
            MappingFields.Add("OnHandStore08", string.Empty);
            MappingFields.Add("OnHandStore09", string.Empty);
            MappingFields.Add("OnHandStore10", string.Empty);
            MappingFields.Add("OnHandStore11", string.Empty);
            MappingFields.Add("OnHandStore12", string.Empty);
            MappingFields.Add("OnHandStore13", string.Empty);
            MappingFields.Add("OnHandStore14", string.Empty);
            MappingFields.Add("OnHandStore15", string.Empty);
            MappingFields.Add("OnHandStore16", string.Empty);
            MappingFields.Add("OnHandStore17", string.Empty);
            MappingFields.Add("OnHandStore18", string.Empty);
            MappingFields.Add("OnHandStore19", string.Empty);
            MappingFields.Add("OnHandStore20", string.Empty);

            MappingFields.Add("ReorderPointStore01", string.Empty);
            MappingFields.Add("ReorderPointStore02", string.Empty);
            MappingFields.Add("ReorderPointStore03", string.Empty);
            MappingFields.Add("ReorderPointStore04", string.Empty);
            MappingFields.Add("ReorderPointStore05", string.Empty);
            MappingFields.Add("ReorderPointStore06", string.Empty);
            MappingFields.Add("ReorderPointStore07", string.Empty);
            MappingFields.Add("ReorderPointStore08", string.Empty);
            MappingFields.Add("ReorderPointStore09", string.Empty);
            MappingFields.Add("ReorderPointStore10", string.Empty);
            MappingFields.Add("ReorderPointStore11", string.Empty);
            MappingFields.Add("ReorderPointStore12", string.Empty);
            MappingFields.Add("ReorderPointStore13", string.Empty);
            MappingFields.Add("ReorderPointStore14", string.Empty);
            MappingFields.Add("ReorderPointStore15", string.Empty);
            MappingFields.Add("ReorderPointStore16", string.Empty);
            MappingFields.Add("ReorderPointStore17", string.Empty);
            MappingFields.Add("ReorderPointStore18", string.Empty);
            MappingFields.Add("ReorderPointStore19", string.Empty);
            MappingFields.Add("ReorderPointStore20", string.Empty);

            MappingFields.Add("OrderByUnit", string.Empty);
            MappingFields.Add("OrderCost", string.Empty);

            MappingFields.Add("Price1", string.Empty);
            MappingFields.Add("Price2", string.Empty);
            MappingFields.Add("Price3", string.Empty);
            MappingFields.Add("Price4", string.Empty);
            MappingFields.Add("Price5", string.Empty);

            MappingFields.Add("ReorderPoint", string.Empty);
            MappingFields.Add("SellByUnit", string.Empty);
            MappingFields.Add("SerialFlag", string.Empty);
            MappingFields.Add("Size", string.Empty);
            MappingFields.Add("TaxCode", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("UPC", string.Empty);
            MappingFields.Add("VendorName", string.Empty);           
            MappingFields.Add("WebDesc", string.Empty);
            MappingFields.Add("WebPrice", string.Empty);
            MappingFields.Add("Manufacturer", string.Empty);
            MappingFields.Add("Weight", string.Empty);
            MappingFields.Add("Keywords", string.Empty);
            MappingFields.Add("WebCategories", string.Empty);

            MappingFields.Add("UOM1ALU", string.Empty);
            MappingFields.Add("UOM1MSRP", string.Empty);
            MappingFields.Add("UOM1NumberOfBaseUnits", string.Empty);
            MappingFields.Add("UOM1Price1", string.Empty);
            MappingFields.Add("UOM1Price2", string.Empty);
            MappingFields.Add("UOM1Price3", string.Empty);
            MappingFields.Add("UOM1Price4", string.Empty);
            MappingFields.Add("UOM1Price5", string.Empty);
            MappingFields.Add("UOM1UnitOfMeasure", string.Empty);
            MappingFields.Add("UOM1UPC", string.Empty);

            MappingFields.Add("UOM2ALU", string.Empty);
            MappingFields.Add("UOM2MSRP", string.Empty);
            MappingFields.Add("UOM2NumberOfBaseUnits", string.Empty);
            MappingFields.Add("UOM2Price1", string.Empty);
            MappingFields.Add("UOM2Price2", string.Empty);
            MappingFields.Add("UOM2Price3", string.Empty);
            MappingFields.Add("UOM2Price4", string.Empty);
            MappingFields.Add("UOM2Price5", string.Empty);
            MappingFields.Add("UOM2UnitOfMeasure", string.Empty);
            MappingFields.Add("UOM2UPC", string.Empty);

            MappingFields.Add("UOM3ALU", string.Empty);
            MappingFields.Add("UOM3MSRP", string.Empty);
            MappingFields.Add("UOM3NumberOfBaseUnits", string.Empty);
            MappingFields.Add("UOM3Price1", string.Empty);
            MappingFields.Add("UOM3Price2", string.Empty);
            MappingFields.Add("UOM3Price3", string.Empty);
            MappingFields.Add("UOM3Price4", string.Empty);
            MappingFields.Add("UOM3Price5", string.Empty);
            MappingFields.Add("UOM3UnitOfMeasure", string.Empty);
            MappingFields.Add("UOM3UPC", string.Empty);

            MappingFields.Add("Vendor2ALU", string.Empty);
            MappingFields.Add("Vendor2OrderCost", string.Empty);
            MappingFields.Add("Vendor2UPC", string.Empty);
            MappingFields.Add("Vendor2VendorName", string.Empty);           

            MappingFields.Add("Vendor3ALU", string.Empty);
            MappingFields.Add("Vendor3OrderCost", string.Empty);
            MappingFields.Add("Vendor3UPC", string.Empty);
            MappingFields.Add("Vendor3VendorName", string.Empty);           

            MappingFields.Add("Vendor4ALU", string.Empty);
            MappingFields.Add("Vendor4OrderCost", string.Empty);
            MappingFields.Add("Vendor4UPC", string.Empty);
            MappingFields.Add("Vendor4VendorName", string.Empty);         

            MappingFields.Add("Vendor5ALU", string.Empty);
            MappingFields.Add("Vendor5OrderCost", string.Empty);
            MappingFields.Add("Vendor5UPC", string.Empty);
            MappingFields.Add("Vendor5VendorName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSPriceDiscountTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            //user mappings
            MappingFields.Add("PriceDiscountName", string.Empty);
            MappingFields.Add("PriceDiscountReason", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("PriceDiscountType", string.Empty);
            MappingFields.Add("IsInactive", string.Empty);
            MappingFields.Add("PriceDiscountPriceLevels", string.Empty);
            MappingFields.Add("PriceDiscountXValue", string.Empty);
            MappingFields.Add("PriceDiscountYValue", string.Empty);
            MappingFields.Add("IsApplicableOverXValue", string.Empty);
            MappingFields.Add("StartDate", string.Empty);
            MappingFields.Add("StopDate", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("PriceDiscountItemUOM", string.Empty);
            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSPriceAdjustmentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("PriceAdjustmentName", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("PriceLevelNumber", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("PriceAdjustmentNewPrice", string.Empty);           

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSDataExtTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DataExtName", string.Empty);
            MappingFields.Add("ListDataExtType", string.Empty);
            MappingFields.Add("List", string.Empty);
            MappingFields.Add("DataExtValue", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSSalesOrderTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("Cashier", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("DiscountPercent", string.Empty);
            MappingFields.Add("Instructions", string.Empty);
            MappingFields.Add("PriceLevelNumber", string.Empty);
            MappingFields.Add("PromoCode", string.Empty);
            MappingFields.Add("SalesOrderNumber", string.Empty);
            MappingFields.Add("SalesOrderStatusDesc", string.Empty);
            MappingFields.Add("SalesOrderType", string.Empty);
            MappingFields.Add("TaxCategory", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("ShipAddressName", string.Empty);
            MappingFields.Add("ShipCity", string.Empty);
            MappingFields.Add("ShipCompanyName", string.Empty);
            MappingFields.Add("ShipCountry", string.Empty);
            MappingFields.Add("ShipFullName", string.Empty);
            MappingFields.Add("ShipPhone", string.Empty);
            MappingFields.Add("ShipPhone2", string.Empty);
            MappingFields.Add("ShipPhone3", string.Empty);
            MappingFields.Add("ShipPhone4", string.Empty);
            MappingFields.Add("ShipPostalCode", string.Empty);
            MappingFields.Add("ShipBy", string.Empty);
            MappingFields.Add("Shipping", string.Empty);
            MappingFields.Add("ShipState", string.Empty);
            MappingFields.Add("ShipStreet", string.Empty);
            MappingFields.Add("ShipStreet2", string.Empty);          
            MappingFields.Add("SalesALU", string.Empty);
            MappingFields.Add("SalesAssociate", string.Empty);
            MappingFields.Add("SalesAttribute", string.Empty);
            MappingFields.Add("SalesCommission", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("SalesDesc2", string.Empty);
            MappingFields.Add("SalesDiscount", string.Empty);
            MappingFields.Add("SalesDiscountPercent", string.Empty);
            MappingFields.Add("SalesDiscountType", string.Empty);
            MappingFields.Add("SalesExtendedPrice", string.Empty);
            MappingFields.Add("SalesPrice", string.Empty);
            MappingFields.Add("SalesQty", string.Empty);
            MappingFields.Add("SalesSerialNumber", string.Empty);
            MappingFields.Add("SalesSize", string.Empty);
            MappingFields.Add("SalesTaxCode", string.Empty);
            MappingFields.Add("SalesUnitOfMeasure", string.Empty);
            MappingFields.Add("SalesUPC", string.Empty);   
           
            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSSalesReceiptTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("Cashier", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("CustomerFullName", string.Empty);           
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("DiscountPercent", string.Empty);
            MappingFields.Add("PriceLevelNumber", string.Empty);
            MappingFields.Add("PromoCode", string.Empty);            
            MappingFields.Add("SalesOrderNumber", string.Empty);
            MappingFields.Add("SalesReceiptNumber", string.Empty);
            MappingFields.Add("SalesReceiptType", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("StoreNumber", string.Empty);
            MappingFields.Add("TaxCategory", string.Empty);
            //11.4 451
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("Workstation", string.Empty);     
            MappingFields.Add("ShipAddressName", string.Empty);
            MappingFields.Add("ShipCity", string.Empty);
            MappingFields.Add("ShipCompanyName", string.Empty);
            MappingFields.Add("ShipCountry", string.Empty);
            MappingFields.Add("ShipFullName", string.Empty);
            MappingFields.Add("ShipPhone", string.Empty);
            MappingFields.Add("ShipPhone2", string.Empty);
            MappingFields.Add("ShipPhone3", string.Empty);
            MappingFields.Add("ShipPhone4", string.Empty);
            MappingFields.Add("ShipPostalCode", string.Empty);
            MappingFields.Add("ShipBy", string.Empty);
            MappingFields.Add("Shipping", string.Empty);
            MappingFields.Add("ShipState", string.Empty);
            MappingFields.Add("ShipStreet", string.Empty);
            MappingFields.Add("ShipStreet2", string.Empty);           
            MappingFields.Add("SalesReceiptALU", string.Empty);
            MappingFields.Add("SalesReceiptAssociate", string.Empty);
            MappingFields.Add("SalesReceiptAttribute", string.Empty);
            MappingFields.Add("SalesReceiptCommission", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("SalesReceiptDesc2", string.Empty);
            MappingFields.Add("SalesReceiptDiscount", string.Empty);
            MappingFields.Add("SalesReceiptDiscountPercent", string.Empty);
            MappingFields.Add("SalesReceiptDiscountType", string.Empty);
            MappingFields.Add("SalesReceiptExtendedPrice", string.Empty);
            MappingFields.Add("SalesReceiptPrice", string.Empty);
            MappingFields.Add("SalesReceiptQty", string.Empty);
            MappingFields.Add("SalesReceiptSerialNumber", string.Empty);
            MappingFields.Add("SalesReceiptSize", string.Empty);
            MappingFields.Add("SalesReceiptTaxCode", string.Empty);
            MappingFields.Add("SalesReceiptUnitOfMeasure", string.Empty);
            MappingFields.Add("SalesReceiptUPC", string.Empty);
            MappingFields.Add("AccountTenderAmount", string.Empty);
            MappingFields.Add("AccountTipAmount", string.Empty);
            MappingFields.Add("CashTenderAmount", string.Empty);
            MappingFields.Add("CheckNumber", string.Empty);
            MappingFields.Add("CheckTenderAmount", string.Empty);
            MappingFields.Add("CreditCardName", string.Empty);
            MappingFields.Add("CreditCardTenderAmount", string.Empty);
            MappingFields.Add("CreditCardTipAmount", string.Empty);
            MappingFields.Add("DebitCardCashback", string.Empty);
            MappingFields.Add("DebitCardTenderAmount", string.Empty);
            MappingFields.Add("DepositTenderAmount", string.Empty);
            MappingFields.Add("GiftCertificateNumber",string.Empty);
            MappingFields.Add("GiftTenderAmount", string.Empty);
            MappingFields.Add("GiftCardTenderAmount", string.Empty);
            MappingFields.Add("GiftCardTipAmount", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSPurchaseOrderTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //user mappings
            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("CancelDate", string.Empty);            
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("DiscountPercent", string.Empty);
            MappingFields.Add("Fee", string.Empty);
            MappingFields.Add("Instructions", string.Empty);
            MappingFields.Add("PurchaseOrderNumber", string.Empty);
            MappingFields.Add("PurchaseOrderStatusDesc", string.Empty);
            MappingFields.Add("ShipToStoreNumber", string.Empty);
            MappingFields.Add("StartShipDate", string.Empty);
            MappingFields.Add("StoreNumber", string.Empty);
            MappingFields.Add("TermsDiscount", string.Empty);
            MappingFields.Add("TermsDiscountDays", string.Empty);
            MappingFields.Add("TermsNetDays", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("VendorName", string.Empty);         
            MappingFields.Add("ALU", string.Empty);           
            MappingFields.Add("Attribute", string.Empty);
            MappingFields.Add("Cost", string.Empty);           
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("Desc2", string.Empty);
            MappingFields.Add("ExtendedCost", string.Empty);
            MappingFields.Add("Qty", string.Empty);
            MappingFields.Add("Size", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("UPC", string.Empty);

            AddMappingFieldsToCollection(MappingFields);


        }

        private void addPOSTimeEntryTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("ClockInTime", string.Empty);
            MappingFields.Add("ClockOutTime", string.Empty);
            MappingFields.Add("CreatedBy", string.Empty);
            MappingFields.Add("QuickBooksFlag", string.Empty);
            MappingFields.Add("EmployeeLoginName", string.Empty);
            MappingFields.Add("StoreNumber", string.Empty);
            AddMappingFieldsToCollection(MappingFields);

      
        }

        private void addPOSVoucherTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("Discount", string.Empty);
            MappingFields.Add("DiscountPercent", string.Empty);
            MappingFields.Add("Fee", string.Empty);
            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("InvoiceDate", string.Empty);
            MappingFields.Add("InvoiceDueDate", string.Empty);
            MappingFields.Add("InvoiceNumber", string.Empty);
            MappingFields.Add("PayeeListID", string.Empty);
            MappingFields.Add("PurchaseOrderNumber", string.Empty);
            MappingFields.Add("QuickBooksFlag", string.Empty);
            MappingFields.Add("StoreNumber", string.Empty);
            MappingFields.Add("TermsDiscount", string.Empty);
            MappingFields.Add("TermsDiscountDays", string.Empty);
            MappingFields.Add("TermsNetDays", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("TxnState", string.Empty);
            MappingFields.Add("VendorName", string.Empty);          
            MappingFields.Add("VoucherType", string.Empty);
            MappingFields.Add("Workstation", string.Empty);          
            MappingFields.Add("ALU", string.Empty);
            MappingFields.Add("Attribute", string.Empty);
            MappingFields.Add("Cost", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("Desc2", string.Empty);
            MappingFields.Add("ExtendedCost", string.Empty);
            MappingFields.Add("QtyReceived", string.Empty);
            MappingFields.Add("Size", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            MappingFields.Add("UPC", string.Empty);
            AddMappingFieldsToCollection(MappingFields);


        }

        private void addPOSInventoryCostAdjustmentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("InventoryAdjustmentSource", string.Empty);
            MappingFields.Add("QuickBooksFlag", string.Empty);
            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Reason", string.Empty);
            MappingFields.Add("StoreNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("TxnState", string.Empty);
            MappingFields.Add("Workstation", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("NewCost", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSInventoryQtyAdjustmentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("InventoryAdjustmentSource", string.Empty);
            MappingFields.Add("QuickBooksFlag", string.Empty);
            MappingFields.Add("Reason", string.Empty);
            MappingFields.Add("StoreNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("TxnState", string.Empty);
            MappingFields.Add("Workstation", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            //Axis-565
            MappingFields.Add("ALU", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("NewQuantity", string.Empty);
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            //Axis-565
            MappingFields.Add("UPC", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addPOSTransferSlipTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("RefNumber", string.Empty);
            MappingFields.Add("Associate", string.Empty);
            MappingFields.Add("Carrier", string.Empty);
            MappingFields.Add("Comments", string.Empty);
            MappingFields.Add("Freight", string.Empty);
            MappingFields.Add("FromStoreNumber", string.Empty);
            MappingFields.Add("SlipETA", string.Empty); 
            MappingFields.Add("ToStoreNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("TxnState", string.Empty);
            MappingFields.Add("Workstation", string.Empty);
            MappingFields.Add("ItemName", string.Empty);
            //Axis-565
            MappingFields.Add("ALU", string.Empty);
            MappingFields.Add("ItemInventoryDepartment", string.Empty);
            MappingFields.Add("Qty", string.Empty);
            MappingFields.Add("SerialNumber", string.Empty);
            MappingFields.Add("UnitOfMeasure", string.Empty);
            //Axis-565
            MappingFields.Add("UPC", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        #endregion

        // Axis 11.1
        #region  Online Transactions Import Types

        private void addOnlineSalesReceiptTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //user mappings
            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("DepartmentRef", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);    
            MappingFields.Add("TxnStatus", string.Empty);
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);            
            MappingFields.Add("LineSalesItemRefName", string.Empty);
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineSalesItemClassRef", string.Empty);    
            MappingFields.Add("LineSalesItemUnitPrice", string.Empty);
            MappingFields.Add("LineSalesItemQty", string.Empty);
            MappingFields.Add("LineSalesItemTaxCodeRefValue", string.Empty);
            MappingFields.Add("LineSalesItemServiceDate", string.Empty);     
            MappingFields.Add("LineDiscountAmount", string.Empty);
            MappingFields.Add("LineDiscountPercentBased", string.Empty);
            MappingFields.Add("LineDiscountPercent", string.Empty);
            MappingFields.Add("LineDiscountAccountRefName", string.Empty);

            MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            MappingFields.Add("CustomerFullyQualifiedName", string.Empty);
            MappingFields.Add("CustomerMemo", string.Empty);   
            MappingFields.Add("BillAddrLine1", string.Empty);
            MappingFields.Add("BillAddrLine2", string.Empty);
            MappingFields.Add("BillAddrLine3", string.Empty);
            MappingFields.Add("BillAddrCity", string.Empty);
            MappingFields.Add("BillAddrCountry", string.Empty);
            MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
            MappingFields.Add("BillAddrPostalCode", string.Empty);
            MappingFields.Add("BillAddrNote", string.Empty);
            MappingFields.Add("BillAddrLine4", string.Empty);
            MappingFields.Add("BillAddrLine5", string.Empty);
            MappingFields.Add("BillAddrLat", string.Empty);
            MappingFields.Add("BillAddrLong", string.Empty);
            MappingFields.Add("ShipAddrLine1", string.Empty);
            MappingFields.Add("ShipAddrLine2", string.Empty);
            MappingFields.Add("ShipAddrLine3", string.Empty);
            MappingFields.Add("ShipAddrCity", string.Empty);
            MappingFields.Add("ShipAddrCountry", string.Empty);
            MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
            MappingFields.Add("ShipAddrPostalCode", string.Empty);
            MappingFields.Add("ShipAddrNote", string.Empty);
            MappingFields.Add("ShipAddrLine4", string.Empty);
            MappingFields.Add("ShipAddrLine5", string.Empty);
            MappingFields.Add("ShipAddrLat", string.Empty);
            MappingFields.Add("ShipAddrLong", string.Empty);
            MappingFields.Add("ClassRef", string.Empty);
            MappingFields.Add("ShipMethodRef", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("TrackingNum", string.Empty);          
            MappingFields.Add("PrintStatus", string.Empty);
            MappingFields.Add("EmailStatus", string.Empty);
            //606
            MappingFields.Add("PrimaryPhone", string.Empty);
            MappingFields.Add("BillEmail", string.Empty);
            MappingFields.Add("Shipping", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("ShippingTaxCode", string.Empty);
            }
            MappingFields.Add("PaymentRefNumber", string.Empty);
            MappingFields.Add("PaymentMethodRef", string.Empty);
            MappingFields.Add("DepositToAccountRef", string.Empty);

            //Improvemnt::493
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("ApplyTaxAfterDiscount", string.Empty);
            }
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineInvoiceTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);    
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("DepartmentRefName", string.Empty);  
            MappingFields.Add("PrivateNote", string.Empty);          
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineSalesItemRefName", string.Empty);
            //486 bug Axis 12.0
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineSalesItemClassRef", string.Empty);
            MappingFields.Add("LineSalesItemUnitPrice", string.Empty);
            MappingFields.Add("LineSalesItemQty", string.Empty);
            MappingFields.Add("LineSalesItemTaxCodeRefValue", string.Empty);
            MappingFields.Add("LineSalesItemServiceDate", string.Empty); 
            MappingFields.Add("LineDiscountAmount", string.Empty);            
            MappingFields.Add("LineDiscountPercentBased", string.Empty);
            MappingFields.Add("LineDiscountPercent", string.Empty);
            MappingFields.Add("LineDiscountAccountRefName", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineDiscountAcctNum", string.Empty);
            MappingFields.Add("TxnTaxCodeRefName", string.Empty);           
            //MappingFields.Add("TaxPercentBased", string.Empty);
            //MappingFields.Add("TaxPercent", string.Empty);           
            MappingFields.Add("CustomerRefDisplayName", string.Empty);
            MappingFields.Add("CustomerMemo", string.Empty);
            MappingFields.Add("BillAddrLine1", string.Empty);
            MappingFields.Add("BillAddrLine2", string.Empty);
            MappingFields.Add("BillAddrLine3", string.Empty);
            MappingFields.Add("BillAddrCity", string.Empty);
            MappingFields.Add("BillAddrCountry", string.Empty);
            MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
            MappingFields.Add("BillAddrPostalCode", string.Empty);
            MappingFields.Add("BillAddrNote", string.Empty);
            MappingFields.Add("BillAddrLine4", string.Empty);
            MappingFields.Add("BillAddrLine5", string.Empty);
            MappingFields.Add("BillAddrLat", string.Empty);
            MappingFields.Add("BillAddrLong", string.Empty);
            MappingFields.Add("ShipAddrLine1", string.Empty);
            MappingFields.Add("ShipAddrLine2", string.Empty);
            MappingFields.Add("ShipAddrLine3", string.Empty);
            MappingFields.Add("ShipAddrCity", string.Empty);
            MappingFields.Add("ShipAddrCountry", string.Empty);
            MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
            MappingFields.Add("ShipAddrPostalCode", string.Empty);
            MappingFields.Add("ShipAddrNote", string.Empty);
            MappingFields.Add("ShipAddrLine4", string.Empty);
            MappingFields.Add("ShipAddrLine5", string.Empty);
            MappingFields.Add("ShipAddrLat", string.Empty);
            MappingFields.Add("ShipAddrLong", string.Empty);
            MappingFields.Add("SalesTermRefName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            //P Axis 13.2 : issue 712
            MappingFields.Add("AllowOnlineACHPayment", string.Empty); 
            MappingFields.Add("AllowOnlineCreditCardPayment", string.Empty);

            MappingFields.Add("ShipMethodRef", string.Empty);           
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("TrackingNum", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("ApplyTaxAfterDiscount", string.Empty);
            }
            //Issue No. 417
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("PrintStatus", string.Empty);
           
            MappingFields.Add("EmailStatus", string.Empty);
            //606
            MappingFields.Add("PrimaryPhone", string.Empty);
            MappingFields.Add("BillEmail", string.Empty);
            //issue 556 BillEmailCc , BillEmailBcc
            MappingFields.Add("BillEmailCc", string.Empty);
            MappingFields.Add("BillEmailBcc", string.Empty);

            MappingFields.Add("Shipping", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("ShippingTaxCode", string.Empty);
            }
            MappingFields.Add("DepositToAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("DepositToAcctNum", string.Empty);
            MappingFields.Add("Deposit", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "FR" || CommonUtilities.GetInstance().CountryVersion == "IN")
            {
                MappingFields.Add("TransactionLocationType", string.Empty);
            }

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineTimeActivityTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Id", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("NameOf", string.Empty);
            MappingFields.Add("EmployeeVendorRefName", string.Empty);
            MappingFields.Add("CustomerRefName", string.Empty);
            MappingFields.Add("DepartmentRefName", string.Empty);
            //Axis 718
            MappingFields.Add("PayrollItemRef", string.Empty);
            //Axis 718 ends
            MappingFields.Add("ItemRefName", string.Empty);
            MappingFields.Add("ClassRefName", string.Empty);
            MappingFields.Add("BillableStatus", string.Empty);
            MappingFields.Add("Taxable", string.Empty);
            MappingFields.Add("HourlyRate", string.Empty);
            MappingFields.Add("Duration", string.Empty);          
            MappingFields.Add("BreakDuration", string.Empty);         
            MappingFields.Add("StartTime", string.Empty);
            MappingFields.Add("EndTime", string.Empty);
            MappingFields.Add("Description", string.Empty);


            AddMappingFieldsToCollection(MappingFields);


        }

        private void addOnlineBillTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            //Axis 11.2 bug no.337
            MappingFields.Add("DepartmentRef", string.Empty);
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);          
            //End Axis 11.2 bug no.337
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineCustomerRef", string.Empty);
            MappingFields.Add("LineAccountRef", string.Empty);
            //improvement::493
            //MappingFields.Add("LineAcctNum", string.Empty);
            //Axis 11.2 bug no.337            
            MappingFields.Add("LineAccountClass", string.Empty);
            //End Axis 11.2 bug no.337
            MappingFields.Add("LineBillableStatus", string.Empty);
            MappingFields.Add("LineAccountTaxCodeRef", string.Empty);
            #region txnTaxDetails

            //if (CommonUtilities.GetInstance().CountryVersion == "US")
            //{
            //    MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            //}
            //MappingFields.Add("TotalTax", string.Empty);
            //MappingFields.Add("TaxLineAmount", string.Empty);
            //if (CommonUtilities.GetInstance().CountryVersion != "US")
            //{
            //    MappingFields.Add("TaxRateRef", string.Empty);
            //}
            //MappingFields.Add("TaxPercentBased", string.Empty);
            //MappingFields.Add("TaxPercent", string.Empty);

            //if (CommonUtilities.GetInstance().CountryVersion != "US")
            //{
            //    MappingFields.Add("NetAmountTaxable", string.Empty);             
            //}
            #endregion
            MappingFields.Add("VendorRefName", string.Empty);
            MappingFields.Add("APAccountRefName", string.Empty);  
            //Improvement::493
            //MappingFields.Add("APAcctNum", string.Empty);
            MappingFields.Add("SalesTermRefName", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            MappingFields.Add("LineItemDescription", string.Empty); 
            MappingFields.Add("LineItemAmount", string.Empty);       
            MappingFields.Add("LineItemRef", string.Empty);

            //bug 486 
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineClassRef", string.Empty);
            MappingFields.Add("LineItemQty", string.Empty);
            MappingFields.Add("LineItemUnitPrice", string.Empty);
            MappingFields.Add("LineItemTaxCodeRef", string.Empty);
            MappingFields.Add("LineItemCustomerRef", string.Empty);
            MappingFields.Add("LineItemBillableStatus", string.Empty);
            
            //BUG ;; 586
            MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "FR")
            {
                MappingFields.Add("TransactionLocationType", string.Empty);
            }
            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineJournalEntryTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);         
            MappingFields.Add("LineDescription", string.Empty);          
            MappingFields.Add("LineBillableStatus", string.Empty);   
            MappingFields.Add("LineEntityType", string.Empty);
            MappingFields.Add("LineEntityRef", string.Empty);
            MappingFields.Add("LineAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineAcctNum", string.Empty);
            MappingFields.Add("LineClassRef", string.Empty);
            MappingFields.Add("LineDepartmentRef", string.Empty);
            MappingFields.Add("LineTaxCodeRef", string.Empty);

            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("LineTaxApplicableOn", string.Empty);
                MappingFields.Add("LineTaxAmount", string.Empty);
            }

            MappingFields.Add("Adjustment", string.Empty);
            MappingFields.Add("Debit", string.Empty);
            MappingFields.Add("Credit", string.Empty);
            //Axis 701
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            //Axis 701 end

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlinePurchaseOrderTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);   
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineItemRef", string.Empty);
            //Axis 12.0 bug 486
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineClassRef", string.Empty);
            MappingFields.Add("LineItemQty", string.Empty);
            MappingFields.Add("LineItemUnitPrice", string.Empty);
            MappingFields.Add("LineItemTaxCodeRef", string.Empty);
            MappingFields.Add("LineCustomerRef", string.Empty);
            MappingFields.Add("LineBillableStatus", string.Empty);
            MappingFields.Add("LineAccountDescription", string.Empty);
            MappingFields.Add("LineAccountAmount", string.Empty);
            MappingFields.Add("LineAccountCustomerRef", string.Empty);
            MappingFields.Add("LineAccountClassRef", string.Empty);  
            MappingFields.Add("LineAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineAcctNum", string.Empty);
            MappingFields.Add("LineAccountBillableStatus", string.Empty);
            MappingFields.Add("LineAccountTaxCodeRef", string.Empty);
            MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            MappingFields.Add("TotalTax", string.Empty);
            MappingFields.Add("TaxLineAmount", string.Empty);
            MappingFields.Add("TaxPercentBased", string.Empty);
            MappingFields.Add("TaxPercent", string.Empty);
            MappingFields.Add("VendorRef", string.Empty);
            MappingFields.Add("APAccountRef", string.Empty);
            //P Axis 13.0 : issue 687
            MappingFields.Add("POEmail", string.Empty);
            //Improvement::493
            //MappingFields.Add("APAcctNum", string.Empty);
            MappingFields.Add("ClassRef", string.Empty);
            MappingFields.Add("SalesTermRef", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("VendorAddrLine1", string.Empty);
            MappingFields.Add("VendorAddrLine2", string.Empty);
            MappingFields.Add("VendorAddrLine3", string.Empty);
            MappingFields.Add("VendorAddrCity", string.Empty);
            MappingFields.Add("VendorAddrCountry", string.Empty);
            MappingFields.Add("VendorAddrSubDivisionCode", string.Empty);
            MappingFields.Add("VendorAddrPostalCode", string.Empty);
            MappingFields.Add("VendorAddrNote", string.Empty);
            MappingFields.Add("VendorAddrLine4", string.Empty);
            MappingFields.Add("VendorAddrLine5", string.Empty);
            MappingFields.Add("VendorAddrLat", string.Empty);
            MappingFields.Add("VendorAddrLong", string.Empty);
            MappingFields.Add("ShipAddrLine1", string.Empty);
            MappingFields.Add("ShipAddrLine2", string.Empty);
            MappingFields.Add("ShipAddrLine3", string.Empty);
            MappingFields.Add("ShipAddrCity", string.Empty);
            MappingFields.Add("ShipAddrCountry", string.Empty);
            MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
            MappingFields.Add("ShipAddrPostalCode", string.Empty);
            MappingFields.Add("ShipAddrNote", string.Empty);
            MappingFields.Add("ShipAddrLine4", string.Empty);
            MappingFields.Add("ShipAddrLine5", string.Empty);
            MappingFields.Add("ShipAddrLat", string.Empty);
            MappingFields.Add("ShipAddrLong", string.Empty);            
            MappingFields.Add("ShipMethodRef", string.Empty);
            MappingFields.Add("POStatus", string.Empty);
            //bug no. 383
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            //Add constant values for text file          

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlinePurchaseTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("PaymentMethodRef", string.Empty);

            MappingFields.Add("DepartmentRef", string.Empty);        
            MappingFields.Add("CurrencyRef", string.Empty);//bug no. 418
            MappingFields.Add("ExchangeRate", string.Empty);//bug no. 418
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineItemRef", string.Empty);

            //Axis 12.0 bug 486
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineClassRef", string.Empty);
            MappingFields.Add("LineItemQty", string.Empty);
            MappingFields.Add("LineItemUnitPrice", string.Empty);
            MappingFields.Add("LineItemTaxCodeRef", string.Empty);
            MappingFields.Add("LineCustomerRef", string.Empty);
            MappingFields.Add("LineBillableStatus", string.Empty);
            MappingFields.Add("LineAccountDescription", string.Empty);
            MappingFields.Add("LineAccountAmount", string.Empty);
            MappingFields.Add("LineAccountCustomerRef", string.Empty);
            MappingFields.Add("LineAccountClassRef", string.Empty);
            MappingFields.Add("LineAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineAcctNum", string.Empty);
            MappingFields.Add("LineAccountBillableStatus", string.Empty);
            MappingFields.Add("LineAccountTaxCodeRef", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("TxnTaxCodeRefName", string.Empty);
                MappingFields.Add("TotalTax", string.Empty);
                MappingFields.Add("TaxLineAmount", string.Empty);
                MappingFields.Add("TaxPercentBased", string.Empty);
                MappingFields.Add("TaxPercent", string.Empty);
            }
            MappingFields.Add("AccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("AcctNum", string.Empty);
            MappingFields.Add("EntityType", string.Empty);
            MappingFields.Add("EntityRef", string.Empty);
           
            OnlineImportType importType = CommonUtilities.GetInstance().SelectedOnlineImportType;
            if (importType != OnlineImportType.CreditCardPurchase)
            {
                MappingFields.Add("PrintStatus", string.Empty);
            }
            if (importType != OnlineImportType.CheckPurchase)
            {
                MappingFields.Add("Credit", string.Empty);
            }
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            if (CommonUtilities.GetInstance().CountryVersion == "FR")
            {
                MappingFields.Add("TransactionLocationType", string.Empty);
            }
            //Add constant values for text file          

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineEstimateTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("DepartmentRef", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty); 
            MappingFields.Add("TxnStatus", string.Empty);          
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineSalesItemRefName", string.Empty);
            //Axis 12.0 bug 486
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineSalesItemClassRef", string.Empty);
            MappingFields.Add("LineSalesItemUnitPrice", string.Empty);
            MappingFields.Add("LineSalesItemQty", string.Empty);
            MappingFields.Add("LineSalesItemTaxCodeRefValue", string.Empty);
            MappingFields.Add("LineSalesItemServiceDate", string.Empty);
            MappingFields.Add("LineDiscountAmount", string.Empty);
            MappingFields.Add("LineDiscountPercentBased", string.Empty);
            MappingFields.Add("LineDiscountPercent", string.Empty);
            MappingFields.Add("LineDiscountAccountRefName", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineDiscountAcctNum", string.Empty);

            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            }
          
            MappingFields.Add("CustomerFullyQualifiedName", string.Empty);
            MappingFields.Add("CustomerMemo", string.Empty);
            MappingFields.Add("BillAddrLine1", string.Empty);
            MappingFields.Add("BillAddrLine2", string.Empty);
            MappingFields.Add("BillAddrLine3", string.Empty);
            MappingFields.Add("BillAddrCity", string.Empty);
            MappingFields.Add("BillAddrCountry", string.Empty);
            MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
            MappingFields.Add("BillAddrPostalCode", string.Empty);
            MappingFields.Add("BillAddrNote", string.Empty);
            MappingFields.Add("BillAddrLine4", string.Empty);
            MappingFields.Add("BillAddrLine5", string.Empty);
            MappingFields.Add("BillAddrLat", string.Empty);
            MappingFields.Add("BillAddrLong", string.Empty);
            MappingFields.Add("ShipAddrLine1", string.Empty);
            MappingFields.Add("ShipAddrLine2", string.Empty);
            MappingFields.Add("ShipAddrLine3", string.Empty);
            MappingFields.Add("ShipAddrCity", string.Empty);
            MappingFields.Add("ShipAddrCountry", string.Empty);
            MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
            MappingFields.Add("ShipAddrPostalCode", string.Empty);
            MappingFields.Add("ShipAddrNote", string.Empty);
            MappingFields.Add("ShipAddrLine4", string.Empty);
            MappingFields.Add("ShipAddrLine5", string.Empty);
            MappingFields.Add("ShipAddrLat", string.Empty);
            MappingFields.Add("ShipAddrLong", string.Empty);
            MappingFields.Add("ClassRef", string.Empty);
            MappingFields.Add("SalesTermRef", string.Empty);
            MappingFields.Add("ShipMethodRef", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("ApplyTaxAfterDiscount", string.Empty);
            }
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("PrintStatus", string.Empty);
            MappingFields.Add("EmailStatus", string.Empty);
            //606
            MappingFields.Add("PrimaryPhone", string.Empty);
            MappingFields.Add("BillEmail", string.Empty);
            MappingFields.Add("Shipping", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("ShippingTaxCode", string.Empty);
            }
            MappingFields.Add("Expiration", string.Empty);
            MappingFields.Add("AcceptedBy", string.Empty);
            MappingFields.Add("AcceptedDate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            if (CommonUtilities.GetInstance().CountryVersion == "FR")
            {
                MappingFields.Add("TransactionLocationType", string.Empty);
            }

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlinePaymentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("TxnStatus", string.Empty);            
            MappingFields.Add("CustomerRef", string.Empty);
            MappingFields.Add("ARAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("ARAcctNum", string.Empty);
            MappingFields.Add("DepositeToAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("DepositeToAcctNum", string.Empty);
            MappingFields.Add("PaymentMethodRef", string.Empty);
            MappingFields.Add("PaymentRefNum", string.Empty);
            MappingFields.Add("CreditCard", string.Empty);//
            MappingFields.Add("TotalAmount", string.Empty);//totalpayment
            MappingFields.Add("ProcessPayment", string.Empty);//
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            //Bug::575
            //MappingFields.Add("PaymentAmount", string.Empty);
            MappingFields.Add("ApplyToInvoiceRef", string.Empty);
            MappingFields.Add("LinePaymentAmount", string.Empty);
            MappingFields.Add("UnappliedAmt", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineVendorCreditTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("DepartmentRef", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineItemRef", string.Empty);
            //Axis 12.0 bug486
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("LineClassRef", string.Empty);
            MappingFields.Add("LineItemQty", string.Empty);
            MappingFields.Add("LineItemUnitPrice", string.Empty);
            MappingFields.Add("LineItemTaxCodeRef", string.Empty);
            MappingFields.Add("LineCustomerRef", string.Empty);
            MappingFields.Add("LineBillableStatus", string.Empty);
            MappingFields.Add("LineAccountDescription", string.Empty);
            MappingFields.Add("LineAccountAmount", string.Empty);
            MappingFields.Add("LineAccountCustomerRef", string.Empty);
            MappingFields.Add("LineAccountClassRef", string.Empty);
            MappingFields.Add("LineAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineAcctNum", string.Empty);
            MappingFields.Add("LineAccountBillableStatus", string.Empty);
            MappingFields.Add("LineAccountTaxCodeRef", string.Empty);

            //586
            MappingFields.Add("TxnTaxCodeRefName", string.Empty);

            MappingFields.Add("VendorRef", string.Empty);
            MappingFields.Add("APAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("APAcctNum", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            if (CommonUtilities.GetInstance().CountryVersion == "FR")
            {
                MappingFields.Add("TransactionLocationType", string.Empty);
            }


            AddMappingFieldsToCollection(MappingFields);


        }

        private void addOnlineBillPaymentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("DepartmentRef", string.Empty);
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("LinePaymentAmount", string.Empty);
            MappingFields.Add("ApplytoBillRef", string.Empty);
            MappingFields.Add("LineDiscountAmount", string.Empty);
            MappingFields.Add("LineDiscountPercentBased", string.Empty);
            MappingFields.Add("LineDiscountPercent", string.Empty);
            MappingFields.Add("LineDiscountAccountRefName", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineDiscountAcctNum", string.Empty);
            MappingFields.Add("VendorRef", string.Empty);
            MappingFields.Add("APAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("APAcctNum", string.Empty);
            MappingFields.Add("PayType", string.Empty);        
            MappingFields.Add("BankAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("BankAcctNum", string.Empty);
            MappingFields.Add("Printstatus", string.Empty);        
            MappingFields.Add("CCAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("CCAcctNum", string.Empty);
            MappingFields.Add("Totalamt", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineCreditMemoTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("TxnDate", string.Empty);
            MappingFields.Add("PrivateNote", string.Empty);
            MappingFields.Add("CustomFieldName1", string.Empty);
            MappingFields.Add("CustomFieldValue1", string.Empty);
            MappingFields.Add("CustomFieldName2", string.Empty);
            MappingFields.Add("CustomFieldValue2", string.Empty);
            MappingFields.Add("CustomFieldName3", string.Empty);
            MappingFields.Add("CustomFieldValue3", string.Empty);    
            MappingFields.Add("TxnStatus", string.Empty);
            MappingFields.Add("LineDescription", string.Empty);
            MappingFields.Add("LineAmount", string.Empty);
            MappingFields.Add("LineSalesItemRefName", string.Empty);
            //Axis 12.0 bug486
             MappingFields.Add("SKU", string.Empty);
            MappingFields.Add("LineSalesItemClassRef", string.Empty);
            MappingFields.Add("LineSalesItemUnitPrice", string.Empty);
            MappingFields.Add("LineSalesItemQty", string.Empty);
            MappingFields.Add("LineSalesItemTaxCodeRefValue", string.Empty);
            MappingFields.Add("LineSalesItemServiceDate", string.Empty);
            MappingFields.Add("LineDiscountAmount", string.Empty);
            MappingFields.Add("LineDiscountPercentBased", string.Empty);
            MappingFields.Add("LineDiscountPercent", string.Empty);
            MappingFields.Add("LineDiscountAccountRefName", string.Empty);
            //Improvement::493
            //MappingFields.Add("LineDiscountAcctNum", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("TxnTaxCodeRefName", string.Empty);
            }
            //MappingFields.Add("TotalTax", string.Empty);
            //MappingFields.Add("TaxLineAmount", string.Empty);
            //MappingFields.Add("TaxPercentBased", string.Empty);
            //MappingFields.Add("TaxPercent", string.Empty);
            MappingFields.Add("CustomerFullyQualifiedName", string.Empty);
            MappingFields.Add("CustomerMemo", string.Empty);
            MappingFields.Add("BillAddrLine1", string.Empty);
            MappingFields.Add("BillAddrLine2", string.Empty);
            MappingFields.Add("BillAddrLine3", string.Empty);
            MappingFields.Add("BillAddrCity", string.Empty);
            MappingFields.Add("BillAddrCountry", string.Empty);
            MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
            MappingFields.Add("BillAddrPostalCode", string.Empty);
            MappingFields.Add("BillAddrNote", string.Empty);
            MappingFields.Add("BillAddrLine4", string.Empty);
            MappingFields.Add("BillAddrLine5", string.Empty);
            MappingFields.Add("BillAddrLat", string.Empty);
            MappingFields.Add("BillAddrLong", string.Empty);
            MappingFields.Add("ShipAddrLine1", string.Empty);
            MappingFields.Add("ShipAddrLine2", string.Empty);
            MappingFields.Add("ShipAddrLine3", string.Empty);
            MappingFields.Add("ShipAddrCity", string.Empty);
            MappingFields.Add("ShipAddrCountry", string.Empty);
            MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
            MappingFields.Add("ShipAddrPostalCode", string.Empty);
            MappingFields.Add("ShipAddrNote", string.Empty);
            MappingFields.Add("ShipAddrLine4", string.Empty);
            MappingFields.Add("ShipAddrLine5", string.Empty);
            MappingFields.Add("ShipAddrLat", string.Empty);
            MappingFields.Add("ShipAddrLong", string.Empty);
            MappingFields.Add("ClassRef", string.Empty);
            MappingFields.Add("SalesTermRef", string.Empty);
            MappingFields.Add("ShipDate", string.Empty);
            MappingFields.Add("TrackingNum", string.Empty);
            MappingFields.Add("PrintStatus", string.Empty);
            MappingFields.Add("EmailStatus", string.Empty);
            MappingFields.Add("BillEmail", string.Empty);
            MappingFields.Add("RemainingCredit", string.Empty);
            MappingFields.Add("PaymentMethodRef", string.Empty);
            MappingFields.Add("Shipping", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("ShippingTaxCode", string.Empty);
            }
            MappingFields.Add("DepositToAccountRef", string.Empty);
            //Improvement::493
            //MappingFields.Add("DepositToAcctNum", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion == "US")
            {
                MappingFields.Add("ApplyTaxAfterDiscount", string.Empty);
            }
            MappingFields.Add("CurrencyRef", string.Empty);
            MappingFields.Add("ExchangeRate", string.Empty);
            if (CommonUtilities.GetInstance().CountryVersion != "US")
            {
                MappingFields.Add("GlobalTaxCalculation", string.Empty);
            }
            if (CommonUtilities.GetInstance().CountryVersion == "FR")
            {
                MappingFields.Add("TransactionLocationType", string.Empty);
            }

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addOnlineEmployeeTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("EmployeeNumber", string.Empty);
             MappingFields.Add("Organization", string.Empty);
             MappingFields.Add("Title", string.Empty);
             MappingFields.Add("GivenName", string.Empty);
             MappingFields.Add("MiddleName", string.Empty);
             MappingFields.Add("FamilyName", string.Empty);
             MappingFields.Add("Suffix", string.Empty);
             MappingFields.Add("DisplayName", string.Empty);
             MappingFields.Add("PrintOnCheckName", string.Empty);
             MappingFields.Add("Active", string.Empty);
             MappingFields.Add("PrimaryPhone", string.Empty);
             MappingFields.Add("Mobile", string.Empty);
             MappingFields.Add("PrimaryEmailAddr", string.Empty);           
             MappingFields.Add("PrimaryAddrLine1", string.Empty);
             MappingFields.Add("PrimaryAddrLine2", string.Empty);
             MappingFields.Add("PrimaryAddrLine3", string.Empty);
             MappingFields.Add("PrimaryAddrCity", string.Empty);
             MappingFields.Add("PrimaryAddrCountry", string.Empty);
             MappingFields.Add("PrimaryAddrSubDivisionCode", string.Empty);
             MappingFields.Add("PrimaryAddrPostalCode", string.Empty);
             MappingFields.Add("PrimaryAddrNote", string.Empty);
             MappingFields.Add("PrimaryAddrLine4", string.Empty);
             MappingFields.Add("PrimaryAddrLine5", string.Empty);
             MappingFields.Add("PrimaryAddrLat", string.Empty);
             MappingFields.Add("PrimaryAddrLong", string.Empty);  
             MappingFields.Add("SSN", string.Empty);
             MappingFields.Add("BillableTime", string.Empty);
             MappingFields.Add("BillRate", string.Empty);
             MappingFields.Add("BirthDate", string.Empty);
             MappingFields.Add("Gender", string.Empty);
             MappingFields.Add("HiredDate", string.Empty);
             MappingFields.Add("ReleasedDate", string.Empty);


            AddMappingFieldsToCollection(MappingFields);

        }

        //bug no. 404
        private void AddOnlineItemsTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Name", string.Empty);
             MappingFields.Add("Description", string.Empty);
             MappingFields.Add("Active", string.Empty);
             MappingFields.Add("FullyQualifiedName", string.Empty);
            //P Axis 13.1 : issue 629
            MappingFields.Add("SKU", string.Empty);

            MappingFields.Add("Taxable", string.Empty);             
             MappingFields.Add("SalesTaxIncluded", string.Empty);            
             MappingFields.Add("UnitPrice", string.Empty);
             MappingFields.Add("RatePercent", string.Empty);  
             MappingFields.Add("Type", string.Empty);
             MappingFields.Add("IncomeAccountRef", string.Empty);
             //Improvement::493
             //MappingFields.Add("IncomeAcctNum", string.Empty);
             MappingFields.Add("PurchaseDesc", string.Empty);
             MappingFields.Add("PurchaseTaxIncluded", string.Empty);
             MappingFields.Add("PurchaseCost", string.Empty);
             MappingFields.Add("ExpenseAccountRef", string.Empty);
             //Improvement::493
             //MappingFields.Add("ExpenseAcctNum", string.Empty);
             MappingFields.Add("AssetAccountRef", string.Empty);
             //Improvement::493
             //MappingFields.Add("AssetAcctNum", string.Empty);
             MappingFields.Add("TrackQtyOnHand", string.Empty);
             MappingFields.Add("QtyOnHand", string.Empty);
             MappingFields.Add("SalesTaxCodeRef", string.Empty);
             MappingFields.Add("PurchaseTaxCodeRef", string.Empty);
             MappingFields.Add("InvStartDate", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }
        private void addOnlineCustomerTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("GivenName", string.Empty);
             MappingFields.Add("MiddleName", string.Empty);
             MappingFields.Add("Title", string.Empty);
             MappingFields.Add("FamilyName", string.Empty);
             MappingFields.Add("Suffix", string.Empty);
             MappingFields.Add("DisplayName", string.Empty);
             MappingFields.Add("FullyQualifiedName", string.Empty);
             MappingFields.Add("CompanyName", string.Empty);
            
             MappingFields.Add("IsActive", string.Empty);
             MappingFields.Add("PrimaryPhone", string.Empty);
             MappingFields.Add("AlternatePhone", string.Empty);
             MappingFields.Add("Mobile", string.Empty);
             MappingFields.Add("Fax", string.Empty);
             MappingFields.Add("PrimaryEmailAddr", string.Empty);
             MappingFields.Add("WebAddr", string.Empty);
             MappingFields.Add("DefaultTaxCodeRef", string.Empty);
             MappingFields.Add("Taxable", string.Empty);
             MappingFields.Add("BillAddr1", string.Empty);
             MappingFields.Add("BillAddr2", string.Empty);
             MappingFields.Add("BillAddr3", string.Empty);
             MappingFields.Add("BillAddr4", string.Empty);
             MappingFields.Add("BillAddr5", string.Empty);
             MappingFields.Add("BillCity", string.Empty);
             MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
             MappingFields.Add("BillPostalCode", string.Empty);
             MappingFields.Add("BillCountry", string.Empty);
             MappingFields.Add("BillNote", string.Empty);
             MappingFields.Add("BillAddrLat", string.Empty);
             MappingFields.Add("BillAddrLong", string.Empty);
             MappingFields.Add("ShipAddr1", string.Empty);
             MappingFields.Add("ShipAddr2", string.Empty);
             MappingFields.Add("ShipAddr3", string.Empty);
             MappingFields.Add("ShipAddr4", string.Empty);
             MappingFields.Add("ShipAddr5", string.Empty);
             MappingFields.Add("ShipCity", string.Empty);
             MappingFields.Add("ShipAddrSubDivisionCode", string.Empty);
             //MappingFields.Add("ShipState", string.Empty);
             MappingFields.Add("ShipPostalCode", string.Empty);
             MappingFields.Add("ShipCountry", string.Empty);
             MappingFields.Add("ShipNote", string.Empty);
         
             MappingFields.Add("ShipAddrLat", string.Empty);
             MappingFields.Add("ShipAddrLong", string.Empty);
             MappingFields.Add("Notes", string.Empty);
             MappingFields.Add("Job", string.Empty);
             MappingFields.Add("ParentRef", string.Empty);
             MappingFields.Add("Level", string.Empty);
             MappingFields.Add("SalesTermRef", string.Empty);
             MappingFields.Add("PaymentMethodRef", string.Empty);
             MappingFields.Add("OpenBalance", string.Empty);
             MappingFields.Add("OpenBalanceDate", string.Empty);
             MappingFields.Add("CurrencyRef", string.Empty);
             MappingFields.Add("PreferredDeliveryMethod", string.Empty);
             MappingFields.Add("ResaleNum", string.Empty);
            AddMappingFieldsToCollection(MappingFields);

        }

        /// <summary>
        /// Axis 11.4 bug no.436
        /// </summary>


        private void addOnlineDepositTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("DocNumber", string.Empty);
             MappingFields.Add("TxnDate", string.Empty);
             MappingFields.Add("DepartmentRefName", string.Empty);
             MappingFields.Add("CurrencyRefName", string.Empty);
             MappingFields.Add("PrivateNote", string.Empty);
             MappingFields.Add("TxnStatus", string.Empty);
             //MappingFields.Add("AttachableRefEntityRef", string.Empty);

             MappingFields.Add("CustomFieldName1", string.Empty);
             MappingFields.Add("CustomFieldValue1", string.Empty);
             MappingFields.Add("CustomFieldName2", string.Empty);
             MappingFields.Add("CustomFieldValue2", string.Empty);
             MappingFields.Add("CustomFieldName3", string.Empty);
             MappingFields.Add("CustomFieldValue3", string.Empty);

             MappingFields.Add("LineDescription", string.Empty);
             MappingFields.Add("LineAmount", string.Empty);
             MappingFields.Add("LinkedtxnDocNumber", string.Empty);
             MappingFields.Add("LinkedtxnTxnType", string.Empty);           
             MappingFields.Add("DepositLineEntityName", string.Empty);
             MappingFields.Add("DepositLineEntityType", string.Empty);
             MappingFields.Add("DepositLineClassRefName", string.Empty);
             MappingFields.Add("DepositLineAccountRefName", string.Empty);
             //Improvement::493
             //MappingFields.Add("DepositLineAcctNum", string.Empty);
             MappingFields.Add("DepositLinePaymentMethodRefName", string.Empty);
             MappingFields.Add("DepositLineCheckNum", string.Empty);
             MappingFields.Add("DepositLineTxnType", string.Empty);

             //bug 476
             if (CommonUtilities.GetInstance().CountryVersion != "US")
             {
                 MappingFields.Add("DepositLineTaxCodeRef", string.Empty);
                 MappingFields.Add("DepositLineTaxApplicableOn", string.Empty);
             }

             MappingFields.Add("LineCustomFieldName1", string.Empty);
             MappingFields.Add("LineCustomFieldValue1", string.Empty);
             MappingFields.Add("LineCustomFieldName2", string.Empty);
             MappingFields.Add("LineCustomFieldValue2", string.Empty);
             MappingFields.Add("LineCustomFieldName3", string.Empty);
             MappingFields.Add("LineCustomFieldValue3", string.Empty);

             MappingFields.Add("DepositToAccountRefName", string.Empty);
             //Improvement::493
             //MappingFields.Add("DepositToAcctNum", string.Empty);

             MappingFields.Add("TxnTaxCodeRef", string.Empty);

             MappingFields.Add("CashBackAccountRef", string.Empty);
             //Improvement::493
             //MappingFields.Add("CashBackAcctNum", string.Empty);

             MappingFields.Add("CashBackAmount", string.Empty);
             MappingFields.Add("CashBackMemo", string.Empty);

             MappingFields.Add("TxnSource", string.Empty);

             MappingFields.Add("TotalAmt", string.Empty);

             //bug 476 add ExchangeRate,GlobalTaxCalculation

             MappingFields.Add("ExchangeRate", string.Empty);
             MappingFields.Add("GlobalTaxCalculation", string.Empty);


            AddMappingFieldsToCollection(MappingFields);

        }

        //Improvemnt 559
        private void addOnlineAccountTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Name", string.Empty);
             MappingFields.Add("SubAccount", string.Empty);
             MappingFields.Add("ParentRef", string.Empty);
             MappingFields.Add("Description", string.Empty);
             MappingFields.Add("FullyQualifiedName", string.Empty);
             MappingFields.Add("Active", string.Empty);
             MappingFields.Add("Classification", string.Empty);
             MappingFields.Add("AccountType", string.Empty);
             MappingFields.Add("AccountSubType", string.Empty);
             MappingFields.Add("AcctNum", string.Empty);
             MappingFields.Add("CurrentBalance", string.Empty);
             MappingFields.Add("CurrentBalanceWithSubAccounts", string.Empty);
             MappingFields.Add("CurrencyRef", string.Empty);
             MappingFields.Add("TaxCodeRef", string.Empty);



            AddMappingFieldsToCollection(MappingFields);

        }

        //592
        private void addOnlineTransferTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Id", string.Empty);
             MappingFields.Add("TxnDate", string.Empty);
             MappingFields.Add("PrivateNote", string.Empty);
             MappingFields.Add("FromAccountRef", string.Empty);
             MappingFields.Add("ToAccountRef", string.Empty);
             MappingFields.Add("Amount", string.Empty);
             MappingFields.Add("TransactionLocationType", string.Empty);


            AddMappingFieldsToCollection(MappingFields);


        }

        //591
        private void addOnlineVendorTag()
         {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Title", string.Empty);
       
              MappingFields.Add("GivenName", string.Empty);
              MappingFields.Add("MiddleName", string.Empty);
              MappingFields.Add("FamilyName", string.Empty);
              MappingFields.Add("Suffix", string.Empty);
              MappingFields.Add("DisplayName", string.Empty);
              MappingFields.Add("CompanyName", string.Empty);
              MappingFields.Add("PrintOnCheckName", string.Empty);
              MappingFields.Add("Active", string.Empty);
              MappingFields.Add("PrimaryPhone", string.Empty);
              MappingFields.Add("AlternatePhone", string.Empty);
              MappingFields.Add("Mobile", string.Empty);
              MappingFields.Add("Fax", string.Empty);

              MappingFields.Add("PrimaryEmailAddr", string.Empty);
              MappingFields.Add("WebAddr", string.Empty);
                          
              MappingFields.Add("BillAddr1", string.Empty);
              MappingFields.Add("BillAddr2", string.Empty);
              MappingFields.Add("BillAddr3", string.Empty);
              MappingFields.Add("BillAddr4", string.Empty);
              MappingFields.Add("BillAddr5", string.Empty);
              MappingFields.Add("BillCity", string.Empty);
              MappingFields.Add("BillAddrSubDivisionCode", string.Empty);
              MappingFields.Add("BillPostalCode", string.Empty);
              MappingFields.Add("BillCountry", string.Empty);
              MappingFields.Add("BillNote", string.Empty);
              MappingFields.Add("BillAddrLat", string.Empty);
              MappingFields.Add("BillAddrLong", string.Empty);


              MappingFields.Add("OtherContactInfo", string.Empty);
              MappingFields.Add("TaxIdentifier", string.Empty);
              MappingFields.Add("TermRef", string.Empty);
              MappingFields.Add("Balance", string.Empty);
              MappingFields.Add("AcctNum", string.Empty);
              MappingFields.Add("Vendor1099", string.Empty);
              MappingFields.Add("CurrencyRef", string.Empty);
              //MappingFields.Add("TaxReportingBasis", string.Empty);
              MappingFields.Add("APAccountRef", string.Empty);

            AddMappingFieldsToCollection(MappingFields);


        }

        private void addOnlineAttachmentTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("FilePath", string.Empty);
            MappingFields.Add("AttachmentNote", string.Empty);
            MappingFields.Add("EntityType", string.Empty);
            MappingFields.Add("DocNumber", string.Empty);
            MappingFields.Add("FullName", string.Empty);

            AddMappingFieldsToCollection(MappingFields);


        }
        #endregion

        #region Xero Transactions Import Types
        //Axis 10 Changes Contacts add tag.
        private void addContactsTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();
            MappingFields.Add("ContactID", string.Empty);
            // MappingFields.Add("ContactNumber", string.Empty);
            MappingFields.Add("ContactStatus", string.Empty);
            MappingFields.Add("Name", string.Empty);
            MappingFields.Add("FirstName", string.Empty);
            MappingFields.Add("LastName", string.Empty);
            MappingFields.Add("EmailAddress", string.Empty);
            MappingFields.Add("SkypeUserName", string.Empty);
            MappingFields.Add("BankAccountDetails", string.Empty);
            MappingFields.Add("TaxNumber", string.Empty);
            MappingFields.Add("AccountsReceivableTaxType", string.Empty);
            MappingFields.Add("AccountsPayableTaxType", string.Empty);
            MappingFields.Add("AddressLine1", string.Empty);
            MappingFields.Add("AddressLine2", string.Empty);
            MappingFields.Add("AddressLine3", string.Empty);
            MappingFields.Add("AddressLine4", string.Empty);
            MappingFields.Add("City", string.Empty);
            MappingFields.Add("Region", string.Empty);
            MappingFields.Add("PostalCode", string.Empty);
            MappingFields.Add("Country", string.Empty);
            MappingFields.Add("AttentionTo", string.Empty);
            MappingFields.Add("AddressType", string.Empty);
            MappingFields.Add("PhoneType", string.Empty);
            MappingFields.Add("PhoneNumber", string.Empty);
            MappingFields.Add("PhoneAreaCode", string.Empty);
            MappingFields.Add("PhoneCountryCode", string.Empty);
            //MappingFields.Add("UpdatedDateUTC", string.Empty);
            //MappingFields.Add("ContactGroups", string.Empty);
            MappingFields.Add("IsSupplier", string.Empty);
            MappingFields.Add("IsCustomer", string.Empty);
            MappingFields.Add("DefaultCurrency", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }

        private void addItemsTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Code", string.Empty);
            MappingFields.Add("Description", string.Empty);
            MappingFields.Add("PurchaseDetailsUnitPrice", string.Empty);
            MappingFields.Add("PurchaseDetailsAccountCode", string.Empty);
            // MappingFields.Add("PurchaseDetailsTaxType", string.Empty);
            MappingFields.Add("SalesDetailsUnitPrice", string.Empty);
            MappingFields.Add("SalesDetailsAccountCode", string.Empty);
            //MappingFields.Add("SalesDetailsTaxType", string.Empty);
            AddMappingFieldsToCollection(MappingFields);


        }

        private void addJournalTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            MappingFields.Add("Narration", string.Empty);
            MappingFields.Add("Date", string.Empty);
            MappingFields.Add("Status", string.Empty);
            MappingFields.Add("Url", string.Empty);
            MappingFields.Add("LineAmountTypes", string.Empty);
            // MappingFields.Add("ShowOnCashBasisReports", string.Empty);
            MappingFields.Add("JournalLinesLineAmount", string.Empty);
            MappingFields.Add("JournalLinesAccountCode", string.Empty);
            MappingFields.Add("JournalLinesDescription", string.Empty);
            MappingFields.Add("JournalLinesTaxType", string.Empty);
            //Axis 141
            MappingFields.Add("TrackingName1", string.Empty);
            MappingFields.Add("TrackingOption1", string.Empty);
            MappingFields.Add("TrackingName2", string.Empty);
            MappingFields.Add("TrackingOption2", string.Empty);
            //Axis 141

            AddMappingFieldsToCollection(MappingFields);


        }

        private void addBankTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //MappingFields.Add("Type", string.Empty);
            MappingFields.Add("ContactName", string.Empty);
            MappingFields.Add("LineItemsDescription", string.Empty);
            MappingFields.Add("LineItemsQuantity", string.Empty);
            MappingFields.Add("LineItemsUnitAmount", string.Empty);
            MappingFields.Add("LineItemsAccountCode", string.Empty);
            MappingFields.Add("LineItemsTaxType", string.Empty);
            MappingFields.Add("LineItemsLineAmount", string.Empty);
            //Axis 141
            MappingFields.Add("TrackingName1", string.Empty);
            MappingFields.Add("TrackingOption1", string.Empty);
            MappingFields.Add("TrackingName2", string.Empty);
            MappingFields.Add("TrackingOption2", string.Empty);
            //Axis 141
            MappingFields.Add("BankAccountCode", string.Empty);
            MappingFields.Add("Date", string.Empty);
            MappingFields.Add("LineAmountTypes", string.Empty);
            MappingFields.Add("Reference", string.Empty);
            MappingFields.Add("CurrencyRate", string.Empty);
            MappingFields.Add("Url", string.Empty);
            MappingFields.Add("Status", string.Empty);
            MappingFields.Add("SubTotal", string.Empty);
            MappingFields.Add("TotalTax", string.Empty);
            MappingFields.Add("Total", string.Empty);


            AddMappingFieldsToCollection(MappingFields);

        }

        private void addXeroInvoiceTag()
        {
            NameValueCollection MappingFields = new NameValueCollection();

            //MappingFields.Add("Type", string.Empty); "Type"= ACCREC for customer & ACCPAY for supplier
            MappingFields.Add("ContactName", string.Empty);
            MappingFields.Add("LineItemsDescription", string.Empty);
            MappingFields.Add("LineItemsQuantity", string.Empty);
            MappingFields.Add("LineItemsUnitAmount", string.Empty);
            MappingFields.Add("LineItemsItemCode", string.Empty);
            MappingFields.Add("LineItemsAccountCode", string.Empty);
            MappingFields.Add("LineItemsTaxType", string.Empty);
            MappingFields.Add("LineItemsTaxAmount", string.Empty);
            MappingFields.Add("LineItemsLineAmount", string.Empty);
            //Axis 141
            MappingFields.Add("TrackingName1", string.Empty);
            MappingFields.Add("TrackingOption1", string.Empty);
            MappingFields.Add("TrackingName2", string.Empty);
            MappingFields.Add("TrackingOption2", string.Empty);
            //Axis 141
            MappingFields.Add("Date", string.Empty);
            MappingFields.Add("DueDate", string.Empty);
            MappingFields.Add("LineAmountTypes", string.Empty);
            MappingFields.Add("InvoiceNumber", string.Empty);
            MappingFields.Add("Reference", string.Empty);
            MappingFields.Add("Url", string.Empty);
            MappingFields.Add("CurrencyCode", string.Empty);
            MappingFields.Add("Status", string.Empty);
            MappingFields.Add("SubTotal", string.Empty);
            MappingFields.Add("TotalTax", string.Empty);
            MappingFields.Add("Total", string.Empty);

            AddMappingFieldsToCollection(MappingFields);

        }
        #endregion

        public void Save()
        {
            if (new UserMappingsCollection().IsExists(this.m_Name))
            {
                if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
                    this.Update();
                else
                    this.UpdateXSL();
            }
            else
            {
                if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
                    this.CreateNew();
                else
                    this.CreateNewXSL();
            }
        }

        /// <summary>
        /// Create new XSL file
        /// </summary>
        private void CreateNewXSL()
        {
            m_settingsPath = Application.StartupPath;
            m_settingsPath += "\\" + this.m_Name + ".xsl";

            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (m_settingsPath.Contains("Program Files (x86)"))
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (m_settingsPath.Contains("Program Files (x86)"))
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }


            if (!File.Exists(m_settingsPath))
            {
                FileStream strm = File.Create(m_settingsPath);
                strm.Close();
            }

            //create instance of stream writter
            StreamWriter wrt = new StreamWriter(m_settingsPath, true);
            wrt.WriteLine("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
            wrt.WriteLine("<xsl:stylesheet version=\"1.0\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">");
            wrt.WriteLine("<xsl:output method=\"xml\" />");
            wrt.WriteLine("<xsl:template match=\" / \">");
            wrt.WriteLine("<xsl:element name=\"Configuration\">");
            wrt.WriteLine("<xsl:element name=\"UserMappings\">");
            wrt.WriteLine("<xsl:element name=\"mappingName\">" + this.m_Name + "</xsl:element>");
            wrt.WriteLine("<xsl:element name=\"MappingType\">" + this.m_MappingType + "</xsl:element>");
            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedImportType.ToString() + "</xsl:element>");
                //axis 11 pos
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedPOSImportType.ToString() + "</xsl:element>");

            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedOnlineImportType.ToString() + "</xsl:element>");

            //Axis 10.0 changes

            else
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedXeroImportType.ToString() + "</xsl:element>");




            for (int j = 0; j < this.m_Mappings.Count; j++)
            {
                wrt.WriteLine("<xsl:element name=\"" + this.m_Mappings.GetKey(j) + "\">" + this.m_Mappings.Get(j) + "</xsl:element>");
            }
            wrt.WriteLine("</xsl:element>");

            wrt.WriteLine("<xsl:element name=\"ConstantMappings\">");
            wrt.WriteLine("<xsl:element name=\"mappingName\">" + this.m_Name + "</xsl:element>");
            wrt.WriteLine("<xsl:element name=\"MappingType\">" + this.m_MappingType + "</xsl:element>");
            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedImportType.ToString() + "</xsl:element>");
                //axis 11 pos
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedPOSImportType.ToString() + "</xsl:element>");
                //axis 11.1
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedOnlineImportType.ToString() + "</xsl:element>");


            //Axis 10.0 changes
            else
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedXeroImportType.ToString() + "</xsl:element>");


            for (int j = 0; j < this.m_ConstantMappings.Count; j++)
            {
                wrt.WriteLine("<xsl:element name=\"" + this.m_ConstantMappingsXML.GetKey(j) + "\">" + this.m_ConstantMappingsXML.Get(j) + "</xsl:element>");
            }
            wrt.WriteLine("</xsl:element>");

            //Axis 8.0
            wrt.WriteLine("<xsl:element name=\"FunctionMappings\">");
            wrt.WriteLine("<xsl:element name=\"MappingName\">" + this.m_Name + "</xsl:element>");
            wrt.WriteLine("<xsl:element name=\"MappingType\">" + this.m_MappingType + "</xsl:element>");
            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedImportType.ToString() + "</xsl:element>");
                //axis 11 pos
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedPOSImportType.ToString() + "</xsl:element>");
                //axis 11.1
            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedOnlineImportType.ToString() + "</xsl:element>");

           
             //Axis 10.0 changes
            else
                wrt.WriteLine("<xsl:element name=\"ImportType\">" + CommonUtilities.GetInstance().SelectedXeroImportType.ToString() + "</xsl:element>");

            for (int j = 0; j < this.m_FunctionMappings.Count; j++)
            {
                wrt.WriteLine("<xsl:element name=\"" + this.m_FunctionMappings.GetKey(j) + "\">" + this.m_FunctionMappings.Get(j) + "</xsl:element>");
            }
            wrt.WriteLine("</xsl:element>");
            wrt.WriteLine("</xsl:element>");
            wrt.WriteLine("</xsl:template>");
            wrt.WriteLine("</xsl:stylesheet>");
            wrt.Close();
        }

        private void CreateNew()
        {
            assembly_version = Convert.ToString(Assembly.GetExecutingAssembly().GetName().Version);

            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
            {
                if (m_settingsPath.Contains("Program Files (x86)"))
                {
                    m_settingsPath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                    System.IO.Directory.CreateDirectory(m_settingsPath);
                    m_settingsPath += @"\Settings.xml";
                }
                else
                {
                    m_settingsPath = System.IO.Path.Combine(Constants.xpmsgPath, "Axis");
                    System.IO.Directory.CreateDirectory(m_settingsPath);
                    m_settingsPath += @"\Settings.xml";
                }

            }
            else
            {
                m_settingsPath = System.IO.Path.Combine(Constants.CommonActiveDir, "Axis");
                System.IO.Directory.CreateDirectory(m_settingsPath);
                m_settingsPath += @"\Settings.xml";
            }



            if (m_settingsPath.StartsWith(Path.DirectorySeparatorChar.ToString()))
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {

                    m_settingsPath = "C:\\Documents and Settings\\All Users\\Documents\\Zed" + m_settingsPath;
                }
                else
                {
                    m_settingsPath = "c:\\Users\\Public\\Documents\\Zed" + m_settingsPath;
                }


                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                        }

                    }
                    else
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {                           
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", m_settingsPath);
                        }
                        else
                        {                           
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", m_settingsPath);
                        }
                    }
                }
                catch { }
            }

            string filePath = m_settingsPath.EndsWith(Path.DirectorySeparatorChar.ToString()) ? m_settingsPath : m_settingsPath;

            try
            {
                System.Xml.XmlDocument xdoc = new XmlDocument();
              

                if (!File.Exists(filePath))
                {

                    //create xml
                    xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));

                    XmlElement root = xdoc.CreateElement("Configuration");
                    xdoc.AppendChild(root);


                    System.Xml.XmlElement Template = xdoc.CreateElement("UserMappings");
                    root.AppendChild(Template);

                    //create new Node as name and store value in that node
                    System.Xml.XmlElement Name = xdoc.CreateElement("ConstantMappings");
                    root.AppendChild(Name);

                    //create new node as FileName and store browse filename
                    System.Xml.XmlElement FileName = xdoc.CreateElement("ItemSettings");
                    root.AppendChild(FileName);

                    System.Xml.XmlElement FileName1 = xdoc.CreateElement("FunctionMappings");
                    root.AppendChild(FileName1);
                    //finally save file
                    xdoc.Save(filePath);                    
                }
                else
                {

                    xdoc.Load(m_settingsPath);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;                  

                    if (root.SelectSingleNode("./FunctionMappings") == null)
                    {
                        XmlNode node = root.AppendChild(xdoc.CreateElement("FunctionMappings"));
                        xdoc.Save(m_settingsPath);
                    }                  

                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "UserMappings")
                        {
                            XmlNode mappToAdd = root.ChildNodes.Item(i).AppendChild(xdoc.CreateElement("mapping"));

                            mappToAdd.AppendChild(xdoc.CreateElement("name")).InnerText = this.m_Name;
                            mappToAdd.AppendChild(xdoc.CreateElement("MappingType")).InnerText = this.m_MappingType;
                            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedImportType.ToString();
                                //axis 11 pos
                            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedPOSImportType.ToString();

                            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedOnlineImportType.ToString();

                                //Axis 10.0 changes
                            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedXeroImportType.ToString();

                            for (int j = 0; j < this.m_Mappings.Count; j++)
                            {
                                mappToAdd.AppendChild(xdoc.CreateElement(this.m_Mappings.GetKey(j))).InnerText = this.m_Mappings.Get(j);
                            }

                            //break;
                        }

                        if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                        {
                            XmlNode mappToAdd = root.ChildNodes.Item(i).AppendChild(xdoc.CreateElement("mapping"));

                            mappToAdd.AppendChild(xdoc.CreateElement("name")).InnerText = this.m_Name;
                            mappToAdd.AppendChild(xdoc.CreateElement("MappingType")).InnerText = this.m_MappingType;
                            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedImportType.ToString();
                                //axis 11  pos
                            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedPOSImportType.ToString();
                                //Axis 11.1
                            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedOnlineImportType.ToString();

                                //Axis 10.0 changes
                                else
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
                                        mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedXeroImportType.ToString();

                            for (int j = 0; j < this.m_ConstantMappings.Count; j++)
                            {
                                mappToAdd.AppendChild(xdoc.CreateElement(this.m_ConstantMappings.GetKey(j))).InnerText = this.m_ConstantMappings.Get(j);
                            }

                            //  break;
                        }

                        //Axis 8.0 Function Mappings
                        if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                        {
                            XmlNode mappToAdd = root.ChildNodes.Item(i).AppendChild(xdoc.CreateElement("mapping"));

                            mappToAdd.AppendChild(xdoc.CreateElement("MappingName")).InnerText = this.m_Name;
                            mappToAdd.AppendChild(xdoc.CreateElement("MappingType")).InnerText = this.m_MappingType;
                            if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedImportType.ToString();
                                // axis 11 pos
                            else
                                if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                                    mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedPOSImportType.ToString();

                                    //Axis 11.1
                            else if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                                    mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedOnlineImportType.ToString();

                           
                                //Axis 10.0 changes
                                else
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
                                        mappToAdd.AppendChild(xdoc.CreateElement("ImportType")).InnerText = CommonUtilities.GetInstance().SelectedXeroImportType.ToString();


                            for (int j = 0; j < this.m_FunctionMappings.Count; j++)
                            {
                                mappToAdd.AppendChild(xdoc.CreateElement(this.m_FunctionMappings.GetKey(j))).InnerText = this.m_FunctionMappings.Get(j);
                            }

                            break;
                        }
                    }
                    xdoc.Save(m_settingsPath);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Exc : " + ex.Message);
            }
        }

        public void Remove()
        {
            if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml") || CommonUtilities.GetInstance().XmlCheckExportFlag)
            {
                #region for other than xml

                string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;

                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        for (int j = 0; j < userMappingsXML.Count; j++)
                        {
                            if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                            {
                                root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));
                                break;
                            }

                        }
                        //break;
                    }

                    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        for (int j = 0; j < userMappingsXML.Count; j++)
                        {
                            if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                            {
                                root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));
                                break;
                            }

                        }
                        // break;
                    }
                    //Axis 8.0
                    if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        for (int j = 0; j < userMappingsXML.Count; j++)
                        {
                            if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                            {
                                root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));
                                break;
                            }

                        }
                        break;
                    }
                }
                xdoc.Save(m_settingsPath);
                #endregion
            }
            else //for xml file
            {
                #region for xml file
                m_settingsPath = Application.StartupPath;

                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                        }

                    }
                    else
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                        }
                    }

                }
                catch { }

                if (!Directory.Exists(m_settingsPath))
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER020"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string[] files = Directory.GetFiles(m_settingsPath);
                    string filename = string.Empty;
                    string fileNameInitial = string.Empty;

                    if (files.Length == 0)
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER021"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        foreach (string file in files)
                        {
                            string fileName = file;
                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                {
                                    if (fileName.Contains("Program Files (x86)"))
                                    {
                                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                                    }
                                    else
                                    {
                                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                                    }
                                }
                                else
                                {
                                    if (fileName.Contains("Program Files (x86)"))
                                    {
                                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                    }
                                    else
                                    {
                                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                                    }
                                }


                            }
                            catch { }

                            FileInfo filedata = new FileInfo(fileName);

                            if (filedata.Extension.Equals(".xsl"))
                            {
                                fileNameInitial = filedata.Name.Substring(0, (filedata.Name.Length - 4));
                                if (fileNameInitial.Equals(this.m_Name))
                                    //File.Delete(filedata.Name);
                                    File.Delete(fileName);
                            }
                        }
                    }
                }
                #endregion
            }
        }

        public void RemoveForDeleteDialog()
        {
            if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
            {
                #region for other than xml
                bool flagText = false;

                string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;

                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        for (int j = 0; j < userMappingsXML.Count; j++)
                        {
                            if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                            {
                                root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));
                                flagText = true;
                                break;
                            }

                        }
                        //break;
                    }

                    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        for (int j = 0; j < userMappingsXML.Count; j++)
                        {
                            if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                            {
                                root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));
                                flagText = true;
                                break;
                            }

                        }
                        break;
                    }
                }
                xdoc.Save(m_settingsPath);

                #endregion

                if (flagText == false)//browse file txt and mapping is qbxml
                {
                    #region for xml file deleteing xml mapping
                    m_settingsPath = Application.StartupPath;

                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        {
                            if (m_settingsPath.Contains("Program Files (x86)"))
                            {
                                m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                            }
                            else
                            {
                                m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                            }
                        }
                        else
                        {
                            if (m_settingsPath.Contains("Program Files (x86)"))
                            {
                                m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                            }
                            else
                            {
                                m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                            }
                        }
                    }
                    catch { }

                    if (!Directory.Exists(m_settingsPath))
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER020"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        string[] files = Directory.GetFiles(m_settingsPath);
                        string filename = string.Empty;
                        string fileNameInitial = string.Empty;

                        if (files.Length == 0)
                        {
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER021"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            foreach (string file in files)
                            {
                                string fileName = file;
                                try
                                {
                                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                    {
                                        if (fileName.Contains("Program Files (x86)"))
                                        {
                                            fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                                        }
                                        else
                                        {
                                            fileName = fileName.Replace("Program Files", Constants.xpPath);
                                        }
                                    }
                                    else
                                    {
                                        if (fileName.Contains("Program Files (x86)"))
                                        {
                                            fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                        }
                                        else
                                        {
                                            fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                                        }
                                    }
                                }
                                catch { }

                                FileInfo filedata = new FileInfo(fileName);

                                if (filedata.Extension.Equals(".xsl"))
                                {
                                    fileNameInitial = filedata.Name.Substring(0, (filedata.Name.Length - 4));
                                    if (fileNameInitial.Equals(this.m_Name))
                                        //File.Delete(filedata.Name);
                                        File.Delete(fileName);
                                }
                            }
                        }
                    }
                    #endregion
                }


            }
            else //for xml file
            {
                #region for xml file
                bool flagXml = false;
                m_settingsPath = Application.StartupPath;

                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                        }
                    }
                    else
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                        }
                    }

                }
                catch { }

                if (!Directory.Exists(m_settingsPath))
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER020"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string[] files = Directory.GetFiles(m_settingsPath);
                    string filename = string.Empty;
                    string fileNameInitial = string.Empty;

                    if (files.Length == 0)
                    {
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER021"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        foreach (string file in files)
                        {
                            string fileName = file;
                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                {
                                    if (fileName.Contains("Program Files (x86)"))
                                    {
                                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                                    }
                                    else
                                    {
                                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                                    }
                                }
                                else
                                {
                                    if (fileName.Contains("Program Files (x86)"))
                                    {
                                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                    }
                                    else
                                    {
                                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                                    }
                                }
                            }
                            catch { }

                            FileInfo filedata = new FileInfo(fileName);

                            if (filedata.Extension.Equals(".xsl"))
                            {
                                fileNameInitial = filedata.Name.Substring(0, (filedata.Name.Length - 4));
                                if (fileNameInitial.Equals(this.m_Name))
                                {
                                    //File.Delete(filedata.Name);
                                    File.Delete(fileName);
                                    flagXml = true;
                                }
                            }
                        }
                    }
                }
                #endregion

                if (flagXml == false)//browse file xml and mapping is text
                {
                    #region for other than xml

                    m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                    
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(m_settingsPath);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "UserMappings")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                            for (int j = 0; j < userMappingsXML.Count; j++)
                            {
                                if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                                {
                                    root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));

                                    break;
                                }

                            }
                            //break;
                        }

                        if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                            for (int j = 0; j < userMappingsXML.Count; j++)
                            {
                                if (userMappingsXML.Item(j).FirstChild.InnerText == this.m_Name)
                                {
                                    root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));

                                    break;
                                }

                            }
                            break;
                        }
                    }
                    xdoc.Save(m_settingsPath);

                    #endregion
                }

            }
        }

        public static void RemoveAll()
        {
            m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            
            System.Xml.XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);

            XmlNode root = (XmlNode)xdoc.DocumentElement;
            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "UserMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    for (int j = 0; j < userMappingsXML.Count; j++)
                    {

                        root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));

                    }
                    break;
                }
                if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    for (int j = 0; j < userMappingsXML.Count; j++)
                    {

                        root.ChildNodes.Item(i).RemoveChild(userMappingsXML.Item(j));

                    }
                    break;
                }
            }
            xdoc.Save(m_settingsPath);

        }

        //logic for updating XSL file
        private void UpdateXSL()
        {
            m_settingsPath = Application.StartupPath;
            m_settingsPath += "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xsl";

            //create OS Realated path
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (m_settingsPath.Contains("Program Files (x86)"))
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (m_settingsPath.Contains("Program Files (x86)"))
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }

            }
            catch { }

            XmlDocument xdoc = new XmlDocument();

            xdoc.Load(m_settingsPath);

            XmlNode list = (XmlNode)xdoc.DocumentElement;

            XmlNode root = list.ChildNodes[1].ChildNodes[0];

            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes[i].Attributes.GetNamedItem("name").Value == "UserMappings")
                {

                    XmlNodeList nodelist = root.ChildNodes[i].ChildNodes;

                    foreach (XmlNode node in nodelist)
                    {
                        if (node.Attributes.GetNamedItem("name").Value.Equals("mappingName"))
                        {
                            node.InnerText = this.m_Name;
                        }
                        else if (node.Attributes.GetNamedItem("name").Value.Equals("MappingType"))
                        {
                            node.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft;
                        }
                        else if (node.Attributes.GetNamedItem("name").Value.Equals("ImportType"))
                        {
                            node.InnerText = this.ImType;


                        }
                        else
                        {
                            node.InnerText = this.m_Mappings.Get(node.Attributes.GetNamedItem("name").Value);
                        }

                    }
                }
                if (root.ChildNodes[i].Attributes.GetNamedItem("name").Value == "ConstantMappings")
                {
                    XmlNodeList nodelist = root.ChildNodes[i].ChildNodes;

                    foreach (XmlNode node in nodelist)
                    {
                        if (node.Attributes.GetNamedItem("name").Value.Equals("mappingName"))
                        {
                            node.InnerText = this.m_Name;
                        }
                        else if (node.Attributes.GetNamedItem("name").Value.Equals("MappingType"))
                        {
                            node.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft;
                        }
                        else if (node.Attributes.GetNamedItem("name").Value.Equals("ImportType"))
                        {
                            node.InnerText = this.ImType;

                        }
                        else
                        {
                            node.InnerText = this.m_ConstantMappingsXML.Get(node.Attributes.GetNamedItem("name").Value);
                        }
                    }
                }
                //Axis 8.0
                if (root.ChildNodes[i].Attributes.GetNamedItem("name").Value == "FunctionMappings")
                {
                    XmlNodeList nodelist = root.ChildNodes[i].ChildNodes;

                    foreach (XmlNode node in nodelist)
                    {
                        if (node.Attributes.GetNamedItem("name").Value.Equals("MappingName"))
                        {
                            node.InnerText = this.m_Name;
                        }
                        else if (node.Attributes.GetNamedItem("name").Value.Equals("MappingType"))
                        {
                            node.InnerText = TransactionImporter.Mappings.GetInstance().ConnectedSoft;
                        }
                        else if (node.Attributes.GetNamedItem("name").Value.Equals("ImportType"))
                        {
                            node.InnerText = this.ImType;

                        }
                        else
                        {
                            node.InnerText = this.m_FunctionMappings.Get(node.Attributes.GetNamedItem("name").Value);
                        }

                    }
                }
            }
            xdoc.Save(m_settingsPath);
        }

        private void Update()
        {
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
           
            System.Xml.XmlDocument xdoc = new XmlDocument();
            xdoc.Load(m_settingsPath);

            XmlNode root = (XmlNode)xdoc.DocumentElement;
            XmlAttribute attribute = root.OwnerDocument.CreateAttribute("version");
            attribute.Value = assembly_version;
            root.Attributes.Append(attribute);

            for (int i = 0; i < root.ChildNodes.Count; i++)
            {
                if (root.ChildNodes.Item(i).Name == "UserMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in userMappingsXML)
                    {
                        if (node.FirstChild.InnerText == this.m_Name)
                        {
                            foreach (XmlNode childNodes in node.ChildNodes)
                            {

                                if (childNodes.Name == "ImportType")
                                {
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedImportType.ToString() != childNodes.InnerText)
                                        {                                          
                                            return;
                                        }
                                    }
                                    //axis 11 pos
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedPOSImportType.ToString() != childNodes.InnerText)
                                        {
                                            return;
                                        }
                                    }
                                    //axis 11.1                                    
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedOnlineImportType.ToString() != childNodes.InnerText)
                                        {
                                           return;
                                        }
                                    }
                                }
                                if (childNodes.Name == "name")
                                    childNodes.InnerText = this.m_Name;
                                else
                                {
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                childNodes.InnerText = this.m_Mappings.Get(childNodes.Name);
                                            }
                                            else if (childNodes.Name == "ImportOption")
                                            {
                                                if (CommonUtilities.GetInstance().GrossNet == true)

                                                    childNodes.Attributes["GrossNet"].Value = "1";
                                                else
                                                    childNodes.Attributes["GrossNet"].Value = "0";
                                            }

                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }                   
                }
                if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in userMappingsXML)
                    {
                        if (node.FirstChild.InnerText == this.m_Name)
                        {
                            foreach (XmlNode childNodes in node.ChildNodes)
                            {
                                if (childNodes.Name == "ImportType")
                                {
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedImportType.ToString() != childNodes.InnerText)
                                        {
                                            return;
                                        }
                                    }
                                    //axis 11 pos
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedPOSImportType.ToString() != childNodes.InnerText)
                                        {
                                            return;
                                        }
                                    }

                                    //axis 11.1                                    
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedOnlineImportType.ToString() != childNodes.InnerText)
                                        {
                                            return;
                                        }
                                    }
                                   
                                }
                                if (childNodes.Name == "name")
                                    childNodes.InnerText = this.m_Name;
                                else
                                {
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "ImportOption")
                                        {
                                            childNodes.InnerText = this.m_ConstantMappings.Get(childNodes.Name);
                                        }
                                    }
                                }
                            }
                           
                        }

                    }
                  
                }
                //Axis 8.0

                if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                {
                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                    foreach (XmlNode node in userMappingsXML)
                    {
                        if (node.FirstChild.InnerText == this.m_Name)
                        {
                            foreach (XmlNode childNodes in node.ChildNodes)
                            {

                                if (childNodes.Name == "ImportType")
                                {
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedImportType.ToString() != childNodes.InnerText)
                                        {
                                           
                                            return;
                                        }
                                    }
                                    //axis 11 pos
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBPOSstring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedPOSImportType.ToString() != childNodes.InnerText)
                                        {
                                           
                                            return;
                                        }
                                    }
                                    //axis 11.1                                    
                                    if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBOnlinestring)
                                    {
                                        if (CommonUtilities.GetInstance().SelectedOnlineImportType.ToString() != childNodes.InnerText)
                                        {
                                            
                                            return;
                                        }
                                    }
                                }
                                if (childNodes.Name == "MappingName")
                                    childNodes.InnerText = this.m_Name;
                                else
                                {
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                childNodes.InnerText = this.m_FunctionMappings.Get(childNodes.Name);
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }

                    }
                    break;
                }
            }
            xdoc.Save(m_settingsPath);

        }

        public string GetSelectListWithHeader()
        {

            string selectQuery = string.Empty;
            for (int i = 0; i < this.m_Mappings.Count; i++)
            {
                if (this.m_Mappings.Get(i) != string.Empty)
                {
                    selectQuery += string.Format("[{0}] As [{1}],", this.m_Mappings.Get(i), this.m_Mappings.GetKey(i));
                }
            }
            if (selectQuery.EndsWith(","))
            {
                selectQuery = selectQuery.Substring(0, selectQuery.Length - 1);
            }

            return selectQuery;
        }


        // Bug 628
        public DataTable GetDataTableValue(DataTable dt)
        {
            DataTable newDt = new DataTable();
            newDt = dt.Copy();
            DataColumn newColumn = new DataColumn();
            //  newColumn = dt.Columns[1];
            //dt.Columns.Add(newColumn);
            int tempIndex = -1;
            //634
			for (int j = 0; j < newDt.Columns.Count; j++)
            {
                newDt.Columns[j].ColumnName = Guid.NewGuid().ToString();
            }
            for (int i = 0; i < this.m_Mappings.Count; i++)
            {
                if (this.m_Mappings.Get(i) != string.Empty)
                {
                    tempIndex++;
                    if (dt.Columns.Contains(this.m_Mappings.Get(i)))
                    {
                        int index = dt.Columns[this.m_Mappings.Get(i)].Ordinal ;
                       // newDt.Columns.Add(this.m_Mappings.GetKey(i));
                        int Newindex = dt.Columns[this.m_Mappings.Get(i)].Ordinal;
                        //if (newDt.Columns.Contains(this.m_Mappings.Get(i)))
                        //{
                        //    newDt.Columns[this.m_Mappings.Get(i)].ColumnName = this.m_Mappings.GetKey(i);

                        //}
                        //else
                        //{
                            newDt.Columns.Add(this.m_Mappings.GetKey(i));
                            //newDt.Columns[this.m_Mappings.GetKey(i)].SetOrdinal(tempIndex);
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                int indexOfNew = newDt.Columns[this.m_Mappings.GetKey(i)].Ordinal;
                                newDt.Rows[j][indexOfNew] = dt.Rows[j][index];
                            }
                        //}
                        newDt.Columns[this.m_Mappings.GetKey(i)].SetOrdinal(tempIndex);

                      
                        //newColumn = new DataColumn();
                        //newColumn = dt.Columns[this.m_Mappings.Get(i)];
                        //newColumn.ColumnName = this.m_Mappings.GetKey(i);
                        //newDt.Columns.Add(newColumn.ColumnName, newColumn.DataType);

                    }
                }
            }
            bool isExists = true;
            int index1 = 0;
            for (int k = 0; k < newDt.Columns.Count; k++)
            {
                isExists = false;

                for (int j = 0; j < this.m_Mappings.Count; j++)
                {
                    if (this.m_Mappings.Get(j) != string.Empty)
                    {
                        if (newDt.Columns[k].ColumnName == this.m_Mappings.GetKey(j))
                        {
                            isExists = true;
                            break;
                        }
                        index1 = k;
                    }
                }
                if (isExists == false)
                {
                    newDt.Columns.RemoveAt(index1);
                    k = -1;
                }
            }



            return newDt;
        }


        //Bug 525 
        public string GetSelectListWithHeader1()
        {

            string selectQuery = string.Empty;
            for (int i = 0; i < this.m_Mappings.Count; i++)
            {
                if (this.m_Mappings.Get(i) != string.Empty)
                {
                    selectQuery += string.Format("{0}`{1},", this.m_Mappings.Get(i), this.m_Mappings.GetKey(i));
                }
            }
            if (selectQuery.EndsWith(","))
            {
                selectQuery = selectQuery.Substring(0, selectQuery.Length - 1);
            }

            return selectQuery;
        }

        //Get Column and Data list in Select query when mapping is without header.
        public string GetSelectList(DataTable dt)
        {
            string selectQuery = string.Empty;

            List<string> columnName = new List<string>();
            for (int i = 0; i < this.m_Mappings.Count; i++)
            {
                if (this.m_Mappings.Get(i) != string.Empty)
                {
                    columnName.Add(this.m_Mappings.Get(i).ToString() + "-" + this.m_Mappings.GetKey(i));
                }
            }
            int number = 1;
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                string colName = dt.Rows[0][i].ToString();
                foreach (string cName in columnName)
                {
                    string[] name = new string[2];
                    name = cName.Split('-');
                    if (name[0].Equals(colName))
                    {

                        colName = colName.Replace(colName, "Column" + (number));

                        selectQuery += string.Format("[{0}] As [{1}],", colName, name[1]);

                        break;
                    }
                }
                number++;

            }

            int lengthQuery = selectQuery.Length;
            if (lengthQuery != 0)
            {
                selectQuery = selectQuery.Remove(lengthQuery - 1);
            }
            return selectQuery;
        }

        #region Axis 11.0 -125 Import options for mapping

        public NameValueCollection getImportOption()
        {
            string transactionAction = "";
            NameValueCollection attrval = new NameValueCollection();
            try
            {
                #region 1. Get Setings.xml path
                string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;

                #endregion

                //2. Get the root element and user mappings tag
                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);
                XmlNode root = (XmlNode)xdoc.DocumentElement;
                XmlNodeList userMappingsXML = null;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        break;
                    }
                }

                //3. Find the mapping node
                string selectedMap = CommonUtilities.GetInstance().SelectedMapping.Name;

                XmlNode editNode = null;
                foreach (XmlNode xn in userMappingsXML)
                {
                    if (xn.FirstChild.InnerText == selectedMap)
                    {
                        editNode = xn;
                        break;
                    }
                }
                //4. Find the import option node
                if (editNode != null)
                {
                    XmlNode opt_node = null;

                    foreach (XmlNode xn in editNode.ChildNodes)
                    {
                        if (xn.Name == "ImportOption")
                        {
                            opt_node = xn;
                            break;
                        }
                    }

                    #region 5. Retrieve the stored attributes or create ImportOption if not present
                    if (opt_node != null)
                    {
                        foreach (XmlAttribute attr in opt_node.Attributes)
                        {
                            attrval.Add(attr.Name, attr.Value);
                        }
                    }
                    else
                    {
                        XmlNode xNode = xdoc.CreateNode(XmlNodeType.Element, "ImportOption", "");
                        XmlAttribute xAttr = xdoc.CreateAttribute("AutoNumbering");
                        xAttr.Value = "0";
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("AutoNumbering", "0");
                        xAttr = xdoc.CreateAttribute("AddUpdate");
                        xAttr.Value = "0";
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("AddUpdate", "0");

                        //755
                        if (TransactionImporter.Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring)
                        {
                            if (CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemdiscount")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemfixedasset")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("iteminventory")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemgroup")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("iteminventoryassembly")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemnoninventory")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemotherCharge")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itempayment")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemreceipt")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemsalestax")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemsalestaxgroup")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemservice")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("itemsubtotal")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("customer")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("vendor")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("employee")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("customfieldlist")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("class")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("othername")
                                || CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("inventorysite"))
                            {
                                transactionAction = "O";
                            }
                            else if (CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("vehiclemileage") ||
                                CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("listmerge") ||
                                CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("clearedstatus") ||
                                CommonUtilities.GetInstance().SelectedImportType.ToString().ToLower().Equals("billingrate"))
                            {
                                transactionAction = "D";
                            }
                            else
                            {
                                transactionAction = "D";
                            }
                        }
                        else
                        {
                            transactionAction = "D";
                        }

                        xAttr = xdoc.CreateAttribute("TransactionAction");
                        xAttr.Value = transactionAction;
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("TransactionAction", transactionAction);
                        xAttr = xdoc.CreateAttribute("ImportAtRow");
                        xAttr.Value = "1";
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("ImportAtRow", "1");
                        xAttr = xdoc.CreateAttribute("GrossNet");
                        if (CommonUtilities.GetInstance().GrossNet == true)
                            xAttr.Value = "1";
                        else
                            xAttr.Value = "0";
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("GrossNet", "0");
                        xAttr = xdoc.CreateAttribute("SkipList");
                        xAttr.Value = "0";
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("SkipList", "0");
                        xAttr = xdoc.CreateAttribute("VendorLookup");
                        xAttr.Value = "0";
                        xNode.Attributes.Append(xAttr);
                        attrval.Add("VendorLookup", "0");
                        editNode.InsertAfter(xNode, editNode.LastChild);
                        xdoc.Save(m_settingsPath);

                    }
                    #endregion
                }
            }
            catch
            {
            }
            return attrval;
        }

        public void setImportOption(NameValueCollection attribute)
        {
            try
            {
                #region 1. Get Setings.xml path
                string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
               
                #endregion

                //2. Get the root element and user mappings tag
                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);
                XmlNode root = (XmlNode)xdoc.DocumentElement;
                XmlNodeList userMappingsXML = null;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        break;
                    }
                }

                //3. Find the mapping node
                string selectedMap = CommonUtilities.GetInstance().SelectedMapping.Name;

                XmlNode editNode = null;
                foreach (XmlNode xn in userMappingsXML)
                {
                    if (xn.FirstChild.InnerText == selectedMap)
                    {
                        editNode = xn;
                        break;
                    }
                }
                //4. Find the import option node
                if (editNode != null)
                {
                    XmlNode opt_node = null;

                    foreach (XmlNode xn in editNode.ChildNodes)
                    {
                        if (xn.Name == "ImportOption")
                        {
                            opt_node = xn;
                            break;
                        }
                    }
                    if (opt_node != null)
                    {
                        for (int i = 0; i < attribute.Count; i++)
                        {
                            string key = attribute.GetKey(i);
                            string value = attribute.Get(i);
                            opt_node.Attributes[key].Value = value;
                        }
                        xdoc.Save(m_settingsPath);
                    }
                }
            }
            catch { }
        }
        #endregion
    }


    public class UserMappingsCollection : Collection<UserMappings>
    {
        private static string m_settingsPath;

        public bool IsExists(string mappingName)
        {
            if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
            {
                string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                
                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                return true;
                            }
                        }
                        break;
                    }
                }
            }
            else if (CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
            {

                m_settingsPath = Application.StartupPath;
                try
                {
                    if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", Constants.xpPath);
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", Constants.xpPath);
                        }
                    }
                    else
                    {
                        if (m_settingsPath.Contains("Program Files (x86)"))
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                        }
                        else
                        {
                            m_settingsPath = m_settingsPath.Replace("Program Files", "Users\\Public\\Documents");
                        }
                    }
                }
                catch { }

                if (!Directory.Exists(m_settingsPath))
                {
                    return false;
                }
                else
                {
                    string[] files = Directory.GetFiles(m_settingsPath);
                    string filename = string.Empty;

                    if (files.Length == 0)
                    {
                        return false;
                    }
                    else
                    {
                        foreach (string file in files)
                        {
                            FileInfo filedata = new FileInfo(file);
                            if (filedata.Extension.Equals(".xsl") && filedata.Name.Substring(0, (filedata.Name.Length - 4)).Equals(mappingName))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Create xml file anf return contents of xml file
        /// </summary>
        /// <param name="mappingName"></param>
        /// <returns></returns>

        public string GetExportMappingFile(string mappingName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();
            string m_settingsPath = string.Empty;
            try
            {
                #region Create XmlDocument
                requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));

               
                //System.Xml.XmlElement Configuration = requestXmlDoc.CreateElement("Configuration");
                //    Configuration.InnerText = Application.ProductVersion;
                System.Xml.XmlElement ExportMapping = requestXmlDoc.CreateElement("ExportMapping");
                requestXmlDoc.AppendChild(ExportMapping);

                System.Xml.XmlElement xNode = requestXmlDoc.CreateElement("Configuration");
                XmlAttribute xAttr = requestXmlDoc.CreateAttribute("Version");
                xAttr.Value = Application.ProductVersion;
                xNode.Attributes.Append(xAttr);
                ExportMapping.AppendChild(xNode);

                #endregion

                #region For Loading Setting.xml and getting xml User Mappings and Costant Mappings for respective selected Mapping Name

                m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                
                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;

                string userMappingXmlData = string.Empty;

                string constantMappingXmlData = string.Empty;

                //Axis 8.0
                string functionMappingXmlData = string.Empty;

                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    #region Get data for UserMapping
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                userMappingXmlData = node.InnerXml;
                            }
                        }

                    }
                    #endregion


                    #region Get data for Constant Mapping
                    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                constantMappingXmlData = node.InnerXml;
                            }
                        }

                    }
                    #endregion

                    //Axis 8.0
                    #region Get Data for Function Mapping

                    if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                functionMappingXmlData = node.InnerXml;
                            }
                        }
                    }

                    #endregion
                }

                #endregion

                #region for assigning usermapping/Constant mapping data to xml

                System.Xml.XmlElement UserMapping = requestXmlDoc.CreateElement("UserMappings");
                ExportMapping.AppendChild(UserMapping);

                System.Xml.XmlElement mapping = requestXmlDoc.CreateElement("mapping");
                UserMapping.AppendChild(mapping);


                mapping.InnerXml = userMappingXmlData;

                System.Xml.XmlElement ConstantMapping = requestXmlDoc.CreateElement("ConstantMappings");
                ExportMapping.AppendChild(ConstantMapping);

                mapping = requestXmlDoc.CreateElement("mapping");
                ConstantMapping.AppendChild(mapping);

                mapping.InnerXml = constantMappingXmlData;

                //Axis 8.0
                System.Xml.XmlElement FunctionMapping = requestXmlDoc.CreateElement("FunctionMappings");
                ExportMapping.AppendChild(FunctionMapping);

                mapping = requestXmlDoc.CreateElement("mapping");
                FunctionMapping.AppendChild(mapping);

                mapping.InnerXml = functionMappingXmlData;

                #endregion

                return requestXmlDoc.InnerXml;
            }
            catch (Exception ex)
            {

                return requestXmlDoc.InnerXml;
            }
        }

        public UserMappings GetUserMapping(string mappingName)
        {
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            UserMappings usermapping = null;
            if ((!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml") && !CommonUtilities.GetInstance().BrowseFileExtension.Equals("iif")) || CommonUtilities.GetInstance().XmlCheckExportFlag)
            {
                #region for other than xml file
                
                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                usermapping = new UserMappings();
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                //break;
                            }

                        }
                    }

                    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {

                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.ConstantMappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }

                            }

                        }

                    }

                    if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                //usermapping = new UserMappings();
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "MappingName")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.FunctionMappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                break;
                            }

                        }
                        break;
                    }
                }
                #endregion

            }
            else if (CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
            {
                #region for xml file
                #region xsl transformation
                try
                {
                    string outputFile = string.Empty;
                    string xslPath = string.Empty;

                    m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                    
                    string xmlPath = m_settingsPath;

                    xslPath = Application.StartupPath + "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xsl";
                    //create OS Related path for xsl file
                    //CommonUtilities.GetInstance().osRelatedPath(xslPath);
                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        {
                            if (xslPath.Contains("Program Files (x86)"))
                            {
                                xslPath = xslPath.Replace("Program Files (x86)", Constants.xpPath);
                            }
                            else
                            {
                                xslPath = xslPath.Replace("Program Files", Constants.xpPath);
                            }
                        }
                        else
                        {
                            if (xslPath.Contains("Program Files (x86)"))
                            {
                                xslPath = xslPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                            }
                            else
                            {
                                xslPath = xslPath.Replace("Program Files", "Users\\Public\\Documents");
                            }
                        }

                    }
                    catch { }

                    outputFile = Application.StartupPath + "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xml";
                    //create OS Related path for transform output file
                    //CommonUtilities.GetInstance().osRelatedPath(outputFile);
                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        {
                            if (outputFile.Contains("Program Files (x86)"))
                            {
                                outputFile = outputFile.Replace("Program Files (x86)", Constants.xpPath);
                            }
                            else
                            {
                                outputFile = outputFile.Replace("Program Files", Constants.xpPath);
                            }
                        }
                        else
                        {
                            if (outputFile.Contains("Program Files (x86)"))
                            {
                                outputFile = outputFile.Replace("Program Files (x86)", "Users\\Public\\Documents");
                            }
                            else
                            {
                                outputFile = outputFile.Replace("Program Files", "Users\\Public\\Documents");
                            }
                        }

                    }
                    catch { }

                    XslTransform myXslTransform = new XslTransform();

                    myXslTransform.Load(xslPath);

                    myXslTransform.Transform(xmlPath, outputFile);

                #endregion


                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(outputFile);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "UserMappings")
                        {
                            #region for UserMappings
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                            usermapping = new UserMappings();

                            foreach (XmlNode childNodes in userMappingsXML)
                            {
                                if (childNodes.Name == "mappingName")
                                    usermapping.Name = childNodes.InnerText;
                                else
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                            }
                                        }
                                    }
                                    else
                                        usermapping.ImType = childNodes.InnerText;
                            }
                            //break;

                            #endregion
                        }

                        if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                        {
                            #region For Constant Mappings
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                            foreach (XmlNode childNodes in userMappingsXML)
                            {
                                if (childNodes.Name == "mappingName")
                                    usermapping.Name = childNodes.InnerText;
                                else
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                usermapping.ConstantMappingsXML.Set(childNodes.Name, childNodes.InnerText);
                                            }
                                        }
                                    }
                                    else
                                        usermapping.ImType = childNodes.InnerText;
                            }
                            //break statement commented due to function mappings
                            // break;

                            #endregion
                        }
                        //Axis 8.0
                        if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                        {
                            #region for FunctionMappings
                            XmlNodeList functionMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                            // usermapping = new UserMappings();

                            foreach (XmlNode childNodes in functionMappingsXML)
                            {
                                if (childNodes.Name == "MappingName")
                                    usermapping.Name = childNodes.InnerText;
                                else
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                usermapping.FunctionMappings.Set(childNodes.Name, childNodes.InnerText);
                                            }
                                        }
                                    }
                                    else
                                        usermapping.ImType = childNodes.InnerText;
                            }
                            break;

                            #endregion
                        }
                    }

                    File.Delete(outputFile);
                }
                catch
                {
                    return null;
                }
                #endregion
            }

            else if (CommonUtilities.GetInstance().BrowseFileExtension.Equals("iif"))
            {
                #region for iif file
                m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                
                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                usermapping = new UserMappings();
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                //break;
                            }

                        }
                    }

                    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {

                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.ConstantMappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                //  break;
                            }

                        }
                        // break;
                    }

                    //Axis 8.0
                    if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "MappingName")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.FunctionMappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                //break;
                            }

                        }
                    }
                }
                #endregion
            }
            return usermapping;
        }

        //new changes in version 6.0 for deleteing mapping on delete error dialog box
        public UserMappings GetUserMappingForDelete(string mappingName)
        {
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            UserMappings usermapping = null;
            if (!CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
            {
                #region for other than xml file

                System.Xml.XmlDocument xdoc = new XmlDocument();
                xdoc.Load(m_settingsPath);

                XmlNode root = (XmlNode)xdoc.DocumentElement;
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    if (root.ChildNodes.Item(i).Name == "UserMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {
                                usermapping = new UserMappings();
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                 if (childNodes.Name != "ImportOption")
                                                {
                                                   usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                 }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }                               
                            }
                        }
                    }

                    if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                    {
                        XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                        foreach (XmlNode node in userMappingsXML)
                        {
                            if (node.FirstChild.InnerText == mappingName)
                            {

                                foreach (XmlNode childNodes in node.ChildNodes)
                                {
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.ConstantMappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                break;
                            }

                        }
                        break;
                    }
                }
                #endregion

                if (usermapping == null)//for deleting xml mapping
                {
                    #region for xml file
                    #region xsl transformation
                    try
                    {
                        string outputFile = string.Empty;
                        string xslPath = string.Empty;

                        m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                        
                        string xmlPath = m_settingsPath;

                        xslPath = Application.StartupPath + "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xsl";
                        //create OS Related path for xsl file
                      
                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                            {
                                if (xslPath.Contains("Program Files (x86)"))
                                {
                                    xslPath = xslPath.Replace("Program Files (x86)", Constants.xpPath);
                                }
                                else
                                {
                                    xslPath = xslPath.Replace("Program Files", Constants.xpPath);
                                }
                            }
                            else
                            {
                                if (xslPath.Contains("Program Files (x86)"))
                                {
                                    xslPath = xslPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                }
                                else
                                {
                                    xslPath = xslPath.Replace("Program Files", "Users\\Public\\Documents");
                                }
                            }

                        }
                        catch { }

                        outputFile = Application.StartupPath + "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xml";
                        //create OS Related path for transform output file
                       
                        try
                        {
                            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                            {
                                if (outputFile.Contains("Program Files (x86)"))
                                {
                                    outputFile = outputFile.Replace("Program Files (x86)", Constants.xpPath);
                                }
                                else
                                {
                                    outputFile = outputFile.Replace("Program Files", Constants.xpPath);
                                }
                            }
                            else
                            {
                                if (outputFile.Contains("Program Files (x86)"))
                                {
                                    outputFile = outputFile.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                }
                                else
                                {
                                    outputFile = outputFile.Replace("Program Files", "Users\\Public\\Documents");
                                }
                            }

                        }
                        catch { }

                        XslTransform myXslTransform = new XslTransform();

                        myXslTransform.Load(xslPath);

                        myXslTransform.Transform(xmlPath, outputFile);

                    #endregion


                        System.Xml.XmlDocument xdoc1 = new XmlDocument();
                        xdoc1.Load(outputFile);

                        XmlNode root1 = (XmlNode)xdoc1.DocumentElement;
                        for (int i = 0; i < root1.ChildNodes.Count; i++)
                        {
                            if (root1.ChildNodes.Item(i).Name == "UserMappings")
                            {
                                #region for UserMappings
                                XmlNodeList userMappingsXML = root1.ChildNodes.Item(i).ChildNodes;

                                usermapping = new UserMappings();

                                foreach (XmlNode childNodes in userMappingsXML)
                                {
                                    if (childNodes.Name == "mappingName")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }                              

                                #endregion
                            }

                            if (root1.ChildNodes.Item(i).Name == "ConstantMappings")
                            {
                                #region For Constant Mappings
                                XmlNodeList userMappingsXML = root1.ChildNodes.Item(i).ChildNodes;

                                foreach (XmlNode childNodes in userMappingsXML)
                                {
                                    if (childNodes.Name == "mappingName")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.ConstantMappingsXML.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;
                                }
                                break;

                                #endregion
                            }
                        }

                        File.Delete(outputFile);
                    }
                    catch
                    {
                        return null;
                    }
                    #endregion
                }
            }
            else if (CommonUtilities.GetInstance().BrowseFileExtension.Equals("xml"))
            {
                #region for xml file
                #region xsl transformation
                try
                {
                    string outputFile = string.Empty;
                    string xslPath = string.Empty;

                    m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                    
                    string xmlPath = m_settingsPath;

                    xslPath = Application.StartupPath + "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xsl";
                    //create OS Related path for xsl file
                  
                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        {
                            if (xslPath.Contains("Program Files (x86)"))
                            {
                                xslPath = xslPath.Replace("Program Files (x86)", Constants.xpPath);
                            }
                            else
                            {
                                xslPath = xslPath.Replace("Program Files", Constants.xpPath);
                            }
                        }
                        else
                        {
                            if (xslPath.Contains("Program Files (x86)"))
                            {
                                xslPath = xslPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                            }
                            else
                            {
                                xslPath = xslPath.Replace("Program Files", "Users\\Public\\Documents");
                            }
                        }
                    }
                    catch { }

                    outputFile = Application.StartupPath + "\\" + CommonUtilities.GetInstance().SelectedMappingName + ".xml";
                    //create OS Related path for transform output file
                 
                    try
                    {
                        if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                        {
                            if (outputFile.Contains("Program Files (x86)"))
                            {
                                outputFile = outputFile.Replace("Program Files (x86)", Constants.xpPath);
                            }
                            else
                            {
                                outputFile = outputFile.Replace("Program Files", Constants.xpPath);
                            }
                        }
                        else
                        {
                            if (outputFile.Contains("Program Files (x86)"))
                            {
                                outputFile = outputFile.Replace("Program Files (x86)", "Users\\Public\\Documents");
                            }
                            else
                            {
                                outputFile = outputFile.Replace("Program Files", "Users\\Public\\Documents");
                            }
                        }

                    }
                    catch { }

                    XslTransform myXslTransform = new XslTransform();

                    myXslTransform.Load(xslPath);

                    myXslTransform.Transform(xmlPath, outputFile);

                #endregion


                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(outputFile);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "UserMappings")
                        {
                            #region for UserMappings
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                            usermapping = new UserMappings();

                            foreach (XmlNode childNodes in userMappingsXML)
                            {
                                if (childNodes.Name == "mappingName")
                                    usermapping.Name = childNodes.InnerText;
                                else
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                            }
                                        }
                                    }
                                    else
                                        usermapping.ImType = childNodes.InnerText;
                            }
                          

                            #endregion
                        }

                        if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                        {
                            #region For Constant Mappings
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                            foreach (XmlNode childNodes in userMappingsXML)
                            {
                                if (childNodes.Name == "mappingName")
                                    usermapping.Name = childNodes.InnerText;
                                else
                                    if (childNodes.Name != "ImportType")
                                    {
                                        if (childNodes.Name != "MappingType")
                                        {
                                            if (childNodes.Name != "ImportOption")
                                            {
                                                usermapping.ConstantMappingsXML.Set(childNodes.Name, childNodes.InnerText);
                                            }
                                        }
                                    }
                                    else
                                        usermapping.ImType = childNodes.InnerText;
                            }
                            break;

                            #endregion
                        }
                    }

                    File.Delete(outputFile);
                }
                catch
                {
                  
                }
                #endregion

                if (usermapping == null)//for deleteing text file mapping
                {
                    #region for other than xml file

                    m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                    
                    System.Xml.XmlDocument xdoc = new XmlDocument();
                    xdoc.Load(m_settingsPath);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {
                        if (root.ChildNodes.Item(i).Name == "UserMappings")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                            foreach (XmlNode node in userMappingsXML)
                            {
                                if (node.FirstChild.InnerText == mappingName)
                                {
                                    usermapping = new UserMappings();
                                    foreach (XmlNode childNodes in node.ChildNodes)
                                    {
                                        if (childNodes.Name == "name")
                                            usermapping.Name = childNodes.InnerText;
                                        else
                                            if (childNodes.Name != "ImportType")
                                            {
                                                if (childNodes.Name != "MappingType")
                                                {
                                                    if (childNodes.Name != "ImportOption")
                                                    {
                                                        usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                    }
                                                }
                                            }
                                            else
                                                usermapping.ImType = childNodes.InnerText;
                                    }
                                   
                                }

                            }
                        }

                        if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;
                            foreach (XmlNode node in userMappingsXML)
                            {
                                if (node.FirstChild.InnerText == mappingName)
                                {

                                    foreach (XmlNode childNodes in node.ChildNodes)
                                    {
                                        if (childNodes.Name == "name")
                                            usermapping.Name = childNodes.InnerText;
                                        else
                                            if (childNodes.Name != "ImportType")
                                            {
                                                if (childNodes.Name != "MappingType")
                                                {
                                                    if (childNodes.Name != "ImportOption")
                                                    {
                                                        usermapping.ConstantMappings.Set(childNodes.Name, childNodes.InnerText);
                                                    }
                                                }
                                            }
                                            else
                                                usermapping.ImType = childNodes.InnerText;
                                    }
                                    break;
                                }

                            }
                            break;
                        }
                    }
                    #endregion
                }
            }
            return usermapping;

        }

        public UserMappingsCollection LoadXSL()
        {
            UserMappingsCollection coll = new UserMappingsCollection();
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            #region

            if (!Directory.Exists(m_settingsPath))
            {             
            }
            else
            {
                string[] files = Directory.GetFiles(m_settingsPath);
                string filename = string.Empty;
                UserMappings usermapping = null;

                if (files.Length == 0)
                {
                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis ER021"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {


                    coll.Clear();
                    foreach (string file in files)
                    {
                        FileInfo filedata = new FileInfo(file);

                        if (filedata.Extension.Equals(".xsl"))
                        {
                            string outputFile = string.Empty;
                            string xslPath = string.Empty;
                            string fileNameInitial = string.Empty;

                            fileNameInitial = filedata.Name.Substring(0, (filedata.Name.Length - 4));

                            m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
                            
                            string xmlPath = m_settingsPath;

                            xslPath = Application.StartupPath + "\\" + fileNameInitial + ".xsl";
                            //create OS Realated path for xsl file
                          
                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                {
                                    if (xslPath.Contains("Program Files (x86)"))
                                    {
                                        xslPath = xslPath.Replace("Program Files (x86)", Constants.xpPath);
                                    }
                                    else
                                    {
                                        xslPath = xslPath.Replace("Program Files", Constants.xpPath);
                                    }
                                }
                                else
                                {
                                    if (xslPath.Contains("Program Files (x86)"))
                                    {
                                        xslPath = xslPath.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                    }
                                    else
                                    {
                                        xslPath = xslPath.Replace("Program Files", "Users\\Public\\Documents");
                                    }
                                }
                            }
                            catch { }

                            outputFile = Application.StartupPath + "\\" + fileNameInitial + ".xml";
                            //create OS Related path for transform output file                          

                            try
                            {
                                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                                {
                                    if (outputFile.Contains("Program Files (x86)"))
                                    {
                                        outputFile = outputFile.Replace("Program Files (x86)", Constants.xpPath);
                                    }
                                    else
                                    {
                                        outputFile = outputFile.Replace("Program Files", Constants.xpPath);
                                    }
                                }
                                else
                                {
                                    if (outputFile.Contains("Program Files (x86)"))
                                    {
                                        outputFile = outputFile.Replace("Program Files (x86)", "Users\\Public\\Documents");
                                    }
                                    else
                                    {
                                        outputFile = outputFile.Replace("Program Files", "Users\\Public\\Documents");
                                    }
                                }
                            }
                            catch { }

                            XslTransform myXslTransform = new XslTransform();

                            myXslTransform.Load(xslPath);

                            myXslTransform.Transform(xmlPath, outputFile);




                            System.Xml.XmlDocument xdoc = new XmlDocument();
                            xdoc.Load(outputFile);

                            XmlNode root = (XmlNode)xdoc.DocumentElement;
                            for (int i = 0; i < root.ChildNodes.Count; i++)
                            {
                                if (root.ChildNodes.Item(i).Name == "UserMappings")
                                {
                                    #region for UserMappings
                                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                                    usermapping = new UserMappings();

                                    foreach (XmlNode childNodes in userMappingsXML)
                                    {
                                        if (childNodes.Name == "mappingName")
                                            usermapping.Name = childNodes.InnerText;
                                        else
                                            if (childNodes.Name != "ImportType")
                                            {
                                                if (childNodes.Name != "MappingType")
                                                {
                                                    if (childNodes.Name != "ImportOption")
                                                    {
                                                        usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                    }
                                                }
                                            }
                                            else
                                                usermapping.ImType = childNodes.InnerText;
                                    }
                                    break;

                                    #endregion
                                }

                                if (root.ChildNodes.Item(i).Name == "ConstantMappings")
                                {
                                    #region for UserMappings
                                    XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                                    usermapping = new UserMappings();

                                    foreach (XmlNode childNodes in userMappingsXML)
                                    {
                                        if (childNodes.Name == "mappingName")
                                            usermapping.Name = childNodes.InnerText;
                                        else
                                            if (childNodes.Name != "ImportType")
                                            {
                                                if (childNodes.Name != "MappingType")
                                                {
                                                    if (childNodes.Name != "ImportOption")
                                                    {
                                                        usermapping.ConstantMappingsXML.Set(childNodes.Name, childNodes.InnerText);
                                                    }
                                                }
                                            }
                                            else
                                                usermapping.ImType = childNodes.InnerText;
                                    }
                                    break;

                                    #endregion
                                }

                                //Axis 8.0
                                if (root.ChildNodes.Item(i).Name == "FunctionMappings")
                                {
                                    #region for FunctionMappings
                                    XmlNodeList functionMappingsXML = root.ChildNodes.Item(i).ChildNodes;

                                    usermapping = new UserMappings();

                                    foreach (XmlNode childNodes in functionMappingsXML)
                                    {
                                        if (childNodes.Name == "MappingName")
                                            usermapping.Name = childNodes.InnerText;
                                        else
                                            if (childNodes.Name != "ImportType")
                                            {
                                                if (childNodes.Name != "MappingType")
                                                {
                                                    if (childNodes.Name != "ImportOption")
                                                    {
                                                        usermapping.FunctionMappings.Set(childNodes.Name, childNodes.InnerText);
                                                    }
                                                }
                                            }
                                            else
                                                usermapping.ImType = childNodes.InnerText;
                                    }
                                    break;

                                    #endregion
                                }
                            }


                            File.Delete(outputFile);
                            coll.Add(usermapping);
                        }
                    }

                }
            }
            return coll;
            #endregion
        }

        public UserMappingsCollection Load()
        {
            UserMappingsCollection coll = new UserMappingsCollection();
            string m_settingsPath = CommonUtilities.GetInstance().settingXmlFilePath;
            
            if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
            {
                if (!Directory.Exists(System.IO.Path.Combine(Constants.xpmsgPath, "Axis")))
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(Constants.xpmsgPath, "Axis"));


                }
            }
            else
            {
                if (!Directory.Exists(System.IO.Path.Combine(Constants.CommonActiveDir, "Axis")))
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.Combine(Constants.CommonActiveDir, "Axis"));

                }
            }
            string filePath = m_settingsPath.EndsWith(Path.DirectorySeparatorChar.ToString()) ? m_settingsPath : m_settingsPath;
            System.Xml.XmlDocument xdoc = new XmlDocument();
            try
            {               

                if (!File.Exists(filePath))
                {

                    //create xml
                    xdoc.AppendChild(xdoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));

                    XmlElement root = xdoc.CreateElement("Configuration");
                    xdoc.AppendChild(root);

                    ////create CodingTemplate as Parent node
                   
                    //Axis 10.0
                    System.Xml.XmlElement Template = xdoc.CreateElement("UserMappings");
                    root.AppendChild(Template);

                    //create new Node as name and store value in that node
                    System.Xml.XmlElement Name = xdoc.CreateElement("ConstantMappings");
                    root.AppendChild(Name);

                    //create new node as FileName and store browse filename
                    System.Xml.XmlElement FileName = xdoc.CreateElement("ItemSettings");
                    root.AppendChild(FileName);

                    System.Xml.XmlElement FileName1 = xdoc.CreateElement("FunctionMappings");
                    root.AppendChild(FileName1);

                    //finally save file
                    xdoc.Save(filePath);

                }
                else
                {
                  
                    xdoc.Load(m_settingsPath);

                    XmlNode root = (XmlNode)xdoc.DocumentElement;
                    for (int i = 0; i < root.ChildNodes.Count; i++)
                    {

                        if (root.ChildNodes.Item(i).Name == "UserMappings")
                        {
                            XmlNodeList userMappingsXML = root.ChildNodes.Item(i).ChildNodes;


                            foreach (XmlNode node in userMappingsXML)
                            {
                                UserMappings usermapping = new UserMappings();
                                foreach (XmlNode childNodes in node.ChildNodes)
                                {                                    
                                    if (childNodes.Name == "name")
                                        usermapping.Name = childNodes.InnerText;
                                    else
                                        if (childNodes.Name != "ImportType")
                                        {
                                            if (childNodes.Name != "MappingType")
                                            {
                                                if (childNodes.Name != "ImportOption")
                                                {
                                                    usermapping.Mappings.Set(childNodes.Name, childNodes.InnerText);
                                                }
                                            }
                                        }
                                        else
                                            usermapping.ImType = childNodes.InnerText;

                                }
                                coll.Add(usermapping);
                            }
                            break;
                        }                       
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception :" + e.Message);
            }
            return coll;
        }
    }
}
