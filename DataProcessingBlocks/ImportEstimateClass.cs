using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportEstimateClass
    {
        private int cnt;
        private static ImportEstimateClass m_ImportEstimateClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;
        public int dt_count = 0;

        #region Constructor

        public ImportEstimateClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import Estimate class
        /// </summary>
        /// <returns></returns>
        public static ImportEstimateClass GetInstance()
        {
            if (m_ImportEstimateClass == null)
                m_ImportEstimateClass = new ImportEstimateClass();
            return m_ImportEstimateClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Estimate QuickBooks collection </returns>
        public DataProcessingBlocks.EstimateQBEntryCollection ImportEstimateData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of Estimate Entry collections.
            DataProcessingBlocks.EstimateQBEntryCollection coll = new EstimateQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
          
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Estimate Entry

            #region Checking Validations
            int refcnt = 0;

             foreach (DataRow dr in dt.Rows)
                {
                   dt_count = dt.Rows.Count;               
                    if (bkWorker.CancellationPending != true)
                    {
                        //Bug 1434
                        System.Threading.Thread.Sleep(100);
                        try
                        {
                            bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                        }
                        catch (Exception ex)
                        {
                            
                        }
                        CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                        validateRowCount = validateRowCount + 1;
                        try
                        {
                            int counterrows = 0;
                            bool resultrows = false;
                            for (int l = 0; l < dt.Columns.Count; l++)
                            {
                                resultrows = dr.IsNull(dt.Columns[l]);
                                if (resultrows)
                                {
                                    counterrows = counterrows + 1;
                                    if (counterrows != dt.Columns.Count)
                                    {
                                        resultrows = false;
                                    }
                                }

                            }
                            if (resultrows == true && counterrows == dt.Columns.Count)
                            {
                                continue;
                            }
                        }
                        catch { }
                        DateTime EstimateDt = new DateTime();
                        string datevalue = string.Empty;
                        if (dt.Columns.Contains("EstimateRefNumber"))
                        {
                            #region Adding ref number
                            DataProcessingBlocks.EstimateQBEntry Estimate = new EstimateQBEntry();
                            Estimate = coll.FindEstimateEntry(dr["EstimateRefNumber"].ToString());
                                
                            int datatbl_cnt = 0;
                            if (Estimate == null)
                            {
                            }
                            else
                            {
                                DataRow[] total_cnt_row = dt.Select("EstimateRefNumber = '" + Estimate.RefNumber.ToString() + "'", "");


                                foreach (DataRow value in total_cnt_row)
                                {
                                    datatbl_cnt++;
                                }

                            }

                            int row_cnt = 0;
                            DataRow[] total_cnt = dt.Select("EstimateRefNumber = '" + EstimateQBEntryCollection.number.ToString() + "'", "");
                            foreach (DataRow value in total_cnt)
                            {
                                row_cnt++;
                            }


                             
                            if (Estimate == null)
                            {
                                Estimate = new EstimateQBEntry();
                                if (dt.Columns.Contains("CustomerRefFullName"))
                                {
                                    #region Validations of Customer Full name
                                    if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                    {
                                        string strCust = dr["CustomerRefFullName"].ToString();
                                        if (strCust.Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());                                                  
                                                    if (Estimate.CustomerRef.FullName == null)
                                                    {
                                                        Estimate.CustomerRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 1000));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());                                                   
                                                    if (Estimate.CustomerRef.FullName == null)
                                                    {
                                                        Estimate.CustomerRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());                                               
                                                if (Estimate.CustomerRef.FullName == null)
                                                {
                                                    Estimate.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());                                         
                                            if (Estimate.CustomerRef.FullName == null)
                                            {
                                                Estimate.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                ///bug 442 11.4 
                                if (dt.Columns.Contains("Currency"))
                                {
                                    #region Validations of Currency Full name
                                    if (dr["Currency"].ToString() != string.Empty)
                                    {
                                        string strCust = dr["Currency"].ToString();
                                        if (strCust.Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                    if (Estimate.CurrencyRef.FullName == null)
                                                    {
                                                        Estimate.CurrencyRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                    if (Estimate.CurrencyRef.FullName == null)
                                                    {
                                                        Estimate.CurrencyRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Estimate.CurrencyRef.FullName == null)
                                                {
                                                    Estimate.CurrencyRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (Estimate.CurrencyRef.FullName == null)
                                            {
                                                Estimate.CurrencyRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("ClassRefFullName"))
                                {
                                    #region Validations of Class Full name
                                    if (dr["ClassRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ClassRefFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());                                                   
                                                    if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                                    {
                                                        Estimate.ClassRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 1000));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());                                                   
                                                    if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                                    {
                                                        Estimate.ClassRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                               
                                                if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                                {
                                                    Estimate.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                            {
                                                Estimate.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("TemplateRefFullName"))
                                {
                                    #region Validations of Template Full name
                                    if (dr["TemplateRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["TemplateRefFullName"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Template full name (" + dr["TemplateRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                    if (Estimate.TemplateRef.FullName == null)
                                                        Estimate.TemplateRef.FullName = null;
                                                    else
                                                        Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString().Substring(0, 100));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                    if (Estimate.TemplateRef.FullName == null)
                                                        Estimate.TemplateRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (Estimate.TemplateRef.FullName == null)
                                                    Estimate.TemplateRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (Estimate.TemplateRef.FullName == null)
                                                Estimate.TemplateRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("TxnDate"))
                                {
                                    #region validations of TxnDate
                                    if (dr["TxnDate"].ToString() != string.Empty)
                                    {
                                        datevalue = dr["TxnDate"].ToString();
                                        if (!DateTime.TryParse(datevalue, out EstimateDt))
                                        {
                                            DateTime dttest = new DateTime();
                                            bool IsValid = false;

                                            try
                                            {
                                                dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                IsValid = true;
                                            }
                                            catch
                                            {
                                                IsValid = false;
                                            }
                                            if (IsValid == false)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        Estimate.TxnDate = datevalue;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        Estimate.TxnDate = datevalue;
                                                    }
                                                }
                                                else
                                                {
                                                    Estimate.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                EstimateDt = dttest;
                                                Estimate.TxnDate = dttest.ToString("yyyy-MM-dd");
                                            }

                                        }
                                        else
                                        {
                                            EstimateDt = Convert.ToDateTime(datevalue);
                                            Estimate.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("EstimateRefNumber"))
                                {
                                    #region Validations of Ref Number
                                    if (datevalue != string.Empty)
                                        Estimate.EstimateDate = EstimateDt;

                                    if (dr["EstimateRefNumber"].ToString() != string.Empty)
                                    {
                                        string strRefNum = dr["EstimateRefNumber"].ToString();
                                        if (strRefNum.Length > 21)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ref Number (" + dr["EstimateRefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.RefNumber = dr["EstimateRefNumber"].ToString().Substring(0, 21);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.RefNumber = dr["EstimateRefNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Estimate.RefNumber = dr["EstimateRefNumber"].ToString();
                                            }

                                        }
                                        else
                                            Estimate.RefNumber = dr["EstimateRefNumber"].ToString();
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("IsActive"))
                                {
                                    #region Validations of IsActive
                                    if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["IsActive"].ToString(), out result))
                                        {
                                            Estimate.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsActive"].ToString().ToLower() == "true")
                                            {
                                                Estimate.IsActive = dr["IsActive"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsActive"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    Estimate.IsActive = dr["IsActive"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        Estimate.IsActive = dr["IsActive"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        Estimate.IsActive = dr["IsActive"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    Estimate.IsActive = dr["IsActive"].ToString();
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("PONumber"))
                                {
                                    #region Validations of PONumber
                                    if (dr["PONumber"].ToString() != string.Empty)
                                    {
                                        string strCust = dr["PONumber"].ToString();
                                        if (strCust.Length > 25)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This PONumber (" + dr["PONumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.PONumber = dr["PONumber"].ToString().Substring(0, 25);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.PONumber = dr["PONumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Estimate.PONumber = dr["PONumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.PONumber = dr["PONumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("TermsRefFullName"))
                                {
                                    #region Validations of Terms Full name
                                    if (dr["TermsRefFullName"].ToString() != string.Empty)
                                    {
                                        string strCust = dr["TermsRefFullName"].ToString();
                                        if (strCust.Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Terms fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                    if (Estimate.TermsRef.FullName == null)
                                                    {
                                                        Estimate.TermsRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString().Substring(0, 100));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                    if (Estimate.TermsRef.FullName == null)
                                                    {
                                                        Estimate.TermsRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                if (Estimate.TermsRef.FullName == null)
                                                {
                                                    Estimate.TermsRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());

                                            if (Estimate.TermsRef.FullName == null)
                                            {
                                                Estimate.TermsRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                DateTime NewDueDt = new DateTime();
                                if (dt.Columns.Contains("DueDate"))
                                {
                                    #region validations of DueDate
                                    if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                    {
                                        string duevalue = dr["DueDate"].ToString();
                                        if (!DateTime.TryParse(duevalue, out NewDueDt))
                                        {
                                            DateTime dttest = new DateTime();
                                            bool IsValid = false;

                                            try
                                            {
                                                dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                IsValid = true;
                                            }
                                            catch
                                            {
                                                IsValid = false;
                                            }
                                            if (IsValid == false)
                                            {
                                                //DialogResult dgv = MessageBox.Show("TxnDate is not valid for this mapping.","Warning",MessageBoxButtons.);
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        Estimate.DueDate = duevalue;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        Estimate.DueDate = duevalue;
                                                    }
                                                }
                                                else
                                                    Estimate.DueDate = duevalue;
                                            }
                                            else
                                            {
                                                Estimate.DueDate = dttest.ToString("yyyy-MM-dd");
                                            }


                                        }
                                        else
                                        {
                                            Estimate.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                        }
                                    }
                                    #endregion
                                }
                                
                                if (dt.Columns.Contains("SalesRepRefFullName"))
                                {
                                    #region Validations of SalesRep Full name
                                    if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["SalesRepRefFullName"].ToString().Length > 5)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Salesrep full name (" + dr["SalesRepRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                    //string strClassFullName = string.Empty;
                                                    if (Estimate.SalesRepRef.FullName == null)
                                                    {
                                                        Estimate.SalesRepRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 5));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                    if (Estimate.SalesRepRef.FullName == null)
                                                    {
                                                        Estimate.SalesRepRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.SalesRepRef.FullName == null)
                                                {
                                                    Estimate.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (Estimate.SalesRepRef.FullName == null)
                                            {
                                                Estimate.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("FOB"))
                                {
                                    #region Validations of FOB
                                    if (dr["FOB"].ToString() != string.Empty)
                                    {
                                        string strCust = dr["FOB"].ToString();
                                        if (strCust.Length > 13)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This FOB (" + dr["FOB"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.FOB = dr["FOB"].ToString().Substring(0, 13);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.FOB = dr["FOB"].ToString();
                                                }
                                            }
                                            else
                                                Estimate.FOB = dr["FOB"].ToString();
                                        }
                                        else
                                        {
                                            Estimate.FOB = dr["FOB"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("ItemSalesTaxRefFullName"))
                                {
                                    #region Validations of ItemSalesTax Full name
                                    if (dr["ItemSalesTaxRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemSalesTaxRefFullName"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemSalesTaxRef full name (" + dr["ItemSalesTaxRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                    if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                                    {
                                                        Estimate.ItemSalesTaxRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString().Substring(0, 100));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                    if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                                    {
                                                        Estimate.ItemSalesTaxRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                                {
                                                    Estimate.ItemSalesTaxRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                            {
                                                Estimate.ItemSalesTaxRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("Memo"))
                                {
                                    #region Validations for Memo
                                    if (dr["Memo"].ToString() != string.Empty)
                                    {
                                        if (dr["Memo"].ToString().Length > 4000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                                    Estimate.Memo = strMemo;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strMemo = dr["Memo"].ToString();
                                                    Estimate.Memo = strMemo;
                                                }
                                            }
                                            else
                                            {
                                                Estimate.Memo = dr["Memo"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            Estimate.Memo = strMemo;
                                        }
                                    }

                                    #endregion
                                }


                                if (dt.Columns.Contains("Other"))
                                {
                                    #region Validations for Other
                                    if (dr["Other"].ToString() != string.Empty)
                                    {
                                        if (dr["Other"].ToString().Length > 29)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Other ( " + dr["Other"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strOther = dr["Other"].ToString().Substring(0, 29);
                                                    Estimate.Other = strOther;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strOther = dr["Other"].ToString();
                                                    Estimate.Other = strOther;
                                                }
                                            }
                                            else
                                            {
                                                Estimate.Other = dr["Other"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strOther = dr["Other"].ToString();
                                            Estimate.Other = strOther;
                                        }
                                    }

                                    #endregion
                                }

                                QuickBookEntities.BillAddress BillAddressItem = new BillAddress();
                                if (dt.Columns.Contains("BillAddr1"))
                                {
                                    #region Validations of Bill Addr1
                                    if (dr["BillAddr1"].ToString() != string.Empty)
                                    {
                                        if (dr["BillAddr1"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Add1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Addr1 = dr["BillAddr1"].ToString().Substring(0, 41);

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("BillAddr2"))
                                {
                                    #region Validations of Bill Addr2
                                    if (dr["BillAddr2"].ToString() != string.Empty)
                                    {
                                        if (dr["BillAddr2"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Add2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Addr2 = dr["BillAddr2"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("BillAddr3"))
                                {
                                    #region Validations of Bill Addr3
                                    if (dr["BillAddr3"].ToString() != string.Empty)
                                    {
                                        if (dr["BillAddr3"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Add3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Addr3 = dr["BillAddr3"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                                }
                                            }
                                            else
                                                BillAddressItem.Addr3 = dr["BillAddr3"].ToString();

                                        }
                                        else
                                        {
                                            BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("BillAddr4"))
                                {
                                    #region Validations of Bill Addr4
                                    if (dr["BillAddr4"].ToString() != string.Empty)
                                    {
                                        if (dr["BillAddr4"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Add4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Addr4 = dr["BillAddr4"].ToString().Substring(0, 41);

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("BillAddr5"))
                                {
                                    #region Validations of Bill Addr5
                                    if (dr["BillAddr5"].ToString() != string.Empty)
                                    {
                                        if (dr["BillAddr5"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Add5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Addr5 = dr["BillAddr5"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                                }
                                            }
                                            else
                                                BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("BillCity"))
                                {
                                    #region Validations of Bill City
                                    if (dr["BillCity"].ToString() != string.Empty)
                                    {
                                        if (dr["BillCity"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill City (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.City = dr["BillCity"].ToString().Substring(0, 31);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.City = dr["BillCity"].ToString();
                                                }
                                            }
                                            else
                                                BillAddressItem.City = dr["BillCity"].ToString();
                                        }
                                        else
                                        {
                                            BillAddressItem.City = dr["BillCity"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("BillState"))
                                {
                                    #region Validations of Bill State
                                    if (dr["BillState"].ToString() != string.Empty)
                                    {
                                        if (dr["BillState"].ToString().Length > 21)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill State (" + dr["BillState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.State = dr["BillState"].ToString().Substring(0, 21);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.State = dr["BillState"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                BillAddressItem.State = dr["BillState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.State = dr["BillState"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("BillPostalCode"))
                                {
                                    #region Validations of Bill Postal Code
                                    if (dr["BillPostalCode"].ToString() != string.Empty)
                                    {
                                        if (dr["BillPostalCode"].ToString().Length > 13)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Postal Code (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.PostalCode = dr["BillPostalCode"].ToString().Substring(0, 13);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                                }
                                            }
                                            else
                                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                        }
                                        else
                                        {
                                            BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("BillCountry"))
                                {
                                    #region Validations of Bill Country
                                    if (dr["BillCountry"].ToString() != string.Empty)
                                    {
                                        if (dr["BillCountry"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Country (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Country = dr["BillCountry"].ToString().Substring(0, 31);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Country = dr["BillCountry"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                BillAddressItem.Country = dr["BillCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Country = dr["BillCountry"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("BillNote"))
                                {
                                    #region Validations of Bill Note
                                    if (dr["BillNote"].ToString() != string.Empty)
                                    {
                                        if (dr["BillNote"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Bill Note (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    BillAddressItem.Note = dr["BillNote"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    BillAddressItem.Note = dr["BillNote"].ToString();
                                                }
                                            }
                                            else
                                                BillAddressItem.Note = dr["BillNote"].ToString();
                                        }
                                        else
                                        {
                                            BillAddressItem.Note = dr["BillNote"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (BillAddressItem.Addr1 != null || BillAddressItem.Addr2 != null || BillAddressItem.Addr3 != null || BillAddressItem.Addr4 != null || BillAddressItem.Addr5 != null
                                    || BillAddressItem.City != null || BillAddressItem.Country != null || BillAddressItem.PostalCode != null || BillAddressItem.State != null || BillAddressItem.Note != null)
                                    Estimate.BillAddress.Add(BillAddressItem);

                                if (dt.Columns.Contains("Phone"))
                                {
                                    #region Validations of Phone
                                    if (dr["Phone"].ToString() != string.Empty)
                                    {
                                        if (dr["Phone"].ToString().Length > 21)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.Phone = dr["Phone"].ToString().Substring(0, 21);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.Phone = dr["Phone"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Estimate.Phone = dr["Phone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("Fax"))
                                {
                                    #region Validations of Fax
                                    if (dr["Fax"].ToString() != string.Empty)
                                    {
                                        if (dr["Fax"].ToString().Length > 21)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.Fax = dr["Fax"].ToString().Substring(0, 21);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.Fax = dr["Fax"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Estimate.Fax = dr["Fax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("Email"))
                                {
                                    #region Validations of Email
                                    if (dr["Email"].ToString() != string.Empty)
                                    {
                                        if (dr["Email"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.Email = dr["Email"].ToString().Substring(0, 100);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.Email = dr["Email"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Estimate.Email = dr["Email"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Email = dr["Email"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();
                                if (dt.Columns.Contains("ShipAddr1"))
                                {
                                    #region Validations of Ship Addr1
                                    if (dr["ShipAddr1"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipAddr1"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ShipAddr2"))
                                {
                                    #region Validations of Ship Addr2
                                    if (dr["ShipAddr2"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipAddr2"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ShipAddr3"))
                                {
                                    #region Validations of Ship Addr3
                                    if (dr["ShipAddr3"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipAddr3"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("ShipAddr4"))
                                {
                                    #region Validations of Ship Addr4
                                    if (dr["ShipAddr4"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipAddr4"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ShipAddr5"))
                                {
                                    #region Validations of Ship Addr5
                                    if (dr["ShipAddr5"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipAddr5"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ShipCity"))
                                {
                                    #region Validations of Ship City
                                    if (dr["ShipCity"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipCity"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.City = dr["ShipCity"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("ShipState"))
                                {
                                    #region Validations of Ship State
                                    if (dr["ShipState"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipState"].ToString().Length > 21)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.State = dr["ShipState"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ShipAddressItem.State = dr["ShipState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("ShipPostalCode"))
                                {
                                    #region Validations of Ship Postal Code
                                    if (dr["ShipPostalCode"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipPostalCode"].ToString().Length > 13)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 13);

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ShipCountry"))
                                {
                                    #region Validations of Ship Country
                                    if (dr["ShipCountry"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipCountry"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Country = dr["ShipCountry"].ToString().Substring(0, 31);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("ShipNote"))
                                {
                                    #region Validations of Ship Note
                                    if (dr["ShipNote"].ToString() != string.Empty)
                                    {
                                        if (dr["ShipNote"].ToString().Length > 41)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ShipAddressItem.Note = dr["ShipNote"].ToString().Substring(0, 41);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ShipAddressItem.Note = dr["ShipNote"].ToString();
                                                }
                                            }
                                            else
                                                ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                        else
                                        {
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                    }
                                    #endregion

                                }

                                if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                                  || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                                    Estimate.ShipAddress.Add(ShipAddressItem);

                                if (dt.Columns.Contains("CustomerMsgRefFullName"))
                                {
                                    #region Validations of CustomerMsgRef Full name
                                    if (dr["CustomerMsgRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomerMsgRefFullName"].ToString().Length > 101)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This CustomerMsgRef full name (" + dr["CustomerMsgRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                                    //string strClassFullName = string.Empty;
                                                    if (Estimate.CustomerMsgRef.FullName == null)
                                                    {
                                                        Estimate.CustomerMsgRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString().Substring(0, 101));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                                   
                                                    if (Estimate.CustomerMsgRef.FullName == null)
                                                    {
                                                        Estimate.CustomerMsgRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                               
                                                if (Estimate.CustomerMsgRef.FullName == null)
                                                {
                                                    Estimate.CustomerMsgRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                            if (Estimate.CustomerMsgRef.FullName == null)
                                            {
                                                Estimate.CustomerMsgRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("IsToBeEmailed"))
                                {
                                    #region Validations of IsToBeEmailed
                                    if (dr["IsToBeEmailed"].ToString() != "<None>")
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["IsToBeEmailed"].ToString(), out result))
                                        {
                                            Estimate.IsToBeEmailed = Convert.ToInt32(dr["IsToBeEmailed"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsToBeEmailed"].ToString().ToLower() == "true")
                                            {
                                                Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsToBeEmailed"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsToBeEmailed (" + dr["IsToBeEmailed"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                    }
                                                }
                                                else
                                                    Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }

                                        }
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("IsTaxIncluded"))
                                {
                                    #region Validations of IsTaxIncluded
                                    if (dr["IsTaxIncluded"].ToString() != "<None>")
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                        {
                                            Estimate.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                            {
                                                Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                    }
                                                }
                                                else
                                                    Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }

                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("CustomerSalesTaxCodeFullName"))
                                {
                                    #region Validations of CustomerSalesTaxCode Full name
                                    if (dr["CustomerSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomerSalesTaxCodeFullName"].ToString().Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This CustomerSalesTaxCode full name (" + dr["CustomerSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                                   
                                                    if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                                    {
                                                        Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                                    if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                                    {
                                                        Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                                if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                                {
                                                    Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                           
                                            if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                            {
                                                Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ExchangeRate"))
                                {
                                    #region Validations for ExchangeRate
                                    if (dr["ExchangeRate"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.ExchangeRate = dr["ExchangeRate"].ToString();
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.ExchangeRate = dr["ExchangeRate"].ToString();
                                                }
                                            }
                                            else
                                                Estimate.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        else
                                        {
                                            //Estimate.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                            Estimate.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                            
                                        }
                                    }

                                    #endregion

                                }

                                #region Estimate Line Add
                                

                                #region Checking and setting SalesTaxCode

                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;

                                }
                              
                                string TaxRateValue = string.Empty;
                                string ItemSaleTaxFullName = string.Empty;
                                //if default settings contain checkBoxGrossToNet checked.
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                    {
                                        if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["SalesTaxCodeFullName"].ToString();
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                                #endregion
                                if (dt.Columns.Contains("ItemRefFullName"))
                                {
                                    DataProcessingBlocks.EstimateLineAdd EstLine = new EstimateLineAdd();

                                    #region Validations of item Full name
                                    if (dr["ItemRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemRefFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                    if (EstLine.ItemRef.FullName == null)
                                                        EstLine.ItemRef.FullName = null;
                                                    else
                                                        EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                    if (EstLine.ItemRef.FullName == null)
                                                        EstLine.ItemRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (EstLine.ItemRef.FullName == null)
                                                    EstLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (EstLine.ItemRef.FullName == null)
                                                EstLine.ItemRef.FullName = null;
                                        }
                                    }
                                    #endregion



                                    if (dt.Columns.Contains("Description"))
                                    {
                                        #region Validations for Description
                                        if (dr["Description"].ToString() != string.Empty)
                                        {
                                            if (dr["Description"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["Description"].ToString().Substring(0, 4095);
                                                        EstLine.Desc = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["Description"].ToString();
                                                        EstLine.Desc = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Desc = dr["Description"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["Description"].ToString();
                                                EstLine.Desc = strDesc;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("Other1"))
                                    {
                                        #region Validations for Other1
                                        if (dr["Other1"].ToString() != string.Empty)
                                        {
                                            if (dr["Other1"].ToString().Length > 29)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strOther1 = dr["Other1"].ToString().Substring(0, 29);
                                                        EstLine.Other1 = strOther1;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strOther1 = dr["Other1"].ToString();
                                                        EstLine.Other1 = strOther1;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Other1 = dr["Other1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strOther1 = dr["Other1"].ToString();
                                                EstLine.Other1 = strOther1;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("Other2"))
                                    {
                                        #region Validations for Other2
                                        if (dr["Other2"].ToString() != string.Empty)
                                        {
                                            if (dr["Other2"].ToString().Length > 29)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strOther2 = dr["Other2"].ToString().Substring(0, 29);
                                                        EstLine.Other2 = strOther2;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strOther2 = dr["Other2"].ToString();
                                                        EstLine.Other2 = strOther2;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Other2 = dr["Other2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strOther2 = dr["Other2"].ToString();
                                                EstLine.Other2 = strOther2;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("Quantity"))
                                    {
                                        #region Validations for Quantity
                                        if (dr["Quantity"].ToString() != string.Empty)
                                        {
                                            string strQuantity = dr["Quantity"].ToString();
                                            EstLine.Quantity = strQuantity;
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("EstimateUnitOfMeasure"))
                                    {
                                        #region Validations for EstimateUnitOfMeasure
                                        if (dr["EstimateUnitOfMeasure"].ToString() != string.Empty)
                                        {
                                            if (dr["EstimateUnitOfMeasure"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This EstimateUnitOfMeasure ( " + dr["EstimateUnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString().Substring(0, 31);
                                                        EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                        EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                }
                                                else
                                                {
                                                    string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                    EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                EstLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }

                                        #endregion

                                    }
                                    if (dt.Columns.Contains("Rate"))
                                    {
                                        #region Validations for Rate
                                        if (dr["Rate"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                          
                                            if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                        }
                                                    }
                                                    //Check if EstLine.Rate is null
                                                    if (EstLine.Rate == null)
                                                    {
                                                        EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                }
                                            }
                                        }

                                        #endregion
                                    }

                                    if (dt.Columns.Contains("EstLineClassRefFullName"))
                                    {
                                        #region Validations of Class Full name
                                        if (dr["EstLineClassRefFullName"].ToString() != string.Empty)
                                        {
                                            if (dr["EstLineClassRefFullName"].ToString().Length > 1000)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Estimate Line Class full name (" + dr["EstLineClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                        if (EstLine.ClassRef.FullName == null)
                                                            EstLine.ClassRef.FullName = null;
                                                        else
                                                            EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString().Substring(0, 1000));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                        if (EstLine.ClassRef.FullName == null)
                                                            EstLine.ClassRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                    if (EstLine.ClassRef.FullName == null)
                                                        EstLine.ClassRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                if (EstLine.ClassRef.FullName == null)
                                                    EstLine.ClassRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    if (dt.Columns.Contains("Amount"))
                                    {
                                        #region Validations for Amount
                                        if (dr["Amount"].ToString() != string.Empty)
                                        {
                                            decimal amount;
                                            if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strAmount = dr["Amount"].ToString();
                                                        EstLine.Amount = strAmount;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strAmount = dr["Amount"].ToString();
                                                        EstLine.Amount = strAmount;
                                                    }
                                                }
                                                else
                                                {
                                                    string strAmount = dr["Amount"].ToString();
                                                    EstLine.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                amount = Amount / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Amount = string.Format("{0:000000.00}", amount);
                                                        }
                                                    }
                                                    //Check if EstLine.Amount is null
                                                    if (EstLine.Amount == null)
                                                    {
                                                        EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                                }
                                            }
                                        }

                                        #endregion
                                    }
                                    //validation for InventorySiteRefFullName
                                    if (dt.Columns.Contains("InventorySiteRefFullName"))
                                    {
                                        #region Validations of Inventory Site Ref Full name
                                        if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Inventory Site Ref name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                        if (EstLine.InventorySiteRef.FullName == null)
                                                            EstLine.InventorySiteRef.FullName = null;
                                                        else
                                                            EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                        if (EstLine.InventorySiteRef.FullName == null)
                                                            EstLine.InventorySiteRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                    if (EstLine.InventorySiteRef.FullName == null)
                                                        EstLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (EstLine.InventorySiteRef.FullName == null)
                                                    EstLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        #endregion


                                    }
                                    if (dt.Columns.Contains("InventorySiteLocationRef"))
                                    {
                                        #region Validations of InventorySiteLocationRef

                                        if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                        {
                                            if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                        if (EstLine.InventorySiteLocationRef.FullName == null)
                                                            EstLine.InventorySiteLocationRef.FullName = null;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                        if (EstLine.InventorySiteLocationRef.FullName == null)
                                                            EstLine.InventorySiteLocationRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                    if (EstLine.InventorySiteLocationRef.FullName == null)
                                                        EstLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (EstLine.InventorySiteLocationRef.FullName == null)
                                                    EstLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                    {
                                        #region Validations of sales tax code Full name
                                        if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This sales tax code name (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                        if (EstLine.SalesTaxCodeRef.FullName == null)
                                                            EstLine.SalesTaxCodeRef.FullName = null;
                                                        else
                                                            EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                        if (EstLine.SalesTaxCodeRef.FullName == null)
                                                            EstLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                    if (EstLine.SalesTaxCodeRef.FullName == null)
                                                        EstLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (EstLine.SalesTaxCodeRef.FullName == null)
                                                    EstLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }

                                    if (dt.Columns.Contains("MarkupRate"))
                                    {
                                        #region Validations for MarkupRate
                                        if (dr["MarkupRate"].ToString() != string.Empty)
                                        {
                                            decimal amount;
                                            if (!decimal.TryParse(dr["MarkupRate"].ToString(), out amount))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This MarkupRate ( " + dr["MarkupRate"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strMarkupRate = dr["MarkupRate"].ToString();
                                                        EstLine.MarkupRate = strMarkupRate;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strMarkupRate = dr["MarkupRate"].ToString();
                                                        EstLine.MarkupRate = strMarkupRate;
                                                    }
                                                }
                                                else
                                                {
                                                    string strMarkupRate = dr["MarkupRate"].ToString();
                                                    EstLine.MarkupRate = strMarkupRate;
                                                }
                                            }
                                            else
                                            {

                                                EstLine.MarkupRate = string.Format("{0:000000.00}", dr["MarkupRate"].ToString());

                                            }
                                        }

                                        #endregion
                                    }

                                    if (dt.Columns.Contains("MarkupRatePercent"))
                                    {
                                        #region Validations for MarkupRatePercent
                                        if (dr["MarkupRatePercent"].ToString() != string.Empty)
                                        {
                                            decimal amount;
                                            if (!decimal.TryParse(dr["MarkupRatePercent"].ToString(), out amount))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This MarkupRatePercent ( " + dr["MarkupRatePercent"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                        EstLine.MarkupRatePercent = strMarkupRate;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                        EstLine.MarkupRatePercent = strMarkupRate;
                                                    }
                                                }
                                                else
                                                {
                                                    string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                    EstLine.MarkupRatePercent = strMarkupRate;
                                                }
                                            }
                                            else
                                            {

                                                EstLine.MarkupRatePercent = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["MarkupRatePercent"].ToString())));

                                            }
                                        }

                                        #endregion
                                    }

                                    //New Feature::601

                                    if (dt.Columns.Contains("PriceLevelRefFullName"))
                                    {
                                        #region Validations of PriceLevel FullName
                                        if (dr["PriceLevelRefFullName"].ToString() != string.Empty)
                                        {
                                            if (dr["PriceLevelRefFullName"].ToString().Length > 100)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                        if (EstLine.PriceLevelRef.FullName == null)
                                                            EstLine.PriceLevelRef.FullName = null;
                                                        else
                                                            EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString().Substring(0, 100));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                        if (EstLine.PriceLevelRef.FullName == null)
                                                            EstLine.PriceLevelRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                    if (EstLine.PriceLevelRef.FullName == null)
                                                        EstLine.PriceLevelRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (EstLine.PriceLevelRef.FullName == null)
                                                    EstLine.PriceLevelRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    /// bug no 443
                                    DataProcessingBlocks.DataExt DataExt1 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("CustomFieldName1"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["CustomFieldName1"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldName1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtName1 ( " + dr["CustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldName1"].ToString().Substring(0, 4095);
                                                        DataExt1.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldName1"].ToString();
                                                        DataExt1.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt1.DataExtName = dr["CustomFieldName1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldName1"].ToString();
                                                DataExt1.DataExtName = strDesc;
                                            }

                                            DataExt1.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("CustomFieldValue1"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldValue1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtValue1 ( " + dr["CustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldValue1"].ToString().Substring(0, 4095);
                                                        DataExt1.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldValue1"].ToString();
                                                        DataExt1.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt1.DataExtValue = dr["CustomFieldValue1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldValue1"].ToString();
                                                DataExt1.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt1.DataExtName != null && DataExt1.DataExtValue != null && DataExt1.OwnerID != null)
                                    {
                                        EstLine.DataExt.Add(DataExt1);
                                    }

                                    DataProcessingBlocks.DataExt DataExt2 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("CustomFieldName2"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["CustomFieldName2"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldName2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtName2 ( " + dr["CustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldName2"].ToString().Substring(0, 4095);
                                                        DataExt2.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldName2"].ToString();
                                                        DataExt2.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt2.DataExtName = dr["CustomFieldName2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldName2"].ToString();
                                                DataExt2.DataExtName = strDesc;
                                            }

                                            DataExt2.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("CustomFieldValue2"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldValue2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtValue2 ( " + dr["CustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldValue2"].ToString().Substring(0, 4095);
                                                        DataExt2.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldValue2"].ToString();
                                                        DataExt2.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt2.DataExtValue = dr["CustomFieldValue2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldValue2"].ToString();
                                                DataExt2.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt2.DataExtName != null && DataExt2.DataExtValue != null && DataExt2.OwnerID != null)
                                    {
                                        EstLine.DataExt.Add(DataExt2);
                                    }

                                    DataProcessingBlocks.DataExt DataExt3 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("CustomFieldName3"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["CustomFieldName3"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldName3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtName3 ( " + dr["CustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldName3"].ToString().Substring(0, 4095);
                                                        DataExt3.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldName3"].ToString();
                                                        DataExt3.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt3.DataExtName = dr["CustomFieldName3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldName3"].ToString();
                                                DataExt3.DataExtName = strDesc;
                                            }

                                            DataExt3.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("CustomFieldValue3"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldValue3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtValue3 ( " + dr["CustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldValue3"].ToString().Substring(0, 4095);
                                                        DataExt3.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldValue3"].ToString();
                                                        DataExt3.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt3.DataExtValue = dr["CustomFieldValue3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldValue3"].ToString();
                                                DataExt3.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt3.DataExtName != null && DataExt3.DataExtValue != null && DataExt3.OwnerID != null)
                                    {
                                        EstLine.DataExt.Add(DataExt3);
                                    }
                                if (EstLine.ItemRef != null)
                                {
                                    Estimate.EstimateLineAdd.Add(EstLine);
                                }
                                if (row_cnt == 1)
                                {
                                    #region Estimate Line add for Freight

                                        if (dt.Columns.Contains("Freight"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding freight charge item to estimate line.
                                                EstLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                                EstLine.ItemRef.FullName = defaultSettings.Frieght;
                                                //Adding freight charge amount to estimate line.


                                                #region Validations for Rate
                                                if (dr["Freight"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (EstLine.Rate == null)
                                                            {
                                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                        }
                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion

                                            }
                                        }
                                        refcnt = 0;

                                        #endregion

                                        #region Estimate Line Add  for Discount

                                        if (dt.Columns.Contains("Discount"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding Discount charge item to estimate line.
                                                EstLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                                EstLine.ItemRef.FullName = defaultSettings.Discount;                                              

                                                #region Validations for Rate
                                                if (dr["Discount"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This Discount ( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (EstLine.Rate == null)
                                                            {
                                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                        }
                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion
                                            }
                                        }

                                        #endregion

                                        #region Estimate Line add for Insurance

                                        if (dt.Columns.Contains("Insurance"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding Insurance charge item to estimate line.
                                                EstLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                                EstLine.ItemRef.FullName = defaultSettings.Insurance;                                           

                                                #region Validations for Rate
                                                if (dr["Insurance"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This Insurance ( " + dr["Insurance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (EstLine.Rate == null)
                                                            {
                                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                        }
                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion
                                            }
                                        }

                                        #endregion

                                        //bug no. 410
                                        #region Estimate Line add for SalesTax

                                        if (dt.Columns.Contains("SalesTax"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding SalesTax charge item to Estimate line.                                          

                                                EstLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                                EstLine.ItemRef.FullName = defaultSettings.SalesTax;

                                                //Adding SalesTax charge amount to Estimate line.

                                                #region Validations for Rate
                                                if (dr["SalesTax"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                string strAmount = dr["SalesTax"].ToString();
                                                                EstLine.Amount = strAmount;
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                string strAmount = dr["SalesTax"].ToString();
                                                                EstLine.Amount = strAmount;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string strAmount = dr["SalesTax"].ToString();
                                                            EstLine.Amount = strAmount;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Amount = string.Format("{0:000000.00}", rate);
                                                                }
                                                            }
                                                            //Check if InvoiceLine.Rate is null
                                                            if (EstLine.Amount == null)
                                                            {
                                                                EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                        }

                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion
                                            }
                                        }


                                        #endregion
                                    }
                                   
                                }
                                #endregion

                                #region EstimateLineGroupAdd
                               // if (Estimate.EstimateLineAdd.Count==0)
                                //{
                                    DataProcessingBlocks.EstimateLineGroupAdd EstgroupLine = new EstimateLineGroupAdd();

                                    if (dt.Columns.Contains("ItemGroupFullName"))
                                    {

                                        #region Validations of item Full name
                                        if (dr["ItemGroupFullName"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupFullName"].ToString().Length > 1000)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Item full name (" + dr["ItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                        if (EstgroupLine.ItemGroupRef.FullName == null)
                                                            EstgroupLine.ItemGroupRef.FullName = null;
                                                        else
                                                            EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString().Substring(0, 1000));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                        if (EstgroupLine.ItemGroupRef.FullName == null)
                                                            EstgroupLine.ItemGroupRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                    if (EstgroupLine.ItemGroupRef.FullName == null)
                                                        EstgroupLine.ItemGroupRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                if (EstgroupLine.ItemGroupRef.FullName == null)
                                                    EstgroupLine.ItemGroupRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }

                                    if (dt.Columns.Contains("ItemGroupQuantity"))
                                    {
                                        #region Validations for Quantity
                                        if (dr["ItemGroupQuantity"].ToString() != string.Empty)
                                        {
                                            string strQuantity = dr["ItemGroupQuantity"].ToString();
                                            EstgroupLine.Quantity = strQuantity;
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("ItemGroupUoM"))
                                    {
                                        #region Validations for EstimateUnitOfMeasure
                                        if (dr["ItemGroupUoM"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupUoM"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupUoM ( " + dr["ItemGroupUoM"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strUnitOfMeasure = dr["ItemGroupUoM"].ToString().Substring(0, 31);
                                                        EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                        EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                }
                                                else
                                                {
                                                    string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                    EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("ItemGroupInventorySiteFullName"))
                                    {
                                        #region Validations of Inventory Site Ref Full name
                                        if (dr["ItemGroupInventorySiteFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["ItemGroupInventorySiteFullName"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroup InventorySite FullName (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteRef.FullName == null)
                                                            EstgroupLine.InventorySiteRef.FullName = null;
                                                        else
                                                            EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteRef.FullName == null)
                                                            EstgroupLine.InventorySiteRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteRef.FullName == null)
                                                        EstgroupLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                if (EstgroupLine.InventorySiteRef.FullName == null)
                                                    EstgroupLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }

                                    if (dt.Columns.Contains("InventorySiteLocationRefFullName"))
                                    {
                                        #region Validations of Inventory Site Location Ref Full name
                                        if (dr["InventorySiteLocationRefFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["InventorySiteLocationRefFullName"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Inventory Site Location Ref FullName (" + dr["InventorySiteLocationRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                            EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                        else
                                                            EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                            EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                        EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                    EstgroupLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }

                                    /// bug no 443
                                    DataProcessingBlocks.DataExt DataExt4 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("ItemGroupCustomFieldName1"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["ItemGroupCustomFieldName1"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldName1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldName1 ( " + dr["ItemGroupCustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldName1"].ToString().Substring(0, 4095);
                                                        DataExt4.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                        DataExt4.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt4.DataExtName = dr["ItemGroupCustomFieldName1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                DataExt4.DataExtName = strDesc;
                                            }

                                            DataExt4.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("ItemGroupCustomFieldValue1"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["ItemGroupCustomFieldValue1"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldValue1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldValue1 ( " + dr["ItemGroupCustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldValue1"].ToString().Substring(0, 4095);
                                                        DataExt4.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                        DataExt4.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt4.DataExtValue = dr["ItemGroupCustomFieldValue1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                DataExt4.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt4.DataExtName != null && DataExt4.DataExtValue != null && DataExt4.OwnerID != null)
                                    {
                                        EstgroupLine.DataExt.Add(DataExt4);
                                    }

                                    DataProcessingBlocks.DataExt DataExt5 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("ItemGroupCustomFieldName2"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["ItemGroupCustomFieldName2"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldName2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldName2 ( " + dr["ItemGroupCustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldName2"].ToString().Substring(0, 4095);
                                                        DataExt5.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                        DataExt5.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt5.DataExtName = dr["ItemGroupCustomFieldName2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                DataExt5.DataExtName = strDesc;
                                            }

                                            DataExt5.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("ItemGroupCustomFieldValue2"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["ItemGroupCustomFieldValue2"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldValue2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldValue2 ( " + dr["ItemGroupCustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldValue2"].ToString().Substring(0, 4095);
                                                        DataExt5.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                        DataExt5.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt5.DataExtValue = dr["ItemGroupCustomFieldValue2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                DataExt5.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt5.DataExtName != null && DataExt5.DataExtValue != null && DataExt5.OwnerID != null)
                                    {
                                        EstgroupLine.DataExt.Add(DataExt5);
                                    }

                                    DataProcessingBlocks.DataExt DataExt6 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("ItemGroupCustomFieldName3"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["ItemGroupCustomFieldName3"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldName3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldName3 ( " + dr["ItemGroupCustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldName3"].ToString().Substring(0, 4095);
                                                        DataExt6.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                        DataExt6.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt6.DataExtName = dr["ItemGroupCustomFieldName3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                DataExt6.DataExtName = strDesc;
                                            }

                                            DataExt6.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("ItemGroupCustomFieldValue3"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["ItemGroupCustomFieldValue3"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldValue3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldValue3 ( " + dr["ItemGroupCustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldValue3"].ToString().Substring(0, 4095);
                                                        DataExt6.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                        DataExt6.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt6.DataExtValue = dr["ItemGroupCustomFieldValue3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                DataExt6.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt6.DataExtName != null && DataExt6.DataExtValue != null && DataExt6.OwnerID != null)
                                    {
                                        EstgroupLine.DataExt.Add(DataExt6);
                                    }
                            if (EstgroupLine.ItemGroupRef != null)
                            {
                                if (EstgroupLine.ItemGroupRef.FullName != null)
                                    Estimate.EstimateLineGroupAdd.Add(EstgroupLine);
                            }
                            #endregion

                                coll.Add(Estimate);
                            }
                            else
                            {
                                #region Estimate Line Add
                                if (dt.Columns.Contains("ItemRefFullName"))
                                {
                                    DataProcessingBlocks.EstimateLineAdd EstLine = new EstimateLineAdd();


                                    #region Checking and setting SalesTaxCode

                                    if (defaultSettings == null)
                                    {
                                        CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        return null;

                                    }
                                    //string IsTaxable = string.Empty;
                                    string TaxRateValue = string.Empty;
                                    string ItemSaleTaxFullName = string.Empty;
                                    //if default settings contain checkBoxGrossToNet checked.
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                        {
                                            if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                            {
                                                string FullName = dr["SalesTaxCodeFullName"].ToString();                                                
                                                ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                            }
                                        }
                                    }
                                    #endregion
                                    // axis 23
                                    cnt = Estimate.EstimateLineAdd.Count - 1 - dt_count;

                                    // end axis 23
                                    if (dt.Columns.Contains("Freight") || dt.Columns.Contains("Insurance") || dt.Columns.Contains("Discount"))
                                    {
                                        for (int i = cnt; i >= 1; i--)
                                        {
                                            Estimate.EstimateLineAdd.RemoveAt(i);
                                        }
                                    }

                                    #region Validations of item Full name
                                    if (dr["ItemRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemRefFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                    if (EstLine.ItemRef.FullName == null)
                                                        EstLine.ItemRef.FullName = null;
                                                    else
                                                        EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                    if (EstLine.ItemRef.FullName == null)
                                                        EstLine.ItemRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (EstLine.ItemRef.FullName == null)
                                                    EstLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (EstLine.ItemRef.FullName == null)
                                                EstLine.ItemRef.FullName = null;
                                        }
                                    }
                                    #endregion


                                    if (dt.Columns.Contains("Description"))
                                    {
                                        #region Validations for Description
                                        if (dr["Description"].ToString() != string.Empty)
                                        {
                                            if (dr["Description"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["Description"].ToString().Substring(0, 4095);
                                                        EstLine.Desc = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["Description"].ToString();
                                                        EstLine.Desc = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Desc = dr["Description"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["Description"].ToString();
                                                EstLine.Desc = strDesc;
                                            }
                                        }

                                        #endregion
                                    }

                                    //bug no. 422

                                    if (dt.Columns.Contains("Other1"))
                                    {
                                        #region Validations for Other1
                                        if (dr["Other1"].ToString() != string.Empty)
                                        {
                                            if (dr["Other1"].ToString().Length > 29)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strOther1 = dr["Other1"].ToString().Substring(0, 29);
                                                        EstLine.Other1 = strOther1;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strOther1 = dr["Other1"].ToString();
                                                        EstLine.Other1 = strOther1;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Other1 = dr["Other1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strOther1 = dr["Other1"].ToString();
                                                EstLine.Other1 = strOther1;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("Other2"))
                                    {
                                        #region Validations for Other2
                                        if (dr["Other2"].ToString() != string.Empty)
                                        {
                                            if (dr["Other2"].ToString().Length > 29)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strOther2 = dr["Other2"].ToString().Substring(0, 29);
                                                        EstLine.Other2 = strOther2;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strOther2 = dr["Other2"].ToString();
                                                        EstLine.Other2 = strOther2;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Other2 = dr["Other2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strOther2 = dr["Other2"].ToString();
                                                EstLine.Other2 = strOther2;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("Quantity"))
                                    {
                                        #region Validations for Quantity
                                        if (dr["Quantity"].ToString() != string.Empty)
                                        {
                                            string strQuantity = dr["Quantity"].ToString();
                                            EstLine.Quantity = strQuantity;
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("EstimateUnitOfMeasure"))
                                    {
                                        #region Validations for EstimateUnitOfMeasure
                                        if (dr["EstimateUnitOfMeasure"].ToString() != string.Empty)
                                        {
                                            if (dr["EstimateUnitOfMeasure"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This EstimateUnitOfMeasure ( " + dr["EstimateUnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString().Substring(0, 31);
                                                        EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                        EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                }
                                                else
                                                {
                                                    string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                    EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                EstLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }

                                        #endregion

                                    }
                                    if (dt.Columns.Contains("Rate"))
                                    {
                                        #region Validations for Rate
                                        if (dr["Rate"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;                                           
                                            if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                        }
                                                    }
                                                    //Check if EstLine.Rate is null
                                                    if (EstLine.Rate == null)
                                                    {
                                                        EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                }
                                            }
                                        }

                                        #endregion
                                    }

                                    if (dt.Columns.Contains("EstLineClassRefFullName"))
                                    {
                                        #region Validations of Class Full name
                                        if (dr["EstLineClassRefFullName"].ToString() != string.Empty)
                                        {
                                            if (dr["EstLineClassRefFullName"].ToString().Length > 1000)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Estimate Line Class full name (" + dr["EstLineClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                        if (EstLine.ClassRef.FullName == null)
                                                            EstLine.ClassRef.FullName = null;
                                                        else
                                                            EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString().Substring(0, 1000));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                        if (EstLine.ClassRef.FullName == null)
                                                            EstLine.ClassRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                    if (EstLine.ClassRef.FullName == null)
                                                        EstLine.ClassRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                if (EstLine.ClassRef.FullName == null)
                                                    EstLine.ClassRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    if (dt.Columns.Contains("Amount"))
                                    {
                                        #region Validations for Amount
                                        if (dr["Amount"].ToString() != string.Empty)
                                        {
                                            decimal amount;
                                            if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strAmount = dr["Amount"].ToString();
                                                        EstLine.Amount = strAmount;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strAmount = dr["Amount"].ToString();
                                                        EstLine.Amount = strAmount;
                                                    }
                                                }
                                                else
                                                {
                                                    string strAmount = dr["Amount"].ToString();
                                                    EstLine.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                amount = Amount / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Amount = string.Format("{0:000000.00}", amount);
                                                        }
                                                    }
                                                    //Check if EstLine.Amount is null
                                                    if (EstLine.Amount == null)
                                                    {
                                                        EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                                }
                                            }
                                        }

                                        #endregion
                                    }

                                    //validation for InventorySiteRefFullName
                                    if (dt.Columns.Contains("InventorySiteRefFullName"))
                                    {
                                        #region Validations of Inventory Site Ref Full name
                                        if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Inventory Site Ref name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                        if (EstLine.InventorySiteRef.FullName == null)
                                                            EstLine.InventorySiteRef.FullName = null;
                                                        else
                                                            EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                        if (EstLine.InventorySiteRef.FullName == null)
                                                            EstLine.InventorySiteRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                    if (EstLine.InventorySiteRef.FullName == null)
                                                        EstLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (EstLine.InventorySiteRef.FullName == null)
                                                    EstLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }
                                    if (dt.Columns.Contains("InventorySiteLocationRef"))
                                    {
                                        #region Validations of InventorySiteLocationRef

                                        if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                        {
                                            if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                        if (EstLine.InventorySiteLocationRef.FullName == null)
                                                            EstLine.InventorySiteLocationRef.FullName = null;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                        if (EstLine.InventorySiteLocationRef.FullName == null)
                                                            EstLine.InventorySiteLocationRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                    if (EstLine.InventorySiteLocationRef.FullName == null)
                                                        EstLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (EstLine.InventorySiteLocationRef.FullName == null)
                                                    EstLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                    {
                                        #region Validations of sales tax code Full name
                                        if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This sales tax code name (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                        if (EstLine.SalesTaxCodeRef.FullName == null)
                                                            EstLine.SalesTaxCodeRef.FullName = null;
                                                        else
                                                            EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                        if (EstLine.SalesTaxCodeRef.FullName == null)
                                                            EstLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                    if (EstLine.SalesTaxCodeRef.FullName == null)
                                                        EstLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (EstLine.SalesTaxCodeRef.FullName == null)
                                                    EstLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    if (dt.Columns.Contains("MarkupRate"))
                                    {
                                        #region Validations for MarkupRate
                                        if (dr["MarkupRate"].ToString() != string.Empty)
                                        {
                                            decimal amount;
                                            if (!decimal.TryParse(dr["MarkupRate"].ToString(), out amount))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This MarkupRate ( " + dr["MarkupRate"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strMarkupRate = dr["MarkupRate"].ToString();
                                                        EstLine.MarkupRate = strMarkupRate;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strMarkupRate = dr["MarkupRate"].ToString();
                                                        EstLine.MarkupRate = strMarkupRate;
                                                    }
                                                }
                                                else
                                                {
                                                    string strMarkupRate = dr["MarkupRate"].ToString();
                                                    EstLine.MarkupRate = strMarkupRate;
                                                }
                                            }
                                            else
                                            {

                                                // EstLine.MarkupRate = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["MarkupRate"].ToString())));
                                                EstLine.MarkupRate = string.Format("{0:000000.00}", dr["MarkupRate"].ToString());

                                            }
                                        }

                                        #endregion
                                    }

                                    if (dt.Columns.Contains("MarkupRatePercent"))
                                    {
                                        #region Validations for MarkupRatePercent
                                        if (dr["MarkupRatePercent"].ToString() != string.Empty)
                                        {
                                            decimal amount;
                                            if (!decimal.TryParse(dr["MarkupRatePercent"].ToString(), out amount))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This MarkupRatePercent ( " + dr["MarkupRatePercent"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                        EstLine.MarkupRatePercent = strMarkupRate;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                        EstLine.MarkupRatePercent = strMarkupRate;
                                                    }
                                                }
                                                else
                                                {
                                                    string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                    EstLine.MarkupRatePercent = strMarkupRate;
                                                }
                                            }
                                            else
                                            {

                                                EstLine.MarkupRatePercent = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["MarkupRatePercent"].ToString())));

                                            }
                                        }

                                        #endregion
                                    }

                                    //New Feature::601

                                    if (dt.Columns.Contains("PriceLevelRefFullName"))
                                    {
                                        #region Validations of PriceLevel FullName
                                        if (dr["PriceLevelRefFullName"].ToString() != string.Empty)
                                        {
                                            if (dr["PriceLevelRefFullName"].ToString().Length > 100)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                        if (EstLine.PriceLevelRef.FullName == null)
                                                            EstLine.PriceLevelRef.FullName = null;
                                                        else
                                                            EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString().Substring(0, 100));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                        if (EstLine.PriceLevelRef.FullName == null)
                                                            EstLine.PriceLevelRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                    if (EstLine.PriceLevelRef.FullName == null)
                                                        EstLine.PriceLevelRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (EstLine.PriceLevelRef.FullName == null)
                                                    EstLine.PriceLevelRef.FullName = null;
                                            }
                                        }
                                        #endregion
                                    }

                                    /// bug no 443
                                    DataProcessingBlocks.DataExt DataExt1 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("CustomFieldName1"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["CustomFieldName1"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldName1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtName1 ( " + dr["CustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldName1"].ToString().Substring(0, 4095);
                                                        DataExt1.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldName1"].ToString();
                                                        DataExt1.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt1.DataExtName = dr["CustomFieldName1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldName1"].ToString();
                                                DataExt1.DataExtName = strDesc;
                                            }

                                            DataExt1.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("CustomFieldValue1"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldValue1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtValue1 ( " + dr["CustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldValue1"].ToString().Substring(0, 4095);
                                                        DataExt1.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldValue1"].ToString();
                                                        DataExt1.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt1.DataExtValue = dr["CustomFieldValue1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldValue1"].ToString();
                                                DataExt1.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt1.DataExtName != null && DataExt1.DataExtValue != null && DataExt1.OwnerID != null)
                                    {
                                        EstLine.DataExt.Add(DataExt1);
                                    }

                                    DataProcessingBlocks.DataExt DataExt2 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("CustomFieldName2"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["CustomFieldName2"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldName2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtName2 ( " + dr["CustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldName2"].ToString().Substring(0, 4095);
                                                        DataExt2.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldName2"].ToString();
                                                        DataExt2.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt2.DataExtName = dr["CustomFieldName2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldName2"].ToString();
                                                DataExt2.DataExtName = strDesc;
                                            }

                                            DataExt2.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("CustomFieldValue2"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldValue2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtValue2 ( " + dr["CustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldValue2"].ToString().Substring(0, 4095);
                                                        DataExt2.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldValue2"].ToString();
                                                        DataExt2.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt2.DataExtValue = dr["CustomFieldValue2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldValue2"].ToString();
                                                DataExt2.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt2.DataExtName != null && DataExt2.DataExtValue != null && DataExt2.OwnerID != null)
                                    {
                                        EstLine.DataExt.Add(DataExt2);
                                    }

                                    DataProcessingBlocks.DataExt DataExt3 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("CustomFieldName3"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["CustomFieldName3"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldName3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtName3 ( " + dr["CustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldName3"].ToString().Substring(0, 4095);
                                                        DataExt3.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldName3"].ToString();
                                                        DataExt3.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt3.DataExtName = dr["CustomFieldName3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldName3"].ToString();
                                                DataExt3.DataExtName = strDesc;
                                            }

                                            DataExt3.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("CustomFieldValue3"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                        {
                                            if (dr["CustomFieldValue3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Line DataExtValue3 ( " + dr["CustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["CustomFieldValue3"].ToString().Substring(0, 4095);
                                                        DataExt3.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["CustomFieldValue3"].ToString();
                                                        DataExt3.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt3.DataExtValue = dr["CustomFieldValue3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["CustomFieldValue3"].ToString();
                                                DataExt3.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }

                                    if (DataExt3.DataExtName != null && DataExt3.DataExtValue != null && DataExt3.OwnerID != null)
                                    {
                                        EstLine.DataExt.Add(DataExt3);
                                    }
                                if (EstLine.ItemRef != null)
                                {
                                    Estimate.EstimateLineAdd.Add(EstLine);
                                }

                                    if (EstimateQBEntryCollection.ref_count == datatbl_cnt - 1)
                                    {
                                        refcnt = 1;
                                    }
                                    if (refcnt == 1)
                                    {

                                        #region Estimate Line add for Freight

                                        if (dt.Columns.Contains("Freight"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding freight charge item to estimate line.
                                                EstLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                                EstLine.ItemRef.FullName = defaultSettings.Frieght;
                                                //Adding freight charge amount to estimate line.


                                                #region Validations for Rate
                                                if (dr["Freight"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (EstLine.Rate == null)
                                                            {
                                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                        }
                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion

                                            }
                                        }
                                        refcnt = 0;

                                        #endregion
                                        #region Estimate Line Add  for Discount

                                        if (dt.Columns.Contains("Discount"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding Discount charge item to estimate line.
                                                EstLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                                EstLine.ItemRef.FullName = defaultSettings.Discount;                                             

                                                #region Validations for Rate
                                                if (dr["Discount"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This Discount ( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (EstLine.Rate == null)
                                                            {
                                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                        }
                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion
                                            }
                                        }

                                        #endregion
                                        #region Estimate Line add for Insurance

                                        if (dt.Columns.Contains("Insurance"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding Insurance charge item to estimate line.
                                                EstLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                                EstLine.ItemRef.FullName = defaultSettings.Insurance;
                                                //Adding Insurance charge amount to estimate line.
                                                #region Commented Code for Amount which is not required
                                                //#region Validations for Insurance Amount
                                                //if (dr["Insurance"].ToString() != string.Empty)
                                                //{
                                                //    decimal amount;
                                                //    if (!decimal.TryParse(dr["Insurance"].ToString(), out amount))
                                                //    {
                                                //        if (isIgnoreAll == false)
                                                //        {
                                                //            string strMessages = "This Insurance amount ( " + dr["Insurance"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                //            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                //            if (Convert.ToString(result) == "Cancel")
                                                //            {
                                                //                continue;
                                                //            }
                                                //            if (Convert.ToString(result) == "No")
                                                //            {
                                                //                return null;
                                                //            }
                                                //            if (Convert.ToString(result) == "Ignore")
                                                //            {
                                                //                string strAmount = dr["Insurance"].ToString();
                                                //                EstLine.Amount = strAmount;
                                                //            }
                                                //            if (Convert.ToString(result) == "Abort")
                                                //            {
                                                //                isIgnoreAll = true;
                                                //                string strAmount = dr["Insurance"].ToString();
                                                //                EstLine.Amount = strAmount;
                                                //            }
                                                //        }
                                                //        else
                                                //        {
                                                //            string strAmount = dr["Insurance"].ToString();
                                                //            EstLine.Amount = strAmount;
                                                //        }
                                                //    }
                                                //    else
                                                //    {
                                                //        if (defaultSettings.GrossToNet == "1")
                                                //        {
                                                //            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                //            {
                                                //                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                //                {
                                                //                    decimal Amount = Convert.ToDecimal(dr["Insurance"].ToString());
                                                //                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                //                       CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                //                       CommonUtilities.GetInstance().CountryVersion == "CA")
                                                //                    {
                                                //                        //decimal TaxRate = 10;
                                                //                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                //                        amount = Amount / (1 + (TaxRate / 100));
                                                //                    }

                                                //                    EstLine.Amount = string.Format("{0:000000.00}", amount);

                                                //                }
                                                //            }
                                                //            //Check if EstLine.Amount is null
                                                //            if (EstLine.Amount == null)
                                                //            {
                                                //                EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Insurance"].ToString()));
                                                //            }
                                                //        }
                                                //        else
                                                //        {
                                                //            EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Insurance"].ToString()));
                                                //        }
                                                //    }
                                                //}

                                                //Estimate.EstimateLineAdd.Add(EstLine);
                                                //#endregion
                                                #endregion

                                                #region Validations for Rate
                                                if (dr["Insurance"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This Insurance ( " + dr["Insurance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (EstLine.Rate == null)
                                                            {
                                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                        }
                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion
                                            }
                                        }

                                        #endregion
                                        //bug no. 410
                                        #region Estimate Line add for SalesTax

                                        if (dt.Columns.Contains("SalesTax"))
                                        {
                                            if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                                            {
                                                EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                                //Adding SalesTax charge item to Estimate line.                                          

                                                EstLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                                EstLine.ItemRef.FullName = defaultSettings.SalesTax;

                                                //Adding SalesTax charge amount to Estimate line.

                                                #region Validations for Rate
                                                if (dr["SalesTax"].ToString() != string.Empty)
                                                {
                                                    decimal rate = 0;
                                                    //decimal amount;
                                                    if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                                    {
                                                        if (isIgnoreAll == false)
                                                        {
                                                            string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                            if (Convert.ToString(result) == "Cancel")
                                                            {
                                                                continue;
                                                            }
                                                            if (Convert.ToString(result) == "No")
                                                            {
                                                                return null;
                                                            }
                                                            if (Convert.ToString(result) == "Ignore")
                                                            {
                                                                string strAmount = dr["SalesTax"].ToString();
                                                                EstLine.Amount = strAmount;
                                                            }
                                                            if (Convert.ToString(result) == "Abort")
                                                            {
                                                                isIgnoreAll = true;
                                                                string strAmount = dr["SalesTax"].ToString();
                                                                EstLine.Amount = strAmount;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string strAmount = dr["SalesTax"].ToString();
                                                            EstLine.Amount = strAmount;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {
                                                                        //decimal TaxRate = 10;
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    EstLine.Amount = string.Format("{0:000000.00}", rate);
                                                                }
                                                            }
                                                            //Check if InvoiceLine.Rate is null
                                                            if (EstLine.Amount == null)
                                                            {
                                                                EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                        }

                                                    }
                                                }
                                                Estimate.EstimateLineAdd.Add(EstLine);
                                                #endregion
                                            }
                                        }


                                        #endregion

                                    }
                                }
                               #endregion
                                #region EstimateLineGroupAdd
                                //if (Estimate.EstimateLineAdd.Count == 0)
                                //{
                                    DataProcessingBlocks.EstimateLineGroupAdd EstgroupLine = new EstimateLineGroupAdd();

                                    if (dt.Columns.Contains("ItemGroupFullName"))
                                    {

                                        #region Validations of item Full name
                                        if (dr["ItemGroupFullName"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupFullName"].ToString().Length > 1000)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Item full name (" + dr["ItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                        if (EstgroupLine.ItemGroupRef.FullName == null)
                                                            EstgroupLine.ItemGroupRef.FullName = null;
                                                        else
                                                            EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString().Substring(0, 1000));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                        if (EstgroupLine.ItemGroupRef.FullName == null)
                                                            EstgroupLine.ItemGroupRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                    if (EstgroupLine.ItemGroupRef.FullName == null)
                                                        EstgroupLine.ItemGroupRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                if (EstgroupLine.ItemGroupRef.FullName == null)
                                                    EstgroupLine.ItemGroupRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }

                                    if (dt.Columns.Contains("ItemGroupQuantity"))
                                    {
                                        #region Validations for Quantity
                                        if (dr["ItemGroupQuantity"].ToString() != string.Empty)
                                        {
                                            string strQuantity = dr["ItemGroupQuantity"].ToString();
                                            EstgroupLine.Quantity = strQuantity;
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("ItemGroupUoM"))
                                    {
                                        #region Validations for EstimateUnitOfMeasure
                                        if (dr["ItemGroupUoM"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupUoM"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupUoM ( " + dr["ItemGroupUoM"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strUnitOfMeasure = dr["ItemGroupUoM"].ToString().Substring(0, 31);
                                                        EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                        EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                    }
                                                }
                                                else
                                                {
                                                    string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                    EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }

                                        #endregion

                                    }

                                    if (dt.Columns.Contains("ItemGroupInventorySiteFullName"))
                                    {
                                        #region Validations of Inventory Site Ref Full name
                                        if (dr["ItemGroupInventorySiteFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["ItemGroupInventorySiteFullName"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroup InventorySite FullName (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteRef.FullName == null)
                                                            EstgroupLine.InventorySiteRef.FullName = null;
                                                        else
                                                            EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteRef.FullName == null)
                                                            EstgroupLine.InventorySiteRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteRef.FullName == null)
                                                        EstgroupLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                if (EstgroupLine.InventorySiteRef.FullName == null)
                                                    EstgroupLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }

                                    if (dt.Columns.Contains("InventorySiteLocationRefFullName"))
                                    {
                                        #region Validations of Inventory Site Location Ref Full name
                                        if (dr["InventorySiteLocationRefFullName"].ToString() != string.Empty)
                                        {

                                            if (dr["InventorySiteLocationRefFullName"].ToString().Length > 31)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Inventory Site Location Ref FullName (" + dr["InventorySiteLocationRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                            EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                        else
                                                            EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString().Substring(0, 3));

                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                        if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                            EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                    }
                                                }
                                                else
                                                {
                                                    EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                        EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                    EstgroupLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        #endregion

                                    }
                                    /// bug no 443
                                    DataProcessingBlocks.DataExt DataExt4 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("ItemGroupCustomFieldName1"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["ItemGroupCustomFieldName1"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldName1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldName1 ( " + dr["ItemGroupCustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldName1"].ToString().Substring(0, 4095);
                                                        DataExt4.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                        DataExt4.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt4.DataExtName = dr["ItemGroupCustomFieldName1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                DataExt4.DataExtName = strDesc;
                                            }

                                            DataExt4.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("ItemGroupCustomFieldValue1"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["ItemGroupCustomFieldValue1"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldValue1"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldValue1 ( " + dr["ItemGroupCustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldValue1"].ToString().Substring(0, 4095);
                                                        DataExt4.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                        DataExt4.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt4.DataExtValue = dr["ItemGroupCustomFieldValue1"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                DataExt4.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    if (DataExt4.DataExtName != null && DataExt4.DataExtValue != null && DataExt4.OwnerID != null)
                                    {
                                        EstgroupLine.DataExt.Add(DataExt4);
                                    }

                                    DataProcessingBlocks.DataExt DataExt5 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("ItemGroupCustomFieldName2"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["ItemGroupCustomFieldName2"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldName2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldName2 ( " + dr["ItemGroupCustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldName2"].ToString().Substring(0, 4095);
                                                        DataExt5.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                        DataExt5.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt5.DataExtName = dr["ItemGroupCustomFieldName2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                DataExt5.DataExtName = strDesc;
                                            }

                                            DataExt5.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("ItemGroupCustomFieldValue2"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["ItemGroupCustomFieldValue2"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldValue2"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldValue2 ( " + dr["ItemGroupCustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldValue2"].ToString().Substring(0, 4095);
                                                        DataExt5.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                        DataExt5.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt5.DataExtValue = dr["ItemGroupCustomFieldValue2"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                DataExt5.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    ///
                                    if (DataExt5.DataExtName != null && DataExt5.DataExtValue != null && DataExt5.OwnerID != null)
                                    {
                                        EstgroupLine.DataExt.Add(DataExt5);
                                    }

                                    DataProcessingBlocks.DataExt DataExt6 = new DataProcessingBlocks.DataExt();

                                    if (dt.Columns.Contains("ItemGroupCustomFieldName3"))
                                    {
                                        #region Validations for DataExtName
                                        if (dr["ItemGroupCustomFieldName3"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldName3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldName3 ( " + dr["ItemGroupCustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldName3"].ToString().Substring(0, 4095);
                                                        DataExt6.DataExtName = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                        DataExt6.DataExtName = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt6.DataExtName = dr["ItemGroupCustomFieldName3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                DataExt6.DataExtName = strDesc;
                                            }

                                            DataExt6.OwnerID = "0";
                                        }

                                        #endregion
                                    }
                                    if (dt.Columns.Contains("ItemGroupCustomFieldValue3"))
                                    {
                                        #region Validations for DataExtValue
                                        if (dr["ItemGroupCustomFieldValue3"].ToString() != string.Empty)
                                        {
                                            if (dr["ItemGroupCustomFieldValue3"].ToString().Length > 4095)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This ItemGroupCustomFieldValue3 ( " + dr["ItemGroupCustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strDesc = dr["ItemGroupCustomFieldValue3"].ToString().Substring(0, 4095);
                                                        DataExt6.DataExtValue = strDesc;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                        DataExt6.DataExtValue = strDesc;
                                                    }
                                                }
                                                else
                                                {
                                                    DataExt6.DataExtValue = dr["ItemGroupCustomFieldValue3"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                DataExt6.DataExtValue = strDesc;
                                            }
                                        }

                                        #endregion
                                    }
                                    ///
                                    if (DataExt6.DataExtName != null && DataExt6.DataExtValue != null && DataExt6.OwnerID != null)
                                    {
                                        EstgroupLine.DataExt.Add(DataExt6);
                                    }
                            if (EstgroupLine.ItemGroupRef != null)
                            {
                                if (EstgroupLine.ItemGroupRef.FullName != null)
                                    Estimate.EstimateLineGroupAdd.Add(EstgroupLine);
                            }
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        EstimateQBEntry Estimate = new DataProcessingBlocks.EstimateQBEntry();

                            #region Without Adding ref number
                            if (dt.Columns.Contains("CustomerRefFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CustomerRefFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                //string strCustomerFullname = string.Empty;
                                                if (Estimate.CustomerRef.FullName == null)
                                                {
                                                    Estimate.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                //string strCustomerFullname = string.Empty;
                                                if (Estimate.CustomerRef.FullName == null)
                                                {
                                                    Estimate.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (Estimate.CustomerRef.FullName == null)
                                            {
                                                Estimate.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (Estimate.CustomerRef.FullName == null)
                                        {
                                            Estimate.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            ///bug 442 11.4 
                            if (dt.Columns.Contains("Currency"))
                            {
                                #region Validations of Currency Full name
                                if (dr["Currency"].ToString() != string.Empty)
                                {
                                    string strCust = dr["Currency"].ToString();
                                    if (strCust.Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Estimate.CurrencyRef.FullName == null)
                                                {
                                                    Estimate.CurrencyRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Estimate.CurrencyRef.FullName == null)
                                                {
                                                    Estimate.CurrencyRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Estimate.CurrencyRef.FullName == null)
                                            {
                                                Estimate.CurrencyRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (Estimate.CurrencyRef.FullName == null)
                                        {
                                            Estimate.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                                {
                                                    Estimate.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 1000));

                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                                {
                                                    Estimate.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                            {
                                                Estimate.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Estimate.ClassRef.FullName == null && Estimate.ClassRef.ListID == null)
                                        {
                                            Estimate.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TemplateRefFullName"))
                            {
                                #region Validations of Template Full name
                                if (dr["TemplateRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["TemplateRefFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Template full name (" + dr["TemplateRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (Estimate.TemplateRef.FullName == null)
                                                    Estimate.TemplateRef.FullName = null;
                                                else
                                                    Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (Estimate.TemplateRef.FullName == null)
                                                    Estimate.TemplateRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (Estimate.TemplateRef.FullName == null)
                                                Estimate.TemplateRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Estimate.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                        if (Estimate.TemplateRef.FullName == null)
                                            Estimate.TemplateRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out EstimateDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Estimate.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            EstimateDt = dttest;
                                            Estimate.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        EstimateDt = Convert.ToDateTime(datevalue);
                                        Estimate.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("EstimateRefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    Estimate.EstimateDate = EstimateDt;

                                if (dr["EstimateRefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["EstimateRefNumber"].ToString();
                                    if (strRefNum.Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["EstimateRefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.RefNumber = dr["EstimateRefNumber"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.RefNumber = dr["EstimateRefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.RefNumber = dr["EstimateRefNumber"].ToString();
                                        }

                                    }
                                    else
                                        Estimate.RefNumber = dr["EstimateRefNumber"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsActive"))
                            {
                                #region Validations of IsActive
                                if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsActive"].ToString(), out result))
                                    {
                                        Estimate.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsActive"].ToString().ToLower() == "true")
                                        {
                                            Estimate.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsActive"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Estimate.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Estimate.IsActive = dr["IsActive"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.IsActive = dr["IsActive"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Estimate.IsActive = dr["IsActive"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("PONumber"))
                            {
                                #region Validations of PONumber
                                if (dr["PONumber"].ToString() != string.Empty)
                                {
                                    string strCust = dr["PONumber"].ToString();
                                    if (strCust.Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PONumber (" + dr["PONumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.PONumber = dr["PONumber"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.PONumber = dr["PONumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.PONumber = dr["PONumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Estimate.PONumber = dr["PONumber"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("TermsRefFullName"))
                            {
                                #region Validations of Terms Full name
                                if (dr["TermsRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["TermsRefFullName"].ToString();
                                    if (strCust.Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                if (Estimate.TermsRef.FullName == null)
                                                {
                                                    Estimate.TermsRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString().Substring(0, 100));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                if (Estimate.TermsRef.FullName == null)
                                                {
                                                    Estimate.TermsRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                            if (Estimate.TermsRef.FullName == null)
                                            {
                                                Estimate.TermsRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());

                                        if (Estimate.TermsRef.FullName == null)
                                        {
                                            Estimate.TermsRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            DateTime NewDueDt = new DateTime();
                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region validations of DueDate
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    string duevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(duevalue, out NewDueDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            //DialogResult dgv = MessageBox.Show("TxnDate is not valid for this mapping.","Warning",MessageBoxButtons.);
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Estimate.DueDate = duevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.DueDate = duevalue;
                                                }
                                            }
                                            else
                                                Estimate.DueDate = duevalue;
                                        }
                                        else
                                        {
                                            Estimate.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        Estimate.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }


                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRep Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["SalesRepRefFullName"].ToString().Length > 5)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Salesrep full name (" + dr["SalesRepRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.SalesRepRef.FullName == null)
                                                {
                                                    Estimate.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 5));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (Estimate.SalesRepRef.FullName == null)
                                                {
                                                    Estimate.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (Estimate.SalesRepRef.FullName == null)
                                            {
                                                Estimate.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.SalesRepRef = new QuickBookEntities.SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Estimate.SalesRepRef.FullName == null)
                                        {
                                            Estimate.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("FOB"))
                            {
                                #region Validations of FOB
                                if (dr["FOB"].ToString() != string.Empty)
                                {
                                    string strCust = dr["FOB"].ToString();
                                    if (strCust.Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FOB (" + dr["FOB"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.FOB = dr["FOB"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.FOB = dr["FOB"].ToString();
                                            }
                                        }
                                        else
                                            Estimate.FOB = dr["FOB"].ToString();
                                    }
                                    else
                                    {
                                        Estimate.FOB = dr["FOB"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ItemSalesTaxRefFullName"))
                            {
                                #region Validations of ItemSalesTax Full name
                                if (dr["ItemSalesTaxRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemSalesTaxRefFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemSalesTaxRef full name (" + dr["ItemSalesTaxRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                                {
                                                    Estimate.ItemSalesTaxRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString().Substring(0, 100));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                                {
                                                    Estimate.ItemSalesTaxRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                            if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                            {
                                                Estimate.ItemSalesTaxRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Estimate.ItemSalesTaxRef.FullName == null && Estimate.ItemSalesTaxRef.ListID == null)
                                        {
                                            Estimate.ItemSalesTaxRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                                Estimate.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                Estimate.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Memo = dr["Memo"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Estimate.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }

                            QuickBookEntities.BillAddress BillAddressItem = new BillAddress();
                            if (dt.Columns.Contains("BillAddr1"))
                            {
                                #region Validations of Bill Addr1
                                if (dr["BillAddr1"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr1 = dr["BillAddr1"].ToString().Substring(0, 41);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillAddr2"))
                            {
                                #region Validations of Bill Addr2
                                if (dr["BillAddr2"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr2 = dr["BillAddr2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillAddr3"))
                            {
                                #region Validations of Bill Addr3
                                if (dr["BillAddr3"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr3 = dr["BillAddr3"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                            }
                                        }
                                        else
                                            BillAddressItem.Addr3 = dr["BillAddr3"].ToString();

                                    }
                                    else
                                    {
                                        BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillAddr4"))
                            {
                                #region Validations of Bill Addr4
                                if (dr["BillAddr4"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr4 = dr["BillAddr4"].ToString().Substring(0, 41);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillAddr5"))
                            {
                                #region Validations of Bill Addr5
                                if (dr["BillAddr5"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr5 = dr["BillAddr5"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                            }
                                        }
                                        else
                                            BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillCity"))
                            {
                                #region Validations of Bill City
                                if (dr["BillCity"].ToString() != string.Empty)
                                {
                                    if (dr["BillCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill City (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.City = dr["BillCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.City = dr["BillCity"].ToString();
                                            }
                                        }
                                        else
                                            BillAddressItem.City = dr["BillCity"].ToString();
                                    }
                                    else
                                    {
                                        BillAddressItem.City = dr["BillCity"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillState"))
                            {
                                #region Validations of Bill State
                                if (dr["BillState"].ToString() != string.Empty)
                                {
                                    if (dr["BillState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill State (" + dr["BillState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.State = dr["BillState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.State = dr["BillState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.State = dr["BillState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.State = dr["BillState"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillPostalCode"))
                            {
                                #region Validations of Bill Postal Code
                                if (dr["BillPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["BillPostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Postal Code (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {

                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                            }
                                        }
                                        else
                                            BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                    }
                                    else
                                    {
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillCountry"))
                            {
                                #region Validations of Bill Country
                                if (dr["BillCountry"].ToString() != string.Empty)
                                {
                                    if (dr["BillCountry"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Country (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Country = dr["BillCountry"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Country = dr["BillCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Country = dr["BillCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Country = dr["BillCountry"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillNote"))
                            {
                                #region Validations of Bill Note
                                if (dr["BillNote"].ToString() != string.Empty)
                                {
                                    if (dr["BillNote"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Note (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Note = dr["BillNote"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Note = dr["BillNote"].ToString();
                                            }
                                        }
                                        else
                                            BillAddressItem.Note = dr["BillNote"].ToString();
                                    }
                                    else
                                    {
                                        BillAddressItem.Note = dr["BillNote"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (BillAddressItem.Addr1 != null || BillAddressItem.Addr2 != null || BillAddressItem.Addr3 != null || BillAddressItem.Addr4 != null || BillAddressItem.Addr5 != null
                                || BillAddressItem.City != null || BillAddressItem.Country != null || BillAddressItem.PostalCode != null || BillAddressItem.State != null || BillAddressItem.Note != null)
                                Estimate.BillAddress.Add(BillAddressItem);

                            if (dt.Columns.Contains("Phone"))
                            {
                                #region Validations of Phone
                                if (dr["Phone"].ToString() != string.Empty)
                                {
                                    if (dr["Phone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.Phone = dr["Phone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.Phone = dr["Phone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Estimate.Phone = dr["Phone"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Fax"))
                            {
                                #region Validations of Fax
                                if (dr["Fax"].ToString() != string.Empty)
                                {
                                    if (dr["Fax"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.Fax = dr["Fax"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.Fax = dr["Fax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Estimate.Fax = dr["Fax"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Email"))
                            {
                                #region Validations of Email
                                if (dr["Email"].ToString() != string.Empty)
                                {
                                    if (dr["Email"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.Email = dr["Email"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.Email = dr["Email"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Estimate.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Estimate.Email = dr["Email"].ToString();
                                    }
                                }
                                #endregion
                            }
                            QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();
                            if (dt.Columns.Contains("ShipAddr1"))
                            {
                                #region Validations of Ship Addr1
                                if (dr["ShipAddr1"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr2"))
                            {
                                #region Validations of Ship Addr2
                                if (dr["ShipAddr2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr3"))
                            {
                                #region Validations of Ship Addr3
                                if (dr["ShipAddr3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipAddr4"))
                            {
                                #region Validations of Ship Addr4
                                if (dr["ShipAddr4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr5"))
                            {
                                #region Validations of Ship Addr5
                                if (dr["ShipAddr5"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipCity"))
                            {
                                #region Validations of Ship City
                                if (dr["ShipCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.City = dr["ShipCity"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipState"))
                            {
                                #region Validations of Ship State
                                if (dr["ShipState"].ToString() != string.Empty)
                                {
                                    if (dr["ShipState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.State = dr["ShipState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipPostalCode"))
                            {
                                #region Validations of Ship Postal Code
                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 13);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipCountry"))
                            {
                                #region Validations of Ship Country
                                if (dr["ShipCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCountry"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipNote"))
                            {
                                #region Validations of Ship Note
                                if (dr["ShipNote"].ToString() != string.Empty)
                                {
                                    if (dr["ShipNote"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Note = dr["ShipNote"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Note = dr["ShipNote"].ToString();
                                            }
                                        }
                                        else
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                    else
                                    {
                                        ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                              || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                                Estimate.ShipAddress.Add(ShipAddressItem);

                            if (dt.Columns.Contains("CustomerMsgRefFullName"))
                            {
                                #region Validations of CustomerMsgRef Full name
                                if (dr["CustomerMsgRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerMsgRefFullName"].ToString().Length > 101)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerMsgRef full name (" + dr["CustomerMsgRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.CustomerMsgRef.FullName == null)
                                                {
                                                    Estimate.CustomerMsgRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString().Substring(0, 101));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.CustomerMsgRef.FullName == null)
                                                {
                                                    Estimate.CustomerMsgRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                            //string strClassFullName = string.Empty;
                                            if (Estimate.CustomerMsgRef.FullName == null)
                                            {
                                                Estimate.CustomerMsgRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                        if (Estimate.CustomerMsgRef.FullName == null)
                                        {
                                            Estimate.CustomerMsgRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("IsToBeEmailed"))
                            {
                                #region Validations of IsToBeEmailed
                                if (dr["IsToBeEmailed"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBeEmailed"].ToString(), out result))
                                    {
                                        Estimate.IsToBeEmailed = Convert.ToInt32(dr["IsToBeEmailed"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBeEmailed"].ToString().ToLower() == "true")
                                        {
                                            Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBeEmailed"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBeEmailed (" + dr["IsToBeEmailed"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                }
                                            }
                                            else
                                                Estimate.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsTaxIncluded"))
                            {
                                #region Validations of IsTaxIncluded
                                if (dr["IsTaxIncluded"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                    {
                                        Estimate.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                            }
                                            else
                                                Estimate.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CustomerSalesTaxCodeFullName"))
                            {
                                #region Validations of CustomerSalesTaxCode Full name
                                if (dr["CustomerSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerSalesTaxCodeFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerSalesTaxCode full name (" + dr["CustomerSalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                                //string strClassFullName = string.Empty;
                                                if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                                {
                                                    Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                                if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                                {
                                                    Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                            if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                            {
                                                Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Estimate.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Estimate.CustomerSalesTaxCodeRef.FullName == null)
                                        {
                                            Estimate.CustomerSalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Estimate.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Estimate.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                            Estimate.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                    else
                                    {
                                        //Estimate.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                        Estimate.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                        
                                    }
                                }

                                #endregion

                            }

                            #region Estimate Line Add
                            if (dt.Columns.Contains("ItemRefFullName"))
                            {

                                DataProcessingBlocks.EstimateLineAdd EstLine = new EstimateLineAdd();

                                #region Checking and setting SalesTaxCode

                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;

                                }
                                //string IsTaxable = string.Empty;
                                string TaxRateValue = string.Empty;
                                string ItemSaleTaxFullName = string.Empty;
                                //if default settings contain checkBoxGrossToNet checked.
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                    {
                                        if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["SalesTaxCodeFullName"].ToString();
                                            //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                                #endregion

                                #region Validations of item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (EstLine.ItemRef.FullName == null)
                                                    EstLine.ItemRef.FullName = null;
                                                else
                                                    EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (EstLine.ItemRef.FullName == null)
                                                    EstLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (EstLine.ItemRef.FullName == null)
                                                EstLine.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        EstLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (EstLine.ItemRef.FullName == null)
                                            EstLine.ItemRef.FullName = null;
                                    }
                                }
                                #endregion

                                if (dt.Columns.Contains("Description"))
                                {
                                    #region Validations for Description
                                    if (dr["Description"].ToString() != string.Empty)
                                    {
                                        if (dr["Description"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["Description"].ToString().Substring(0, 4095);
                                                    EstLine.Desc = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["Description"].ToString();
                                                    EstLine.Desc = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.Desc = dr["Description"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            EstLine.Desc = strDesc;
                                        }
                                    }

                                    #endregion

                                }
                                if (dt.Columns.Contains("Quantity"))
                                {
                                    #region Validations for Quantity
                                    if (dr["Quantity"].ToString() != string.Empty)
                                    {
                                        string strQuantity = dr["Quantity"].ToString();
                                        EstLine.Quantity = strQuantity;
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("EstimateUnitOfMeasure"))
                                {
                                    #region Validations for EstimateUnitOfMeasure
                                    if (dr["EstimateUnitOfMeasure"].ToString() != string.Empty)
                                    {
                                        if (dr["EstimateUnitOfMeasure"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This EstimateUnitOfMeasure ( " + dr["EstimateUnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                    EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                    EstLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                                EstLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["EstimateUnitOfMeasure"].ToString();
                                            EstLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }

                                    #endregion

                                }
                                if (dt.Columns.Contains("Rate"))
                                {
                                    #region Validations for Rate
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        //decimal amount;
                                        if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                    {
                                                        decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            //decimal TaxRate = 10;
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            rate = Rate / (1 + (TaxRate / 100));
                                                        }

                                                        EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                    }
                                                }
                                                //Check if EstLine.Rate is null
                                                if (EstLine.Rate == null)
                                                {
                                                    EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                }
                                            }
                                            else
                                            {
                                                EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("EstLineClassRefFullName"))
                                {
                                    #region Validations of Class Full name
                                    if (dr["EstLineClassRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["EstLineClassRefFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Estimate Line Class full name (" + dr["EstLineClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                    if (EstLine.ClassRef.FullName == null)
                                                        EstLine.ClassRef.FullName = null;
                                                    else
                                                        EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString().Substring(0, 1000));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                    if (EstLine.ClassRef.FullName == null)
                                                        EstLine.ClassRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                                if (EstLine.ClassRef.FullName == null)
                                                    EstLine.ClassRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.ClassRef = new ClassRef(dr["EstLineClassRefFullName"].ToString());
                                            if (EstLine.ClassRef.FullName == null)
                                                EstLine.ClassRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("Amount"))
                                {
                                    #region Validations for Amount
                                    if (dr["Amount"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strAmount = dr["Amount"].ToString();
                                                    EstLine.Amount = strAmount;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strAmount = dr["Amount"].ToString();
                                                    EstLine.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                EstLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                    {
                                                        decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            //decimal TaxRate = 10;
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            amount = Amount / (1 + (TaxRate / 100));
                                                        }

                                                        EstLine.Amount = string.Format("{0:000000.00}", amount);
                                                    }
                                                }
                                                //Check if EstLine.Amount is null
                                                if (EstLine.Amount == null)
                                                {
                                                    EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                                }
                                            }
                                            else
                                            {
                                                EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                    }

                                    #endregion
                                }

                                //validation for InventorySiteRefFullName
                                if (dt.Columns.Contains("InventorySiteRefFullName"))
                                {
                                    #region Validations of Inventory Site Ref Full name
                                    if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                    {

                                        if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Inventory Site Ref name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                    if (EstLine.InventorySiteRef.FullName == null)
                                                        EstLine.InventorySiteRef.FullName = null;
                                                    else
                                                        EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 3));

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                    if (EstLine.InventorySiteRef.FullName == null)
                                                        EstLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (EstLine.InventorySiteRef.FullName == null)
                                                    EstLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (EstLine.InventorySiteRef.FullName == null)
                                                EstLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    #endregion


                                }
                                if (dt.Columns.Contains("InventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                    if (EstLine.InventorySiteLocationRef.FullName == null)
                                                        EstLine.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                    if (EstLine.InventorySiteLocationRef.FullName == null)
                                                        EstLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (EstLine.InventorySiteLocationRef.FullName == null)
                                                    EstLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (EstLine.InventorySiteLocationRef.FullName == null)
                                                EstLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    #region Validations of sales tax code Full name
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {

                                        if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This sales tax code name (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                    if (EstLine.SalesTaxCodeRef.FullName == null)
                                                        EstLine.SalesTaxCodeRef.FullName = null;
                                                    else
                                                        EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                    if (EstLine.SalesTaxCodeRef.FullName == null)
                                                        EstLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (EstLine.SalesTaxCodeRef.FullName == null)
                                                    EstLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (EstLine.SalesTaxCodeRef.FullName == null)
                                                EstLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }


                                if (dt.Columns.Contains("MarkupRate"))
                                {
                                    #region Validations for MarkupRate
                                    if (dr["MarkupRate"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["MarkupRate"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This MarkupRate ( " + dr["MarkupRate"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strMarkupRate = dr["MarkupRate"].ToString();
                                                    EstLine.MarkupRate = strMarkupRate;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strMarkupRate = dr["MarkupRate"].ToString();
                                                    EstLine.MarkupRate = strMarkupRate;
                                                }
                                            }
                                            else
                                            {
                                                string strMarkupRate = dr["MarkupRate"].ToString();
                                                EstLine.MarkupRate = strMarkupRate;
                                            }
                                        }
                                        else
                                        {

                                            // EstLine.MarkupRate = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["MarkupRate"].ToString())));
                                            EstLine.MarkupRate = string.Format("{0:000000.00}", dr["MarkupRate"].ToString());

                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("MarkupRatePercent"))
                                {
                                    #region Validations for MarkupRatePercent
                                    if (dr["MarkupRatePercent"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["MarkupRatePercent"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This MarkupRatePercent ( " + dr["MarkupRatePercent"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                    EstLine.MarkupRatePercent = strMarkupRate;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                    EstLine.MarkupRatePercent = strMarkupRate;
                                                }
                                            }
                                            else
                                            {
                                                string strMarkupRate = dr["MarkupRatePercent"].ToString();
                                                EstLine.MarkupRatePercent = strMarkupRate;
                                            }
                                        }
                                        else
                                        {

                                            EstLine.MarkupRatePercent = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["MarkupRatePercent"].ToString())));

                                        }
                                    }

                                    #endregion
                                }

                                //New Feature::601

                                if (dt.Columns.Contains("PriceLevelRefFullName"))
                                {
                                    #region Validations of PriceLevel FullName
                                    if (dr["PriceLevelRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["PriceLevelRefFullName"].ToString().Length > 100)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                    if (EstLine.PriceLevelRef.FullName == null)
                                                        EstLine.PriceLevelRef.FullName = null;
                                                    else
                                                        EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString().Substring(0, 100));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                    if (EstLine.PriceLevelRef.FullName == null)
                                                        EstLine.PriceLevelRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (EstLine.PriceLevelRef.FullName == null)
                                                    EstLine.PriceLevelRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                            if (EstLine.PriceLevelRef.FullName == null)
                                                EstLine.PriceLevelRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                                /// bug no 443
                                DataProcessingBlocks.DataExt DataExt1 = new DataProcessingBlocks.DataExt();

                                if (dt.Columns.Contains("CustomFieldName1"))
                                {
                                    #region Validations for DataExtName
                                    if (dr["CustomFieldName1"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomFieldName1"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Line DataExtName1 ( " + dr["CustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["CustomFieldName1"].ToString().Substring(0, 4095);
                                                    DataExt1.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["CustomFieldName1"].ToString();
                                                    DataExt1.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt1.DataExtName = dr["CustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName1"].ToString();
                                            DataExt1.DataExtName = strDesc;
                                        }

                                        DataExt1.OwnerID = "0";
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("CustomFieldValue1"))
                                {
                                    #region Validations for DataExtValue
                                    if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomFieldValue1"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Line DataExtValue1 ( " + dr["CustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["CustomFieldValue1"].ToString().Substring(0, 4095);
                                                    DataExt1.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["CustomFieldValue1"].ToString();
                                                    DataExt1.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt1.DataExtValue = dr["CustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue1"].ToString();
                                            DataExt1.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (DataExt1.DataExtName != null && DataExt1.DataExtValue != null && DataExt1.OwnerID != null)
                                {
                                    EstLine.DataExt.Add(DataExt1);
                                }
                                DataProcessingBlocks.DataExt DataExt2 = new DataProcessingBlocks.DataExt();

                                if (dt.Columns.Contains("CustomFieldName2"))
                                {
                                    #region Validations for DataExtName
                                    if (dr["CustomFieldName2"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomFieldName2"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Line DataExtName2 ( " + dr["CustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["CustomFieldName2"].ToString().Substring(0, 4095);
                                                    DataExt2.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["CustomFieldName2"].ToString();
                                                    DataExt2.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt2.DataExtName = dr["CustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName2"].ToString();
                                            DataExt2.DataExtName = strDesc;
                                        }

                                        DataExt2.OwnerID = "0";
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("CustomFieldValue2"))
                                {
                                    #region Validations for DataExtValue
                                    if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomFieldValue2"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Line DataExtValue2 ( " + dr["CustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["CustomFieldValue2"].ToString().Substring(0, 4095);
                                                    DataExt2.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["CustomFieldValue2"].ToString();
                                                    DataExt2.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt2.DataExtValue = dr["CustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue2"].ToString();
                                            DataExt2.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (DataExt2.DataExtName != null && DataExt2.DataExtValue != null && DataExt2.OwnerID != null)
                                {
                                    EstLine.DataExt.Add(DataExt2);
                                }
                                DataProcessingBlocks.DataExt DataExt3 = new DataProcessingBlocks.DataExt();

                                if (dt.Columns.Contains("CustomFieldName3"))
                                {
                                    #region Validations for DataExtName
                                    if (dr["CustomFieldName3"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomFieldName3"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Line DataExtName3 ( " + dr["CustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["CustomFieldName3"].ToString().Substring(0, 4095);
                                                    DataExt3.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["CustomFieldName3"].ToString();
                                                    DataExt3.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt3.DataExtName = dr["CustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName3"].ToString();
                                            DataExt3.DataExtName = strDesc;
                                        }

                                        DataExt3.OwnerID = "0";
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("CustomFieldValue3"))
                                {
                                    #region Validations for DataExtValue
                                    if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                    {
                                        if (dr["CustomFieldValue3"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Line DataExtValue3 ( " + dr["CustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["CustomFieldValue3"].ToString().Substring(0, 4095);
                                                    DataExt3.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["CustomFieldValue3"].ToString();
                                                    DataExt3.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt3.DataExtValue = dr["CustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue3"].ToString();
                                            DataExt3.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                ///
                                if (DataExt3.DataExtName != null && DataExt3.DataExtValue != null && DataExt3.OwnerID != null)
                                {
                                    EstLine.DataExt.Add(DataExt3);
                                }
                            if (EstLine.ItemRef != null)
                            {
                                Estimate.EstimateLineAdd.Add(EstLine);
                            }

                                #region Estimate Line add for Insurance

                                if (dt.Columns.Contains("Insurance"))
                                {
                                    if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                    {
                                        EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                        //Adding Insurance charge item to estimate line.
                                        EstLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                        EstLine.ItemRef.FullName = defaultSettings.Insurance;
                                        
                                        #region Validations for Rate
                                        if (dr["Insurance"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                            //decimal amount;
                                            if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Insurance ( " + dr["Insurance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                    EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                        }
                                                    }
                                                    //Check if EstLine.Rate is null
                                                    if (EstLine.Rate == null)
                                                    {
                                                        EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                }
                                            }
                                        }
                                        Estimate.EstimateLineAdd.Add(EstLine);
                                        #endregion
                                    }
                                }

                                #endregion

                                #region Estimate Line Add  for Discount

                                if (dt.Columns.Contains("Discount"))
                                {
                                    if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                    {
                                        EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                        //Adding Discount charge item to estimate line.
                                        EstLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                        EstLine.ItemRef.FullName = defaultSettings.Discount;
                                       
                                        #region Validations for Rate
                                        if (dr["Discount"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                            //decimal amount;
                                            if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Discount ( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                        EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                    EstLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                        }
                                                    }
                                                    //Check if EstLine.Rate is null
                                                    if (EstLine.Rate == null)
                                                    {
                                                        EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                }
                                            }
                                        }
                                        Estimate.EstimateLineAdd.Add(EstLine);
                                        #endregion
                                    }
                                }

                                #endregion

                                //bug no. 410
                                #region Estimate Line add for SalesTax

                                if (dt.Columns.Contains("SalesTax"))
                                {
                                    if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                                    {
                                        EstLine = new DataProcessingBlocks.EstimateLineAdd();

                                        //Adding SalesTax charge item to Estimate line.                                          

                                        EstLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                        EstLine.ItemRef.FullName = defaultSettings.SalesTax;

                                        //Adding SalesTax charge amount to Estimate line.

                                        #region Validations for Rate
                                        if (dr["SalesTax"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                            //decimal amount;
                                            if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strAmount = dr["SalesTax"].ToString();
                                                        EstLine.Amount = strAmount;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strAmount = dr["SalesTax"].ToString();
                                                        EstLine.Amount = strAmount;
                                                    }
                                                }
                                                else
                                                {
                                                    string strAmount = dr["SalesTax"].ToString();
                                                    EstLine.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Estimate.IsTaxIncluded != null && Estimate.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Estimate.IsTaxIncluded == "true" || Estimate.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            EstLine.Amount = string.Format("{0:000000.00}", rate);
                                                        }
                                                    }
                                                    //Check if InvoiceLine.Rate is null
                                                    if (EstLine.Amount == null)
                                                    {
                                                        EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    EstLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                }

                                            }
                                        }
                                        Estimate.EstimateLineAdd.Add(EstLine);
                                        #endregion
                                    }
                                }


                                #endregion
                            }
                            #endregion

                            #region EstimateLineGroupAdd
                           // if (Estimate.EstimateLineAdd.Count == 0)
                           // {
                                DataProcessingBlocks.EstimateLineGroupAdd EstgroupLine = new EstimateLineGroupAdd();

                                if (dt.Columns.Contains("ItemGroupFullName"))
                                {

                                    #region Validations of item Full name
                                    if (dr["ItemGroupFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item full name (" + dr["ItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                    if (EstgroupLine.ItemGroupRef.FullName == null)
                                                        EstgroupLine.ItemGroupRef.FullName = null;
                                                    else
                                                        EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString().Substring(0, 1000));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                    if (EstgroupLine.ItemGroupRef.FullName == null)
                                                        EstgroupLine.ItemGroupRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                                if (EstgroupLine.ItemGroupRef.FullName == null)
                                                    EstgroupLine.ItemGroupRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstgroupLine.ItemGroupRef = new ItemRef(dr["ItemGroupFullName"].ToString());
                                            if (EstgroupLine.ItemGroupRef.FullName == null)
                                                EstgroupLine.ItemGroupRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("ItemGroupQuantity"))
                                {
                                    #region Validations for Quantity
                                    if (dr["ItemGroupQuantity"].ToString() != string.Empty)
                                    {
                                        string strQuantity = dr["ItemGroupQuantity"].ToString();
                                        EstgroupLine.Quantity = strQuantity;
                                    }

                                    #endregion

                                }

                                if (dt.Columns.Contains("ItemGroupUoM"))
                                {
                                    #region Validations for EstimateUnitOfMeasure
                                    if (dr["ItemGroupUoM"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupUoM"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupUoM ( " + dr["ItemGroupUoM"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strUnitOfMeasure = dr["ItemGroupUoM"].ToString().Substring(0, 31);
                                                    EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                    EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                                EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["ItemGroupUoM"].ToString();
                                            EstgroupLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }

                                    #endregion

                                }

                                if (dt.Columns.Contains("ItemGroupInventorySiteFullName"))
                                {
                                    #region Validations of Inventory Site Ref Full name
                                    if (dr["ItemGroupInventorySiteFullName"].ToString() != string.Empty)
                                    {

                                        if (dr["ItemGroupInventorySiteFullName"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroup InventorySite FullName (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteRef.FullName == null)
                                                        EstgroupLine.InventorySiteRef.FullName = null;
                                                    else
                                                        EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString().Substring(0, 3));

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteRef.FullName == null)
                                                        EstgroupLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                                if (EstgroupLine.InventorySiteRef.FullName == null)
                                                    EstgroupLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstgroupLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["ItemGroupInventorySiteFullName"].ToString());
                                            if (EstgroupLine.InventorySiteRef.FullName == null)
                                                EstgroupLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }

                                if (dt.Columns.Contains("InventorySiteLocationRefFullName"))
                                {
                                    #region Validations of Inventory Site Location Ref Full name
                                    if (dr["InventorySiteLocationRefFullName"].ToString() != string.Empty)
                                    {

                                        if (dr["InventorySiteLocationRefFullName"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Inventory Site Location Ref FullName (" + dr["InventorySiteLocationRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                        EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                    else
                                                        EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString().Substring(0, 3));

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                    if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                        EstgroupLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                                if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                    EstgroupLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            EstgroupLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRefFullName"].ToString());
                                            if (EstgroupLine.InventorySiteLocationRef.FullName == null)
                                                EstgroupLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }

                                /// bug no 443
                                DataProcessingBlocks.DataExt DataExt4 = new DataProcessingBlocks.DataExt();

                                if (dt.Columns.Contains("ItemGroupCustomFieldName1"))
                                {
                                    #region Validations for DataExtName
                                    if (dr["ItemGroupCustomFieldName1"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName1"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName1 ( " + dr["ItemGroupCustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName1"].ToString().Substring(0, 4095);
                                                    DataExt4.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                    DataExt4.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt4.DataExtName = dr["ItemGroupCustomFieldName1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                            DataExt4.DataExtName = strDesc;
                                        }

                                        DataExt4.OwnerID = "0";
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ItemGroupCustomFieldValue1"))
                                {
                                    #region Validations for DataExtValue
                                    if (dr["ItemGroupCustomFieldValue1"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue1"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue1 ( " + dr["ItemGroupCustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue1"].ToString().Substring(0, 4095);
                                                    DataExt4.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                    DataExt4.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt4.DataExtValue = dr["ItemGroupCustomFieldValue1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                            DataExt4.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                ///
                                if (DataExt4.DataExtName != null && DataExt4.DataExtValue != null && DataExt4.OwnerID != null)
                                {
                                    EstgroupLine.DataExt.Add(DataExt4);
                                }

                                DataProcessingBlocks.DataExt DataExt5 = new DataProcessingBlocks.DataExt();

                                if (dt.Columns.Contains("ItemGroupCustomFieldName2"))
                                {
                                    #region Validations for DataExtName
                                    if (dr["ItemGroupCustomFieldName2"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName2"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName2 ( " + dr["ItemGroupCustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName2"].ToString().Substring(0, 4095);
                                                    DataExt5.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                    DataExt5.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt5.DataExtName = dr["ItemGroupCustomFieldName2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                            DataExt5.DataExtName = strDesc;
                                        }

                                        DataExt5.OwnerID = "0";
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ItemGroupCustomFieldValue2"))
                                {
                                    #region Validations for DataExtValue
                                    if (dr["ItemGroupCustomFieldValue2"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue2"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue2 ( " + dr["ItemGroupCustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue2"].ToString().Substring(0, 4095);
                                                    DataExt5.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                    DataExt5.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt5.DataExtValue = dr["ItemGroupCustomFieldValue2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                            DataExt5.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                ///
                                if (DataExt5.DataExtName != null && DataExt5.DataExtValue != null && DataExt5.OwnerID != null)
                                {
                                    EstgroupLine.DataExt.Add(DataExt5);
                                }

                                DataProcessingBlocks.DataExt DataExt6 = new DataProcessingBlocks.DataExt();

                                if (dt.Columns.Contains("ItemGroupCustomFieldName3"))
                                {
                                    #region Validations for DataExtName
                                    if (dr["ItemGroupCustomFieldName3"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName3"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName3 ( " + dr["ItemGroupCustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName3"].ToString().Substring(0, 4095);
                                                    DataExt6.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                    DataExt6.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt6.DataExtName = dr["ItemGroupCustomFieldName3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                            DataExt6.DataExtName = strDesc;
                                        }

                                        DataExt6.OwnerID = "0";
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ItemGroupCustomFieldValue3"))
                                {
                                    #region Validations for DataExtValue
                                    if (dr["ItemGroupCustomFieldValue3"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue3"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue3 ( " + dr["ItemGroupCustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue3"].ToString().Substring(0, 4095);
                                                    DataExt6.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                    DataExt6.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                DataExt6.DataExtValue = dr["ItemGroupCustomFieldValue3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                            DataExt6.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                ///
                                if (DataExt6.DataExtName != null && DataExt6.DataExtValue != null && DataExt6.OwnerID != null)
                                {
                                    EstgroupLine.DataExt.Add(DataExt6);
                                }
                        if (EstgroupLine.ItemGroupRef != null)
                        {
                            if (EstgroupLine.ItemGroupRef.FullName != null)
                                Estimate.EstimateLineGroupAdd.Add(EstgroupLine);
                        }
                        #endregion

                            coll.Add(Estimate);

                            #endregion
                        }
                    }
                    else
                    {
                        return null;
                    }                
            }
        
            #endregion

            #region Customer,Item and Account Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                   if (CommonUtilities.GetInstance().SkipListFlag == false)
                   {
                    if (dt.Columns.Contains("CustomerRefFullName"))
                    {

                        if (dr["CustomerRefFullName"].ToString() != string.Empty)
                        {
                            string customerName = dr["CustomerRefFullName"].ToString();
                            string[] arr = new string[15];
                            /// 11.4 bug 442
                                string[] CurrencyArr = new string[10];

                            if (customerName.Contains(":"))
                            {
                                arr = customerName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["CustomerRefFullName"].ToString();
                            }

                                /// 11.4 bug 442
                                if (dt.Columns.Contains("Currency"))
                                {
                                    if (dr["Currency"].ToString() != string.Empty)
                                    {
                                        string Currency = dr["Currency"].ToString();

                                        CurrencyArr[0] = dr["Currency"].ToString();
                                    }
                                }
                                #region Set Customer Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                        qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                        CustomerQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    FullName.InnerText = FullName.InnerText.TrimEnd(':');
                                                    CustomerQueryRq.AppendChild(FullName);
                                                }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        CustomerQueryRq.AppendChild(FullName);
                                    }
                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {

                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {
                                                #region Customer Add Query

                                                XmlDocument xmldocadd = new XmlDocument();
                                                xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                xmldocadd.AppendChild(qbXMLcust);
                                                XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                CustomerAddRq.SetAttribute("requestID", "1");
                                                XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                CustomerAddRq.AppendChild(CustomerAdd);

                                                XmlElement Name = xmldocadd.CreateElement("Name");
                                                Name.InnerText = arr[i];
                                                CustomerAdd.AppendChild(Name);

                                                   

                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                                INIChildFullName.InnerText = INIChildFullName.InnerText.TrimEnd(':');
                                                            XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                            CustomerAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }
                                                #region Adding Bill Address of Customer.

                                                if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                    (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                {
                                                    XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                    CustomerAdd.AppendChild(BillAddress);
                                                    if (dt.Columns.Contains("BillAddr1"))
                                                    {

                                                        if (dr["BillAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                            BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                            BillAddress.AppendChild(BillAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr2"))
                                                    {
                                                        if (dr["BillAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                            BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                            BillAddress.AppendChild(BillAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr3"))
                                                    {
                                                        if (dr["BillAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                            BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                            BillAddress.AppendChild(BillAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr4"))
                                                    {
                                                        if (dr["BillAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                            BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                            BillAddress.AppendChild(BillAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr5"))
                                                    {
                                                        if (dr["BillAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                            BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                            BillAddress.AppendChild(BillAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCity"))
                                                    {
                                                        if (dr["BillCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCity = xmldocadd.CreateElement("City");
                                                            BillCity.InnerText = dr["BillCity"].ToString();
                                                            BillAddress.AppendChild(BillCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillState"))
                                                    {
                                                        if (dr["BillState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillState = xmldocadd.CreateElement("State");
                                                            BillState.InnerText = dr["BillState"].ToString();
                                                            BillAddress.AppendChild(BillState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillPostalCode"))
                                                    {
                                                        if (dr["BillPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                            BillAddress.AppendChild(BillPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCountry"))
                                                    {
                                                        if (dr["BillCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                            BillCountry.InnerText = dr["BillCountry"].ToString();
                                                            BillAddress.AppendChild(BillCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillNote"))
                                                    {
                                                        if (dr["BillNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                            BillNote.InnerText = dr["BillNote"].ToString();
                                                            BillAddress.AppendChild(BillNote);
                                                        }
                                                    }
                                                }

                                                #endregion

                                                #region Adding Ship Address of Customer.
                                                if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                   (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                {
                                                    XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                    CustomerAdd.AppendChild(ShipAddress);
                                                    if (dt.Columns.Contains("ShipAddr1"))
                                                    {

                                                        if (dr["ShipAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                            ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr2"))
                                                    {
                                                        if (dr["ShipAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                            ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr3"))
                                                    {
                                                        if (dr["ShipAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                            ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr4"))
                                                    {
                                                        if (dr["ShipAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                            ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr5"))
                                                    {
                                                        if (dr["ShipAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                            ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCity"))
                                                    {
                                                        if (dr["ShipCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                            ShipCity.InnerText = dr["ShipCity"].ToString();
                                                            ShipAddress.AppendChild(ShipCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipState"))
                                                    {
                                                        if (dr["ShipState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipState = xmldocadd.CreateElement("State");
                                                            ShipState.InnerText = dr["ShipState"].ToString();
                                                            ShipAddress.AppendChild(ShipState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipPostalCode"))
                                                    {
                                                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                            ShipAddress.AppendChild(ShipPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCountry"))
                                                    {
                                                        if (dr["ShipCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                            ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                            ShipAddress.AppendChild(ShipCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipNote"))
                                                    {
                                                        if (dr["ShipNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                            ShipNote.InnerText = dr["ShipNote"].ToString();
                                                            ShipAddress.AppendChild(ShipNote);
                                                        }
                                                    }

                                                }


                                                #endregion

                                                if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                                {
                                                    if (dt.Columns.Contains("Phone"))
                                                    {
                                                        if (dr["Phone"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                            Phone.InnerText = dr["Phone"].ToString();
                                                            CustomerAdd.AppendChild(Phone);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Fax"))
                                                    {
                                                        if (dr["Fax"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                            Fax.InnerText = dr["Fax"].ToString();
                                                            CustomerAdd.AppendChild(Fax);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Email"))
                                                    {
                                                        if (dr["Email"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Email = xmldocadd.CreateElement("Email");
                                                            Email.InnerText = dr["Email"].ToString();
                                                            CustomerAdd.AppendChild(Email);
                                                        }
                                                    }
                                                }

                                                    ///11.4 bug no 442 Add currency 452 and 454
                                                    if (CurrencyArr[0] != null)
                                                    {
                                                        XmlElement CurrencyRef = xmldocadd.CreateElement("CurrencyRef");
                                                        XmlElement FullName = xmldocadd.CreateElement("FullName");
                                                        FullName.InnerText = CurrencyArr[0];
                                                        CurrencyRef.AppendChild(FullName);
                                                        CustomerAdd.AppendChild(CurrencyRef);
                                                    }

                                                    string custinput = xmldocadd.OuterXml;
                                                    string respcust = string.Empty;
                                                    try
                                                    {
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                    }

                                                    else
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                }
                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                finally
                                                {
                                                    if (respcust != string.Empty)
                                                    {
                                                        System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                        outputcustXMLDoc.LoadXml(respcust);
                                                        foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                        {
                                                            string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                            if (statusSeveritycust == "Error")
                                                            {
                                                                string msg = "New Customer could not be created into QuickBooks \n ";
                                                                msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                //Task 1435 (Axis 6.0):
                                                                ErrorSummary summary = new ErrorSummary(msg);
                                                                summary.ShowDialog();
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }

                            }
                               #endregion
                        }
                    }

                    //Solution for BUG 633
                    if (dt.Columns.Contains("ItemRefFullName"))
                    {
                        if (dr["ItemRefFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemRefFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemRefFullName"].ToString();
                            }

                            #region Setting SalesTaxCode and IsTaxIncluded

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                      
                            string TaxRateValue = string.Empty;
                            string IsTaxIncluded = string.Empty;
                            string netRate = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;

                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);
                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }

                                //validate IsTaxInluded value if present
                                if (dt.Columns.Contains("IsTaxIncluded"))
                                {
                                    if (dr["IsTaxIncluded"].ToString() != string.Empty && dr["IsTaxIncluded"].ToString() != "<None>")
                                    {
                                        int result = 0;
                                        if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                        {
                                            IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                            {
                                                IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }

                                    }
                                }

                                //Calculate cost
                                if (dt.Columns.Contains("Rate"))
                                {
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                        {
                                            if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                            {
                                                decimal Rate;
                                                if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                                {
                                                   
                                                    netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                }
                                                else
                                                {
                                                    Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    netRate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                        }
                                        if (netRate == string.Empty)
                                        {
                                            netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dt.Columns.Contains("Rate"))
                                {
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                    }
                                }
                            }

                            #endregion

                            #region Set Item Query

                            for (int i = 0; i < arr.Length; i++)
                            {
                                int item = 0;
                                int a = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");


                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");

                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        ItemQueryRq.AppendChild(FullName);
                                    }
                                   
                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }
                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {

                                                if (defaultSettings.Type == "NonInventoryPart")
                                                {
                                                    #region Item NonInventory Add Query

                                                    XmlDocument ItemNonInvendoc = new XmlDocument();
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                                    XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                    ItemNonInvendoc.AppendChild(qbXMLINI);
                                                    XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                    qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                    qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                    ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                    ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                    XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                    ININame.InnerText = arr[i];
                                                    //ININame.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemNonInventoryAdd.AppendChild(ININame);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }

                                                    //Adding Tax Code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {

                                                            XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                            ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                                    //Adding Desc And Rate
                                                    //Solution for BUG 631 and 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement INIDesc = ItemNonInvendoc.CreateElement("Desc");
                                                            INIDesc.InnerText = dr["Description"].ToString();
                                                            INISalesAndPurchase.AppendChild(INIDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemNonInvendoc.CreateElement("Price");
                                                            ISRate.InnerText = netRate;
                                                            INISalesAndPurchase.AppendChild(ISRate);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                        INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                        //INIFullName.InnerText = "Sales";
                                                        INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                    }
                                                    string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                                    //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                                    string respItemNonInvendoc = string.Empty;
                                                    try
                                                    {
                                                        respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemNonInvendoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strtest2 = respItemNonInvendoc;

                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "Service")
                                                {
                                                    #region Item Service Add Query

                                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemServiceAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }
                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                        }
                                                    }


                                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                                    //Adding Desc And Rate
                                                    //Solution for BUG 631 and 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ISSalesAndPurchase.AppendChild(ISDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemServiceAdddoc.CreateElement("Price");
                                                            ISRate.InnerText = netRate;
                                                            ISSalesAndPurchase.AppendChild(ISRate);
                                                            IsPresent = true;
                                                        }
                                                    }

                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                        //Adding IncomeAccount FullName.
                                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                        IsPresent = true;
                                                    }

                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                  
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                     
                                                        respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemInventoryAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                ItemInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }

                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    //Adding Desc
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ItemInventoryAdd.AppendChild(ISDesc);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                            ISRate.InnerText = netRate;
                                                            ItemInventoryAdd.AppendChild(ISRate);
                                                        }
                                                    }


                                                    //Adding IncomeAccountRef
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                    }

                                                    //Adding COGSAccountRef
                                                    if (defaultSettings.COGSAccount != string.Empty)
                                                    {
                                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                    }

                                                    //Adding AssetAccountRef
                                                    if (defaultSettings.AssetAccount != string.Empty)
                                                    {
                                                        XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                        XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                        INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                    }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                         respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest4 = respItemInventoryAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            else
            {
                    return null;
            }
          }
            #endregion

            #endregion

            return coll;
        }
   
    }
}
