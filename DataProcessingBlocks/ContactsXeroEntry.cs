﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;
using System.Linq;
using TransactionImporter;
using Xero.NetStandard.OAuth2.Model;
using Xero.NetStandard.OAuth2.Api;
using DataProcessingBlocks.XeroConnection;
using Newtonsoft.Json;
using Xero.NetStandard.OAuth2.Models;
using System.Threading.Tasks;


namespace DataProcessingBlocks
{
    [XmlRootAttribute("ContactsXeroEntry", Namespace = "", IsNullable = false)]
    public class ContactsXeroEntry
    {
        #region Private Member Variable

        private Guid m_ContactID;
        private string m_ContactStatus;
        private string m_Name;
        private string m_FirstName;
        private string m_LastName;
        private string m_EmailAddress;
        private string m_SkypeUserName;
        private string m_BankAccountDetails;
        private string m_TaxNumber;
        private string m_AccountsReceivableTaxType;
        private string m_AccountsPayableTaxType;
        private Collection<QuickBookEntities.Addresses> m_Addresses = new Collection<QuickBookEntities.Addresses>();
        private Collection<QuickBookEntities.Phones> m_Phones = new Collection<QuickBookEntities.Phones>();
        private DateTime m_UpdatedDateUTC;
        private ContactGroups m_ContactGroups;
        private bool m_IsSupplier;
        private bool m_IsCustomer;
        private string m_DefaultCurrency;
        #endregion

        #region Constructor
        public ContactsXeroEntry()
        {
        }
        #endregion

        #region Public Properties

        public Guid ContactID
        {
            get { return m_ContactID; }
            set { m_ContactID = value; }
        }

        public string ContactStatus
        {
            get { return m_ContactStatus; }
            set { m_ContactStatus = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }


        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }


        public string EmailAddress
        {
            get { return m_EmailAddress; }
            set { m_EmailAddress = value; }
        }


        public string SkypeUserName
        {
            get { return m_SkypeUserName; }
            set { m_SkypeUserName = value; }
        }


        public string BankAccountDetails
        {
            get { return m_BankAccountDetails; }
            set { m_BankAccountDetails = value; }
        }

        public string TaxNumber
        {
            get { return m_TaxNumber; }
            set { m_TaxNumber = value; }
        }

        public string AccountsReceivableTaxType
        {
            get { return m_AccountsReceivableTaxType; }
            set { m_AccountsReceivableTaxType = value; }
        }

        public string AccountsPayableTaxType
        {
            get { return m_AccountsPayableTaxType; }
            set { m_AccountsPayableTaxType = value; }
        }

        [XmlArray("AddressesREM")]
        public Collection<QuickBookEntities.Addresses> Addresses
        {
            get { return m_Addresses; }
            set { m_Addresses = value; }
        }

        [XmlArray("PhonesREM")]
        public Collection<QuickBookEntities.Phones> Phones
        {
            get { return m_Phones; }
            set { m_Phones = value; }
        }
        
        public ContactGroups ContactGroups
        {
            get { return m_ContactGroups; }
            set { m_ContactGroups = value; }
        }
        
        public bool IsSupplier
        {
            get { return m_IsSupplier; }
            set { m_IsSupplier = value; }
        }

        public bool IsCustomer
        {
            get { return m_IsCustomer; }
            set { m_IsCustomer = value; }
        }

        public string DefaultCurrency
        {
            get { return m_DefaultCurrency; }
            set { m_DefaultCurrency = value; }
        }
        
        #endregion

        #region Public Methods
        /// <summary>
        ///  fetch name from xero
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<bool> CheckAndGetNameExistsInXeroAsync(string name)
        {
            try
            {
                Contacts response = new Contacts();
                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                string query = "Name == \"" + name + "\"";
                var contactResponse =  res.GetContactsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, null, query, null, null, null, null);
                contactResponse.Wait();
                response = contactResponse.Result;
                if (response._Contacts.Count > 0)
                {
                    TransactionImporter.TransactionImporter.refnoflag = true;
                    CommonUtilities.GetInstance().existTxnId =  (response._Contacts[0].ContactID ?? Guid.Empty);
                }
                else
                {
                    CommonUtilities.GetInstance().existTxnId = Guid.Empty;
                    TransactionImporter.TransactionImporter.refnoflag = false;
                }
            }
            catch (Exception ex)
            {
                
            }
            return TransactionImporter.TransactionImporter.refnoflag;
        }

        /// Creating request file for exporting data to quickbook.
        public async Task<bool> ExportToQuickBooksAsync(string statusMessage, string requestText, int rowcount, string txnid)
        {
            bool reponseFlag = false;
            Contacts contactResponse = new Contacts();
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            //try
            //{
            //    DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ContactsXeroEntry>.xeroSaveXML(this, fileName);
            //}
            //catch
            //{
            //    statusMessage += "\n ";
            //    statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
            //    return false;
            //}

            //System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            //requestXmlDoc.Load(fileName);

            //string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            //System.IO.File.Delete(fileName);

            DataProcessingBlocks.ContactsXeroEntry Contact = new ContactsXeroEntry();
            Contact xeroContact = new Contact();

            if (ContactID != null)
            {
                xeroContact.ContactID = ContactID;
            }

            try
            {
                if (ContactStatus != string.Empty && ContactStatus != null)
                {
                    //int contactStatusEnum = (int)((Contact.ContactStatusEnum)Enum.Parse(typeof(Contact.ContactStatusEnum), ContactStatus));
                    bool checkFlag = Enum.IsDefined(typeof(Contact.ContactStatusEnum), ContactStatus);
                    if (checkFlag)
                    {
                        xeroContact.ContactStatus = (Contact.ContactStatusEnum)Enum.Parse(typeof(Contact.ContactStatusEnum), ContactStatus);
                    }
                }
            }
            catch(Exception ex)
            {
            }

            if (Name != string.Empty)
            {
                xeroContact.Name = Name;
            }

            if (FirstName != string.Empty)
            {
                xeroContact.FirstName = FirstName;
            }

            if (LastName != string.Empty)
            {
                xeroContact.LastName = LastName;
            }
            if (EmailAddress != string.Empty)
            {
                xeroContact.EmailAddress = EmailAddress;
            }
            if (SkypeUserName != string.Empty)
            {
                xeroContact.SkypeUserName = SkypeUserName;
            }
            if (BankAccountDetails != string.Empty)
            {
                xeroContact.BankAccountDetails = BankAccountDetails;
            }
            if (TaxNumber != string.Empty)
            {
                xeroContact.TaxNumber = TaxNumber;
            }
            if (AccountsReceivableTaxType != string.Empty)
            {
                xeroContact.AccountsReceivableTaxType = AccountsReceivableTaxType;
            }
            if (AccountsPayableTaxType != string.Empty)
            {
                xeroContact.AccountsPayableTaxType = AccountsPayableTaxType;
            }

            Xero.NetStandard.OAuth2.Model.Address add = new Xero.NetStandard.OAuth2.Model.Address();
            
            foreach (QuickBookEntities.Addresses additem in Addresses)
            {
                if (additem.AddressType != string.Empty && additem.AddressType != null)
                {
                    bool checkFlag = Enum.IsDefined(typeof(Xero.NetStandard.OAuth2.Model.Address.AddressTypeEnum), additem.AddressType);
                    if (checkFlag)
                    {
                        add.AddressType = (Xero.NetStandard.OAuth2.Model.Address.AddressTypeEnum)Enum.Parse(typeof(Xero.NetStandard.OAuth2.Model.Address.AddressTypeEnum), additem.AddressType);
                    }
                }

                add.AddressLine1 = additem.Addr1;
                add.AddressLine2 = additem.Addr2;
                add.AddressLine3 = additem.Addr3;
                add.AddressLine4 = additem.Addr4;
                add.AttentionTo = additem.AttentionTo;
                add.City = additem.City;
                add.Region = additem.Region;
                add.PostalCode = additem.PostalCode;
                add.Country = additem.Country;
                add.AttentionTo = additem.AttentionTo;
            }

            xeroContact.Addresses = new List<Xero.NetStandard.OAuth2.Model.Address>();
            xeroContact.Addresses.Add(add);
            
            Phone phn = new Phone();
            foreach (QuickBookEntities.Phones additem in Phones)
            {
                if (additem.PhoneType != string.Empty && additem.PhoneType != null)
                {
                    bool checkFlag = Enum.IsDefined(typeof(Phone.PhoneTypeEnum), additem.PhoneType);
                    if (checkFlag)
                    {
                        phn.PhoneType = (Phone.PhoneTypeEnum)Enum.Parse(typeof(Phone.PhoneTypeEnum), additem.PhoneType);
                    }
                }

                phn.PhoneNumber = additem.PhoneNumber;
                phn.PhoneAreaCode = additem.PhoneAreaCode;
                phn.PhoneCountryCode = additem.PhoneCountryCode;
            }

            xeroContact.Phones = new List<Phone>();
            xeroContact.Phones.Add(phn);

            if (IsSupplier != null)
            {
                xeroContact.IsSupplier = IsSupplier;
            }

            if (IsCustomer != null)
            {
                xeroContact.IsCustomer = IsCustomer;
            }

            if (DefaultCurrency != string.Empty && DefaultCurrency != null)
            {
                bool checkFlag = Enum.IsDefined(typeof(CurrencyCode), DefaultCurrency);
                if (checkFlag)
                {
                    xeroContact.DefaultCurrency = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), DefaultCurrency);
                }
            }

            Contacts xeroContacts = new Contacts();
            xeroContacts._Contacts = new List<Contact>();
            xeroContacts._Contacts.Add(xeroContact);

            string resp = string.Empty;
            string responseFile = string.Empty;

            try
            {
                //requestText = ModelSerializer.Serialize(xeroContact);
                //requestXmlDoc.LoadXml(requestText);
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);

                XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                var res = new AccountingApi();
                CommonUtilities.GetInstance().Type = "Contact";
                if (TransactionImporter.TransactionImporter.rdbuttonoverwrite == false)
                {
                   var response = res.CreateContactAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, xeroContact);
                    response.Wait();
                    contactResponse = response.Result;
                    if (contactResponse._Contacts.Count > 0)
                    {
                        txnid = contactResponse._Contacts[0].ContactID.ToString();
                        reponseFlag = true;
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += contactResponse._Contacts[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
                else if(TransactionImporter.TransactionImporter.rdbuttonoverwrite == true)
                {
                    if (TransactionImporter.TransactionImporter.refnoflag == true)
                    {
                        var response =  res.UpdateContactAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, CommonUtilities.GetInstance().existTxnId, xeroContacts);
                        response.Wait();
                        reponseFlag = true;
                    }
                    else
                    {
                        var response =  res.CreateContactAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID, xeroContact);
                        response.Wait();
                        contactResponse = response.Result;
                        reponseFlag = true;
                    }
                    if (contactResponse._Contacts != null)
                    {
                        txnid = contactResponse._Contacts[0].ContactID.ToString();
                    }
                    else
                    {
                        statusMessage += "\n ";
                        statusMessage += contactResponse._Contacts[0].ValidationErrors;
                        statusMessage += "\n ";
                        reponseFlag = false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                //CommonUtilities.GetInstance().SaveResponseFile(contactResponse.ToString(), responseFile);
            }
           
            //TransactionImporter.TransactionImporter.testFlag = true;
           
            CommonUtilities.GetInstance().xeroMessage = statusMessage;
            CommonUtilities.GetInstance().xeroTxnId = txnid;
            return reponseFlag;
        }

        #endregion
    }

    public class ContactsXeroEntryCollection : Collection<ContactsXeroEntry>
    {
    }

    public enum ContactStatus
    {
        ACTIVE,
        DELETED,
    }
    public enum AddressType
    {
        POBOX,
        STREET,
    }
    public enum PhoneType
    {
        DEFAULT,
        DDI,
        MOBILE,
        FAX,
    }
}

