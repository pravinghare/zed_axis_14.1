using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;


// ==============================================================================================
// 
// StatementChargesQBEntry.cs
//
// This file contains the implementations of the Statement Charges Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Statement Charges Entry Imports.
//        Statement Charges Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 

// ==============================================================================================





namespace DataProcessingBlocks
{
    [XmlRootAttribute("StatementChargesQBEntry", Namespace = "", IsNullable = false)]
   public class StatementChargesQBEntry
    {
        #region  Private Member Variable

        private CustomerRef m_CustomerRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private ItemRef m_ItemRef;
        private InventorySiteRef m_InventorySiteRef;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Rate;
        private string m_Amount;
        private string m_Desc;
        private ARAccountRef m_ARAccountRef;
        private ClassRef m_ClassRef;
        private string m_BilledDate;
        private string m_DueDate;
        private OverrideItemAccountRef m_OverrideItemAccountRef;
        private DateTime m_EstimateDate;
        //private string m_ExternalGUID;
        //private string m_IncludeRetElement;

        #endregion

        #region Constructors

        public StatementChargesQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

       [XmlElement(DataType = "string")]
       public string TxnDate
       {
           get
           {
               try
               {

                   if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                   {
                       return null;
                   }
                   else
                       return Convert.ToString(this.m_TxnDate);
               }
               catch
               {
                   return null;
               }
           }
           set
           {
               this.m_TxnDate = value;
           }
       }

       public string RefNumber
       {
           get { return m_RefNumber; }
           set { m_RefNumber = value; }
       }

        public ItemRef ItemRef
        {
            get { return m_ItemRef; }
            set { m_ItemRef = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return m_InventorySiteRef; }
            set { m_InventorySiteRef = value; }
        }
        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        public string Rate
        {
            get { return this.m_Rate; }
            set { this.m_Rate = value; }
        }
        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }
        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public ARAccountRef ARAccountRef
        {
            get { return m_ARAccountRef; }
            set { m_ARAccountRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }


       [XmlElement(DataType = "string")]
        public string BilledDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_BilledDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_BilledDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_BilledDate = value;
            }
        }
       [XmlElement(DataType = "string")]
        public string DueDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }
        }
        
       public OverrideItemAccountRef OverrideItemAccountRef
       {
           get { return m_OverrideItemAccountRef; }
           set { m_OverrideItemAccountRef = value; }
       }
       
        
        [XmlIgnoreAttribute()]
       public DateTime EstimateDate
       {
           get
           {
               try
               {
                   if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                       return this.m_EstimateDate;
                   else

                       return this.m_EstimateDate;
               }
               catch
               {
                   return this.m_EstimateDate;
               }
           }
           set { this.m_EstimateDate = value; }
       }
       

        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.StatementChargesQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);

            }
            catch (Exception ex)
            {
                // throw;
            }


            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement ChargeAddRq = requestXmlDoc.CreateElement("ChargeAddRq");
            inner.AppendChild(ChargeAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement ChargeAdd = requestXmlDoc.CreateElement("ChargeAdd");

            ChargeAddRq.AppendChild(ChargeAdd);
            //requestXML = requestXML.Replace("<EstimateLineAddREM />", string.Empty);
            //requestXML = requestXML.Replace("<EstimateLineAddREM>", string.Empty);
            //requestXML = requestXML.Replace("</EstimateLineAddREM>", string.Empty);
            //requestXML = requestXML.Replace("<EstimateLineAdd />", string.Empty);

            //requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            //requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            //requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            //requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            //requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            //requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            ChargeAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd"))
            //{
            //    if (oNode.SelectSingleNode("Phone") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd/Phone");
            //        ChargeAdd.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd"))
            //{
            //    if (oNode.SelectSingleNode("Fax") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd/Fax");
            //        ChargeAdd.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd"))
            //{
            //    if (oNode.SelectSingleNode("Email") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd/Email");
            //        ChargeAdd.RemoveChild(childNode);
            //    }
            //}
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/ChargeAddRq/ChargeAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                ChargeAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                ChargeAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ChargeAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ChargeAddRs/ChargeRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCharge(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Charge ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="EstimateRef No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public Hashtable CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            Hashtable ChargeTable = new Hashtable();

            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateQueryRq aggregate and fill in field values for it
            XmlElement ChargeQueryRq = requestXmlDoc.CreateElement("ChargeQueryRq");
            inner.AppendChild(ChargeQueryRq);

            //Create Refno aggregate and fill in field values for it
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            ChargeQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            ChargeQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return ChargeTable;
            }

            if (resp == string.Empty)
            {
                return ChargeTable;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no EstimateTable exists.
                    return ChargeTable;

                }
                else
                    if (resp.Contains("statusSeverity=\"Warn\""))
                    {
                        //Returning means there is no EstimateTable exists.
                        return ChargeTable;
                    }
                    else
                    {
                        //Getting listid and Edit Sequence details of EstimateTable.
                        string listID = string.Empty;
                        string editSequence = string.Empty;
                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ChargeQueryRs/ChargeRet"))
                        {
                            //Get ListID of ChargeTable.
                            if (oNode.SelectSingleNode("ListID") != null)
                            {
                                listID = oNode.SelectSingleNode("ListID").InnerText.ToString();
                            }
                            //Get Edit Sequence of EstimateTable.
                            if (oNode.SelectSingleNode("EditSequence") != null)
                            {
                                editSequence = oNode.SelectSingleNode("EditSequence").InnerText.ToString();
                            }
                        }
                        if (listID != string.Empty && editSequence != string.Empty)
                        {
                            ChargeTable.Add(listID, editSequence);
                        }
                        return ChargeTable;
                    }
            }
        }

        /// <summary>
        /// This method is used for updating EstimateQBEntry information
        /// of existing EstimateQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateChargeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.StatementChargesQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create CheckModRq aggregate and fill in field values for it
            System.Xml.XmlElement ChargeQBEntryModRq = requestXmlDoc.CreateElement("ChargeModRq");
            inner.AppendChild(ChargeQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement ChargeMod = requestXmlDoc.CreateElement("ChargeMod");
            ChargeQBEntryModRq.AppendChild(ChargeMod);

            //requestXML = requestXML.Replace("<EstimatePerItemREM>", string.Empty);
            //requestXML = requestXML.Replace("</EstimatePerItemREM>", string.Empty);
            //requestXML = requestXML.Replace("<EstimateLineAddREM />", string.Empty);
            //requestXML = requestXML.Replace("<EstimateLineAddREM>", string.Empty);
            //requestXML = requestXML.Replace("</EstimateLineAddREM>", string.Empty);

            //requestXML = requestXML.Replace("<EstimateLineAdd />", string.Empty);

            //requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            //requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            //requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            //requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            //requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            //requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            //requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            //requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            //requestXML = requestXML.Replace("<EstimateLineAdd>", "<EstimateLineMod>");
            //requestXML = requestXML.Replace("</EstimateLineAdd>", "</EstimateLineMod>");
            //requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            //requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            ChargeMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/ChargeModRq/ChargeMod"))
            {
                //if (oNode.SelectSingleNode("Phone") != null)
                //{
                //    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeModRq/ChargeMod/Phone");
                //    EstimateMod.RemoveChild(childNode);
                //}
            }
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/EstimateModRq/EstimateMod"))
            //{
            //if (oNode.SelectSingleNode("Fax") != null)
            //{
            //    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EstimateModRq/EstimateMod/Fax");
            //    EstimateMod.RemoveChild(childNode);
            //}
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/EstimateModRq/EstimateMod"))
            //{
            //if (oNode.SelectSingleNode("Email") != null)
            //{
            //    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/EstimateModRq/EstimateMod/Email");
            //    EstimateMod.RemoveChild(childNode);
            //}
            //}
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/ChargeModRq/ChargeMod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                ChargeQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By EstimateRefNumber) : " + rowcount.ToString());
            else
                ChargeQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeModRq/ChargeMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeModRq/ChargeMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            //System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            // requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ChargeModRq/ChargeMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ChargeModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ChargeModRs/ChargeRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCharge(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }


    public class ChargeQBEntryCollection : Collection<StatementChargesQBEntry>
    {
        /// <summary>
        ///   /// This method is used for getting existing charge date, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public StatementChargesQBEntry FindChargeEntry(DateTime date)
        {
            foreach (StatementChargesQBEntry item in this)
            {
                if (item.EstimateDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
       /// <summary>
        ///   /// This method is used for getting existing charge refno, If not exists then it return null.
       /// </summary>
       /// <param name="refNumber"></param>
       /// <returns></returns>
        public StatementChargesQBEntry FindChargeEntry(string refNumber)
        {
            foreach (StatementChargesQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }
       /// <summary>
        ///   /// This method is used for getting existing charge date and refno, If not exists then it return null.
       /// </summary>
       /// <param name="date"></param>
       /// <param name="refNumber"></param>
       /// <returns></returns>
        public StatementChargesQBEntry FindChargeEntry(DateTime date, string refNumber)
        {
            foreach (StatementChargesQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.EstimateDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }






    }
