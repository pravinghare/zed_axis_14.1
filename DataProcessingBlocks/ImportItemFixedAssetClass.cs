using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemFixedAssetClass
    {
        private static ImportItemFixedAssetClass m_ImportItemFixedAssetClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemFixedAssetClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemFixedAsset class
        /// </summary>
        /// <returns></returns>
        public static ImportItemFixedAssetClass GetInstance()
        {
            if (m_ImportItemFixedAssetClass == null)
                m_ImportItemFixedAssetClass = new ImportItemFixedAssetClass();
            return m_ImportItemFixedAssetClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemFixedAsset and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemFixedAsset QuickBooks collection </returns>
        public DataProcessingBlocks.ItemFixedAssetQBEntryCollection ImportItemFixedAssetData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemFixedAsset Entry collections.
            DataProcessingBlocks.ItemFixedAssetQBEntryCollection coll = new ItemFixedAssetQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemFixedAsset Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemFixedAssetDt = new DateTime();
                    string datevalue = string.Empty;

                    //ItemFixedAsset Validation
                    DataProcessingBlocks.ItemFixedAssetQBEntry ItemFixedAsset = new ItemFixedAssetQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.Name = dr["Name"].ToString().Substring(0, 31);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BarCodeValue"))
                    {
                        #region Validations of BarCodeValue
                        if (dr["BarCodeValue"].ToString() != string.Empty)
                        {
                            if (dr["BarCodeValue"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BarCodeValue (" + dr["BarCodeValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemFixedAsset.BarCode.BarCodeValue == null)
                                            ItemFixedAsset = null;
                                        else
                                            ItemFixedAsset.BarCode = new BarCode(dr["BarCodeValue"].ToString().Substring(0, 50));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemFixedAsset.BarCode.BarCodeValue == null)
                                            ItemFixedAsset = null;
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                }
                            }
                            else
                            {
                                ItemFixedAsset.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                ItemFixedAsset.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    ItemFixedAsset.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        ItemFixedAsset.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemFixedAsset.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemFixedAsset.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemFixedAsset.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 651
                    if (dt.Columns.Contains("ClassRefFullName"))
                    {
                        #region Validations of Class FullName
                        if (dr["ClassRefFullName"].ToString() != string.Empty)
                        {
                            if (dr["ClassRefFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class FullName (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemFixedAsset.ClassRef.FullName == null)
                                            ItemFixedAsset.ClassRef.FullName = null;
                                        else
                                            ItemFixedAsset.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemFixedAsset.ClassRef.FullName == null)
                                            ItemFixedAsset.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                    if (ItemFixedAsset.ClassRef.FullName == null)
                                        ItemFixedAsset.ClassRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemFixedAsset.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                if (ItemFixedAsset.ClassRef.FullName == null)
                                    ItemFixedAsset.ClassRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AcquiredAs"))
                    {
                        #region Validations of AcquiredAs
                        if (dr["AcquiredAs"].ToString() != string.Empty)
                        {
                            try
                            {
                                ItemFixedAsset.AcquiredAs = Convert.ToString((DataProcessingBlocks.AcquiredAs)Enum.Parse(typeof(DataProcessingBlocks.AcquiredAs), dr["AcquiredAs"].ToString(), true));
                            }
                            catch
                            {
                                ItemFixedAsset.AcquiredAs = dr["AcquiredAs"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PurchaseDesc"))
                    {
                        #region Validations of PurchaseDesc
                        if (dr["PurchaseDesc"].ToString() != string.Empty)
                        {
                            if (dr["PurchaseDesc"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PurchaseDesc (" + dr["PurchaseDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.PurchaseDesc = dr["PurchaseDesc"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.PurchaseDesc = dr["PurchaseDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PurchaseDate"))
                    {
                        #region validations of PurchaseDate
                        if (dr["PurchaseDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["PurchaseDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out ItemFixedAssetDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PurchaseDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemFixedAsset.PurchaseDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemFixedAsset.PurchaseDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        ItemFixedAsset.PurchaseDate = datevalue;
                                    }
                                }
                                else
                                {
                                    ItemFixedAssetDt = dttest;
                                    ItemFixedAsset.PurchaseDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                ItemFixedAssetDt = Convert.ToDateTime(datevalue);
                                ItemFixedAsset.PurchaseDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PurchaseCost"))
                    {
                        #region Validations for PurchaseCost
                        if (dr["PurchaseCost"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["PurchaseCost"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Purchase Cost ( " + dr["PurchaseCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.PurchaseCost = dr["PurchaseCost"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.PurchaseCost = dr["PurchaseCost"].ToString();
                                    }
                                }
                                else
                                    ItemFixedAsset.PurchaseCost = dr["PurchaseCost"].ToString();
                            }
                            else
                            {
                                ItemFixedAsset.PurchaseCost = string.Format("{0:00000000.00}", Convert.ToDouble(dr["PurchaseCost"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("VendorOrPayeeName"))
                    {
                        #region Validations of VendorOrPayeeName
                        if (dr["VendorOrPayeeName"].ToString() != string.Empty)
                        {
                            if (dr["VendorOrPayeeName"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This VendorOrPayeeName (" + dr["VendorOrPayeeName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.VendorOrPayeeName = dr["VendorOrPayeeName"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.VendorOrPayeeName = dr["VendorOrPayeeName"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.VendorOrPayeeName = dr["VendorOrPayeeName"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.VendorOrPayeeName = dr["VendorOrPayeeName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AssetAccountFullName"))
                    {
                        #region Validations of AssetAccount FullName
                        if (dr["AssetAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["AssetAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AssetAccount FullName (" + dr["AssetAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                        if (ItemFixedAsset.AssetAccountRef.FullName == null)
                                            ItemFixedAsset.AssetAccountRef.FullName = null;
                                        else
                                            ItemFixedAsset.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                        if (ItemFixedAsset.AssetAccountRef.FullName == null)
                                            ItemFixedAsset.AssetAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                    if (ItemFixedAsset.AssetAccountRef.FullName == null)
                                        ItemFixedAsset.AssetAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemFixedAsset.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                if (ItemFixedAsset.AssetAccountRef.FullName == null)
                                    ItemFixedAsset.AssetAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    QuickBookEntities.FixedAssetSalesInfo fixedAssetSalesInfoItem = new FixedAssetSalesInfo();

                    if (dt.Columns.Contains("FixedAssetSalesInfoSalesDesc"))
                    {
                        #region Validations of FixedAssetSalesInfo SalesDesc
                        if (dr["FixedAssetSalesInfoSalesDesc"].ToString() != string.Empty)
                        {
                            if (dr["FixedAssetSalesInfoSalesDesc"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FixedAssetSalesInfo SalesDesc (" + dr["FixedAssetSalesInfoSalesDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        fixedAssetSalesInfoItem.SalesDesc = dr["FixedAssetSalesInfoSalesDesc"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        fixedAssetSalesInfoItem.SalesDesc = dr["FixedAssetSalesInfoSalesDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    fixedAssetSalesInfoItem.SalesDesc = dr["FixedAssetSalesInfoSalesDesc"].ToString();
                                }
                            }
                            else
                            {
                                fixedAssetSalesInfoItem.SalesDesc = dr["FixedAssetSalesInfoSalesDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FixedAssetSalesInfoSalesDate"))
                    {
                        #region validations of FixedAssetSalesInfo SalesDate
                        if (dr["FixedAssetSalesInfoSalesDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["FixedAssetSalesInfoSalesDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out ItemFixedAssetDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FixedAssetSalesInfo SalesDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            fixedAssetSalesInfoItem.SalesDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            fixedAssetSalesInfoItem.SalesDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        fixedAssetSalesInfoItem.SalesDate = datevalue;
                                    }
                                }
                                else
                                {
                                    ItemFixedAssetDt = dttest;
                                    fixedAssetSalesInfoItem.SalesDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                ItemFixedAssetDt = Convert.ToDateTime(datevalue);
                                fixedAssetSalesInfoItem.SalesDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FixedAssetSalesInfoSalesPrice"))
                    {
                        #region Validations for FixedAssetSalesInfo SalesPrice
                        if (dr["FixedAssetSalesInfoSalesPrice"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["FixedAssetSalesInfoSalesPrice"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FixedAssetSalesInfo SalesPrice ( " + dr["FixedAssetSalesInfoSalesPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        fixedAssetSalesInfoItem.SalesPrice = dr["FixedAssetSalesInfoSalesPrice"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        fixedAssetSalesInfoItem.SalesPrice = dr["FixedAssetSalesInfoSalesPrice"].ToString();
                                    }
                                }
                                else
                                    fixedAssetSalesInfoItem.SalesPrice = dr["FixedAssetSalesInfoSalesPrice"].ToString();
                            }
                            else
                            {
                                fixedAssetSalesInfoItem.SalesPrice = string.Format("{0:00000000.00}", Convert.ToDouble(dr["FixedAssetSalesInfoSalesPrice"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("FixedAssetSalesInfoSalesExpense"))
                    {
                        #region Validations for FixedAssetSalesInfo SalesExpense
                        if (dr["FixedAssetSalesInfoSalesExpense"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["FixedAssetSalesInfoSalesExpense"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FixedAssetSalesInfo SalesExpense ( " + dr["FixedAssetSalesInfoSalesExpense"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        fixedAssetSalesInfoItem.SalesExpense = dr["FixedAssetSalesInfoSalesExpense"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        fixedAssetSalesInfoItem.SalesExpense = dr["FixedAssetSalesInfoSalesExpense"].ToString();
                                    }
                                }
                                else
                                    fixedAssetSalesInfoItem.SalesExpense = dr["FixedAssetSalesInfoSalesExpense"].ToString();
                            }
                            else
                            {
                                fixedAssetSalesInfoItem.SalesExpense = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["FixedAssetSalesInfoSalesExpense"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (fixedAssetSalesInfoItem.SalesDate != null || fixedAssetSalesInfoItem.SalesDesc != null || fixedAssetSalesInfoItem.SalesExpense != null || fixedAssetSalesInfoItem.SalesPrice != null)
                        ItemFixedAsset.FixedAssetSalesInfo.Add(fixedAssetSalesInfoItem);


                    if (dt.Columns.Contains("AssetDesc"))
                    {
                        #region Validations of AssetDesc
                        if (dr["AssetDesc"].ToString() != string.Empty)
                        {
                            if (dr["AssetDesc"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AssetDesc (" + dr["AssetDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.AssetDesc = dr["AssetDesc"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.AssetDesc = dr["AssetDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.AssetDesc = dr["AssetDesc"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.AssetDesc = dr["AssetDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Location"))
                    {
                        #region Validations of Location
                        if (dr["Location"].ToString() != string.Empty)
                        {
                            if (dr["Location"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Location (" + dr["Location"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.Location = dr["Location"].ToString().Substring(0, 50);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.Location = dr["Location"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.Location = dr["Location"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.Location = dr["Location"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("PONumber"))
                    {
                        #region Validations of PONumber
                        if (dr["PONumber"].ToString() != string.Empty)
                        {
                            if (dr["PONumber"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PONumber (" + dr["PONumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.PONumber = dr["PONumber"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.PONumber = dr["PONumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.PONumber = dr["PONumber"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.PONumber = dr["PONumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SerialNumber"))
                    {
                        #region Validations of SerialNumber
                        if (dr["SerialNumber"].ToString() != string.Empty)
                        {
                            if (dr["SerialNumber"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 30);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.SerialNumber = dr["SerialNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("WarrantyExpDate"))
                    {
                        #region validations of WarrantyExpDate
                        if (dr["WarrantyExpDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["WarrantyExpDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out ItemFixedAssetDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This WarrantyExp Date (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemFixedAsset.WarrantyExpDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemFixedAsset.WarrantyExpDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        ItemFixedAsset.WarrantyExpDate = datevalue;
                                    }
                                }
                                else
                                {
                                    ItemFixedAssetDt = dttest;
                                    ItemFixedAsset.WarrantyExpDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                ItemFixedAssetDt = Convert.ToDateTime(datevalue);
                                ItemFixedAsset.WarrantyExpDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["Notes"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.Notes = dr["Notes"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.Notes = dr["Notes"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("AssetNumber"))
                    {
                        #region Validations of AssetNumber
                        if (dr["AssetNumber"].ToString() != string.Empty)
                        {
                            if (dr["AssetNumber"].ToString().Length > 10)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AssetNumber (" + dr["AssetNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.AssetNumber = dr["AssetNumber"].ToString().Substring(0, 10);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.AssetNumber = dr["AssetNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemFixedAsset.AssetNumber = dr["AssetNumber"].ToString();
                                }
                            }
                            else
                            {
                                ItemFixedAsset.AssetNumber = dr["AssetNumber"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("CostBasis"))
                    {
                        #region Validations for CostBasis
                        if (dr["CostBasis"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["CostBasis"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CostBasis ( " + dr["CostBasis"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.CostBasis = dr["CostBasis"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.CostBasis = dr["CostBasis"].ToString();
                                    }
                                }
                                else
                                    ItemFixedAsset.CostBasis = dr["CostBasis"].ToString();
                            }
                            else
                            {
                                ItemFixedAsset.CostBasis = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["CostBasis"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("YearEndAccumulatedDepreciation"))
                    {
                        #region Validations for YearEndAccumulatedDepreciation
                        if (dr["YearEndAccumulatedDepreciation"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["YearEndAccumulatedDepreciation"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This YearEndAccumulatedDepreciation ( " + dr["YearEndAccumulatedDepreciation"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.YearEndAccumulatedDepreciation = dr["YearEndAccumulatedDepreciation"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.YearEndAccumulatedDepreciation = dr["YearEndAccumulatedDepreciation"].ToString();
                                    }
                                }
                                else
                                    ItemFixedAsset.YearEndAccumulatedDepreciation = dr["YearEndAccumulatedDepreciation"].ToString();
                            }
                            else
                            {

                                ItemFixedAsset.YearEndAccumulatedDepreciation = string.Format("{0:00000000.00}", Convert.ToDouble(dr["YearEndAccumulatedDepreciation"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("YearEndBookValue"))
                    {
                        #region Validations for YearEndBookValue
                        if (dr["YearEndBookValue"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["YearEndBookValue"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This YearEndBook Value ( " + dr["YearEndBookValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemFixedAsset.YearEndBookValue = dr["YearEndBookValue"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemFixedAsset.YearEndBookValue = dr["YearEndBookValue"].ToString();
                                    }
                                }
                                else
                                    ItemFixedAsset.YearEndBookValue = dr["YearEndBookValue"].ToString();
                            }
                            else
                            {
                                ItemFixedAsset.YearEndBookValue = string.Format("{0:00000000.00}", Convert.ToDouble(dr["YearEndBookValue"].ToString()));
                            }
                        }

                        #endregion
                    }

                    coll.Add(ItemFixedAsset);

                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }

    }
}
