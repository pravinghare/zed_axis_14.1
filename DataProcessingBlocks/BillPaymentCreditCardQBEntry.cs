// ==============================================================================================
// 
// BillPaymentCreditCardQbEntry.cs
//
// This file contains the implementations of the BillPayment Credit Card Entry private members , 
// Properties, Constructors and Methods for QuickBooks Bill Payment Credit card Imports.
//         Bill Payment Credit card Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping field (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("BillPaymentCreditCardQBEntry", Namespace = "", IsNullable = false)]
    public class BillPaymentCreditCardQBEntry
    {
        #region  Private Member Variable

        private PayeeEntityRef m_PayeeEntityRef;
        private APAccountRef m_APAccountRef;
        private string m_TxnDate;
        private CreditCardAccountRef m_CreditCardAccountRef;
        private string m_RefNumber;
        private string m_Memo;
        private string m_ExchangeRate;
        private Collection<AppliedToTxnAdd> m_AppliedToTxnAdd = new Collection<AppliedToTxnAdd>();
        private DateTime m_BillDate;

        #endregion

        #region Construtor

        public BillPaymentCreditCardQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public PayeeEntityRef PayeeEntityRef
        {
            get { return m_PayeeEntityRef; }
            set { this.m_PayeeEntityRef = value; }
        }
        public APAccountRef APAccountRef
        {
            get { return m_APAccountRef; }
            set { m_APAccountRef = value; }
        }   

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }
        public CreditCardAccountRef CreditCardAccountRef
        {
            get { return m_CreditCardAccountRef; }
            set { m_CreditCardAccountRef = value; }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        [XmlArray("AppliedToTxnAddREM")]
        public Collection<AppliedToTxnAdd> AppliedToTxnAdd
        {
            get { return m_AppliedToTxnAdd; }
            set { m_AppliedToTxnAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime BillDate
        {
            get { return m_BillDate; }
            set { m_BillDate = value; }
        }

        #endregion

        #region Public Methods

        /// This method is used to creating xml request for  export data to quickbook
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillPaymentCreditCardQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }


            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create DepositEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement BillPaymentCreditCardAddRq = requestXmlDoc.CreateElement("BillPaymentCreditCardAddRq");
            inner.AppendChild(BillPaymentCreditCardAddRq);

            //Create DepositEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement BillPaymentCreditCardAdd = requestXmlDoc.CreateElement("BillPaymentCreditCardAdd");

            BillPaymentCreditCardAddRq.AppendChild(BillPaymentCreditCardAdd);

            requestXML = requestXML.Replace("<AppliedToTxnAddREM />", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("</AppliedToTxnAddREM>", string.Empty);

            requestXML = requestXML.Replace("<SetCreditREM />", string.Empty);
            requestXML = requestXML.Replace("<SetCreditREM>", string.Empty);
            requestXML = requestXML.Replace("</SetCreditREM>", string.Empty);

            BillPaymentCreditCardAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillPaymentCreditCardAddRq/BillPaymentCreditCardAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                BillPaymentCreditCardAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                BillPaymentCreditCardAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());
            
            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
         
                try
                {
                    responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                    if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                    {
                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                    }
                    else

                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
                finally
                {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillPaymentCreditCardAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = oNode.Attributes["requestID"].Value.ToString();
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillPaymentCreditCardAddRs/BillPaymentCreditCardRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfBillPaymentCreditCard(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Bill Payment Credit Card ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Bill Payment Credit Card Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillPaymentCreditCardQueryRq aggregate and fill in field values for it
            XmlElement BillPaymentCreditCardQueryRq = requestXmlDoc.CreateElement("BillPaymentCreditCardQueryRq");
            inner.AppendChild(BillPaymentCreditCardQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            BillPaymentCreditCardQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            BillPaymentCreditCardQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
              try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                    
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                    
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating BillPaymentCreditCard information
        /// of existing BillPaymentCreditCard with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateBillPaymentCreditCardInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                } 
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillPaymentCreditCardQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement BillPaymentCreditCardQBEntryModRq = requestXmlDoc.CreateElement("BillPaymentCreditCardModRq");
            inner.AppendChild(BillPaymentCreditCardQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement BillPaymentCreditCardMod = requestXmlDoc.CreateElement("BillPaymentCreditCardMod");
            BillPaymentCreditCardQBEntryModRq.AppendChild(BillPaymentCreditCardMod);

            requestXML = requestXML.Replace("<BillPaymentCreditCardPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</BillPaymentCreditCardPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAddREM />", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("</AppliedToTxnAddREM>", string.Empty);

            requestXML = requestXML.Replace("<SetCreditREM />", string.Empty);
            requestXML = requestXML.Replace("<SetCreditREM>", string.Empty);
            requestXML = requestXML.Replace("</SetCreditREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<BillPaymentCreditCardLineAdd>", "<BillPaymentCreditCardLineMod>");
            requestXML = requestXML.Replace("</BillPaymentCreditCardLineAdd>", "</BillPaymentCreditCardLineMod>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");

            BillPaymentCreditCardMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillPaymentCreditCardAddRq/BillPaymentCreditCardAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                BillPaymentCreditCardQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By BillPaymentCreditCardRefNumber) : " + rowcount.ToString());
            else
                BillPaymentCreditCardQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;
            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillPaymentCreditCardModRq/BillPaymentCreditCardMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillPaymentCreditCardModRq/BillPaymentCreditCardMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillPaymentCreditCardModRq/BillPaymentCreditCardMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                    
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                    
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillPaymentCreditCardModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillPaymentCreditCardModRs/BillPaymentCreditCardRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfBillPaymentCreditCard(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
 
        #endregion

    }

    public class BillPaymentCreditCardQBEntryCollection : Collection<BillPaymentCreditCardQBEntry>
    {
        /// <summary>
        ///  This method is used for getting existing bill payment credit card date, If not exists then it return null.
        /// </summary>
        public BillPaymentCreditCardQBEntry FindBillPaymentCreditCardQBEntry(DateTime date)
        {
            foreach (BillPaymentCreditCardQBEntry item in this)
            {
                if (item.BillDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        ///  This method is used for getting existing bill payment credit card refNumber, If not exists then it return null.
        /// </summary>
        public BillPaymentCreditCardQBEntry FindBillPaymentCreditCardQBEntry(string refNumber)
        {
            foreach (BillPaymentCreditCardQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        ///  This method is used for getting existing bill payment credit card date and refnumber, If not exists then it return null.
        /// </summary>
        public BillPaymentCreditCardQBEntry FindBillPaymentCreditCardQBEntry(DateTime date, string refNumber)
        {
            foreach (BillPaymentCreditCardQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.BillDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }
            
}
