// ==============================================================================================
// 
// SalesOrderQbEntry.cs
//
// This file contains the implementations of the Sales Order Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Sales Order QB Entry Imports.
//         Sales Order Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping fields (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks
{
    
    [XmlRootAttribute("SalesOrderQBEntry", Namespace = "", IsNullable = false)]
    public class SalesOrderQBEntry
    {
        #region  Private Member Variable
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private TemplateRef m_TemplateRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private TermsRef m_TermsRef;
        private string m_DueDate;
        private SalesRepRef m_SalesRepRef;
        private string m_PONumber;
        private string m_FOB;
        private string m_ShipDate;
        private ShipMethodRef m_ShipMethodRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_Memo;
        private CustomerMsgRef m_CustomerMsgRef;
        private string m_IsToBePrinted;
        private string m_IsToBeEmailed;
        private string m_IsTaxIncluded;
        private string m_IsManuallyClosed;
        private string m_Others;
        private string m_ExchangeRate;
        private CustomerSalesTaxCodeRef m_CustomerSalesTaxCodeRef;
        private Collection<BillAddress> m_BillAddress = new Collection<BillAddress>();
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        private Collection<SalesOrderLineAdd> m_SalesOrderLineAdd = new Collection<SalesOrderLineAdd>();
        private DateTime m_SalesOrderDate;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;
        //Axis 10.2 (Bug No -92)
        private Collection<SalesOrderLineGroupAdd> m_SalesOrderLineGroupAdd = new Collection<SalesOrderLineGroupAdd>();

        /// <summary>
        ///bug 442 11.4
        /// </summary>
        private CurrencyRef m_CurrencyRef;
        #endregion

        #region Construtor

        public SalesOrderQBEntry()
        {
           
        }

        #endregion

        #region Public Properties

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }
        /// <summary>
        /// 11.4  bug no 442
        /// </summary>
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public TemplateRef TemplateRef
        {
            get { return m_TemplateRef; }
            set { m_TemplateRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {

            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        [XmlArray("BillAddressREM")]
        public Collection<BillAddress> BillAddress
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }


        public string PONumber
        {
            get { return m_PONumber; }
            set { m_PONumber = value; }
        }
       
        public TermsRef TermsRef
        {
            get { return m_TermsRef; }
            set { m_TermsRef = value; }
        }

        [XmlElement(DataType = "string")]
        public String DueDate
        {
             get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }

            
        }
      
        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
       
        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        [XmlElement(DataType = "string")]
        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }

        }

        
        public ShipMethodRef ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }
      
        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return m_ItemSalesTaxRef; }
            set { m_ItemSalesTaxRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsManuallyClosed
        {
            get { return m_IsManuallyClosed; }
            set { m_IsManuallyClosed = value; }
        }
       
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }
              

        public CustomerMsgRef CustomerMsgRef
        {
            get { return m_CustomerMsgRef; }
            set { m_CustomerMsgRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public CustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get { return m_CustomerSalesTaxCodeRef; }
            set { m_CustomerSalesTaxCodeRef = value; }
        }

        //Axis bug no.#96
        public string Other
        {
            get { return m_Others; }
            set { m_Others = value; }
        }
        //End Axis bug no.#96

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }
               
        [XmlArray("SalesOrderLineAddREM")]
        public Collection<SalesOrderLineAdd> SalesOrderLineAdd
        {
            get { return m_SalesOrderLineAdd; }
            set { m_SalesOrderLineAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime SalesOrderDate
        {
            get { return m_SalesOrderDate; }
            set { m_SalesOrderDate = value; }
        }

          //Axis 10.2(bug no-92)
        [XmlArray("SalesOrderLineGroupAddREM")]
        public Collection<SalesOrderLineGroupAdd>SalesOrderLineGroupAdd
        {
            get { return m_SalesOrderLineGroupAdd; }
            set { m_SalesOrderLineGroupAdd = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.SalesOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesOrderEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesOrderAddRq = requestXmlDoc.CreateElement("SalesOrderAddRq");
            inner.AppendChild(SalesOrderAddRq);

            //Create SalesOrderEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement SalesOrderAdd = requestXmlDoc.CreateElement("SalesOrderAdd");


            SalesOrderAddRq.AppendChild(SalesOrderAdd);

            requestXML = requestXML.Replace("<SalesOrderLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAdd />", string.Empty);

              //Axis 10.2(Bug No -92)
            requestXML = requestXML.Replace("<SalesOrderLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineGroupAddREM>", string.Empty);
            //End Changes 

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);     
         
            SalesOrderAdd.InnerXml = requestXML;

            //For add request id to track error message
            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd/CurrencyRef");
                    SalesOrderAdd.RemoveChild(childNode);
                }
            }
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd/Phone");
                    SalesOrderAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd/Fax");
                    SalesOrderAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd/Email");
                    SalesOrderAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd/InventorySiteLocationRef");
                    SalesOrderAdd.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    SalesOrderAdd.RemoveAll();


                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesOrderAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                SalesOrderAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());
            

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesOrderAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesOrderAddRs/SalesOrderRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Sales Order ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Sales Order Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesOrderQueryRq   aggregate and fill in field values for it
            XmlElement SalesOrderQueryRq = requestXmlDoc.CreateElement("SalesOrderQueryRq");
            inner.AppendChild(SalesOrderQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            SalesOrderQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            SalesOrderQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating SalesOrder information
        /// of existing SalesOrder with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateSalesOrderInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.SalesOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesOrderQBEntryModRq = requestXmlDoc.CreateElement("SalesOrderModRq");
            inner.AppendChild(SalesOrderQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement SalesOrderMod = requestXmlDoc.CreateElement("SalesOrderMod");
            SalesOrderQBEntryModRq.AppendChild(SalesOrderMod);

            requestXML = requestXML.Replace("<SalesOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");

            requestXML = requestXML.Replace("<SalesOrderLineAdd>","<SalesOrderLineMod>");
            requestXML = requestXML.Replace("</SalesOrderLineAdd>","</SalesOrderLineMod>");
            requestXML = requestXML.Replace("<SalesOrderLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<SalesOrderLineGroupAdd>", "<SalesOrderLineGroupMod>");
            requestXML = requestXML.Replace("</SalesOrderLineGroupAdd>", "</SalesOrderLineGroupMod>");
            requestXML = requestXML.Replace("<SalesOrderLineGroupAdd />", string.Empty);

            //Axis 10.2(Bug No -92)
            requestXML = requestXML.Replace("<SalesOrderLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineGroupAddREM>", string.Empty);
            //End Changes 

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");
            SalesOrderMod.InnerXml = requestXML;

            //For add request id to track error message
            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/CurrencyRef");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }

            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/Phone");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/Fax");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/Email");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/InventorySiteLocationRef");
                    SalesOrderMod.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    SalesOrderMod.RemoveAll();


                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesOrderQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By SalesOrderRefNumber) : " + rowcount.ToString());
            else
                SalesOrderQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesOrderModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesOrderModRs/SalesOrderRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
//Bug No.412
        /// <summary>
        /// This method is used for appending data to the corresponding list id.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendSalesOrderInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence,List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.SalesOrderQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesOrderQBEntryModRq = requestXmlDoc.CreateElement("SalesOrderModRq");
            inner.AppendChild(SalesOrderQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement SalesOrderMod = requestXmlDoc.CreateElement("SalesOrderMod");
            SalesOrderQBEntryModRq.AppendChild(SalesOrderMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
            //Axis 706
            List<string> GroupTxnLineID = new List<string>();
            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                if (!txnLineIDList[i].Contains("GroupTxnLineID_"))
                {
                    addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></SalesOrderLineMod><SalesOrderLineMod>";


                    XmlNode SalesOrderLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod");
                    System.Xml.XmlElement SalesOrderGroupMod = requestXmlDoc.CreateElement("SalesOrderLineMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = txnLineIDList[i];
                    SalesOrderGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertAfter(SalesOrderGroupMod, SalesOrderLineMod.ChildNodes[i == 0 ? i : i - 1]);
                }
                else
                {
                    string GroupTxnID = txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty);
                    GroupTxnLineID.Add(txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty));
                    XmlNode SalesOrderMod1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod");
                    //XmlNode InvoiceLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod");
                    System.Xml.XmlElement SalesOrderLineGroupMod = requestXmlDoc.CreateElement("SalesOrderLineGroupMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = GroupTxnID;
                    SalesOrderLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertAfter(SalesOrderLineGroupMod, SalesOrderMod1.ChildNodes[i == 0 ? i : i - 1]);
                }
            }

            XmlNode SalesOrderMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod");

            System.Xml.XmlElement SalesOrderAdd = requestXmlDoc.CreateElement("SalesOrderLine");
            //for (int i = 0; i < txnLineIDList.Count; i++)
            //{
            //    addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></SalesOrderLineMod><SalesOrderLineMod>";
            //}
            //for (int i = 0; i < request.Length; i++)
            //{
            //    if (Listcnt != 0)
            //    {
            //        if (subStringCnt == 1)
            //        {
            //            //subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></SalesOrderLineMod><SalesOrderLineMod><TxnLineID>-1</TxnLineID><ItemRef>");
            //            subResultString = request[i].Replace("<ItemRef>", addString + "<TxnLineID>-1</TxnLineID><ItemRef>");
            //            subStringCnt++;
            //        }
            //        else
            //        {
            //            subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            //        }
            //    }
            //    else
            //    {
            //        subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            //    }

            //    if (stringCnt == request.Length)
            //    {
            //        resultString += subResultString;
            //    }
            //    else
            //    {
            //        resultString += subResultString + "</ItemRef>";
            //    }
            //    stringCnt++;
            //    Listcnt--;
            //}
            //requestXML = resultString;

            //Axis 706 ends

            requestXML = requestXML.Replace("<SalesOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");

            requestXML = requestXML.Replace("<SalesOrderLineAdd>", "<SalesOrderLineMod>");
            requestXML = requestXML.Replace("</SalesOrderLineAdd>", "</SalesOrderLineMod>");

            requestXML = requestXML.Replace("<SalesOrderLineAdd />", string.Empty);

            //Axis 10.2(Bug No -92)
            requestXML = requestXML.Replace("<SalesOrderLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrderLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrderLineGroupAddREM>", string.Empty);
            //End Changes 

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            //Axis 706
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");

            string[] SalesOrderLineModFragment;

            int indexSalesOrderLine = requestXML.IndexOf(@"<SalesOrderLineMod>");
            int indexSalesOrderLineGroup = requestXML.IndexOf(@"<SalesOrderLineGroupMod>");

            if (indexSalesOrderLineGroup <= 0 || indexSalesOrderLine < indexSalesOrderLineGroup)
            {
                SalesOrderLineModFragment = requestXML.Split(new string[] { "<SalesOrderLineMod>" }, StringSplitOptions.None);
            }
            else
            {
                SalesOrderLineModFragment = requestXML.Split(new string[] { "<SalesOrderLineGroupMod>" }, StringSplitOptions.None);
            }

            if (SalesOrderLineModFragment.Length > 0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = SalesOrderLineModFragment[0];


                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").FirstChild;


                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertBefore(xfrag1, firstChild1);

                requestXML = requestXML.Replace(SalesOrderLineModFragment[0], string.Empty);

                //  requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(xfrag1, InvoiceMod2.LastChild);

            }



            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertAfter(xfrag, SalesOrderMod2.LastChild);

            //Axis 706 ends

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/Phone");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/Fax");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/Email");
                    SalesOrderMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod/InventorySiteLocationRef");
                    SalesOrderMod.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    SalesOrderMod.RemoveAll();


                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesOrderAddRq/SalesOrderAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesOrderQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By SalesOrderRefNumber) : " + rowcount.ToString());
            else
                SalesOrderQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesOrderModRq/SalesOrderMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesOrderModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesOrderModRs/SalesOrderRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesOrder(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion

    }

    public class SaleOrderQBEntryCollection : Collection<SalesOrderQBEntry>
    {
        public SalesOrderQBEntry FindSalesOrderEntry(DateTime date)
        {
            foreach (SalesOrderQBEntry item in this)
            {
                if (item.SalesOrderDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        public static int salesorder_cnt = 0;
        public static string salesorder_number = "";
        public SalesOrderQBEntry FindSalesOrderEntry(string refNumber)
        {
            salesorder_number = refNumber;
            foreach (SalesOrderQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    salesorder_cnt++;
                    return item;
                }
            }
            salesorder_cnt = 0;
            return null;
        }
        public SalesOrderQBEntry FindSalesOrderEntry(DateTime date, string refNumber)
        {
            foreach (SalesOrderQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.SalesOrderDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }

    /// <summary>
    /// Adding new line for sales order
    /// </summary>
    [XmlRootAttribute("SalesOrderLineAdd", Namespace = "", IsNullable = false)]
    public class SalesOrderLineAdd
    {
        #region Private Member Variables

        private ItemRef m_ItemRef;
        private string m_Desc;
        private string m_Quantity;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_UnitOfMeasure;
        private string m_Rate;
        private string m_RatePercent;
        private PriceLevelRef m_PriceLevelRef;
        private ClassRef m_ClassRef;
        private string m_Amount;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;

        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_Other1;
        private string m_Other2;
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();
        #endregion

        #region  Constructors

        public SalesOrderLineAdd()
        {

        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }
        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
                this.m_RatePercent = null;
            }
        }

        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { this.m_SerialNumber = value; }
        }


        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

       

        public string RatePercent
        {
            get { return this.m_RatePercent; }
            set
            {
                this.m_RatePercent = value;
                this.m_Rate = null;
            }
        }
        public PriceLevelRef PriceLevelRef
        {
            get { return this.m_PriceLevelRef; }
            set { m_PriceLevelRef = value; }
        }
             
        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }

        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }
 
        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        public string Other2
        {
            get { return this.m_Other2; }
            set { this.m_Other2 = value; }
        }
         [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }
        #endregion
    }

    /// <summary>
    /// Adding new line for sales order group
    /// </summary>
    [XmlRootAttribute("SalesOrderLineGroupAdd", Namespace = "", IsNullable = false)]
    public class SalesOrderLineGroupAdd
    {
        private ItemRef m_ItemGroupRef;
        private string m_ItemGroupQuantity;
        private string m_ItemGroupUOM;
        private InventorySiteRef m_ItemGroupInventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        //Bug no. 331       
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();
        
        //end Bug no. 331


        public ItemRef ItemGroupRef
        {
            get { return this.m_ItemGroupRef; }
            set { this.m_ItemGroupRef = value; }
        }

        public string Quantity
        {
            get { return this.m_ItemGroupQuantity; }
            set { this.m_ItemGroupQuantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_ItemGroupUOM; }
            set { this.m_ItemGroupUOM = value; }
        }
        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_ItemGroupInventorySiteRef; }
            set { this.m_ItemGroupInventorySiteRef = value; }
        }
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }
        //bug no. 331
        [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }
        //end bug no. 331
    }

   
}
