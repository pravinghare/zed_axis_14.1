using System;
using System.Collections.Generic;
using System.Text;

namespace DataProcessingBlocks
{
    public class TIException :Exception
    {
        private string m_Message;
        private string m_ErrorCode;
        public TIException(string errorCode)
        {
            this.m_Message = MessageCodes.GetValue(errorCode);
            this.m_ErrorCode = errorCode;
        }
        public TIException(string errorCode,string Path)
        {
            this.m_Message = string.Format(MessageCodes.GetValue(errorCode), Path);
            this.m_ErrorCode = errorCode;
        }
        public override string Message
        {
            get { return this.m_Message; }
        }
        public string ErrorCode
        {
            get { return this.m_ErrorCode; }
        }
    }
}
