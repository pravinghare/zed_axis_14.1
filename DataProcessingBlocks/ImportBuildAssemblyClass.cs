﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportBuildAssemblyClass
    {

        private static ImportBuildAssemblyClass m_ImportBuildAssemblyClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;


        #region Constructor

        public ImportBuildAssemblyClass()
        {

        }

        #endregion

        /// <summary>
        /// creating instance of class
        /// </summary>
        /// <returns></returns>
        public static ImportBuildAssemblyClass GetInstance()
        {
            if (m_ImportBuildAssemblyClass == null)
                m_ImportBuildAssemblyClass = new ImportBuildAssemblyClass();
            return m_ImportBuildAssemblyClass;
        }

         /// <summary>
        /// This method is used for validating import data and create Build Assembly and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Vendor QuickBooks collection </returns>
        public DataProcessingBlocks.BuildAssemblyQBEntryCollection ImportBuildAssemblyData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

             //Create an instance of Vendor Entry collections.
            DataProcessingBlocks.BuildAssemblyQBEntryCollection coll = new BuildAssemblyQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Vendor Entry

            #region Checking Validations

            DateTime BADt = new DateTime();
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                  //  DateTime BuilAssemblyDt = new DateTime();
                    string datevalue = string.Empty;

                    //Build Assembly Validation
                    DataProcessingBlocks.BuildAssemblyQBEntry BA = new BuildAssemblyQBEntry();

                    if (dt.Columns.Contains("ItemInventoryAssemblyRef"))
                    {
                        #region Validations of ItemInventoryAssemblyRef
                        if (dr["ItemInventoryAssemblyRef"].ToString() != string.Empty)
                        {
                            if (dr["ItemInventoryAssemblyRef"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemInventoryAssemblyRef (" + dr["ItemInventoryAssemblyRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.ItemInventoryAssemblyRef = new ItemInventoryAssemblyRef(dr["ItemInventoryAssemblyRef"].ToString());

                                        if (BA.ItemInventoryAssemblyRef.FullName == null)
                                        {
                                            BA.ItemInventoryAssemblyRef.FullName = null;
                                        }
                                        else
                                        {
                                            BA.ItemInventoryAssemblyRef = new ItemInventoryAssemblyRef(dr["ItemInventoryAssemblyRef"].ToString().Substring(0, 1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.ItemInventoryAssemblyRef = new ItemInventoryAssemblyRef(dr["ItemInventoryAssemblyRef"].ToString());
                                        if (BA.ItemInventoryAssemblyRef.FullName == null)
                                        {
                                            BA.ItemInventoryAssemblyRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    BA.ItemInventoryAssemblyRef = new ItemInventoryAssemblyRef(dr["ItemInventoryAssemblyRef"].ToString());
                                    if (BA.ItemInventoryAssemblyRef.FullName == null)
                                    {
                                        BA.ItemInventoryAssemblyRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                BA.ItemInventoryAssemblyRef = new ItemInventoryAssemblyRef(dr["ItemInventoryAssemblyRef"].ToString());
                                if (BA.ItemInventoryAssemblyRef.FullName == null)
                                {
                                    BA.ItemInventoryAssemblyRef.FullName = null;
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("InventorySiteRef"))
                    {
                        #region Validations of InventorySiteRef
                        if (dr["InventorySiteRef"].ToString() != string.Empty)
                        {
                            if (dr["InventorySiteRef"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This InventorySiteRef (" + dr["InventorySiteRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRef"].ToString());

                                        if (BA.InventorySiteRef.FullName == null)
                                        {
                                            BA.InventorySiteRef.FullName = null;
                                        }
                                        else
                                        {
                                            BA.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRef"].ToString().Substring(0, 1000));
                                        }

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRef"].ToString());
                                        if (BA.InventorySiteRef.FullName == null)
                                        {
                                            BA.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {                                   
                                    BA.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRef"].ToString());
                                    if (BA.InventorySiteRef.FullName == null)
                                    {
                                        BA.InventorySiteRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {                               
                                BA.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRef"].ToString());
                                if (BA.InventorySiteRef.FullName == null)
                                {
                                    BA.InventorySiteRef.FullName = null;
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("InventorySiteLocationRef"))
                    {
                        #region Validations of InventorySiteLocationRef
                        if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                        {
                            if (dr["InventorySiteLocationRef"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This InventorySiteLocationRef (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        
                                        BA.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());

                                        if (BA.InventorySiteLocationRef.FullName == null)
                                        {
                                            BA.InventorySiteLocationRef.FullName = null;
                                        }
                                        else
                                        {
                                            BA.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString().Substring(0, 1000));
                                        }
                                       

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (BA.InventorySiteLocationRef.FullName == null)
                                        {
                                            BA.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {                                  
                                    BA.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                    if (BA.InventorySiteLocationRef.FullName == null)
                                    {
                                        BA.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {                               
                                BA.InventorySiteLocationRef = new InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                               
                                if (BA.InventorySiteLocationRef.FullName == null)
                                {
                                    BA.InventorySiteLocationRef.FullName = null;
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SerialNumber"))
                    {
                        #region Validations of SerialNumber
                        if (dr["SerialNumber"].ToString() != string.Empty)
                        {
                            if (dr["SerialNumber"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    BA.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            else
                            {
                                BA.SerialNumber = dr["SerialNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LotNumber"))
                    {
                        #region Validations of LotNumber
                        if (dr["LotNumber"].ToString() != string.Empty)
                        {
                            if (dr["LotNumber"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.LotNumber = dr["LotNumber"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    BA.LotNumber = dr["LotNumber"].ToString();
                                }
                            }
                            else
                            {
                                BA.LotNumber = dr["LotNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TxnDate"))
                    {                       

                        #region validations of TxnDate
                        if (dr["TxnDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["TxnDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out BADt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {

                                            BA.TxnDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BA.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        BA.TxnDate = datevalue;
                                    }
                                }
                                else
                                {
                                    BADt = dttest;
                                    BA.TxnDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                BADt = Convert.ToDateTime(datevalue);
                                BA.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Validations of RefNumber
                        if (dr["RefNumber"].ToString() != string.Empty)
                        {
                            if (dr["RefNumber"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This RefNumber (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.RefNumber = dr["RefNumber"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.RefNumber = dr["RefNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    BA.RefNumber = dr["RefNumber"].ToString();
                                }
                            }
                            else
                            {
                                BA.RefNumber = dr["RefNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Memo"))
                    {
                        #region Validations of Memo
                        if (dr["Memo"].ToString() != string.Empty)
                        {
                            if (dr["Memo"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Memo (" + dr["Memo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.Memo = dr["Memo"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.Memo = dr["Memo"].ToString();
                                    }
                                }
                                else
                                {
                                    BA.Memo = dr["Memo"].ToString();
                                }
                            }
                            else
                            {
                                BA.Memo = dr["Memo"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("QuantityToBuild"))
                    {
                        #region Validations of QuantityToBuild
                        if (dr["QuantityToBuild"].ToString() != string.Empty)
                        {
                            if (dr["QuantityToBuild"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This QuantityToBuild (" + dr["QuantityToBuild"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BA.QuantityToBuild = dr["QuantityToBuild"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BA.QuantityToBuild = dr["QuantityToBuild"].ToString();
                                    }
                                }
                                else
                                {
                                    BA.QuantityToBuild = dr["QuantityToBuild"].ToString();
                                }
                            }
                            else
                            {
                                BA.QuantityToBuild = dr["QuantityToBuild"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MarkPendingIfRequired"))
                    {                       

                        #region Validations of MarkPendingIfRequired
                        if (dr["MarkPendingIfRequired"].ToString() != "<None>")
                        {

                            int result = 0;
                            if (int.TryParse(dr["MarkPendingIfRequired"].ToString(), out result))
                            {
                                BA.MarkPendingIfRequired = Convert.ToInt32(dr["MarkPendingIfRequired"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["MarkPendingIfRequired"].ToString().ToLower() == "true")
                                {
                                    BA.MarkPendingIfRequired = dr["MarkPendingIfRequired"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["MarkPendingIfRequired"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        BA.MarkPendingIfRequired = dr["MarkPendingIfRequired"].ToString().ToLower(); ;
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This MarkPendingIfRequired (" + dr["MarkPendingIfRequired"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(results) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            BA.MarkPendingIfRequired = dr["MarkPendingIfRequired"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BA.MarkPendingIfRequired = dr["MarkPendingIfRequired"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BA.MarkPendingIfRequired = dr["MarkPendingIfRequired"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }
                    coll.Add(BA);

                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }
    }
}
