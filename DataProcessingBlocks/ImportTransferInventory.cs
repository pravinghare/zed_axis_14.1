using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportTransferInventory
    {
        public static ImportTransferInventory m_ImportTransferInventory;
        public bool isIgnoreAll = false;

        #region Constuctor

        public ImportTransferInventory()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Transfer Inventory class
        /// </summary>
        /// <returns></returns>
        /// 
        public static ImportTransferInventory GetInstance()
        {
            if (m_ImportTransferInventory == null)
                m_ImportTransferInventory = new ImportTransferInventory();
            return m_ImportTransferInventory;
        }

        /// <summary>
        /// This method is used for validating import data.
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Transfer Inventory QuickBooks collection </returns>
        public DataProcessingBlocks.TransferInventoryQBEntryCollection ImportTransferInventoryData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of Invoice Entry collections.
            DataProcessingBlocks.TransferInventoryQBEntryCollection coll = new TransferInventoryQBEntryCollection();
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {                    
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Add TransferInventory Ref Number
                        TransferInventoryQBEntry TransferInventory = new TransferInventoryQBEntry();
                        TransferInventory = coll.FindTransferInventoryQBEntry(dr["RefNumber"].ToString());
                        if (TransferInventory == null)
                        {
                            TransferInventory = new TransferInventoryQBEntry();
                            DateTime TransferInventoryDt = new DateTime();
                            string datevalue = string.Empty;

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out TransferInventoryDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TransferInventory.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TransferInventory.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                TransferInventory.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryDt = dttest;
                                            TransferInventory.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        TransferInventoryDt = Convert.ToDateTime(datevalue);
                                        TransferInventory.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number
                               // if (datevalue != string.Empty)
                                    //SalesOrder.SalesOrderDate = SalesOrderDt;

                                    if (dr["RefNumber"].ToString() != string.Empty)
                                    {
                                        string strRefNum = dr["RefNumber"].ToString();
                                        if (strRefNum.Length > 11)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    TransferInventory.RefNumber = dr["RefNumber"].ToString().Substring(0, 11);
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    TransferInventory.RefNumber = dr["RefNumber"].ToString();
                                                }
                                            }
                                            else
                                                TransferInventory.RefNumber = dr["RefNumber"].ToString();

                                        }
                                        else
                                            TransferInventory.RefNumber = dr["RefNumber"].ToString();
                                    }
                                #endregion
                            }

                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                                TransferInventory.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                TransferInventory.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            TransferInventory.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        TransferInventory.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("FromInventorySiteRefFullName"))
                            {
                                #region Validations of FromInventorySiteRef Full name
                                if (dr["FromInventorySiteRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["FromInventorySiteRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["FromInventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                                if (TransferInventory.FromInventorySiteRef.FullName == null)
                                                    TransferInventory.FromInventorySiteRef.FullName = null;                                                
                                                else
                                                    TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString().Substring(0, 1000));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                                if (TransferInventory.FromInventorySiteRef.FullName == null)
                                                    TransferInventory.FromInventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                            if (TransferInventory.FromInventorySiteRef.FullName == null)
                                                TransferInventory.FromInventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                        if (TransferInventory.FromInventorySiteRef.FullName == null)
                                            TransferInventory.FromInventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ToInventorySiteRefFullName"))
                            {
                                #region Validations of ToInventorySiteRef Full name
                                if (dr["ToInventorySiteRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ToInventorySiteRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ToInventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                                if (TransferInventory.ToInventorySiteRef.FullName == null)
                                                    TransferInventory.ToInventorySiteRef.FullName = null;
                                                else
                                                    TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString().Substring(0, 1000));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                                if (TransferInventory.ToInventorySiteRef.FullName == null)
                                                    TransferInventory.ToInventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                            if (TransferInventory.ToInventorySiteRef.FullName == null)
                                                TransferInventory.ToInventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                        if (TransferInventory.ToInventorySiteRef.FullName == null)
                                            TransferInventory.ToInventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("InventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                                    TransferInventory.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                                    TransferInventory.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                                TransferInventory.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                            TransferInventory.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            #region Adding TransferInventory Line

                            DataProcessingBlocks.TransferInventoryLineAdd TransferInventoryLine = new TransferInventoryLineAdd();

                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (TransferInventoryLine.ItemRef.FullName == null)
                                                    TransferInventoryLine.ItemRef.FullName = null;
                                                else
                                                    TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 209));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (TransferInventoryLine.ItemRef.FullName == null)
                                                    TransferInventoryLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (TransferInventoryLine.ItemRef.FullName == null)
                                                TransferInventoryLine.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (TransferInventoryLine.ItemRef.FullName == null)
                                            TransferInventoryLine.ItemRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("FromInventorySiteLocationRef"))
                            {
                                #region Validations of FromInventorySiteLocationRef

                                if (dr["FromInventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["FromInventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["FromInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                            if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                                TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                        if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                            TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ToInventorySiteLocationRef"))
                            {
                                #region Validations of ToInventorySiteLocationRef

                                if (dr["ToInventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["ToInventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["ToInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                            if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                                TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                        if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                            TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("QuantityToTransfer"))
                            {
                                #region Validations for QuantityToTransfer
                                if (dr["QuantityToTransfer"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["QuantityToTransfer"].ToString();
                                    TransferInventoryLine.QuantityToTransfer = strQuantity;
                                }

                                #endregion
                            }

                            //P Axis 13.1 : issue 659
                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    string strserialNum = dr["SerialNumber"].ToString();
                                    if (strserialNum.Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                            TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                    else
                                        TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    string strlotNum = dr["LotNumber"].ToString();
                                    if (strlotNum.Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                            TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                    else
                                        TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                }
                                #endregion
                            }


                            TransferInventory.TransferInventoryLineAdd.Add(TransferInventoryLine);
                            #endregion

                            coll.Add(TransferInventory);
                        }
                        else
                        {
                            #region Adding TransferInventory Line

                            DataProcessingBlocks.TransferInventoryLineAdd TransferInventoryLine = new TransferInventoryLineAdd();



                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (TransferInventoryLine.ItemRef.FullName == null)
                                                    TransferInventoryLine.ItemRef.FullName = null;
                                                else
                                                    TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 1000));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (TransferInventoryLine.ItemRef.FullName == null)
                                                    TransferInventoryLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (TransferInventoryLine.ItemRef.FullName == null)
                                                TransferInventoryLine.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (TransferInventoryLine.ItemRef.FullName == null)
                                            TransferInventoryLine.ItemRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("FromInventorySiteLocationRef"))
                            {
                                #region Validations of FromInventorySiteLocationRef

                                if (dr["FromInventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["FromInventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["FromInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                            if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                                TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.FromInventorySiteLocationRef = new QuickBookEntities.FromInventorySiteLocationRef(dr["FromInventorySiteLocationRef"].ToString());
                                        if (TransferInventoryLine.FromInventorySiteLocationRef.FullName == null)
                                            TransferInventoryLine.FromInventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ToInventorySiteLocationRef"))
                            {
                                #region Validations of ToInventorySiteLocationRef

                                if (dr["ToInventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["ToInventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["ToInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                                if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                                    TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                            if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                                TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.ToInventorySiteLocationRef = new QuickBookEntities.ToInventorySiteLocationRef(dr["ToInventorySiteLocationRef"].ToString());
                                        if (TransferInventoryLine.ToInventorySiteLocationRef.FullName == null)
                                            TransferInventoryLine.ToInventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("QuantityToTransfer"))
                            {
                                #region Validations for QuantityToTransfer
                                if (dr["QuantityToTransfer"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["QuantityToTransfer"].ToString();
                                    TransferInventoryLine.QuantityToTransfer = strQuantity;
                                }

                                #endregion
                            }

                            //P Axis 13.1 : issue 659
                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    string strserialNum = dr["SerialNumber"].ToString();
                                    if (strserialNum.Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                            TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                    else
                                        TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    string strlotNum = dr["LotNumber"].ToString();
                                    if (strlotNum.Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventoryLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                            TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                    else
                                        TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                }
                                #endregion
                            }

                            TransferInventory.TransferInventoryLineAdd.Add(TransferInventoryLine);

                            #endregion

                        }
                        #endregion
                    }
                    else
                    {
                        #region Without Adding Ref Number
                        TransferInventoryQBEntry TransferInventory = new TransferInventoryQBEntry();

                        DateTime TransferInventoryDt = new DateTime();
                        string datevalue = string.Empty;


                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out TransferInventoryDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventory.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventory.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            TransferInventory.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryDt = dttest;
                                        TransferInventory.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    TransferInventoryDt = Convert.ToDateTime(datevalue);
                                    TransferInventory.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref Number
                           // if (datevalue != string.Empty)


                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                TransferInventory.RefNumber = dr["RefNumber"].ToString().Substring(0, 11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                TransferInventory.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                            TransferInventory.RefNumber = dr["RefNumber"].ToString();

                                    }
                                    else
                                        TransferInventory.RefNumber = dr["RefNumber"].ToString();
                                }
                            #endregion
                        }



                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                            TransferInventory.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            TransferInventory.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        TransferInventory.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    TransferInventory.Memo = strMemo;
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("FromInventorySiteRefFullName"))
                        {
                            #region Validations of FromInventorySiteRef Full name
                            if (dr["FromInventorySiteRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["FromInventorySiteRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item full name (" + dr["FromInventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                            if (TransferInventory.FromInventorySiteRef.FullName == null)
                                                TransferInventory.FromInventorySiteRef.FullName = null;
                                            else
                                                TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString().Substring(0, 1000));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                            if (TransferInventory.FromInventorySiteRef.FullName == null)
                                                TransferInventory.FromInventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                        if (TransferInventory.FromInventorySiteRef.FullName == null)
                                            TransferInventory.FromInventorySiteRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    TransferInventory.FromInventorySiteRef = new FromInventorySiteRef(dr["FromInventorySiteRefFullName"].ToString());
                                    if (TransferInventory.FromInventorySiteRef.FullName == null)
                                        TransferInventory.FromInventorySiteRef.FullName = null;
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ToInventorySiteRefFullName"))
                        {
                            #region Validations of ToInventorySiteRef Full name
                            if (dr["ToInventorySiteRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ToInventorySiteRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item full name (" + dr["ToInventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                            if (TransferInventory.ToInventorySiteRef.FullName == null)
                                                TransferInventory.ToInventorySiteRef.FullName = null;
                                            else
                                                TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString().Substring(0, 1000));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                            if (TransferInventory.ToInventorySiteRef.FullName == null)
                                                TransferInventory.ToInventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                        if (TransferInventory.ToInventorySiteRef.FullName == null)
                                            TransferInventory.ToInventorySiteRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    TransferInventory.ToInventorySiteRef = new ToInventorySiteRef(dr["ToInventorySiteRefFullName"].ToString());
                                    if (TransferInventory.ToInventorySiteRef.FullName == null)
                                        TransferInventory.ToInventorySiteRef.FullName = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("InventorySiteLocationRef"))
                        {
                            #region Validations of InventorySiteLocationRef

                            if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                            {
                                if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                                TransferInventory.InventorySiteLocationRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                                TransferInventory.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                            TransferInventory.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    TransferInventory.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                    if (TransferInventory.InventorySiteLocationRef.FullName == null)
                                        TransferInventory.InventorySiteLocationRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        #region Adding TransferInventory Line

                        DataProcessingBlocks.TransferInventoryLineAdd TransferInventoryLine = new TransferInventoryLineAdd();

                        if (dt.Columns.Contains("ItemRefFullName"))
                        {
                            #region Validations of item Full name
                            if (dr["ItemRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemRefFullName"].ToString().Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (TransferInventoryLine.ItemRef.FullName == null)
                                                TransferInventoryLine.ItemRef.FullName = null;
                                            else
                                                TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString().Substring(0, 209));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (TransferInventoryLine.ItemRef.FullName == null)
                                                TransferInventoryLine.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (TransferInventoryLine.ItemRef.FullName == null)
                                            TransferInventoryLine.ItemRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    TransferInventoryLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    if (TransferInventoryLine.ItemRef.FullName == null)
                                        TransferInventoryLine.ItemRef.FullName = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("QuantityToTransfer"))
                        {
                            #region Validations for QuantityToTransfer
                            if (dr["QuantityToTransfer"].ToString() != string.Empty)
                            {
                                string strQuantity = dr["QuantityToTransfer"].ToString();
                                TransferInventoryLine.QuantityToTransfer = strQuantity;
                            }

                            #endregion
                        }

                        //P Axis 13.1 : issue 659
                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                string strserialNum = dr["SerialNumber"].ToString();
                                if (strserialNum.Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                        TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                                }
                                else
                                    TransferInventoryLine.SerialNumber = dr["SerialNumber"].ToString();
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LotNumber"))
                        {
                            #region Validations of LotNumber
                            if (dr["LotNumber"].ToString() != string.Empty)
                            {
                                string strlotNum = dr["LotNumber"].ToString();
                                if (strlotNum.Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            TransferInventoryLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                        TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                                }
                                else
                                    TransferInventoryLine.LotNumber = dr["LotNumber"].ToString();
                            }
                            #endregion
                        }

                        TransferInventory.TransferInventoryLineAdd.Add(TransferInventoryLine);

                        #endregion

                        coll.Add(TransferInventory);

                        #endregion

                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Item Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                    if (dt.Columns.Contains("ItemRefFullName"))
                    {
                        if (dr["ItemRefFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemRefFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemRefFullName"].ToString();
                            }

                            #region Set Item Query

                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");


                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        //FullName.InnerText = dr["ItemRefFullName"].ToString();
                                        ItemQueryRq.AppendChild(FullName);
                                    }

                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }

                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {

                                                if (defaultSettings.Type == "NonInventoryPart")
                                                {
                                                    #region Item NonInventory Add Query

                                                    XmlDocument ItemNonInvendoc = new XmlDocument();
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                                    XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                    ItemNonInvendoc.AppendChild(qbXMLINI);
                                                    XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                    qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                    qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                    ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                    ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                    XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                    ININame.InnerText = arr[i];
                                                    //ININame.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemNonInventoryAdd.AppendChild(ININame);

                                                    //Solution for BUG 633
                                                    //if (i > 0 && i <= 2)
                                                    //{
                                                    //    if (arr[i] != null && arr[i] != string.Empty)
                                                    //    {
                                                    //        //Adding Parent
                                                    //        XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                    //        ItemNonInventoryAdd.AppendChild(INIParent);

                                                    //        if (i == 2)
                                                    //        {
                                                    //            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                    //            INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    //            INIParent.AppendChild(INIChildFullName);
                                                    //        }
                                                    //        else if (i == 1)
                                                    //        {
                                                    //            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");
                                                    //            INIChildFullName.InnerText = arr[i - 1];
                                                    //            INIParent.AppendChild(INIChildFullName);
                                                    //        }

                                                    //    }
                                                    //}

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }


                                                    //Adding Tax Code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                            ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                                    //Adding Desc and Rate
                                                    //Solution for BUG 631 nad 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemNonInvendoc.CreateElement("Desc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            INISalesAndPurchase.AppendChild(ISDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("CustomPrice"))
                                                    {
                                                        if (dr["CustomPrice"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISCustom = ItemNonInvendoc.CreateElement("Price");
                                                            ISCustom.InnerText = dr["CustomPrice"].ToString();
                                                            INISalesAndPurchase.AppendChild(ISCustom);
                                                            IsPresent = true;
                                                        }
                                                    }

                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                        INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                        //INIFullName.InnerText = "Sales";
                                                        INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                    }
                                                    string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                                    //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                                    string respItemNonInvendoc = string.Empty;
                                                    try
                                                    {
                                                        respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemNonInvendoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strtest2 = respItemNonInvendoc;

                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "Service")
                                                {
                                                    #region Item Service Add Query

                                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemServiceAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    //if (i > 0 && i <= 2)
                                                    //{
                                                    //    if (arr[i] != null && arr[i] != string.Empty)
                                                    //    {
                                                    //        //Adding Parent
                                                    //        XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                    //        ItemServiceAdd.AppendChild(INIParent);

                                                    //        if (i == 2)
                                                    //        {
                                                    //            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                    //            INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    //            INIParent.AppendChild(INIChildFullName);
                                                    //        }
                                                    //        else if (i == 1)
                                                    //        {
                                                    //            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                    //            INIChildFullName.InnerText = arr[i - 1];
                                                    //            INIParent.AppendChild(INIChildFullName);
                                                    //        }

                                                    //    }
                                                    //}

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }


                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                        }
                                                    }


                                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                                    //Adding Desc and Rate
                                                    //Solution for BUG 631 nad 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ISSalesAndPurchase.AppendChild(ISDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("CustomPrice"))
                                                    {
                                                        if (dr["CustomPrice"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISCustom = ItemServiceAdddoc.CreateElement("Price");
                                                            ISCustom.InnerText = dr["CustomPrice"].ToString();
                                                            ISSalesAndPurchase.AppendChild(ISCustom);
                                                            IsPresent = true;
                                                        }
                                                    }

                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                        //Adding IncomeAccount FullName.
                                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                    //ItemServiceAdddoc.Save("C://ItemServiceAdddoc.xml");
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemInventoryAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    //if (i > 0 && i <= 2)
                                                    //{
                                                    //    if (arr[i] != null && arr[i] != string.Empty)
                                                    //    {
                                                    //        //Adding Parent
                                                    //        XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                    //        ItemInventoryAdd.AppendChild(INIParent);

                                                    //        if (i == 2)
                                                    //        {
                                                    //            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                    //            INIChildFullName.InnerText = arr[i - 2] + ":" + arr[i - 1];
                                                    //            INIParent.AppendChild(INIChildFullName);
                                                    //        }
                                                    //        else if (i == 1)
                                                    //        {
                                                    //            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                    //            INIChildFullName.InnerText = arr[i - 1];
                                                    //            INIParent.AppendChild(INIChildFullName);
                                                    //        }

                                                    //    }
                                                    //}


                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                ItemInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }


                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    //Adding Desc and Rate
                                                    //Solution for BUG 631 nad 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ItemInventoryAdd.AppendChild(ISDesc);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("CustomPrice"))
                                                    {
                                                        if (dr["CustomPrice"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISCustom = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                            ISCustom.InnerText = dr["CustomPrice"].ToString();
                                                            ItemInventoryAdd.AppendChild(ISCustom);
                                                        }
                                                    }

                                                    //Adding IncomeAccountRef
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                    }

                                                    //Adding COGSAccountRef
                                                    if (defaultSettings.COGSAccount != string.Empty)
                                                    {
                                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                    }

                                                    //Adding AssetAccountRef
                                                    if (defaultSettings.AssetAccount != string.Empty)
                                                    {
                                                        XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                        XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                        INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                    }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest4 = respItemInventoryAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }
                }
            }
                else
                {
                    return null;
                }
            }
            #endregion

            return coll;
        }
    }
}