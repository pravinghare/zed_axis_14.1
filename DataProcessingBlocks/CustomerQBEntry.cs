using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("CustomerQBEntry", Namespace = "", IsNullable = false)]
    public class CustomerQBEntry
    {
        #region Private Member Variable

        private string m_Name;
        private string m_IsActive;
        private ParentRef m_ParentRef;
        private string m_CompanyName;
        private string m_Salutation;
        private string m_FirstName;
        private string m_MiddleName;
        private string m_LastName;
        private Collection<BillAddress> m_BillAddress = new Collection<BillAddress>();
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        //P Axis 13.2 : issue 702
        private string m_Phone;
        private Collection<AdditionalContactRef> m_AdditionalContactRef = new Collection<AdditionalContactRef>();
        private string m_AltPhone;
        private string m_Fax;
        private string m_Email;
        private string m_Contact;
        private string m_AltContact;
        private CustomerTypeRef m_CustomerTypeRef;
        private TermsRef m_TermRef;
        private SalesRepRef m_SalesRepRef;
        private string m_OpenBalance;
        private string m_OpenBalanceDate;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_SalesTaxCountry;
        private string m_ReSaleNumber;
        private string m_AccountNumber;
        private string m_CreditLimit;
        private PreferredPaymentMethodRef m_PreferredPaymentMethodRef;
        private Collection<CreditCardInfo> m_CreditCardInfo=new Collection<CreditCardInfo>();
        private string m_JobStatus;
        private string m_JobStartDate;
        private string m_JobProjectedEndDate;
        private string m_JobEndDate;
        private string m_JobDesc;
        private JobTypeRef m_JobTypeRef;
        private string m_Notes;
        private AdditionalNotes m_AdditionalNote;
       
        private PriceLevelRef m_PriceLevelRef;
        private string m_ExternalGUID;
        private CurrencyRef m_CurrencyRef;

        //Axis 331
        private ClassRef m_Class;
        private string m_JobTitle;
        private Collection<ShipToAddress> m_ShipToAddress = new Collection<ShipToAddress>();
      
        private string m_Cc;
        private string m_PreferredDeliveryMethod;
        private string m_TaxRegistrationNumber;

        #endregion

        #region Constructor
        public CustomerQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public ParentRef ParentRef
        {
            get { return m_ParentRef; }
            set { m_ParentRef = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }


        public string Salutation
        {
            get { return m_Salutation; }
            set { m_Salutation = value; }
        }


        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }


        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }


        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }


        [XmlArray("BillAddressREM")]
        public Collection<BillAddress> BillAddress
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        //P Axis 13.2 : issue 702
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }

        public string AltPhone
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }


        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string Contact
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string AltContact
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

        public CustomerTypeRef CustomerTypeRef
        {
            get { return m_CustomerTypeRef; }
            set { m_CustomerTypeRef = value; }
        }

        public TermsRef TermsRef
        {
            get { return m_TermRef; }
            set { m_TermRef = value; }
        }

        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }

        public string OpenBalance
        {
            get { return m_OpenBalance; }
            set { m_OpenBalance = value; }
        }
        //Axis 311
        public ClassRef ClassRef
        {
            get { return m_Class; }
            set { m_Class = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }
        public AdditionalNotes AdditionalNotes
        {
            get { return m_AdditionalNote; }
            set { m_AdditionalNote = value; }
        }
        public string JobTitle
        {
            get { return m_JobTitle; }
            set { m_JobTitle = value; }
        }
         [XmlArray("ShipToAddressREM")]
        public Collection<ShipToAddress> ShipToAddress
        {
            get { return m_ShipToAddress; }
            set { m_ShipToAddress = value; }
        }
       
        public string Cc
        {
            get { return m_Cc; }
            set { m_Cc = value; }
        }
         [XmlArray("AdditionalContactREM")]
        public Collection<AdditionalContactRef> AdditionalContactRef
        {
            get { return m_AdditionalContactRef; }
            set { m_AdditionalContactRef = value; }
        }
       
        public string PreferredDeliveryMethod
        {
            get { return m_PreferredDeliveryMethod; }
            set { m_PreferredDeliveryMethod = value; }
        }

        public string TaxRegistrationNumber
        {
            get { return m_TaxRegistrationNumber; }
            set { m_TaxRegistrationNumber = value; }
        }

        [XmlElement(DataType = "string")]
        public string OpenBalanceDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_OpenBalanceDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_OpenBalanceDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_OpenBalanceDate = value;
            }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }

        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return m_ItemSalesTaxRef; }
            set { m_ItemSalesTaxRef = value; }
        }


        public string SalesTaxCountry
        {
            get { return m_SalesTaxCountry; }
            set { m_SalesTaxCountry = value; }
        }

        public string ResaleNumber
        {
            get { return m_ReSaleNumber; }
            set { m_ReSaleNumber = value; }
        }


        public string AccountNumber
        {
            get { return m_AccountNumber; }
            set { m_AccountNumber = value; }
        }

        public string CreditLimit
        {
            get { return m_CreditLimit; }
            set { m_CreditLimit = value; }
        }
        public PreferredPaymentMethodRef PreferredPaymentMethodRef
        {
            get { return m_PreferredPaymentMethodRef; }
            set { m_PreferredPaymentMethodRef = value; }
        }

        
        [XmlArray("CreditCardInfoREM")]
        public Collection<CreditCardInfo> CreditCardInfo
        {
            get { return m_CreditCardInfo; }
            set { m_CreditCardInfo = value; }
        }

        public string JobStatus
        {
            get { return m_JobStatus; }
            set { m_JobStatus = value; }
        }

        [XmlElement(DataType = "string")]
        public string JobStartDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_JobStartDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_JobStartDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_JobStartDate = value;
            }
        }

        [XmlElement(DataType = "string")]
        public string JobProjectedEndDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_JobProjectedEndDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_JobProjectedEndDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_JobProjectedEndDate = value;
            }
        }

        [XmlElement(DataType = "string")]
        public string JobEndDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_JobEndDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_JobEndDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_JobEndDate = value;
            }
        }

        public string JobDesc
        {
            get { return m_JobDesc; }
            set { m_JobDesc = value; }
        }

        public JobTypeRef JobTypeRef
        {
            get { return m_JobTypeRef; }
            set { m_JobTypeRef = value; }
        }

     

        public PriceLevelRef PriceLevelRef
        {
            get { return m_PriceLevelRef; }
            set { m_PriceLevelRef = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CustomerQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();
            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement CustomerAddRq = requestXmlDoc.CreateElement("CustomerAddRq");
            inner.AppendChild(CustomerAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement CustomerAdd = requestXmlDoc.CreateElement("CustomerAdd");

            CustomerAddRq.AppendChild(CustomerAdd);

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<CreditCardInfoREM />", string.Empty);
            requestXML = requestXML.Replace("<CreditCardInfoREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditCardInfoREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipToAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipToAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipToAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<AdditionalContactREM />", string.Empty);
            requestXML = requestXML.Replace("<AdditionalContactREM>", string.Empty);
            requestXML = requestXML.Replace("</AdditionalContactREM>", string.Empty);

            CustomerAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CustomerAddRq/CustomerAdd"))
            //{
            //    if (oNode.SelectSingleNode("Phone") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerAddRq/CustomerAdd/Phone");
            //        CustomerAdd.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CustomerAddRq/CustomerAdd"))
            //{
            //    if (oNode.SelectSingleNode("Fax") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerAddRq/CustomerAdd/Fax");
            //        CustomerAdd.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CustomerAddRq/CustomerAdd"))
            //{
            //    if (oNode.SelectSingleNode("Email") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerAddRq/CustomerAdd/Email");
            //        CustomerAdd.RemoveChild(childNode);
            //    }
            //}

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            statusMessage += "\n ";
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs/CustomerRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCustomer(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating CustomerQBEntry information
        /// of existing CustomerQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateCustomerInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, string noteID)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CustomerQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement CustomerQBEntryModRq = requestXmlDoc.CreateElement("CustomerModRq");
            inner.AppendChild(CustomerQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement CustomerMod = requestXmlDoc.CreateElement("CustomerMod");
            CustomerQBEntryModRq.AppendChild(CustomerMod);


            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<CreditCardInfoREM />", string.Empty);
            requestXML = requestXML.Replace("<CreditCardInfoREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditCardInfoREM>", string.Empty);


            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
          
            requestXML = requestXML.Replace("<ShipToAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipToAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipToAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AdditionalContactREM />", string.Empty);
            requestXML = requestXML.Replace("<AdditionalContactREM>", string.Empty);
            requestXML = requestXML.Replace("</AdditionalContactREM>", string.Empty);
            requestXML = requestXML.Replace("<AdditionalNotes>", "<AdditionalNotesMod>");
            requestXML = requestXML.Replace("</AdditionalNotes>", "</AdditionalNotesMod>");

            CustomerMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod"))
            //{
            //    if (oNode.SelectSingleNode("Phone") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod/Phone");
            //        CustomerMod.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod"))
            //{
            //    if (oNode.SelectSingleNode("Fax") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod/Fax");
            //        CustomerMod.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod"))
            //{
            //    if (oNode.SelectSingleNode("Email") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod/Email");
            //        CustomerMod.RemoveChild(childNode);
            //    }
            //}

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;
            
            //721
            if (requestXmlDoc.OuterXml.Contains("AdditionalNotesMod"))
            {
                XmlNode Note = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod/AdditionalNotesMod").FirstChild;
                System.Xml.XmlElement NoteID = requestXmlDoc.CreateElement("NoteID");
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CustomerModRq/CustomerMod/AdditionalNotesMod").InsertBefore(NoteID, Note).InnerText = (Convert.ToInt32(noteID) + 1).ToString(); ;
            }
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerModRs/CustomerRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCustomer(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class CustomerQBEntryCollection : Collection<CustomerQBEntry>
    {
        
    }

    public enum JobStatus
    {
        Awarded,
        Closed,
        InProgress,
        None,
        NotAwarded,
        Pending
    }
    public enum PreferredDeliveryMethod
    {
        None,
        Email,
        Fax
    }
   
}
