using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public  class ImportInventoryAdjustmentClass
    {
        private static ImportInventoryAdjustmentClass m_ImportInventoryAdjustmentClass;
        public bool isIgnoreAll = false;

        #region Constructor
        public ImportInventoryAdjustmentClass()
        { 
        
        }
        #endregion

        /// <summary>
        ///Create an instance of Inventory Adjustment class.
        /// </summary>
        /// <returns></returns>
        public static ImportInventoryAdjustmentClass GetInstance()
        {
            if (m_ImportInventoryAdjustmentClass == null)
                m_ImportInventoryAdjustmentClass = new ImportInventoryAdjustmentClass();
            return m_ImportInventoryAdjustmentClass;
        }
        /// <summary>
        /// This method is used for validating import data and create InventoryAdjustment transaction
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <param name="defaultSettings"></param>
        /// <returns></returns>
        public DataProcessingBlocks.InventoryAdjustmentEntryCollection ImportInventoryAdjustmentData(DataTable dt , ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            DataProcessingBlocks.InventoryAdjustmentEntryCollection coll = new InventoryAdjustmentEntryCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    DateTime InventoryAddDt = new DateTime();
                    string datevalue = string.Empty;
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Adding ref number
                        InventoryAdjustmentEntry InventoryAdjustment = new InventoryAdjustmentEntry();
                        InventoryAdjustment = coll.FindInventoryAdjustmentEntry(dr["RefNumber"].ToString());
                        if (InventoryAdjustment == null)
                        {
                            InventoryAdjustment = new InventoryAdjustmentEntry();
                            if (dt.Columns.Contains("AccountRefFullName"))
                            {
                                #region Validations of AccountRef Full Name
                                if (dr["AccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strAccountRef = dr["AccountRefFullName"].ToString();
                                    if (strAccountRef.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                                if (InventoryAdjustment.AccountRef.FullName == null)
                                                {
                                                    InventoryAdjustment.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                                if (InventoryAdjustment.AccountRef.FullName == null)
                                                {
                                                    InventoryAdjustment.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                            if (InventoryAdjustment.AccountRef.FullName == null)
                                            {
                                                InventoryAdjustment.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                        if (InventoryAdjustment.AccountRef.FullName == null)
                                        {
                                            InventoryAdjustment.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out InventoryAddDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjustment.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjustment.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjustment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAddDt = dttest;
                                            InventoryAdjustment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        InventoryAddDt = Convert.ToDateTime(datevalue);
                                        InventoryAdjustment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");

                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number

                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjustment.RefNumber = dr["RefNumber"].ToString().Substring(0,11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                        InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("InventorySiteRefFullName"))
                            {
                                #region Validations of InventorySiteRef Full Name
                                if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["InventorySiteRefFullName"].ToString();
                                    if (strTerms.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This InventorySiteRef FullName is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                                {
                                                    InventoryAdjustment.InventorySiteRef.FullName = null;
                                                }
                                                else
                                                {
                                                    InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 1000));
                                                }

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                                {
                                                    InventoryAdjustment.InventorySiteRef.FullName = null;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                            {
                                                InventoryAdjustment.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                        {
                                            InventoryAdjustment.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                           

                            if (dt.Columns.Contains("CustomerRefFullName"))
                            {
                                #region Validations of CustomerRef Full name
                                if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["CustomerRefFullName"].ToString();
                                    if (strTerms.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (InventoryAdjustment.CustomerRef.FullName == null)
                                                {
                                                    InventoryAdjustment.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0,1000));
                                                }

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (InventoryAdjustment.CustomerRef.FullName == null)
                                                {
                                                    InventoryAdjustment.CustomerRef.FullName = null;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (InventoryAdjustment.CustomerRef.FullName == null)
                                            {
                                                InventoryAdjustment.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (InventoryAdjustment.CustomerRef.FullName == null)
                                        {
                                            InventoryAdjustment.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of ClassRef Full name
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["ClassRefFullName"].ToString();
                                    if (strTerms.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ClassRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                                if (InventoryAdjustment.ClassRef.FullName == null)
                                                {
                                                    InventoryAdjustment.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0, 1000));

                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                                if (InventoryAdjustment.ClassRef.FullName == null)
                                                {
                                                    InventoryAdjustment.ClassRef.FullName = null;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                            if (InventoryAdjustment.ClassRef.FullName == null)
                                            {
                                                InventoryAdjustment.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (InventoryAdjustment.ClassRef.FullName == null)
                                        {
                                            InventoryAdjustment.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0,4000);
                                                InventoryAdjustment.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                InventoryAdjustment.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            InventoryAdjustment.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        InventoryAdjustment.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }

                            

                            #region Inventory Adjustment Line Add
                            DataProcessingBlocks.InventoryAdjustmentLineAdd InventoryAdjLine = new InventoryAdjustmentLineAdd();
                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of Item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    InventoryAdjLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (InventoryAdjLine.ItemRef.FullName == null)
                                    {
                                        InventoryAdjLine.ItemRef.FullName = null;
                                    }
                                }
                                #endregion

                            }

                            //Create an object of Quantity adjustment only when datatable contains any one value..
                            if ((dt.Columns.Contains("QuantityAdjNewQuantity") && dr["QuantityAdjNewQuantity"].ToString() != string.Empty) || (dt.Columns.Contains("QuantityAdjDifference") && dr["QuantityAdjDifference"].ToString() != string.Empty))
                            {
                                InventoryAdjLine.QuantityAdjustment = new QuantityAdjustment();
                                if (dt.Columns.Contains("QuantityAdjNewQuantity"))
                                {
                                    #region Validations for QuantityAjustment NewQuantity
                                    if (dr["QuantityAdjNewQuantity"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["QuantityAdjNewQuantity"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This QANewQuantiy ( " + dr["QuantityAdjNewQuantity"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;

                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                }
                                            }
                                        }
                                        else
                                        {

                                            //P Axis 13.1 : issue 698
                                            //InventoryAdjLine.QuantityAdjustment.NewQuantity = string.Format("{0:000000.00}", Convert.ToDouble(dr["QuantityAdjNewQuantity"].ToString()));
                                            InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                        }
                                    }

                                    #endregion
                                }
                                // bug 496 axis 12.0
                                if (dt.Columns.Contains("QuantityAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["QuantityAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["QuantityAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Inventory Site Ref Full Name (" + dr["QuantityAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("QuantityAdjDifference"))
                                {
                                    #region Validations for QuantityDifference
                                    if (dr["QuantityAdjDifference"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["QuantityAdjDifference"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This QAQuantityAdjDifference ( " + dr["QuantityAdjDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                        }
                                    }

                                    #endregion
                                }
                            }

                            // axis 10.0 chnages

                            //P Axis 13.1 : issue 698
                            if (dt.Columns.Contains("QuantityAdjSerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["QuantityAdjSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["QuantityAdjSerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QuantityAdjSerialNumber (" + dr["QuantityAdjSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("QuantityAdjLotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["QuantityAdjLotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["QuantityAdjLotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QuantityAdjLotNumber (" + dr["QuantityAdjLotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //P Axis 13.1 : issue 698 ENDS

                            // axis 10.0 chnges ends

                            //Create an object of value adjustment only when datable contains any one value.
                            if ((dt.Columns.Contains("ValueAdjNewQuantity") && dr["ValueAdjNewQuantity"].ToString() != string.Empty)
                                || (dt.Columns.Contains("ValueAdjNewValue") && dr["ValueAdjNewValue"].ToString() != string.Empty)
                                || (dt.Columns.Contains("ValueAdjQuantityDifference") && dr["ValueAdjQuantityDifference"].ToString() != string.Empty)
                                || (dt.Columns.Contains("ValueAdjValueDifference") && dr["ValueAdjValueDifference"].ToString() != string.Empty))
                            {
                                InventoryAdjLine.ValueAdjustment = new ValueAdjustment();
                                if (dt.Columns.Contains("ValueAdjNewQuantity"))
                                {
                                    #region Validations for Value Adjustment NewQuantity
                                    if (dr["ValueAdjNewQuantity"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjNewQuantity"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjNewQuantity ( " + dr["ValueAdjNewQuantity"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                        }
                                    }

                                    #endregion
                                }
                                // bug 496 axis 12.0
                                //if (dt.Columns.Contains("InventorySiteLocationRef"))
                                //{
                                //    #region Validations of InventorySiteLocationRef

                                //    if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                //    {
                                //        if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                //        {
                                //            if (isIgnoreAll == false)
                                //            {
                                //                string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                //                if (Convert.ToString(result) == "Cancel")
                                //                {
                                //                    continue;
                                //                }
                                //                if (Convert.ToString(result) == "No")
                                //                {
                                //                    return null;
                                //                }
                                //                if (Convert.ToString(result) == "Ignore")
                                //                {
                                //                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                //                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                //                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                //                }
                                //                if (Convert.ToString(result) == "Abort")
                                //                {
                                //                    isIgnoreAll = true;
                                //                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                //                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                //                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                //                }
                                //            }
                                //            else
                                //            {
                                //                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                //                if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                //                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                //            }
                                //        }
                                //        else
                                //        {
                                //            InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                //            if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                //                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                //        }
                                //    }
                                //    #endregion
                                //}
                                if (dt.Columns.Contains("ValueAdjQuantityDifference"))
                                {
                                    #region Validations for Value Adjustment Quantity Difference
                                    if (dr["ValueAdjQuantityDifference"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjQuantityDifference"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjQuantityDifference ( " + dr["ValueAdjQuantityDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("ValueAdjNewValue"))
                                {
                                    #region Validations for ValueAdjNewValue
                                    if (dr["ValueAdjNewValue"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjNewValue"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjNewValue ( " + dr["ValueAdjNewValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.ValueAdjustment.NewValue = string.Format("{0:0.00}", Convert.ToDouble(dr["ValueAdjNewValue"].ToString()));
                                        }
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ValueAdjValueDifference"))
                                {
                                    #region Validations for ValueDifference
                                    if (dr["ValueAdjValueDifference"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjValueDifference"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjValueDifference ( " + dr["ValueAdjValueDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            //InventoryAdjLine.ValueAdjustment.ValueDifference = string.Format("{0:000000.00}", Convert.ToDouble(dr["ValueAdjValueDifference"].ToString()));
                                            InventoryAdjLine.ValueAdjustment.ValueDifference = Math.Round(Convert.ToDouble(dr["ValueAdjValueDifference"].ToString()), 5).ToString();
                                        }
                                    }
                                    #endregion
                                }
                            }
                           

                            if ((dt.Columns.Contains("SerialNumberAdjAddSerialNumber") && dr["SerialNumberAdjAddSerialNumber"].ToString() != string.Empty)
                               || (dt.Columns.Contains("SerialNumberAdjRemoveSerialNumber") && dr["SerialNumberAdjRemoveSerialNumber"].ToString() != string.Empty))
                               
                            {
                                InventoryAdjLine.SerialNumberAdjustment = new SerialNumberAdjustment();

                                if (dt.Columns.Contains("SerialNumberAdjAddSerialNumber"))
                                {
                                    #region Validations of SerialNumberAdjAddSerialNumber

                                    if (dr["SerialNumberAdjAddSerialNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumberAdjAddSerialNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumberAdjAddSerialNumber (" + dr["SerialNumberAdjAddSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                                  
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("SerialNumberAdjRemoveSerialNumber"))
                                {
                                    #region Validations of SerialNumberAdjAddSerialNumber

                                    if (dr["SerialNumberAdjRemoveSerialNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumberAdjRemoveSerialNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumberAdjRemoveSerialNumber (" + dr["SerialNumberAdjRemoveSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("SerialNumberAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["SerialNumberAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumberAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumberAdj InventorySiteLocationRef (" + dr["SerialNumberAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }


                            }

                            if ((dt.Columns.Contains("LotNumberAdjLotNumber") && dr["LotNumberAdjLotNumber"].ToString() != string.Empty)
                              || (dt.Columns.Contains("LotNumberAdjCountAdjustment") && dr["LotNumberAdjCountAdjustment"].ToString() != string.Empty)
                               || (dt.Columns.Contains("LotNumberAdjInventorySiteLocationRef") && dr["LotNumberAdjInventorySiteLocationRef"].ToString() != string.Empty))
                            {
                                InventoryAdjLine.LotNumberAdjustment = new LotNumberAdjustment();

                                if (dt.Columns.Contains("LotNumberAdjLotNumber"))
                                {
                                    #region Validations of SerialNumberAdjAddSerialNumber

                                    if (dr["LotNumberAdjLotNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumberAdjLotNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumberAdjLotNumber (" + dr["LotNumberAdjLotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("LotNumberAdjCountAdjustment"))
                                {
                                    #region Validations of ItemLine LotNumber
                                    if (dr["LotNumberAdjCountAdjustment"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumberAdjCountAdjustment"].ToString().Length > 40)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumberAdjCountAdjustment (" + dr["LotNumberAdjCountAdjustment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("LotNumberAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["LotNumberAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumberAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumberAdjInventorySiteLocationRef (" + dr["LotNumberAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                            }




                            #endregion

                            if (InventoryAdjLine.ItemRef != null || InventoryAdjLine.QuantityAdjustment != null || InventoryAdjLine.QuantityAdjustment.QuantityDifference != null || InventoryAdjLine.ValueAdjustment != null || InventoryAdjLine.SerialNumberAdjustment != null || InventoryAdjLine.LotNumberAdjustment != null)
                            {
                                InventoryAdjustment.InventoryAdjustmentLineAdd.Add(InventoryAdjLine);
                            }

                            coll.Add(InventoryAdjustment);
                        }
                        else
                        {
                            #region Inventory Adjustment Line Add
                            DataProcessingBlocks.InventoryAdjustmentLineAdd InventoryAdjLine = new InventoryAdjustmentLineAdd();
                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of Item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    InventoryAdjLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (InventoryAdjLine.ItemRef.FullName == null)
                                    {
                                        InventoryAdjLine.ItemRef.FullName = null;
                                    }
                                }
                                #endregion

                            }

                            //Create an object of Quantity adjustment only when datatable contains any one value..
                            if ((dt.Columns.Contains("QuantityAdjNewQuantity") && dr["QuantityAdjNewQuantity"].ToString() != string.Empty) || (dt.Columns.Contains("QuantityAdjDifference") && dr["QuantityAdjDifference"].ToString() != string.Empty))
                            {
                                //Bug ;; 584
                                //InventoryAdjLine.QuantityAdjustment = new QuantityAdjustment();
                                //if (dt.Columns.Contains("QuantityAdjNewQuantity"))
                                //{
                                InventoryAdjLine.QuantityAdjustment = new QuantityAdjustment();
                                if (dt.Columns.Contains("QuantityAdjNewQuantity"))
                                {
                                    #region Validations for QuantityAjustment NewQuantity
                                    if (dr["QuantityAdjNewQuantity"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["QuantityAdjNewQuantity"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This QANewQuantiy ( " + dr["QuantityAdjNewQuantity"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;

                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                        }
                                    }

                                    #endregion
                                }
                                // bug 496 axis 12.0
                                if (dt.Columns.Contains("QuantityAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["QuantityAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["QuantityAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Inventory Site Ref Full Name (" + dr["QuantityAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("QuantityAdjDifference"))
                                {
                                    #region Validations for QuantityDifference
                                    if (dr["QuantityAdjDifference"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["QuantityAdjDifference"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This QAQuantityAdjDifference ( " + dr["QuantityAdjDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                        }
                                    }

                                    #endregion
                                }
                                //}
                            }

                            //P Axis 13.1 : issue 698
                            if (dt.Columns.Contains("QuantityAdjSerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["QuantityAdjSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["QuantityAdjSerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QuantityAdjSerialNumber (" + dr["QuantityAdjSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("QuantityAdjLotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["QuantityAdjLotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["QuantityAdjLotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QuantityAdjLotNumber (" + dr["QuantityAdjLotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //P Axis 13.1 : issue 698 ENDS

                            //Create an object of value adjustment only when datable contains any one value.
                            if ((dt.Columns.Contains("ValueAdjNewQuantity") && dr["ValueAdjNewQuantity"].ToString() != string.Empty)
                                || (dt.Columns.Contains("ValueAdjNewValue") && dr["ValueAdjNewValue"].ToString() != string.Empty)
                                || (dt.Columns.Contains("ValueAdjQuantityDifference") && dr["ValueAdjQuantityDifference"].ToString() != string.Empty)
                                || (dt.Columns.Contains("ValueAdjValueDifference") && dr["ValueAdjValueDifference"].ToString() != string.Empty))
                            {
                                InventoryAdjLine.ValueAdjustment = new ValueAdjustment();
                                if (dt.Columns.Contains("ValueAdjNewQuantity"))
                                {
                                    #region Validations for Value Adjustment NewQuantity
                                    if (dr["ValueAdjNewQuantity"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjNewQuantity"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjNewQuantity ( " + dr["ValueAdjNewQuantity"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                        }
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ValueAdjQuantityDifference"))
                                {
                                    #region Validations for Value Adjustment Quantity Difference
                                    if (dr["ValueAdjQuantityDifference"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjQuantityDifference"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjQuantityDifference ( " + dr["ValueAdjQuantityDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("ValueAdjNewValue"))
                                {
                                    #region Validations for ValueAdjNewValue
                                    if (dr["ValueAdjNewValue"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjNewValue"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjNewValue ( " + dr["ValueAdjNewValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.ValueAdjustment.NewValue = string.Format("{0:0.00}", Convert.ToDouble(dr["ValueAdjNewValue"].ToString()));
                                        }
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ValueAdjValueDifference"))
                                {
                                    #region Validations for ValueDifference
                                    if (dr["ValueAdjValueDifference"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ValueAdjValueDifference"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ValueAdjValueDifference ( " + dr["ValueAdjValueDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                    if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                    {
                                                        InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //P Axis 13.1 : issue 698
                                            //InventoryAdjLine.ValueAdjustment.ValueDifference = string.Format("{0:000000.00}", Convert.ToDouble(dr["ValueAdjValueDifference"].ToString()));
                                            InventoryAdjLine.ValueAdjustment.ValueDifference = Math.Round(Convert.ToDouble(dr["ValueAdjValueDifference"].ToString()), 5).ToString();
                                        }
                                    }
                                    #endregion
                                }
                            }

                            if ((dt.Columns.Contains("SerialNumberAdjAddSerialNumber") && dr["SerialNumberAdjAddSerialNumber"].ToString() != string.Empty)
                             || (dt.Columns.Contains("SerialNumberAdjRemoveSerialNumber") && dr["SerialNumberAdjRemoveSerialNumber"].ToString() != string.Empty))
                            {
                                InventoryAdjLine.SerialNumberAdjustment = new SerialNumberAdjustment();

                                if (dt.Columns.Contains("SerialNumberAdjAddSerialNumber"))
                                {
                                    #region Validations of SerialNumberAdjAddSerialNumber

                                    if (dr["SerialNumberAdjAddSerialNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumberAdjAddSerialNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumberAdjAddSerialNumber (" + dr["SerialNumberAdjAddSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("SerialNumberAdjRemoveSerialNumber"))
                                {
                                    #region Validations of SerialNumberAdjAddSerialNumber

                                    if (dr["SerialNumberAdjRemoveSerialNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumberAdjRemoveSerialNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumberAdjRemoveSerialNumber (" + dr["SerialNumberAdjRemoveSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("SerialNumberAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["SerialNumberAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumberAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumberAdj InventorySiteLocationRef (" + dr["SerialNumberAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }


                            }

                            if ((dt.Columns.Contains("LotNumberAdjLotNumber") && dr["LotNumberAdjLotNumber"].ToString() != string.Empty)
                              || (dt.Columns.Contains("LotNumberAdjCountAdjustment") && dr["LotNumberAdjCountAdjustment"].ToString() != string.Empty))
                            {
                                InventoryAdjLine.LotNumberAdjustment = new LotNumberAdjustment();

                                if (dt.Columns.Contains("LotNumberAdjLotNumber"))
                                {
                                    #region Validations of SerialNumberAdjAddSerialNumber

                                    if (dr["LotNumberAdjLotNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumberAdjLotNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumberAdjLotNumber (" + dr["LotNumberAdjLotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("LotNumberAdjCountAdjustment"))
                                {
                                    #region Validations of ItemLine LotNumber
                                    if (dr["LotNumberAdjCountAdjustment"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumberAdjCountAdjustment"].ToString().Length > 40)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumberAdjCountAdjustment (" + dr["LotNumberAdjCountAdjustment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("LotNumberAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["LotNumberAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumberAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumberAdjInventorySiteLocationRef (" + dr["LotNumberAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }

                            }
                          
                            #endregion

                            if (InventoryAdjLine.ItemRef != null || InventoryAdjLine.QuantityAdjustment.NewQuantity != null || InventoryAdjLine.QuantityAdjustment.QuantityDifference != null || InventoryAdjLine.ValueAdjustment.NewQuantity != null || InventoryAdjLine.ValueAdjustment.NewValue != null || InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber != null || InventoryAdjLine.LotNumberAdjustment.LotNumber!= null)
                            {
                                InventoryAdjustment.InventoryAdjustmentLineAdd.Add(InventoryAdjLine);
                            }
                        }


                        #endregion
                    }
                    else
                    {
                        InventoryAdjustmentEntry InventoryAdjustment = new InventoryAdjustmentEntry();

                        #region without adding Ref Number
                        if (dt.Columns.Contains("AccountRefFullName"))
                        {
                            #region Validations of AccountRef Full Name
                            if (dr["AccountRefFullName"].ToString() != string.Empty)
                            {
                                string strAccountRef = dr["AccountRefFullName"].ToString();
                                if (strAccountRef.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                            if (InventoryAdjustment.AccountRef.FullName == null)
                                            {
                                                InventoryAdjustment.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                            if (InventoryAdjustment.AccountRef.FullName == null)
                                            {
                                                InventoryAdjustment.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                        if (InventoryAdjustment.AccountRef.FullName == null)
                                        {
                                            InventoryAdjustment.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    InventoryAdjustment.AccountRef = new AccountRef(dr["AccountRefFullName"].ToString());
                                    if (InventoryAdjustment.AccountRef.FullName == null)
                                    {
                                        InventoryAdjustment.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out InventoryAddDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjustment.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjustment.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjustment.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        InventoryAddDt = dttest;
                                        InventoryAdjustment.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    InventoryAddDt = Convert.ToDateTime(datevalue);
                                    InventoryAdjustment.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");

                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref Number

                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["RefNumber"].ToString();
                                if (strRefNum.Length > 11)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                                    }
                                }
                                else
                                    InventoryAdjustment.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("InventorySiteRefFullName"))
                        {
                            #region Validations of InventorySiteRef Full Name
                            if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                            {
                                string strTerms = dr["InventorySiteRefFullName"].ToString();
                                if (strTerms.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This InventorySiteRef FullName is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                            {
                                                InventoryAdjustment.InventorySiteRef.FullName = null;
                                            }
                                            else
                                            {
                                                InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString().Substring(0, 1000));
                                            }

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                            {
                                                InventoryAdjustment.InventorySiteRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                        {
                                            InventoryAdjustment.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    InventoryAdjustment.InventorySiteRef = new InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                    if (InventoryAdjustment.InventorySiteRef.FullName == null)
                                    {
                                        InventoryAdjustment.InventorySiteRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        //if (dt.Columns.Contains("InventorySiteLocationRef"))
                        //{
                        //    #region Validations of InventorySiteLocationRef

                        //    if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                        //    {
                        //        if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                        //        {
                        //            if (isIgnoreAll == false)
                        //            {
                        //                string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                        //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                        //                if (Convert.ToString(result) == "Cancel")
                        //                {
                        //                    continue;
                        //                }
                        //                if (Convert.ToString(result) == "No")
                        //                {
                        //                    return null;
                        //                }
                        //                if (Convert.ToString(result) == "Ignore")
                        //                {
                        //                    InventoryAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                        //                    if (InventoryAdjustment.InventorySiteLocationRef.FullName == null)
                        //                        InventoryAdjustment.InventorySiteLocationRef.FullName = null;
                        //                }
                        //                if (Convert.ToString(result) == "Abort")
                        //                {
                        //                    isIgnoreAll = true;
                        //                    InventoryAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                        //                    if (InventoryAdjustment.InventorySiteLocationRef.FullName == null)
                        //                        InventoryAdjustment.InventorySiteLocationRef.FullName = null;
                        //                }
                        //            }
                        //            else
                        //            {
                        //                InventoryAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                        //                if (InventoryAdjustment.InventorySiteLocationRef.FullName == null)
                        //                    InventoryAdjustment.InventorySiteLocationRef.FullName = null;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            InventoryAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                        //            if (InventoryAdjustment.InventorySiteLocationRef.FullName == null)
                        //                InventoryAdjustment.InventorySiteLocationRef.FullName = null;
                        //        }
                        //    }
                        //    #endregion
                        //}

                        if (dt.Columns.Contains("CustomerRefFullName"))
                        {
                            #region Validations of CustomerRef Full name
                            if (dr["CustomerRefFullName"].ToString() != string.Empty)
                            {
                                string strTerms = dr["CustomerRefFullName"].ToString();
                                if (strTerms.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomerRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (InventoryAdjustment.CustomerRef.FullName == null)
                                            {
                                                InventoryAdjustment.CustomerRef.FullName = null;
                                            }
                                            else
                                            {
                                                InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (InventoryAdjustment.CustomerRef.FullName == null)
                                            {
                                                InventoryAdjustment.CustomerRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (InventoryAdjustment.CustomerRef.FullName == null)
                                        {
                                            InventoryAdjustment.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    InventoryAdjustment.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                    if (InventoryAdjustment.CustomerRef.FullName == null)
                                    {
                                        InventoryAdjustment.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ClassRefFullName"))
                        {
                            #region Validations of ClassRef Full name
                            if (dr["ClassRefFullName"].ToString() != string.Empty)
                            {
                                string strTerms = dr["ClassRefFullName"].ToString();
                                if (strTerms.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ClassRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                            if (InventoryAdjustment.ClassRef.FullName == null)
                                            {
                                                InventoryAdjustment.ClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0,1000));
                                            }

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                            if (InventoryAdjustment.ClassRef.FullName == null)
                                            {
                                                InventoryAdjustment.ClassRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (InventoryAdjustment.ClassRef.FullName == null)
                                        {
                                            InventoryAdjustment.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    InventoryAdjustment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                    if (InventoryAdjustment.ClassRef.FullName == null)
                                    {
                                        InventoryAdjustment.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString().Substring(0,4000);
                                            InventoryAdjustment.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            InventoryAdjustment.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        InventoryAdjustment.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    InventoryAdjustment.Memo = strMemo;
                                }
                            }

                            #endregion

                        }


                        #region Inventory Adjustment Line Add
                        DataProcessingBlocks.InventoryAdjustmentLineAdd InventoryAdjLine = new InventoryAdjustmentLineAdd();
                        if (dt.Columns.Contains("ItemRefFullName"))
                        {
                            #region Validations of Item Full name
                            if (dr["ItemRefFullName"].ToString() != string.Empty)
                            {
                                InventoryAdjLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                //string strCustomerFullname = string.Empty;
                                if (InventoryAdjLine.ItemRef.FullName == null)
                                {
                                    InventoryAdjLine.ItemRef.FullName = null;
                                }
                            }
                            #endregion

                        }

                        //Create an object of Quantity adjustment only when datatable contains any one value..
                        if ((dt.Columns.Contains("QuantityAdjNewQuantity") && dr["QuantityAdjNewQuantity"].ToString() != string.Empty) || (dt.Columns.Contains("QuantityAdjDifference") && dr["QuantityAdjDifference"].ToString() != string.Empty))
                        {
                           
                                InventoryAdjLine.QuantityAdjustment = new QuantityAdjustment();
                            if (dt.Columns.Contains("QuantityAdjNewQuantity"))
                            {
                                #region Validations for QuantityAjustment NewQuantity
                                if (dr["QuantityAdjNewQuantity"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["QuantityAdjNewQuantity"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QANewQuantiy ( " + dr["QuantityAdjNewQuantity"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                            if (InventoryAdjLine.QuantityAdjustment.NewQuantity == null)
                                            {
                                                InventoryAdjLine.QuantityAdjustment.NewQuantity = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //P Axis 13.1 : issue 698
                                        InventoryAdjLine.QuantityAdjustment.NewQuantity = dr["QuantityAdjNewQuantity"].ToString();
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("QuantityAdjDifference"))
                            {
                                #region Validations for QuantityDifference
                                if (dr["QuantityAdjDifference"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["QuantityAdjDifference"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This QAQuantityAdjDifference ( " + dr["QuantityAdjDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                            if (InventoryAdjLine.QuantityAdjustment.QuantityDifference == null)
                                            {
                                                InventoryAdjLine.QuantityAdjustment.QuantityDifference = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //P Axis 13.1 : issue 698
                                        InventoryAdjLine.QuantityAdjustment.QuantityDifference = dr["QuantityAdjDifference"].ToString();
                                    }
                                }

                                #endregion
                            }

                                if (dt.Columns.Contains("QuantityAdjSerialNumber"))
                                {
                                    #region Validations for QuantityAdjSerialNumber
                                    if (dr["QuantityAdjSerialNumber"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["QuantityAdjSerialNumber"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This QuantityAdjSerialNumber ( " + dr["QuantityAdjSerialNumber"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.SerialNumber == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.SerialNumber = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.SerialNumber == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.SerialNumber = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.SerialNumber == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.SerialNumber = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.SerialNumber = dr["QuantityAdjSerialNumber"].ToString();
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("QuantityAdjLotNumber"))
                                {
                                    #region Validations for QuantityAdjSerialNumber
                                    if (dr["QuantityAdjLotNumber"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["QuantityAdjLotNumber"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This QuantityAdjLotNumber ( " + dr["QuantityAdjLotNumber"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.LotNumber == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.LotNumber = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                                    if (InventoryAdjLine.QuantityAdjustment.LotNumber == null)
                                                    {
                                                        InventoryAdjLine.QuantityAdjustment.LotNumber = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                                if (InventoryAdjLine.QuantityAdjustment.LotNumber == null)
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.LotNumber = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.LotNumber = dr["QuantityAdjLotNumber"].ToString();
                                        }
                                    }

                                    #endregion
                                }

                                
                                if (dt.Columns.Contains("QuantityAdjInventorySiteLocationRef"))
                                {
                                    #region Validations of InventorySiteLocationRef

                                    if (dr["QuantityAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                    {
                                        if (dr["QuantityAdjInventorySiteLocationRef"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Inventory Site Ref Full Name (" + dr["QuantityAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                    if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                        InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["QuantityAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.QuantityAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    #endregion
                                }
                        }

                        //Create an object of value adjustment only when datable contains any one value.
                        if ((dt.Columns.Contains("ValueAdjNewQuantity") && dr["ValueAdjNewQuantity"].ToString() != string.Empty)
                            || (dt.Columns.Contains("ValueAdjNewValue") && dr["ValueAdjNewValue"].ToString() != string.Empty)
                            || (dt.Columns.Contains("ValueAdjQuantityDifference") && dr["ValueAdjQuantityDifference"].ToString() != string.Empty)
                            || (dt.Columns.Contains("ValueAdjValueDifference") && dr["ValueAdjValueDifference"].ToString() != string.Empty))
                        {
                            InventoryAdjLine.ValueAdjustment = new ValueAdjustment();
                            if (dt.Columns.Contains("ValueAdjNewQuantity"))
                            {
                                #region Validations for Value Adjustment NewQuantity
                                if (dr["ValueAdjNewQuantity"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ValueAdjNewQuantity"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ValueAdjNewQuantity ( " + dr["ValueAdjNewQuantity"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                            if (InventoryAdjLine.ValueAdjustment.NewQuantity == null)
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewQuantity = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //P Axis 13.1 : issue 698
                                        InventoryAdjLine.ValueAdjustment.NewQuantity = dr["ValueAdjNewQuantity"].ToString();
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ValueAdjQuantityDifference"))
                            {
                                #region Validations for Value Adjustment Quantity Difference
                                if (dr["ValueAdjQuantityDifference"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ValueAdjQuantityDifference"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ValueAdjQuantityDifference ( " + dr["ValueAdjQuantityDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                            if (InventoryAdjLine.ValueAdjustment.QuantityDifference == null)
                                            {
                                                InventoryAdjLine.ValueAdjustment.QuantityDifference = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //P Axis 13.1 : issue 698
                                        InventoryAdjLine.ValueAdjustment.QuantityDifference = dr["ValueAdjQuantityDifference"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ValueAdjNewValue"))
                            {
                                #region Validations for ValueAdjNewValue
                                if (dr["ValueAdjNewValue"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ValueAdjNewValue"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ValueAdjNewValue ( " + dr["ValueAdjNewValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.NewValue = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.ValueAdjustment.NewValue = dr["ValueAdjNewValue"].ToString();
                                            if (InventoryAdjLine.ValueAdjustment.NewValue == null)
                                            {
                                                InventoryAdjLine.ValueAdjustment.NewValue = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.ValueAdjustment.NewValue = string.Format("{0:0.00}", Convert.ToDouble(dr["ValueAdjNewValue"].ToString()));
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ValueAdjValueDifference"))
                            {
                                #region Validations for ValueDifference
                                if (dr["ValueAdjValueDifference"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ValueAdjValueDifference"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ValueAdjValueDifference ( " + dr["ValueAdjValueDifference"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                                if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                                {
                                                    InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.ValueAdjustment.ValueDifference = dr["ValueAdjValueDifference"].ToString();
                                            if (InventoryAdjLine.ValueAdjustment.ValueDifference == null)
                                            {
                                                InventoryAdjLine.ValueAdjustment.ValueDifference = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //P Axis 13.1 : issue 698
                                        //InventoryAdjLine.ValueAdjustment.ValueDifference = string.Format("{0:000000.00}", Convert.ToDouble(dr["ValueAdjValueDifference"].ToString()));
                                        InventoryAdjLine.ValueAdjustment.ValueDifference=Math.Round(Convert.ToDouble(dr["ValueAdjValueDifference"].ToString()), 5).ToString();
                                    }
                                }
                                #endregion
                            }
                        }

                        if ((dt.Columns.Contains("SerialNumberAdjAddSerialNumber") && dr["SerialNumberAdjAddSerialNumber"].ToString() != string.Empty)
                         || (dt.Columns.Contains("SerialNumberAdjRemoveSerialNumber") && dr["SerialNumberAdjRemoveSerialNumber"].ToString() != string.Empty))
                        {
                            InventoryAdjLine.SerialNumberAdjustment = new SerialNumberAdjustment();

                            if (dt.Columns.Contains("SerialNumberAdjAddSerialNumber"))
                            {
                                #region Validations of SerialNumberAdjAddSerialNumber

                                if (dr["SerialNumberAdjAddSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumberAdjAddSerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumberAdjAddSerialNumber (" + dr["SerialNumberAdjAddSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.SerialNumberAdjustment.AddSerialNumber = dr["SerialNumberAdjAddSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumberAdjRemoveSerialNumber"))
                            {
                                #region Validations of SerialNumberAdjAddSerialNumber

                                if (dr["SerialNumberAdjRemoveSerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumberAdjRemoveSerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumberAdjRemoveSerialNumber (" + dr["SerialNumberAdjRemoveSerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.SerialNumberAdjustment.RemoveSerialNumber = dr["SerialNumberAdjRemoveSerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SerialNumberAdjInventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["SerialNumberAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumberAdjInventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumberAdj InventorySiteLocationRef (" + dr["SerialNumberAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["SerialNumberAdjInventorySiteLocationRef"].ToString());
                                        if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                            InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }


                        }

                        if ((dt.Columns.Contains("LotNumberAdjLotNumber") && dr["LotNumberAdjLotNumber"].ToString() != string.Empty)
                          || (dt.Columns.Contains("LotNumberAdjCountAdjustment") && dr["LotNumberAdjCountAdjustment"].ToString() != string.Empty))
                        {
                            InventoryAdjLine.LotNumberAdjustment = new LotNumberAdjustment();

                            if (dt.Columns.Contains("LotNumberAdjLotNumber"))
                            {
                                #region Validations of LotNumberAdjLotNumber

                                if (dr["LotNumberAdjLotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumberAdjLotNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumberAdjLotNumber (" + dr["LotNumberAdjLotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.LotNumberAdjustment.LotNumber = dr["LotNumberAdjLotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumberAdjCountAdjustment"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["LotNumberAdjCountAdjustment"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumberAdjCountAdjustment"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumberAdjCountAdjustment (" + dr["LotNumberAdjCountAdjustment"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.LotNumberAdjustment.CountAdjustment = dr["LotNumberAdjCountAdjustment"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumberAdjInventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["LotNumberAdjInventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumberAdjInventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumberAdjInventorySiteLocationRef (" + dr["LotNumberAdjInventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                                if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                    InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                            if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                                InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InventoryAdjLine.LotNumberAdjustment.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["LotNumberAdjInventorySiteLocationRef"].ToString());
                                        if (InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName == null)
                                            InventoryAdjLine.SerialNumberAdjustment.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                        }

                        #endregion

                        if (InventoryAdjLine.ItemRef != null || InventoryAdjLine.QuantityAdjustment.NewQuantity != null || InventoryAdjLine.QuantityAdjustment.QuantityDifference != null || InventoryAdjLine.ValueAdjustment.NewQuantity != null || InventoryAdjLine.ValueAdjustment.NewValue != null)
                        {
                            InventoryAdjustment.InventoryAdjustmentLineAdd.Add(InventoryAdjLine);
                        }

                        coll.Add(InventoryAdjustment);
                        #endregion
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Customer and Item Requests
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    listCount++;
                   if (CommonUtilities.GetInstance().SkipListFlag == false)
                   {
                    if (dt.Columns.Contains("CustomerRefFullName"))
                    {
                        if (dr["CustomerRefFullName"].ToString() != string.Empty)
                        {
                            string customerName = dr["CustomerRefFullName"].ToString();
                            string[] arr = new string[15];
                            if (customerName.Contains(":"))
                            {
                                arr = customerName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["CustomerRefFullName"].ToString();
                            }
                            #region Set Customer Query
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                            XmlDocument pxmldoc = new XmlDocument();
                            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                            pxmldoc.AppendChild(qbXML);
                            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                            qbXML.AppendChild(qbXMLMsgsRq);
                            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                            XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                            qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                            CustomerQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                            CustomerQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        CustomerQueryRq.AppendChild(FullName);
                                    }
                            string pinput = pxmldoc.OuterXml;
                            string resp = string.Empty;
                            try
                            {
                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                            }
                            catch (Exception ex)
                            {
                                CommonUtilities.WriteErrorLog(ex.Message);
                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                            }
                            finally
                            {
                                if (resp != string.Empty)
                                {

                                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                    outputXMLDoc.LoadXml(resp);
                                    string statusSeverity = string.Empty;
                                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                    {
                                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                    }
                                    outputXMLDoc.RemoveAll();
                                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                                    {

                                        #region Customer Add Query

                                        XmlDocument xmldocadd = new XmlDocument();
                                        xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                        xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                        xmldocadd.AppendChild(qbXMLcust);
                                        XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                        qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                        qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                        qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                        CustomerAddRq.SetAttribute("requestID", "1");
                                        XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                        CustomerAddRq.AppendChild(CustomerAdd);
                                        XmlElement Name = xmldocadd.CreateElement("Name");
                                                Name.InnerText = arr[i];
                                        CustomerAdd.AppendChild(Name);

                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                            CustomerAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }

                                        #region Adding Bill Address of Customer.
                                        if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                            (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                        {
                                            XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                            CustomerAdd.AppendChild(BillAddress);
                                            if (dt.Columns.Contains("BillAddr1"))
                                            {

                                                if (dr["BillAddr1"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                    BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                    BillAddress.AppendChild(BillAdd1);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillAddr2"))
                                            {
                                                if (dr["BillAddr2"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                    BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                    BillAddress.AppendChild(BillAdd2);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillAddr3"))
                                            {
                                                if (dr["BillAddr3"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                    BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                    BillAddress.AppendChild(BillAdd3);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillAddr4"))
                                            {
                                                if (dr["BillAddr4"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                    BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                    BillAddress.AppendChild(BillAdd4);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillAddr5"))
                                            {
                                                if (dr["BillAddr5"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                    BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                    BillAddress.AppendChild(BillAdd5);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillCity"))
                                            {
                                                if (dr["BillCity"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillCity = xmldocadd.CreateElement("City");
                                                    BillCity.InnerText = dr["BillCity"].ToString();
                                                    BillAddress.AppendChild(BillCity);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillState"))
                                            {
                                                if (dr["BillState"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillState = xmldocadd.CreateElement("State");
                                                    BillState.InnerText = dr["BillState"].ToString();
                                                    BillAddress.AppendChild(BillState);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillPostalCode"))
                                            {
                                                if (dr["BillPostalCode"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                    BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                    BillAddress.AppendChild(BillPostalCode);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillCountry"))
                                            {
                                                if (dr["BillCountry"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                    BillCountry.InnerText = dr["BillCountry"].ToString();
                                                    BillAddress.AppendChild(BillCountry);
                                                }
                                            }
                                            if (dt.Columns.Contains("BillNote"))
                                            {
                                                if (dr["BillNote"].ToString() != string.Empty)
                                                {
                                                    XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                    BillNote.InnerText = dr["BillNote"].ToString();
                                                    BillAddress.AppendChild(BillNote);
                                                }
                                            }

                                        }

                                        #endregion

                                        #region Adding Ship Address of Customer.

                                        if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                          (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                        {
                                            XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                            CustomerAdd.AppendChild(ShipAddress);
                                            if (dt.Columns.Contains("ShipAddr1"))
                                            {

                                                if (dr["ShipAddr1"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                    ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                    ShipAddress.AppendChild(ShipAdd1);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipAddr2"))
                                            {
                                                if (dr["ShipAddr2"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                    ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                    ShipAddress.AppendChild(ShipAdd2);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipAddr3"))
                                            {
                                                if (dr["ShipAddr3"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                    ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                    ShipAddress.AppendChild(ShipAdd3);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipAddr4"))
                                            {
                                                if (dr["ShipAddr4"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                    ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                    ShipAddress.AppendChild(ShipAdd4);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipAddr5"))
                                            {
                                                if (dr["ShipAddr5"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                    ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                    ShipAddress.AppendChild(ShipAdd5);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipCity"))
                                            {
                                                if (dr["ShipCity"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                    ShipCity.InnerText = dr["ShipCity"].ToString();
                                                    ShipAddress.AppendChild(ShipCity);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipState"))
                                            {
                                                if (dr["ShipState"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipState = xmldocadd.CreateElement("State");
                                                    ShipState.InnerText = dr["ShipState"].ToString();
                                                    ShipAddress.AppendChild(ShipState);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipPostalCode"))
                                            {
                                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                    ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                    ShipAddress.AppendChild(ShipPostalCode);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipCountry"))
                                            {
                                                if (dr["ShipCountry"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                    ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                    ShipAddress.AppendChild(ShipCountry);
                                                }
                                            }
                                            if (dt.Columns.Contains("ShipNote"))
                                            {
                                                if (dr["ShipNote"].ToString() != string.Empty)
                                                {
                                                    XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                    ShipNote.InnerText = dr["ShipNote"].ToString();
                                                    ShipAddress.AppendChild(ShipNote);
                                                }
                                            }


                                        }

                                        #endregion

                                        string custinput = xmldocadd.OuterXml;
                                        string respcust = string.Empty;
                                        try
                                        {
                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (respcust != string.Empty)
                                            {
                                                System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                outputcustXMLDoc.LoadXml(respcust);
                                                foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                {
                                                    string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                    if (statusSeveritycust == "Error")
                                                    {
                                                        string msg = "New Customer could not be created into QuickBooks \n ";
                                                        msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                        //MessageBox.Show(msg);
                                                        //Task 1435 (Axis 6.0):
                                                        ErrorSummary summary = new ErrorSummary(msg);
                                                        summary.ShowDialog();
                                                        CommonUtilities.WriteErrorLog(msg);
                                                    }
                                                }
                                            }
                                        }
                                        #endregion

                                    }
                                }

                            }

                                }
                            #endregion
                        }
                        }
                    }

                    //Solution for BUG 633
                    if (dt.Columns.Contains("ItemRefFullName"))
                    {
                        if (dr["ItemRefFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemRefFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemRefFullName"].ToString();
                            }

                            #region Set Item Query

                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");

                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];                                                                      
                                        ItemQueryRq.AppendChild(FullName);
                                    }
                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {

                                                if (defaultSettings.Type == "NonInventoryPart")
                                                {
                                                    #region Item NonInventory Add Query

                                                    XmlDocument ItemNonInvendoc = new XmlDocument();
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                    ItemNonInvendoc.AppendChild(qbXMLINI);
                                                    XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                    qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                    qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                    ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                    ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                    XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                    ININame.InnerText = arr[i];
                                                    //ININame.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemNonInventoryAdd.AppendChild(ININame);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }

                                                    //Adding Tax Code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                            ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                        INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                        //INIFullName.InnerText = "Sales";
                                                        INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                                        IsPresent = true;
                                                    }

                                                    if (IsPresent == true)
                                                    {
                                                        ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                    }

                                                    string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;                                                  
                                                    string respItemNonInvendoc = string.Empty;
                                                    try
                                                    {
                                                        respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemNonInvendoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strtest2 = respItemNonInvendoc;

                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "Service")
                                                {
                                                    #region Item Service Add Query

                                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                  
                                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemServiceAdd.AppendChild(NameIS);


                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }


                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                        }
                                                    }


                                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                   
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                        //Adding IncomeAccount FullName.
                                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                    //ItemServiceAdddoc.Save("C://ItemServiceAdddoc.xml");
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemInventoryAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                ItemInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }
                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    //Adding IncomeAccountRef
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                    }

                                                    //Adding COGSAccountRef
                                                    if (defaultSettings.COGSAccount != string.Empty)
                                                    {
                                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                    }

                                                    //Adding AssetAccountRef
                                                    if (defaultSettings.AssetAccount != string.Empty)
                                                    {
                                                        XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                        XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                        INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                    }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest4 = respItemInventoryAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }
                }
            }
                else { return null; }
            }
            #endregion

            return coll;
        }
    }
}
