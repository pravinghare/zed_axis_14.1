using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemPaymentClass
    {
        private static ImportItemPaymentClass m_ImportItemPaymentClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemPaymentClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemPayment class
        /// </summary>
        /// <returns></returns>
        public static ImportItemPaymentClass GetInstance()
        {
            if (m_ImportItemPaymentClass == null)
                m_ImportItemPaymentClass = new ImportItemPaymentClass();
            return m_ImportItemPaymentClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemPayment and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemPayment QuickBooks collection </returns>
        public DataProcessingBlocks.ItemPaymentQBEntryCollection ImportItemPaymentData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemPayment Entry collections.
            DataProcessingBlocks.ItemPaymentQBEntryCollection coll = new ItemPaymentQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemPayment Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemPaymentDt = new DateTime();
                    string datevalue = string.Empty;

                    //ItemPayment Validation
                    DataProcessingBlocks.ItemPaymentQBEntry ItemPayment = new ItemPaymentQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemPayment.Name = dr["Name"].ToString().Substring(0, 31);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemPayment.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemPayment.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                ItemPayment.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("BarCodeValue"))
                    {
                        #region Validations of BarCodeValue
                        if (dr["BarCodeValue"].ToString() != string.Empty)
                        {
                            if (dr["BarCodeValue"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BarCodeValue (" + dr["BarCodeValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemPayment.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemPayment.BarCode.BarCodeValue == null)
                                            ItemPayment = null;
                                        else
                                            ItemPayment.BarCode = new BarCode(dr["BarCodeValue"].ToString().Substring(0, 50));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemPayment.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemPayment.BarCode.BarCodeValue == null)
                                            ItemPayment = null;
                                    }
                                }
                                else
                                {
                                    ItemPayment.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                }
                            }
                            else
                            {
                                ItemPayment.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                ItemPayment.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    ItemPayment.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        ItemPayment.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemPayment.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemPayment.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemPayment.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 651
                    if (dt.Columns.Contains("ClassRefFullName"))
                    {
                        #region Validations of Class FullName
                        if (dr["ClassRefFullName"].ToString() != string.Empty)
                        {
                            if (dr["ClassRefFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class FullName (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemPayment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemPayment.ClassRef.FullName == null)
                                            ItemPayment.ClassRef.FullName = null;
                                        else
                                            ItemPayment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemPayment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemPayment.ClassRef.FullName == null)
                                            ItemPayment.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemPayment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                    if (ItemPayment.ClassRef.FullName == null)
                                        ItemPayment.ClassRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemPayment.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                if (ItemPayment.ClassRef.FullName == null)
                                    ItemPayment.ClassRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ItemDesc"))
                    {
                        #region Validations of ItemDesc
                        if (dr["ItemDesc"].ToString() != string.Empty)
                        {
                            if (dr["ItemDesc"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemDesc (" + dr["ItemDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemPayment.ItemDesc = dr["ItemDesc"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemPayment.ItemDesc = dr["ItemDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemPayment.ItemDesc = dr["ItemDesc"].ToString();
                                }
                            }
                            else
                            {
                                ItemPayment.ItemDesc = dr["ItemDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("DepositToAccountFullName"))
                    {
                        #region Validations of DepositToAccount FullName
                        if (dr["DepositToAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["DepositToAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DepositToAccount FullName (" + dr["DepositToAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemPayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountFullName"].ToString());
                                        if (ItemPayment.DepositToAccountRef.FullName == null)
                                            ItemPayment.DepositToAccountRef.FullName = null;
                                        else
                                            ItemPayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemPayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountFullName"].ToString());
                                        if (ItemPayment.DepositToAccountRef.FullName == null)
                                            ItemPayment.DepositToAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemPayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountFullName"].ToString());
                                    if (ItemPayment.DepositToAccountRef.FullName == null)
                                        ItemPayment.DepositToAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemPayment.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountFullName"].ToString());
                                if (ItemPayment.DepositToAccountRef.FullName == null)
                                    ItemPayment.DepositToAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PaymentMethodFullName"))
                    {
                        #region Validations of PaymentMethod FullName
                        if (dr["PaymentMethodFullName"].ToString() != string.Empty)
                        {
                            if (dr["PaymentMethodFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PaymentMethod FullName (" + dr["PaymentMethodFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemPayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodFullName"].ToString());
                                        if (ItemPayment.PaymentMethodRef.FullName == null)
                                            ItemPayment.PaymentMethodRef.FullName = null;
                                        else
                                            ItemPayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemPayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodFullName"].ToString());
                                        if (ItemPayment.PaymentMethodRef.FullName == null)
                                            ItemPayment.PaymentMethodRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemPayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodFullName"].ToString());
                                    if (ItemPayment.PaymentMethodRef.FullName == null)
                                        ItemPayment.PaymentMethodRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemPayment.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodFullName"].ToString());
                                if (ItemPayment.PaymentMethodRef.FullName == null)
                                    ItemPayment.PaymentMethodRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    
                    coll.Add(ItemPayment);

                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }
        
            #endregion

            return coll;
        }

    }
}
