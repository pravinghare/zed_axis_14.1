using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportCustomerClass
    {
        private static ImportCustomerClass m_ImportCustomerClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportCustomerClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportCustomerClass GetInstance()
        {
            if (m_ImportCustomerClass == null)
                m_ImportCustomerClass = new ImportCustomerClass();
            return m_ImportCustomerClass;
        }


        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Customer QuickBooks collection </returns>
        public DataProcessingBlocks.CustomerQBEntryCollection ImportCustomerData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of Customer Entry collections.
            DataProcessingBlocks.CustomerQBEntryCollection coll = new CustomerQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime CustomerDt = new DateTime();
                    string datevalue = string.Empty;
                    
                    //Customer Validation
                    DataProcessingBlocks.CustomerQBEntry Customer = new CustomerQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Name = dr["Name"].ToString().Substring(0, 100);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                Customer.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    Customer.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Customer.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Customer.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Customer.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        #region Validations of Parent FullName
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            if (dr["ParentFullName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Parent Full Name (" + dr["ParentFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (Customer.ParentRef.FullName == null)
                                            Customer.ParentRef.FullName = null;
                                        else
                                            Customer.ParentRef = new ParentRef(dr["ParentFullName"].ToString().Substring(0, 100));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (Customer.ParentRef.FullName == null)
                                            Customer.ParentRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                    if (Customer.ParentRef.FullName == null)
                                        Customer.ParentRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                if (Customer.ParentRef.FullName == null)
                                    Customer.ParentRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CompanyName"))
                    {
                        #region Validations of CompanyName
                        if (dr["CompanyName"].ToString() != string.Empty)
                        {
                            if (dr["CompanyName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CompanyName (" + dr["CompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CompanyName = dr["CompanyName"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CompanyName = dr["CompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.CompanyName = dr["CompanyName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.CompanyName = dr["CompanyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Salutation"))
                    {
                        #region Validations of Salutation
                        if (dr["Salutation"].ToString() != string.Empty)
                        {
                            if (dr["Salutation"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Salutation (" + dr["Salutation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Salutation = dr["Salutation"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Salutation = dr["Salutation"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Salutation = dr["Salutation"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Salutation= dr["Salutation"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FirstName"))
                    {
                        #region Validations of FirstName
                        if (dr["FirstName"].ToString() != string.Empty)
                        {
                            if (dr["FirstName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.FirstName = dr["FirstName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.FirstName = dr["FirstName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MiddleName"))
                    {
                        #region Validations of MiddleName
                        if (dr["MiddleName"].ToString() != string.Empty)
                        {
                            if (dr["MiddleName"].ToString().Length > 5)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.MiddleName = dr["MiddleName"].ToString().Substring(0, 5);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.MiddleName = dr["MiddleName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LastName"))
                    {
                        #region Validations of LastName
                        if (dr["LastName"].ToString() != string.Empty)
                        {
                            if (dr["LastName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LastName (" + dr["LastName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.LastName = dr["LastName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.LastName = dr["LastName"].ToString();
                                }
                            }
                            else
                            {
                                Customer.LastName = dr["LastName"].ToString();
                            }
                        }
                        #endregion
                    }
                    QuickBookEntities.BillAddress BillAddressItem = new BillAddress();
                    if (dt.Columns.Contains("BillAddr1"))
                    {
                        #region Validations of Bill Addr1
                        if (dr["BillAddr1"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Addr1 = dr["BillAddr1"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr2"))
                    {
                        #region Validations of Bill Addr2
                        if (dr["BillAddr2"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Addr2 = dr["BillAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("BillAddr3"))
                    {
                        #region Validations of Bill Addr3
                        if (dr["BillAddr3"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr3"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Addr3 = dr["BillAddr3"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.Addr3 = dr["BillAddr3"].ToString();

                            }
                            else
                            {
                                BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr4"))
                    {
                        #region Validations of Bill Addr4
                        if (dr["BillAddr4"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr4"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Addr4 = dr["BillAddr4"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillAddr5"))
                    {
                        #region Validations of Bill Addr5
                        if (dr["BillAddr5"].ToString() != string.Empty)
                        {
                            if (dr["BillAddr5"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Add5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Addr5 = dr["BillAddr5"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                            }
                            else
                            {
                                BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillCity"))
                    {
                        #region Validations of Bill City
                        if (dr["BillCity"].ToString() != string.Empty)
                        {
                            if (dr["BillCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill City (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.City = dr["BillCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.City = dr["BillCity"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.City = dr["BillCity"].ToString();
                            }
                            else
                            {
                                BillAddressItem.City = dr["BillCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillState"))
                    {
                        #region Validations of Bill State
                        if (dr["BillState"].ToString() != string.Empty)
                        {
                            if (dr["BillState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill State (" + dr["BillState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.State = dr["BillState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.State = dr["BillState"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.State = dr["BillState"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.State = dr["BillState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillPostalCode"))
                    {
                        #region Validations of Bill Postal Code
                        if (dr["BillPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["BillPostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Postal Code (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                            }
                            else
                            {
                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillCountry"))
                    {
                        #region Validations of Bill Country
                        if (dr["BillCountry"].ToString() != string.Empty)
                        {
                            if (dr["BillCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Country (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Country = dr["BillCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Country = dr["BillCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Country = dr["BillCountry"].ToString();
                                }
                            }
                            else
                            {
                                BillAddressItem.Country = dr["BillCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillNote"))
                    {
                        #region Validations of Bill Note
                        if (dr["BillNote"].ToString() != string.Empty)
                        {
                            if (dr["BillNote"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Bill Note (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        BillAddressItem.Note = dr["BillNote"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        BillAddressItem.Note = dr["BillNote"].ToString();
                                    }
                                }
                                else
                                    BillAddressItem.Note = dr["BillNote"].ToString();
                            }
                            else
                            {
                                BillAddressItem.Note = dr["BillNote"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (BillAddressItem.Addr1 != null || BillAddressItem.Addr2 != null || BillAddressItem.Addr3 != null || BillAddressItem.Addr4 != null || BillAddressItem.Addr5 != null
                                || BillAddressItem.City != null || BillAddressItem.Country != null || BillAddressItem.PostalCode != null || BillAddressItem.State != null || BillAddressItem.Note != null)
                        Customer.BillAddress.Add(BillAddressItem);

                    QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();

                    if (dt.Columns.Contains("ShipAddr1"))
                    {
                        #region Validations of Ship Addr1
                        if (dr["ShipAddr1"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipAddr2"))
                    {
                        #region Validations of Ship Addr2
                        if (dr["ShipAddr2"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("ShipAddr3"))
                    {
                        #region Validations of Ship Addr3
                        if (dr["ShipAddr3"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr3"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                }
                            }
                            else
                            {
                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipAddr4"))
                    {
                        #region Validations of Ship Addr4
                        if (dr["ShipAddr4"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr4"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipAddr5"))
                    {
                        #region Validations of Ship Addr5
                        if (dr["ShipAddr5"].ToString() != string.Empty)
                        {
                            if (dr["ShipAddr5"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipCity"))
                    {
                        #region Validations of Ship City
                        if (dr["ShipCity"].ToString() != string.Empty)
                        {
                            if (dr["ShipCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.City = dr["ShipCity"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.City = dr["ShipCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipState"))
                    {
                        #region Validations of Ship State
                        if (dr["ShipState"].ToString() != string.Empty)
                        {
                            if (dr["ShipState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.State = dr["ShipState"].ToString();
                                }
                            }
                            else
                            {
                                ShipAddressItem.State = dr["ShipState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipPostalCode"))
                    {
                        #region Validations of Ship Postal Code
                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["ShipPostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString().Substring(0, 13);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipCountry"))
                    {
                        #region Validations of Ship Country
                        if (dr["ShipCountry"].ToString() != string.Empty)
                        {
                            if (dr["ShipCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Country = dr["ShipCountry"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipNote"))
                    {
                        #region Validations of Ship Note
                        if (dr["ShipNote"].ToString() != string.Empty)
                        {
                            if (dr["ShipNote"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipAddressItem.Note = dr["ShipNote"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                }
                                else
                                    ShipAddressItem.Note = dr["ShipNote"].ToString();
                            }
                            else
                            {
                                ShipAddressItem.Note = dr["ShipNote"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                              || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                        Customer.ShipAddress.Add(ShipAddressItem);

                    //P Axis 13.2 : issue 702
                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Phone = dr["Phone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("AltPhone"))
                    {
                        #region Validations of Alt Phone
                        if (dr["AltPhone"].ToString() != string.Empty)
                        {
                            if (dr["AltPhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltPhone (" + dr["AltPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.AltPhone = dr["AltPhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.AltPhone = dr["AltPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.AltPhone = dr["AltPhone"].ToString();
                                }
                            }
                            else
                            {
                                Customer.AltPhone = dr["AltPhone"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 1023)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Email = dr["Email"].ToString().Substring(0, 1023);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Contact"))
                    {
                        #region Validations of Contact
                        if (dr["Contact"].ToString() != string.Empty)
                        {
                            if (dr["Contact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Contact (" + dr["Contact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Contact = dr["Contact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Contact = dr["Contact"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Contact = dr["Contact"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Contact = dr["Contact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AltContact"))
                    {
                        #region Validations of AltContact
                        if (dr["AltContact"].ToString() != string.Empty)
                        {
                            if (dr["AltContact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltContact (" + dr["AltContact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.AltContact = dr["AltContact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.AltContact = dr["AltContact"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.AltContact = dr["AltContact"].ToString();
                                }
                            }
                            else
                            {
                                Customer.AltContact = dr["AltContact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CustomerTypeFullName"))
                    {
                        #region Validations of CustomerType FullName
                        if (dr["CustomerTypeFullName"].ToString() != string.Empty)
                        {
                            if (dr["CustomerTypeFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CustomerType FullName (" + dr["CustomerTypeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CustomerTypeRef = new CustomerTypeRef(dr["CustomerTypeFullName"].ToString());
                                        if (Customer.CustomerTypeRef.FullName == null)
                                            Customer.CustomerTypeRef.FullName = null;
                                        else
                                            Customer.CustomerTypeRef = new CustomerTypeRef(dr["CustomerTypeFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CustomerTypeRef = new CustomerTypeRef(dr["CustomerTypeFullName"].ToString());
                                        if (Customer.CustomerTypeRef.FullName == null)
                                            Customer.CustomerTypeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.CustomerTypeRef = new CustomerTypeRef(dr["CustomerTypeFullName"].ToString());
                                    if (Customer.CustomerTypeRef.FullName == null)
                                        Customer.CustomerTypeRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.CustomerTypeRef = new CustomerTypeRef(dr["CustomerTypeFullName"].ToString());
                                if (Customer.CustomerTypeRef.FullName == null)
                                    Customer.CustomerTypeRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TermFullName"))
                    {
                        #region Validations of Term FullName
                        if (dr["TermFullName"].ToString() != string.Empty)
                        {
                            if (dr["TermFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TermRef FullName  (" + dr["TermFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.TermsRef = new TermsRef(dr["TermFullName"].ToString());
                                        if (Customer.TermsRef.FullName == null)
                                            Customer.TermsRef.FullName = null;
                                        else
                                            Customer.TermsRef = new TermsRef(dr["TermFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.TermsRef = new TermsRef(dr["TermFullName"].ToString());
                                        if (Customer.TermsRef.FullName == null)
                                            Customer.TermsRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.TermsRef = new TermsRef(dr["TermFullName"].ToString());
                                    if (Customer.TermsRef.FullName == null)
                                        Customer.TermsRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.TermsRef = new TermsRef(dr["TermFullName"].ToString());
                                if (Customer.TermsRef.FullName == null)
                                    Customer.TermsRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesRepFullName"))
                    {
                        #region Validations of SalesRep FullName
                        if (dr["SalesRepFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesRepFullName"].ToString().Length > 5)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesRep FullName  (" + dr["SalesRepFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.SalesRepRef = new SalesRepRef(dr["SalesRepFullName"].ToString());
                                        if (Customer.SalesRepRef.FullName == null)
                                            Customer.SalesRepRef.FullName = null;
                                        else
                                            Customer.SalesRepRef = new SalesRepRef(dr["SalesRepFullName"].ToString().Substring(0, 5));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.SalesRepRef = new SalesRepRef(dr["SalesRepFullName"].ToString());
                                        if (Customer.SalesRepRef.FullName == null)
                                            Customer.SalesRepRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.SalesRepRef = new SalesRepRef(dr["SalesRepFullName"].ToString());
                                    if (Customer.SalesRepRef.FullName == null)
                                        Customer.SalesRepRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.SalesRepRef = new SalesRepRef(dr["SalesRepFullName"].ToString());
                                if (Customer.SalesRepRef.FullName == null)
                                    Customer.SalesRepRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OpenBalance"))
                    {
                        #region Validations for OpenBalance
                        if (dr["OpenBalance"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["OpenBalance"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OpenBalance ( " + dr["OpenBalance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.OpenBalance = dr["OpenBalance"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.OpenBalance = dr["OpenBalance"].ToString();
                                    }
                                }
                                else
                                    Customer.OpenBalance = dr["OpenBalance"].ToString();
                            }
                            else
                            {
                                Customer.OpenBalance = string.Format("{0:00000000.00}", Convert.ToDouble(dr["OpenBalance"].ToString()));
                            }
                        }

                        #endregion

                    }

                    if (dt.Columns.Contains("OpenBalanceDate"))
                    {
                        #region validations of OpenBalanceDate
                        if (dr["OpenBalanceDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["OpenBalanceDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out CustomerDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This OpenBalanceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Customer.OpenBalanceDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.OpenBalanceDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Customer.OpenBalanceDate = datevalue;
                                    }
                                }
                                else
                                {
                                    CustomerDt = dttest;
                                    Customer.OpenBalanceDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                CustomerDt = Convert.ToDateTime(datevalue);
                                Customer.OpenBalanceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }
                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                    {
                        if (dt.Columns.Contains("SalesTaxCodeFullName"))
                        {
                            #region Validations of SalesTaxCode FullName
                            if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTaxCode FullName  (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Customer.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Customer.SalesTaxCodeRef.FullName == null)
                                                Customer.SalesTaxCodeRef.FullName = null;
                                            else
                                                Customer.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Customer.SalesTaxCodeRef.FullName == null)
                                                Customer.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Customer.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (Customer.SalesTaxCodeRef.FullName == null)
                                            Customer.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                    if (Customer.SalesTaxCodeRef.FullName == null)
                                        Customer.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            #endregion
                        }
                    }

                    #region Checking and setting SalesTaxCode

                    if (defaultSettings == null)
                    {
                        CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return null;

                    }
                    //string IsTaxable = string.Empty;
                    string TaxRateValue = string.Empty;
                    string ItemSaleTaxFullName = string.Empty;
                    //if default settings contain checkBoxGrossToNet checked.
                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                    {
                        if (defaultSettings.GrossToNet == "1")
                        {
                            if (dt.Columns.Contains("SalesTaxCodeFullName"))
                            {
                                if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string FullName = dr["SalesTaxCodeFullName"].ToString();
                                   ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);
                                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                }
                            }
                        }
                    }
                    #endregion



                    if (dt.Columns.Contains("ItemSalesTaxFullName"))
                    {
                        #region Validations of ItemSalesTax FullName
                        if (dr["ItemSalesTaxFullName"].ToString() != string.Empty)
                        {
                            if (dr["ItemSalesTaxFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemSalesTax FullName  (" + dr["ItemSalesTaxFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty,dr["ItemSalesTaxFullName"].ToString());
                                        if (Customer.ItemSalesTaxRef.FullName == null)
                                            Customer.ItemSalesTaxRef.FullName = null;
                                        else
                                            Customer.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty,dr["ItemSalesTaxFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                        if (Customer.ItemSalesTaxRef.FullName == null)
                                            Customer.ItemSalesTaxRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty,dr["ItemSalesTaxFullName"].ToString());
                                    if (Customer.ItemSalesTaxRef.FullName == null)
                                        Customer.ItemSalesTaxRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty,dr["ItemSalesTaxFullName"].ToString());
                                if (Customer.ItemSalesTaxRef.FullName == null)
                                    Customer.ItemSalesTaxRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesTaxCountry"))
                    {
                        #region Validations of SalesTax Country
                        if (dr["SalesTaxCountry"].ToString() != string.Empty)
                        {
                            if (dr["SalesTaxCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTax Country (" + dr["SalesTaxCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.SalesTaxCountry = dr["SalesTaxCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.SalesTaxCountry = dr["SalesTaxCountry"].ToString();
                                    }
                                }
                                else
                                    Customer.SalesTaxCountry = dr["SalesTaxCountry"].ToString();
                            }
                            else
                            {
                                Customer.SalesTaxCountry = dr["SalesTaxCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ReSaleNumber"))
                    {
                        #region Validations of ReSale Number
                        if (dr["ReSaleNumber"].ToString() != string.Empty)
                        {
                            if (dr["ReSaleNumber"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ReSale Number (" + dr["ReSaleNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.ResaleNumber = dr["ReSaleNumber"].ToString().Substring(0, 15);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.ResaleNumber = dr["ReSaleNumber"].ToString();
                                    }
                                }
                                else
                                    Customer.ResaleNumber = dr["ReSaleNumber"].ToString();
                            }
                            else
                            {
                                Customer.ResaleNumber = dr["ReSaleNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AccountNumber"))
                    {
                        #region Validations of Account Number
                        if (dr["AccountNumber"].ToString() != string.Empty)
                        {
                            if (dr["AccountNumber"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Account Number (" + dr["AccountNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.AccountNumber = dr["AccountNumber"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.AccountNumber = dr["AccountNumber"].ToString();
                                    }
                                }
                                else
                                    Customer.AccountNumber = dr["AccountNumber"].ToString();
                            }
                            else
                            {
                                Customer.AccountNumber = dr["AccountNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CreditLimit"))
                    {
                        #region Validations for CreditLimit
                        if (dr["CreditLimit"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["CreditLimit"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CreditLimit ( " + dr["CreditLimit"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CreditLimit = dr["CreditLimit"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CreditLimit = dr["CreditLimit"].ToString();
                                    }
                                }
                                else
                                    Customer.CreditLimit = dr["CreditLimit"].ToString();
                            }
                            else
                            {
                                Customer.CreditLimit = string.Format("{0:00000000.00}", Convert.ToDouble(dr["CreditLimit"].ToString()));
                            }
                        }

                        #endregion

                    }

                    if (dt.Columns.Contains("PreferredPaymentMethodFullName"))
                    {
                        #region Validations of PreferredPaymentMethod FullName
                        if (dr["PreferredPaymentMethodFullName"].ToString() != string.Empty)
                        {
                            if (dr["PreferredPaymentMethodFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PreferredPaymentMethodFullName FullName  (" + dr["PreferredPaymentMethodFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.PreferredPaymentMethodRef = new PreferredPaymentMethodRef(dr["PreferredPaymentMethodFullName"].ToString());
                                        if (Customer.PreferredPaymentMethodRef.FullName == null)
                                            Customer.PreferredPaymentMethodRef.FullName = null;
                                        else
                                            Customer.PreferredPaymentMethodRef = new PreferredPaymentMethodRef(dr["PreferredPaymentMethodFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.PreferredPaymentMethodRef = new PreferredPaymentMethodRef(dr["PreferredPaymentMethodFullName"].ToString());
                                        if (Customer.PreferredPaymentMethodRef.FullName == null)
                                            Customer.PreferredPaymentMethodRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.PreferredPaymentMethodRef = new PreferredPaymentMethodRef(dr["PreferredPaymentMethodFullName"].ToString());
                                    if (Customer.PreferredPaymentMethodRef.FullName == null)
                                        Customer.PreferredPaymentMethodRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.PreferredPaymentMethodRef = new PreferredPaymentMethodRef(dr["PreferredPaymentMethodFullName"].ToString());
                                if (Customer.PreferredPaymentMethodRef.FullName == null)
                                    Customer.PreferredPaymentMethodRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    QuickBookEntities.CreditCardInfo creditInfoItem = new CreditCardInfo();

                    if (dt.Columns.Contains("CreditCardInfoCreditCardNumber"))
                    {
                        #region Validations of CreditCardNumber
                        if (dr["CreditCardInfoCreditCardNumber"].ToString() != string.Empty)
                        {
                            if (dr["CreditCardInfoCreditCardNumber"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CreditCardNumber (" + dr["CreditCardInfoCreditCardNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        creditInfoItem.CreditCardNumber = dr["CreditCardInfoCreditCardNumber"].ToString().Substring(0, 25);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        creditInfoItem.CreditCardNumber = dr["CreditCardInfoCreditCardNumber"].ToString();
                                    }
                                }
                                else
                                    creditInfoItem.CreditCardNumber = dr["CreditCardInfoCreditCardNumber"].ToString();
                            }
                            else
                            {
                                creditInfoItem.CreditCardNumber = dr["CreditCardInfoCreditCardNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CreditCardInfoExpirationMonth"))
                    {
                        #region Validation ExpirationMonth

                        if (dr["CreditCardInfoExpirationMonth"].ToString() != string.Empty)
                        {
                            if (dr["CreditCardInfoExpirationMonth"].ToString().Length > 12)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ExpirationMonth (" + dr["CreditCardInfoExpirationMonth"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        creditInfoItem.ExpirationMonth = dr["CreditCardInfoExpirationMonth"].ToString().Substring(0, 12);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        creditInfoItem.ExpirationMonth = dr["CreditCardInfoExpirationMonth"].ToString();
                                    }
                                }
                                else
                                    creditInfoItem.ExpirationMonth = dr["CreditCardInfoExpirationMonth"].ToString();
                            }
                            else
                            {
                                creditInfoItem.ExpirationMonth = dr["CreditCardInfoExpirationMonth"].ToString();
                            }
                        }
                        
                        #endregion
                    }

                    if (dt.Columns.Contains("CreditCardInfoExpirationYear"))
                    {
                        #region Validation ExpirationYear

                        if (dr["CreditCardInfoExpirationYear"].ToString() != string.Empty)
                        {
                            if (dr["CreditCardInfoExpirationYear"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ExpirationYear (" + dr["CreditCardInfoExpirationYear"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        creditInfoItem.ExpirationYear = dr["CreditCardInfoExpirationYear"].ToString().Substring(0, 20);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        creditInfoItem.ExpirationYear = dr["CreditCardInfoExpirationYear"].ToString();
                                    }
                                }
                                else
                                    creditInfoItem.ExpirationYear =dr["CreditCardInfoExpirationYear"].ToString();
                            }
                            else
                            {
                                creditInfoItem.ExpirationYear = dr["CreditCardInfoExpirationYear"].ToString();
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("CreditCardInfoNameOnCard"))
                    {
                        #region Validation NameOnCard

                        if (dr["CreditCardInfoNameOnCard"].ToString() != string.Empty)
                        {
                            if (dr["CreditCardInfoNameOnCard"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This NameOnCard (" + dr["CreditCardInfoNameOnCard"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        creditInfoItem.NameOnCard = dr["CreditCardInfoNameOnCard"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        creditInfoItem.NameOnCard = dr["CreditCardInfoNameOnCard"].ToString();
                                    }
                                }
                                else
                                    creditInfoItem.NameOnCard = dr["CreditCardInfoNameOnCard"].ToString();
                            }
                            else
                            {
                                creditInfoItem.NameOnCard = dr["CreditCardInfoNameOnCard"].ToString();
                            }
                        }

                        #endregion
                    }


                    if (dt.Columns.Contains("CreditCardInfoCreditCardAddress"))
                    {
                        #region Validation CreditCardAddress

                        if (dr["CreditCardInfoCreditCardAddress"].ToString() != string.Empty)
                        {
                            if (dr["CreditCardInfoCreditCardAddress"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CreditCardAddress (" + dr["CreditCardInfoCreditCardAddress"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        creditInfoItem.CreditCardAddress = dr["CreditCardInfoCreditCardAddress"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        creditInfoItem.CreditCardAddress = dr["CreditCardInfoCreditCardAddress"].ToString();
                                    }
                                }
                                else
                                    creditInfoItem.CreditCardAddress = dr["CreditCardInfoCreditCardAddress"].ToString();
                            }
                            else
                            {
                                creditInfoItem.CreditCardAddress = dr["CreditCardInfoCreditCardAddress"].ToString();
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("CreditCardInfoCreditCardPostalCode"))
                    {
                        #region Validation CreditCardPostalCode

                        if (dr["CreditCardInfoCreditCardPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["CreditCardInfoCreditCardPostalCode"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CreditCardPostalCode (" + dr["CreditCardInfoCreditCardPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        creditInfoItem.CreditCardPostalCode = dr["CreditCardInfoCreditCardPostalCode"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        creditInfoItem.CreditCardPostalCode = dr["CreditCardInfoCreditCardPostalCode"].ToString();
                                    }
                                }
                                else
                                    creditInfoItem.CreditCardPostalCode = dr["CreditCardInfoCreditCardPostalCode"].ToString();
                            }
                            else
                            {
                                creditInfoItem.CreditCardPostalCode = dr["CreditCardInfoCreditCardPostalCode"].ToString();
                            }
                        }

                        #endregion
                    }


                    if (creditInfoItem.CreditCardNumber != null || creditInfoItem.ExpirationMonth != null || creditInfoItem.ExpirationYear != null || creditInfoItem.CreditCardAddress != null || creditInfoItem.CreditCardPostalCode != null
                              || creditInfoItem.NameOnCard != null)
                        Customer.CreditCardInfo.Add(creditInfoItem);



                    if (dt.Columns.Contains("JobStatus"))
                    {
                        #region Validations of JobStatus
                        if (dr["JobStatus"].ToString() != string.Empty)
                        {
                            try
                            {
                                Customer.JobStatus = Convert.ToString((DataProcessingBlocks.JobStatus)Enum.Parse(typeof(DataProcessingBlocks.JobStatus), dr["JobStatus"].ToString(), true));
                            }
                            catch
                            {
                                Customer.JobStatus = dr["JobStatus"].ToString();
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("JobStartDate"))
                    {
                        #region validations of JobStartDate
                        if (dr["JobStartDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["JobStartDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out CustomerDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This JobStartDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Customer.JobStartDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.JobStartDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Customer.JobStartDate = datevalue;
                                    }
                                }
                                else
                                {
                                    CustomerDt = dttest;
                                    Customer.JobStartDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                CustomerDt = Convert.ToDateTime(datevalue);
                                Customer.JobStartDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("JobProjectedEndDate"))
                    {
                        #region validations of JobProjectedEndDate
                        if (dr["JobProjectedEndDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["JobProjectedEndDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out CustomerDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This JobProjectedEndDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Customer.JobProjectedEndDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.JobProjectedEndDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Customer.JobProjectedEndDate = datevalue;
                                    }
                                }
                                else
                                {
                                    CustomerDt = dttest;
                                    Customer.JobProjectedEndDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                CustomerDt = Convert.ToDateTime(datevalue);
                                Customer.JobProjectedEndDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("JobEndDate"))
                    {
                        #region validations of JobEndDate
                        if (dr["JobEndDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["JobEndDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out CustomerDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This JobEndDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Customer.JobEndDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Customer.JobEndDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        Customer.JobEndDate = datevalue;
                                    }
                                }
                                else
                                {
                                    CustomerDt = dttest;
                                    Customer.JobEndDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                CustomerDt = Convert.ToDateTime(datevalue);
                                Customer.JobEndDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("JobDesc"))
                    {
                        #region Validations of JobDesc
                        if (dr["JobDesc"].ToString() != string.Empty)
                        {
                            if (dr["JobDesc"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JobDesc (" + dr["JobDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.JobDesc = dr["JobDesc"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.JobDesc = dr["JobDesc"].ToString();
                                    }
                                }
                                else
                                    Customer.JobDesc = dr["JobDesc"].ToString();
                            }
                            else
                            {
                                Customer.JobDesc = dr["JobDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("JobTypeFullName"))
                    {
                        #region Validations of JobType FullName
                        if (dr["JobTypeFullName"].ToString() != string.Empty)
                        {
                            if (dr["JobTypeFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JobType FullName  (" + dr["JobTypeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.JobTypeRef = new JobTypeRef(dr["JobTypeFullName"].ToString());
                                        if (Customer.JobTypeRef.FullName == null)
                                            Customer.JobTypeRef.FullName = null;
                                        else
                                            Customer.JobTypeRef = new JobTypeRef(dr["JobTypeFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.JobTypeRef = new JobTypeRef(dr["JobTypeFullName"].ToString());
                                        if (Customer.JobTypeRef.FullName == null)
                                            Customer.JobTypeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.JobTypeRef = new JobTypeRef(dr["JobTypeFullName"].ToString());
                                    if (Customer.JobTypeRef.FullName == null)
                                        Customer.JobTypeRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.JobTypeRef = new JobTypeRef(dr["JobTypeFullName"].ToString());
                                if (Customer.JobTypeRef.FullName == null)
                                    Customer.JobTypeRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["JobDNotesesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Notes = dr["Notes"].ToString().Substring(0, 4095);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                    Customer.Notes = dr["Notes"].ToString();
                            }
                            else
                            {
                                Customer.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PriceLevelFullName"))
                    {
                        #region Validations of PriceLevel FullName
                        if (dr["PriceLevelFullName"].ToString() != string.Empty)
                        {
                            if (dr["PriceLevelFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.PriceLevelRef = new PriceLevelRef(dr["PriceLevelFullName"].ToString());
                                        if (Customer.PriceLevelRef.FullName == null)
                                            Customer.PriceLevelRef.FullName = null;
                                        else
                                            Customer.PriceLevelRef = new PriceLevelRef(dr["PriceLevelFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.PriceLevelRef = new PriceLevelRef(dr["PriceLevelFullName"].ToString());
                                        if (Customer.PriceLevelRef.FullName == null)
                                            Customer.PriceLevelRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.PriceLevelRef = new PriceLevelRef(dr["PriceLevelFullName"].ToString());
                                    if (Customer.PriceLevelRef.FullName == null)
                                        Customer.PriceLevelRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.PriceLevelRef = new PriceLevelRef(dr["PriceLevelFullName"].ToString());
                                if (Customer.PriceLevelRef.FullName == null)
                                    Customer.PriceLevelRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ExternalGUID"))
                    {
                        #region Validations of ExternalGUID
                        if (dr["ExternalGUID"].ToString() != string.Empty)
                        {
                            if (dr["ExternalGUID"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ExternalGUID (" + dr["ExternalGUID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.ExternalGUID = dr["ExternalGUID"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.ExternalGUID = dr["ExternalGUID"].ToString();
                                    }
                                }
                                else
                                    Customer.ExternalGUID = dr["NotExternalGUIDes"].ToString();
                            }
                            else
                            {
                                Customer.ExternalGUID = dr["ExternalGUID"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("CurrencyFullName"))
                    {
                        #region Validations of Currency FullName
                        if (dr["CurrencyFullName"].ToString() != string.Empty)
                        {
                            if (dr["CurrencyFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Currency FullName   (" + dr["CurrencyFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                        if (Customer.CurrencyRef.FullName == null)
                                            Customer.CurrencyRef.FullName = null;
                                        else
                                            Customer.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                        if (Customer.CurrencyRef.FullName == null)
                                            Customer.CurrencyRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                    if (Customer.CurrencyRef.FullName == null)
                                        Customer.CurrencyRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                if (Customer.CurrencyRef.FullName == null)
                                    Customer.CurrencyRef.FullName = null;
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("TaxRegistrationNumber"))
                    {
                        #region Validations of TaxRegistrationNumber
                        if (dr["TaxRegistrationNumber"].ToString() != string.Empty)
                        {
                            if (dr["TaxRegistrationNumber"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxRegistrationNumber (" + dr["TaxRegistrationNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString();
                                }
                            }
                            else
                            {
                                Customer.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString();
                            }
                        }
                        #endregion

                    }


                    //Axis 331 start
                    #region additional fields

                    //1.jobtitle
                    if (dt.Columns.Contains("JobTitle"))
                    {
                        #region Validations of JobTitle
                        if (dr["JobTitle"].ToString() != string.Empty)
                        {
                            if (dr["JobTitle"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JobTitle (" + dr["JobTitle"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.JobTitle = dr["JobTitle"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.JobTitle = dr["JobTitle"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.JobTitle = dr["JobTitle"].ToString();
                                }
                            }
                            else
                            {
                                Customer.JobTitle = dr["JobTitle"].ToString();
                            }
                        }
                        #endregion
                    }

                    //  2.classs
                    if (dt.Columns.Contains("Class"))
                    {
                        #region Validations of Class FullName
                        if (dr["Class"].ToString() != string.Empty)
                        {
                            if (dr["Class"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class Full Name (" + dr["Class"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.ClassRef = new ClassRef(dr["Class"].ToString());
                                        if (Customer.ClassRef.FullName == null)
                                            Customer.ClassRef.FullName = null;
                                        else
                                            Customer.ClassRef = new ClassRef(dr["Class"].ToString().Substring(0, 100));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.ClassRef = new ClassRef(dr["Class"].ToString());
                                        if (Customer.ClassRef.FullName == null)
                                            Customer.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Customer.ClassRef = new ClassRef(dr["Class"].ToString());
                                    if (Customer.ClassRef.FullName == null)
                                        Customer.ClassRef.FullName = null;
                                }
                            }
                            else
                            {
                                Customer.ClassRef = new ClassRef(dr["Class"].ToString());
                                if (Customer.ClassRef.FullName == null)
                                    Customer.ClassRef.FullName = null;
                            }
                        }
                        #endregion
                    }
                    //3.cc

                    if (dt.Columns.Contains("Cc"))
                    {
                        #region Validations of Cc
                        if (dr["Cc"].ToString() != string.Empty)
                        {
                            if (dr["Cc"].ToString().Length > 1023)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Cc (" + dr["Cc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Cc = dr["Cc"].ToString().Substring(0, 1023);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Cc = dr["Cc"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Cc = dr["Cc"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Cc = dr["Cc"].ToString();
                            }
                        }
                        #endregion
                    }

                    //4. AdditionalContact

                    QuickBookEntities.AdditionalContactRef AdditionalContact = new AdditionalContactRef();
                    if (dt.Columns.Contains("ContactName"))
                    {
                        #region Validations of ContactName
                        if (dr["ContactName"].ToString() != string.Empty)
                        {
                            if (dr["ContactName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ContactName (" + dr["ContactName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        AdditionalContact.ContactName = dr["ContactName"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        AdditionalContact.ContactName = dr["ContactName"].ToString();
                                    }
                                }
                                else
                                {
                                    AdditionalContact.ContactName = dr["ContactName"].ToString();
                                }
                            }
                            else
                            {
                                AdditionalContact.ContactName = dr["ContactName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ContactValue"))
                    {
                        #region Validations of ContactValue
                        if (dr["ContactValue"].ToString() != string.Empty)
                        {
                            if (dr["ContactValue"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ContactValue (" + dr["ContactValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        AdditionalContact.ContactValue = dr["ContactValue"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        AdditionalContact.ContactValue = dr["ContactValue"].ToString();
                                    }
                                }
                                else
                                {
                                    AdditionalContact.ContactValue = dr["ContactValue"].ToString();
                                }
                            }
                            else
                            {
                                AdditionalContact.ContactValue = dr["ContactValue"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (AdditionalContact.ContactName != null || AdditionalContact.ContactName != null)
                        Customer.AdditionalContactRef.Add(AdditionalContact);


                    QuickBookEntities.ShipToAddress ShipToAddressItem = new ShipToAddress();

                    if (dt.Columns.Contains("ShipToAddr1"))
                    {
                        #region Validations of Ship Addr1
                        if (dr["ShipToAddr1"].ToString() != string.Empty)
                        {
                            if (dr["ShipToAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipToAddr1 (" + dr["ShipToAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Addr1 = dr["ShipToAddr1"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Addr1 = dr["ShipToAddr1"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Addr1 = dr["ShipToAddr1"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Addr1 = dr["ShipToAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToAddr2"))
                    {
                        #region Validations of Ship Addr2
                        if (dr["ShipToAddr2"].ToString() != string.Empty)
                        {
                            if (dr["ShipToAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipToAddr2 (" + dr["ShipToAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Addr2 = dr["ShipToAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        ShipToAddressItem.Addr2 = dr["ShipToAddr2"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Addr2 = dr["ShipToAddr2"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Addr2 = dr["ShipToAddr2"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("ShipToAddr3"))
                    {
                        #region Validations of Ship Addr3
                        if (dr["ShipToAddr3"].ToString() != string.Empty)
                        {
                            if (dr["ShipToAddr3"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipToAddr3 (" + dr["ShipToAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Addr3 = dr["ShipToAddr3"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Addr3 = dr["ShipToAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipToAddressItem.Addr3 = dr["ShipToAddr3"].ToString();
                                }
                            }
                            else
                            {
                                ShipToAddressItem.Addr3 = dr["ShipToAddr3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToAddr4"))
                    {
                        #region Validations of Ship Addr4
                        if (dr["ShipToAddr4"].ToString() != string.Empty)
                        {
                            if (dr["ShipToAddr4"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipToAddr4 (" + dr["ShipToAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Addr4 = dr["ShipToAddr4"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Addr4 = dr["ShipToAddr4"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Addr4 = dr["ShipToAddr4"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Addr4 = dr["ShipToAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToAddr5"))
                    {
                        #region Validations of Ship Addr5
                        if (dr["ShipToAddr5"].ToString() != string.Empty)
                        {
                            if (dr["ShipToAddr5"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ShipToAddr5 (" + dr["ShipToAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Addr5 = dr["ShipToAddr5"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Addr5 = dr["ShipToAddr5"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Addr5 = dr["ShipToAddr5"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Addr5 = dr["ShipToAddr5"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToCity"))
                    {
                        #region Validations of ShipToCity
                        if (dr["ShipToCity"].ToString() != string.Empty)
                        {
                            if (dr["ShipToCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship To City (" + dr["ShipToCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.City = dr["ShipCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.City = dr["ShipToCity"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.City = dr["ShipToCity"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.City = dr["ShipToCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToState"))
                    {
                        #region Validations of Ship State
                        if (dr["ShipToState"].ToString() != string.Empty)
                        {
                            if (dr["ShipToState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship To State (" + dr["ShipToState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.State = dr["ShipToState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.State = dr["ShipToState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipToAddressItem.State = dr["ShipToState"].ToString();
                                }
                            }
                            else
                            {
                                ShipToAddressItem.State = dr["ShipToState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToPostalCode"))
                    {
                        #region Validations of Ship Postal Code
                        if (dr["ShipToPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["ShipToPostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship Postal Code (" + dr["ShipToPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.PostalCode = dr["ShipToPostalCode"].ToString().Substring(0, 13);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.PostalCode = dr["ShipToPostalCode"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.PostalCode = dr["ShipToPostalCode"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.PostalCode = dr["ShipToPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToCountry"))
                    {
                        #region Validations of Ship Country
                        if (dr["ShipToCountry"].ToString() != string.Empty)
                        {
                            if (dr["ShipToCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship To Country (" + dr["ShipToCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Country = dr["ShipToCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Country = dr["ShipToCountry"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Country = dr["ShipToCountry"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Country = dr["ShipToCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ShipToNote"))
                    {
                        #region Validations of Ship To Note
                        if (dr["ShipToNote"].ToString() != string.Empty)
                        {
                            if (dr["ShipToNote"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship To Note (" + dr["ShipToNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Note = dr["ShipToNote"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Note = dr["ShipToNote"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Note = dr["ShipToNote"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Note = dr["ShipToNote"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("ShipToName"))
                    {
                        #region Validations of Ship To Name
                        if (dr["ShipToName"].ToString() != string.Empty)
                        {
                            if (dr["ShipToName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Ship To Name (" + dr["ShipToName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.Name = dr["ShipToName"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.Name = dr["ShipToName"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.Name = dr["ShipToName"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.Name = dr["ShipToName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("DefaultShipTo"))
                    {
                        #region Validations of DefaultShipTo
                        if (dr["DefaultShipTo"].ToString() != string.Empty)
                        {
                            if (dr["DefaultShipTo"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Default Ship To (" + dr["DefaultShipTo"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ShipToAddressItem.DefaultShipTo = dr["DefaultShipTo"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ShipToAddressItem.DefaultShipTo = dr["DefaultShipTo"].ToString();
                                    }
                                }
                                else
                                    ShipToAddressItem.DefaultShipTo = dr["DefaultShipTo"].ToString();
                            }
                            else
                            {
                                ShipToAddressItem.DefaultShipTo = dr["DefaultShipTo"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (ShipToAddressItem.Addr1 != null || ShipToAddressItem.Addr2 != null || ShipToAddressItem.Addr3 != null || ShipToAddressItem.Addr4 != null || ShipToAddressItem.Addr5 != null
                              || ShipToAddressItem.City != null || ShipToAddressItem.Country != null || ShipToAddressItem.PostalCode != null || ShipToAddressItem.State != null || ShipToAddressItem.Note != null || ShipToAddressItem.DefaultShipTo != null || ShipToAddressItem.Name != null)
                        Customer.ShipToAddress.Add(ShipToAddressItem);


                    //Axis 619
                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.Phone = dr["Phone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Customer.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                Customer.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }
                    //Axis 619 end

                    if (dt.Columns.Contains("AdditionalNotes"))
                    {
                        #region Validations of AdditionalNotes
                        if (dr["AdditionalNotes"].ToString() != string.Empty)
                        {
                            if (dr["AdditionalNotes"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AdditionalNotes  (" + dr["AdditionalNotes"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Customer.AdditionalNotes = new AdditionalNotes(dr["AdditionalNotes"].ToString());
                                        if (Customer.AdditionalNotes.Note == null)
                                            Customer.AdditionalNotes.Note = null;
                                        else
                                        {
                                            Customer.AdditionalNotes = new AdditionalNotes(dr["AdditionalNotes"].ToString().Substring(0, 159));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Customer.AdditionalNotes = new AdditionalNotes(dr["AdditionalNotes"].ToString());
                                        if (Customer.AdditionalNotes.Note == null)
                                            Customer.AdditionalNotes.Note = null;
                                    }
                                }
                                else
                                {
                                    Customer.AdditionalNotes = new AdditionalNotes(dr["AdditionalNotes"].ToString());
                                    if (Customer.AdditionalNotes.Note == null)
                                        Customer.AdditionalNotes.Note = null;
                                }
                            }
                            else
                            {
                                Customer.AdditionalNotes = new AdditionalNotes(dr["AdditionalNotes"].ToString());
                                if (Customer.AdditionalNotes.Note == null)
                                    Customer.AdditionalNotes.Note = null;
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("PreferredDeliveryMethod"))
                    {
                        #region Validations of JobStatus
                        if (dr["PreferredDeliveryMethod"].ToString() != string.Empty)
                        {
                            try
                            {
                                Customer.PreferredDeliveryMethod = Convert.ToString((DataProcessingBlocks.PreferredDeliveryMethod)Enum.Parse(typeof(DataProcessingBlocks.PreferredDeliveryMethod), dr["PreferredDeliveryMethod"].ToString(), true));
                            }
                            catch
                            {
                                Customer.PreferredDeliveryMethod = dr["PreferredDeliveryMethod"].ToString();
                            }
                        }
                        #endregion

                    }

                   
                    //Axis 331 end
                    #endregion


                    coll.Add(Customer);
                    
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (dt.Columns.Contains("ParentFullName"))
                    {

                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            string customerName = dr["ParentFullName"].ToString();
                            string[] arr = new string[15];
                            if (customerName.Contains(":"))
                            {
                                arr = customerName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ParentFullName"].ToString();
                            }

                            #region Set Customer Query
                           
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                    qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                    CustomerQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                CustomerQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        CustomerQueryRq.AppendChild(FullName);
                                    }
                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }


                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {

                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {
                                                #region Customer Add Query

                                                XmlDocument xmldocadd = new XmlDocument();
                                                xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                xmldocadd.AppendChild(qbXMLcust);
                                                XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                CustomerAddRq.SetAttribute("requestID", "1");
                                                XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                CustomerAddRq.AppendChild(CustomerAdd);

                                                XmlElement Name = xmldocadd.CreateElement("Name");
                                                Name.InnerText = arr[i];
                                                CustomerAdd.AppendChild(Name);

                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                            CustomerAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }

                                               

                                                string custinput = xmldocadd.OuterXml;
                                                string respcust = string.Empty;
                                                try
                                                {
                                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                    {
                                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                    }

                                                    else
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                }

                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                finally
                                                {
                                                    if (respcust != string.Empty)
                                                    {
                                                        System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                        outputcustXMLDoc.LoadXml(respcust);
                                                        foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                        {
                                                            string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                            if (statusSeveritycust == "Error")
                                                            {
                                                                string msg = "New Customer could not be created into QuickBooks \n ";
                                                                msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                //Task 1435 (Axis 6.0):
                                                                ErrorSummary summary = new ErrorSummary(msg);
                                                                summary.ShowDialog();
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            #endregion
                            }
                            
                        }
                    }
                }
            }


            #endregion

            #endregion
            
            return coll;
        }

    }
}
