using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.IO;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportInvoiceClass
    {
        private int cnt;
        private static ImportInvoiceClass m_ImportInvoiceClass;
        public bool isIgnoreAll = false;
        public int dt_count = 0;
        #region Constuctor

        public ImportInvoiceClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Invoice class
        /// </summary>
        /// <returns></returns>
        public static ImportInvoiceClass GetInstance()
        {
            if (m_ImportInvoiceClass == null)
                m_ImportInvoiceClass = new ImportInvoiceClass();
            return m_ImportInvoiceClass;
        }


        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Invoice QuickBooks collection </returns>
        public DataProcessingBlocks.InVoiceQBEntryCollection ImportInvoiceData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings) 
        {
            //Create an instance of Invoice Entry collections.
            DataProcessingBlocks.InVoiceQBEntryCollection coll = new InVoiceQBEntryCollection();
            isIgnoreAll = false;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For Invoice Entry

            #region Checking Validations
            DateTime InvoiceDt = new DateTime();
            string datevalue = string.Empty;
          
            int validateRowCount = 1;
            int listCount = 1;
            int refcnt = 0;

            CommonUtilities.GetInstance().ValidateFlag = false;

            foreach (DataRow dr in dt.Rows)
            {
                dt_count = dt.Rows.Count;
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);

                    try
                    {

                        bkWorker.ReportProgress(validateRowCount*100  / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                       //MessageBox.Show(ex.Message);
                    }
                  
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    if (dt.Columns.Contains("InvoiceRefNumber"))
                    {
                        #region Add Invoice Ref Number
                        InvoiceQBEntry Invoice = new InvoiceQBEntry();
                        Invoice = coll.FindInvoiceEntry(dr["InvoiceRefNumber"].ToString());

                        int datatbl_cnt = 0;
                        if (Invoice == null)
                        {
                        }
                        else
                        {

                            DataRow[] total_cnt_row = dt.Select("InvoiceRefNumber = '" + InVoiceQBEntryCollection.inv_number.ToString() + "'", "");

                            foreach (DataRow value in total_cnt_row)
                            {
                                datatbl_cnt++;
                            }

                        }

                        int row_cnt = 0;
                        DataRow[] total_cnt = dt.Select("InvoiceRefNumber = '" + InVoiceQBEntryCollection.inv_number.ToString() + "'", "");
                        foreach (DataRow value in total_cnt)
                        {
                            row_cnt++;
                        }

                        if (Invoice == null)
                        {
                            #region Invoice Null
                            Invoice = new InvoiceQBEntry();

                            if (dt.Columns.Contains("CustomerRefFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["CustomerRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CustomerRefFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (Invoice.CustomerRef.FullName == null)
                                                {
                                                    Invoice.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                                if (Invoice.CustomerRef.FullName == null)
                                                {
                                                    Invoice.CustomerRef.FullName = null;
                                                }
                                            }


                                        }
                                        else
                                        {
                                            Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (Invoice.CustomerRef.FullName == null)
                                            {
                                                Invoice.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (Invoice.CustomerRef.FullName == null)
                                        {
                                            Invoice.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }
                            ///bug 442 11.4 
                            if (dt.Columns.Contains("Currency"))
                            {
                                #region Validations of Currency Full name
                                if (dr["Currency"].ToString() != string.Empty)
                                {
                                    string strCust = dr["Currency"].ToString();
                                    if (strCust.Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Invoice.CurrencyRef.FullName == null)
                                                {
                                                    Invoice.CurrencyRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Invoice.CurrencyRef.FullName == null)
                                                {
                                                    Invoice.CurrencyRef.FullName = null;
                                                }
                                            }


                                        }
                                        else
                                        {
                                            Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Invoice.CurrencyRef.FullName == null)
                                            {
                                                Invoice.CurrencyRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (Invoice.CurrencyRef.FullName == null)
                                        {
                                            Invoice.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                                {
                                                    Invoice.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                                {
                                                    Invoice.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                            {
                                                Invoice.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                        {
                                            Invoice.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("AccountRef"))
                            {
                                #region Validations of Template Full name
                                if (dr["AccountRef"].ToString() != string.Empty)
                                {
                                    if (dr["AccountRef"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Template full name (" + dr["AccountRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                                if (Invoice.ARAccountRef.FullName == null)
                                                    Invoice.ARAccountRef.FullName = null;
                                                else
                                                    Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                                if (Invoice.ARAccountRef.FullName == null)
                                                    Invoice.ARAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                            if (Invoice.ARAccountRef.FullName == null)
                                                Invoice.ARAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                        if (Invoice.ARAccountRef.FullName == null)
                                            Invoice.ARAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("TemplateRefFullName"))
                            {
                                #region Validations of Template Full name
                                if (dr["TemplateRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["TemplateRefFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Template full name (" + dr["TemplateRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (Invoice.TemplateRef.FullName == null)
                                                    Invoice.TemplateRef.FullName = null;
                                                else
                                                    Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                                if (Invoice.TemplateRef.FullName == null)
                                                    Invoice.TemplateRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (Invoice.TemplateRef.FullName == null)
                                                Invoice.TemplateRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                        if (Invoice.TemplateRef.FullName == null)
                                            Invoice.TemplateRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["SalesRepRefFullName"].ToString().Length > 5)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesRepRef full name (" + dr["SalesRepRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (Invoice.SalesRepRef.FullName == null)
                                                    Invoice.SalesRepRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (Invoice.SalesRepRef.FullName == null)
                                                    Invoice.SalesRepRef.FullName = null;
                                                else
                                                    Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 5));
                                            }
                                        }
                                        else
                                        {
                                            Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (Invoice.SalesRepRef.FullName == null)
                                                Invoice.SalesRepRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                        if (Invoice.SalesRepRef.FullName == null)
                                            Invoice.SalesRepRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("FOB"))
                            {
                                #region Validations of PONumber
                                if (dr["FOB"].ToString() != string.Empty)
                                {
                                    string strCust = dr["FOB"].ToString();
                                    if (strCust.Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This FOB (" + dr["FOB"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.FOB = dr["FOB"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.FOB = dr["FOB"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.FOB = dr["FOB"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.FOB = dr["FOB"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipDate"))
                            {
                                #region validations of ShipDate
                                if (dr["ShipDate"].ToString() != "<None>" || dr["ShipDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["ShipDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Invoice.ShipDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.ShipDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Invoice.ShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InvoiceDt = dttest;
                                            Invoice.ShipDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        InvoiceDt = Convert.ToDateTime(datevalue);
                                        Invoice.ShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Invoice.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Invoice.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InvoiceDt = dttest;
                                            Invoice.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        InvoiceDt = Convert.ToDateTime(datevalue);
                                        Invoice.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("InvoiceRefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    Invoice.InvoiceDate = InvoiceDt;

                                if (dr["InvoiceRefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["InvoiceRefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["InvoiceRefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.RefNumber = dr["InvoiceRefNumber"].ToString().Substring(0, 11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                                        }
                                    }
                                    else
                                        Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsPending"))
                            {
                                #region Validations of IsPending
                                if (dr["IsPending"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsPending"].ToString(), out result))
                                    {
                                        Invoice.IsPending = Convert.ToInt32(dr["IsPending"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsPending"].ToString().ToLower() == "true")
                                        {
                                            Invoice.IsPending = dr["IsPending"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsPending"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Invoice.IsPending = dr["IsPending"].ToString().ToLower(); ;
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsPending (" + dr["IsPending"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Invoice.IsPending = dr["IsPending"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.IsPending = dr["IsPending"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Invoice.IsPending = dr["IsPending"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsFinanceCharge"))
                            {
                                #region Validations of IsFinanceCharge
                                if (dr["IsFinanceCharge"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsFinanceCharge"].ToString(), out result))
                                    {
                                        Invoice.IsFinanceCharge = Convert.ToInt32(dr["IsFinanceCharge"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsFinanceCharge"].ToString().ToLower() == "true")
                                        {
                                            Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsFinanceCharge"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsFinanceCharge (" + dr["IsFinanceCharge"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("PONumber"))
                            {
                                #region Validations of PONumber
                                if (dr["PONumber"].ToString() != string.Empty)
                                {
                                    string strCust = dr["PONumber"].ToString();
                                    if (strCust.Length > 25)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PONumber (" + dr["PONumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.PONumber = dr["PONumber"].ToString().Substring(0, 25);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.PONumber = dr["PONumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.PONumber = dr["PONumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.PONumber = dr["PONumber"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("TermsRefFullName"))
                            {
                                #region Validations of Terms Full name
                                if (dr["TermsRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["TermsRefFullName"].ToString();
                                    if (strCust.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Terms fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                if (Invoice.TermsRef.FullName == null)
                                                {
                                                    Invoice.TermsRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                                if (Invoice.TermsRef.FullName == null)
                                                {
                                                    Invoice.TermsRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                            if (Invoice.TermsRef.FullName == null)
                                            {
                                                Invoice.TermsRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());

                                        if (Invoice.TermsRef.FullName == null)
                                        {
                                            Invoice.TermsRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipMethodRefFullName"))
                            {
                                #region Validations of ShipMethodRef Full name
                                if (dr["ShipMethodRefFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ShipMethodRefFullName"].ToString();
                                    if (strCust.Length > 15)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Method Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                                if (Invoice.ShipMethodRef.FullName == null)
                                                {
                                                    Invoice.ShipMethodRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString().Substring(0, 15));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                                if (Invoice.ShipMethodRef.FullName == null)
                                                {
                                                    Invoice.ShipMethodRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());

                                            if (Invoice.ShipMethodRef.FullName == null)
                                            {
                                                Invoice.ShipMethodRef.FullName = null;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());

                                        if (Invoice.ShipMethodRef.FullName == null)
                                        {
                                            Invoice.ShipMethodRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            DateTime NewDueDt = new DateTime();
                            if (dt.Columns.Contains("DueDate"))
                            {
                                #region validations of DueDate
                                if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                                {
                                    string duevalue = dr["DueDate"].ToString();
                                    if (!DateTime.TryParse(duevalue, out NewDueDt))
                                    {

                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Invoice.DueDate = duevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.DueDate = duevalue;
                                                }
                                            }
                                            else
                                            {
                                                Invoice.DueDate = duevalue;
                                            }
                                        }
                                        else
                                        {
                                            Invoice.DueDate = dttest.ToString("yyyy-MM-dd");
                                        }


                                    }
                                    else
                                    {
                                        Invoice.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemSalesTaxRefFullName"))
                            {
                                #region Validations of ItemSalesTax Full name
                                if (dr["ItemSalesTaxRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemSalesTaxRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemTaxRef full name (" + dr["ItemSalesTaxRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                                {
                                                    Invoice.ItemSalesTaxRef.FullName = null;
                                                }
                                                else
                                                    Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                                if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                                {
                                                    Invoice.ItemSalesTaxRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                            if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                            {
                                                Invoice.ItemSalesTaxRef.FullName = null;
                                            }

                                        }

                                    }
                                    else
                                    {
                                        Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                        {
                                            Invoice.ItemSalesTaxRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                                Invoice.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                Invoice.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            Invoice.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Invoice.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }

                            QuickBookEntities.BillAddress BillAddressItem = new BillAddress();
                            if (dt.Columns.Contains("BillAddr1"))
                            {
                                #region Validations of Bill Addr1
                                if (dr["BillAddr1"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr1 = dr["BillAddr1"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillAddr2"))
                            {
                                #region Validations of Bill Addr2
                                if (dr["BillAddr2"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr2 = dr["BillAddr2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillAddr3"))
                            {
                                #region Validations of Bill Addr3
                                if (dr["BillAddr3"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr3 = dr["BillAddr3"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillAddr4"))
                            {
                                #region Validations of Bill Addr4
                                if (dr["BillAddr4"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr4 = dr["BillAddr4"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillAddr5"))
                            {
                                #region Validations of Bill Addr5
                                if (dr["BillAddr5"].ToString() != string.Empty)
                                {
                                    if (dr["BillAddr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Add5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Addr5 = dr["BillAddr5"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillCity"))
                            {
                                #region Validations of Bill City
                                if (dr["BillCity"].ToString() != string.Empty)
                                {
                                    if (dr["BillCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill City (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.City = dr["BillCity"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.City = dr["BillCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.City = dr["BillCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.City = dr["BillCity"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillState"))
                            {
                                #region Validations of Bill State
                                if (dr["BillState"].ToString() != string.Empty)
                                {
                                    if (dr["BillState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill State (" + dr["BillState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.State = dr["BillState"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.State = dr["BillState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.State = dr["BillState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.State = dr["BillState"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillPostalCode"))
                            {
                                #region Validations of Bill Postal Code
                                if (dr["BillPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["BillPostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Postal Code (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("BillCountry"))
                            {
                                #region Validations of Bill Country
                                if (dr["BillCountry"].ToString() != string.Empty)
                                {
                                    if (dr["BillCountry"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Country (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Country = dr["BillCountry"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Country = dr["BillCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Country = dr["BillCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Country = dr["BillCountry"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("BillNote"))
                            {
                                #region Validations of Bill Note
                                if (dr["BillNote"].ToString() != string.Empty)
                                {
                                    if (dr["BillNote"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Bill Note (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                BillAddressItem.Note = dr["BillNote"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                BillAddressItem.Note = dr["BillNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            BillAddressItem.Note = dr["BillNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Note = dr["BillNote"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (BillAddressItem.Addr1 != null || BillAddressItem.Addr2 != null || BillAddressItem.Addr3 != null || BillAddressItem.Addr4 != null || BillAddressItem.Addr5 != null
                                || BillAddressItem.City != null || BillAddressItem.Country != null || BillAddressItem.PostalCode != null || BillAddressItem.State != null || BillAddressItem.Note != null)
                                Invoice.BillAddress.Add(BillAddressItem);

                            if (dt.Columns.Contains("Phone"))
                            {
                                #region Validations of Phone
                                if (dr["Phone"].ToString() != string.Empty)
                                {
                                    if (dr["Phone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.Phone = dr["Phone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.Phone = dr["Phone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Phone = dr["Phone"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Fax"))
                            {
                                #region Validations of Fax
                                if (dr["Fax"].ToString() != string.Empty)
                                {
                                    if (dr["Fax"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.Fax = dr["Fax"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.Fax = dr["Fax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Fax = dr["Fax"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Email"))
                            {
                                #region Validations of Email
                                if (dr["Email"].ToString() != string.Empty)
                                {
                                    if (dr["Email"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.Email = dr["Email"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.Email = dr["Email"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Email = dr["Email"].ToString();
                                    }
                                }
                                #endregion
                            }

                            QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();
                            if (dt.Columns.Contains("ShipAddr1"))
                            {
                                #region Validations of Ship Addr1
                                if (dr["ShipAddr1"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr2"))
                            {
                                #region Validations of Ship Addr2
                                if (dr["ShipAddr2"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr3"))
                            {
                                #region Validations of Ship Addr3
                                if (dr["ShipAddr3"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipAddr4"))
                            {
                                #region Validations of Ship Addr4
                                if (dr["ShipAddr4"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipAddr5"))
                            {
                                #region Validations of Ship Addr5
                                if (dr["ShipAddr5"].ToString() != string.Empty)
                                {
                                    if (dr["ShipAddr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipCity"))
                            {
                                #region Validations of Ship City
                                if (dr["ShipCity"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCity"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.City = dr["ShipCity"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.City = dr["ShipCity"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipState"))
                            {
                                #region Validations of Ship State
                                if (dr["ShipState"].ToString() != string.Empty)
                                {
                                    if (dr["ShipState"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.State = dr["ShipState"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.State = dr["ShipState"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipPostalCode"))
                            {
                                #region Validations of Ship Postal Code
                                if (dr["ShipPostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["ShipPostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ShipCountry"))
                            {
                                #region Validations of Ship Country
                                if (dr["ShipCountry"].ToString() != string.Empty)
                                {
                                    if (dr["ShipCountry"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ShipNote"))
                            {
                                #region Validations of Ship Note
                                if (dr["ShipNote"].ToString() != string.Empty)
                                {
                                    if (dr["ShipNote"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ShipAddressItem.Note = dr["ShipNote"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ShipAddressItem.Note = dr["ShipNote"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                              || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                                Invoice.ShipAddress.Add(ShipAddressItem);

                            if (dt.Columns.Contains("CustomerMsgRefFullName"))
                            {
                                #region Validations of CustomerMsgRef Full name
                                if (dr["CustomerMsgRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerMsgRefFullName"].ToString().Length > 101)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerMsgRef full name (" + dr["CustomerMsgRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                                if (Invoice.CustomerMsgRef.FullName == null)
                                                {
                                                    Invoice.CustomerMsgRef.FullName = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                                if (Invoice.CustomerMsgRef.FullName == null)
                                                {
                                                    Invoice.CustomerMsgRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                            if (Invoice.CustomerMsgRef.FullName == null)
                                            {
                                                Invoice.CustomerMsgRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                        if (Invoice.CustomerMsgRef.FullName == null)
                                        {
                                            Invoice.CustomerMsgRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("IsToBeEmailed"))
                            {
                                #region Validations of IsToBeEmailed
                                if (dr["IsToBeEmailed"].ToString() != "<None>" || dr["IsToBeEmailed"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBeEmailed"].ToString(), out result))
                                    {
                                        Invoice.IsToBeEmailed = Convert.ToInt32(dr["IsToBeEmailed"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBeEmailed"].ToString().ToLower() == "true")
                                        {
                                            Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBeEmailed"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBeEmailed (" + dr["IsToBeEmailed"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsToBePrinted"))
                            {
                                #region Validations of IsToBePrinted
                                if (dr["IsToBePrinted"].ToString() != "<None>" || dr["IsToBePrinted"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                    {
                                        Invoice.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                        {
                                            Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }

                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsTaxIncluded"))
                            {
                                #region Validations of IsTaxIncluded
                                if (dr["IsTaxIncluded"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                    {
                                        Invoice.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                            }
                                            else
                                            {
                                                Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("CustomerSalesTaxCodeRefFullName"))
                            {
                                #region Validations of CustomerSalesTaxCode Full name
                                if (dr["CustomerSalesTaxCodeRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CustomerSalesTaxCodeRefFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomerTaxCodeRef Full Name (" + dr["CustomerSalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                                if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                                {
                                                    Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                                if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                                {
                                                    Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                            if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                            {
                                                Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                        {
                                            Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Other"))
                            {
                                #region Validation of Other

                                if (dr["Other"].ToString() != string.Empty)
                                {
                                    if (dr["Other"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other (" + dr["Other"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.Other = dr["Other"].ToString().Substring(0, 29);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.Other = dr["Other"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.Other = dr["Other"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Other = dr["Other"].ToString();
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is invalid for this mapping of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        //Invoice.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                        Invoice.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                    }
                                }

                                #endregion

                            }

                            #region Adding Invoice Line

                            DataProcessingBlocks.InvoiceLineAdd InvLine = new DataProcessingBlocks.InvoiceLineAdd();


                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;

                            string ItemSaleTaxFullName = string.Empty;

                            //if default settings contain checkBoxGrossToNet checked.
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                    {
                                        if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["SalesTaxCodeRefFullName"].ToString();
                                            //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                            #endregion


                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (InvLine.ItemRef.FullName == null)
                                                    InvLine.ItemRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (InvLine.ItemRef.FullName == null)
                                                    InvLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (InvLine.ItemRef.FullName == null)
                                                InvLine.ItemRef.FullName = null;
                                        }

                                    }
                                    else
                                    {
                                        InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (InvLine.ItemRef.FullName == null)
                                            InvLine.ItemRef.FullName = null;
                                    }

                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Description"].ToString();
                                                InvLine.Desc = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Description"].ToString();
                                                InvLine.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            InvLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        InvLine.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    InvLine.Quantity = strQuantity;

                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("Rate"))
                            {
                                #region Validations for Rate
                                if (dr["Rate"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (InvLine.Rate == null)
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InvLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InvLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("InvoiceLineClassFullName"))
                            {
                                #region Validations of Invoice Line Class Full name
                                if (dr["InvoiceLineClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["InvoiceLineClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Invoice Line Class name (" + dr["InvoiceLineClassFullName"].ToString() + ") is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                                if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                                {
                                                    InvLine.ClassRef.FullName = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                                if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                                {
                                                    InvLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                            if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                            {
                                                InvLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());

                                        if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                        {
                                            InvLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["Amount"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            InvLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                       CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                       CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Amount = string.Format("{0:000000.00}", amount);

                                                }
                                            }
                                            //Check if EstLine.Amount is null
                                            if (InvLine.Amount == null)
                                            {
                                                InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }
                                    }
                                }

                                #endregion
                            }

                            //InventorySiteRefFullName
                            if (dt.Columns.Contains("InventorySiteRefFullName"))
                            {
                                #region Validations of Inventory Site Ref Full Name
                                if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (InvLine.InventorySiteRef.FullName == null)
                                                    InvLine.InventorySiteRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (InvLine.InventorySiteRef.FullName == null)
                                                    InvLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InvLine.InventorySiteRef.FullName == null)
                                                InvLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (InvLine.InventorySiteRef.FullName == null)
                                            InvLine.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("InventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (InvLine.InventorySiteLocationRef.FullName == null)
                                                    InvLine.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (InvLine.InventorySiteLocationRef.FullName == null)
                                                    InvLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (InvLine.InventorySiteLocationRef.FullName == null)
                                                InvLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (InvLine.InventorySiteLocationRef.FullName == null)
                                            InvLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ServiceDate"))
                            {
                                #region validations of ServiceDate
                                if (dr["ServiceDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["ServiceDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.ServiceDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.ServiceDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                InvLine.ServiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        InvLine.ServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                {
                                    #region Validations of sales tax code Full name
                                    if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["SalesTaxCodeRefFullName"].ToString().Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This sales tax code name (" + dr["SalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                    if (InvLine.SalesTaxCodeRef.FullName == null)
                                                        InvLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                    if (InvLine.SalesTaxCodeRef.FullName == null)
                                                        InvLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                if (InvLine.SalesTaxCodeRef.FullName == null)
                                                    InvLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                            if (InvLine.SalesTaxCodeRef.FullName == null)
                                                InvLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                            {
                                if (dt.Columns.Contains("IsTaxable"))
                                {
                                    #region Validations of IsTaxable
                                    if (dr["IsTaxable"].ToString() != "<None>")
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["IsTaxable"].ToString(), out result))
                                        {
                                            
                                            InvLine.IsTaxable = Convert.ToInt32(dr["IsTaxable"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsTaxable"].ToString().ToLower() == "true")
                                            {

                                                InvLine.IsTaxable = dr["IsTaxable"].ToString().ToLower(); ;
                                            }
                                            else
                                            {
                                                if (dr["IsTaxable"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                   
                                                InvLine.IsTaxable = dr["IsTaxable"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsTaxable (" + dr["IsTaxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(results) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                       InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;

                                                        InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                    }
                                                }
                                                else
                                                {

                                                    InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }

                            }
                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                InvLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                InvLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            InvLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        InvLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion
                            }

                            //New Feature::601

                            if (dt.Columns.Contains("PriceLevelRefFullName"))
                            {
                                #region Validations of PriceLevel FullName
                                if (dr["PriceLevelRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["PriceLevelRefFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (InvLine.PriceLevelRef.FullName == null)
                                                    InvLine.PriceLevelRef.FullName = null;
                                                else
                                                    InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (InvLine.PriceLevelRef.FullName == null)
                                                    InvLine.PriceLevelRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                            if (InvLine.PriceLevelRef.FullName == null)
                                                InvLine.PriceLevelRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                        if (InvLine.PriceLevelRef.FullName == null)
                                            InvLine.PriceLevelRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Other1"))
                            {
                                #region Validations for Other1

                                if (dr["Other1"].ToString() != string.Empty)
                                {
                                    if (dr["Other1"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Other1"].ToString();
                                                InvLine.Other1 = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Other1"].ToString();
                                                InvLine.Other1 = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Other1"].ToString();
                                            InvLine.Other1 = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["other1"].ToString();
                                        InvLine.Other1 = strDesc;
                                    }
                                }


                                #endregion
                            }
                            if (dt.Columns.Contains("Other2"))
                            {
                                #region Validations for Other2

                                if (dr["Other2"].ToString() != string.Empty)
                                {
                                    if (dr["Other2"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Other2"].ToString();
                                                InvLine.Other2 = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Other2"].ToString();
                                                InvLine.Other2 = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Other2"].ToString();
                                            InvLine.Other2 = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["other2"].ToString();
                                        InvLine.Other2 = strDesc;
                                    }
                                }


                                #endregion
                            }
                           
                            //Bug no 331
                            QuickBookStreams.DataExt dataext = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("CustomFieldName1"))
                            {
                                dataext.OwnerID = "0";
                                #region Validations for CustomFieldName1
                                if (dr["CustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName1 ( " + dr["CustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldName1"].ToString().Substring(0, 4095);
                                                dataext.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldName1"].ToString();
                                                dataext.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName1"].ToString();
                                            dataext.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName1"].ToString();
                                        dataext.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue1"))
                            {
                                #region Validations for CustomFieldValue1
                                if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue1 ( " + dr["CustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldValue1"].ToString().Substring(0, 4095);
                                                dataext.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldValue1"].ToString();
                                                dataext.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue1"].ToString();
                                            dataext.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue1"].ToString();
                                        dataext.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext.DataExtName != null && dataext.DataExtValue != null && dataext.OwnerID != null)
                            { 
                                InvLine.DataExt.Add(dataext);
                            }

                            QuickBookStreams.DataExt dataext1 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("CustomFieldName2"))
                            {
                                dataext1.OwnerID = "0";
                                #region Validations for CustomFieldName2
                                if (dr["CustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName2 ( " + dr["CustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldName2"].ToString().Substring(0, 4095);
                                                dataext1.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldName2"].ToString();
                                                dataext1.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName2"].ToString();
                                            dataext1.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName2"].ToString();
                                        dataext1.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue2"))
                            {
                                #region Validations for CustomFieldValue2
                                if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue2 ( " + dr["CustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldValue2"].ToString().Substring(0, 4095);
                                                dataext1.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldValue2"].ToString();
                                                dataext1.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue2"].ToString();
                                            dataext1.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue2"].ToString();
                                        dataext1.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext1.DataExtName != null && dataext1.DataExtValue != null && dataext1.OwnerID != null)
                            {
                                InvLine.DataExt.Add(dataext1);
                            }

                            ///443
                            ///
                            QuickBookStreams.DataExt dataext4 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("CustomFieldName3"))
                            {
                                dataext4.OwnerID = "0";
                                #region Validations for CustomFieldName3
                                if (dr["CustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName3"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName3 ( " + dr["CustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldName3"].ToString().Substring(0, 4095);
                                                dataext4.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldName3"].ToString();
                                                dataext4.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName3"].ToString();
                                            dataext4.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName3"].ToString();
                                        dataext4.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue3"))
                            {
                                #region Validations for CustomFieldValue3
                                if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue3"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue3 ( " + dr["CustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldValue3"].ToString().Substring(0, 4095);
                                                dataext4.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldValue3"].ToString();
                                                dataext4.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue3"].ToString();
                                            dataext4.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue3"].ToString();
                                        dataext4.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext4.DataExtName != null && dataext4.DataExtValue != null && dataext4.OwnerID != null)
                            {
                                InvLine.DataExt.Add(dataext4);
                            }
                            //end bug no 331

                            //11.4 444
                            if (dt.Columns.Contains("LinkToSalesOrder"))
                            {
                                #region Validations of LinkToSalesOrder
                                if (dr["LinkToSalesOrder"].ToString() != string.Empty)
                                {
                                    string strLinkToTxnID = dr["LinkToSalesOrder"].ToString();
                                    InvLine.LinkToSalesOrder = dr["LinkToSalesOrder"].ToString();
                                }
                                #endregion

                            }  
                            if(InvLine.ItemRef != null)
                            Invoice.InvoiceLineAdd.Add(InvLine);
                            if (row_cnt == 1)
                            {

                                 #region Invoice Line add for Freight

                                if (dt.Columns.Contains("Freight"))
                                {
                                    if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                    {
                                        InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                        //Adding freight charge item to invoice line.
                                        InvLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                        InvLine.ItemRef.FullName = defaultSettings.Frieght;                                        

                                        #region Validations for Rate
                                        if (dr["Freight"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                            //decimal amount;
                                            if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This Freight Rate ( " + dr["Freight"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                        InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                        InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                    InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                        }
                                                    }
                                                    //Check if InvoiceLine.Rate is null
                                                    if (InvLine.Rate == null)
                                                    {
                                                        InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                    }
                                                }
                                                else
                                                {
                                                    InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                }
                                            }
                                        }
                                        if (InvLine.ItemRef != null)
                                        Invoice.InvoiceLineAdd.Add(InvLine);

                                        #endregion
                                    }
                                }

                                #endregion                          

                                 #region Invoice Line add for Insurance

                            if (dt.Columns.Contains("Insurance"))
                            {
                                if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                {
                                    InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                    //Adding freight charge item to invoice line.
                                    InvLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                    InvLine.ItemRef.FullName = defaultSettings.Insurance;
                                   
                                    #region Validations for Rate
                                    if (dr["Insurance"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        //decimal amount;
                                        if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Insurance Rate ( " + dr["Insurance"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                    InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                    InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                    {
                                                        decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            rate = Rate / (1 + (TaxRate / 100));
                                                        }

                                                        InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                    }
                                                }
                                                //Check if InvoiceLine.Rate is null
                                                if (InvLine.Rate == null)
                                                {
                                                    InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                }
                                            }
                                            else
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                            }
                                        }
                                    }
                                    if (InvLine.ItemRef != null)
                                    Invoice.InvoiceLineAdd.Add(InvLine);

                                    #endregion
                                }
                            }

                            #endregion

                                 #region Invoice Line Add  for Discount

                            if (dt.Columns.Contains("Discount"))
                            {
                                if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                {
                                    InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                    //Adding freight charge item to invoice line.
                                    InvLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                    InvLine.ItemRef.FullName = defaultSettings.Discount;
                                  
                                    #region Validations for Rate
                                    if (dr["Discount"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        //decimal amount;
                                        if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Discount Rate ( " + dr["Discount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                    InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                    InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                    {
                                                        decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            //decimal TaxRate = 10;
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            rate = Rate / (1 + (TaxRate / 100));
                                                        }

                                                        InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                    }
                                                }
                                                //Check if InvoiceLine.Rate is null
                                                if (InvLine.Rate == null)
                                                {
                                                    InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                }
                                            }
                                            else
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                            }
                                        }
                                    }
                                    if (InvLine.ItemRef != null)
                                    Invoice.InvoiceLineAdd.Add(InvLine);

                                    #endregion
                                }
                            }

                                #endregion

                            //bug no. 410
                            #region invoice Line add for SalesTax

                            if (dt.Columns.Contains("SalesTax"))
                            {
                                if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                                {
                                    InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                    //Adding SalesTax charge item to Invoive line.                                          

                                    InvLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                    InvLine.ItemRef.FullName = defaultSettings.SalesTax;

                                    //Adding SalesTax charge amount to Invoive line.

                                    #region Validations for Rate
                                    if (dr["SalesTax"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        //decimal amount;
                                        if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strAmount = dr["SalesTax"].ToString();
                                                    InvLine.Amount = strAmount;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strAmount = dr["SalesTax"].ToString();
                                                    InvLine.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                string strAmount = dr["SalesTax"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                    {
                                                        decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            //decimal TaxRate = 10;
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            rate = Rate / (1 + (TaxRate / 100));
                                                        }

                                                        InvLine.Amount = string.Format("{0:000000.00}", rate);
                                                    }
                                                }
                                                //Check if InvoiceLine.Rate is null
                                                if (InvLine.Amount == null)
                                                {
                                                    InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                }
                                            }
                                            else
                                            {
                                                InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                            }

                                        }
                                    }
                                    if (InvLine.ItemRef != null)
                                    Invoice.InvoiceLineAdd.Add(InvLine);
                                    #endregion
                                }
                            }


                            #endregion
                            }
                            #endregion


                            DataProcessingBlocks.InvoiceLineGroupAdd InvGroup = new DataProcessingBlocks.InvoiceLineGroupAdd();

                            #region Adding Invoice group
                           
                                if (dt.Columns.Contains("ItemGroupFullName"))
                                {

                                    #region Validations of item Full name
                                    if (dr["ItemGroupFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item Group full name (" + dr["ItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                    if (InvGroup.ItemGroupRef.FullName == null)
                                                        InvGroup.ItemGroupRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                    if (InvGroup.ItemGroupRef.FullName == null)
                                                        InvGroup.ItemGroupRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                if (InvGroup.ItemGroupRef.FullName == null)
                                                    InvGroup.ItemGroupRef.FullName = null;
                                            }

                                        }
                                        else
                                        {
                                            InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                            if (InvGroup.ItemGroupRef.FullName == null)
                                                InvGroup.ItemGroupRef.FullName = null;
                                        }

                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("ItemGroupQuantity"))
                                {
                                    #region Validations for Quantity
                                    if (dr["ItemGroupQuantity"].ToString() != string.Empty)
                                    {
                                        string strQuantity = dr["ItemGroupQuantity"].ToString();
                                        InvGroup.Quantity = strQuantity;

                                    }

                                    #endregion

                                }

                                if (dt.Columns.Contains("ItemGroupUOM"))
                                {
                                    #region Validations for UnitOfMeasure
                                    if (dr["ItemGroupUOM"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupUOM"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                    InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                    InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                            InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }

                                    #endregion
                                }
                                //Bug no 331

                                QuickBookStreams.DataExt dataext2 = new QuickBookStreams.DataExt();

                                if (dt.Columns.Contains("ItemGroupCustomFieldName1"))
                                {
                                    dataext2.OwnerID = "0";
                                    #region Validations for ItemGroupCustomFieldName1
                                    if (dr["ItemGroupCustomFieldName1"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName1"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName1 ( " + dr["ItemGroupCustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName1"].ToString().Substring(0, 4095);
                                                    dataext2.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                    dataext2.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                dataext2.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                            dataext2.DataExtName = strDesc;
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("ItemGroupCustomFieldValue1"))
                                {
                                    #region Validations for ItemGroupCustomFieldValue1
                                    if (dr["ItemGroupCustomFieldValue1"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue1"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue1 ( " + dr["ItemGroupCustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue1"].ToString().Substring(0, 4095);
                                                    dataext2.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                    dataext2.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                dataext2.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                            dataext2.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dataext2.DataExtName != null && dataext2.DataExtValue != null && dataext2.OwnerID != null)
                                { 
                                    InvGroup.DataExt.Add(dataext2);
                                }

                                QuickBookStreams.DataExt dataext3 = new QuickBookStreams.DataExt();
                                if (dt.Columns.Contains("ItemGroupCustomFieldName2"))
                                {
                                    dataext3.OwnerID = "0";
                                    #region Validations for ItemGroupCustomFieldName2
                                    if (dr["ItemGroupCustomFieldName2"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName2"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName2 ( " + dr["ItemGroupCustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName2"].ToString().Substring(0, 4095);
                                                    dataext3.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                    dataext3.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                dataext3.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                            dataext3.DataExtName = strDesc;
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("ItemGroupCustomFieldValue2"))
                                {
                                    #region Validations for ItemGroupCustomFieldValue2
                                    if (dr["ItemGroupCustomFieldValue2"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue2"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue2 ( " + dr["ItemGroupCustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue2"].ToString().Substring(0, 4095);
                                                    dataext3.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                    dataext3.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                dataext3.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                            dataext3.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dataext3.DataExtName != null && dataext3.DataExtValue != null && dataext3.OwnerID != null)
                                { 
                                    InvGroup.DataExt.Add(dataext3);
                                }

                                /// 439
                                /// 
                                QuickBookStreams.DataExt dataext5 = new QuickBookStreams.DataExt();
                                if (dt.Columns.Contains("ItemGroupCustomFieldName3"))
                                {
                                    dataext5.OwnerID = "0";
                                    #region Validations for ItemGroupCustomFieldName3
                                    if (dr["ItemGroupCustomFieldName3"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName3"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName3 ( " + dr["ItemGroupCustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName3"].ToString().Substring(0, 4095);
                                                    dataext5.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                    dataext5.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                dataext5.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                            dataext5.DataExtName = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ItemGroupCustomFieldValue3"))
                                {
                                    #region Validations for ItemGroupCustomFieldValue3
                                    if (dr["ItemGroupCustomFieldValue3"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue3"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue3 ( " + dr["ItemGroupCustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue3"].ToString().Substring(0, 4095);
                                                    dataext5.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                    dataext5.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                dataext5.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                            dataext5.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dataext5.DataExtName != null && dataext5.DataExtValue != null && dataext5.OwnerID != null)
                                { 
                                    InvGroup.DataExt.Add(dataext5);
                                }
                                //end bug no 331
                                /// bug no 450
                                if (InvGroup.ItemGroupRef != null)
                                {
                                    if (InvGroup.ItemGroupRef.FullName != null)
                                        Invoice.InvoiceLineGroupAdd.Add(InvGroup);
                                }
                            

                            #endregion

                            // Axis 10.0

                            #region Adding DiscountLineAdd.

                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                            {
                                
                                 QuickBookEntities.DiscountLineAdd discountretitem = new DiscountLineAdd();
                                 QuickBookEntities.ShippingLineAdd shippingretitem = new ShippingLineAdd();

                                string TaxRatevalueForDiscount = string.Empty;

                               // #region OCmmented Code
                                if (dt.Columns.Contains("DiscountLineAmount"))
                                {
                                    #region Validations for Amount
                                    if (dr["DiscountLineAmount"].ToString() != string.Empty)
                                    {
                                        decimal finalamount;
                                        if (!decimal.TryParse(dr["DiscountLineAmount"].ToString(), out finalamount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DiscountLineAmount ( " + dr["ShippingLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strAmount = dr["DiscountLineAmount"].ToString();
                                                    discountretitem.Amount = strAmount;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strAmount = dr["DiscountLineAmount"].ToString();
                                                    discountretitem.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                string strAmount = dr["DiscountLineAmount"].ToString();
                                                discountretitem.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRatevalueForDiscount != string.Empty && discountretitem.IsTaxable != null && discountretitem.IsTaxable != string.Empty)
                                                {
                                                    if (discountretitem.IsTaxable == "true" || discountretitem.IsTaxable == "1")
                                                    {
                                                        decimal Amountdec = Convert.ToDecimal(dr["DiscountLineAmount"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            //decimal TaxRate = 10;
                                                            decimal TaxRate = Convert.ToDecimal(TaxRatevalueForDiscount);
                                                            finalamount = Amountdec / (1 + (TaxRate / 100));
                                                        }

                                                        discountretitem.Amount = string.Format("{0:000000.00}", finalamount);
                                                    }
                                                }
                                                //Check if EstLine.Amount is null
                                                if (discountretitem.Amount == null)
                                                {
                                                    discountretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountLineAmount"].ToString()));
                                                }
                                            }
                                            else
                                            {
                                                discountretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountLineAmount"].ToString()));
                                            }
                                        }
                                    }

                                    #endregion
                                }
                               // #endregion



                                if (dt.Columns.Contains("DiscountLineIsTaxable"))
                                {
                                    #region Validations of IsTaxable
                                    if (dr["DiscountLineIsTaxable"].ToString() != "<None>")
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["DiscountLineIsTaxable"].ToString(), out result))
                                        {
                                            discountretitem.IsTaxable = Convert.ToInt32(dr["DiscountLineIsTaxable"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["DiscountLineIsTaxable"].ToString().ToLower() == "true")
                                            {
                                                discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString().ToLower(); ;
                                            }
                                            else
                                            {
                                                if (dr["DiscountLineIsTaxable"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This DiscountLineIsTaxable (" + dr["DiscountLineIsTaxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(results) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString();
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("DiscountLineAccountRefFullName"))
                                {
                                    #region Validations of Account Full name
                                    if (dr["DiscountLineAccountRefFullName"].ToString() != string.Empty)
                                    {
                                        string strAccount = dr["DiscountLineAccountRefFullName"].ToString();
                                        if (strAccount.Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Account Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
												
                                                    discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
													
                                                    if (discountretitem.AccountRef.FullName == null)
                                                    {
                                                        discountretitem.AccountRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString().Substring(0, 1000));
													
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
													
                                                    if (discountretitem.AccountRef.FullName == null)
                                                    {
                                                        discountretitem.AccountRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
                                                if (discountretitem.AccountRef.FullName == null)
                                                {
                                                    discountretitem.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
                                           
                                            if (discountretitem.AccountRef.FullName == null)
                                            {
                                                discountretitem.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                if (discountretitem.Amount != null || discountretitem.IsTaxable != null || discountretitem.AccountRef != null)
                                    Invoice.DiscountLineAdd.Add(discountretitem);

                                if (dt.Columns.Contains("ShippingLineAmount"))
                                {
                                    #region Validations for Amount
                                    if (dr["ShippingLineAmount"].ToString() != string.Empty)
                                    {
                                        decimal finalamount;
                                        if (!decimal.TryParse(dr["ShippingLineAmount"].ToString(), out finalamount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ShippingLineAmount ( " + dr["ShippingLineAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strAmount = dr["ShippingLineAmount"].ToString();
                                                    shippingretitem.Amount = strAmount;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strAmount = dr["ShippingLineAmount"].ToString();
                                                    shippingretitem.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                string strAmount = dr["ShippingLineAmount"].ToString();
                                                shippingretitem.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (shippingretitem.Amount == null)
                                                {
                                                    shippingretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ShippingLineAmount"].ToString()));
                                                }
                                            }
                                            else
                                            {
                                                shippingretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ShippingLineAmount"].ToString()));
                                            }
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("ShippingLineAccountRefFullName"))
                                {
                                    #region Validations of Account Full name
                                    if (dr["ShippingLineAccountRefFullName"].ToString() != string.Empty)
                                    {
                                        string strAccount = dr["ShippingLineAccountRefFullName"].ToString();
                                        if (strAccount.Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Account Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                                    if (shippingretitem.AccountRef.FullName == null)
                                                    {
                                                        shippingretitem.AccountRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString().Substring(0, 1000));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                                    if (shippingretitem.AccountRef.FullName == null)
                                                    {
                                                        shippingretitem.AccountRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                                if (shippingretitem.AccountRef.FullName == null)
                                                {
                                                    shippingretitem.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (shippingretitem.AccountRef.FullName == null)
                                            {
                                                shippingretitem.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }

                                if (shippingretitem.Amount != null || shippingretitem.AccountRef != null)
                                    Invoice.ShippingLineAdd.Add(shippingretitem);

                            }
                            #endregion

                                                   

                            coll.Add(Invoice);
                            #endregion
                        }
                        else
                        {
                            #region for existing ref number
                            #region Adding Invoice Line

                            DataProcessingBlocks.InvoiceLineAdd InvLine = new DataProcessingBlocks.InvoiceLineAdd();


                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;

                            //if default settings contain checkBoxGrossToNet checked.
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                    {
                                        if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["SalesTaxCodeRefFullName"].ToString();

                                            //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                            #endregion

                           // cnt = Invoice.InvoiceLineAdd.Count - 1;
                            // Axis 23
                            cnt = Invoice.InvoiceLineAdd.Count - 1 - dt_count;
                            // end Axis 23
                            if (dt.Columns.Contains("Freight") || dt.Columns.Contains("Insurance") || dt.Columns.Contains("Discount"))
                            {
                                for (int i = cnt; i >= 1; i--)
                                {
                                    Invoice.InvoiceLineAdd.RemoveAt(i);
                                }
                            }

                            if (dt.Columns.Contains("ItemRefFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemRefFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (InvLine.ItemRef.FullName == null)
                                                    InvLine.ItemRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                                if (InvLine.ItemRef.FullName == null)
                                                    InvLine.ItemRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (InvLine.ItemRef.FullName == null)
                                                InvLine.ItemRef.FullName = null;
                                        }

                                    }
                                    else
                                    {
                                        InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (InvLine.ItemRef.FullName == null)
                                            InvLine.ItemRef.FullName = null;
                                    }

                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Description"].ToString();
                                                InvLine.Desc = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Description"].ToString();
                                                InvLine.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            InvLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        InvLine.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    InvLine.Quantity = strQuantity;

                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("Rate"))
                            {
                                #region Validations for Rate
                                if (dr["Rate"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (InvLine.Rate == null)
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("SerialNumber"))
                                {
                                    #region Validations of ItemLine SerialNumber
                                    if (dr["SerialNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["SerialNumber"].ToString().Length > 4095)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("LotNumber"))
                                {
                                    #region Validations of ItemLine LotNumber
                                    if (dr["LotNumber"].ToString() != string.Empty)
                                    {
                                        if (dr["LotNumber"].ToString().Length > 40)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.LotNumber = dr["LotNumber"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                InvLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            InvLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    #endregion
                                }


                                if (dt.Columns.Contains("InvoiceLineClassFullName"))
                                {
                                    #region Validations of Invoice Line Class Full name
                                    if (dr["InvoiceLineClassFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["InvoiceLineClassFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Invoice Line Class name (" + dr["InvoiceLineClassFullName"].ToString() + ") is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                                    if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                                    {
                                                        InvLine.ClassRef.FullName = null;
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                                    if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                                    {
                                                        InvLine.ClassRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                                if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                                {
                                                    InvLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());

                                            if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                            {
                                                InvLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["Amount"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            InvLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                       CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                       CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Amount = string.Format("{0:000000.00}", amount);

                                                }
                                            }
                                            //Check if EstLine.Amount is null
                                            if (InvLine.Amount == null)
                                            {
                                                InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("InventorySiteRefFullName"))
                            {
                                #region Validations of Inventory Site Ref Full Name
                                if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (InvLine.InventorySiteRef.FullName == null)
                                                    InvLine.InventorySiteRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                                if (InvLine.InventorySiteRef.FullName == null)
                                                    InvLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InvLine.InventorySiteRef.FullName == null)
                                                InvLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (InvLine.InventorySiteRef.FullName == null)
                                            InvLine.InventorySiteRef.FullName = null;
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ServiceDate"))
                            {
                                #region validations of ServiceDate
                                if (dr["ServiceDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["ServiceDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.ServiceDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.ServiceDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                InvLine.ServiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        InvLine.ServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                {
                                    #region Validations of sales tax code Full name
                                    if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["SalesTaxCodeRefFullName"].ToString().Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This sales tax code name (" + dr["SalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                    if (InvLine.SalesTaxCodeRef.FullName == null)
                                                        InvLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                    if (InvLine.SalesTaxCodeRef.FullName == null)
                                                        InvLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                if (InvLine.SalesTaxCodeRef.FullName == null)
                                                    InvLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                            if (InvLine.SalesTaxCodeRef.FullName == null)
                                                InvLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                            {
                                if (dt.Columns.Contains("IsTaxable"))
                                {
                                    #region Validations of IsTaxable
                                    if (dr["IsTaxable"].ToString() != "<None>")
                                    {

                                        int result = 0;
                                        if (int.TryParse(dr["IsTaxable"].ToString(), out result))
                                        {

                                            InvLine.IsTaxable = Convert.ToInt32(dr["IsTaxable"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsTaxable"].ToString().ToLower() == "true")
                                            {

                                                InvLine.IsTaxable = dr["IsTaxable"].ToString().ToLower(); ;
                                            }
                                            else
                                            {
                                                if (dr["IsTaxable"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else

                                                    InvLine.IsTaxable = dr["IsTaxable"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsTaxable (" + dr["IsTaxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(results) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;

                                                        InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                    }
                                                }
                                                else
                                                {

                                                    InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                }
                                            }

                                        }
                                    }
                                    #endregion
                                }

                            }
                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                InvLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                InvLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            InvLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        InvLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion
                            }

                            //New Feature::601

                            if (dt.Columns.Contains("PriceLevelRefFullName"))
                            {
                                #region Validations of PriceLevel FullName
                                if (dr["PriceLevelRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["PriceLevelRefFullName"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (InvLine.PriceLevelRef.FullName == null)
                                                    InvLine.PriceLevelRef.FullName = null;
                                                else
                                                    InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString().Substring(0, 100));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                                if (InvLine.PriceLevelRef.FullName == null)
                                                    InvLine.PriceLevelRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                            if (InvLine.PriceLevelRef.FullName == null)
                                                InvLine.PriceLevelRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                        if (InvLine.PriceLevelRef.FullName == null)
                                            InvLine.PriceLevelRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Other1"))
                            {
                                #region Validations for Other1

                                if (dr["Other1"].ToString() != string.Empty)
                                {
                                    if (dr["Other1"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Other1"].ToString();
                                                InvLine.Other1 = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Other1"].ToString();
                                                InvLine.Other1 = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Other1"].ToString();
                                            InvLine.Other1 = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["other1"].ToString();
                                        InvLine.Other1 = strDesc;
                                    }
                                }


                                #endregion
                            }
                            if (dt.Columns.Contains("Other2"))
                            {
                                #region Validations for Other2

                                if (dr["Other2"].ToString() != string.Empty)
                                {
                                    if (dr["Other2"].ToString().Length > 29)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Other2"].ToString();
                                                InvLine.Other2 = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Other2"].ToString();
                                                InvLine.Other2 = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Other2"].ToString();
                                            InvLine.Other2 = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["other2"].ToString();
                                        InvLine.Other2 = strDesc;
                                    }
                                }


                                #endregion
                            }
                           

                            //Bug no 331
                            QuickBookStreams.DataExt dataext = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("CustomFieldName1"))
                            {
                                dataext.OwnerID = "0";
                                #region Validations for CustomFieldName1
                                if (dr["CustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName1 ( " + dr["CustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldName1"].ToString().Substring(0, 4095);
                                                dataext.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldName1"].ToString();
                                                dataext.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName1"].ToString();
                                            dataext.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName1"].ToString();
                                        dataext.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue1"))
                            {
                                #region Validations for CustomFieldValue1
                                if (dr["CustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue1 ( " + dr["CustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldValue1"].ToString().Substring(0, 4095);
                                                dataext.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldValue1"].ToString();
                                                dataext.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue1"].ToString();
                                            dataext.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue1"].ToString();
                                        dataext.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext.DataExtName != null && dataext.DataExtValue != null && dataext.OwnerID != null)
                            {
                                InvLine.DataExt.Add(dataext);
                            }

                            QuickBookStreams.DataExt dataext1 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("CustomFieldName2"))
                            {
                                dataext1.OwnerID = "0";
                                #region Validations for CustomFieldName2
                                if (dr["CustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName2 ( " + dr["CustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldName2"].ToString().Substring(0, 4095);
                                                dataext1.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldName2"].ToString();
                                                dataext1.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName2"].ToString();
                                            dataext1.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName2"].ToString();
                                        dataext1.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue2"))
                            {
                                #region Validations for CustomFieldValue2
                                if (dr["CustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue2 ( " + dr["CustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldValue2"].ToString().Substring(0, 4095);
                                                dataext1.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldValue2"].ToString();
                                                dataext1.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue2"].ToString();
                                            dataext1.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue2"].ToString();
                                        dataext1.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext1.DataExtName != null && dataext1.DataExtValue != null && dataext1.OwnerID != null)
                            {
                                InvLine.DataExt.Add(dataext1);
                            }

                            ///443
                            QuickBookStreams.DataExt dataext4 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("CustomFieldName3"))
                            {
                                dataext4.OwnerID = "0";
                                #region Validations for CustomFieldName3
                                if (dr["CustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldName3"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldName3 ( " + dr["CustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldName3"].ToString().Substring(0, 4095);
                                                dataext4.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldName3"].ToString();
                                                dataext4.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldName3"].ToString();
                                            dataext4.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName3"].ToString();
                                        dataext4.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("CustomFieldValue3"))
                            {
                                #region Validations for CustomFieldValue3
                                if (dr["CustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["CustomFieldValue3"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CustomFieldValue3 ( " + dr["CustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["CustomFieldValue3"].ToString().Substring(0, 4095);
                                                dataext4.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["CustomFieldValue3"].ToString();
                                                dataext4.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["CustomFieldValue3"].ToString();
                                            dataext4.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue3"].ToString();
                                        dataext4.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext4.DataExtName != null && dataext4.DataExtValue != null && dataext4.OwnerID != null)
                            {
                                InvLine.DataExt.Add(dataext4);
                            }
                            //end bug no 331
                            //11.4 444
                            if (dt.Columns.Contains("LinkToSalesOrder"))
                            {
                                #region Validations of LinkToSalesOrder
                                if (dr["LinkToSalesOrder"].ToString() != string.Empty)
                                {
                                    string strLinkToTxnID = dr["LinkToSalesOrder"].ToString();
                                    InvLine.LinkToSalesOrder = dr["LinkToSalesOrder"].ToString();
                                }
                                #endregion

                            }  

                            if (InvLine.ItemRef != null)
                            Invoice.InvoiceLineAdd.Add(InvLine);                           
                            #endregion

                            //new changes for version 6.0
                            DataProcessingBlocks.InvoiceLineGroupAdd InvGroup = new DataProcessingBlocks.InvoiceLineGroupAdd();

                            #region Adding Invoice group
                           
                                if (dt.Columns.Contains("ItemGroupFullName"))
                                {

                                    #region Validations of item Full name
                                    if (dr["ItemGroupFullName"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupFullName"].ToString().Length > 1000)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item Group full name (" + dr["ItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                    if (InvGroup.ItemGroupRef.FullName == null)
                                                        InvGroup.ItemGroupRef.FullName = null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                    if (InvGroup.ItemGroupRef.FullName == null)
                                                        InvGroup.ItemGroupRef.FullName = null;
                                                }
                                            }
                                            else
                                            {
                                                InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                if (InvGroup.ItemGroupRef.FullName == null)
                                                    InvGroup.ItemGroupRef.FullName = null;
                                            }

                                        }
                                        else
                                        {
                                            InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                            if (InvGroup.ItemGroupRef.FullName == null)
                                                InvGroup.ItemGroupRef.FullName = null;
                                        }

                                    }
                                    #endregion
                                }

                                if (dt.Columns.Contains("ItemGroupQuantity"))
                                {
                                    #region Validations for Quantity
                                    if (dr["ItemGroupQuantity"].ToString() != string.Empty)
                                    {
                                        string strQuantity = dr["ItemGroupQuantity"].ToString();
                                        InvGroup.Quantity = strQuantity;

                                    }

                                    #endregion

                                }

                                if (dt.Columns.Contains("ItemGroupUOM"))
                                {
                                    #region Validations for UnitOfMeasure
                                    if (dr["ItemGroupUOM"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupUOM"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                    InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                    InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                                }
                                            }
                                            else
                                            {
                                                string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                            InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }

                                    #endregion
                                }
                                //Bug no 331

                                QuickBookStreams.DataExt dataext2 = new QuickBookStreams.DataExt();

                                if (dt.Columns.Contains("ItemGroupCustomFieldName1"))
                                {
                                    dataext2.OwnerID = "0";
                                    #region Validations for ItemGroupCustomFieldName1
                                    if (dr["ItemGroupCustomFieldName1"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName1"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName1 ( " + dr["ItemGroupCustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName1"].ToString().Substring(0, 4095);
                                                    dataext2.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                    dataext2.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                dataext2.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                            dataext2.DataExtName = strDesc;
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("ItemGroupCustomFieldValue1"))
                                {
                                    #region Validations for ItemGroupCustomFieldValue1
                                    if (dr["ItemGroupCustomFieldValue1"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue1"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue1 ( " + dr["ItemGroupCustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue1"].ToString().Substring(0, 4095);
                                                    dataext2.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                    dataext2.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                dataext2.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                            dataext2.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dataext2.DataExtName != null && dataext2.DataExtValue != null && dataext2.OwnerID != null)
                                { 
                                    InvGroup.DataExt.Add(dataext2);
                                }

                                QuickBookStreams.DataExt dataext3 = new QuickBookStreams.DataExt();
                                if (dt.Columns.Contains("ItemGroupCustomFieldName2"))
                                {
                                    dataext3.OwnerID = "0";
                                    #region Validations for ItemGroupCustomFieldName2
                                    if (dr["ItemGroupCustomFieldName2"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName2"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName2 ( " + dr["ItemGroupCustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName2"].ToString().Substring(0, 4095);
                                                    dataext3.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                    dataext3.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                dataext3.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                            dataext3.DataExtName = strDesc;
                                        }
                                    }

                                    #endregion
                                }

                                if (dt.Columns.Contains("ItemGroupCustomFieldValue2"))
                                {
                                    #region Validations for ItemGroupCustomFieldValue2
                                    if (dr["ItemGroupCustomFieldValue2"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue2"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue2 ( " + dr["ItemGroupCustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue2"].ToString().Substring(0, 4095);
                                                    dataext3.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                    dataext3.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                dataext3.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                            dataext3.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dataext3.DataExtName != null && dataext3.DataExtValue != null && dataext3.OwnerID != null)
                                { 
                                    InvGroup.DataExt.Add(dataext3);
                                }

                                /// 439
                                /// 
                                QuickBookStreams.DataExt dataext5 = new QuickBookStreams.DataExt();
                                if (dt.Columns.Contains("ItemGroupCustomFieldName3"))
                                {
                                    dataext5.OwnerID = "0";
                                    #region Validations for ItemGroupCustomFieldName3
                                    if (dr["ItemGroupCustomFieldName3"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldName3"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldName3 ( " + dr["ItemGroupCustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldName3"].ToString().Substring(0, 4095);
                                                    dataext5.DataExtName = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                    dataext5.DataExtName = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                dataext5.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                            dataext5.DataExtName = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dt.Columns.Contains("ItemGroupCustomFieldValue3"))
                                {
                                    #region Validations for ItemGroupCustomFieldValue3
                                    if (dr["ItemGroupCustomFieldValue3"].ToString() != string.Empty)
                                    {
                                        if (dr["ItemGroupCustomFieldValue3"].ToString().Length > 31)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ItemGroupCustomFieldValue3 ( " + dr["ItemGroupCustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strDesc = dr["ItemGroupCustomFieldValue3"].ToString().Substring(0, 4095);
                                                    dataext5.DataExtValue = strDesc;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                    dataext5.DataExtValue = strDesc;
                                                }
                                            }
                                            else
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                dataext5.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                            dataext5.DataExtValue = strDesc;
                                        }
                                    }

                                    #endregion
                                }
                                if (dataext5.DataExtName != null && dataext5.DataExtValue != null && dataext5.OwnerID != null)
                                { 
                                    InvGroup.DataExt.Add(dataext5);
                                }
                                //end bug no 331

                            /// bug no 450
                                if (InvGroup.ItemGroupRef != null)
                                {

                                    if (InvGroup.ItemGroupRef.FullName != null)
                                        Invoice.InvoiceLineGroupAdd.Add(InvGroup);
                                }
                            

                            #endregion                          

                            if (dt.Columns.Contains("InventorySiteLocationRef"))
                            {
                                #region Validations of InventorySiteLocationRef

                                if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                                {
                                    if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (InvLine.InventorySiteLocationRef.FullName == null)
                                                    InvLine.InventorySiteLocationRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                                if (InvLine.InventorySiteLocationRef.FullName == null)
                                                    InvLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (InvLine.InventorySiteLocationRef.FullName == null)
                                                InvLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (InvLine.InventorySiteLocationRef.FullName == null)
                                            InvLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if(InVoiceQBEntryCollection.inv_cnt == datatbl_cnt-1)
                            {
                                    refcnt = 1;
                            }

                             if (refcnt == 1)
                             {
                                 #region Invoice Line add for Freight

                                 if (dt.Columns.Contains("Freight"))
                                 {
                                     if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                     {
                                         InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                         //Adding freight charge item to invoice line.
                                         InvLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                         InvLine.ItemRef.FullName = defaultSettings.Frieght;
                                         //Adding freight charge amount to invoice line.
                                         #region Commented code for Amount which is not required.
                                         //    #region Validations for Freight Amount
                                         //     if (dr["Freight"].ToString() != string.Empty)
                                         //    {
                                         //        decimal amount;
                                         //        if (!decimal.TryParse(dr["Freight"].ToString(), out amount))
                                         //        {
                                         //            if (isIgnoreAll == false)
                                         //            {
                                         //                string strMessages = "This Freight amount ( " + dr["Freight"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                         //                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                         //                if (Convert.ToString(result) == "Cancel")
                                         //                {
                                         //                    continue;
                                         //                }
                                         //                if (Convert.ToString(result) == "No")
                                         //                {
                                         //                    return null;
                                         //                }
                                         //                if (Convert.ToString(result) == "Ignore")
                                         //                {
                                         //                    string strAmount = dr["Freight"].ToString();
                                         //                    InvLine.Amount = strAmount;
                                         //                }
                                         //                if (Convert.ToString(result) == "Abort")
                                         //                {
                                         //                    isIgnoreAll = true;
                                         //                    string strAmount = dr["Freight"].ToString();
                                         //                    InvLine.Amount = strAmount;
                                         //                }
                                         //            }
                                         //            else
                                         //            {
                                         //                string strAmount = dr["Freight"].ToString();
                                         //                InvLine.Amount = strAmount;
                                         //            }
                                         //        }
                                         //        else
                                         //        {
                                         //            if (defaultSettings.GrossToNet == "1")
                                         //            {
                                         //                if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                         //                {
                                         //                    if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                         //                    {
                                         //                        decimal Amount = Convert.ToDecimal(dr["Freight"].ToString());
                                         //                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                         //                           CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                         //                           CommonUtilities.GetInstance().CountryVersion == "CA")
                                         //                        {
                                         //                            //decimal TaxRate = 10;
                                         //                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                         //                            amount = Amount / (1 + (TaxRate / 100));
                                         //                        }

                                         //                        InvLine.Amount = string.Format("{0:000000.00}", amount);

                                         //                    }
                                         //                }
                                         //                //Check if EstLine.Amount is null
                                         //                if (InvLine.Amount == null)
                                         //                {
                                         //                    InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Freight"].ToString()));
                                         //                }
                                         //            }
                                         //            else
                                         //            {
                                         //                InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Freight"].ToString()));
                                         //            }
                                         //        }
                                         //    }
                                         //    Invoice.InvoiceLineAdd.Add(InvLine);
                                         //}
                                         //    #endregion
                                         #endregion

                                         #region Validations for Rate
                                         if (dr["Freight"].ToString() != string.Empty)
                                         {
                                             decimal rate = 0;
                                             //decimal amount;
                                             if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                             {
                                                 if (isIgnoreAll == false)
                                                 {
                                                     string strMessages = "This Freight Rate ( " + dr["Freight"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                     DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                     if (Convert.ToString(result) == "Cancel")
                                                     {
                                                         continue;
                                                     }
                                                     if (Convert.ToString(result) == "No")
                                                     {
                                                         return null;
                                                     }
                                                     if (Convert.ToString(result) == "Ignore")
                                                     {
                                                         decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                         InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                     }
                                                     if (Convert.ToString(result) == "Abort")
                                                     {
                                                         isIgnoreAll = true;
                                                         decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                         InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                     InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                 }
                                             }
                                             else
                                             {
                                                 if (defaultSettings.GrossToNet == "1")
                                                 {
                                                     if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                     {
                                                         if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                         {
                                                             decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                             if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "CA")
                                                             {
                                                                 //decimal TaxRate = 10;
                                                                 decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                 rate = Rate / (1 + (TaxRate / 100));
                                                             }

                                                             InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                         }
                                                     }
                                                     //Check if InvoiceLine.Rate is null
                                                     if (InvLine.Rate == null)
                                                     {
                                                         InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                 }
                                             }
                                         }
                                         if (InvLine.ItemRef != null)
                                         Invoice.InvoiceLineAdd.Add(InvLine);

                                         #endregion
                                     }
                                 }

                                 #endregion

                                 #region Invoice Line add for Insurance

                                 if (dt.Columns.Contains("Insurance"))
                                 {
                                     if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                     {
                                         InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                         //Adding freight charge item to invoice line.
                                         InvLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                         InvLine.ItemRef.FullName = defaultSettings.Insurance;                                        

                                         #region Validations for Rate
                                         if (dr["Insurance"].ToString() != string.Empty)
                                         {
                                             decimal rate = 0;
                                             //decimal amount;
                                             if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                             {
                                                 if (isIgnoreAll == false)
                                                 {
                                                     string strMessages = "This Insurance Rate ( " + dr["Insurance"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                     DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                     if (Convert.ToString(result) == "Cancel")
                                                     {
                                                         continue;
                                                     }
                                                     if (Convert.ToString(result) == "No")
                                                     {
                                                         return null;
                                                     }
                                                     if (Convert.ToString(result) == "Ignore")
                                                     {
                                                         decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                         InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                     }
                                                     if (Convert.ToString(result) == "Abort")
                                                     {
                                                         isIgnoreAll = true;
                                                         decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                         InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                     InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                 }
                                             }
                                             else
                                             {
                                                 if (defaultSettings.GrossToNet == "1")
                                                 {
                                                     if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                     {
                                                         if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                         {
                                                             decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                             if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "CA")
                                                             {
                                                                 //decimal TaxRate = 10;
                                                                 decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                 rate = Rate / (1 + (TaxRate / 100));
                                                             }

                                                             InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                         }
                                                     }
                                                     //Check if InvoiceLine.Rate is null
                                                     if (InvLine.Rate == null)
                                                     {
                                                         InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                 }
                                             }
                                         }
                                         if (InvLine.ItemRef != null)
                                         Invoice.InvoiceLineAdd.Add(InvLine);

                                         #endregion
                                     }
                                 }

                                 #endregion

                                 #region Invoice Line Add  for Discount

                                 if (dt.Columns.Contains("Discount"))
                                 {
                                     if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                     {
                                         InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                         //Adding freight charge item to invoice line.
                                         InvLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                         InvLine.ItemRef.FullName = defaultSettings.Discount;
                                         //Adding freight charge amount to invoice line.
                                         #region Commented code for Amount which is not required
                                         //#region Validations for Freight Amount
                                         //if (dr["Discount"].ToString() != string.Empty)
                                         //{
                                         //    decimal amount;
                                         //    if (!decimal.TryParse(dr["Discount"].ToString(), out amount))
                                         //    {
                                         //        if (isIgnoreAll == false)
                                         //        {
                                         //            string strMessages = "This Discount amount ( " + dr["Discount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                         //            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                         //            if (Convert.ToString(result) == "Cancel")
                                         //            {
                                         //                continue;
                                         //            }
                                         //            if (Convert.ToString(result) == "No")
                                         //            {
                                         //                return null;
                                         //            }
                                         //            if (Convert.ToString(result) == "Ignore")
                                         //            {
                                         //                string strAmount = dr["Discount"].ToString();
                                         //                InvLine.Amount = strAmount;
                                         //            }
                                         //            if (Convert.ToString(result) == "Abort")
                                         //            {
                                         //                isIgnoreAll = true;
                                         //                string strAmount = dr["Discount"].ToString();
                                         //                InvLine.Amount = strAmount;
                                         //            }
                                         //        }
                                         //        else
                                         //        {
                                         //            string strAmount = dr["Discount"].ToString();
                                         //            InvLine.Amount = strAmount;
                                         //        }
                                         //    }
                                         //    else
                                         //    {
                                         //        if (defaultSettings.GrossToNet == "1")
                                         //        {
                                         //            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                         //            {
                                         //                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                         //                {
                                         //                    decimal Amount = Convert.ToDecimal(dr["Discount"].ToString());
                                         //                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                         //                       CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                         //                       CommonUtilities.GetInstance().CountryVersion == "CA")
                                         //                    {
                                         //                        //decimal TaxRate = 10;
                                         //                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                         //                        amount = Amount / (1 + (TaxRate / 100));
                                         //                    }

                                         //                    InvLine.Amount = string.Format("{0:000000.00}", amount);

                                         //                }
                                         //            }
                                         //            //Check if EstLine.Amount is null
                                         //            if (InvLine.Amount == null)
                                         //            {
                                         //                InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Discount"].ToString()));
                                         //            }
                                         //        }
                                         //        else
                                         //        {
                                         //            InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Discount"].ToString()));
                                         //        }
                                         //    }
                                         //}

                                         //Invoice.InvoiceLineAdd.Add(InvLine);
                                         //#endregion
                                         #endregion

                                         #region Validations for Rate
                                         if (dr["Discount"].ToString() != string.Empty)
                                         {
                                             decimal rate = 0;
                                             //decimal amount;
                                             if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                             {
                                                 if (isIgnoreAll == false)
                                                 {
                                                     string strMessages = "This Discount Rate ( " + dr["Discount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                     DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                     if (Convert.ToString(result) == "Cancel")
                                                     {
                                                         continue;
                                                     }
                                                     if (Convert.ToString(result) == "No")
                                                     {
                                                         return null;
                                                     }
                                                     if (Convert.ToString(result) == "Ignore")
                                                     {
                                                         decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                         InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                     }
                                                     if (Convert.ToString(result) == "Abort")
                                                     {
                                                         isIgnoreAll = true;
                                                         decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                         InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                     InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                                 }
                                             }
                                             else
                                             {
                                                 if (defaultSettings.GrossToNet == "1")
                                                 {
                                                     if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                     {
                                                         if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                         {
                                                             decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                             if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "CA")
                                                             {
                                                                 //decimal TaxRate = 10;
                                                                 decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                 rate = Rate / (1 + (TaxRate / 100));
                                                             }

                                                             InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                         }
                                                     }
                                                     //Check if InvoiceLine.Rate is null
                                                     if (InvLine.Rate == null)
                                                     {
                                                         InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                 }
                                             }
                                         }
                                         if (InvLine.ItemRef != null)
                                         Invoice.InvoiceLineAdd.Add(InvLine);

                                         #endregion
                                     }
                                 }

                                 #endregion

                                 //bug no. 410
                                 #region invoice Line add for SalesTax

                                 if (dt.Columns.Contains("SalesTax"))
                                 {
                                     if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                                     {
                                         InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                         //Adding SalesTax charge item to Invoive line.                                          

                                         InvLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                         InvLine.ItemRef.FullName = defaultSettings.SalesTax;

                                         //Adding SalesTax charge amount to Invoive line.

                                         #region Validations for Rate
                                         if (dr["SalesTax"].ToString() != string.Empty)
                                         {
                                             decimal rate = 0;
                                             //decimal amount;
                                             if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                             {
                                                 if (isIgnoreAll == false)
                                                 {
                                                     string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                     DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                     if (Convert.ToString(result) == "Cancel")
                                                     {
                                                         continue;
                                                     }
                                                     if (Convert.ToString(result) == "No")
                                                     {
                                                         return null;
                                                     }
                                                     if (Convert.ToString(result) == "Ignore")
                                                     {
                                                         string strAmount = dr["SalesTax"].ToString();
                                                         InvLine.Amount = strAmount;
                                                     }
                                                     if (Convert.ToString(result) == "Abort")
                                                     {
                                                         isIgnoreAll = true;
                                                         string strAmount = dr["SalesTax"].ToString();
                                                         InvLine.Amount = strAmount;
                                                     }
                                                 }
                                                 else
                                                 {
                                                     string strAmount = dr["SalesTax"].ToString();
                                                     InvLine.Amount = strAmount;
                                                 }
                                             }
                                             else
                                             {
                                                 if (defaultSettings.GrossToNet == "1")
                                                 {
                                                     if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                                     {
                                                         if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                         {
                                                             decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                             if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                 CommonUtilities.GetInstance().CountryVersion == "CA")
                                                             {
                                                                 //decimal TaxRate = 10;
                                                                 decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                 rate = Rate / (1 + (TaxRate / 100));
                                                             }

                                                             InvLine.Amount = string.Format("{0:000000.00}", rate);
                                                         }
                                                     }
                                                     //Check if InvoiceLine.Rate is null
                                                     if (InvLine.Amount == null)
                                                     {
                                                         InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                     }
                                                 }
                                                 else
                                                 {
                                                     InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                 }

                                             }
                                         }
                                         if (InvLine.ItemRef != null)
                                         Invoice.InvoiceLineAdd.Add(InvLine);
                                         #endregion
                                     }
                                 }


                                 #endregion
                                 refcnt = 0;
                             }

                            #endregion
                            //coll.Add(Invoice);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Without Adding Ref Number
                        InvoiceQBEntry Invoice = new InvoiceQBEntry();
                        if (dt.Columns.Contains("CustomerRefFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["CustomerRefFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["CustomerRefFullName"].ToString();
                                if (strCust.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (Invoice.CustomerRef.FullName == null)
                                            {
                                                Invoice.CustomerRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                            if (Invoice.CustomerRef.FullName == null)
                                            {
                                                Invoice.CustomerRef.FullName = null;
                                            }
                                        }


                                    }
                                    else
                                    {
                                        Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                        if (Invoice.CustomerRef.FullName == null)
                                        {
                                            Invoice.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Invoice.CustomerRef = new CustomerRef(dr["CustomerRefFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (Invoice.CustomerRef.FullName == null)
                                    {
                                        Invoice.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        ///bug 442 11.4 
                        if (dt.Columns.Contains("Currency"))
                        {
                            #region Validations of Currency Full name
                            if (dr["Currency"].ToString() != string.Empty)
                            {
                                string strCust = dr["Currency"].ToString();
                                if (strCust.Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Invoice.CurrencyRef.FullName == null)
                                            {
                                                Invoice.CurrencyRef.FullName = null;
                                            }
                                            else
                                            {
                                                Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Invoice.CurrencyRef.FullName == null)
                                            {
                                                Invoice.CurrencyRef.FullName = null;
                                            }
                                        }


                                    }
                                    else
                                    {
                                        Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        if (Invoice.CurrencyRef.FullName == null)
                                        {
                                            Invoice.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Invoice.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (Invoice.CurrencyRef.FullName == null)
                                    {
                                        Invoice.CurrencyRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ClassRefFullName"))
                        {
                            #region Validations of Class Full name
                            if (dr["ClassRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ClassRefFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                            {
                                                Invoice.ClassRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                            {
                                                Invoice.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                        if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                        {
                                            Invoice.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Invoice.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                    //string strClassFullName = string.Empty;
                                    if (Invoice.ClassRef.FullName == null && Invoice.ClassRef.ListID == null)
                                    {
                                        Invoice.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("AccountRef"))
                        {
                            #region Validations of Template Full name
                            if (dr["AccountRef"].ToString() != string.Empty)
                            {
                                if (dr["AccountRef"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Template full name (" + dr["AccountRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                            if (Invoice.ARAccountRef.FullName == null)
                                                Invoice.ARAccountRef.FullName = null;
                                            else
                                                Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString().Substring(0, 100));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                            if (Invoice.ARAccountRef.FullName == null)
                                                Invoice.ARAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                        if (Invoice.ARAccountRef.FullName == null)
                                            Invoice.ARAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Invoice.ARAccountRef = new ARAccountRef(dr["AccountRef"].ToString());
                                    if (Invoice.ARAccountRef.FullName == null)
                                        Invoice.ARAccountRef.FullName = null;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TemplateRefFullName"))
                        {
                            #region Validations of Template Full name
                            if (dr["TemplateRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["TemplateRefFullName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Template full name (" + dr["TemplateRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (Invoice.TemplateRef.FullName == null)
                                                Invoice.TemplateRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                            if (Invoice.TemplateRef.FullName == null)
                                                Invoice.TemplateRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                        if (Invoice.TemplateRef.FullName == null)
                                            Invoice.TemplateRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Invoice.TemplateRef = new TemplateRef(dr["TemplateRefFullName"].ToString());
                                    if (Invoice.TemplateRef.FullName == null)
                                        Invoice.TemplateRef.FullName = null;
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("SalesRepRefFullName"))
                        {
                            #region Validations of SalesRepRef Full name
                            if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["SalesRepRefFullName"].ToString().Length > 5)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesRepRef full name (" + dr["SalesRepRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (Invoice.SalesRepRef.FullName == null)
                                                Invoice.SalesRepRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (Invoice.SalesRepRef.FullName == null)
                                                Invoice.SalesRepRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                        if (Invoice.SalesRepRef.FullName == null)
                                            Invoice.SalesRepRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Invoice.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                    if (Invoice.SalesRepRef.FullName == null)
                                        Invoice.SalesRepRef.FullName = null;
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("FOB"))
                        {
                            #region Validations of PONumber
                            if (dr["FOB"].ToString() != string.Empty)
                            {
                                string strCust = dr["FOB"].ToString();
                                if (strCust.Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This FOB (" + dr["FOB"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.FOB = dr["FOB"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.FOB = dr["FOB"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.FOB = dr["FOB"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.FOB = dr["FOB"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipDate"))
                        {
                            #region validations of ShipDate
                            if (dr["ShipDate"].ToString() != "<None>" || dr["ShipDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["ShipDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShipDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.ShipDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.ShipDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Invoice.ShipDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        InvoiceDt = dttest;
                                        Invoice.ShipDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    InvoiceDt = Convert.ToDateTime(datevalue);
                                    Invoice.ShipDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Invoice.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        InvoiceDt = dttest;
                                        Invoice.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    InvoiceDt = Convert.ToDateTime(datevalue);
                                    Invoice.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("InvoiceRefNumber"))
                        {
                            #region Validations of Ref Number
                            if (datevalue != string.Empty)
                                Invoice.InvoiceDate = InvoiceDt;

                            if (dr["InvoiceRefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["InvoiceRefNumber"].ToString();
                                if (strRefNum.Length > 11)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["InvoiceRefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                                    }
                                }
                                else
                                    Invoice.RefNumber = dr["InvoiceRefNumber"].ToString();
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("IsPending"))
                        {
                            #region Validations of IsPending
                            if (dr["IsPending"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsPending"].ToString(), out result))
                                {
                                    Invoice.IsPending = Convert.ToInt32(dr["IsPending"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsPending"].ToString().ToLower() == "true")
                                    {
                                        Invoice.IsPending = dr["IsPending"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsPending"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Invoice.IsPending = dr["IsPending"].ToString().ToLower(); ;
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsPending (" + dr["IsPending"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Invoice.IsPending = dr["IsPending"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.IsPending = dr["IsPending"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.IsPending = dr["IsPending"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("IsFinanceCharge"))
                        {
                            #region Validations of IsFinanceCharge
                            if (dr["IsFinanceCharge"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsFinanceCharge"].ToString(), out result))
                                {
                                    Invoice.IsFinanceCharge = Convert.ToInt32(dr["IsFinanceCharge"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsFinanceCharge"].ToString().ToLower() == "true")
                                    {
                                        Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsFinanceCharge"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsFinanceCharge (" + dr["IsFinanceCharge"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.IsFinanceCharge = dr["IsFinanceCharge"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("PONumber"))
                        {
                            #region Validations of PONumber
                            if (dr["PONumber"].ToString() != string.Empty)
                            {
                                string strCust = dr["PONumber"].ToString();
                                if (strCust.Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PONumber (" + dr["PONumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.PONumber = dr["PONumber"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.PONumber = dr["PONumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.PONumber = dr["PONumber"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.PONumber = dr["PONumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TermsRefFullName"))
                        {
                            #region Validations of Terms Full name
                            if (dr["TermsRefFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["TermsRefFullName"].ToString();
                                if (strCust.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Terms fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                            if (Invoice.TermsRef.FullName == null)
                                            {
                                                Invoice.TermsRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                            if (Invoice.TermsRef.FullName == null)
                                            {
                                                Invoice.TermsRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());
                                        if (Invoice.TermsRef.FullName == null)
                                        {
                                            Invoice.TermsRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Invoice.TermsRef = new QuickBookEntities.TermsRef(dr["TermsRefFullName"].ToString());

                                    if (Invoice.TermsRef.FullName == null)
                                    {
                                        Invoice.TermsRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("ShipMethodRefFullName"))
                        {
                            #region Validations of ShipMethodRef Full name
                            if (dr["ShipMethodRefFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ShipMethodRefFullName"].ToString();
                                if (strCust.Length > 15)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Method Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                            if (Invoice.ShipMethodRef.FullName == null)
                                            {
                                                Invoice.ShipMethodRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());
                                            if (Invoice.ShipMethodRef.FullName == null)
                                            {
                                                Invoice.ShipMethodRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());

                                        if (Invoice.ShipMethodRef.FullName == null)
                                        {
                                            Invoice.ShipMethodRef.FullName = null;
                                        }
                                    }

                                }
                                else
                                {
                                    Invoice.ShipMethodRef = new ShipMethodRef(dr["ShipMethodRefFullName"].ToString());

                                    if (Invoice.ShipMethodRef.FullName == null)
                                    {
                                        Invoice.ShipMethodRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        DateTime NewDueDt = new DateTime();
                        if (dt.Columns.Contains("DueDate"))
                        {
                            #region validations of DueDate
                            if (dr["DueDate"].ToString() != "<None>" || dr["DueDate"].ToString() != string.Empty)
                            {
                                string duevalue = dr["DueDate"].ToString();
                                if (!DateTime.TryParse(duevalue, out NewDueDt))
                                {

                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(duevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DueDate (" + duevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Invoice.DueDate = duevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.DueDate = duevalue;
                                            }
                                        }
                                        else
                                        {
                                            Invoice.DueDate = duevalue;
                                        }
                                    }
                                    else
                                    {
                                        Invoice.DueDate = dttest.ToString("yyyy-MM-dd");
                                    }


                                }
                                else
                                {
                                    Invoice.DueDate = DateTime.Parse(duevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ItemSalesTaxRefFullName"))
                        {
                            #region Validations of ItemSalesTax Full name
                            if (dr["ItemSalesTaxRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemSalesTaxRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemTaxRef full name (" + dr["ItemSalesTaxRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                            if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                            {
                                                Invoice.ItemSalesTaxRef.FullName = null;
                                            }
                                            else
                                                Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString().Substring(0, 31));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                            if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                            {
                                                Invoice.ItemSalesTaxRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                        if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                        {
                                            Invoice.ItemSalesTaxRef.FullName = null;
                                        }

                                    }

                                }
                                else
                                {
                                    Invoice.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                                    //string strClassFullName = string.Empty;
                                    if (Invoice.ItemSalesTaxRef.FullName == null && Invoice.ItemSalesTaxRef.ListID == null)
                                    {
                                        Invoice.ItemSalesTaxRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            Invoice.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            Invoice.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Invoice.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    Invoice.Memo = strMemo;
                                }
                            }

                            #endregion
                        }

                        QuickBookEntities.BillAddress BillAddressItem = new BillAddress();
                        if (dt.Columns.Contains("BillAddr1"))
                        {
                            #region Validations of Bill Addr1
                            if (dr["BillAddr1"].ToString() != string.Empty)
                            {
                                if (dr["BillAddr1"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Add1 (" + dr["BillAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr1 = dr["BillAddr1"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddr2"))
                        {
                            #region Validations of Bill Addr2
                            if (dr["BillAddr2"].ToString() != string.Empty)
                            {
                                if (dr["BillAddr2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Add2 (" + dr["BillAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr2 = dr["BillAddr2"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("BillAddr3"))
                        {
                            #region Validations of Bill Addr3
                            if (dr["BillAddr3"].ToString() != string.Empty)
                            {
                                if (dr["BillAddr3"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Add3 (" + dr["BillAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr3 = dr["BillAddr3"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("BillAddr4"))
                        {
                            #region Validations of Bill Addr4
                            if (dr["BillAddr4"].ToString() != string.Empty)
                            {
                                if (dr["BillAddr4"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Add4 (" + dr["BillAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr4 = dr["BillAddr4"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("BillAddr5"))
                        {
                            #region Validations of Bill Addr5
                            if (dr["BillAddr5"].ToString() != string.Empty)
                            {
                                if (dr["BillAddr5"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Add5 (" + dr["BillAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Addr5 = dr["BillAddr5"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("BillCity"))
                        {
                            #region Validations of Bill City
                            if (dr["BillCity"].ToString() != string.Empty)
                            {
                                if (dr["BillCity"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill City (" + dr["BillCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.City = dr["BillCity"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.City = dr["BillCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.City = dr["BillCity"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.City = dr["BillCity"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("BillState"))
                        {
                            #region Validations of Bill State
                            if (dr["BillState"].ToString() != string.Empty)
                            {
                                if (dr["BillState"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill State (" + dr["BillState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.State = dr["BillState"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.State = dr["BillState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.State = dr["BillState"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.State = dr["BillState"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("BillPostalCode"))
                        {
                            #region Validations of Bill Postal Code
                            if (dr["BillPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["BillPostalCode"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Postal Code (" + dr["BillPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.PostalCode = dr["BillPostalCode"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("BillCountry"))
                        {
                            #region Validations of Bill Country
                            if (dr["BillCountry"].ToString() != string.Empty)
                            {
                                if (dr["BillCountry"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Country (" + dr["BillCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Country = dr["BillCountry"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Country = dr["BillCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Country = dr["BillCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Country = dr["BillCountry"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("BillNote"))
                        {
                            #region Validations of Bill Note
                            if (dr["BillNote"].ToString() != string.Empty)
                            {
                                if (dr["BillNote"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Bill Note (" + dr["BillNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            BillAddressItem.Note = dr["BillNote"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            BillAddressItem.Note = dr["BillNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        BillAddressItem.Note = dr["BillNote"].ToString();
                                    }
                                }
                                else
                                {
                                    BillAddressItem.Note = dr["BillNote"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (BillAddressItem.Addr1 != null || BillAddressItem.Addr2 != null || BillAddressItem.Addr3 != null || BillAddressItem.Addr4 != null || BillAddressItem.Addr5 != null
                            || BillAddressItem.City != null || BillAddressItem.Country != null || BillAddressItem.PostalCode != null || BillAddressItem.State != null || BillAddressItem.Note != null)
                            Invoice.BillAddress.Add(BillAddressItem);

                        if (dt.Columns.Contains("Phone"))
                        {
                            #region Validations of Phone
                            if (dr["Phone"].ToString() != string.Empty)
                            {
                                if (dr["Phone"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.Phone = dr["Phone"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.Phone = dr["Phone"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Fax"))
                        {
                            #region Validations of Fax
                            if (dr["Fax"].ToString() != string.Empty)
                            {
                                if (dr["Fax"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.Fax = dr["Fax"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.Fax = dr["Fax"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Email"))
                        {
                            #region Validations of Email
                            if (dr["Email"].ToString() != string.Empty)
                            {
                                if (dr["Email"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.Email = dr["Email"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.Email = dr["Email"].ToString();
                                }
                            }
                            #endregion
                        }

                        QuickBookEntities.ShipAddress ShipAddressItem = new ShipAddress();
                        if (dt.Columns.Contains("ShipAddr1"))
                        {
                            #region Validations of Ship Addr1
                            if (dr["ShipAddr1"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr1"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add1 (" + dr["ShipAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr1 = dr["ShipAddr1"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipAddr2"))
                        {
                            #region Validations of Ship Addr2
                            if (dr["ShipAddr2"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add2 (" + dr["ShipAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr2 = dr["ShipAddr2"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipAddr3"))
                        {
                            #region Validations of Ship Addr3
                            if (dr["ShipAddr3"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr3"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add3 (" + dr["ShipAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr3 = dr["ShipAddr3"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipAddr4"))
                        {
                            #region Validations of Ship Addr4
                            if (dr["ShipAddr4"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr4"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add4 (" + dr["ShipAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr4 = dr["ShipAddr4"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipAddr5"))
                        {
                            #region Validations of Ship Addr5
                            if (dr["ShipAddr5"].ToString() != string.Empty)
                            {
                                if (dr["ShipAddr5"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Add5 (" + dr["ShipAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Addr5 = dr["ShipAddr5"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipCity"))
                        {
                            #region Validations of Ship City
                            if (dr["ShipCity"].ToString() != string.Empty)
                            {
                                if (dr["ShipCity"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship City (" + dr["ShipCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.City = dr["ShipCity"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.City = dr["ShipCity"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.City = dr["ShipCity"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipState"))
                        {
                            #region Validations of Ship State
                            if (dr["ShipState"].ToString() != string.Empty)
                            {
                                if (dr["ShipState"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship State (" + dr["ShipState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.State = dr["ShipState"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.State = dr["ShipState"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.State = dr["ShipState"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipPostalCode"))
                        {
                            #region Validations of Ship Postal Code
                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                            {
                                if (dr["ShipPostalCode"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Postal Code (" + dr["ShipPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.PostalCode = dr["ShipPostalCode"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ShipCountry"))
                        {
                            #region Validations of Ship Country
                            if (dr["ShipCountry"].ToString() != string.Empty)
                            {
                                if (dr["ShipCountry"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Country (" + dr["ShipCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Country = dr["ShipCountry"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ShipNote"))
                        {
                            #region Validations of Ship Note
                            if (dr["ShipNote"].ToString() != string.Empty)
                            {
                                if (dr["ShipNote"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ship Note (" + dr["ShipNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ShipAddressItem.Note = dr["ShipNote"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ShipAddressItem.Note = dr["ShipNote"].ToString();
                                    }
                                }
                                else
                                {
                                    ShipAddressItem.Note = dr["ShipNote"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (ShipAddressItem.Addr1 != null || ShipAddressItem.Addr2 != null || ShipAddressItem.Addr3 != null || ShipAddressItem.Addr4 != null || ShipAddressItem.Addr5 != null
                          || ShipAddressItem.City != null || ShipAddressItem.Country != null || ShipAddressItem.PostalCode != null || ShipAddressItem.State != null || ShipAddressItem.Note != null)
                            Invoice.ShipAddress.Add(ShipAddressItem);

                        if (dt.Columns.Contains("CustomerMsgRefFullName"))
                        {
                            #region Validations of CustomerMsgRef Full name
                            if (dr["CustomerMsgRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["CustomerMsgRefFullName"].ToString().Length > 101)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomerMsgRef full name (" + dr["CustomerMsgRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                            if (Invoice.CustomerMsgRef.FullName == null)
                                            {
                                                Invoice.CustomerMsgRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                            if (Invoice.CustomerMsgRef.FullName == null)
                                            {
                                                Invoice.CustomerMsgRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                        if (Invoice.CustomerMsgRef.FullName == null)
                                        {
                                            Invoice.CustomerMsgRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Invoice.CustomerMsgRef = new QuickBookEntities.CustomerMsgRef(dr["CustomerMsgRefFullName"].ToString());
                                    if (Invoice.CustomerMsgRef.FullName == null)
                                    {
                                        Invoice.CustomerMsgRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("IsToBeEmailed"))
                        {
                            #region Validations of IsToBeEmailed
                            if (dr["IsToBeEmailed"].ToString() != "<None>" || dr["IsToBeEmailed"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsToBeEmailed"].ToString(), out result))
                                {
                                    Invoice.IsToBeEmailed = Convert.ToInt32(dr["IsToBeEmailed"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsToBeEmailed"].ToString().ToLower() == "true")
                                    {
                                        Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsToBeEmailed"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsToBeEmailed (" + dr["IsToBeEmailed"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.IsToBeEmailed = dr["IsToBeEmailed"].ToString();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("IsToBePrinted"))
                        {
                            #region Validations of IsToBePrinted
                            if (dr["IsToBePrinted"].ToString() != "<None>" || dr["IsToBePrinted"].ToString() != string.Empty)
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                {
                                    Invoice.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                    {
                                        Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                        }

                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("IsTaxIncluded"))
                        {
                            #region Validations of IsTaxIncluded
                            if (dr["IsTaxIncluded"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                {
                                    Invoice.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                    {
                                        Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }
                                        else
                                        {
                                            Invoice.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("CustomerSalesTaxCodeRefFullName"))
                        {
                            #region Validations of CustomerSalesTaxCode Full name
                            if (dr["CustomerSalesTaxCodeRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["CustomerSalesTaxCodeRefFullName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomerTaxCodeRef Full Name (" + dr["CustomerSalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                            if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                            {
                                                Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                            if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                            {
                                                Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                        if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                        {
                                            Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Invoice.CustomerSalesTaxCodeRef = new QuickBookEntities.CustomerSalesTaxCodeRef(dr["CustomerSalesTaxCodeRefFullName"].ToString());
                                    //string strClassFullName = string.Empty;
                                    if (Invoice.CustomerSalesTaxCodeRef.FullName == null)
                                    {
                                        Invoice.CustomerSalesTaxCodeRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("Other"))
                        {
                            #region Validation of Other

                            if (dr["Other"].ToString() != string.Empty)
                            {
                                if (dr["Other"].ToString().Length > 29)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Other (" + dr["Other"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.Other = dr["Other"].ToString().Substring(0, 29);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.Other = dr["Other"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.Other = dr["Other"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.Other = dr["Other"].ToString();
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is invalid for this mapping of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Invoice.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Invoice.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Invoice.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                    Invoice.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                }
                            }

                            #endregion

                        }

                        #region Adding Invoice Line

                        DataProcessingBlocks.InvoiceLineAdd InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                        #region Checking and setting SalesTaxCode

                        if (defaultSettings == null)
                        {
                            CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return null;

                        }
                        //string IsTaxable = string.Empty;
                        string TaxRateValue = string.Empty;

                        string ItemSaleTaxFullName = string.Empty;

                        //if default settings contain checkBoxGrossToNet checked.
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                {
                                    if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeRefFullName"].ToString();


                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);
                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                        }
                        #endregion


                        if (dt.Columns.Contains("ItemRefFullName"))
                        {
                            #region Validations of item Full name
                            if (dr["ItemRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemRefFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item full name (" + dr["ItemRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (InvLine.ItemRef.FullName == null)
                                                InvLine.ItemRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                            if (InvLine.ItemRef.FullName == null)
                                                InvLine.ItemRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                        if (InvLine.ItemRef.FullName == null)
                                            InvLine.ItemRef.FullName = null;
                                    }

                                }
                                else
                                {
                                    InvLine.ItemRef = new ItemRef(dr["ItemRefFullName"].ToString());
                                    if (InvLine.ItemRef.FullName == null)
                                        InvLine.ItemRef.FullName = null;
                                }

                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Description"))
                        {
                            #region Validations for Description
                            if (dr["Description"].ToString() != string.Empty)
                            {
                                if (dr["Description"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            InvLine.Desc = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["Description"].ToString();
                                            InvLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        InvLine.Desc = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["Description"].ToString();
                                    InvLine.Desc = strDesc;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("Quantity"))
                        {
                            #region Validations for Quantity
                            if (dr["Quantity"].ToString() != string.Empty)
                            {
                                string strQuantity = dr["Quantity"].ToString();
                                InvLine.Quantity = strQuantity;

                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("Rate"))
                        {
                            #region Validations for Rate
                            if (dr["Rate"].ToString() != string.Empty)
                            {
                                decimal rate = 0;
                                //decimal amount;
                                if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Rate ( " + dr["Rate"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {
                                        decimal strRate = Convert.ToDecimal(dr["Rate"].ToString());
                                        InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                        {
                                            if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                            {
                                                decimal Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {                                                  
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    rate = Rate / (1 + (TaxRate / 100));
                                                }

                                                InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                            }
                                        }
                                        //Check if InvoiceLine.Rate is null
                                        if (InvLine.Rate == null)
                                        {
                                            InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                    else
                                    {
                                        InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                    }
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of ItemLine SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SerialNumber"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    InvLine.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LotNumber"))
                        {
                            #region Validations of ItemLine LotNumber
                            if (dr["LotNumber"].ToString() != string.Empty)
                            {
                                if (dr["LotNumber"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        InvLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    InvLine.LotNumber = dr["LotNumber"].ToString();
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("InvoiceLineClassFullName"))
                        {
                            #region Validations of Invoice Line Class Full name
                            if (dr["InvoiceLineClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["InvoiceLineClassFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Invoice Line Class name (" + dr["InvoiceLineClassFullName"].ToString() + ") is exceeded maximum length of quickbooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                            if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                            {
                                                InvLine.ClassRef.FullName = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                            if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                            {
                                                InvLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());
                                        if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                        {
                                            InvLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    InvLine.ClassRef = new ClassRef(string.Empty, dr["InvoiceLineClassFullName"].ToString());

                                    if (InvLine.ClassRef.FullName == null && InvLine.ClassRef.ListID == null)
                                    {
                                        InvLine.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Amount"))
                        {
                            #region Validations for Amount
                            if (dr["Amount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            InvLine.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["Amount"].ToString();
                                            InvLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["Amount"].ToString();
                                        InvLine.Amount = strAmount;
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                        {
                                            if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                            {
                                                decimal Amount = Convert.ToDecimal(dr["Amount"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                   CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                   CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {                                                   
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    amount = Amount / (1 + (TaxRate / 100));
                                                }

                                                InvLine.Amount = string.Format("{0:000000.00}", amount);

                                            }
                                        }
                                        //Check if EstLine.Amount is null
                                        if (InvLine.Amount == null)
                                        {
                                            InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));
                                    }
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("InventorySiteRefFullName"))
                        {
                            #region Validations of Inventory Site Ref Full Name
                            if (dr["InventorySiteRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["InventorySiteRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InvLine.InventorySiteRef.FullName == null)
                                                InvLine.InventorySiteRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                            if (InvLine.InventorySiteRef.FullName == null)
                                                InvLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                        if (InvLine.InventorySiteRef.FullName == null)
                                            InvLine.InventorySiteRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    InvLine.InventorySiteRef = new QuickBookEntities.InventorySiteRef(dr["InventorySiteRefFullName"].ToString());
                                    if (InvLine.InventorySiteRef.FullName == null)
                                        InvLine.InventorySiteRef.FullName = null;
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ServiceDate"))
                        {
                            #region validations of ServiceDate
                            if (dr["ServiceDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["ServiceDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out InvoiceDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ServiceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.ServiceDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.ServiceDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.ServiceDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.ServiceDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    InvLine.ServiceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                            {
                                #region Validations of sales tax code Full name
                                if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTaxCodeRefFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This sales tax code name (" + dr["SalesTaxCodeRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                if (InvLine.SalesTaxCodeRef.FullName == null)
                                                    InvLine.SalesTaxCodeRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                                if (InvLine.SalesTaxCodeRef.FullName == null)
                                                    InvLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                            if (InvLine.SalesTaxCodeRef.FullName == null)
                                                InvLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.SalesTaxCodeRef = new QuickBookEntities.SalesTaxCodeRef(dr["SalesTaxCodeRefFullName"].ToString());
                                        if (InvLine.SalesTaxCodeRef.FullName == null)
                                            InvLine.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                        }
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                        {
                            if (dt.Columns.Contains("IsTaxable"))
                            {
                                #region Validations of IsTaxable
                                if (dr["IsTaxable"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxable"].ToString(), out result))
                                    {

                                        InvLine.IsTaxable = Convert.ToInt32(dr["IsTaxable"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxable"].ToString().ToLower() == "true")
                                        {

                                            InvLine.IsTaxable = dr["IsTaxable"].ToString().ToLower(); ;
                                        }
                                        else
                                        {
                                            if (dr["IsTaxable"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else

                                                InvLine.IsTaxable = dr["IsTaxable"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxable (" + dr["IsTaxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;

                                                    InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                                }
                                            }
                                            else
                                            {

                                                InvLine.IsTaxable = dr["IsTaxable"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                        }
                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations for UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            InvLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            InvLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        InvLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }
                                else
                                {
                                    string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    InvLine.UnitOfMeasure = strUnitOfMeasure;
                                }
                            }

                            #endregion
                        }

                        //New Feature::601

                        if (dt.Columns.Contains("PriceLevelRefFullName"))
                        {
                            #region Validations of PriceLevel FullName
                            if (dr["PriceLevelRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["PriceLevelRefFullName"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PriceLevel FullName   (" + dr["PriceLevelRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                            if (InvLine.PriceLevelRef.FullName == null)
                                                InvLine.PriceLevelRef.FullName = null;
                                            else
                                                InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString().Substring(0, 100));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                            if (InvLine.PriceLevelRef.FullName == null)
                                                InvLine.PriceLevelRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                        if (InvLine.PriceLevelRef.FullName == null)
                                            InvLine.PriceLevelRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    InvLine.PriceLevelRef = new PriceLevelRef(dr["PriceLevelRefFullName"].ToString());
                                    if (InvLine.PriceLevelRef.FullName == null)
                                        InvLine.PriceLevelRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Other1"))
                        {
                            #region Validations for Other1

                            if (dr["Other1"].ToString() != string.Empty)
                            {
                                if (dr["Other1"].ToString().Length > 29)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Other1 ( " + dr["Other1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;

                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["Other1"].ToString();
                                            InvLine.Other1 = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["Other1"].ToString();
                                            InvLine.Other1 = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Other1"].ToString();
                                        InvLine.Other1 = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["other1"].ToString();
                                    InvLine.Other1 = strDesc;
                                }
                            }


                            #endregion
                        }
                        if (dt.Columns.Contains("Other2"))
                        {
                            #region Validations for Other2

                            if (dr["Other2"].ToString() != string.Empty)
                            {
                                if (dr["Other2"].ToString().Length > 29)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Other2 ( " + dr["Other2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["Other2"].ToString();
                                            InvLine.Other2 = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["Other2"].ToString();
                                            InvLine.Other2 = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Other2"].ToString();
                                        InvLine.Other2 = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["other2"].ToString();
                                    InvLine.Other2 = strDesc;
                                }
                            }


                            #endregion
                        }
                       
                        //Bug no 331
                        QuickBookStreams.DataExt dataext = new QuickBookStreams.DataExt();
                        if (dt.Columns.Contains("CustomFieldName1"))
                        {
                            dataext.OwnerID = "0";
                            #region Validations for CustomFieldName1
                            if (dr["CustomFieldName1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName1 ( " + dr["CustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["CustomFieldName1"].ToString().Substring(0, 4095);
                                            dataext.DataExtName = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["CustomFieldName1"].ToString();
                                            dataext.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName1"].ToString();
                                        dataext.DataExtName = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["CustomFieldName1"].ToString();
                                    dataext.DataExtName = strDesc;
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue1"))
                        {
                            #region Validations for CustomFieldValue1
                            if (dr["CustomFieldValue1"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue1 ( " + dr["CustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["CustomFieldValue1"].ToString().Substring(0, 4095);
                                            dataext.DataExtValue = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["CustomFieldValue1"].ToString();
                                            dataext.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue1"].ToString();
                                        dataext.DataExtValue = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["CustomFieldValue1"].ToString();
                                    dataext.DataExtValue = strDesc;
                                }
                            }

                            #endregion
                        }
                        if (dataext.DataExtName != null && dataext.DataExtValue != null && dataext.OwnerID != null)
                        {
                            InvLine.DataExt.Add(dataext);
                        }

                        QuickBookStreams.DataExt dataext1 = new QuickBookStreams.DataExt();
                        if (dt.Columns.Contains("CustomFieldName2"))
                        {
                            dataext1.OwnerID = "0";
                            #region Validations for CustomFieldName2
                            if (dr["CustomFieldName2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName2 ( " + dr["CustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["CustomFieldName2"].ToString().Substring(0, 4095);
                                            dataext1.DataExtName = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["CustomFieldName2"].ToString();
                                            dataext1.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName2"].ToString();
                                        dataext1.DataExtName = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["CustomFieldName2"].ToString();
                                    dataext1.DataExtName = strDesc;
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue2"))
                        {
                            #region Validations for CustomFieldValue2
                            if (dr["CustomFieldValue2"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue2 ( " + dr["CustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["CustomFieldValue2"].ToString().Substring(0, 4095);
                                            dataext1.DataExtValue = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["CustomFieldValue2"].ToString();
                                            dataext1.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue2"].ToString();
                                        dataext1.DataExtValue = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["CustomFieldValue2"].ToString();
                                    dataext1.DataExtValue = strDesc;
                                }
                            }

                            #endregion
                        }
                        if (dataext1.DataExtName != null && dataext1.DataExtValue != null && dataext1.OwnerID != null)
                        {
                            InvLine.DataExt.Add(dataext1);
                        }

                        ///443
                        QuickBookStreams.DataExt dataext4 = new QuickBookStreams.DataExt();
                        if (dt.Columns.Contains("CustomFieldName3"))
                        {
                            dataext4.OwnerID = "0";
                            #region Validations for CustomFieldName3
                            if (dr["CustomFieldName3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldName3"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldName3 ( " + dr["CustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["CustomFieldName3"].ToString().Substring(0, 4095);
                                            dataext4.DataExtName = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["CustomFieldName3"].ToString();
                                            dataext4.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldName3"].ToString();
                                        dataext4.DataExtName = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["CustomFieldName3"].ToString();
                                    dataext4.DataExtName = strDesc;
                                }
                            }

                            #endregion
                        }

                        if (dt.Columns.Contains("CustomFieldValue3"))
                        {
                            #region Validations for CustomFieldValue3
                            if (dr["CustomFieldValue3"].ToString() != string.Empty)
                            {
                                if (dr["CustomFieldValue3"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CustomFieldValue3 ( " + dr["CustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["CustomFieldValue3"].ToString().Substring(0, 4095);
                                            dataext4.DataExtValue = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["CustomFieldValue3"].ToString();
                                            dataext4.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["CustomFieldValue3"].ToString();
                                        dataext4.DataExtValue = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["CustomFieldValue3"].ToString();
                                    dataext4.DataExtValue = strDesc;
                                }
                            }

                            #endregion
                        }
                        if (dataext4.DataExtName != null && dataext4.DataExtValue != null && dataext4.OwnerID != null)
                        {
                            InvLine.DataExt.Add(dataext4);
                        }
                        //end bug no 331

                        //11.4 444
                        if (dt.Columns.Contains("LinkToSalesOrder"))
                        {
                            #region Validations of LinkToSalesOrder
                            if (dr["LinkToSalesOrder"].ToString() != string.Empty)
                            {
                                string strLinkToTxnID = dr["LinkToSalesOrder"].ToString();
                                InvLine.LinkToSalesOrder = dr["LinkToSalesOrder"].ToString();
                            }
                            #endregion

                        }  

                        if (InvLine.ItemRef != null)
                        Invoice.InvoiceLineAdd.Add(InvLine);

                        #region Invoice Line add for Freight

                        if (dt.Columns.Contains("Freight"))
                        {
                            if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                            {
                                InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                //Adding freight charge item to invoice line.
                                InvLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                InvLine.ItemRef.FullName = defaultSettings.Frieght;                            

                                #region Validations for Rate
                                if (dr["Freight"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Freight Rate ( " + dr["Freight"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (InvLine.Rate == null)
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                        }
                                    }
                                }
                                if (InvLine.ItemRef != null)
                                Invoice.InvoiceLineAdd.Add(InvLine);

                                #endregion
                            }
                        }

                        #endregion

                        #region Invoice Line add for Insurance

                        if (dt.Columns.Contains("Insurance"))
                        {
                            if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                            {
                                InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                //Adding freight charge item to invoice line.
                                InvLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                InvLine.ItemRef.FullName = defaultSettings.Insurance;                               

                                #region Validations for Rate
                                if (dr["Insurance"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Insurance Rate ( " + dr["Insurance"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (InvLine.Rate == null)
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                        }
                                    }
                                }
                                if (InvLine.ItemRef != null)
                                Invoice.InvoiceLineAdd.Add(InvLine);

                                #endregion
                            }
                        }

                        #endregion

                        #region Invoice Line Add  for Discount

                        if (dt.Columns.Contains("Discount"))
                        {
                            if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                            {
                                InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                //Adding freight charge item to invoice line.
                                InvLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                InvLine.ItemRef.FullName = defaultSettings.Discount;                               

                                #region Validations for Rate
                                if (dr["Discount"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Discount Rate ( " + dr["Discount"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                            InvLine.Rate = Convert.ToString(Math.Round(strRate, 5));
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Rate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (InvLine.Rate == null)
                                            {
                                                InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Rate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                        }
                                    }
                                }
                                if (InvLine.ItemRef != null)
                                Invoice.InvoiceLineAdd.Add(InvLine);

                                #endregion
                            }
                        }

                        #endregion

                        //bug no. 410
                        #region invoice Line add for SalesTax

                        if (dt.Columns.Contains("SalesTax"))
                        {
                            if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                            {
                                InvLine = new DataProcessingBlocks.InvoiceLineAdd();

                                //Adding SalesTax charge item to Invoive line.                                          

                                InvLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                InvLine.ItemRef.FullName = defaultSettings.SalesTax;

                                //Adding SalesTax charge amount to Invoive line.

                                #region Validations for Rate
                                if (dr["SalesTax"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["SalesTax"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["SalesTax"].ToString();
                                                InvLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["SalesTax"].ToString();
                                            InvLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Invoice.IsTaxIncluded != null && Invoice.IsTaxIncluded != string.Empty)
                                            {
                                                if (Invoice.IsTaxIncluded == "true" || Invoice.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    InvLine.Amount = string.Format("{0:000000.00}", rate);
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (InvLine.Amount == null)
                                            {
                                                InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            InvLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                        }

                                    }
                                }
                                if (InvLine.ItemRef != null)
                                Invoice.InvoiceLineAdd.Add(InvLine);
                                #endregion
                            }
                        }


                        #endregion
                       
                        #endregion

                        //new changes for version 6.0
                        DataProcessingBlocks.InvoiceLineGroupAdd InvGroup = new DataProcessingBlocks.InvoiceLineGroupAdd();
                       
                            #region Adding Invoice group

                            if (dt.Columns.Contains("ItemGroupFullName"))
                            {

                                #region Validations of item Full name
                                if (dr["ItemGroupFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Group full name (" + dr["ItemGroupFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                if (InvGroup.ItemGroupRef.FullName == null)
                                                    InvGroup.ItemGroupRef.FullName = null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                                if (InvGroup.ItemGroupRef.FullName == null)
                                                    InvGroup.ItemGroupRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                            if (InvGroup.ItemGroupRef.FullName == null)
                                                InvGroup.ItemGroupRef.FullName = null;
                                        }

                                    }
                                    else
                                    {
                                        InvGroup.ItemGroupRef = new ItemGroupRef(dr["ItemGroupFullName"].ToString());
                                        if (InvGroup.ItemGroupRef.FullName == null)
                                            InvGroup.ItemGroupRef.FullName = null;
                                    }

                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupQuantity"))
                            {
                                #region Validations for Quantity
                                if (dr["ItemGroupQuantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["ItemGroupQuantity"].ToString();
                                    InvGroup.Quantity = strQuantity;

                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("ItemGroupUOM"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["ItemGroupUOM"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupUOM"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                                InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                            InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["ItemGroupUOM"].ToString();
                                        InvGroup.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion
                            }
                            //Bug no 331
                            QuickBookStreams.DataExt dataext2 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("ItemGroupCustomFieldName1"))
                            {
                                dataext2.OwnerID = "0";
                                #region Validations for ItemGroupCustomFieldName1
                                if (dr["ItemGroupCustomFieldName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupCustomFieldName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupCustomFieldName1 ( " + dr["ItemGroupCustomFieldName1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName1"].ToString().Substring(0, 4095);
                                                dataext2.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                                dataext2.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                            dataext2.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["ItemGroupCustomFieldName1"].ToString();
                                        dataext2.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupCustomFieldValue1"))
                            {
                                #region Validations for ItemGroupCustomFieldValue1
                                if (dr["ItemGroupCustomFieldValue1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupCustomFieldValue1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupCustomFieldValue1 ( " + dr["ItemGroupCustomFieldValue1"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue1"].ToString().Substring(0, 4095);
                                                dataext2.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                                dataext2.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                            dataext2.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["ItemGroupCustomFieldValue1"].ToString();
                                        dataext2.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext2.DataExtName != null && dataext2.DataExtValue != null && dataext2.OwnerID != null)
                            { 
                                InvGroup.DataExt.Add(dataext2);
                            }

                            QuickBookStreams.DataExt dataext3 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("ItemGroupCustomFieldName2"))
                            {
                                dataext3.OwnerID = "0";
                                #region Validations for ItemGroupCustomFieldName2
                                if (dr["ItemGroupCustomFieldName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupCustomFieldName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupCustomFieldName2 ( " + dr["ItemGroupCustomFieldName2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName2"].ToString().Substring(0, 4095);
                                                dataext3.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                                dataext3.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                            dataext3.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["ItemGroupCustomFieldName2"].ToString();
                                        dataext3.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("ItemGroupCustomFieldValue2"))
                            {
                                #region Validations for ItemGroupCustomFieldValue2
                                if (dr["ItemGroupCustomFieldValue2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupCustomFieldValue2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupCustomFieldValue2 ( " + dr["ItemGroupCustomFieldValue2"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue2"].ToString().Substring(0, 4095);
                                                dataext3.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                                dataext3.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                            dataext3.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["ItemGroupCustomFieldValue2"].ToString();
                                        dataext3.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext3.DataExtName != null && dataext3.DataExtValue != null && dataext3.OwnerID != null)
                            {                         
                                InvGroup.DataExt.Add(dataext3);
                            }
                            /// 439
                            /// 
                            QuickBookStreams.DataExt dataext5 = new QuickBookStreams.DataExt();
                            if (dt.Columns.Contains("ItemGroupCustomFieldName3"))
                            {
                                dataext5.OwnerID = "0";
                                #region Validations for ItemGroupCustomFieldName3
                                if (dr["ItemGroupCustomFieldName3"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupCustomFieldName3"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupCustomFieldName3 ( " + dr["ItemGroupCustomFieldName3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldName3"].ToString().Substring(0, 4095);
                                                dataext5.DataExtName = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                                dataext5.DataExtName = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                            dataext5.DataExtName = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["ItemGroupCustomFieldName3"].ToString();
                                        dataext5.DataExtName = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ItemGroupCustomFieldValue3"))
                            {
                                #region Validations for ItemGroupCustomFieldValue3
                                if (dr["ItemGroupCustomFieldValue3"].ToString() != string.Empty)
                                {
                                    if (dr["ItemGroupCustomFieldValue3"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemGroupCustomFieldValue3 ( " + dr["ItemGroupCustomFieldValue3"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["ItemGroupCustomFieldValue3"].ToString().Substring(0, 4095);
                                                dataext5.DataExtValue = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                                dataext5.DataExtValue = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                            dataext5.DataExtValue = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["ItemGroupCustomFieldValue3"].ToString();
                                        dataext5.DataExtValue = strDesc;
                                    }
                                }

                                #endregion
                            }
                            if (dataext5.DataExtName != null && dataext5.DataExtValue != null && dataext5.OwnerID != null)
                            {
                                InvGroup.DataExt.Add(dataext5);
                            }
                            //end bug no 331

                            /// bug no 450
                            if (InvGroup.ItemGroupRef != null)
                            {

                                if (InvGroup.ItemGroupRef.FullName != null)
                                    Invoice.InvoiceLineGroupAdd.Add(InvGroup);
                            }

                            #endregion
                        

                        // Axis 10.0

                        #region Adding DiscountLineRet and ShippingLineRet
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                        {


                            QuickBookEntities.DiscountLineAdd discountretitem = new DiscountLineAdd();
                            QuickBookEntities.ShippingLineAdd shippingretitem = new ShippingLineAdd();

                            string TaxRatevalueForDiscount = string.Empty;

                            if (dt.Columns.Contains("DiscountLineAmount"))
                            {
                                #region Validations for Amount
                                if (dr["DiscountLineAmount"].ToString() != string.Empty)
                                {
                                    decimal finalamount;
                                    if (!decimal.TryParse(dr["DiscountLineAmount"].ToString(), out finalamount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountLineAmount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["DiscountLineAmount"].ToString();
                                                discountretitem.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["DiscountLineAmount"].ToString();
                                                discountretitem.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["DiscountLineAmount"].ToString();
                                            discountretitem.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRatevalueForDiscount != string.Empty && discountretitem.IsTaxable != null && discountretitem.IsTaxable != string.Empty)
                                            {
                                                if (discountretitem.IsTaxable == "true" || discountretitem.IsTaxable == "1")
                                                {
                                                    decimal Amountdec = Convert.ToDecimal(dr["DiscountLineAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRatevalueForDiscount);
                                                        finalamount = Amountdec / (1 + (TaxRate / 100));
                                                    }

                                                    discountretitem.Amount = string.Format("{0:000000.00}", finalamount);
                                                }
                                            }
                                            //Check if EstLine.Amount is null
                                            if (discountretitem.Amount == null)
                                            {
                                                discountretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountLineAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            discountretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["DiscountLineAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountLineIsTaxable"))
                            {
                                #region Validations of IsTaxable
                                if (dr["DiscountLineIsTaxable"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["DiscountLineIsTaxable"].ToString(), out result))
                                    {
                                        discountretitem.IsTaxable = Convert.ToInt32(dr["DiscountLineIsTaxable"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["DiscountLineIsTaxable"].ToString().ToLower() == "true")
                                        {
                                            discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString().ToLower(); ;
                                        }
                                        else
                                        {
                                            if (dr["DiscountLineIsTaxable"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This DiscountLineIsTaxable (" + dr["DiscountLineIsTaxable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                discountretitem.IsTaxable = dr["DiscountLineIsTaxable"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("DiscountLineAccountRefFullName"))
                            {
                                #region Validations of Account Full name
                                if (dr["DiscountLineAccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["DiscountLineAccountRefFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Account Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
                                                if (discountretitem.AccountRef.FullName == null)
                                                {
                                                    discountretitem.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
                                                if (discountretitem.AccountRef.FullName == null)
                                                {
                                                    discountretitem.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
                                            if (discountretitem.AccountRef.FullName == null)
                                            {
                                                discountretitem.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        discountretitem.AccountRef = new AccountRef(dr["DiscountLineAccountRefFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (discountretitem.AccountRef.FullName == null)
                                        {
                                            discountretitem.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (discountretitem.Amount != null || discountretitem.IsTaxable != null || discountretitem.AccountRef != null)
                                Invoice.DiscountLineAdd.Add(discountretitem);

                            if (dt.Columns.Contains("ShippingLineAmount"))
                            {
                                #region Validations for Amount
                                if (dr["ShippingLineAmount"].ToString() != string.Empty)
                                {
                                    decimal finalamount;
                                    if (!decimal.TryParse(dr["ShippingLineAmount"].ToString(), out finalamount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ShippingLineAmount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ShippingLineAmount"].ToString();
                                                shippingretitem.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ShippingLineAmount"].ToString();
                                                shippingretitem.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ShippingLineAmount"].ToString();
                                            shippingretitem.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {                                        
                                        shippingretitem.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ShippingLineAmount"].ToString()));
                                       
                                    }
                                }

                                #endregion
                            }
							
					
                            if (dt.Columns.Contains("ShippingLineAccountRefFullName"))
                            {
                                #region Validations of Account Full name
                                if (dr["ShippingLineAccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["ShippingLineAccountRefFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Account Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                                if (shippingretitem.AccountRef.FullName == null)
                                                {
                                                    shippingretitem.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                                if (shippingretitem.AccountRef.FullName == null)
                                                {
                                                    shippingretitem.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                            if (shippingretitem.AccountRef.FullName == null)
                                            {
                                                shippingretitem.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        shippingretitem.AccountRef = new AccountRef(dr["ShippingLineAccountRefFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (shippingretitem.AccountRef.FullName == null)
                                        {
                                            shippingretitem.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (shippingretitem.Amount != null || shippingretitem.AccountRef != null)
                                Invoice.ShippingLineAdd.Add(shippingretitem);

                        }

                        #endregion

                      
                        if (dt.Columns.Contains("InventorySiteLocationRef"))
                        {
                            #region Validations of InventorySiteLocationRef

                            if (dr["InventorySiteLocationRef"].ToString() != string.Empty)
                            {
                                if (dr["InventorySiteLocationRef"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Inventory Site Ref Full Name (" + dr["InventorySiteLocationRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (InvLine.InventorySiteLocationRef.FullName == null)
                                                InvLine.InventorySiteLocationRef.FullName = null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                            if (InvLine.InventorySiteLocationRef.FullName == null)
                                                InvLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                        if (InvLine.InventorySiteLocationRef.FullName == null)
                                            InvLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    InvLine.InventorySiteLocationRef = new QuickBookEntities.InventorySiteLocationRef(dr["InventorySiteLocationRef"].ToString());
                                    if (InvLine.InventorySiteLocationRef.FullName == null)
                                        InvLine.InventorySiteLocationRef.FullName = null;
                                }
                            }
                            #endregion
                        }


                        coll.Add(Invoice);
                        #endregion
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
            ///bug no 442 11.4
            #region Customer,Item and Account Requests
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount *100  / dt.Rows.Count);
                    }
                    catch(Exception ex)
                    { }
                    listCount++;
                   if (CommonUtilities.GetInstance().SkipListFlag == false)
                   {
                    if (dt.Columns.Contains("CustomerRefFullName"))
                    {
                        if (dr["CustomerRefFullName"].ToString() != string.Empty)
                        {
                            string customerName = dr["CustomerRefFullName"].ToString();
                            string[] arr = new string[15];
                            /// 11.4 bug 442
                            string[] CurrencyArr = new string[10];
                            if (customerName.Contains(":"))
                            {
                                arr = customerName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["CustomerRefFullName"].ToString();
                            }
                            /// 11.4 bug 442
                            if (dt.Columns.Contains("Currency"))
                            {
                                if (dr["Currency"].ToString() != string.Empty)
                                {
                                    string Currency = dr["Currency"].ToString();

                                    CurrencyArr[0] = dr["Currency"].ToString();
                                }
                            }
                            #region Set Customer Query
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                    qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                    CustomerQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                               FullName.InnerText= FullName.InnerText.TrimEnd(':');
                                                CustomerQueryRq.AppendChild(FullName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        CustomerQueryRq.AppendChild(FullName);
                                    }
                                    //XmlElement ActiveStatus = pxmldoc.CreateElement("ActiveStatus");
                                    //CustomerQueryRq.AppendChild(ActiveStatus).InnerText = "All";
                                    string pinput = pxmldoc.OuterXml;
                                    //pxmldoc.Save("C://Test.xml");
                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {

                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {
                                                #region Customer Add Query

                                                XmlDocument xmldocadd = new XmlDocument();
                                                xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                xmldocadd.AppendChild(qbXMLcust);
                                                XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                CustomerAddRq.SetAttribute("requestID", "1");
                                                XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                CustomerAddRq.AppendChild(CustomerAdd);
                                                XmlElement Name = xmldocadd.CreateElement("Name");
                                                Name.InnerText = arr[i];
                                                CustomerAdd.AppendChild(Name);    

                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            INIChildFullName.InnerText = INIChildFullName.InnerText.TrimEnd(':');
                                                            XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                            CustomerAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }
                                                #region Adding Bill Address of Customer.
                                                if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                            (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                {
                                                    XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                    CustomerAdd.AppendChild(BillAddress);
                                                    if (dt.Columns.Contains("BillAddr1"))
                                                    {

                                                        if (dr["BillAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                            BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                            BillAddress.AppendChild(BillAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr2"))
                                                    {
                                                        if (dr["BillAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                            BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                            BillAddress.AppendChild(BillAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr3"))
                                                    {
                                                        if (dr["BillAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                            BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                            BillAddress.AppendChild(BillAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr4"))
                                                    {
                                                        if (dr["BillAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                            BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                            BillAddress.AppendChild(BillAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillAddr5"))
                                                    {
                                                        if (dr["BillAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                            BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                            BillAddress.AppendChild(BillAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCity"))
                                                    {
                                                        if (dr["BillCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCity = xmldocadd.CreateElement("City");
                                                            BillCity.InnerText = dr["BillCity"].ToString();
                                                            BillAddress.AppendChild(BillCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillState"))
                                                    {
                                                        if (dr["BillState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillState = xmldocadd.CreateElement("State");
                                                            BillState.InnerText = dr["BillState"].ToString();
                                                            BillAddress.AppendChild(BillState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillPostalCode"))
                                                    {
                                                        if (dr["BillPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                            BillAddress.AppendChild(BillPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillCountry"))
                                                    {
                                                        if (dr["BillCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                            BillCountry.InnerText = dr["BillCountry"].ToString();
                                                            BillAddress.AppendChild(BillCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("BillNote"))
                                                    {
                                                        if (dr["BillNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                            BillNote.InnerText = dr["BillNote"].ToString();
                                                            BillAddress.AppendChild(BillNote);
                                                        }
                                                    }
                                                }


                                                #endregion

                                                #region Adding Ship Address of Customer.

                                                if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                  (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                {
                                                    XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                    CustomerAdd.AppendChild(ShipAddress);
                                                    if (dt.Columns.Contains("ShipAddr1"))
                                                    {

                                                        if (dr["ShipAddr1"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                            ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd1);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr2"))
                                                    {
                                                        if (dr["ShipAddr2"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                            ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd2);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr3"))
                                                    {
                                                        if (dr["ShipAddr3"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                            ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd3);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr4"))
                                                    {
                                                        if (dr["ShipAddr4"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                            ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd4);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipAddr5"))
                                                    {
                                                        if (dr["ShipAddr5"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                            ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                            ShipAddress.AppendChild(ShipAdd5);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCity"))
                                                    {
                                                        if (dr["ShipCity"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                            ShipCity.InnerText = dr["ShipCity"].ToString();
                                                            ShipAddress.AppendChild(ShipCity);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipState"))
                                                    {
                                                        if (dr["ShipState"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipState = xmldocadd.CreateElement("State");
                                                            ShipState.InnerText = dr["ShipState"].ToString();
                                                            ShipAddress.AppendChild(ShipState);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipPostalCode"))
                                                    {
                                                        if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                            ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                            ShipAddress.AppendChild(ShipPostalCode);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipCountry"))
                                                    {
                                                        if (dr["ShipCountry"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                            ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                            ShipAddress.AppendChild(ShipCountry);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("ShipNote"))
                                                    {
                                                        if (dr["ShipNote"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                            ShipNote.InnerText = dr["ShipNote"].ToString();
                                                            ShipAddress.AppendChild(ShipNote);
                                                        }
                                                    }
                                                }
                                                
                                                #endregion

                                                #region Adding Phone, Emails of Customer.

                                                if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                                {
                                                    if (dt.Columns.Contains("Phone"))
                                                    {
                                                        if (dr["Phone"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                            Phone.InnerText = dr["Phone"].ToString();
                                                            CustomerAdd.AppendChild(Phone);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Fax"))
                                                    {
                                                        if (dr["Fax"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                            Fax.InnerText = dr["Fax"].ToString();
                                                            CustomerAdd.AppendChild(Fax);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Email"))
                                                    {
                                                        if (dr["Email"].ToString() != string.Empty)
                                                        {
                                                            XmlElement Email = xmldocadd.CreateElement("Email");
                                                            Email.InnerText = dr["Email"].ToString();
                                                            CustomerAdd.AppendChild(Email);
                                                        }
                                                    }
                                                }
                                                #endregion

                                                #region Currency of Customer.  ///11.4 bug no 442 Add currency 452 and 454
                                                if (CurrencyArr[0] != null && CurrencyArr[0] != "")
                                                {
                                                    XmlElement CurrencyRef = xmldocadd.CreateElement("CurrencyRef");
                                                    XmlElement FullName = xmldocadd.CreateElement("FullName");
                                                    FullName.InnerText = CurrencyArr[0];
                                                    CurrencyRef.AppendChild(FullName);
                                                    CustomerAdd.AppendChild(CurrencyRef);
                                                }
                                                #endregion

                                                string custinput = xmldocadd.OuterXml;
                                                string respcust = string.Empty;
                                                try
                                                {
                                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                    {
                                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                    }

                                                    else
                                                        respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                }

                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                finally
                                                {
                                                    if (respcust != string.Empty)
                                                    {
                                                        System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                        outputcustXMLDoc.LoadXml(respcust);
                                                        foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                        {
                                                            string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                            if (statusSeveritycust == "Error")
                                                            {
                                                                string msg = "New Customer could not be created into QuickBooks \n ";
                                                                msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                ErrorSummary summary = new ErrorSummary(msg);
                                                                summary.ShowDialog();
                                                                CommonUtilities.WriteErrorLog(oNodecust.Attributes["statusMessage"].Value.ToString());
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion

                                            }
                                        }

                                    }


                                }

                            }
                            #endregion
                        }
                    }

                    //Solution for BUG 633
                    if (dt.Columns.Contains("ItemRefFullName"))
                    {
                        #region ItemRefFullName
                        if (dr["ItemRefFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ItemRefFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ItemRefFullName"].ToString();
                            }

                            #region Setting SalesTaxCode and IsTaxIncluded

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;]
                            string TaxRateValue = string.Empty;
                            string IsTaxIncluded = string.Empty;
                            string netRate = string.Empty;

                            string ItemSaleTaxFullName = string.Empty;

                            //if default settings contain checkBoxGrossToNet checked.

                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                {
                                    if (dt.Columns.Contains("SalesTaxCodeRefFullName"))
                                    {
                                        if (dr["SalesTaxCodeRefFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["SalesTaxCodeRefFullName"].ToString();
                                           
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                                //validate IsTaxInluded value if present
                                if (dt.Columns.Contains("IsTaxIncluded"))
                                {
                                    if (dr["IsTaxIncluded"].ToString() != string.Empty && dr["IsTaxIncluded"].ToString() != "<None>")
                                    {
                                        int result = 0;
                                        if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                        {
                                            IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                            {
                                                IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }

                                    }
                                }

                                //Calculate cost
                                if (dt.Columns.Contains("Rate"))
                                {
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                        {
                                            if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                            {
                                                decimal Rate;
                                                if (!decimal.TryParse(dr["Rate"].ToString(), out rate))
                                                {
                                                    //Rate = 0;
                                                    netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                                }
                                                else
                                                {
                                                    Rate = Convert.ToDecimal(dr["Rate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    netRate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                        }
                                        if (netRate == string.Empty)
                                        {
                                            netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dt.Columns.Contains("Rate"))
                                {
                                    if (dr["Rate"].ToString() != string.Empty)
                                    {
                                        netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Rate"]), 5));
                                    }
                                }
                               
                            }

                            #endregion

                            #region Set Item Query

                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");

                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];                                       
                                        ItemQueryRq.AppendChild(FullName);
                                    }


                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }


                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {


                                                if (defaultSettings.Type == "NonInventoryPart")
                                                {
                                                    #region Item NonInventory Add Query

                                                    XmlDocument ItemNonInvendoc = new XmlDocument();
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                   
                                                    XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                    ItemNonInvendoc.AppendChild(qbXMLINI);
                                                    XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                    qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                    qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                    ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                    ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                    XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                    ININame.InnerText = arr[i];
                                                    //ININame.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemNonInventoryAdd.AppendChild(ININame);
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                ItemNonInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }


                                                    //Adding Tax Code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                            ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                                    //Adding Desc and Rate
                                                    //Solution for BUG 631 nad 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemNonInvendoc.CreateElement("Desc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            INISalesAndPurchase.AppendChild(ISDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemNonInvendoc.CreateElement("Price");
                                                            ISRate.InnerText = netRate;
                                                            INISalesAndPurchase.AppendChild(ISRate);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                        INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                        //INIFullName.InnerText = "Sales";
                                                        INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIAccountRefFullName);
                                                        IsPresent = true;
                                                    }

                                                    if (IsPresent == true)
                                                    {
                                                        ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                    }
                                                    string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                                    //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                                    string respItemNonInvendoc = string.Empty;
                                                    try
                                                    {
                                                        //CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenMultiUser);
                                                        
                                                        //Axis 10.2 (bug No-66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                        respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                        }
                                                        else
                                                        {
                                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemNonInvendocinput);
                                                        }
                                                        //End Changes

                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemNonInvendoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strtest2 = respItemNonInvendoc;

                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "Service")
                                                {
                                                    #region Item Service Add Query

                                                    XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                    XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                    ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                    ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemServiceAdd.AppendChild(NameIS);

                                                    


                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }

                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                        }
                                                    }


                                                    XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                    bool IsPresent = false;
                                                    //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                                    //Adding Desc And Rate
                                                    //Solution for BUG 631 and 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ISSalesAndPurchase.AppendChild(ISDesc);
                                                            IsPresent = true;
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemServiceAdddoc.CreateElement("Price");
                                                            ISRate.InnerText = netRate;
                                                            ISSalesAndPurchase.AppendChild(ISRate);
                                                            IsPresent = true;
                                                        }
                                                    }

                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                        ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                        //Adding IncomeAccount FullName.
                                                        XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                        ISIncomeAccountRef.AppendChild(ISFullName);
                                                        IsPresent = true;
                                                    }
                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;

                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        
                                                        //Axis 10.2 (bug No-66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemServiceAddinput);
                                                        }
                                                        //End Changes

                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];
                                                    //NameIS.InnerText = dr["ItemRefFullName"].ToString();
                                                    ItemInventoryAdd.AppendChild(NameIS);

                                                
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {

                                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {

                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                ItemInventoryAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }

                                                        }
                                                    }


                                                    //Adding Tax code Element.
                                                    if (defaultSettings.TaxCode != string.Empty)
                                                    {
                                                        if (defaultSettings.TaxCode.Length < 4)
                                                        {
                                                            XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                            ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                            XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIFullName.InnerText = defaultSettings.TaxCode;
                                                            INISalesTaxCodeRef.AppendChild(INIFullName);
                                                        }
                                                    }

                                                    //Adding Desc and Rate
                                                    //Solution for BUG 631 nad 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                            ISDesc.InnerText = dr["Description"].ToString();
                                                            ItemInventoryAdd.AppendChild(ISDesc);
                                                        }
                                                    }
                                                    if (dt.Columns.Contains("Rate"))
                                                    {
                                                        if (dr["Rate"].ToString() != string.Empty)
                                                        {
                                                            XmlElement ISRate = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                            ISRate.InnerText = netRate;
                                                            ItemInventoryAdd.AppendChild(ISRate);
                                                        }
                                                    }
                                                    //Adding IncomeAccountRef
                                                    if (defaultSettings.IncomeAccount != string.Empty)
                                                    {
                                                        XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                        XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                        INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                    }

                                                    //Adding COGSAccountRef
                                                    if (defaultSettings.COGSAccount != string.Empty)
                                                    {
                                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                    }

                                                    //Adding AssetAccountRef
                                                    if (defaultSettings.AssetAccount != string.Empty)
                                                    {
                                                        XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                        ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                        XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                        INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                    }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenMultiUser);

                                                        //Axis 10.2 (bug No-66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemInventoryAddinput);
                                                        }
                                                        //End Changes
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest4 = respItemInventoryAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                        #endregion
                    }


                    //new changes in version 6.0 add item group creation
                    if (dt.Columns.Contains("ItemGroupFullName"))
                    {
                        #region Set Item Group Query
                        XmlDocument pxmldoc = new XmlDocument();
                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                        pxmldoc.AppendChild(qbXML);
                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                        qbXML.AppendChild(qbXMLMsgsRq);
                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                        XmlElement ItemGroupQueryRq = pxmldoc.CreateElement("ItemGroupQueryRq");
                        qbXMLMsgsRq.AppendChild(ItemGroupQueryRq);
                        ItemGroupQueryRq.SetAttribute("requestID", "1");
                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                        FullName.InnerText = dr["ItemGroupFullName"].ToString();
                        ItemGroupQueryRq.AppendChild(FullName);


                        string pinput = pxmldoc.OuterXml;

                        string resp = string.Empty;
                        try
                        {
                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                        }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                        }
                        finally
                        {
                            if (resp != string.Empty)
                            {

                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                outputXMLDoc.LoadXml(resp);
                                string statusSeverity = string.Empty;
                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemGroupQueryRs"))
                                {
                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                }
                                outputXMLDoc.RemoveAll();
                                if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                {
                                    #region Customer Add Query

                                    XmlDocument xmldocadd = new XmlDocument();
                                    xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                    xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                    xmldocadd.AppendChild(qbXMLcust);
                                    XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                    qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                    qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemGroupAddRq = xmldocadd.CreateElement("ItemGroupAddRq");
                                    qbXMLMsgsRqcust.AppendChild(ItemGroupAddRq);
                                    ItemGroupAddRq.SetAttribute("requestID", "1");
                                    XmlElement ItemGroupAdd = xmldocadd.CreateElement("ItemGroupAdd");
                                    ItemGroupAddRq.AppendChild(ItemGroupAdd);
                                    XmlElement Name = xmldocadd.CreateElement("Name");
                                    Name.InnerText = dr["ItemGroupFullName"].ToString();
                                    ItemGroupAdd.AppendChild(Name);

                                   
                                    string custinput = xmldocadd.OuterXml;
                                    string respcust = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                        }

                                        else
                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                    }
                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (respcust != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                            outputcustXMLDoc.LoadXml(respcust);
                                            foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemGroupAddRs"))
                                            {
                                                string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                if (statusSeveritycust == "Error")
                                                {
                                                    CommonUtilities.WriteErrorLog(oNodecust.Attributes["statusMessage"].Value.ToString());
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }

                        }

                        #endregion
                    }
                }
            }
                else
                {
                    return null;
                }
            }

            return coll;
            #endregion

            #endregion
        }


    }


}

