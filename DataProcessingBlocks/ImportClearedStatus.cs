﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportClearedStatus
    {
        private static ImportClearedStatus m_ImportClearedStatus;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor
        public ImportClearedStatus()
        {

        }
        #endregion

        /// <summary>
        /// Create an instance of Import ClearedStatus
        /// </summary>
        /// <returns></returns>
        public static ImportClearedStatus GetInstance()
        {
            if (m_ImportClearedStatus == null)
                m_ImportClearedStatus = new ImportClearedStatus();
            return m_ImportClearedStatus;
        }

        /// <summary>
        /// This method is used for validating import data and create clearedstatus request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>clearedstatus QuickBooks collection </returns>

        public DataProcessingBlocks.ClearedStatusQBEntryCollection ImportClearedStatusData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of ClearedStatus Entry collections.
            DataProcessingBlocks.ClearedStatusQBEntryCollection coll = new ClearedStatusQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Constant Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;

                    //Cleared Status Validation
                    DataProcessingBlocks.ClearedStatusQBEntry classList = new ClearedStatusQBEntry();

                    if (dt.Columns.Contains("TxnID"))
                    {
                        #region Validations for TxnID
                        if (dr["TxnID"].ToString() != string.Empty)
                        {
                            classList.TxnID = dr["TxnID"].ToString();

                            if (classList.TxnID == null)
                            {
                                classList.TxnID = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TxnLineID"))
                    {
                        #region Validations for TxnLineID
                        if (dr["TxnLineID"].ToString() != string.Empty)
                        {
                            classList.TxnLineID = dr["TxnLineID"].ToString();

                            if (classList.TxnLineID == null)
                            {
                                classList.TxnLineID = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ClearedStatus"))
                    {
                        #region validations of Cleared Status
                        if (dr["ClearedStatus"].ToString() != string.Empty)
                        {
                            try
                            {
                                classList.ClearedStatus = Convert.ToString((DataProcessingBlocks.ClearedStatus)Enum.Parse(typeof(DataProcessingBlocks.ClearedStatus), dr["ClearedStatus"].ToString(), true));
                            }
                            catch
                            {
                                classList.ClearedStatus = dr["ClearedStatus"].ToString();
                            }
                        }
                        #endregion
                    }

                    coll.Add(classList);
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }
            #endregion
      
            #endregion

            return coll;
        }

    }
}
