using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.ComponentModel;
using EDI.Constant;
using System.Threading;

namespace DataProcessingBlocks
{
    public class ImportJournalClass
    {
        private static ImportJournalClass m_ImportJournalClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;
        public ImportJournalClass()
        {
 
        }

        /// <summary>
        /// Create an instance of Import Journal class
        /// </summary>
        /// <returns></returns>
        public static ImportJournalClass GetInstance()
        {
            if (m_ImportJournalClass == null)
                m_ImportJournalClass = new ImportJournalClass();
            return m_ImportJournalClass;
        }

        /// <summary>
        /// This method is used for getting Debit line details for Journal Import.
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private DataProcessingBlocks.JournalDebitLine GetDebitLine(DataRow dr)
        {
            #region Get Debit Amount
            decimal amount;
            int testNumber = 0;
            //Create an instance of Journal Debit line.
            DataProcessingBlocks.JournalDebitLine debitline = new DataProcessingBlocks.JournalDebitLine();
            if (dr["Debit"].ToString() != string.Empty)
            {
                //Validating debit amount in decimal.
                if (!decimal.TryParse(dr["Debit"].ToString(), out amount))
                {
                    #region Validations of Debit Amount
                    if (DataProcessingBlocks.CommonUtilities.GetInstance().ErrorHandlingType == DataProcessingBlocks.ImportErrorType.IncludeError)
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Debit amount (" + dr["Debit"].ToString() + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                return null;
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                debitline.Amount = dr["Debit"].ToString();
                                testNumber = 1;
                            }
                            if (Convert.ToString(result) == "Abort")
                            {
                                isIgnoreAll = true;
                                debitline.Amount = dr["Debit"].ToString();
                                testNumber = 1;
                            }
                        }
                        else
                        {
                            debitline.Amount = dr["Debit"].ToString();
                            testNumber = 1;
                        }

                    }
                    else if (DataProcessingBlocks.CommonUtilities.GetInstance().ErrorHandlingType == DataProcessingBlocks.ImportErrorType.ExcludeError)
                    {
                        return null;
                    }
                    #endregion
                }

                else
                {


                    //debitline.Amount = "00.00";
                }
            }
                
            if (dr.Table.Columns.Contains("Memo"))
            {
                #region Validations of Memo
                if (dr["Memo"].ToString() != "<None>")
                {
                    //Checking length of Memo in debit line.

                    if (dr["Memo"].ToString().Length > 4000)
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Memo (" + dr["Memo"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                debitline.Memo = null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                debitline.Memo = dr["Memo"].ToString().Substring(0, 4000);
                            }
                            if (Convert.ToString(result) == "Abort")
                            {
                                isIgnoreAll = true;
                                debitline.Memo = dr["Memo"].ToString();
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                        }
                        else
                            debitline.Memo = dr["Memo"].ToString();

                    }
                    else
                    {
                        debitline.Memo = dr["Memo"].ToString();
                    }
                }
                #endregion
            }
            if (testNumber != 1)
            {
                if (debitline.Amount != "00.00")
                    debitline.Amount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["Debit"].ToString())));
            }

            if (dr.Table.Columns.Contains("AccountListID"))
            {
                //Passing acount list id 
                debitline.AccountRef = new AccountRef(dr["AccountListID"].ToString(), dr["AccountFullName"].ToString());
            }
            else
            {
                if (dr.Table.Columns.Contains("AccountFullName"))
                {
                    #region Validations of Account FullName
                    if (dr["AccountFullName"].ToString() != string.Empty)
                    {
                        //Checking account full name length.
                        if (dr["AccountFullName"].ToString().Length > 1000)
                        {
                            if (isIgnoreAll == false)
                            {
                                string strMessages = "This Account Full name (" + dr["AccountFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                if (Convert.ToString(result) == "Cancel")
                                {
                                    debitline.AccountRef = null;
                                }
                                if (Convert.ToString(result) == "Ignore")
                                {
                                    debitline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString().Substring(0, 1000));
                                }
                                if (Convert.ToString(result) == "Ignore")
                                {
                                    isIgnoreAll = true;
                                    debitline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString().Substring(0, 1000));
                                }
                                if (Convert.ToString(result) == "No")
                                {
                                    isQuit = true;
                                    return null;
                                }
                            }
                            else
                                debitline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString());
                        }
                        else
                            debitline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString());

                    }
                    else
                        debitline.AccountRef = new AccountRef(string.Empty, string.Empty);
                    #endregion
                }
            }

            if (dr.Table.Columns.Contains("EntityRefFullName"))
            {
                #region Validations of Entity Ref Full name
                if (dr["EntityRefFullName"].ToString() != "<None>")
                {
                    //Checking Entity full name length 
                    if (dr["EntityRefFullName"].ToString().Length > 1000)
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Entiry Ref Full name (" + dr["EntityRefFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                debitline.EntityRef = null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                debitline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                if (debitline.EntityRef.FullName == null && debitline.EntityRef.ListID == null)
                                {
                                    debitline.EntityRef = null;
                                }
                                else
                                {
                                    debitline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString().Substring(0,1000));
                                }
                            }
                            if (Convert.ToString(result) == "Abort")
                            {
                                isIgnoreAll = true;
                                debitline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                if (debitline.EntityRef.FullName == null && debitline.EntityRef.ListID == null)
                                {
                                    debitline.EntityRef = null;
                                }
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                        }
                        else
                        {
                            debitline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                            if (debitline.EntityRef.FullName == null && debitline.EntityRef.ListID == null)
                            {
                                debitline.EntityRef = null;
                            }
                        }
                    }
                    else
                    {
                        debitline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                        if (debitline.EntityRef.FullName == null && debitline.EntityRef.ListID == null)
                        {
                            debitline.EntityRef = null;
                        }
                    }

                }
                #endregion
            }

            if (dr.Table.Columns.Contains("TxnLineID"))
            {
                #region vaidations of TxnLineID
                //Checking TxnLineId validations.
                if (dr["TxnLineID"].ToString() != string.Empty)
                {
                    if (dr["TxnLineID"].ToString() != "<None>")
                    {
                        debitline.TxnLineID = dr["TxnLineID"].ToString();
                    }
                }
                #endregion
            }
            if (dr.Table.Columns.Contains("ClassRefListID") && dr.Table.Columns.Contains("ClassRefFullName"))
            {
                #region validations of Both ClassID and ClassFullName
                //Checking ClassList validations.
                debitline.ClassRef = new ClassRef(dr["ClassRefListID"].ToString(), dr["ClassRefFullName"].ToString());
                if (debitline.ClassRef.FullName == null && debitline.ClassRef.ListID == null)
                {
                    debitline.ClassRef = null;
                }
                #endregion
            }
            else if (dr.Table.Columns.Contains("ClassRefListID"))
            {
                #region validations of ClassRefListID
                //Checking classref listid validations.
                debitline.ClassRef = new ClassRef(dr["ClassRefListID"].ToString(), string.Empty);
                if (debitline.ClassRef.FullName == null && debitline.ClassRef.ListID == null)
                {
                    debitline.ClassRef = null;
                }
                #endregion
            }
            else if (dr.Table.Columns.Contains("ClassRefFullName"))
            {
                #region Validations of Class ref full name
                if (dr["ClassRefFullName"].ToString() != string.Empty)
                {
                    //checking classfull name length.
                    if (dr["ClassRefFullName"].ToString().Length > 1000)
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Class Full name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                debitline.ClassRef = null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                
                                debitline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                if (debitline.ClassRef.FullName == null && debitline.ClassRef.ListID == null)
                                {
                                    debitline.ClassRef = null;
                                }
                                else
                                {
                                    debitline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0,1000));
                                }
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                isIgnoreAll = true;
                                debitline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                if (debitline.ClassRef.FullName == null && debitline.ClassRef.ListID == null)
                                {
                                    debitline.ClassRef = null;
                                }
                                else
                                {
                                    debitline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0,1000));
                                }
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                        }
                        else
                        {
                            debitline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                            if (debitline.ClassRef.FullName == null && debitline.ClassRef.ListID == null)
                            {
                                debitline.ClassRef = null;
                            }
                        }
                    }
                    else
                    {
                        debitline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                        if (debitline.ClassRef.FullName == null && debitline.ClassRef.ListID == null)
                        {
                            debitline.ClassRef = null;
                        }
                    }

                }
                #endregion
            }
            if (dr.Table.Columns.Contains("ItemSalesTaxRefListID") && dr.Table.Columns.Contains("ItemSalesTaxRefFullName"))
            {
                #region validations of ItemSales listid
                //Checking ItemSalesTax validations.
                debitline.ItemSalesTaxRef = new ItemSalesTaxRef(dr["ItemSalesTaxRefListID"].ToString(), dr["ItemSalesTaxRefFullName"].ToString());
                if (debitline.ItemSalesTaxRef.FullName == null && debitline.ItemSalesTaxRef.ListID == null)
                {
                    debitline.ItemSalesTaxRef = null;
                }

                #endregion
            }
            else if (dr.Table.Columns.Contains("ItemSalesTaxRefListID"))
            {
                #region Validations of Item Sales Tax RefList id
                //Checking itemsales tax list id.
                debitline.ItemSalesTaxRef = new ItemSalesTaxRef(dr["ItemSalesTaxRefListID"].ToString(), string.Empty);
                if (debitline.ItemSalesTaxRef.FullName == null && debitline.ItemSalesTaxRef.ListID == null)
                {
                    debitline.ItemSalesTaxRef = null;
                }
                #endregion
            }
            else if (dr.Table.Columns.Contains("ItemSalesTaxRefFullName"))
            {
                #region Validations of Item Salestax
                //Checking ItemSalestax full name validations.
                if (dr["ItemSalesTaxRefFullName"].ToString() != string.Empty)
                {
                    debitline.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                    if (debitline.ItemSalesTaxRef.FullName == null && debitline.ItemSalesTaxRef.ListID == null)
                    {
                        debitline.ItemSalesTaxRef = null;
                    }

                }
                #endregion
            }
            if (dr.Table.Columns.Contains("BillableStatus"))
            {
                #region Validations of BillableStatus
                //Validations of Billable status
                if (dr["BillableStatus"].ToString() != string.Empty)
                {
                    try
                    {
                        debitline.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["BillableStatus"].ToString(), true));
                    }
                    catch
                    {
                        debitline.BillableStatus = string.Empty;
                    }
                }
                #endregion
            }
            return debitline;
            #endregion
        }

        /// <summary>
        /// This method is used for getting Credit Line Details for Journal Import.
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private DataProcessingBlocks.JournalCreditLine GetCreditLine(DataRow dr)
        {
            #region Get Credit Amount
            decimal amount;
            int newtest = 0;
            DataProcessingBlocks.JournalCreditLine creditline = new DataProcessingBlocks.JournalCreditLine();
            if (dr["Credit"].ToString() != string.Empty)
            {
                //Parsing credit amount in decimal
                if (!decimal.TryParse(dr["Credit"].ToString(), out amount))
                {
                    #region Validations of Credit Amount
                    if (DataProcessingBlocks.CommonUtilities.GetInstance().ErrorHandlingType == DataProcessingBlocks.ImportErrorType.IncludeError)
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Credit amount (" + dr["Credit"].ToString() + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                return null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                creditline.Amount = dr["Credit"].ToString();
                                newtest = 1;
                            }
                            if (Convert.ToString(result) == "Abort")
                            {
                                isIgnoreAll = true;
                                creditline.Amount = dr["Credit"].ToString();
                                newtest = 1;
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                        }
                        else
                        {
                            creditline.Amount = dr["Credit"].ToString();
                            newtest = 1;
                        }

                    }
                    else if (DataProcessingBlocks.CommonUtilities.GetInstance().ErrorHandlingType == DataProcessingBlocks.ImportErrorType.ExcludeError)
                    {
                        return null;
                    }
                    #endregion

                }
            }
            //else
                //creditline.Amount = "00.00";

            if (dr.Table.Columns.Contains("Memo"))
            {
                #region Validations of Memo
                if (dr["Memo"].ToString() != "<None>")
                {
                    //Checking Length of memo.
                    if (dr["Memo"].ToString().Length > 4000)
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Memo (" + dr["Memo"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                creditline.Memo = null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                creditline.Memo = dr["Memo"].ToString().Substring(0, 4000);
                            }
                            if (Convert.ToString(result) == "Abort")
                            {
                                isIgnoreAll = true;
                                creditline.Memo = dr["Memo"].ToString();
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                        }
                        else
                            creditline.Memo = dr["Memo"].ToString();

                    }
                    else
                    {
                        creditline.Memo = dr["Memo"].ToString();
                    }
                }


                #endregion
            }
            if (newtest != 1)
                if (creditline.Amount != "00.00")
                    creditline.Amount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["Credit"].ToString())));

            if (dr.Table.Columns.Contains("AccountListID"))
            {
                #region Validations of AccountListID
                //Checking validations of AccountListId.
                creditline.AccountRef = new AccountRef(dr["AccountListID"].ToString(), dr["AccountFullName"].ToString());
                #endregion
            }
            else
            {
                if (dr.Table.Columns.Contains("AccountFullName"))
                {
                    if (dr["AccountFullName"].ToString() != string.Empty)
                    {
                        #region Validations of Account FullName
                        //Checking length of AccountFull name.
                        if (dr["AccountFullName"].ToString().Length > 1000)
                        {
                            if (isIgnoreAll == false)
                            {
                                string strMessages = "This Account Full name (" + dr["AccountFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                if (Convert.ToString(result) == "Cancel")
                                {
                                    creditline.AccountRef = null;
                                }
                                if (Convert.ToString(result) == "Ignore")
                                {
                                    creditline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString().Substring(0, 1000));
                                }
                                if (Convert.ToString(result) == "Abort")
                                {
                                    isIgnoreAll = true;
                                    creditline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString());
                                }
                                if (Convert.ToString(result) == "No")
                                {
                                    isQuit = true; 
                                    return null;
                                }
                            }
                            else
                            {
                                creditline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString());
                            }
                        }
                        else
                        {
                            creditline.AccountRef = new AccountRef(string.Empty, dr["AccountFullName"].ToString());
                        }

                        #endregion
                    }
                }
            }
            if (dr.Table.Columns.Contains("EntityRefListID") && dr.Table.Columns.Contains("EntityRefFullName"))
            {
                #region validations of EntityRef
                //Checking validation of Entity Listid and full name.
                creditline.EntityRef = new EntityRef(dr["EntityRefListID"].ToString(), dr["EntityRefFullName"].ToString());
                if (creditline.EntityRef.FullName == null && creditline.EntityRef.ListID == null)
                {
                    creditline.EntityRef = null;
                }
                #endregion
            }
            if (dr.Table.Columns.Contains("EntityRefListID"))
            {
                #region validations of Entity Ref
                //Validation of EntityRefListID.
                if (dr["EntityRefListID"].ToString() != "<None>")
                {
                    creditline.EntityRef = new EntityRef(dr["EntityRefListID"].ToString(), dr["EntityRefFullName"].ToString());
                    if (creditline.EntityRef.FullName == null && creditline.EntityRef.ListID == null)
                    {
                        creditline.EntityRef = null;
                    }
                }
                #endregion
            }
            else
            {
                if (dr.Table.Columns.Contains("EntityRefFullName"))
                {

                    #region Validations of Entity Ref
                    if (dr["EntityRefFullName"].ToString() != "<None>")
                    {
                        if (dr["EntityRefFullName"].ToString().Length > 1000)//Checking length of Entityfullname
                        {
                            if (isIgnoreAll == false)
                            {
                                string strMessages = "This Entity Full name (" + dr["EntityRefFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                if (Convert.ToString(result) == "Cancel")
                                {
                                    creditline.EntityRef = null;
                                }
                                if (Convert.ToString(result) == "Ignore")
                                {
                                    creditline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                    if (creditline.EntityRef.FullName == null && creditline.EntityRef.ListID == null)
                                    {
                                        creditline.EntityRef = null;
                                    }
                                    else
                                    {
                                        creditline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString().Substring(0,1000));
                                    }
                                }
                                if (Convert.ToString(result) == "Ignore")
                                {
                                    isIgnoreAll = true;
                                    creditline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                    if (creditline.EntityRef.FullName == null && creditline.EntityRef.ListID == null)
                                    {
                                        creditline.EntityRef = null;
                                    }
                                    else
                                    {
                                        creditline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString().Substring(0, 1000));
                                    }
                                }
                                if (Convert.ToString(result) == "No")
                                {
                                    isQuit = true;
                                    return null;
                                }
                            }
                            else
                            {
                                creditline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                if (creditline.EntityRef.FullName == null && creditline.EntityRef.ListID == null)
                                {
                                    creditline.EntityRef = null;
                                }
                            }
                        }
                        else
                        {
                            creditline.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                            if (creditline.EntityRef.FullName == null && creditline.EntityRef.ListID == null)
                            {
                                creditline.EntityRef = null;
                            }
                        }
                    }
                    #endregion
                }
            }

            if (dr.Table.Columns.Contains("TxnLineID"))
            {
                #region Validations of TxnLineID
                //Checking validations of TxnLineID.
                if (dr["TxnLineID"].ToString() != string.Empty)
                    creditline.TxnLineID = dr["TxnLineID"].ToString();
                #endregion
            }
            if (dr.Table.Columns.Contains("ClassRefListID") && dr.Table.Columns.Contains("ClassRefFullName"))
            {
                #region Validations of ClassRef
                //Checking validations of ClassFull name
                creditline.ClassRef = new ClassRef(dr["ClassRefListID"].ToString(), dr["ClassRefFullName"].ToString());
                if (creditline.ClassRef.FullName == null && creditline.ClassRef.ListID == null)
                {
                    creditline.ClassRef = null;
                }
                #endregion
            }
            else if (dr.Table.Columns.Contains("ClassRefListID"))
            {
                #region Validations of ClassRef
                //Checking validations of Classreflistid.
                creditline.ClassRef = new ClassRef(dr["ClassRefListID"].ToString(), string.Empty);
                if (creditline.ClassRef.FullName == null && creditline.ClassRef.ListID == null)
                {
                    creditline.ClassRef = null;
                }
                #endregion
            }
            else if (dr.Table.Columns.Contains("ClassRefFullName"))
            {
                #region Validations of Class Full Name
                if (dr["ClassRefFullName"].ToString() != string.Empty)
                {
                    if (dr["ClassRefFullName"].ToString().Length > 1000)//Checking length of Classfullname
                    {
                        if (isIgnoreAll == false)
                        {
                            string strMessages = "This Class Full name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                            if (Convert.ToString(result) == "Cancel")
                            {
                                creditline.ClassRef = null;
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                creditline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                if (creditline.ClassRef.FullName == null && creditline.ClassRef.ListID == null)
                                {
                                    creditline.ClassRef = null;
                                }
                                else
                                {
                                    creditline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0,1000));
                                }
                            }
                            if (Convert.ToString(result) == "Ignore")
                            {
                                isIgnoreAll = true;
                                creditline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                if (creditline.ClassRef.FullName == null && creditline.ClassRef.ListID == null)
                                {
                                    creditline.ClassRef = null;
                                }
                                else
                                {
                                    creditline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 1000));
                                }
                            }
                            if (Convert.ToString(result) == "No")
                            {
                                isQuit = true;
                                return null;
                            }
                        }
                        else
                        {
                            creditline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                            if (creditline.ClassRef.FullName == null && creditline.ClassRef.ListID == null)
                            {
                                creditline.ClassRef = null;
                            }
                        }
                    }
                    else
                    {
                        creditline.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                        if (creditline.ClassRef.FullName == null && creditline.ClassRef.ListID == null)
                        {
                            creditline.ClassRef = null;
                        }
                    }
                }
                #endregion

            }
            if (dr.Table.Columns.Contains("ItemSalesTaxRefListID") && dr.Table.Columns.Contains("ItemSalesTaxRefFullName"))
            {
                #region Validations of ItemSalesTaxRef
                //Checking validations of Itemsales tax
                creditline.ItemSalesTaxRef = new ItemSalesTaxRef(dr["ItemSalesTaxRefListID"].ToString(), dr["ItemSalesTaxRefFullName"].ToString());
                if (creditline.ItemSalesTaxRef.FullName == null && creditline.ItemSalesTaxRef.ListID == null)
                {
                    creditline.ItemSalesTaxRef = null;
                }
                #endregion
            }
            else if (dr.Table.Columns.Contains("ItemSalesTaxRefListID"))
            {
                #region Validations of ItemSalesTaxRef

                creditline.ItemSalesTaxRef = new ItemSalesTaxRef(dr["ItemSalesTaxRefListID"].ToString(), string.Empty);
                if (creditline.ItemSalesTaxRef.FullName == null && creditline.ItemSalesTaxRef.ListID == null)
                {
                    creditline.ItemSalesTaxRef = null;
                }

                #endregion
            }
            else if (dr.Table.Columns.Contains("ItemSalesTaxRefFullName"))
            {
                #region Validations of ItemSalesTaxRef

                creditline.ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxRefFullName"].ToString());
                if (creditline.ItemSalesTaxRef.FullName == null && creditline.ItemSalesTaxRef.ListID == null)
                {
                    creditline.ItemSalesTaxRef = null;
                }

                #endregion
            }
            if (dr.Table.Columns.Contains("BillableStatus"))
            {
                #region validations of Billable Status
                //validations of Billable status.
                if (dr["BillableStatus"].ToString() != string.Empty)
                {
                    try
                    {
                        creditline.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["BillableStatus"].ToString(), true));
                    }
                    catch
                    {
                        creditline.BillableStatus = string.Empty;
                    }
                }
                #endregion
            }
            return creditline;
            #endregion
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Journal QuickBooks collection </returns>
        public DataProcessingBlocks.JournalQBEntryCollection ImportJournalData(DataTable dt, ref string logDirectory)
        {
            
            //Create an instance of Journal Entry collections.
            DataProcessingBlocks.JournalQBEntryCollection coll = new DataProcessingBlocks.JournalQBEntryCollection();
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            #region For Journal Entry

            #region Checking Validations
            //Checking validations by data row wise.
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        if (isQuit)
                            return null;
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Adding Ref Number

                        DateTime journalDt = new DateTime();
                        string datevalue = string.Empty;

                        string strRefNumValue = string.Empty;
                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref number
                            if (dr["RefNumber"].ToString() != "<None>" || dr["RefNumber"].ToString() != string.Empty)
                            {
                                if (dr["RefNumber"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This RefNumber (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            strRefNumValue = dr["RefNumber"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            isQuit = true;
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            strRefNumValue = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                        strRefNumValue = dr["RefNumber"].ToString();
                                }
                                else
                                {
                                    strRefNumValue = dr["RefNumber"].ToString();
                                }
                            }
                            #endregion

                        }

                        string strAccountFullName = string.Empty;
                        if (dt.Columns.Contains("AccountFullName"))
                        {
                            if (dr["AccountFullName"].ToString() != string.Empty)
                            {
                                strAccountFullName = dr["AccountFullName"].ToString();
                            }

                        }

                        //Finding same refnumber journal entry.
                        DataProcessingBlocks.JournalQBEntry journal = coll.FindJournalEntry(dr["RefNumber"].ToString());
                        if (journal == null)
                        {

                            journal = new DataProcessingBlocks.JournalQBEntry();

                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region Validation for Txndate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {

                                    datevalue = dr["TxnDate"].ToString();
                                    DateTime dttest = new DateTime();
                                    //Parsing the txndate with date format.
                                    if (!DateTime.TryParse(dr["TxnDate"].ToString(), out journalDt))
                                    {

                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + dr["TxnDate"].ToString() + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.TxnDate = dr["TxnDate"].ToString();
                                                }
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    journal.TxnDate = dr["TxnDate"].ToString();
                                                }
                                            }
                                            else
                                                journal.TxnDate = dr["TxnDate"].ToString();
                                        }
                                        else
                                        {
                                            journal.TxnDate = dttest.ToString();
                                        }


                                    }
                                    else
                                    {
                                        journal.TxnDate = DateTime.Parse(dr["TxnDate"].ToString()).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                                if (datevalue != string.Empty)
                                    journal.JournalDate = journalDt;
                                //journal.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                            if (strRefNumValue != string.Empty)
                            {
                                journal.RefNumber = dr["RefNumber"].ToString();
                            }
                            if (dt.Columns.Contains("IsAdjustment"))
                            {
                                //Checking validations of IsAdjustment.
                                if (dr["IsAdjustment"].ToString() != "<None>" || dr["IsAdjustment"].ToString() != string.Empty)
                                {
                                    #region validations for IsAdjustment
                                    int result = 0;
                                    if (int.TryParse(dr["IsAdjustment"].ToString(), out result))
                                    {
                                        journal.IsAdjustment = Convert.ToInt32(dr["IsAdjustment"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsAdjustment"].ToString().ToLower() == "true")
                                        {
                                            journal.IsAdjustment = dr["IsAdjustment"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsAdjustment"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                journal.IsAdjustment = dr["IsAdjustment"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsAdjustment (" + dr["IsAdjustment"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                            }
                                            else
                                                journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                        }

                                    }
                                    #endregion
                                }
                            }

                            if (dt.Columns.Contains("IsHomeCurrencyAdjustment"))
                            {
                                //Checking validations of IsHomeCurrencyAdjustment.
                                if (dr["IsHomeCurrencyAdjustment"].ToString() != "<None>" || dr["IsHomeCurrencyAdjustment"].ToString() != string.Empty)
                                {
                                    #region validations for IsHomeCurrencyAdjustment
                                    int result = 0;
                                    if (int.TryParse(dr["IsHomeCurrencyAdjustment"].ToString(), out result))
                                    {
                                        journal.IsHomeCurrencyAdjustment = Convert.ToInt32(dr["IsHomeCurrencyAdjustment"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsHomeCurrencyAdjustment"].ToString().ToLower() == "true")
                                        {
                                            journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsHomeCurrencyAdjustment"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsHomeCurrencyAdjustment (" + dr["IsHomeCurrencyAdjustment"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);

                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                                }
                                            }
                                            else
                                                journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                        }

                                    }
                                    #endregion
                                }
                            }

                            if (dt.Columns.Contains("IsAmountsEnteredInHomeCurrency"))
                            {
                                //Checking validations of IsAmountsEnteredInHomeCurrency.
                                if (dr["IsAmountsEnteredInHomeCurrency"].ToString() != "<None>" || dr["IsAmountsEnteredInHomeCurrency"].ToString() != string.Empty)
                                {
                                    #region validations for IsAmountsEnteredInHomeCurrency
                                    int result = 0;
                                    if (int.TryParse(dr["IsAmountsEnteredInHomeCurrency"].ToString(), out result))
                                    {
                                        journal.IsAmountsEnteredInHomeCurrency = Convert.ToInt32(dr["IsAmountsEnteredInHomeCurrency"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower() == "true")
                                        {
                                            journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsAmountsEnteredInHomeCurrency (" + dr["IsAmountsEnteredInHomeCurrency"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                                }
                                            }
                                            else
                                                journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                        }

                                    }
                                    #endregion
                                }
                            }
                            if (dt.Columns.Contains("CurrencyFullName"))
                            {
                                #region Validations of CurrencyRef Fullname
                                if (dr["CurrencyFullName"].ToString() != "<None>" || dr["CurrencyFullName"].ToString() != string.Empty)
                                {
                                    //Checking Currency full name length 
                                    if (dr["CurrencyFullName"].ToString().Length > 64)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency Ref Full name (" + dr["CurrencyFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                                if (journal.CurrencyRef.FullName == null)
                                                {
                                                    journal.CurrencyRef = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                                if (journal.CurrencyRef.FullName == null)
                                                {
                                                    journal.CurrencyRef = null;
                                                }
                                                else
                                                {
                                                    journal.CurrencyRef =new CurrencyRef(dr["CurrencyFullName"].ToString().Substring(0, 64));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                            if (journal.CurrencyRef.FullName == null)
                                            {
                                                journal.CurrencyRef = null;
                                            }
                                        }

                                    }
                                    else
                                    {
                                        journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                        if (journal.CurrencyRef.FullName == null)
                                        {
                                            journal.CurrencyRef = null;
                                        }
                                    }

                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["ExchangeRate"].ToString();
                                                journal.ExchangeRate = strRate;
                                            }
                                        }
                                        else
                                        {
                                            journal.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                       journal.ExchangeRate =Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();//bug 540
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("IsBillable"))
                            {
                                //validations ob IsBillabel
                                if (dr["IsBillable"].ToString() != "<None>" || dr["IsBillable"].ToString() != string.Empty)
                                {

                                    #region validations for IsBillable
                                    int result = 0;
                                    if (int.TryParse(dr["IsBillable"].ToString(), out result))
                                    {
                                        journal.IsBillable = Convert.ToInt32(dr["IsBillable"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsBillable"].ToString().ToLower() == "true")
                                        {
                                            journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsBillable"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsBillable (" + dr["IsBillable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    journal.IsBillable = dr["IsBillable"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.IsBillable = dr["IsBillable"].ToString();
                                                }
                                            }
                                            else
                                                journal.IsBillable = dr["IsBillable"].ToString();
                                        }

                                    }
                                    #endregion
                                }
                            }
                            if (dt.Columns.Contains("IsBillable"))
                            {
                                //validations ob IsBillabel
                                if (dr["IsBillable"].ToString() != "<None>" || dr["IsBillable"].ToString() != string.Empty)
                                {

                                    #region validations for IsBillable
                                    int result = 0;
                                    if (int.TryParse(dr["IsBillable"].ToString(), out result))
                                    {
                                        journal.IsBillable = Convert.ToInt32(dr["IsBillable"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsBillable"].ToString().ToLower() == "true")
                                        {
                                            journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsBillable"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsBillable (" + dr["IsBillable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    journal.IsBillable = dr["IsBillable"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.IsBillable = dr["IsBillable"].ToString();
                                                }
                                            }
                                            else
                                                journal.IsBillable = dr["IsBillable"].ToString();
                                        }

                                    }
                                    #endregion
                                }
                            }

                            #region For Amount Details
                            if (dt.Columns.Contains("Debit") == true && dt.Columns.Contains("Credit") == true)
                            {
                                if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() == string.Empty)
                                {
                                    //dr["Debit"] = "00.00";
                                    //dr["Credit"] = "00.00";
                                    coll.Add(journal);
                                }
                                if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() == string.Empty)
                                {
                                    //Getting Debit line 
                                    DataProcessingBlocks.JournalDebitLine debitLine = this.GetDebitLine(dr);
                                    if (debitLine != null)
                                    {
                                        journal.JournalDebitLine.Add(debitLine);
                                        if (debitLine.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.DrAmount += Convert.ToDecimal(debitLine.Amount);
                                        }
                                        catch
                                        {
                                            //journal.DrAmount = Convert.ToDecimal("00.00");
                                        }
                                        coll.Add(journal);
                                    }
                                }
                                else if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() != string.Empty)
                                {
                                    //Getting Credit line.
                                    DataProcessingBlocks.JournalCreditLine creditline = this.GetCreditLine(dr);
                                    if (creditline != null)
                                    {
                                        journal.JournalCreditLine.Add(creditline);
                                        if (creditline.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.CrAmount += Convert.ToDecimal(creditline.Amount);
                                        }
                                        catch
                                        {
                                            //journal.CrAmount = Convert.ToDecimal("00.00");
                                        }
                                        coll.Add(journal);
                                    }
                                }
                                else if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() != string.Empty)
                                {
                                    //getting Debit line
                                    DataProcessingBlocks.JournalDebitLine debitLine = this.GetDebitLine(dr);
                                    //Getting Credit line.
                                    DataProcessingBlocks.JournalCreditLine creditline = this.GetCreditLine(dr);
                                    if (debitLine != null)
                                    {
                                        journal.JournalDebitLine.Add(debitLine);
                                        if (debitLine.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.DrAmount += Convert.ToDecimal(debitLine.Amount);
                                        }
                                        catch
                                        {
                                            //journal.DrAmount = Convert.ToDecimal("00.00");
                                        }
                                    }
                                    if (creditline != null)
                                    {
                                        journal.JournalCreditLine.Add(creditline);
                                        if (creditline.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.CrAmount += Convert.ToDecimal(creditline.Amount);
                                        }
                                        catch
                                        {
                                            //journal.CrAmount = Convert.ToDecimal("00.00");
                                        }
                                    }
                                    //if (debitLine != null || creditline != null)
                                    //{
                                        coll.Add(journal);
                                    //}
                                    if (debitLine == null && debitLine == null)
                                        return null;

                                }

                            }

                            #endregion
                        }
                        else
                        {

                            if (journal.RefNumber != dr["RefNumber"].ToString() && dr["RefNumber"].ToString() != string.Empty)
                            {
                                journal = new DataProcessingBlocks.JournalQBEntry();

                                if (dt.Columns.Contains("TxnDate"))
                                {
                                    //validations of txndate.
                                    #region Validation for Txndate
                                    if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                    {

                                        datevalue = dr["TxnDate"].ToString();
                                        DateTime dttest = new DateTime();
                                        //Parsing the txndate with date format.
                                        if (!DateTime.TryParse(dr["TxnDate"].ToString(), out journalDt))
                                        {

                                            bool IsValid = false;

                                            try
                                            {
                                                dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                IsValid = true;
                                            }
                                            catch
                                            {
                                                IsValid = false;
                                            }
                                            if (IsValid == false)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This TxnDate (" + dr["TxnDate"].ToString() + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        journal.TxnDate = dr["TxnDate"].ToString();
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        isQuit = true;
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        journal.TxnDate = dr["TxnDate"].ToString();
                                                    }
                                                }
                                                else
                                                    journal.TxnDate = dr["TxnDate"].ToString();
                                            }
                                            else
                                            {
                                                journal.TxnDate = dttest.ToString();
                                            }


                                        }
                                        else
                                        {
                                            //journal.TxnDate = dr["TxnDate"].ToString();
                                            journal.TxnDate = DateTime.Parse(dr["TxnDate"].ToString()).ToString("yyyy-MM-dd");
                                        }
                                    }
                                    #endregion

                                    if (datevalue != string.Empty)
                                        journal.JournalDate = journalDt;
                                    //journal.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                                if (strRefNumValue != string.Empty)
                                {
                                    //Validations of Ref number.
                                    journal.RefNumber = dr["RefNumber"].ToString();
                                }
                                if (dt.Columns.Contains("IsAdjustment"))
                                {
                                    if (dr["IsAdjustment"].ToString() != "<None>")
                                    {

                                        #region validations for IsAdjustment
                                        //Validations of IsAdjustment.
                                        int result = 0;
                                        if (int.TryParse(dr["IsAdjustment"].ToString(), out result))
                                        {
                                            journal.IsAdjustment = Convert.ToInt32(dr["IsAdjustment"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsAdjustment"].ToString().ToLower() == "true")
                                            {
                                                journal.IsAdjustment = dr["IsAdjustment"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsAdjustment"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    journal.IsAdjustment = dr["IsAdjustment"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsAdjustment (" + dr["IsAdjustment"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        isQuit = true;
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                                    }
                                                }
                                                else
                                                    journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                            }

                                        }
                                        #endregion
                                    }
                                }
                                if (dt.Columns.Contains("IsHomeCurrencyAdjustment"))
                                {
                                    //Checking validations of IsHomeCurrencyAdjustment.
                                    if (dr["IsHomeCurrencyAdjustment"].ToString() != "<None>" || dr["IsHomeCurrencyAdjustment"].ToString() != string.Empty)
                                    {
                                        #region validations for IsHomeCurrencyAdjustment
                                        int result = 0;
                                        if (int.TryParse(dr["IsHomeCurrencyAdjustment"].ToString(), out result))
                                        {
                                            journal.IsHomeCurrencyAdjustment = Convert.ToInt32(dr["IsHomeCurrencyAdjustment"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsHomeCurrencyAdjustment"].ToString().ToLower() == "true")
                                            {
                                                journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsHomeCurrencyAdjustment"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsHomeCurrencyAdjustment (" + dr["IsHomeCurrencyAdjustment"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        isQuit = true;
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                                    }
                                                }
                                                else
                                                    journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                            }

                                        }
                                        #endregion
                                    }
                                }

                                if (dt.Columns.Contains("IsAmountsEnteredInHomeCurrency"))
                                {
                                    //Checking validations of IsAmountsEnteredInHomeCurrency.
                                    if (dr["IsAmountsEnteredInHomeCurrency"].ToString() != "<None>" || dr["IsAmountsEnteredInHomeCurrency"].ToString() != string.Empty)
                                    {
                                        #region validations for IsAmountsEnteredInHomeCurrency
                                        int result = 0;
                                        if (int.TryParse(dr["IsAmountsEnteredInHomeCurrency"].ToString(), out result))
                                        {
                                            journal.IsAmountsEnteredInHomeCurrency = Convert.ToInt32(dr["IsAmountsEnteredInHomeCurrency"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower() == "true")
                                            {
                                                journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsAmountsEnteredInHomeCurrency (" + dr["IsAmountsEnteredInHomeCurrency"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        isQuit = true;
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                                    }
                                                }
                                                else
                                                    journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                            }

                                        }
                                        #endregion
                                    }
                                }
                                if (dt.Columns.Contains("CurrencyFullName"))
                                {
                                    #region Validations of CurrencyRef Fullname
                                    if (dr["CurrencyFullName"].ToString() != "<None>" || dr["CurrencyFullName"].ToString() != string.Empty)
                                    {
                                        //Checking Currency full name length 
                                        if (dr["CurrencyFullName"].ToString().Length > 64)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Currency Ref Full name (" + dr["CurrencyFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                                    if (journal.CurrencyRef.FullName == null)
                                                    {
                                                        journal.CurrencyRef = null;
                                                    }
                                                    else
                                                    {
                                                        journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString().Substring(0,64));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                                    if (journal.CurrencyRef.FullName == null)
                                                    {
                                                        journal.CurrencyRef = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                                if (journal.CurrencyRef.FullName == null)
                                                {
                                                    journal.CurrencyRef = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                            if (journal.CurrencyRef.FullName == null)
                                            {
                                                journal.CurrencyRef = null;
                                            }
                                        }

                                    }
                                    #endregion
                                }
                                if (dt.Columns.Contains("ExchangeRate"))
                                {
                                    #region Validations for ExchangeRate
                                    if (dr["ExchangeRate"].ToString() != string.Empty)
                                    {
                                        decimal amount;
                                        if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    isQuit = true;
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    string strRate = dr["ExchangeRate"].ToString();
                                                    journal.ExchangeRate = strRate;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    journal.ExchangeRate = dr["ExchangeRate"].ToString();
                                                }
                                            }
                                            else
                                                journal.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        else
                                        {
                                           
                                            journal.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                         }
                                    }

                                    #endregion

                                }
                                if (dt.Columns.Contains("IsBillable"))
                                {
                                    //Validations of IsBillable
                                    if (dr["IsBillable"].ToString() != "<None>")
                                    {
                                        #region validations for IsBillable
                                        int result = 0;
                                        if (int.TryParse(dr["IsBillable"].ToString(), out result))
                                        {
                                            journal.IsBillable = Convert.ToInt32(dr["IsBillable"].ToString()) > 0 ? "true" : "false";
                                        }
                                        else
                                        {
                                            string strvalid = string.Empty;
                                            if (dr["IsBillable"].ToString().ToLower() == "true")
                                            {
                                                journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                            }
                                            else
                                            {
                                                if (dr["IsBillable"].ToString().ToLower() != "false")
                                                {
                                                    strvalid = "invalid";
                                                }
                                                else
                                                    journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                            }
                                            if (strvalid != string.Empty)
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This IsBillable (" + dr["IsBillable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(results) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        isQuit = true;
                                                        return null;
                                                    }
                                                    if (Convert.ToString(results) == "Ignore")
                                                    {
                                                        journal.IsBillable = dr["IsBillable"].ToString();
                                                    }
                                                    if (Convert.ToString(results) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        journal.IsBillable = dr["IsBillable"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    journal.IsBillable = dr["IsBillable"].ToString();
                                                }
                                            }

                                        }
                                        #endregion
                                    }
                                }
                                //if (dr["Debit"].ToString() != string.Empty || dr["Credit"].ToString() != string.Empty)
                                //{
                                    coll.Add(journal);
                                //}
                            }

                            #region For Amount Details
                            if (dt.Columns.Contains("Debit") == true && dt.Columns.Contains("Credit") == true)
                            {
                                if (isQuit)
                                    return null;
                                if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() == string.Empty)
                                {
                                    //dr["Debit"] = "00.00";
                                    //dr["Credit"] = "00.00";
                                    //coll.Add(journal);
                                }
                                if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() == string.Empty)
                                {
                                    if (isQuit)
                                        return null;
                                    //Getting Debit line.
                                    DataProcessingBlocks.JournalDebitLine debitLine = this.GetDebitLine(dr);
                                    if (debitLine != null)
                                    {
                                        journal.JournalDebitLine.Add(debitLine);
                                        if (debitLine.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.DrAmount += Convert.ToDecimal(debitLine.Amount);
                                        }
                                        catch
                                        {
                                            //journal.DrAmount = Convert.ToDecimal("00.00");
                                        }

                                    }
                                }
                                else if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() != string.Empty)
                                {
                                    if (isQuit)
                                        return null;
                                    //getting Credit line.
                                    DataProcessingBlocks.JournalCreditLine creditline = this.GetCreditLine(dr);
                                    if (creditline != null)
                                    {
                                        journal.JournalCreditLine.Add(creditline);
                                        if (creditline.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.CrAmount += Convert.ToDecimal(creditline.Amount);
                                        }
                                        catch
                                        {
                                            //journal.CrAmount = Convert.ToDecimal("00.00");
                                        }

                                    }
                                }
                                else if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() != string.Empty)
                                {
                                    //Getting Debit line
                                    DataProcessingBlocks.JournalDebitLine debitLine = this.GetDebitLine(dr);

                                    //Getting credit line.
                                    DataProcessingBlocks.JournalCreditLine creditline = this.GetCreditLine(dr);
                                    if (isQuit)
                                        return null;
                                    if (debitLine != null)
                                    {
                                        journal.JournalDebitLine.Add(debitLine);
                                        if (debitLine.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.DrAmount += Convert.ToDecimal(debitLine.Amount);
                                        }
                                        catch
                                        {
                                            //journal.DrAmount = Convert.ToDecimal("00.00");
                                        }
                                    }
                                    if (creditline != null)
                                    {
                                        journal.JournalCreditLine.Add(creditline);
                                        if (creditline.ItemSalesTaxRef != null)
                                        {
                                            journal.IsVarifyBalance = true;
                                        }
                                        try
                                        {
                                            journal.CrAmount += Convert.ToDecimal(creditline.Amount);
                                        }
                                        catch
                                        {
                                            //journal.CrAmount = Convert.ToDecimal("00.00");
                                        }

                                    }



                                }

                            }

                            #endregion

                        }
                        #endregion
                    }
                    else
                    {
                        #region Without adding Ref Number
                        //Create new instance of Journal 
                        DataProcessingBlocks.JournalQBEntry journal = new DataProcessingBlocks.JournalQBEntry();
                        DateTime journalDt = new DateTime();
                        string datevalue = string.Empty;
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region Validation for Txndate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {

                                datevalue = dr["TxnDate"].ToString();
                                DateTime dttest = new DateTime();
                                //Parsing the txndate with date format.
                                if (!DateTime.TryParse(dr["TxnDate"].ToString(), out journalDt))
                                {

                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + dr["TxnDate"].ToString() + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                journal.TxnDate = dr["TxnDate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.TxnDate = dr["TxnDate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            journal.TxnDate = dr["TxnDate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        journal.TxnDate = dttest.ToString();
                                    }


                                }
                                else
                                {
                                    journal.TxnDate = DateTime.Parse(dr["TxnDate"].ToString()).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("IsAdjustment"))
                        {
                            //Checking validations of IsAdjustment.
                            if (dr["IsAdjustment"].ToString() != "<None>" || dr["IsAdjustment"].ToString() != string.Empty)
                            {
                                #region validations for IsAdjustment
                                int result = 0;
                                if (int.TryParse(dr["IsAdjustment"].ToString(), out result))
                                {
                                    journal.IsAdjustment = Convert.ToInt32(dr["IsAdjustment"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsAdjustment"].ToString().ToLower() == "true")
                                    {
                                        journal.IsAdjustment = dr["IsAdjustment"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsAdjustment"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            journal.IsAdjustment = dr["IsAdjustment"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsAdjustment (" + dr["IsAdjustment"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            journal.IsAdjustment = dr["IsAdjustment"].ToString();
                                        }
                                    }

                                }
                                #endregion
                            }
                        }

                        if (dt.Columns.Contains("IsHomeCurrencyAdjustment"))
                        {
                            //Checking validations of IsHomeCurrencyAdjustment.
                            if (dr["IsHomeCurrencyAdjustment"].ToString() != "<None>" || dr["IsHomeCurrencyAdjustment"].ToString() != string.Empty)
                            {
                                #region validations for IsHomeCurrencyAdjustment
                                int result = 0;
                                if (int.TryParse(dr["IsHomeCurrencyAdjustment"].ToString(), out result))
                                {
                                    journal.IsHomeCurrencyAdjustment = Convert.ToInt32(dr["IsHomeCurrencyAdjustment"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsHomeCurrencyAdjustment"].ToString().ToLower() == "true")
                                    {
                                        journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsHomeCurrencyAdjustment"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsHomeCurrencyAdjustment (" + dr["IsHomeCurrencyAdjustment"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                            }
                                        }
                                        else
                                            journal.IsHomeCurrencyAdjustment = dr["IsHomeCurrencyAdjustment"].ToString();
                                    }

                                }
                                #endregion
                            }
                        }

                        if (dt.Columns.Contains("IsAmountsEnteredInHomeCurrency"))
                        {
                            //Checking validations of IsAmountsEnteredInHomeCurrency.
                            if (dr["IsAmountsEnteredInHomeCurrency"].ToString() != "<None>" || dr["IsAmountsEnteredInHomeCurrency"].ToString() != string.Empty)
                            {
                                #region validations for IsAmountsEnteredInHomeCurrency
                                int result = 0;
                                if (int.TryParse(dr["IsAmountsEnteredInHomeCurrency"].ToString(), out result))
                                {
                                    journal.IsAmountsEnteredInHomeCurrency = Convert.ToInt32(dr["IsAmountsEnteredInHomeCurrency"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower() == "true")
                                    {
                                        journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsAmountsEnteredInHomeCurrency (" + dr["IsAmountsEnteredInHomeCurrency"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                            }
                                        }
                                        else
                                            journal.IsAmountsEnteredInHomeCurrency = dr["IsAmountsEnteredInHomeCurrency"].ToString();
                                    }

                                }
                                #endregion
                            }
                        }
                        if (dt.Columns.Contains("CurrencyFullName"))
                        {
                            #region Validations of CurrencyRef Fullname
                            if (dr["CurrencyFullName"].ToString() != "<None>" || dr["CurrencyFullName"].ToString() != string.Empty)
                            {
                                //Checking Currency full name length 
                                if (dr["CurrencyFullName"].ToString().Length > 64)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Currency Ref Full name (" + dr["CurrencyFullName"].ToString() + ") is exceeded maximum length of QuickBooks.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                            if (journal.CurrencyRef.FullName == null)
                                            {
                                                journal.CurrencyRef = null;
                                            }
                                            else
                                            {
                                                journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString().Substring(0,64));
                                            }
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            isQuit = true;
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                            if (journal.CurrencyRef.FullName == null)
                                            {
                                                journal.CurrencyRef = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                        if (journal.CurrencyRef.FullName == null)
                                        {
                                            journal.CurrencyRef = null;
                                        }
                                    }
                                }
                                else
                                {
                                    journal.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                    if (journal.CurrencyRef.FullName == null)
                                    {
                                        journal.CurrencyRef = null;
                                    }
                                }

                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            isQuit = true;
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["ExchangeRate"].ToString();
                                            journal.ExchangeRate = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            journal.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                        journal.ExchangeRate = dr["ExchangeRate"].ToString();
                                }
                                else
                                {
                                    journal.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();

                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("IsBillable"))
                        {
                            //validations ob IsBillabel
                            if (dr["IsBillable"].ToString() != "<None>" || dr["IsBillable"].ToString() != string.Empty)
                            {
                                #region validations for IsBillable
                                int result = 0;
                                if (int.TryParse(dr["IsBillable"].ToString(), out result))
                                {
                                    journal.IsBillable = Convert.ToInt32(dr["IsBillable"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsBillable"].ToString().ToLower() == "true")
                                    {
                                        journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsBillable"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            journal.IsBillable = dr["IsBillable"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsBillable (" + dr["IsBillable"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                journal.IsBillable = dr["IsBillable"].ToString();
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                isQuit = true;
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                journal.IsBillable = dr["IsBillable"].ToString();
                                            }
                                        }
                                        else
                                            journal.IsBillable = dr["IsBillable"].ToString();
                                    }

                                }
                                #endregion
                            }
                        }

                        #region For Amount Details
                        if (dt.Columns.Contains("Debit") == true && dt.Columns.Contains("Credit") == true)
                        {
                            if (isQuit)
                                return null;
                            if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() == string.Empty)
                            {
                                //dr["Debit"] = "00.00";
                                //dr["Credit"] = "00.00";
                                coll.Add(journal);
                            }
                            if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() == string.Empty)
                            {
                                //Getting Debit line 
                                DataProcessingBlocks.JournalDebitLine debitLine = this.GetDebitLine(dr);
                                if (isQuit)
                                    return null;
                                if (debitLine != null)
                                {
                                    journal.JournalDebitLine.Add(debitLine);
                                    if (debitLine.ItemSalesTaxRef != null)
                                    {
                                        journal.IsVarifyBalance = true;
                                    }
                                    try
                                    {
                                        journal.DrAmount += Convert.ToDecimal(debitLine.Amount);
                                    }
                                    catch
                                    {
                                        //journal.DrAmount = Convert.ToDecimal("00.00");
                                    }
                                    coll.Add(journal);
                                }
                            }
                            else if (dr["Debit"].ToString() == string.Empty && dr["Credit"].ToString() != string.Empty)
                            {
                                //getting credit line.
                                DataProcessingBlocks.JournalCreditLine creditline = this.GetCreditLine(dr);
                                if (isQuit)
                                    return null;
                                if (creditline != null)
                                {
                                    journal.JournalCreditLine.Add(creditline);
                                    if (creditline.ItemSalesTaxRef != null)
                                    {
                                        journal.IsVarifyBalance = true;
                                    }
                                    try
                                    {
                                        journal.CrAmount += Convert.ToDecimal(creditline.Amount);
                                    }
                                    catch
                                    {
                                        //journal.CrAmount = Convert.ToDecimal("00.00");
                                    }
                                    coll.Add(journal);
                                }
                            }
                            else if (dr["Debit"].ToString() != string.Empty && dr["Credit"].ToString() != string.Empty)
                            {
                                //getting debit line.
                                DataProcessingBlocks.JournalDebitLine debitLine = this.GetDebitLine(dr);
                                if (isQuit)
                                    return null;
                                //Getting credit line.
                                DataProcessingBlocks.JournalCreditLine creditline = this.GetCreditLine(dr);
                                if (isQuit)
                                    return null;
                                if (debitLine != null)
                                {
                                    journal.JournalDebitLine.Add(debitLine);
                                    if (debitLine.ItemSalesTaxRef != null)
                                    {
                                        journal.IsVarifyBalance = true;
                                    }
                                    try
                                    {
                                        journal.DrAmount += Convert.ToDecimal(debitLine.Amount);
                                    }
                                    catch
                                    {
                                        //journal.DrAmount = Convert.ToDecimal("00.00");
                                    }
                                }
                                if (creditline != null)
                                {
                                    journal.JournalCreditLine.Add(creditline);
                                    if (creditline.ItemSalesTaxRef != null)
                                    {
                                        journal.IsVarifyBalance = true;
                                    }
                                    try
                                    {
                                        journal.CrAmount += Convert.ToDecimal(creditline.Amount);
                                    }
                                    catch
                                    {
                                        //journal.CrAmount = Convert.ToDecimal("00.00");
                                    }
                                }
                                //if (debitLine != null || creditline != null)
                                //{
                                    coll.Add(journal);
                                //}

                            }
                        }

                        #endregion

                        
                        #endregion
                    }
                }
                else
                {
                    return null;
                }

            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }

            #endregion

            return coll;
        }
    }
}
