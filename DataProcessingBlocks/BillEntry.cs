// ==============================================================================================
// 
// BillEntry.cs
//
// This file contains the implementations of the Bill Entry private members , 
// Properties, Constructors and Methods for QuickBooks Bill Imports.
//         BillEntry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 Mapping field
// (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using EDI.Constant;
using TransactionImporter;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("BillEntry", Namespace = "", IsNullable = false)]
    public class BillEntry
    {
        #region  Private Member Variable

        private VendorRef m_VendorRef;
        //Axis 617 
        private CurrencyRef m_CurrencyRef;
        //Axis 617 ends
        private APAccountRef m_APAccountRef;
        private string m_TxnDate;
        private string m_DueDate;
        private string m_RefNumber;
        private TermsRef m_TermsRef;
        private string m_Memo;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_ExchangeRate;
        private string m_LinkToTxnID;
        private Collection<ExpenseLineAdd> m_ExpenseLineAdd = new Collection<ExpenseLineAdd>();
        private Collection<ItemLineAdd> m_ItemLineAdd = new Collection<ItemLineAdd>();

        private DataExt m_DataExt;
       

        private DateTime m_BillDate;
        //Axis 10.0
        private string m_LinkToPurchaseOrder;

       // private string m_TxnId;
        
        #endregion

        #region Constructors

        public  BillEntry()
        {
          
        }
        
        #endregion

        #region Public Properties


        public DataExt DataExt
        {
            get { return m_DataExt; }
            set { m_DataExt = value; }
        }

        public VendorRef VendorRef
        {
            get { return m_VendorRef; }
            set { m_VendorRef = value; }
        }

        //Axis 617 
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        //Axis 617 ends
        public APAccountRef APAccountRef
        {
            get { return this.m_APAccountRef; }
            set { this.m_APAccountRef = value; }

        }

        public string LinkToPurchaseOrder
        {
            get
            { return this.m_LinkToPurchaseOrder; }
            set { this.m_LinkToPurchaseOrder = value; }
        }
        

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }


        [XmlElement(DataType = "string")]
        public string DueDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }
        }


        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }
        
       
        public TermsRef TermsRef
        {
            get { return this.m_TermsRef; }
            set { this.m_TermsRef = value; }
        }
             
       
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string IsTaxIncluded
        {
            get { return this.m_IsTaxIncluded; }
            set { this.m_IsTaxIncluded = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public string ExchangeRate
        {
            get { return this.m_ExchangeRate; }
            set { this.m_ExchangeRate = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime BillDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_BillDate) <= DateTime.MinValue)
                        return this.m_BillDate;
                    else

                        return this.m_BillDate;
                }
                catch
                {
                    return this.m_BillDate;
                }
            }
            set { this.m_BillDate = value; }
        }

          
        public string LinkToTxnID
        {
            get
            { return this.m_LinkToTxnID; }
            set { this.m_LinkToTxnID = value; }
        }

        [XmlArray("ExpenseLineAddREM")]
        public Collection<ExpenseLineAdd> ExpenseLineAdd
        {
            get { return m_ExpenseLineAdd; }
            set { m_ExpenseLineAdd = value; }
        }

        [XmlArray("ItemLineAddREM")]
        public Collection<ItemLineAdd> ItemLineAdd
        {
            get { return m_ItemLineAdd; }
            set { m_ItemLineAdd = value; }
        }

        //Axis 9
        //public string TxnId
        //{
        //    get { return m_TxnId; }
        //    set { m_TxnId = value; }
        //}

        #endregion

        #region Public Methods
           /// <summary>
           /// Creating request file for exporting Bill data to quickbook.
           /// </summary>
           /// <param name="statusMessage"></param>
           /// <param name="requestText"></param>
           /// <param name="rowcount"></param>
           /// <param name="AppName"></param>
           /// <returns></returns>
        
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

           
            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);
           
            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillAddRq aggregate and fill in field values for it
            System.Xml.XmlElement BillAddRq = requestXmlDoc.CreateElement("BillAddRq");
            inner.AppendChild(BillAddRq);

            //Create BillEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement BillAdd = requestXmlDoc.CreateElement("BillAdd");
            
            BillAddRq.AppendChild(BillAdd);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<txnid/>",string.Empty);
            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            BillAdd.InnerXml = requestXML;
            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd/CurrencyRef");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();

                }
            }
            //Axis 617 ends

            //For Remove LinkToPurchaseOrder
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("LinkToPurchaseOrder") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd/LinkToPurchaseOrder");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();

                }
            }



            if (requeststring != string.Empty)
                BillAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                BillAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            ////For TxnID.Remove LinkToTxn
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("LinkToTxnID") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd/LinkToTxnID");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();

                }
            }

            XmlNodeList xnList = requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd/ItemLineAdd");
            foreach (XmlNode childNode in xnList)
            {
                if(childNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode node = childNode.SelectSingleNode("ItemRef");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();
                }
            }

                requestText = requestXmlDoc.OuterXml;

            string responseFile = string.Empty;
            string resp = string.Empty;

            try
            {
                try
                {
                    // Axis 10.2(bug No.67)
                    responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                    //End Changes
                }
                catch
                { }

                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {   
                 resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                }
        }
           
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);                    

                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                        
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillAddRs/BillRet"))
                    {     
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }
                
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBill(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Bill ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Bill Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create BillQueryRq aggregate and fill in field values for it
            XmlElement BillQueryRq = requestXmlDoc.CreateElement("BillQueryRq");
            inner.AppendChild(BillQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            BillQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            BillQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;

           
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating Bill information
        /// of existing Bill with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateBillInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                } 
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);
            
            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement BillQBEntryModRq = requestXmlDoc.CreateElement("BillModRq");
            inner.AppendChild(BillQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement BillMod = requestXmlDoc.CreateElement("BillMod");
            BillQBEntryModRq.AppendChild(BillMod);

            requestXML = requestXML.Replace("<BillPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</BillPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<BillLineAdd>","<BillLineMod>");
            requestXML = requestXML.Replace("</BillLineAdd>","</BillLineMod>");

            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");

            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            BillMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }

            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd/CurrencyRef");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();

                }
            }
            //Axis 617 ends

            if (requeststring != string.Empty)
                BillQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By BillRefNumber) : " + rowcount.ToString());
            else
                BillQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());            

            requestText = requestXmlDoc.OuterXml;

            //For TxnID.Remove LinkToTxn
            string txnLineId = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillModRq/BillMod/ItemLineMod"))
            {
                if (oNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod/ItemLineMod/LinkToTxn");
                    node.ParentNode.RemoveChild(node);
                }
            }

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod").InsertBefore(ListID, firstChild).InnerText = listID;           
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");           
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

           

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillModRs/BillRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBill(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        //Bug NO. 412
        /// <summary>
        /// function for Append data to existing transaction for Bill
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendBillInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.BillEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement BillQBEntryModRq = requestXmlDoc.CreateElement("BillModRq");
            inner.AppendChild(BillQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement BillMod = requestXmlDoc.CreateElement("BillMod");
            BillQBEntryModRq.AppendChild(BillMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.

            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></ItemLineMod><ItemLineMod>";
            }
            for (int i = 0; i < request.Length; i++)
            {
                if (Listcnt != 0)
                {
                    if (subStringCnt == 1)
                    {                        
                        subResultString = request[i].Replace("<ItemRef>", addString + "<TxnLineID>-1</TxnLineID><ItemRef>");
                        subStringCnt++;
                    }
                    else
                    {
                        subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                    }
                }
                else
                {
                    subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                }

                if (stringCnt == request.Length)
                {
                    resultString += subResultString;
                }
                else
                {
                    resultString += subResultString + "</ItemRef>";
                }
                stringCnt++;
                Listcnt--;
            }
            requestXML = resultString;


            requestXML = requestXML.Replace("<BillPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</BillPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<BillLineAdd>", "<BillLineMod>");
            requestXML = requestXML.Replace("</BillLineAdd>", "</BillLineMod>");

            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");
         
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);

            BillMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                BillQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By BillRefNumber) : " + rowcount.ToString());
            else
                BillQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;

            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillAddRq/BillAdd/CurrencyRef");
                    node.ParentNode.RemoveChild(node);
                    node.RemoveAll();

                }
            }
            //Axis 617 ends

            //For TxnID.Remove LinkToTxn
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/BillModRq/BillMod/ItemLineMod"))
            {
                if (oNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod/ItemLineMod/LinkToTxn");
                    node.ParentNode.RemoveChild(node);

                }
            }
            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod").InsertBefore(ListID, firstChild).InnerText = listID;
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/BillModRq/BillMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/BillModRs/BillRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofBill(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
 
        #endregion
    }

    /// This method is used for getting existing Bill date, If not exists then it return null.
    public class BillEntryCollection : Collection<BillEntry>
    {
        public BillEntry FindBillEntry(DateTime date)
        {
            foreach (BillEntry item in this)
            {
                if (item.BillDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        public static int bill_cnt = 0;
        public static string bill_number = "";
        /// <summary>
        /// This method is used for getting existing Bill refno, If not exists then it return null.
        /// </summary>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public BillEntry FindBillEntry(string refNumber)
        {
            bill_number = refNumber;
            foreach (BillEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    bill_cnt++;
                    return item;
                }
            }
            bill_cnt = 0;
            return null;
        }

        /// <summary>
        /// /// This method is used for getting existing Bill date and bill refno, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public BillEntry FindBillEntry(DateTime date, string refNumber)
        {
            foreach (BillEntry item in this)
            {
                if (item.RefNumber == refNumber && item.BillDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }


    [XmlRootAttribute("ExpenseLineAdd", Namespace = "", IsNullable = false)]
    public class ExpenseLineAdd
    {
        private AccountRef m_AccountRef;
        private string m_Amount;
        private string m_Memo;
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_BillableStatus;
        private SalesRepRef m_SalesRepRef;
        
        //Improvement::548
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

        public ExpenseLineAdd()
        { }


        public AccountRef AccountRef
        {
            get
            {
                return m_AccountRef;
            }
            set
            {
                m_AccountRef = value;
            }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public string Memo
        {
            get
            {
                return this.m_Memo;
            }
            set
            {
                this.m_Memo = value;
            }
        }

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }
        public string BillableStatus
        {
            get
            {
                return this.m_BillableStatus;
            }
            set
            {
                this.m_BillableStatus = value;
            }
        }      

        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }

        //Improvement::548
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }

        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }
    }


    [XmlRootAttribute("ItemLineAdd", Namespace = "", IsNullable = false)]
    public class ItemLineAdd
    {
        private ItemRef m_ItemRef;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_Description;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Cost;
        private string m_Amount;

        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef; 
        private string m_BillableStatus;

        private SalesRepRef m_SalesRepRef;

        //Improvement::548
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

        private OverrideItemAccountRef m_OverrideItemAccount;
        private LinkToTxn m_LinkToTxn;


        public ItemLineAdd()
        { }

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }


        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }

        // axis 10.0 changhes 
        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { 
                // axis 10.0 changes for desktop connection condition
            if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
          {
            this.m_SerialNumber = value; }
            
         }
            // axis 10.0 changes ends
        }

        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set
            {
                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    this.m_LotNumber = value;
                }
                // axis 10.0 changes ends
            }
        }
        // axis 10.0 chnages ends
        public string Desc
        {
            get { return this.m_Description; }
            set { this.m_Description = value; }
        }

       
        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }
        public string Cost
        {
            get { return this.m_Cost; }
            set { this.m_Cost = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

     
        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public string BillableStatus
        {
            get { return this.m_BillableStatus; }
            set { this.m_BillableStatus = value; }
        }

        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
        //improvement::548
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }
        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideItemAccount; }
            set { this.m_OverrideItemAccount = value; }
        }

        public LinkToTxn LinkToTxn
        {
            get { return this.m_LinkToTxn; }
            set { this.m_LinkToTxn = value; }
        }
    }

    public enum BillableStatus
    {
        Billable,
        NotBillable,
        HasBeenBilled
    }
}
