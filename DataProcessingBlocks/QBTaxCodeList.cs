using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using Interop.QBXMLRP2Lib;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.Data;
using Intuit.Ipp.DataService;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using System.Threading.Tasks;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class provide Properties of QBTaxCodeList
    /// </summary>        
    public class QBTaxCodeList
    {
        #region Private Members
        private string m_Name;
        private string ticket1 = string.Empty;
        #endregion

        #region Public Properties
        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }
       
        #endregion
    }

    /// <summary>
    /// Collection Class used for Getting TaxCodeList from QuickBook.
    /// </summary>
    public class QBTaxCodeListCollection : Collection<QBTaxCodeList>
    {
        /// <summary>
        /// This method is used to get TaxCode name by passing ItemRetElement as Name.
        /// </summary>
        /// <param name="QBFileName"></param>
        /// <returns></returns>
        public Collection<QBTaxCodeList> PopulateTaxCodeList(string QBFileName)
        {
            //to store TaxCode in List
            List<string> taxList = new List<string>();

            XmlDocument xmlDoc = new XmlDocument();
            //Add processing Request
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration("1.0", null, null));
            xmlDoc.AppendChild(xmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = xmlDoc.CreateElement("QBXML");
            xmlDoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = xmlDoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement queryRequest = null;
            if (CommonUtilities.GetInstance().CountryVersion == "CA" || CommonUtilities.GetInstance().CountryVersion == "US")
            {
                queryRequest = xmlDoc.CreateElement("SalesTaxCodeQueryRq");
            }
            else
            {
                queryRequest = xmlDoc.CreateElement("ItemSalesTaxQueryRq");
            }
            qbXMLMsgsRq.AppendChild(queryRequest);

            queryRequest.SetAttribute("requestID", "1");
            XmlElement includeRetElement = xmlDoc.CreateElement("IncludeRetElement");
            queryRequest.AppendChild(includeRetElement).InnerText = "Name";

            string ticket = string.Empty;
            string responseList = string.Empty;


            //if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
            {
                try
                {
                    DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);

                    //Getting ticket for request.
                    ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                    //Processing the request.
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }


                #region commented code

                // if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                //{
                //    try
                //    {
                //        DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
                //        ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                //    }
                //    catch { }
                //}
                //else 
                //{
                //    ticket = CommonUtilities.GetInstance().ticketvalue;
                //}
                //responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                #endregion

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            else
            {
                try
                {
                    //Getting ticket for request.
                    ticket = CommonUtilities.GetInstance().ticketvalue;
                    responseList = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, xmlDoc.OuterXml);
                }
                catch {
                    // throw new Exception("Invalid file format for this connection");
                }
            }
        
            if (responseList != string.Empty)
            {
                XmlDocument XmlDocForTaxCodeList = new XmlDocument();
                XmlDocForTaxCodeList.LoadXml(responseList);

                XmlNodeList TaxCodeList = null;
                if (CommonUtilities.GetInstance().CountryVersion == "CA" || CommonUtilities.GetInstance().CountryVersion == "US")
                {
                    TaxCodeList = XmlDocForTaxCodeList.GetElementsByTagName("SalesTaxCodeRet");
                }
                else
                {
                    TaxCodeList = XmlDocForTaxCodeList.GetElementsByTagName("ItemSalesTaxRet");
                }

                foreach (XmlNode TaxCodeNode in TaxCodeList)
                {
                    QBTaxCodeList taxCodeList = new QBTaxCodeList();
                    foreach (XmlNode node in TaxCodeNode.ChildNodes)
                    {
                        if (node.Name.Equals("Name"))
                        {
                            taxCodeList.Name = node.InnerText;
                            taxList.Add(node.InnerText);
                        }
                    }
                    this.Add(taxCodeList);
                }

                taxList.Insert(0, "<None>");
                CommonUtilities.GetInstance().TaxCodeList = taxList;
            }
            return this;
        }

        public async System.Threading.Tasks.Task<Collection<QBTaxCodeList>> PopulateTaxCodeListOnline()
        {
            DataService dataService = null;

            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);

            QueryService<TaxCode> customerQueryService = new QueryService<TaxCode>(serviceContext);
            ReadOnlyCollection<Intuit.Ipp.Data.TaxCode> dataItem;
            List<string> taxList = new List<string>();
            bool status = true;
            dataItem = new ReadOnlyCollection<Intuit.Ipp.Data.TaxCode>(customerQueryService.ExecuteIdsQuery("Select * From TaxCode"));
            QBTaxCodeList taxCodeList = new QBTaxCodeList();
            foreach (var TaxCodeNode in dataItem)
            {   
                if (TaxCodeNode.Active == status)
                {
                    taxCodeList.Name = TaxCodeNode.Name.ToString();
                    taxList.Add(TaxCodeNode.Name.ToString());
                    this.Add(taxCodeList);
                    taxCodeList = new QBTaxCodeList();
                }
               
            }
           
            taxList.Insert(0, "<None>");
            CommonUtilities.GetInstance().TaxCodeList = taxList;          
            return this;
        }

    }

}
