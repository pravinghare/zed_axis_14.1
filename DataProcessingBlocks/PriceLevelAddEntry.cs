// ==============================================================================================
// 
// PriceLevelAddEntry.cs
//
// This file contains the implementations of the Price level Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Price Level Entry Imports.
//         Price Level Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping fields (CurrencyRef) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;


namespace DataProcessingBlocks
{
    [XmlRootAttribute("PriceLevelAddEntry", Namespace = "", IsNullable = false)]
    public class PriceLevelAddEntry
    {
        #region  Private Member Variable

        private string m_Name;
        private Collection<PriceLevelPerItem> m_PriceLevelPerItemAdd = new Collection<PriceLevelPerItem>();
        private CurrencyRef m_CurrencyRef;

        #endregion

        #region Constructors

        public PriceLevelAddEntry()
        {

        }

        #endregion

        #region Public Properties

        public string Name
        {
            get { return this.m_Name; }
            set { this.m_Name = value; }
        }

        [XmlArray("PriceLevelPerItemREM")]
        public Collection<PriceLevelPerItem> PriceLevelPerItem
        {
            get { return m_PriceLevelPerItemAdd; }
            set { m_PriceLevelPerItemAdd = value; }
        }

        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used for Adding new Price Level into QuickBooks.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }      
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.PriceLevelAddEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);

            }
            catch (Exception ex)
            {
                // throw;
            }


            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create PriceLevelAddRq aggregate and fill in field values for it
            System.Xml.XmlElement PriceLevelAddRq = requestXmlDoc.CreateElement("PriceLevelAddRq");
            inner.AppendChild(PriceLevelAddRq);

            //Create PriceLevelAdd aggregate and fill in field values for it
            System.Xml.XmlElement PriceLevelAdd = requestXmlDoc.CreateElement("PriceLevelAdd");


            PriceLevelAddRq.AppendChild(PriceLevelAdd);
            requestXML = requestXML.Replace("<PriceLevelPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</PriceLevelPerItemREM>", string.Empty);

            PriceLevelAdd.InnerXml = requestXML;

            //PriceLevelAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PriceLevelAddRq/PriceLevelAdd"))
            {
                if (oNode.SelectSingleNode("Name") != null)
                {
                    requeststring = oNode.SelectSingleNode("Name").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                PriceLevelAddRq.SetAttribute("requestID", "Name :" + requeststring + " RowNumber (By PricelevelName) : " + rowcount.ToString());
            else
                PriceLevelAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());
            

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PriceLevelAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PriceLevelAddRs/PriceLevelRet"))
                    {
                        //Axis-25(Axis 10.1) Price Level Import. 

                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofPriceLevel(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing price leveles listid and editsequence.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="pricelLevelName"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public Hashtable CheckAndGetIDPriceLevelExistsInQuickBooks(string pricelLevelName, string AppName)
        {
            Hashtable pricelevelTable = new Hashtable();
            
            XmlDocument requestXmlDoc = new System.Xml.XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create PriceLevelQueryRq aggregate and fill in field values for it
            System.Xml.XmlElement PriceLevelQueryRq = requestXmlDoc.CreateElement("PriceLevelQueryRq");
            inner.AppendChild(PriceLevelQueryRq);

            //Create FullName aggregate and fill in field values for it
            System.Xml.XmlElement FullName = requestXmlDoc.CreateElement("FullName");
            FullName.InnerText = pricelLevelName;
            PriceLevelQueryRq.AppendChild(FullName);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return pricelevelTable;
            }

            if (resp == string.Empty)
            {
                return pricelevelTable;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no price level exists.
                    return pricelevelTable;

                }
                else
                    if (resp.Contains("statusSeverity=\"Warn\""))
                    {
                        //Returning means there is no price level exists.
                        return pricelevelTable;
                    }
                    else
                    {
                        //Getting listid and Edit Sequence details of Price Level.
                        string listID = string.Empty;
                        string editSequence = string.Empty;
                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                        outputXMLDoc.LoadXml(resp);
                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PriceLevelQueryRs/PriceLevelRet"))
                        {
                            //Get ListID of price Level.
                            if (oNode.SelectSingleNode("ListID") != null)
                            {
                                listID = oNode.SelectSingleNode("ListID").InnerText.ToString();
                            }
                            //Get Edit Sequence of Price Level.
                            if (oNode.SelectSingleNode("EditSequence") != null)
                            {
                                editSequence = oNode.SelectSingleNode("EditSequence").InnerText.ToString();
                            }
                        }
                        if (listID != string.Empty && editSequence != string.Empty)
                        {
                            pricelevelTable.Add(listID, editSequence);
                        }
                        return pricelevelTable;
                    }
            }

        }

        /// <summary>
        /// This method is used for updating price level information
        /// of existing price level with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdatePriceLevelInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID,string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.PriceLevelAddEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create PriceLevelModRq aggregate and fill in field values for it
            System.Xml.XmlElement PriceLevelModRq = requestXmlDoc.CreateElement("PriceLevelModRq");
            inner.AppendChild(PriceLevelModRq);

            //Create PriceLevelMod aggregate and fill in field values for it
            System.Xml.XmlElement PriceLevelMod = requestXmlDoc.CreateElement("PriceLevelMod");
            PriceLevelModRq.AppendChild(PriceLevelMod);

           

            requestXML = requestXML.Replace("<PriceLevelPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</PriceLevelPerItemREM>", string.Empty);

            PriceLevelMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/PriceLevelAddRq/PriceLevelAdd"))
            {
                if (oNode.SelectSingleNode("Name") != null)
                {
                    requeststring = oNode.SelectSingleNode("Name").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
               PriceLevelModRq.SetAttribute("requestID", "Name :" + requeststring + " RowNumber (By PricelevelName) : " + rowcount.ToString());
            else
                PriceLevelModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode nameChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PriceLevelModRq/PriceLevelMod/Name");
            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PriceLevelModRq/PriceLevelMod").InsertBefore(ListID, nameChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;

            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/PriceLevelModRq/PriceLevelMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PriceLevelModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/PriceLevelModRs/PriceLevelRet"))
                    {
                        //Axis-25(Axis 10.1) Price Level Import.  

                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofPriceLevel(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class PriceLevelCollection : Collection<PriceLevelAddEntry>
    {
        /// <summary>
        /// This method is used for validate given name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PriceLevelAddEntry FindPriceLevelEntry(string name)
        {
            foreach (PriceLevelAddEntry item in this)
            {
                if (item.Name == name)
                {
                    return item;
                }
            }
            return null;
        }
    }
    /// <summary>
    ///This class for Declaring properties
    /// </summary>
    [XmlRootAttribute("PriceLevelPerItem", Namespace = "", IsNullable = false)]
    public class PriceLevelPerItem
    {
        private ItemRef m_ItemRef;
        private string m_CustomPrice;
        //private string m_CustomPricePercent;
        private string m_AdjustPercentage;
        private string m_AdjustRelativeTo;

        public PriceLevelPerItem()
        { }


        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string CustomPrice
        {
            get { return this.m_CustomPrice; }
            set { this.m_CustomPrice = value; }
        }

        //public string CustomPricePercent
        //{
        //    get { return this.m_CustomPricePercent; }
        //    set { this.m_CustomPricePercent = value; }
        //}

        public string AdjustPercentage
        {
            get { return this.m_AdjustPercentage; }
            set { this.m_AdjustPercentage = value; }
        }

        public string AdjustRelativeTo
        {
            get { return this.m_AdjustRelativeTo; }
            set { this.m_AdjustRelativeTo = value; }
        }

    }

}
