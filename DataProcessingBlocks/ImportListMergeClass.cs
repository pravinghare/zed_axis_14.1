﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDI.Constant;
using System.Data;
using System.ComponentModel;
using System.Windows.Forms;
using QuickBookEntities;
using System.Runtime.CompilerServices;

namespace DataProcessingBlocks
{
    public class ImportListMergeClass
    {
        private int cnt;
        private static ImportListMergeClass m_ImportListMergeClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constuctor
        public ImportListMergeClass()
        {

        }
        #endregion

        /// <summary>
        /// Create an instance of Import ListMerge class
        /// </summary>
        /// <returns></returns>
        public static ImportListMergeClass GetInstance()
        {
            if (m_ImportListMergeClass == null)
                m_ImportListMergeClass = new ImportListMergeClass();
            return m_ImportListMergeClass;
        }

        /// <summary>
        /// This method is used for validating import data and create ListMerge request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ListMerge QuickBooks collection </returns>
        public DataProcessingBlocks.ListMergeQBEntryCollection ImportListMergeData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            //Create an instance of ListMerge Entry collections.
            DataProcessingBlocks.ListMergeQBEntryCollection coll = new ListMergeQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            #region For ListMerge Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex) { }

                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string datevalue = string.Empty;

                    #region Add listMerge 
                    DataProcessingBlocks.ListMergeQBEntry listMerge = new ListMergeQBEntry();

                    if (dt.Columns.Contains("ListMergeType"))
                    {
                        #region Validations for ListMergeType
                        if (dr["ListMergeType"].ToString() != string.Empty)
                        {
                            listMerge.ListMergeType = dr["ListMergeType"].ToString();

                            if (listMerge.ListMergeType == null)
                            {
                                listMerge.ListMergeType = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MergeFrom"))
                    {
                        #region Validations for MergeFrom
                        if (dr["MergeFrom"].ToString() != string.Empty)
                        {
                            listMerge.MergeFrom = dr["MergeFrom"].ToString();

                            if (listMerge.MergeFrom == null)
                            {
                                listMerge.MergeFrom = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MergeTo"))
                    {
                        #region Validations for MergeTo
                        if (dr["MergeTo"].ToString() != string.Empty)
                        {
                            listMerge.MergeTo = dr["MergeTo"].ToString();

                            if (listMerge.MergeTo == null)
                            {
                                listMerge.MergeTo = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SameShipAddrOk"))
                    {
                        #region Validations of SameShipAddrOk
                        if (dr["SameShipAddrOk"].ToString() != "<None>" || dr["SameShipAddrOk"].ToString() != string.Empty)
                        {
                            int result = 0;
                            if (int.TryParse(dr["SameShipAddrOk"].ToString(), out result))
                            {
                                listMerge.SameShipAddrOk = Convert.ToInt32(dr["SameShipAddrOk"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["SameShipAddrOk"].ToString().ToLower() == "true")
                                {
                                    listMerge.SameShipAddrOk = dr["SameShipAddrOk"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["SameShipAddrOk"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        listMerge.SameShipAddrOk = dr["SameShipAddrOk"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SameShipAddrOk (" + dr["SameShipAddrOk"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            listMerge.SameShipAddrOk = dr["SameShipAddrOk"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            listMerge.SameShipAddrOk = dr["SameShipAddrOk"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        listMerge.SameShipAddrOk = dr["SameShipAddrOk"].ToString();
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    coll.Add(listMerge);
                    #endregion
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region 
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return coll;
        }
    }
}
