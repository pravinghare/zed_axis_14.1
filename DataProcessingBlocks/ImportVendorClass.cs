using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportVendorClass
    {
        private static ImportVendorClass m_ImportVendorClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportVendorClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import Vendor class
        /// </summary>
        /// <returns></returns>
        public static ImportVendorClass GetInstance()
        {
            if (m_ImportVendorClass == null)
                m_ImportVendorClass = new ImportVendorClass();
            return m_ImportVendorClass;
        }


        /// <summary>
        /// This method is used for validating import data and create Vendor and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Vendor QuickBooks collection </returns>
        public DataProcessingBlocks.VendorQBEntryCollection ImportVendorData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of Vendor Entry collections.
            DataProcessingBlocks.VendorQBEntryCollection coll = new VendorQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Vendor Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime VendorDt = new DateTime();
                    string datevalue = string.Empty;

                    //Vendor Validation
                    DataProcessingBlocks.VendorQBEntry Vendor = new VendorQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Name = dr["Name"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                Vendor.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    Vendor.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Vendor.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Vendor.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Vendor.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CompanyName"))
                    {
                        #region Validations of CompanyName
                        if (dr["CompanyName"].ToString() != string.Empty)
                        {
                            if (dr["CompanyName"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CompanyName (" + dr["CompanyName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.CompanyName = dr["CompanyName"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.CompanyName = dr["CompanyName"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.CompanyName = dr["CompanyName"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.CompanyName = dr["CompanyName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Salutation"))
                    {
                        #region Validations of Salutation
                        if (dr["Salutation"].ToString() != string.Empty)
                        {
                            if (dr["Salutation"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Salutation (" + dr["Salutation"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Salutation = dr["Salutation"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Salutation = dr["Salutation"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.Salutation = dr["Salutation"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.Salutation = dr["Salutation"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("FirstName"))
                    {
                        #region Validations of FirstName
                        if (dr["FirstName"].ToString() != string.Empty)
                        {
                            if (dr["FirstName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This FirstName (" + dr["FirstName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.FirstName = dr["FirstName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.FirstName = dr["FirstName"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.FirstName = dr["FirstName"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.FirstName = dr["FirstName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("MiddleName"))
                    {
                        #region Validations of MiddleName
                        if (dr["MiddleName"].ToString() != string.Empty)
                        {
                            if (dr["MiddleName"].ToString().Length > 5)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This MiddleName (" + dr["MiddleName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.MiddleName = dr["MiddleName"].ToString().Substring(0, 5);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.MiddleName = dr["MiddleName"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.MiddleName = dr["MiddleName"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.MiddleName = dr["MiddleName"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("LastName"))
                    {
                        #region Validations of LastName
                        if (dr["LastName"].ToString() != string.Empty)
                        {
                            if (dr["LastName"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LastName (" + dr["LastName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.LastName = dr["LastName"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.LastName = dr["LastName"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.LastName = dr["LastName"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.LastName = dr["LastName"].ToString();
                            }
                        }
                        #endregion
                    }
                    QuickBookEntities.VendorAddress VendorAddressItem = new VendorAddress();
                    if (dt.Columns.Contains("VendorAddr1"))
                    {
                        #region Validations of Vendor Addr1
                        if (dr["VendorAddr1"].ToString() != string.Empty)
                        {
                            if (dr["VendorAddr1"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Addr1 (" + dr["VendorAddr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                                }
                            }
                            else
                            {
                                VendorAddressItem.Addr1 = dr["VendorAddr1"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorAddr2"))
                    {
                        #region Validations of Vendor Addr2
                        if (dr["VendorAddr2"].ToString() != string.Empty)
                        {
                            if (dr["VendorAddr2"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Addr2 (" + dr["VendorAddr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                                }
                            }
                            else
                            {
                                VendorAddressItem.Addr2 = dr["VendorAddr2"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("VendorAddr3"))
                    {
                        #region Validations of Vendor Addr3
                        if (dr["VendorAddr3"].ToString() != string.Empty)
                        {
                            if (dr["VendorAddr3"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Add3 (" + dr["VendorAddr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                                    }
                                }
                                else
                                    VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();

                            }
                            else
                            {
                                VendorAddressItem.Addr3 = dr["VendorAddr3"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorAddr4"))
                    {
                        #region Validations of Vendor Addr4
                        if (dr["VendorAddr4"].ToString() != string.Empty)
                        {
                            if (dr["VendorAddr4"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Add4 (" + dr["VendorAddr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString().Substring(0, 41);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                                }
                            }
                            else
                            {
                                VendorAddressItem.Addr4 = dr["VendorAddr4"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorAddr5"))
                    {
                        #region Validations of Vendor Addr5
                        if (dr["VendorAddr5"].ToString() != string.Empty)
                        {
                            if (dr["VendorAddr5"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Add5 (" + dr["VendorAddr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                                    }
                                }
                                else
                                    VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                            }
                            else
                            {
                                VendorAddressItem.Addr5 = dr["VendorAddr5"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorCity"))
                    {
                        #region Validations of Vendor City
                        if (dr["VendorCity"].ToString() != string.Empty)
                        {
                            if (dr["VendorCity"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor City (" + dr["VendorCity"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.City = dr["VendorCity"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.City = dr["VendorCity"].ToString();
                                    }
                                }
                                else
                                    VendorAddressItem.City = dr["VendorCity"].ToString();
                            }
                            else
                            {
                                VendorAddressItem.City = dr["VendorCity"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorState"))
                    {
                        #region Validations of Vendor State
                        if (dr["VendorState"].ToString() != string.Empty)
                        {
                            if (dr["VendorState"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor State (" + dr["VendorState"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.State = dr["VendorState"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.State = dr["VendorState"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.State = dr["VendorState"].ToString();
                                }
                            }
                            else
                            {
                                VendorAddressItem.State = dr["VendorState"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorPostalCode"))
                    {
                        #region Validations of Vendor Postal Code
                        if (dr["VendorPostalCode"].ToString() != string.Empty)
                        {
                            if (dr["VendorPostalCode"].ToString().Length > 13)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Postal Code (" + dr["VendorPostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString().Substring(0, 13);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                                    }
                                }
                                else
                                    VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                            }
                            else
                            {
                                VendorAddressItem.PostalCode = dr["VendorPostalCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorCountry"))
                    {
                        #region Validations of Vendor Country
                        if (dr["VendorCountry"].ToString() != string.Empty)
                        {
                            if (dr["VendorCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Country (" + dr["VendorCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Country = dr["VendorCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                    }
                                }
                                else
                                {
                                    VendorAddressItem.Country = dr["VendorCountry"].ToString();
                                }
                            }
                            else
                            {
                                VendorAddressItem.Country = dr["VendorCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorNote"))
                    {
                        #region Validations of Vendor Note
                        if (dr["VendorNote"].ToString() != string.Empty)
                        {
                            if (dr["VendorNote"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Vendor Note (" + dr["VendorNote"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        VendorAddressItem.Note = dr["VendorNote"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        VendorAddressItem.Note = dr["VendorNote"].ToString();
                                    }
                                }
                                else
                                    VendorAddressItem.Note = dr["VendorNote"].ToString();
                            }
                            else
                            {
                                VendorAddressItem.Note = dr["VendorNote"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (VendorAddressItem.Addr1 != null || VendorAddressItem.Addr2 != null || VendorAddressItem.Addr3 != null || VendorAddressItem.Addr4 != null || VendorAddressItem.Addr5 != null
                                || VendorAddressItem.City != null || VendorAddressItem.Country != null || VendorAddressItem.PostalCode != null || VendorAddressItem.State != null || VendorAddressItem.Note != null)
                        Vendor.VendorAddress.Add(VendorAddressItem);

                    
                    if (dt.Columns.Contains("Phone"))
                    {
                        #region Validations of Phone
                        if (dr["Phone"].ToString() != string.Empty)
                        {
                            if (dr["Phone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Phone = dr["Phone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.Phone = dr["Phone"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.Phone = dr["Phone"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AltPhone"))
                    {
                        #region Validations of Alt Phone
                        if (dr["AltPhone"].ToString() != string.Empty)
                        {
                            if (dr["AltPhone"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltPhone (" + dr["AltPhone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.AltPhone = dr["AltPhone"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.AltPhone = dr["AltPhone"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.AltPhone = dr["AltPhone"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.AltPhone = dr["AltPhone"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("Fax"))
                    {
                        #region Validations of Fax
                        if (dr["Fax"].ToString() != string.Empty)
                        {
                            if (dr["Fax"].ToString().Length > 21)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Fax = dr["Fax"].ToString().Substring(0, 21);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.Fax = dr["Fax"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.Fax = dr["Fax"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Email"))
                    {
                        #region Validations of Email
                        if (dr["Email"].ToString() != string.Empty)
                        {
                            if (dr["Email"].ToString().Length > 1023)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Email = dr["Email"].ToString().Substring(0, 1023);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.Email = dr["Email"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.Email = dr["Email"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Contact"))
                    {
                        #region Validations of Contact
                        if (dr["Contact"].ToString() != string.Empty)
                        {
                            if (dr["Contact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Contact (" + dr["Contact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Contact = dr["Contact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Contact = dr["Contact"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.Contact = dr["Contact"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.Contact = dr["Contact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AltContact"))
                    {
                        #region Validations of AltContact
                        if (dr["AltContact"].ToString() != string.Empty)
                        {
                            if (dr["AltContact"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This AltContact (" + dr["AltContact"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.AltContact = dr["AltContact"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.AltContact = dr["AltContact"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.AltContact = dr["AltContact"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.AltContact = dr["AltContact"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("NameOnCheck"))
                    {
                        #region Validations of AltContact
                        if (dr["NameOnCheck"].ToString() != string.Empty)
                        {
                            if (dr["NameOnCheck"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This NameOnCheck (" + dr["NameOnCheck"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.NameOnCheck = dr["NameOnCheck"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.NameOnCheck = dr["NameOnCheck"].ToString();
                                    }
                                }
                                else
                                {
                                    Vendor.NameOnCheck = dr["NameOnCheck"].ToString();
                                }
                            }
                            else
                            {
                                Vendor.NameOnCheck = dr["NameOnCheck"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("AccountNumber"))
                    {
                        #region Validations of Account Number
                        if (dr["AccountNumber"].ToString() != string.Empty)
                        {
                            if (dr["AccountNumber"].ToString().Length > 99)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Account Number (" + dr["AccountNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.AccountNumber = dr["AccountNumber"].ToString().Substring(0, 99);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.AccountNumber = dr["AccountNumber"].ToString();
                                    }
                                }
                                else
                                    Vendor.AccountNumber = dr["AccountNumber"].ToString();
                            }
                            else
                            {
                                Vendor.AccountNumber = dr["AccountNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Notes"))
                    {
                        #region Validations of Notes
                        if (dr["Notes"].ToString() != string.Empty)
                        {
                            if (dr["Notes"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Notes (" + dr["JobDNotesesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.Notes = dr["Notes"].ToString().Substring(0, 4095);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.Notes = dr["Notes"].ToString();
                                    }
                                }
                                else
                                    Vendor.Notes = dr["Notes"].ToString();
                            }
                            else
                            {
                                Vendor.Notes = dr["Notes"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("VendorTypeFullName"))
                    {
                        #region Validations of VendorType FullName
                        if (dr["VendorTypeFullName"].ToString() != string.Empty)
                        {
                            if (dr["VendorTypeFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This VendorType FullName (" + dr["VendorTypeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.VendorTypeRef = new VendorTypeRef(dr["VendorTypeFullName"].ToString());
                                        if (Vendor.VendorTypeRef.FullName == null)
                                            Vendor.VendorTypeRef.FullName = null;
                                        else
                                            Vendor.VendorTypeRef = new VendorTypeRef(dr["VendorTypeFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.VendorTypeRef = new VendorTypeRef(dr["VendorTypeFullName"].ToString());
                                        if (Vendor.VendorTypeRef.FullName == null)
                                            Vendor.VendorTypeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.VendorTypeRef = new VendorTypeRef(dr["VendorTypeFullName"].ToString());
                                    if (Vendor.VendorTypeRef.FullName == null)
                                        Vendor.VendorTypeRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.VendorTypeRef = new VendorTypeRef(dr["VendorTypeFullName"].ToString());
                                if (Vendor.VendorTypeRef.FullName == null)
                                    Vendor.VendorTypeRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TermsFullName"))
                    {
                        #region Validations of Term FullName
                        if (dr["TermsFullName"].ToString() != string.Empty)
                        {
                            if (dr["TermsFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TermRef FullName  (" + dr["TermsFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.TermsRef = new TermsRef(dr["TermsFullName"].ToString());
                                        if (Vendor.TermsRef.FullName == null)
                                            Vendor.TermsRef.FullName = null;
                                        else
                                            Vendor.TermsRef = new TermsRef(dr["TermsFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.TermsRef = new TermsRef(dr["TermsFullName"].ToString());
                                        if (Vendor.TermsRef.FullName == null)
                                            Vendor.TermsRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.TermsRef = new TermsRef(dr["TermsFullName"].ToString());
                                    if (Vendor.TermsRef.FullName == null)
                                        Vendor.TermsRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.TermsRef = new TermsRef(dr["TermsFullName"].ToString());
                                if (Vendor.TermsRef.FullName == null)
                                    Vendor.TermsRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CreditLimit"))
                    {
                        #region Validations for CreditLimit
                        if (dr["CreditLimit"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["CreditLimit"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This CreditLimit ( " + dr["CreditLimit"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.CreditLimit = dr["CreditLimit"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.CreditLimit = dr["CreditLimit"].ToString();
                                    }
                                }
                                else
                                    Vendor.CreditLimit = dr["CreditLimit"].ToString();
                            }
                            else
                            {
                                Vendor.CreditLimit = string.Format("{0:00000000.00}", Convert.ToDouble(dr["CreditLimit"].ToString()));
                            }
                        }

                        #endregion
                    }

                    if (dt.Columns.Contains("VendorTaxIdent"))
                    {
                        #region Validations of VendorTaxIdent
                        if (dr["VendorTaxIdent"].ToString() != string.Empty)
                        {
                            if (dr["VendorTaxIdent"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This VendorTaxIdent (" + dr["VendorTaxIdent"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.VendorTaxIdent = dr["VendorTaxIdent"].ToString().Substring(0, 15);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.VendorTaxIdent = dr["VendorTaxIdent"].ToString();
                                    }
                                }
                                else
                                    Vendor.VendorTaxIdent = dr["VendorTaxIdent"].ToString();
                            }
                            else
                            {
                                Vendor.VendorTaxIdent = dr["VendorTaxIdent"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsVendorEligibleFor1099"))
                    {
                        #region Validations of IsVendorEligibleFor1099
                        if (dr["IsVendorEligibleFor1099"].ToString() != "<None>" || dr["IsVendorEligibleFor1099"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsVendorEligibleFor1099"].ToString(), out result))
                            {
                                Vendor.IsVendorEligibleFor1099 = Convert.ToInt32(dr["IsVendorEligibleFor1099"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsVendorEligibleFor1099"].ToString().ToLower() == "true")
                                {
                                    Vendor.IsVendorEligibleFor1099 = dr["IsVendorEligibleFor1099"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsVendorEligibleFor1099"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Vendor.IsVendorEligibleFor1099 = dr["IsVendorEligibleFor1099"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsVendorEligibleFor1099 (" + dr["IsVendorEligibleFor1099"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Vendor.IsVendorEligibleFor1099 = dr["IsVendorEligibleFor1099"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.IsVendorEligibleFor1099 = dr["IsVendorEligibleFor1099"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Vendor.IsVendorEligibleFor1099 = dr["IsVendorEligibleFor1099"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OpenBalance"))
                    {
                        #region Validations for OpenBalance
                        if (dr["OpenBalance"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["OpenBalance"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This OpenBalance( " + dr["OpenBalance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.OpenBalance = dr["OpenBalance"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.OpenBalance = dr["OpenBalance"].ToString();
                                    }
                                }
                                else
                                    Vendor.OpenBalance = dr["OpenBalance"].ToString();
                            }
                            else
                            {
                                Vendor.OpenBalance = string.Format("{0:00000000.00}", Convert.ToDouble(dr["OpenBalance"].ToString()));
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("OpenBalanceDate"))
                    {
                        #region validations of OpenBalanceDate
                        if (dr["OpenBalanceDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["OpenBalanceDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out VendorDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;
                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                if (isIgnoreAll == false)
                                {
                                        string strMessages = "This OpenBalanceDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                            Vendor.OpenBalanceDate = datevalue;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                            Vendor.OpenBalanceDate = datevalue;
                                    }
                                }
                                else
                                    {
                                        Vendor.OpenBalanceDate = datevalue;
                            }
                                }
                            else
                            {
                                    VendorDt = dttest;
                                    Vendor.OpenBalanceDate = dttest.ToString("yyyy-MM-dd");
                                }
                            }
                            else
                            {
                                VendorDt = Convert.ToDateTime(datevalue);
                                Vendor.OpenBalanceDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                        }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BillingRateFullName"))
                    {
                        #region Validations of BillingRate FullName
                        if (dr["BillingRateFullName"].ToString() != string.Empty)
                        {
                            if (dr["BillingRateFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BillingRate FullName  (" + dr["BillingRateFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                        if (Vendor.BillingRateRef.FullName == null)
                                            Vendor.BillingRateRef.FullName = null;
                                        else
                                            Vendor.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                        if (Vendor.BillingRateRef.FullName == null)
                                            Vendor.BillingRateRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                    if (Vendor.BillingRateRef.FullName == null)
                                        Vendor.BillingRateRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.BillingRateRef = new BillingRateRef(dr["BillingRateFullName"].ToString());
                                if (Vendor.BillingRateRef.FullName == null)
                                    Vendor.BillingRateRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ExternalGUID"))
                    {
                        #region Validations of ExternalGUID
                        if (dr["ExternalGUID"].ToString() != string.Empty)
                        {
                            if (dr["ExternalGUID"].ToString().Length > 41)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ExternalGUID (" + dr["ExternalGUID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.ExternalGUID = dr["ExternalGUID"].ToString().Substring(0, 41);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.ExternalGUID = dr["ExternalGUID"].ToString();
                                    }
                                }
                                else
                                    Vendor.ExternalGUID = dr["NotExternalGUIDes"].ToString();
                            }
                            else
                            {
                                Vendor.ExternalGUID = dr["ExternalGUID"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                    {
                        if (dt.Columns.Contains("SalesTaxCodeFullName"))
                        {
                            #region Validations of SalesTaxCode FullName
                            if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SalesTaxCode FullName  (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Vendor.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Vendor.SalesTaxCodeRef.FullName == null)
                                                Vendor.SalesTaxCodeRef.FullName = null;
                                            else
                                                Vendor.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Vendor.SalesTaxCodeRef.FullName == null)
                                                Vendor.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Vendor.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (Vendor.SalesTaxCodeRef.FullName == null)
                                            Vendor.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                    if (Vendor.SalesTaxCodeRef.FullName == null)
                                        Vendor.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            #endregion
                        }
                    }
                    if (dt.Columns.Contains("SalesTaxCountry"))
                    {
                        #region Validations of SalesTax Country
                        if (dr["SalesTaxCountry"].ToString() != string.Empty)
                        {
                            if (dr["SalesTaxCountry"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTax Country (" + dr["SalesTaxCountry"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.SalesTaxCountry = dr["SalesTaxCountry"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.SalesTaxCountry = dr["SalesTaxCountry"].ToString();
                                    }
                                }
                                else
                                    Vendor.SalesTaxCountry = dr["SalesTaxCountry"].ToString();
                            }
                            else
                            {
                                Vendor.SalesTaxCountry = dr["SalesTaxCountry"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsSalesTaxAgency"))
                    {
                        #region Validations of IsSalesTaxAgency
                        if (dr["IsSalesTaxAgency"].ToString() != "<None>" || dr["IsSalesTaxAgency"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsSalesTaxAgency"].ToString(), out result))
                            {
                                Vendor.IsSalesTaxAgency = Convert.ToInt32(dr["IsSalesTaxAgency"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsSalesTaxAgency"].ToString().ToLower() == "true")
                                {
                                    Vendor.IsSalesTaxAgency = dr["IsSalesTaxAgency"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsSalesTaxAgency"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Vendor.IsSalesTaxAgency = dr["IsSalesTaxAgency"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsSalesTaxAgency (" + dr["IsSalesTaxAgency"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Vendor.IsSalesTaxAgency = dr["IsSalesTaxAgency"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.IsSalesTaxAgency = dr["IsSalesTaxAgency"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Vendor.IsSalesTaxAgency = dr["IsSalesTaxAgency"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesTaxReturnFullName"))
                    {
                        #region Validations of SalesTaxReturn FullName
                        if (dr["SalesTaxReturnFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesTaxReturnFullName"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTaxReturn FullName  (" + dr["SalesTaxReturnFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.SalesTaxReturnRef = new SalesTaxReturnRef(dr["SalesTaxReturnFullName"].ToString());
                                        if (Vendor.SalesTaxReturnRef.FullName == null)
                                            Vendor.SalesTaxReturnRef.FullName = null;
                                        else
                                            Vendor.SalesTaxReturnRef = new SalesTaxReturnRef(dr["SalesTaxReturnFullName"].ToString().Substring(0, 31));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.SalesTaxReturnRef = new SalesTaxReturnRef(dr["SalesTaxReturnFullName"].ToString());
                                        if (Vendor.SalesTaxReturnRef.FullName == null)
                                            Vendor.SalesTaxReturnRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.SalesTaxReturnRef = new SalesTaxReturnRef(dr["SalesTaxReturnFullName"].ToString());
                                    if (Vendor.SalesTaxReturnRef.FullName == null)
                                        Vendor.SalesTaxReturnRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.SalesTaxReturnRef = new SalesTaxReturnRef(dr["SalesTaxReturnFullName"].ToString());
                                if (Vendor.SalesTaxReturnRef.FullName == null)
                                    Vendor.SalesTaxReturnRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TaxRegistrationNumber"))
                    {
                        #region Validations of TaxRegistration Number
                        if (dr["TaxRegistrationNumber"].ToString() != string.Empty)
                        {
                            if (dr["TaxRegistrationNumber"].ToString().Length > 30)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxRegistration Number (" + dr["TaxRegistrationNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString().Substring(0, 30);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString();
                                    }
                                }
                                else
                                    Vendor.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString();
                            }
                            else
                            {
                                Vendor.TaxRegistrationNumber = dr["TaxRegistrationNumber"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ReportingPeriod"))
                    {
                        #region Validations of Reporting Period
                        if (dr["ReportingPeriod"].ToString() != string.Empty)
                        {
                            if (dr["ReportingPeriod"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Reporting Period (" + dr["ReportingPeriod"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.ReportingPeriod = dr["ReportingPeriod"].ToString().Substring(0, 31);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.ReportingPeriod = dr["ReportingPeriod"].ToString();
                                    }
                                }
                                else
                                    Vendor.ReportingPeriod = dr["ReportingPeriod"].ToString();
                            }
                            else
                            {
                                Vendor.ReportingPeriod = dr["ReportingPeriod"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsTaxTrackedOnPurchases"))
                    {
                        #region Validations of IsTaxTrackedOnPurchases
                        if (dr["IsTaxTrackedOnPurchases"].ToString() != "<None>" || dr["IsTaxTrackedOnPurchases"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsTaxTrackedOnPurchases"].ToString(), out result))
                            {
                                Vendor.IsTaxTrackedOnPurchases = Convert.ToInt32(dr["IsTaxTrackedOnPurchases"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsTaxTrackedOnPurchases"].ToString().ToLower() == "true")
                                {
                                    Vendor.IsTaxTrackedOnPurchases = dr["IsTaxTrackedOnPurchases"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsTaxTrackedOnPurchases"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Vendor.IsTaxTrackedOnPurchases = dr["IsTaxTrackedOnPurchases"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsTaxTrackedOnPurchases (" + dr["IsTaxTrackedOnPurchases"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Vendor.IsTaxTrackedOnPurchases = dr["IsTaxTrackedOnPurchases"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.IsTaxTrackedOnPurchases = dr["IsTaxTrackedOnPurchases"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Vendor.IsTaxTrackedOnPurchases = dr["IsTaxTrackedOnPurchases"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TaxOnPurchasesAccountFullName"))
                    {
                        #region Validations of TaxOnPurchasesAccount FullName
                        if (dr["TaxOnPurchasesAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["TaxOnPurchasesAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxOnPurchasesAccount FullName  (" + dr["TaxOnPurchasesAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.TaxOnPurchasesAccountRef = new TaxOnPurchasesAccountRef(dr["TaxOnPurchasesAccountFullName"].ToString());
                                        if (Vendor.TaxOnPurchasesAccountRef.FullName == null)
                                            Vendor.TaxOnPurchasesAccountRef.FullName = null;
                                        else
                                            Vendor.TaxOnPurchasesAccountRef = new TaxOnPurchasesAccountRef(dr["TaxOnPurchasesAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.TaxOnPurchasesAccountRef = new TaxOnPurchasesAccountRef(dr["TaxOnPurchasesAccountFullName"].ToString());
                                        if (Vendor.TaxOnPurchasesAccountRef.FullName == null)
                                            Vendor.TaxOnPurchasesAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.TaxOnPurchasesAccountRef = new TaxOnPurchasesAccountRef(dr["TaxOnPurchasesAccountFullName"].ToString());
                                    if (Vendor.TaxOnPurchasesAccountRef.FullName == null)
                                        Vendor.TaxOnPurchasesAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.TaxOnPurchasesAccountRef = new TaxOnPurchasesAccountRef(dr["TaxOnPurchasesAccountFullName"].ToString());
                                if (Vendor.TaxOnPurchasesAccountRef.FullName == null)
                                    Vendor.TaxOnPurchasesAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsTaxTrackedOnSales"))
                    {
                        #region Validations of IsTaxTrackedOnSales
                        if (dr["IsTaxTrackedOnSales"].ToString() != "<None>" || dr["IsTaxTrackedOnSales"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsTaxTrackedOnSales"].ToString(), out result))
                            {
                                Vendor.IsTaxTrackedOnSales = Convert.ToInt32(dr["IsTaxTrackedOnSales"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsTaxTrackedOnSales"].ToString().ToLower() == "true")
                                {
                                    Vendor.IsTaxTrackedOnSales = dr["IsTaxTrackedOnSales"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsTaxTrackedOnSales"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Vendor.IsTaxTrackedOnSales = dr["IsTaxTrackedOnSales"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsTaxTrackedOnSales (" + dr["IsTaxTrackedOnSales"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Vendor.IsTaxTrackedOnSales = dr["IsTaxTrackedOnSales"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.IsTaxTrackedOnSales = dr["IsTaxTrackedOnSales"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Vendor.IsTaxTrackedOnSales = dr["IsTaxTrackedOnSales"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TaxOnSalesAccountFullName"))
                    {
                        #region Validations of TaxOnSalesAccount FullName
                        if (dr["TaxOnSalesAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["TaxOnSalesAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TaxOnSalesAccount FullName  (" + dr["TaxOnSalesAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.TaxOnSalesAccountRef = new TaxOnSalesAccountRef(dr["TaxOnSalesAccountFullName"].ToString());
                                        if (Vendor.TaxOnSalesAccountRef.FullName == null)
                                            Vendor.TaxOnSalesAccountRef.FullName = null;
                                        else
                                            Vendor.TaxOnSalesAccountRef = new TaxOnSalesAccountRef(dr["TaxOnSalesAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.TaxOnSalesAccountRef = new TaxOnSalesAccountRef(dr["TaxOnSalesAccountFullName"].ToString());
                                        if (Vendor.TaxOnSalesAccountRef.FullName == null)
                                            Vendor.TaxOnSalesAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.TaxOnSalesAccountRef = new TaxOnSalesAccountRef(dr["TaxOnSalesAccountFullName"].ToString());
                                    if (Vendor.TaxOnSalesAccountRef.FullName == null)
                                        Vendor.TaxOnSalesAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.TaxOnSalesAccountRef = new TaxOnSalesAccountRef(dr["TaxOnSalesAccountFullName"].ToString());
                                if (Vendor.TaxOnSalesAccountRef.FullName == null)
                                    Vendor.TaxOnSalesAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("IsTaxOnTax"))
                    {
                        #region Validations of IsTaxOnTax
                        if (dr["IsTaxOnTax"].ToString() != "<None>" || dr["IsTaxOnTax"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsTaxOnTax"].ToString(), out result))
                            {
                                Vendor.IsTaxOnTax = Convert.ToInt32(dr["IsTaxOnTax"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsTaxOnTax"].ToString().ToLower() == "true")
                                {
                                    Vendor.IsTaxOnTax = dr["IsTaxOnTax"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsTaxOnTax"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        Vendor.IsTaxOnTax = dr["IsTaxOnTax"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsTaxOnTax (" + dr["IsTaxOnTax"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            Vendor.IsTaxOnTax = dr["IsTaxOnTax"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Vendor.IsTaxOnTax = dr["IsTaxOnTax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Vendor.IsTaxOnTax = dr["IsTaxOnTax"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("PrefillAccountFullName"))
                    {
                        #region Validations of PrefillAccount FullName
                        if (dr["PrefillAccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["PrefillAccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This PrefillAccount FullName   (" + dr["PrefillAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.PrefillAccountRef = new PrefillAccountRef(dr["PrefillAccountFullName"].ToString());
                                        if (Vendor.PrefillAccountRef.FullName == null)
                                            Vendor.PrefillAccountRef.FullName = null;
                                        else
                                            Vendor.PrefillAccountRef = new PrefillAccountRef(dr["PrefillAccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.PrefillAccountRef = new PrefillAccountRef(dr["PrefillAccountFullName"].ToString());
                                        if (Vendor.PrefillAccountRef.FullName == null)
                                            Vendor.PrefillAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.PrefillAccountRef = new PrefillAccountRef(dr["PrefillAccountFullName"].ToString());
                                    if (Vendor.PrefillAccountRef.FullName == null)
                                        Vendor.PrefillAccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.PrefillAccountRef = new PrefillAccountRef(dr["PrefillAccountFullName"].ToString());
                                if (Vendor.PrefillAccountRef.FullName == null)
                                    Vendor.PrefillAccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("CurrencyFullName"))
                    {
                        #region Validations of Currency FullName
                        if (dr["CurrencyFullName"].ToString() != string.Empty)
                        {
                            if (dr["CurrencyFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Currency FullName   (" + dr["CurrencyFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        Vendor.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                        if (Vendor.CurrencyRef.FullName == null)
                                            Vendor.CurrencyRef.FullName = null;
                                        else
                                            Vendor.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        Vendor.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                        if (Vendor.CurrencyRef.FullName == null)
                                            Vendor.CurrencyRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Vendor.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                    if (Vendor.CurrencyRef.FullName == null)
                                        Vendor.CurrencyRef.FullName = null;
                                }
                            }
                            else
                            {
                                Vendor.CurrencyRef = new CurrencyRef(dr["CurrencyFullName"].ToString());
                                if (Vendor.CurrencyRef.FullName == null)
                                    Vendor.CurrencyRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    coll.Add(Vendor);

                }
                else
                {
                    return null;
                }
            }
            #endregion


            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }
         
            #endregion

            return coll;
        }

    }
}
