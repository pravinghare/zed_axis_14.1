// ==============================================================================================
// 
// SalesReceiptQbEntry.cs
//
// This file contains the implementations of the Sales Receipt Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Sales Receipt QB Entry Imports.
//         Sales Receipt Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping fields (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================
using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("SalesReceiptQBEntry", Namespace = "", IsNullable = false)]
    public class SalesReceiptQBEntry
    {
        #region  Private Member Variable
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private TemplateRef m_TemplateRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private string m_IsPending;
        private string m_CheckNumber;
        private PaymentMethodRef m_PaymentMethodRef;
        private string m_DueDate;
        private SalesRepRef m_SalesRepRef;
        private string m_FOB;
        private string m_ShipDate;
        private ShipMethodRef m_ShipMethodRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_Memo;
        private CustomerMsgRef m_CustomerMsgRef;
        private string m_IsToBePrinted;
        private string m_IsToBeEmailed;
        private string m_IsTaxIncluded;
        private CustomerSalesTaxCodeRef m_CustomerSalesTaxCodeRef;
        private DepositToAccountRef m_DepositToAccountRef;
        private CreditCardTxnInputInfo m_CreditCardTxnInputInfo;
        private CreditCardTxnResultInfo m_CreditCardTxnResultInfo;
        private string m_ExchangeRate;
        private string m_Others;
        private Collection<BillAddress> m_BillAddress = new Collection<BillAddress>();
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        private Collection<SalesReceiptLineAdd> m_SalesReceiptLineAdd = new Collection<SalesReceiptLineAdd>();


        private string m_IncludeRetElement;
        private DateTime m_SalesReceiptDate;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;
        //Axis 10.0
        private Collection<DiscountLineAdd> m_DiscountLineAdd = new Collection<DiscountLineAdd>();
        private Collection<ShippingLineAdd> m_ShippingLineAdd = new Collection<ShippingLineAdd>();
        private Collection<SalesReceiptLineGroupAdd> m_SalesReceiptLineGroupAdd = new Collection<SalesReceiptLineGroupAdd>();
        /// <summary>
        ///bug 442 11.4
        /// </summary>
        private CurrencyRef m_CurrencyRef;
        #endregion

        #region Construtor

        public SalesReceiptQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }
        /// <summary>
        /// 11.4  bug no 442
        /// </summary>
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public TemplateRef TemplateRef
        {
            get { return m_TemplateRef; }
            set { m_TemplateRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }



        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        [XmlArray("BillAddressREM")]
        public Collection<BillAddress> BillAddress
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }


        public string CheckNumber
        {
            get { return m_CheckNumber; }
            set { m_CheckNumber = value; }
        }


        public PaymentMethodRef PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }

        [XmlElement(DataType = "string")]
        public String DueDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }
        }


        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }

        }

        public ShipMethodRef ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return m_ItemSalesTaxRef; }
            set { m_ItemSalesTaxRef = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }


        public CustomerMsgRef CustomerMsgRef
        {
            get { return m_CustomerMsgRef; }
            set { m_CustomerMsgRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }


        [XmlElement(DataType = "string")]
        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }
        public CustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get { return m_CustomerSalesTaxCodeRef; }
            set { m_CustomerSalesTaxCodeRef = value; }
        }

        public DepositToAccountRef DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }

        [XmlIgnoreAttribute()]
        public CreditCardTxnInputInfo CreditCardTxnInputInfo
        {
            get { return m_CreditCardTxnInputInfo; }
            set { m_CreditCardTxnInputInfo = value; }
        }

        [XmlIgnoreAttribute()]
        public CreditCardTxnResultInfo CreditCardTxnResultInfo
        {
            get { return m_CreditCardTxnResultInfo; }
            set { m_CreditCardTxnResultInfo = value; }
        }

        //Axis bug no.#96
        public string Other
        {
            get { return m_Others; }
            set { m_Others = value; }
        }
        //End Axis bug no.#96

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        [XmlArray("SalesReceiptLineAddREM")]
        public Collection<SalesReceiptLineAdd> SalesReceiptLineAdd
        {
            get { return m_SalesReceiptLineAdd; }
            set { m_SalesReceiptLineAdd = value; }
        }


        [XmlIgnoreAttribute()]
        public string IncludeRetElement
        {
            get { return m_IncludeRetElement; }
            set { m_IncludeRetElement = value; }
        }
        [XmlIgnoreAttribute()]
        public DateTime SalesReceiptDate
        {
            get { return m_SalesReceiptDate; }
            set { m_SalesReceiptDate = value; }
        }

        // Axis 10.0

        [XmlArray("DiscountLineAddREM")]
        public Collection<DiscountLineAdd> DiscountLineAdd
        {
            get { return m_DiscountLineAdd; }

            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    m_DiscountLineAdd = value;
                }
            }
        }

        [XmlArray("ShippingLineAddREM")]
        public Collection<ShippingLineAdd> ShippingLineAdd
        {
            get { return m_ShippingLineAdd; }

            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    m_ShippingLineAdd = value;
                }
            }
        }

        [XmlArray("SalesReceiptLineGroupAddREM")]
        public Collection<SalesReceiptLineGroupAdd> SalesReceiptLineGroupAdd
        {
            get { return m_SalesReceiptLineGroupAdd;  }
            set { m_SalesReceiptLineGroupAdd = value; }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.SalesReceiptQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesReceiptEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptAddRq = requestXmlDoc.CreateElement("SalesReceiptAddRq");
            inner.AppendChild(SalesReceiptAddRq);

            //Create SalesReceiptEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptAdd = requestXmlDoc.CreateElement("SalesReceiptAdd");

            SalesReceiptAddRq.AppendChild(SalesReceiptAdd);

            requestXML = requestXML.Replace("<SalesReceiptLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAdd />", string.Empty);

            //Axis 10.2(Bug No -92)
            requestXML = requestXML.Replace("<SalesReceiptLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineGroupAddREM>", string.Empty);
            //End Changes 

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            
                requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
                requestXML = requestXML.Replace("<DiscountLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("</DiscountLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);
                requestXML = requestXML.Replace("<ShippingLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("</ShippingLineAddREM>", string.Empty);

                requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
                requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
                requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
                requestXML = requestXML.Replace("<DataExt />", string.Empty);
            
            SalesReceiptAdd.InnerXml = requestXML;

            //For add request id to track error message
            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/CurrencyRef");
                    SalesReceiptAdd.RemoveChild(childNode);
                }
            }

            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/Phone");
                    SalesReceiptAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/Fax");
                    SalesReceiptAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/Email");
                    SalesReceiptAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/InventorySiteLocationRef");
                    SalesReceiptAdd.ParentNode.RemoveChild(node);
                    SalesReceiptAdd.RemoveAll();
                }
            }


            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesReceiptAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                SalesReceiptAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());



            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesReceiptAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesReceiptAddRs/SalesReceiptRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesReceipt(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Sales Reciept ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Sales Reciept Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            try
            {
                //Add the prolog processing instructions
                requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
                requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

                //Create the outer request envelope tag
                XmlElement outer = requestXmlDoc.CreateElement("QBXML");
                requestXmlDoc.AppendChild(outer);

                //Create the inner request envelope & any needed attributes
                XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
                outer.AppendChild(inner);
                inner.SetAttribute("onError", "stopOnError");

                //Create SalesReceiptQueryRq aggregate and fill in field values for it
                XmlElement SalesReceiptQueryRq = requestXmlDoc.CreateElement("SalesReceiptQueryRq");
                inner.AppendChild(SalesReceiptQueryRq);

                //Create Refno aggregate and fill in field values for it.
                XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
                RefNumber.InnerText = RefNo;
                SalesReceiptQueryRq.AppendChild(RefNumber);

                //Create IncludeRetElement for fast execution.
                XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
                IncludeRetElement.InnerText = "TxnID";
                SalesReceiptQueryRq.AppendChild(IncludeRetElement);
            }
            catch (Exception) { }

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating SalesReceipt information
        /// of existing SalesReceipt with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateSalesReceiptInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.SalesReceiptQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptQBEntryModRq = requestXmlDoc.CreateElement("SalesReceiptModRq");
            inner.AppendChild(SalesReceiptQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptMod = requestXmlDoc.CreateElement("SalesReceiptMod");
            SalesReceiptQBEntryModRq.AppendChild(SalesReceiptMod);

            requestXML = requestXML.Replace("<SalesReceiptPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAdd>", "<SalesReceiptLineMod>");
            requestXML = requestXML.Replace("</SalesReceiptLineAdd>", "</SalesReceiptLineMod>");

            requestXML = requestXML.Replace("<SalesReceiptLineAdd />", string.Empty);

            //Axis 10.2(Bug No -92)
            requestXML = requestXML.Replace("<SalesReceiptLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineGroupAddREM>", string.Empty);
            //End Changes

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            //Axis 10.2 bug no 70
            requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);

            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            SalesReceiptMod.InnerXml = requestXML;

            //For add request id to track error message
            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/CurrencyRef");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }

            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/Phone");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/Fax");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/Email");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/InventorySiteLocationRef");
                    SalesReceiptMod.ParentNode.RemoveChild(node);
                    SalesReceiptMod.RemoveAll();
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesReceiptQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By SalesReceiptRefNumber) : " + rowcount.ToString());
            else
                SalesReceiptQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesReceiptModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesReceiptModRs/SalesReceiptRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesReceipt(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

//Bug No.412
        /// <summary>
        /// Append data to perticular listid
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendSalesReceiptInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.SalesReceiptQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptQBEntryModRq = requestXmlDoc.CreateElement("SalesReceiptModRq");
            inner.AppendChild(SalesReceiptQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement SalesReceiptMod = requestXmlDoc.CreateElement("SalesReceiptMod");
            SalesReceiptQBEntryModRq.AppendChild(SalesReceiptMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
            //Axis 706
            List<string> GroupTxnLineID = new List<string>();
            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                if (!txnLineIDList[i].Contains("GroupTxnLineID_"))
                {
                    addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></SalesReceiptLineMod><SalesReceiptLineMod>";


                    XmlNode SalesReceiptLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod");
                    System.Xml.XmlElement SalesReceiptLineGroupMod = requestXmlDoc.CreateElement("SalesReceiptLineMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = txnLineIDList[i];
                    SalesReceiptLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertAfter(SalesReceiptLineGroupMod, SalesReceiptLineMod.ChildNodes[i == 0 ? i : i - 1]);
                }
                else
                {
                    string GroupTxnID = txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty);
                    GroupTxnLineID.Add(txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty));
                    XmlNode SalesReceiptMod1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod");
                    //XmlNode SalesReceiptLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/SalesReceiptLineMod");
                    System.Xml.XmlElement SalesReceiptLineGroupMod = requestXmlDoc.CreateElement("SalesReceiptLineGroupMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = GroupTxnID;
                    SalesReceiptLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertAfter(SalesReceiptLineGroupMod, SalesReceiptMod1.ChildNodes[i == 0 ? i : i - 1]);
                }
            }

            XmlNode SalesReceiptMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod");

            System.Xml.XmlElement SalesReceiptAdd = requestXmlDoc.CreateElement("SalesReceiptLine");
            //Axis 706 ends
            requestXML = requestXML.Replace("<SalesReceiptPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineAdd>", "<SalesReceiptLineMod>");
            requestXML = requestXML.Replace("</SalesReceiptLineAdd>", "</SalesReceiptLineMod>");

            requestXML = requestXML.Replace("<SalesReceiptLineAdd />", string.Empty);

            //Axis 10.2(Bug No -92)
            requestXML = requestXML.Replace("<SalesReceiptLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesReceiptLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesReceiptLineGroupAddREM>", string.Empty);
            //End Changes

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            //Axis 10.2 bug no 70
            requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);

           // requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            //Axis 706
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");

            string[] SalesReceiptLineModFragment;

            int indexSalesReceiptLine = requestXML.IndexOf(@"<SalesReceiptLineMod>");
            int indexSalesReceiptLineGroup = requestXML.IndexOf(@"<SalesReceiptLineGroupMod>");

            if (indexSalesReceiptLineGroup <= 0 || indexSalesReceiptLine < indexSalesReceiptLineGroup)
            {
                SalesReceiptLineModFragment = requestXML.Split(new string[] { "<SalesReceiptLineMod>" }, StringSplitOptions.None);
            }
            else
            {
                SalesReceiptLineModFragment = requestXML.Split(new string[] { "<SalesReceiptLineGroupMod>" }, StringSplitOptions.None);
            }

            if (SalesReceiptLineModFragment.Length > 0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = SalesReceiptLineModFragment[0];


                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").FirstChild;


                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertBefore(xfrag1, firstChild1);

                requestXML = requestXML.Replace(SalesReceiptLineModFragment[0], string.Empty);

                //  requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertAfter(xfrag1, SalesReceiptMod2.LastChild);

            }



            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertAfter(xfrag, SalesReceiptMod2.LastChild);
            //Axis 706 ends

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/Phone");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/Fax");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod/Email");
                    SalesReceiptMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd/InventorySiteLocationRef");
                    SalesReceiptMod.ParentNode.RemoveChild(node);
                    SalesReceiptMod.RemoveAll();
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/SalesReceiptAddRq/SalesReceiptAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                SalesReceiptQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By SalesReceiptRefNumber) : " + rowcount.ToString());
            else
                SalesReceiptQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/SalesReceiptModRq/SalesReceiptMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesReceiptModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/SalesReceiptModRs/SalesReceiptRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofSalesReceipt(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
    }


    public class SalesReceiptQBEntryCollection : Collection<SalesReceiptQBEntry>
    {
        /// <summary>
        /// This method is used for getting existing sales receipt date, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public SalesReceiptQBEntry FindSalesReceiptEntry(DateTime date)
        {
            foreach (SalesReceiptQBEntry item in this)
            {
                if (item.SalesReceiptDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        public static int salesreceipt_cnt = 0;
        public static string salesreceipt_number = "";
        /// <summary>
        ///   /// This method is used for getting existing sales receipt refno, If not exists then it return null.
        /// </summary>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public SalesReceiptQBEntry FindSalesReceiptEntry(string refNumber)
        {
            salesreceipt_number = refNumber;
            foreach (SalesReceiptQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    salesreceipt_cnt++;
                    return item;
                }
            }
            salesreceipt_cnt = 0;
            return null;
        }
        /// <summary>
        ///   /// This method is used for getting existing sales receipt date and refno, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public SalesReceiptQBEntry FindSalesReceiptEntry(DateTime date, string refNumber)
        {
            foreach (SalesReceiptQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.SalesReceiptDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }


    /// <summary>
    /// Adding new line for sales receipt
    /// </summary>
    [XmlRootAttribute("SalesReceiptLineAdd", Namespace = "", IsNullable = false)]
    public class SalesReceiptLineAdd
    {
        #region Private Member Variables

        private ItemRef m_ItemRef;
        private string m_Desc;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Rate;
        private string m_RatePercent;
        //601
        private PriceLevelRef m_PriceLevelFullName;
        private ClassRef m_ClassRef;
        private string m_Amount;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        //P Axis 13.1 : issue 659
        private string m_SerialNumber;
        private string m_LotNumber;

        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_istaxable;
        //private DateTime m_ServiceDate;
        private OverrideItemAccountRef m_OverrideAccountRef;
        private string m_Other1;
        private string m_Other2;
        //private CreditCardTxnInputInfo m_CreditCardTxnInputInfo;
        //private CreditCardTxnResultInfo m_CreditCardTxnResultInfo;
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();
    
        #endregion

        #region  Constructors

        public SalesReceiptLineAdd()
        {

        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
                this.m_RatePercent = null;
            }
        }

        public string RatePercent
        {
            get { return this.m_RatePercent; }
            set
            {
                this.m_RatePercent = value;
                this.m_Rate = null;
            }
        }

        //601
        public PriceLevelRef PriceLevelRef
        {
            get { return this.m_PriceLevelFullName; }
            set { this.m_PriceLevelFullName = value; }
        }

        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }

        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set
            {
                this.m_InventorySiteLocationRef = value;
            }
        }

        //P Axis 13.1 : issue 659
        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { this.m_SerialNumber = value; }
        }
        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }
        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }
      
        public string IsTaxable
        {
            get { return this.m_istaxable; }
            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    this.m_istaxable = value;
                }
            }
        }
       

        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideAccountRef; }
            set { this.m_OverrideAccountRef = value; }
        }

        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        public string Other2
        {
            get { return this.m_Other2; }
            set { this.m_Other2 = value; }
        }       

        [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }

        #endregion
    }

    /// <summary>
    /// Adding new line for sales receipt group
    /// </summary>
    [XmlRootAttribute("SalesReceiptLineGroupAdd", Namespace = "", IsNullable = false)]
    public class SalesReceiptLineGroupAdd
    {
        private ItemRef m_ItemGroupRef;
        private string m_ItemGroupQuantity;
        private string m_ItemGroupUOM;
        private InventorySiteRef m_ItemGroupInventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        //P Axis 13.1 : issue 659
        private string m_ItemGroupSerialNumber;
        private string m_ItemGroupLotNumber;

        //Bug no. 331       
       // private Collection<DataExt> m_ItemGroupDataExt = new Collection<DataExt>();
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();
        //end Bug no. 331

        public ItemRef ItemGroupRef
        {
            get { return this.m_ItemGroupRef; }
            set { this.m_ItemGroupRef = value; }
        }

        public string Quantity
        {
            get { return this.m_ItemGroupQuantity; }
            set { this.m_ItemGroupQuantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_ItemGroupUOM; }
            set { this.m_ItemGroupUOM = value; }
        }
        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_ItemGroupInventorySiteRef; }
            set { this.m_ItemGroupInventorySiteRef = value; }
        }
        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }

        //P Axis 13.1 : issue 659
        public string SerialNumber
        {
            get { return this.m_ItemGroupSerialNumber; }
            set { this.m_ItemGroupSerialNumber = value; }
        }
        public string LotNumber
        {
            get { return this.m_ItemGroupLotNumber; }
            set { this.m_ItemGroupLotNumber = value; }
        }

        //bug no. 331
        [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }
        //end bug no. 331
    }
}
