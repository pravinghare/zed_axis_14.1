using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemSalesTaxGroupClass
    {
        private static ImportItemSalesTaxGroupClass m_ImportItemSalesTaxGroupClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemSalesTaxGroupClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemSalesTaxGroup class
        /// </summary>
        /// <returns></returns>
        public static ImportItemSalesTaxGroupClass GetInstance()
        {
            if (m_ImportItemSalesTaxGroupClass == null)
                m_ImportItemSalesTaxGroupClass = new ImportItemSalesTaxGroupClass();
            return m_ImportItemSalesTaxGroupClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemSalesTaxGroup and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemSalesTaxGroup QuickBooks collection </returns>
        public DataProcessingBlocks.ItemSalesTaxGroupQBEntryCollection ImportItemSalesTaxGroupData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemSalesTaxGroup Entry collections.
            DataProcessingBlocks.ItemSalesTaxGroupQBEntryCollection coll = new ItemSalesTaxGroupQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemSalesTaxGroup Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemSalesTaxGroupDt = new DateTime();
                    string datevalue = string.Empty;

                    //ItemSalesTaxGroup Validation
                    DataProcessingBlocks.ItemSalesTaxGroupQBEntry ItemSalesTaxGroup = new ItemSalesTaxGroupQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name

                        ItemSalesTaxGroup = coll.FindItemSalesTaxGroupEntry(dr["Name"].ToString());
                        if (ItemSalesTaxGroup == null)
                        {
                            ItemSalesTaxGroup = new ItemSalesTaxGroupQBEntry();
                            if (dr["Name"].ToString() != string.Empty)
                            {
                                if (dr["Name"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemSalesTaxGroup.Name = dr["Name"].ToString().Substring(0, 31);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemSalesTaxGroup.Name = dr["Name"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemSalesTaxGroup.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemSalesTaxGroup.Name = dr["Name"].ToString();
                                }
                            }
                            if (dt.Columns.Contains("ItemSalesTaxFullName"))
                            {
                                if (ItemSalesTaxGroup.ItemSalesTaxRef == null)
                                    ItemSalesTaxGroup.ItemSalesTaxRef = new List<ItemSalesTaxRef>();
                                #region Validations of ItemSalesTax FullName
                                if (dr["ItemSalesTaxFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemSalesTaxFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemSalesTax FullName (" + dr["ItemSalesTaxFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                                if (ItemSalesTaxRef.FullName == null)
                                                    ItemSalesTaxRef.FullName = null;
                                                else
                                                    ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString().Substring(0, 31));
                                                ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                                if (ItemSalesTaxRef.FullName == null)
                                                    ItemSalesTaxRef.FullName = null;
                                                ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);

                                            }
                                        }
                                        else
                                        {
                                            ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                            if (ItemSalesTaxRef.FullName == null)
                                                ItemSalesTaxRef.FullName = null;
                                            ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);
                                        }
                                    }
                                    else
                                    {
                                        ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                        if (ItemSalesTaxRef.FullName == null)
                                            ItemSalesTaxRef.FullName = null;
                                        ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsActive"))
                            {
                                #region Validations of IsActive
                                if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsActive"].ToString(), out result))
                                    {
                                        ItemSalesTaxGroup.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsActive"].ToString().ToLower() == "true")
                                        {
                                            ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsActive"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemDesc"))
                            {
                                #region Validations of ItemDesc
                                if (dr["ItemDesc"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDesc (" + dr["ItemDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString();
                                    }
                                }
                                #endregion
                            }
                            coll.Add(ItemSalesTaxGroup);

                        }
                        else
                        {
                            if (dt.Columns.Contains("ItemSalesTaxFullName"))
                            {
                                if (ItemSalesTaxGroup.ItemSalesTaxRef == null)
                                    ItemSalesTaxGroup.ItemSalesTaxRef = new List<ItemSalesTaxRef>();
                                #region Validations of ItemSalesTax FullName
                                if (dr["ItemSalesTaxFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemSalesTaxFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemSalesTax FullName (" + dr["ItemSalesTaxFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                                if (ItemSalesTaxRef.FullName == null)
                                                    ItemSalesTaxRef.FullName = null;
                                                else
                                                     ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString().Substring(0, 31));
                                                ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                                if (ItemSalesTaxRef.FullName == null)
                                                    ItemSalesTaxRef.FullName = null;
                                                ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);

                                            }
                                        }
                                        else
                                        {
                                            ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                            if (ItemSalesTaxRef.FullName == null)
                                                ItemSalesTaxRef.FullName = null;
                                                ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);
                                        }
                                    }
                                    else
                                    {
                                        ItemSalesTaxRef ItemSalesTaxRef = new ItemSalesTaxRef(string.Empty, dr["ItemSalesTaxFullName"].ToString());
                                        if (ItemSalesTaxRef.FullName == null)
                                            ItemSalesTaxRef.FullName = null;
                                                ItemSalesTaxGroup.ItemSalesTaxRef.Add(ItemSalesTaxRef);
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsActive"))
                            {
                                #region Validations of IsActive
                                if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsActive"].ToString(), out result))
                                    {
                                        ItemSalesTaxGroup.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsActive"].ToString().ToLower() == "true")
                                        {
                                            ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsActive"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemSalesTaxGroup.IsActive = dr["IsActive"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemDesc"))
                            {
                                #region Validations of ItemDesc
                                if (dr["ItemDesc"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDesc (" + dr["ItemDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemSalesTaxGroup.ItemDesc = dr["ItemDesc"].ToString();
                                    }
                                }
                                #endregion
                            }

                        }
                        #endregion
                    }

                   



                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }
       
            #endregion

            return coll;
        }

    }
}
