﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using System.Linq;
using Xero.NetStandard.OAuth2.Model;
using DataProcessingBlocks.XeroConnection;
using Xero.NetStandard.OAuth2.Api;
using System.Threading.Tasks;

namespace DataProcessingBlocks
{
    class ImportXeroBankClass
    {
        private static ImportXeroBankClass m_ImportXeroBankClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportXeroBankClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportXeroBankClass GetInstance()
        {
            if (m_ImportXeroBankClass == null)
                m_ImportXeroBankClass = new ImportXeroBankClass();
            return m_ImportXeroBankClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Journal QuickBooks collection </returns>

        public async Task<BankTransactions> ImportBankXeroDataAsync(DataTable dt, string logDirectory)
        {
            //Create an instance of Customer Entry collections.
            // DataProcessingBlocks.XeroBankEntryCollection coll = new XeroBankEntryCollection();
            BankTransactions banknode = new BankTransactions();
            BankTransaction bank = new BankTransaction();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            DateTime BanklDt = new DateTime();
            string datevalue = string.Empty;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            // DataProcessingBlocks.XeroBankEntry bank = new XeroBankEntry();
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.LongDatePattern = "yyyy-MM-dd'T'HH':'mm':'ss'";

            #region For Bank Entry

            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }


                        string bankref = string.Empty;

                        if (dt.Columns.Contains("Reference"))
                        {
                            #region Validations of Reference
                            if (dr["Reference"].ToString() != string.Empty)
                            {
                                if (dr["Reference"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Reference ( " + dr["Reference"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bankref = dr["Reference"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bankref = dr["Reference"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bankref = dr["Reference"].ToString();
                                    }
                                }
                                else
                                {
                                    bankref = dr["Reference"].ToString();
                                }
                            }

                            if (bank.Reference != null && bank.Reference.Equals(bankref))
                            {
                                bank.Reference = bankref;
                            }
                            else
                            {
                                bank = new BankTransaction();
                                bank.Reference = bankref;
                                bank.LineItems = new List<LineItem>();
                                bank.BankAccount = new Account();

                                if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.BankSpend)
                                {
                                    bank.Type = BankTransaction.TypeEnum.SPEND;
                                }
                                else
                                {
                                    bank.Type = BankTransaction.TypeEnum.RECEIVE;
                                }

                                if (banknode._BankTransactions == null)
                                {
                                    banknode._BankTransactions = new List<BankTransaction>();
                                }
                                banknode._BankTransactions.Add(bank);
                            }

                            #endregion
                        }

                        Contact con = new Contact();

                        if (dt.Columns.Contains("ContactName"))
                        {
                            #region Validations of ContactName
                            if (dr["ContactName"].ToString() != string.Empty)
                            {
                                if (dr["ContactName"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ContactName ( " + dr["ContactName"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            con.Name = dr["ContactName"].ToString().Substring(0, 100);
                                            bank.Contact = con;

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            con.Name = dr["ContactName"].ToString();
                                            bank.Contact = con;
                                        }
                                    }
                                    else
                                    {
                                        con.Name = dr["ContactName"].ToString();
                                        bank.Contact = con;
                                    }
                                }
                                else
                                {
                                    con.Name = dr["ContactName"].ToString();
                                    bank.Contact = con;
                                }
                            }
                            #endregion
                        }

                        LineItem lineitem = new LineItem();

                        if (dt.Columns.Contains("LineItemsDescription"))
                        {
                            #region Validations of LineItemsQuantity
                            if (dr["LineItemsDescription"].ToString() != string.Empty)
                            {
                                if (dr["LineItemsDescription"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemsDescription ( " + dr["LineItemsDescription"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            lineitem.Description = dr["LineItemsDescription"].ToString().Substring(0, 25);
                                            //bank.LineItems = lineitem;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            lineitem.Description = dr["LineItemsDescription"].ToString();
                                            //bank.LineItems = lineitem;
                                        }
                                    }
                                    else
                                    {
                                        lineitem.Description = dr["LineItemsDescription"].ToString();
                                        // bank.LineItems = lineitem;
                                    }
                                }
                                else
                                {
                                    lineitem.Description = dr["LineItemsDescription"].ToString();
                                    //bank.LineItems = lineitem;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LineItemsQuantity"))
                        {
                            #region Validations of LineItemsQuantity
                            double quantity = 0;
                            if (dr["LineItemsQuantity"].ToString() != string.Empty)
                            {
                                if (dr["LineItemsQuantity"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemsQuantity ( " + dr["LineItemsQuantity"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            lineitem.Quantity = Convert.ToDouble(dr["LineItemsQuantity"].ToString().Substring(0, 25));
                                            // bank.LineItems = lineitem;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            quantity = Convert.ToDouble(dr["LineItemsQuantity"].ToString());
                                            lineitem.Quantity = (double?)quantity;
                                            //bank.LineItems = lineitem;
                                        }
                                    }
                                    else
                                    {
                                        quantity = Convert.ToDouble(dr["LineItemsQuantity"].ToString());
                                        lineitem.Quantity = (double?)quantity;
                                        // bank.LineItems = lineitem;
                                    }
                                }
                                else
                                {
                                    quantity = Convert.ToDouble(dr["LineItemsQuantity"].ToString());
                                    lineitem.Quantity = (double?)quantity;
                                    //  bank.LineItems = lineitem;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemsUnitAmount"))
                        {
                            #region Validations of LineItemsUnitAmount
                            double unitamount = 0;
                            if (dr["LineItemsUnitAmount"].ToString() != string.Empty)
                            {
                                if (dr["LineItemsUnitAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemsUnitAmount ( " + dr["LineItemsUnitAmount"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            unitamount = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString().Substring(0, 25));
                                            lineitem.UnitAmount = (double?)unitamount;
                                            //bank.LineItems = lineitem;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            unitamount = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString());
                                            lineitem.UnitAmount = (double?)unitamount;
                                            //bank.LineItems = lineitem;
                                        }
                                    }
                                    else
                                    {
                                        unitamount = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString());
                                        lineitem.UnitAmount = (double?)unitamount;
                                        //bank.LineItems = lineitem;
                                    }
                                }
                                else
                                {
                                    unitamount = Convert.ToDouble(dr["LineItemsUnitAmount"].ToString());
                                    lineitem.UnitAmount = (double?)unitamount;
                                    // bank.LineItems = lineitem;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemsAccountCode"))
                        {
                            #region Validations of LineItemsAccountCode

                            if (dr["LineItemsAccountCode"].ToString() != string.Empty)
                            {
                                if (dr["LineItemsAccountCode"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemsAccountCode ( " + dr["LineItemsAccountCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            lineitem.AccountCode = dr["LineItemsAccountCode"].ToString().Substring(0, 25);

                                            //bank.LineItems = lineitem;


                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            lineitem.AccountCode = dr["LineItemsAccountCode"].ToString();
                                            //bank.LineItems = lineitem;
                                        }
                                    }
                                    else
                                    {
                                        lineitem.AccountCode = dr["LineItemsAccountCode"].ToString();
                                        //bank.LineItems = lineitem;
                                    }
                                }
                                else
                                {
                                    lineitem.AccountCode = dr["LineItemsAccountCode"].ToString();
                                    // bank.LineItems = lineitem;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemsTaxType"))
                        {
                            #region Validations of LineItemsTaxType
                            if (dr["LineItemsTaxType"].ToString() != string.Empty)
                            {
                                if (dr["LineItemsTaxType"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemsTaxType ( " + dr["LineItemsTaxType"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            lineitem.TaxType = dr["LineItemsTaxType"].ToString().Substring(0, 25);

                                            //bank.LineItems = lineitem;


                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            lineitem.TaxType = dr["LineItemsTaxType"].ToString();
                                            // bank.LineItems = lineitem;
                                        }
                                    }
                                    else
                                    {
                                        lineitem.TaxType = dr["LineItemsTaxType"].ToString();
                                        //bank.LineItems = lineitem;
                                    }
                                }
                                else
                                {
                                    lineitem.TaxType = dr["LineItemsTaxType"].ToString();
                                    // bank.LineItems = lineitem;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("LineItemsLineAmount"))
                        {
                            #region Validations of LineItemsLineAmount
                            decimal lineamt = 0;
                            if (dr["LineItemsLineAmount"].ToString() != string.Empty)
                            {
                                if (dr["LineItemsLineAmount"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineItemsLineAmount ( " + dr["LineItemsLineAmount"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString().Substring(0, 25));
                                            lineitem.LineAmount = (double?)lineamt;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString());
                                            lineitem.LineAmount = (double?)lineamt;
                                        }
                                    }
                                    else
                                    {
                                        lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString());
                                        lineitem.LineAmount = (double?)lineamt;
                                    }
                                }
                                else
                                {
                                    lineamt = Convert.ToDecimal(dr["LineItemsLineAmount"].ToString());
                                    lineitem.LineAmount = (double?)lineamt;
                                }
                            }
                            #endregion
                        }

                        //Axis 141
                        if (dt.Columns.Contains("TrackingName1"))
                        {
                            #region Validations of TrackingName1
                            Guid trackingCategoryGuid1 = new Guid();
                            string trackingCategoryName1 = "";
                            string optionName1 = "";

                            if (dr["TrackingName1"].ToString() != string.Empty)
                            {
                                if (dr["TrackingName1"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TrackingName1 ( " + dr["TrackingName1"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            trackingCategoryName1 = dr["TrackingName1"].ToString();
                                            if (dr["TrackingOption1"].ToString() != string.Empty)
                                            {
                                                optionName1 = dr["TrackingOption1"].ToString();
                                            }
                                            lineitem.Tracking.Add(new LineItemTracking()
                                            {
                                                TrackingCategoryID = trackingCategoryGuid1,
                                                Name = trackingCategoryName1,
                                                Option = optionName1
                                            });
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            trackingCategoryName1 = dr["TrackingName1"].ToString();
                                            if (dr["TrackingOption1"].ToString() != string.Empty)
                                            {
                                                optionName1 = dr["TrackingOption1"].ToString();
                                            }
                                            lineitem.Tracking.Add(new LineItemTracking()
                                            {
                                                TrackingCategoryID = trackingCategoryGuid1,
                                                Name = trackingCategoryName1,
                                                Option = optionName1
                                            });
                                        }
                                    }
                                    else
                                    {
                                        trackingCategoryName1 = dr["TrackingName1"].ToString();
                                        if (dr["TrackingOption1"].ToString() != string.Empty)
                                        {
                                            optionName1 = dr["TrackingOption1"].ToString();
                                        }
                                        lineitem.Tracking.Add(new LineItemTracking()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid1,
                                            Name = trackingCategoryName1,
                                            Option = optionName1
                                        });
                                    }
                                }
                                else
                                {
                                    trackingCategoryName1 = dr["TrackingName1"].ToString();
                                    if (dr["TrackingOption1"].ToString() != string.Empty)
                                    {
                                        optionName1 = dr["TrackingOption1"].ToString();
                                    }
                                    lineitem.Tracking.Add(new LineItemTracking()
                                    {
                                        TrackingCategoryID = trackingCategoryGuid1,
                                        Name = trackingCategoryName1,
                                        Option = optionName1
                                    });
                                }
                            }


                            #endregion
                        }

                        if (dt.Columns.Contains("TrackingName2"))
                        {
                            #region Validations of TrackingName2
                            Guid trackingCategoryGuid2 = new Guid();
                            string trackingCategoryName2 = "";
                            string optionName2 = "";

                            if (dr["TrackingName2"].ToString() != string.Empty)
                            {
                                if (dr["TrackingName2"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TrackingName2 ( " + dr["TrackingName2"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            trackingCategoryName2 = dr["TrackingName2"].ToString();
                                            if (dr["TrackingOption2"].ToString() != string.Empty)
                                            {
                                                optionName2 = dr["TrackingOption2"].ToString();
                                            }
                                            lineitem.Tracking.Add(new LineItemTracking()
                                            {
                                                TrackingCategoryID = trackingCategoryGuid2,
                                                Name = trackingCategoryName2,
                                                Option = optionName2
                                            });
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            trackingCategoryName2 = dr["TrackingName2"].ToString();
                                            if (dr["TrackingOption2"].ToString() != string.Empty)
                                            {
                                                optionName2 = dr["TrackingOption2"].ToString();
                                            }
                                            lineitem.Tracking.Add(new LineItemTracking()
                                            {
                                                TrackingCategoryID = trackingCategoryGuid2,
                                                Name = trackingCategoryName2,
                                                Option = optionName2
                                            });
                                        }
                                    }
                                    else
                                    {
                                        trackingCategoryName2 = dr["TrackingName2"].ToString();
                                        if (dr["TrackingOption2"].ToString() != string.Empty)
                                        {
                                            optionName2 = dr["TrackingOption2"].ToString();
                                        }
                                        lineitem.Tracking.Add(new LineItemTracking()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid2,
                                            Name = trackingCategoryName2,
                                            Option = optionName2
                                        });
                                    }
                                }
                                else
                                {
                                    trackingCategoryName2 = dr["TrackingName2"].ToString();
                                    if (dr["TrackingOption2"].ToString() != string.Empty)
                                    {
                                        optionName2 = dr["TrackingOption2"].ToString();
                                    }
                                    lineitem.Tracking.Add(new LineItemTracking()
                                    {
                                        TrackingCategoryID = trackingCategoryGuid2,
                                        Name = trackingCategoryName2,
                                        Option = optionName2
                                    });
                                }
                            }


                            #endregion
                        }

                        if (bank.LineItems == null)
                        {
                            bank.LineItems = new List<LineItem>();
                        }
                        //Axis 141 ends
                        bank.LineItems.Add(lineitem);


                        if (dt.Columns.Contains("BankAccountCode"))
                        {
                            #region Validations of BankAccountCode
                            if (dr["BankAccountCode"].ToString() != string.Empty)
                            {
                                if (dr["BankAccountCode"].ToString().Length > 500)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BankAccountCode ( " + dr["BankAccountCode"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.BankAccount.Code = dr["BankAccountCode"].ToString().Substring(0, 100);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.BankAccount.Code = dr["BankAccountCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bank.BankAccount.Code = dr["BankAccountCode"].ToString();
                                    }
                                }
                                else
                                {
                                    //Axis 141
                                    if (bank.BankAccount == null)
                                    {
                                        bank.BankAccount = new Account();
                                    }
                                    bank.BankAccount.Code = dr["BankAccountCode"].ToString();
                                }
                            }
                            #endregion
                        }

                        XeroConnectionInfo xeroConnectionInfo = new XeroConnectionInfo();
                        var CheckConnectionInfo = await xeroConnectionInfo.GetXeroConnectionDetailsFromXml();
                        var accountingApi = new AccountingApi();
                        var bankTransactionList = accountingApi.GetAccountsAsync(CheckConnectionInfo.AccessToken, CheckConnectionInfo.TenantID);
                        bankTransactionList.Wait();

                        var bankaccountname = bankTransactionList.Result._Accounts.Where(account => account.BankAccountType == Account.BankAccountTypeEnum.BANK);

                        string bankaccountnamebyuser = bank.BankAccount.Code;
                        foreach (Account acc in bankaccountname)
                        {
                            if (bankaccountnamebyuser.Equals(acc.Name))
                            {
                                bank.BankAccount.AccountID = acc.AccountID;
                            }
                        }

                        if (dt.Columns.Contains("Date"))
                        {
                            #region Validation of Date
                            if (dr["Date"].ToString() != "<None>" || dr["Date"].ToString() != string.Empty)
                            {

                                datevalue = dr["Date"].ToString();
                                datevalue = datevalue.Trim();
                                if (datevalue.Contains(" "))//time present
                                {
                                    int pos = datevalue.IndexOf(" ");
                                    datevalue = datevalue.Substring(0, pos);
                                }
                                DateTime temp_date = new DateTime();
                                bool flg1 = false;
                                string[] date_formats = { "d-M-yyyy", "d/M/yyyy", "d-MM-yyyy", "d/MM/yyyy", "dd-M-yyyy", "dd/M/yyyy", "dd-MM-yyyy", "dd/MM/yyyy",  //for date-month-year
                                                      "M-d-yyyy","M/d/yyyy", "MM-d-yyyy","MM/d/yyyy", "M-dd-yyyy", "M/dd/yyyy", "MM-dd-yyyy", "MM/dd/yyyy",    //for month-date-year
                                                      "yyyy-M-d", "yyyy/M/d", "yyyy-MM-d", "yyyy/MM/d", "yyyy-M-dd", "yyyy/M/dd", "yyyy-MM-dd", "yyyy/MM/dd"   //for year-month-date
                                                    };

                                for (int i = 0; i < date_formats.Length; i++)
                                {
                                    flg1 = DateTime.TryParseExact(datevalue, date_formats[i], CultureInfo.InvariantCulture, DateTimeStyles.None, out temp_date);
                                    if (flg1 == true)
                                    {
                                        break;
                                    }
                                }
                                if (flg1 == true)
                                {
                                    BanklDt = new DateTime(temp_date.Year, temp_date.Month, temp_date.Day);
                                    bank.Date = BanklDt;
                                }
                                else
                                {
                                    DateTime dttest = DateTime.Today;
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Date (" + datevalue + ") is not valid for this mapping.Please Enter Date in format of (yyyy-MM-dd'T'HH':'mm':'ss).If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.Date = dttest;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.Date = dttest;
                                        }
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("LineAmountTypes"))
                        {
                            string valid_line = dr["LineAmountTypes"].ToString().ToLower().Trim();

                            #region valdation for LineAmountTypes
                            if (valid_line != string.Empty)
                            {
                                if (valid_line.Equals("exclusive"))
                                {
                                    bank.LineAmountTypes = LineAmountTypes.Exclusive;
                                }
                                else if (valid_line.Equals("inclusive"))
                                {
                                    bank.LineAmountTypes = LineAmountTypes.Inclusive;
                                }
                                else if (valid_line.Equals("notax"))
                                {
                                    bank.LineAmountTypes = LineAmountTypes.NoTax;
                                }
                                else
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LineAmountTypes value ( " + dr["LineAmountTypes"].ToString() + " )  is invalid .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.LineAmountTypes = LineAmountTypes.Exclusive;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.LineAmountTypes = LineAmountTypes.Exclusive;
                                        }
                                    }
                                    else
                                    {
                                        bank.LineAmountTypes = LineAmountTypes.Exclusive;
                                    }

                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("CurrencyRate"))
                        {
                            #region Validations of CurrencyRate
                            if (dr["CurrencyRate"].ToString() != string.Empty)
                            {
                                if (dr["CurrencyRate"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CurrencyRate ( " + dr["CurrencyRate"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.CurrencyRate = Convert.ToDouble(dr["CurrencyRate"].ToString().Substring(0, 25));

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.CurrencyRate = Convert.ToDouble(dr["CurrencyRate"].ToString());

                                        }
                                    }
                                    else
                                    {
                                        bank.CurrencyRate = Convert.ToDouble(dr["CurrencyRate"].ToString());

                                    }
                                }
                                else
                                {
                                    bank.CurrencyRate = Convert.ToDouble(dr["CurrencyRate"].ToString());

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Url"))
                        {
                            #region Validations of Url
                            if (dr["Url"].ToString() != string.Empty)
                            {
                                if (dr["Url"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Url ( " + dr["Url"].ToString() + " ) is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.Url = dr["Url"].ToString().Substring(0, 25);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.Url = dr["Url"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        bank.Url = dr["Url"].ToString();
                                    }
                                }
                                else
                                {
                                    bank.Url = dr["Url"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Status"))
                        {
                            #region Validations of Status
                            if (dr["Status"].ToString() != string.Empty && dr["Status"].ToString() != null)
                            {
                                string bankStatus = string.Empty;
                                if (dr["Status"].ToString().Length > 25)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Status (" + dr["Status"].ToString() + ") is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string status = dr["Status"].ToString().Substring(0, 25);
                                            bankStatus = status.ToUpper();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string status = dr["Status"].ToString();
                                            bankStatus = status.ToUpper();
                                        }
                                    }
                                    else
                                    {
                                        string status = dr["Status"].ToString();
                                        bankStatus = status.ToUpper();
                                    }
                                }
                                else
                                {
                                    string status = dr["Status"].ToString();
                                    bankStatus = status.ToUpper();
                                }

                                bool checkFlag = Enum.IsDefined(typeof(BankTransaction.StatusEnum), bankStatus);
                                if (checkFlag)
                                {
                                    bank.Status = (BankTransaction.StatusEnum)Enum.Parse(typeof(BankTransaction.StatusEnum), bankStatus);
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("SubTotal"))
                        {
                            #region Validations of SubTotal
                            if (dr["SubTotal"].ToString() != string.Empty)
                            {
                                if (dr["SubTotal"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SubTotal ( " + dr["SubTotal"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString().Substring(0, 25));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        bank.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString());
                                    }
                                }
                                else
                                {
                                    bank.SubTotal = Convert.ToDouble(dr["SubTotal"].ToString());
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TotalTax"))
                        {
                            #region Validations of TotalTax
                            if (dr["TotalTax"].ToString() != string.Empty)
                            {
                                if (dr["TotalTax"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TotalTax ( " + dr["TotalTax"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString().Substring(0, 25));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        bank.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString());
                                    }
                                }
                                else
                                {
                                    bank.TotalTax = Convert.ToDouble(dr["TotalTax"].ToString());
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Total"))
                        {
                            #region Validations of Total
                            if (dr["Total"].ToString() != string.Empty)
                            {
                                if (dr["Total"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Total ( " + dr["Total"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            bank.Total = Convert.ToDouble(dr["Total"].ToString().Substring(0, 25));

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            bank.Total = Convert.ToDouble(dr["Total"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        bank.Total = Convert.ToDouble(dr["Total"].ToString());
                                    }
                                }
                                else
                                {
                                    bank.Total = Convert.ToDouble(dr["Total"].ToString());
                                }
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + ex.Source);
                    }
                }
                else
                {
                    return null;
                }
            }

            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return banknode;
        }
    }
}
