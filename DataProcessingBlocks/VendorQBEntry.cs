using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("VendorQBEntry", Namespace = "", IsNullable = false)]
    public class VendorQBEntry
    {
        #region Private Member Variable

        private string m_Name;
        private string m_IsActive;
        private string m_CompanyName;
        private string m_Salutation;
        private string m_FirstName;
        private string m_MiddleName;
        private string m_LastName;
        private Collection<VendorAddress> m_VendorAddress = new Collection<VendorAddress>();
        private string m_Phone;
        private string m_AltPhone;
        private string m_Fax;
        private string m_Email;
        private string m_Contact;
        private string m_AltContact;
        private string m_NameOnCheck;
        private string m_AccountNumber;
        private string m_Notes;
        private VendorTypeRef m_VendorTypeRef;
        private TermsRef m_TermRef;
        private string m_CreditLimit;
        private string m_VendorTaxIdent;
        private string m_IsVendorEligibleFor1099;
        private string m_OpenBalance;
        private string m_OpenBalanceDate;
        private BillingRateRef m_BillingRateRef;
        private string m_ExternalGUID;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_SalesTaxCountry;
        private string m_IsSalesTaxAgency;
        private SalesTaxReturnRef m_SalesTaxReturnRef;
        private string m_TaxRegistrationNumber;
        private string m_ReportingPeriod;
        private string m_IsTaxTrackedOnPurchases;
        private TaxOnPurchasesAccountRef m_TaxOnPurchasesAccountRef;
        private string m_IsTaxTrackedOnSales;
        private TaxOnSalesAccountRef m_TaxOnSalesAccountRef;
        private string m_IsTaxOnTax;
        private PrefillAccountRef m_PrefillAccountRef;
        private CurrencyRef m_CurrencyRef;
        

        #endregion

        #region Constructor
        public VendorQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        public string CompanyName
        {
            get { return m_CompanyName; }
            set { m_CompanyName = value; }
        }
        
        public string Salutation
        {
            get { return m_Salutation; }
            set { m_Salutation = value; }
        }
        
        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        
        public string MiddleName
        {
            get { return m_MiddleName; }
            set { m_MiddleName = value; }
        }

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        
        [XmlArray("VendorAddressREM")]
        public Collection<VendorAddress> VendorAddress
        {
            get { return m_VendorAddress; }
            set { m_VendorAddress = value; }
        }
        
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        
        public string AltPhone
        {
            get { return m_AltPhone; }
            set { m_AltPhone = value; }
        }
        
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        public string Contact
        {
            get { return m_Contact; }
            set { m_Contact = value; }
        }

        public string AltContact
        {
            get { return m_AltContact; }
            set { m_AltContact = value; }
        }

        public string NameOnCheck
        {
            get { return m_NameOnCheck; }
            set { m_NameOnCheck = value; }
        }

        public string AccountNumber
        {
            get { return m_AccountNumber; }
            set { m_AccountNumber = value; }
        }

        public string Notes
        {
            get { return m_Notes; }
            set { m_Notes = value; }
        }

        public VendorTypeRef VendorTypeRef
        {
            get { return m_VendorTypeRef; }
            set { m_VendorTypeRef = value; }
        }

        public TermsRef TermsRef
        {
            get { return m_TermRef; }
            set { m_TermRef = value; }
        }

        public string CreditLimit
        {
            get { return m_CreditLimit; }
            set { m_CreditLimit = value; }
        }

        public string VendorTaxIdent
        {
            get { return m_VendorTaxIdent; }
            set { m_VendorTaxIdent = value; }
        }

        public string IsVendorEligibleFor1099
        {
            get { return m_IsVendorEligibleFor1099; }
            set { m_IsVendorEligibleFor1099 = value; }
        }

        public string OpenBalance
        {
            get { return m_OpenBalance; }
            set { m_OpenBalance = value; }
        }

        public string OpenBalanceDate
        {
            get { return m_OpenBalanceDate; }
            set { m_OpenBalanceDate = value; }
        }

        public BillingRateRef BillingRateRef
        {
            get { return m_BillingRateRef; }
            set { m_BillingRateRef = value; }
        }

        public string ExternalGUID
        {
            get { return m_ExternalGUID; }
            set { m_ExternalGUID = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }

        public string SalesTaxCountry
        {
            get { return m_SalesTaxCountry; }
            set { m_SalesTaxCountry = value; }
        }

        public string IsSalesTaxAgency
        {
            get { return m_IsSalesTaxAgency; }
            set { m_IsSalesTaxAgency = value; }
        }

        public SalesTaxReturnRef SalesTaxReturnRef
        {
            get { return m_SalesTaxReturnRef; }
            set { m_SalesTaxReturnRef = value; }
        }

        public string TaxRegistrationNumber
        {
            get { return m_TaxRegistrationNumber; }
            set { m_TaxRegistrationNumber = value; }
        }

        public string ReportingPeriod
        {
            get { return m_ReportingPeriod; }
            set { m_ReportingPeriod = value; }
        }

        public string IsTaxTrackedOnPurchases
        {
            get { return m_IsTaxTrackedOnPurchases; }
            set { m_IsTaxTrackedOnPurchases = value; }
        }

        public TaxOnPurchasesAccountRef TaxOnPurchasesAccountRef
        {
            get { return m_TaxOnPurchasesAccountRef; }
            set { m_TaxOnPurchasesAccountRef = value; }
        }

        public string IsTaxTrackedOnSales
        {
            get { return m_IsTaxTrackedOnSales; }
            set { m_IsTaxTrackedOnSales = value; }
        }

        public TaxOnSalesAccountRef TaxOnSalesAccountRef
        {
            get { return m_TaxOnSalesAccountRef; }
            set { m_TaxOnSalesAccountRef = value; }
        }

        public string IsTaxOnTax
        {
            get { return m_IsTaxOnTax; }
            set { m_IsTaxOnTax = value; }
        }
        
        public PrefillAccountRef PrefillAccountRef
        {
            get { return m_PrefillAccountRef; }
            set { m_PrefillAccountRef = value; }
        }

        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }


        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.VendorQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement VendorAddRq = requestXmlDoc.CreateElement("VendorAddRq");
            inner.AppendChild(VendorAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement VendorAdd = requestXmlDoc.CreateElement("VendorAdd");

            VendorAddRq.AppendChild(VendorAdd);

            requestXML = requestXML.Replace("<VendorAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorAddressREM>", string.Empty);

            VendorAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

   //bug no-
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorAddRq/VendorAdd"))
            //{
            //    if (oNode.SelectSingleNode("Phone") == null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorAddRq/VendorAdd/Phone");
            //        VendorAdd.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorAddRq/VendorAdd"))
            //{
            //    if (oNode.SelectSingleNode("Fax") == null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorAddRq/VendorAdd/Fax");
            //        VendorAdd.RemoveChild(childNode);
            //    }
            //}
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorAddRq/VendorAdd"))
            //{
            //    if (oNode.SelectSingleNode("Email") == null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorAddRq/VendorAdd/Email");
            //        VendorAdd.RemoveChild(childNode);
            //    }
            //}

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorAddRs/VendorRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofVendor(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating VendorQBEntry information
        /// of existing VendorQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateVendorInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.VendorQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement VendorQBEntryModRq = requestXmlDoc.CreateElement("VendorModRq");
            inner.AppendChild(VendorQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement VendorMod = requestXmlDoc.CreateElement("VendorMod");
            VendorQBEntryModRq.AppendChild(VendorMod);


            requestXML = requestXML.Replace("<VendorAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<VendorAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</VendorAddressREM>", string.Empty);


            VendorMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod/Phone");
                    VendorMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod"))
            {
                if (oNode.SelectSingleNode("OpenBalance") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod/OpenBalance");
                    VendorMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod"))
            {
                if (oNode.SelectSingleNode("OpenBalanceDate") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod/OpenBalanceDate");
                    VendorMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod/Fax");
                    VendorMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod/Email");
                    VendorMod.RemoveChild(childNode);
                }
            }




            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");          
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod").InsertBefore(ListID, firstChild).InnerText = listID;            
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");           
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/VendorModRq/VendorMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }

                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorModRs/VendorRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofVendor(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class VendorQBEntryCollection : Collection<VendorQBEntry>
    {
        
    }


}
