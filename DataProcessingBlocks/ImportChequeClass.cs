using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using DataProcessingBlocks.CheckModule;
using Streams;
using System.ComponentModel;
using EDI.Constant;
using System.Collections;

namespace DataProcessingBlocks
{
    public class ImportChequeClass
    {
        private int cnt;
        private static ImportChequeClass m_ImportChequeClass;
        public bool isIgnoreAll = false;
        public int dt_count = 0;

        #region Constructor
        public ImportChequeClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import Cheque class
        /// </summary>
        /// <returns></returns>
        public static ImportChequeClass GetInstance()
        {
            if (m_ImportChequeClass == null)
                m_ImportChequeClass = new ImportChequeClass();
            return m_ImportChequeClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Cheque QuickBooks collection </returns>
        public DataProcessingBlocks.CheckModule.CheckEntryCollection ImportChequeData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            DataProcessingBlocks.CheckModule.CheckEntryCollection coll = new DataProcessingBlocks.CheckModule.CheckEntryCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            int refcnt = 0;
            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                dt_count = dt.Rows.Count;
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    //bug 501
                    if (CommonUtilities.GetInstance().IsStatementFeature() == false)
                    {
                        try
                        {
                            bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                        }
                        catch (Exception ex)
                        {
                            
                        }
                    }
                    else
                    {
                        CommonUtilities.GetInstance().ValidateFlag = true;
                        CommonUtilities.GetInstance().ListFlag = false;
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = "Validating row " + (validateRowCount) + " of " + dt.Rows.Count + "...";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    DateTime CheckDt = new DateTime();
                    string datevalue = string.Empty;
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Adding ref number

                        CheckEntry Check = new CheckEntry();
                        #region Check whether Statement Import functionality
                        if (CommonUtilities.GetInstance().IsStatementFeature())
                        {
                            Check.IsTaxIncluded = "true";
                        }
                        #endregion
                        Check = coll.FindCheckEntry(dr["RefNumber"].ToString());

                        int datatbl_cnt = 0;
                        if (Check == null)
                        {
                        }
                        else
                        {

                            DataRow[] total_cnt_row = dt.Select("RefNumber = '" + CheckEntryCollection.chk_number.ToString() + "'", "");

                            foreach (DataRow value in total_cnt_row)
                            {
                                datatbl_cnt++;
                            }

                        }

                        int row_cnt = 0;
                        DataRow[] total_cnt = dt.Select("RefNumber = '" + CheckEntryCollection.chk_number.ToString() + "'", "");
                        foreach (DataRow value in total_cnt)
                        {
                            row_cnt++;
                        }
                        #endregion

                        if (Check == null)
                        {
                            #region check entry validations
                            Check = new CheckEntry();
                            if (dt.Columns.Contains("AccountFullName"))
                            {
                                #region Validations of Account Full Name
                                if (dr["AccountFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["AccountFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Account fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                                if (Check.AccountRef.FullName == null)
                                                {
                                                    Check.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                                if (Check.AccountRef.FullName == null)
                                                {
                                                    Check.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                            if (Check.AccountRef.FullName == null)
                                            {
                                                Check.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());

                                        if (Check.AccountRef.FullName == null)
                                        {
                                            Check.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("PayeeEntityFullName"))
                            {
                                #region Validations of Payee Entity Full Name
                                if (dr["PayeeEntityFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["PayeeEntityFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PayeeEntity fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                                if (Check.PayeeEntityRef.FullName == null)
                                                {
                                                    Check.PayeeEntityRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                                if (Check.PayeeEntityRef.FullName == null)
                                                {
                                                    Check.PayeeEntityRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                            if (Check.PayeeEntityRef.FullName == null)
                                            {
                                                Check.PayeeEntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());

                                        if (Check.PayeeEntityRef.FullName == null)
                                        {
                                            Check.PayeeEntityRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            //Axis 617 
                            if (dt.Columns.Contains("Currency"))
                            {
                                #region Validations of Currency Full name
                                if (dr["Currency"].ToString() != string.Empty)
                                {
                                    string strCust = dr["Currency"].ToString();
                                    if (strCust.Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Check.CurrencyRef.FullName == null)
                                                {
                                                    Check.CurrencyRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (Check.CurrencyRef.FullName == null)
                                                {
                                                    Check.CurrencyRef.FullName = null;
                                                }
                                            }


                                        }
                                        else
                                        {
                                            Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Check.CurrencyRef.FullName == null)
                                            {
                                                Check.CurrencyRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (Check.CurrencyRef.FullName == null)
                                        {
                                            Check.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis 617 ends
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out CheckDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Check.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Check.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Check.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            CheckDt = dttest;
                                            Check.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        CheckDt = Convert.ToDateTime(datevalue);
                                        Check.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    Check.CheckDate = CheckDt;

                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.RefNumber = dr["RefNumber"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Check.RefNumber = dr["RefNumber"].ToString();
                                        }

                                    }
                                    else
                                        Check.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                                Check.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                Check.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            Check.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Check.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }

                            QuickBookEntities.Address Address = new Address();
                            if (dt.Columns.Contains("Addr1"))
                            {
                                #region Validations of Addr1
                                if (dr["Addr1"].ToString() != string.Empty)
                                {
                                    if (dr["Addr1"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Addr1 (" + dr["Addr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Addr1 = dr["Addr1"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Addr1 = dr["Addr1"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Addr1 = dr["Addr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr1 = dr["Addr1"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Addr2"))
                            {
                                #region Validations of Addr2
                                if (dr["Addr2"].ToString() != string.Empty)
                                {
                                    if (dr["Addr2"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Addr2 (" + dr["Addr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Addr2 = dr["Addr2"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Addr2 = dr["Addr2"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Addr2 = dr["Addr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr2 = dr["Addr2"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Addr3"))
                            {
                                #region Validations of Addr3
                                if (dr["Addr3"].ToString() != string.Empty)
                                {
                                    if (dr["Addr3"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Addr3 (" + dr["Addr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Addr3 = dr["Addr3"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Addr3 = dr["Addr3"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Addr3 = dr["Addr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr3 = dr["Addr3"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Addr4"))
                            {
                                #region Validations of Addr4
                                if (dr["Addr4"].ToString() != string.Empty)
                                {
                                    if (dr["Addr4"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Addr4 (" + dr["Addr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Addr4 = dr["Addr4"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Addr4 = dr["Addr4"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Addr4 = dr["Addr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr4 = dr["Addr4"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Addr5"))
                            {
                                #region Validations of Addr5
                                if (dr["Addr5"].ToString() != string.Empty)
                                {
                                    if (dr["Addr5"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Addr5 (" + dr["Addr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Addr5 = dr["Addr5"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Addr5 = dr["Addr5"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Addr5 = dr["Addr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr5 = dr["Addr5"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("City"))
                            {
                                #region Validations of City
                                if (dr["City"].ToString() != string.Empty)
                                {
                                    if (dr["City"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This City (" + dr["City"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.City = dr["City"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.City = dr["City"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.City = dr["City"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.City = dr["City"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("State"))
                            {
                                #region Validations of State
                                if (dr["State"].ToString() != string.Empty)
                                {
                                    if (dr["State"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This State (" + dr["State"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.State = dr["State"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.State = dr["State"].ToString();

                                            }
                                        }
                                        else
                                        {
                                            Address.State = dr["State"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.State = dr["State"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("PostalCode"))
                            {
                                #region Validations of Postal Code
                                if (dr["PostalCode"].ToString() != string.Empty)
                                {
                                    if (dr["PostalCode"].ToString().Length > 13)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Postal Code (" + dr["PostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.PostalCode = dr["PostalCode"].ToString().Substring(0, 13);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.PostalCode = dr["PostalCode"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.PostalCode = dr["PostalCode"].ToString();

                                        }
                                    }
                                    else
                                    {
                                        Address.PostalCode = dr["PostalCode"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("Country"))
                            {
                                #region Validations of Country
                                if (dr["Country"].ToString() != string.Empty)
                                {
                                    if (dr["Country"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Country (" + dr["Country"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Country = dr["Country"].ToString().Substring(0, 31);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Country = dr["Country"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Country = dr["Country"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Country = dr["Country"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Note"))
                            {
                                #region Validations of Note
                                if (dr["Note"].ToString() != string.Empty)
                                {
                                    if (dr["Note"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Note (" + dr["Note"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Address.Note = dr["Note"].ToString().Substring(0, 41);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Address.Note = dr["Note"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Address.Note = dr["Note"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Note = dr["Note"].ToString();
                                    }
                                }
                                #endregion

                            }
                            if (Address.Addr1 != null || Address.Addr2 != null || Address.Addr3 != null || Address.Addr4 != null || Address.Addr5 != null
                                || Address.City != null || Address.Country != null || Address.PostalCode != null || Address.State != null || Address.Note != null)
                                Check.Address.Add(Address);

                            if (dt.Columns.Contains("Phone"))
                            {
                                #region Validations of Phone
                                if (dr["Phone"].ToString() != string.Empty)
                                {
                                    if (dr["Phone"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.Phone = dr["Phone"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.Phone = dr["Phone"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Check.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.Phone = dr["Phone"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Fax"))
                            {
                                #region Validations of Fax
                                if (dr["Fax"].ToString() != string.Empty)
                                {
                                    if (dr["Fax"].ToString().Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.Fax = dr["Fax"].ToString().Substring(0, 21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.Fax = dr["Fax"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Check.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.Fax = dr["Fax"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Email"))
                            {
                                #region Validations of Email
                                if (dr["Email"].ToString() != string.Empty)
                                {
                                    if (dr["Email"].ToString().Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.Email = dr["Email"].ToString().Substring(0, 100);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.Email = dr["Email"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Check.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.Email = dr["Email"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsToBePrinted"))
                            {
                                #region Validations of IsToBePrinted
                                if (dr["IsToBePrinted"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                    {
                                        Check.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                        {
                                            Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                                }
                                            }
                                            else
                                            {
                                                Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("IsTaxIncluded"))
                            {
                                #region Validations of IsTaxIncluded
                                if (dr["IsTaxIncluded"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                    {

                                        Check.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                            }
                                            else
                                            {
                                                Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("SalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (Check.SalesTaxCodeRef.FullName == null)
                                                {
                                                    Check.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (Check.SalesTaxCodeRef.FullName == null)
                                                {
                                                    Check.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Check.SalesTaxCodeRef.FullName == null)
                                            {
                                                Check.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());

                                        if (Check.SalesTaxCodeRef.FullName == null)
                                        {
                                            Check.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Check.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                       // Check.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                        Check.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();

                                    }
                                }

                                #endregion

                            }
                            //CHANGES MADE BY MAYURA
                            //Create an object of ApplyCheckTOTxnAdd only when dataTable contain atleast one value from TxnID and Amount.
                            
                            //P Axis 13.1 : issue 689
                            #region Adding ApplyCheckToTxnAdd
                            QuickBookEntities.ApplyCheckToTxnAdd appliedchktotxn = new ApplyCheckToTxnAdd();
                            if (dt.Columns.Contains("ApplyCheckToTxnID"))
                            {
                                #region Validations of ApplyCheckToTxnID
                                if (dr["ApplyCheckToTxnID"].ToString() != string.Empty)
                                {
                                    Hashtable creditmemoRefno = new Hashtable();

                                    //to check whether ref number already exsist
                                    creditmemoRefno = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("CreditMemo", dr["ApplyCheckToTxnID"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                    if (creditmemoRefno.Count == 0)
                                    {
                                        string strMessages = "ApplyToCheckTxn  does not exist in Credit Memo";
                                        DialogResult result = CommonUtilities.ShowLinkToTxnMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            appliedchktotxn.TxnID = "Skip";
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            return null;
                                        }
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in creditmemoRefno)
                                        {
                                            appliedchktotxn.TxnID = de.Value.ToString();
                                        }
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ApplyCheckToTxnAmount"))
                            {
                                #region Validations of ApplyCheckToTxnAmount
                                if (dr["ApplyCheckToTxnAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ApplyCheckToTxnAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ApplyCheckToTxn Amount ( " + dr["ApplyCheckToTxnAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                appliedchktotxn.Amount = dr["ApplyCheckToTxnAmount"].ToString();
                                                if (appliedchktotxn.Amount == null)
                                                {
                                                    appliedchktotxn.Amount = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                appliedchktotxn.Amount = dr["ApplyCheckToTxnAmount"].ToString();
                                                if (appliedchktotxn.Amount == null)
                                                {
                                                    appliedchktotxn.Amount = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            appliedchktotxn.Amount = dr["ApplyCheckToTxnAmount"].ToString();
                                            if (appliedchktotxn.Amount == null)
                                            {
                                                appliedchktotxn.Amount = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Check.ApplyCheckToTxnAdd = new QuickBookEntities.ApplyCheckToTxnAdd(string.Empty, string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["ApplyCheckToTxnAmount"].ToString()))));
                                        appliedchktotxn.Amount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["ApplyCheckToTxnAmount"].ToString())));

                                        if (appliedchktotxn.Amount == null)
                                        {
                                            appliedchktotxn.Amount = null;
                                        }
                                    }
                                }
                                #endregion
                            }
                            #endregion
                            if (appliedchktotxn.TxnID != null || appliedchktotxn.Amount != null)
                            {
                                Check.ApplyCheckToTxnAdd.Add(appliedchktotxn);
                            }

                            #region Expense Line Add
                            DataProcessingBlocks.CheckModule.ExpenseLineAdd ExpLine = new DataProcessingBlocks.CheckModule.ExpenseLineAdd();

                            #region Checking and setting SalesTaxCode
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;

                                }
                            }
                           
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings != null)
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                        {
                                            if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                            {
                                                string FullName = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                                // IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);
                                                ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ExpenseAccountFullName"))
                            {
                                #region Validations of Account Full name
                                if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["ExpenseAccountFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());

                                        if (ExpLine.AccountRef.FullName == null)
                                        {
                                            ExpLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseAmount"))
                            {
                                #region Validations for ExpenseAmount
                                if (dr["ExpenseAmount"].ToString() != string.Empty)
                                {
                                    decimal amount = 0;

                                    if (!decimal.TryParse(dr["ExpenseAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAmount ( " + dr["ExpenseAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            if (CommonUtilities.GetInstance().BtnPreview == false)
                                            {
                                                if (defaultSettings == null)
                                                {
                                                    ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                }
                                                else
                                                {
                                                    if (defaultSettings.GrossToNet == "1")
                                                    {
                                                        if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                        {
                                                            if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                            {
                                                                decimal Amount = Convert.ToDecimal(dr["ExpenseAmount"].ToString());
                                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                {                                                                   
                                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                    amount = Amount / (1 + (TaxRate / 100));
                                                                }

                                                                ExpLine.Amount = string.Format("{0:000000.00}", amount);
                                                            }
                                                        }
                                                        //Check if ExpLine.Amount is null
                                                        if (ExpLine.Amount == null)
                                                        {
                                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                        }

                                    }


                                }

                                #endregion

                            }
                            
                            if (dt.Columns.Contains("ExpenseMemo"))
                            {
                                #region Validations for Memo
                                if (dr["ExpenseMemo"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseMemo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseMemo ( " + dr["ExpenseMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["ExpenseMemo"].ToString().Substring(0, 4000);
                                                ExpLine.Memo = strMemo;

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["ExpenseMemo"].ToString();
                                                ExpLine.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["ExpenseMemo"].ToString();
                                            ExpLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["ExpenseMemo"].ToString();
                                        ExpLine.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ExpenseCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseCustomer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());

                                        if (ExpLine.CustomerRef.FullName == null)
                                        {
                                            ExpLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseClassFullName"))
                            {
                                #region Validations of ExpenseClass Full name
                                if (dr["ExpenseClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseClass name (" + dr["ExpenseClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                            if (ExpLine.ClassRef.FullName == null)
                                            {
                                                ExpLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());

                                        if (ExpLine.ClassRef.FullName == null)
                                        {
                                            ExpLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());

                                        if (ExpLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ExpLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseBillableStatus"))
                            {
                                #region validations of  Expense Billable Status
                                if (dr["ExpenseBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ExpLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ExpLine.BillableStatus = dr["ExpenseBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                        if (ExpLine.SalesRepRef.FullName == null)
                                        {
                                            ExpLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt1 == null)
                                            ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                        {
                                            ExpLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt1 == null)
                                        ExpLine.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                    else
                                        ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, ExpLine.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                    if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                    {
                                        ExpLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt2 == null)
                                            ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                        {
                                            ExpLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt2 == null)
                                        ExpLine.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                    else
                                        ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, ExpLine.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                    if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                    {
                                        ExpLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (ExpLine.AccountRef != null || ExpLine.Amount != null || ExpLine.BillableStatus != null || ExpLine.ClassRef != null || ExpLine.CustomerRef != null || ExpLine.Memo != null || ExpLine.SalesTaxCodeRef != null)
                            {
                                Check.ExpenseLineAdd.Add(ExpLine);
                            }

                            #endregion

                            #region Item Line Add
                            DataProcessingBlocks.CheckModule.ItemLineAdd ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();



                            #region Checking and setting SalesTaxCode
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;
                                }
                            }
                            
                            TaxRateValue = string.Empty;
                            ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings != null)
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                        {
                                            if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                            {
                                                string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                                //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                                ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ItemFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemFullName"].ToString() != string.Empty)
                                {
                                    ItemLine.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                    if (ItemLine.ItemRef.FullName == null)
                                        ItemLine.ItemRef.FullName = null;

                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ItemInventorySiteFullName"))
                            {
                                #region Validations of Inventory SiteRef Full name
                                if (dr["ItemInventorySiteFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemInventorySiteFullName"].ToString();
                                    if (strCust.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());

                                        if (ItemLine.InventorySiteRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ItemInventorySiteLocationFullName"))
                            {
                                #region Validations of Inventory SiteRef Location Full name
                                if (dr["ItemInventorySiteLocationFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemInventorySiteLocationFullName"].ToString();
                                    if (strCust.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                                if (ItemLine.InventorySiteLocationRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteLocationRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                                if (ItemLine.InventorySiteLocationRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                            if (ItemLine.InventorySiteLocationRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());

                                        if (ItemLine.InventorySiteLocationRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Description"].ToString().Substring(0, 4095);
                                                ItemLine.Desc = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Description"].ToString();
                                                ItemLine.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            ItemLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        ItemLine.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    ItemLine.Quantity = strQuantity;
                                }

                                #endregion

                            }

                            //axis 10.0 chnages
                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            // axis 10.0 chnages ends


                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {                                   
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cost ( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().BtnPreview == false)
                                        {
                                            if (defaultSettings != null)
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {                                                               
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                cost = Cost / (1 + (TaxRate / 100));
                                                            }

                                                            ItemLine.Cost = Convert.ToString(Math.Round(cost, 5));
                                                        }
                                                    }
                                                    //Check if ItemLine.Amount is null
                                                    if (ItemLine.Cost == null)
                                                    {
                                                        ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ItemAmount"))
                            {
                                #region Validations for ItemAmount
                                if (dr["ItemAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ItemAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemAmount ( " + dr["ItemAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().BtnPreview == false)
                                        {
                                            if (defaultSettings != null)
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Amount = Convert.ToDecimal(dr["ItemAmount"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {                                                               
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                amount = Amount / (1 + (TaxRate / 100));
                                                            }

                                                            ItemLine.Amount = string.Format("{0:000000.00}", amount);
                                                        }
                                                    }
                                                    //Check if ItemLine.Amount is null
                                                    if (ItemLine.Amount == null)
                                                    {
                                                        ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                                    }
                                                }

                                                else
                                                {
                                                    ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ItemCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());

                                        if (ItemLine.CustomerRef.FullName == null)
                                        {
                                            ItemLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemClassFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ItemClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Class name (" + dr["ItemClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());

                                        if (ItemLine.ClassRef.FullName == null)
                                        {
                                            ItemLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());

                                        if (ItemLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ItemLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemBillableStatus"))
                            {
                                #region validations of Billable Status
                                if (dr["ItemBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemLine.BillableStatus = dr["ItemBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                            {
                                #region Validations of ItemSalesRepRef Full name
                                if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                        if (ItemLine.SalesRepRef.FullName == null)
                                        {
                                            ItemLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt1 == null)
                                            ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                        {
                                            ItemLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt1 == null)
                                        ItemLine.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                    else
                                        ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, ItemLine.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                    if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                    {
                                        ItemLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt2 == null)
                                            ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                        {
                                            ItemLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt2 == null)
                                        ItemLine.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                    else
                                        ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, ItemLine.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                    if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                    {
                                        ItemLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of OverrideItemAccount Full name
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["OverrideItemAccountFullName"].ToString();
                                    if (strSales.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This OverrideItemAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());

                                        if (ItemLine.OverrideItemAccountRef.FullName == null)
                                        {
                                            ItemLine.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }



                            #endregion

                            if (ItemLine.Amount != null || ItemLine.BillableStatus != null || ItemLine.ClassRef != null || ItemLine.Cost != null || ItemLine.CustomerRef != null || ItemLine.Desc != null || ItemLine.ItemRef != null || ItemLine.Quantity != null || ItemLine.SalesTaxCodeRef != null || ItemLine.UnitOfMeasure != null)
                            {
                                Check.ItemLineAdd.Add(ItemLine);
                            }


                            // Axis 10.0
                            if (row_cnt == 1)
                            {
                                #region Item Line add for Freight

                                if (dt.Columns.Contains("Freight"))
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                        {
                                            ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                            //Adding freight charge item to estimate line.
                                            ItemLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                            ItemLine.ItemRef.FullName = defaultSettings.Frieght;
                                            //Adding freight charge amount to estimate line.


                                            #region Validations for Rate
                                            if (dr["Freight"].ToString() != string.Empty)
                                            {
                                                decimal rate = 0;
                                               
                                                if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                                {
                                                    if (isIgnoreAll == false)
                                                    {
                                                        string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                        if (Convert.ToString(result) == "Cancel")
                                                        {
                                                            continue;
                                                        }
                                                        if (Convert.ToString(result) == "No")
                                                        {
                                                            return null;
                                                        }
                                                        if (Convert.ToString(result) == "Ignore")
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                        if (Convert.ToString(result) == "Abort")
                                                        {
                                                            isIgnoreAll = true;
                                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                        ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                                    {
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.GrossToNet == "1")
                                                            {
                                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                                {
                                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                                    {
                                                                        decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                        {                                                                           
                                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                            rate = Rate / (1 + (TaxRate / 100));
                                                                        }

                                                                        ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                                    }
                                                                }
                                                                //Check if EstLine.Rate is null
                                                                if (ItemLine.Cost == null)
                                                                {
                                                                    ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                    }
                                                }
                                            }
                                            Check.ItemLineAdd.Add(ItemLine);
                                            #endregion

                                        }
                                    }
                                }

                                #endregion

                                #region Item Line add for Insurance

                                if (dt.Columns.Contains("Insurance"))
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                        {
                                            ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                            //Adding Insurance charge item to estimate line.
                                            ItemLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                            ItemLine.ItemRef.FullName = defaultSettings.Insurance;
                                            //Adding Insurance charge amount to estimate line.


                                            #region Validations for Rate
                                            if (dr["Insurance"].ToString() != string.Empty)
                                            {
                                                decimal rate = 0;
                                                //decimal amount;
                                                if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                                {
                                                    if (isIgnoreAll == false)
                                                    {
                                                        string strMessages = "This Insurance ( " + dr["Insurance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                        if (Convert.ToString(result) == "Cancel")
                                                        {
                                                            continue;
                                                        }
                                                        if (Convert.ToString(result) == "No")
                                                        {
                                                            return null;
                                                        }
                                                        if (Convert.ToString(result) == "Ignore")
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                        if (Convert.ToString(result) == "Abort")
                                                        {
                                                            isIgnoreAll = true;
                                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                        ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                                    {
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.GrossToNet == "1")
                                                            {
                                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                                {
                                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                                    {
                                                                        decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                        {                                                                           
                                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                            rate = Rate / (1 + (TaxRate / 100));
                                                                        }

                                                                        ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                                    }
                                                                }
                                                                //Check if EstLine.Rate is null
                                                                if (ItemLine.Cost == null)
                                                                {
                                                                    ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                    }
                                                }
                                            }
                                            Check.ItemLineAdd.Add(ItemLine);
                                            #endregion
                                        }
                                    }
                                }

                                #endregion

                                #region Item Line Add  for Discount

                                if (dt.Columns.Contains("Discount"))
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                        {
                                            ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                            //Adding Discount charge item to estimate line.
                                            ItemLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                            ItemLine.ItemRef.FullName = defaultSettings.Discount;
                                            //Adding Discount charge amount to estimate line.

                                            #region Validations for Rate
                                            if (dr["Discount"].ToString() != string.Empty)
                                            {
                                                decimal rate = 0;
                                                //decimal amount;
                                                if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                                {
                                                    if (isIgnoreAll == false)
                                                    {
                                                        string strMessages = "This Discount ( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                        if (Convert.ToString(result) == "Cancel")
                                                        {
                                                            continue;
                                                        }
                                                        if (Convert.ToString(result) == "No")
                                                        {
                                                            return null;
                                                        }
                                                        if (Convert.ToString(result) == "Ignore")
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                        if (Convert.ToString(result) == "Abort")
                                                        {
                                                            isIgnoreAll = true;
                                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                        ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                                    {
                                                        if (defaultSettings.GrossToNet == "1")
                                                        {
                                                            if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                            {
                                                                if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                                {
                                                                    decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                    {                                                                       
                                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                        rate = Rate / (1 + (TaxRate / 100));
                                                                    }

                                                                    ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                                }
                                                            }
                                                            //Check if EstLine.Rate is null
                                                            if (ItemLine.Cost == null)
                                                            {
                                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                    }
                                                }
                                            }
                                            Check.ItemLineAdd.Add(ItemLine);
                                            #endregion
                                        }
                                    }
                                }

                                #endregion
                            }
                            #endregion
                            coll.Add(Check);
                        }
                        else
                        {
                            #region Expense Line Add
                            DataProcessingBlocks.CheckModule.ExpenseLineAdd ExpLine = new DataProcessingBlocks.CheckModule.ExpenseLineAdd();

                            #region Checking and setting SalesTaxCode
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;
                                }
                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings != null)
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                        {
                                            if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                            {
                                                string FullName = dr["ExpenseSalesTaxCodeFullName"].ToString();

                                                ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ExpenseAccountFullName"))
                            {
                                #region Validations of Account Full name
                                if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["ExpenseAccountFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());

                                        if (ExpLine.AccountRef.FullName == null)
                                        {
                                            ExpLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExpenseAmount"))
                            {
                                #region Validations for ExpenseAmount
                                if (dr["ExpenseAmount"].ToString() != string.Empty)
                                {
                                    decimal amount = 0;

                                    if (!decimal.TryParse(dr["ExpenseAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAmount ( " + dr["ExpenseAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            if (CommonUtilities.GetInstance().BtnPreview == false)
                                            {
                                                if (defaultSettings == null)
                                                {
                                                    ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                }
                                                else
                                                {
                                                    if (defaultSettings.GrossToNet == "1")
                                                    {
                                                        if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                        {
                                                            if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                            {
                                                                decimal Amount = Convert.ToDecimal(dr["ExpenseAmount"].ToString());
                                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                {                                                                  
                                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                    amount = Amount / (1 + (TaxRate / 100));
                                                                }

                                                                ExpLine.Amount = string.Format("{0:000000.00}", amount);
                                                            }
                                                        }
                                                        //Check if ExpLine.Amount is null
                                                        if (ExpLine.Amount == null)
                                                        {
                                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                        }

                                    }


                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseMemo"))
                            {
                                #region Validations for Memo
                                if (dr["ExpenseMemo"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseMemo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseMemo ( " + dr["ExpenseMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["ExpenseMemo"].ToString().Substring(0, 4000);
                                                ExpLine.Memo = strMemo;

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["ExpenseMemo"].ToString();
                                                ExpLine.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["ExpenseMemo"].ToString();
                                            ExpLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["ExpenseMemo"].ToString();
                                        ExpLine.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ExpenseCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseCustomer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());

                                        if (ExpLine.CustomerRef.FullName == null)
                                        {
                                            ExpLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseClassFullName"))
                            {
                                #region Validations of ExpenseClass Full name
                                if (dr["ExpenseClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseClass name (" + dr["ExpenseClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                            if (ExpLine.ClassRef.FullName == null)
                                            {
                                                ExpLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());

                                        if (ExpLine.ClassRef.FullName == null)
                                        {
                                            ExpLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());

                                        if (ExpLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ExpLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseBillableStatus"))
                            {
                                #region validations of  Expense Billable Status
                                if (dr["ExpenseBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ExpLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ExpLine.BillableStatus = dr["ExpenseBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }
                            //Improvement::548
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                        if (ExpLine.SalesRepRef.FullName == null)
                                        {
                                            ExpLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ExpLine.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                            //        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                            //        {
                            //            ExpLine.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt1 == null)
                                            ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                        {
                                            ExpLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt1 == null)
                                        ExpLine.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                    else
                                        ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, ExpLine.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                    if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                    {
                                        ExpLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ExpLine.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                            //        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                            //        {
                            //            ExpLine.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt2 == null)
                                            ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                        {
                                            ExpLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt2 == null)
                                        ExpLine.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                    else
                                        ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, ExpLine.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                    if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                    {
                                        ExpLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }
                            if (ExpLine.AccountRef != null || ExpLine.Amount != null || ExpLine.BillableStatus != null || ExpLine.ClassRef != null || ExpLine.CustomerRef != null || ExpLine.Memo != null || ExpLine.SalesTaxCodeRef != null)
                            {
                                Check.ExpenseLineAdd.Add(ExpLine);
                            }

                            #endregion

                            #region Item Line Add
                            DataProcessingBlocks.CheckModule.ItemLineAdd ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                            #region Checking and setting SalesTaxCode
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;
                                }
                            }
                            
                            TaxRateValue = string.Empty;
                            ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (CommonUtilities.GetInstance().BtnPreview == false)
                            {
                                if (defaultSettings != null)
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                        {
                                            if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                            {
                                                string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                                //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                                ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            cnt = Check.ItemLineAdd.Count - 1 - dt_count;

                            if (dt.Columns.Contains("Freight") || dt.Columns.Contains("Insurance") || dt.Columns.Contains("Discount"))
                            {
                                int deleteCount = 0;

                                for (int i = cnt; i >= 1; i--)
                                {

                                    if (deleteCount < 2)
                                    {
                                        Check.ItemLineAdd.RemoveAt(i);
                                        deleteCount++;
                                    }


                                }
                            }

                            if (dt.Columns.Contains("ItemFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemFullName"].ToString() != string.Empty)
                                {
                                    ItemLine.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                    if (ItemLine.ItemRef.FullName == null)
                                        ItemLine.ItemRef.FullName = null;

                                }
                                #endregion

                            }
							//Bug 528
                            if (dt.Columns.Contains("ItemInventorySiteFullName"))
                            {
                                #region Validations of Inventory SiteRef Full name
                                if (dr["ItemInventorySiteFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemInventorySiteFullName"].ToString();
                                    if (strCust.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());

                                        if (ItemLine.InventorySiteRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("ItemInventorySiteLocationFullName"))
                            {
                                #region Validations of Inventory Site location Full name
                                if (dr["ItemInventorySiteLocationFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemInventorySiteLocationFullName"].ToString();
                                    if (strCust.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                                if (ItemLine.InventorySiteLocationRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteLocationRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                                if (ItemLine.InventorySiteLocationRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteLocationRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                            if (ItemLine.InventorySiteLocationRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());

                                        if (ItemLine.InventorySiteLocationRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }


                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Description"].ToString().Substring(0, 4095);
                                                ItemLine.Desc = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Description"].ToString();
                                                ItemLine.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            ItemLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        ItemLine.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    ItemLine.Quantity = strQuantity;
                                }

                                #endregion

                            }

                            //axis 10.0 chnages
                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }
                            // axis 10.0 chnages ends

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cost ( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().BtnPreview == false)
                                        {
                                            if (defaultSettings != null)
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {                                                               
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                cost = Cost / (1 + (TaxRate / 100));
                                                            }

                                                            ItemLine.Cost = Convert.ToString(Math.Round(cost, 5));
                                                        }
                                                    }
                                                    //Check if ItemLine.Amount is null
                                                    if (ItemLine.Cost == null)
                                                    {
                                                        ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ItemAmount"))
                            {
                                #region Validations for ItemAmount
                                if (dr["ItemAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ItemAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemAmount ( " + dr["ItemAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (CommonUtilities.GetInstance().BtnPreview == false)
                                        {
                                            if (defaultSettings != null)
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Amount = Convert.ToDecimal(dr["ItemAmount"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {                                                               
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                amount = Amount / (1 + (TaxRate / 100));
                                                            }

                                                            ItemLine.Amount = string.Format("{0:000000.00}", amount);
                                                        }
                                                    }
                                                    //Check if ItemLine.Amount is null
                                                    if (ItemLine.Amount == null)
                                                    {
                                                        ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion
                            }
                            if (dt.Columns.Contains("ItemCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());

                                        if (ItemLine.CustomerRef.FullName == null)
                                        {
                                            ItemLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemClassFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ItemClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Class name (" + dr["ItemClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());

                                        if (ItemLine.ClassRef.FullName == null)
                                        {
                                            ItemLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());

                                        if (ItemLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ItemLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemBillableStatus"))
                            {
                                #region validations of Billable Status
                                if (dr["ItemBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemLine.BillableStatus = dr["ItemBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                            {
                                #region Validations of ItemSalesRepRef Full name
                                if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                        if (ItemLine.SalesRepRef.FullName == null)
                                        {
                                            ItemLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ItemLine.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                            //        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                            //        {
                            //            ItemLine.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt1 == null)
                                            ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                        {
                                            ItemLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt1 == null)
                                        ItemLine.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                    else
                                        ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, ItemLine.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                    if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                    {
                                        ItemLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ItemLine.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                            //        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                            //        {
                            //            ItemLine.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt2 == null)
                                            ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                        {
                                            ItemLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt2 == null)
                                        ItemLine.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                    else
                                        ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, ItemLine.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                    if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                    {
                                        ItemLine.DataExt2 = null;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of OverrideItemAccount Full name
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["OverrideItemAccountFullName"].ToString();
                                    if (strSales.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This OverrideItemAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());

                                        if (ItemLine.OverrideItemAccountRef.FullName == null)
                                        {
                                            ItemLine.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                           
                            Check.ItemLineAdd.Add(ItemLine);
                           
                            // Axis 10.0 


                            if (CheckEntryCollection.chk_cnt == datatbl_cnt - 1)
                            {
                                refcnt = 1;
                            }




                            if (refcnt == 1)
                            {
                                #region Item Line add for Freight



                                if (dt.Columns.Contains("Freight"))
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                        {
                                            ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                            //Adding freight charge item to estimate line.
                                            ItemLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                            ItemLine.ItemRef.FullName = defaultSettings.Frieght;
                                            //Adding freight charge amount to estimate line.


                                            #region Validations for Rate
                                            if (dr["Freight"].ToString() != string.Empty)
                                            {
                                                decimal rate = 0;
                                                
                                                if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                                {
                                                    if (isIgnoreAll == false)
                                                    {
                                                        string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                        if (Convert.ToString(result) == "Cancel")
                                                        {
                                                            continue;
                                                        }
                                                        if (Convert.ToString(result) == "No")
                                                        {
                                                            return null;
                                                        }
                                                        if (Convert.ToString(result) == "Ignore")
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                        if (Convert.ToString(result) == "Abort")
                                                        {
                                                            isIgnoreAll = true;
                                                            decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                        ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                                    {
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.GrossToNet == "1")
                                                            {
                                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                                {
                                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                                    {
                                                                        decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                        {                                                                           
                                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                            rate = Rate / (1 + (TaxRate / 100));
                                                                        }

                                                                        ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                                    }
                                                                }

                                                                if (ItemLine.Cost == null)
                                                                {
                                                                    ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                    }
                                                }
                                            }
                                            Check.ItemLineAdd.Add(ItemLine);
                                            #endregion

                                        }
                                    }
                                }
                                #endregion


                                #region Item Line add for Insurance

                                if (dt.Columns.Contains("Insurance"))
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                        {
                                            ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                            //Adding Insurance charge item to estimate line.
                                            ItemLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                            ItemLine.ItemRef.FullName = defaultSettings.Insurance;
                                            //Adding Insurance charge amount to estimate line.


                                            #region Validations for Rate
                                            if (dr["Insurance"].ToString() != string.Empty)
                                            {
                                                decimal rate = 0;
                                              
                                                if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                                {
                                                    if (isIgnoreAll == false)
                                                    {
                                                        string strMessages = "This Insurance ( " + dr["Insurance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                        if (Convert.ToString(result) == "Cancel")
                                                        {
                                                            continue;
                                                        }
                                                        if (Convert.ToString(result) == "No")
                                                        {
                                                            return null;
                                                        }
                                                        if (Convert.ToString(result) == "Ignore")
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                        if (Convert.ToString(result) == "Abort")
                                                        {
                                                            isIgnoreAll = true;
                                                            decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                        ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                                    {
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.GrossToNet == "1")
                                                            {
                                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                                {
                                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                                    {
                                                                        decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                        {                                                                           
                                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                            rate = Rate / (1 + (TaxRate / 100));
                                                                        }

                                                                        ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                                    }
                                                                }
                                                                //Check if EstLine.Rate is null
                                                                if (ItemLine.Cost == null)
                                                                {
                                                                    ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                    }
                                                }
                                            }
                                            Check.ItemLineAdd.Add(ItemLine);
                                            #endregion
                                        }
                                    }
                                }

                                #endregion

                                #region Item Line Add  for Discount

                                if (dt.Columns.Contains("Discount"))
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                        {
                                            ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                            //Adding Discount charge item to estimate line.
                                            ItemLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                            ItemLine.ItemRef.FullName = defaultSettings.Discount;
                                            //Adding Discount charge amount to estimate line.

                                            #region Validations for Rate
                                            if (dr["Discount"].ToString() != string.Empty)
                                            {
                                                decimal rate = 0;
                                               
                                                if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                                {
                                                    if (isIgnoreAll == false)
                                                    {
                                                        string strMessages = "This Discount ( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                        if (Convert.ToString(result) == "Cancel")
                                                        {
                                                            continue;
                                                        }
                                                        if (Convert.ToString(result) == "No")
                                                        {
                                                            return null;
                                                        }
                                                        if (Convert.ToString(result) == "Ignore")
                                                        {
                                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                        if (Convert.ToString(result) == "Abort")
                                                        {
                                                            isIgnoreAll = true;
                                                            decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                            ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                        ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                    }
                                                }
                                                else
                                                {
                                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                                    {
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.GrossToNet == "1")
                                                            {
                                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                                {
                                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                                    {
                                                                        decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                        {                                                                          
                                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                            rate = Rate / (1 + (TaxRate / 100));
                                                                        }

                                                                        ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                                    }
                                                                }
                                                                //Check if EstLine.Rate is null
                                                                if (ItemLine.Cost == null)
                                                                {
                                                                    ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                    }
                                                }
                                            }
                                            Check.ItemLineAdd.Add(ItemLine);
                                            #endregion
                                        }
                                    }
                                }

                                #endregion

                                //bug no. 410
                                #region check Line add for SalesTax

                                if (dt.Columns.Contains("SalesTax"))
                                {
                                    if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                                    {
                                        ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                        //Adding SalesTax charge item to check line.                                          

                                        ItemLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                        ItemLine.ItemRef.FullName = defaultSettings.SalesTax;

                                        //Adding SalesTax charge amount to check line.

                                        #region Validations for Rate
                                        if (dr["SalesTax"].ToString() != string.Empty)
                                        {
                                            decimal rate = 0;
                                          
                                            if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                            {
                                                if (isIgnoreAll == false)
                                                {
                                                    string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                    if (Convert.ToString(result) == "Cancel")
                                                    {
                                                        continue;
                                                    }
                                                    if (Convert.ToString(result) == "No")
                                                    {
                                                        return null;
                                                    }
                                                    if (Convert.ToString(result) == "Ignore")
                                                    {
                                                        string strAmount = dr["SalesTax"].ToString();
                                                        ItemLine.Amount = strAmount;
                                                    }
                                                    if (Convert.ToString(result) == "Abort")
                                                    {
                                                        isIgnoreAll = true;
                                                        string strAmount = dr["SalesTax"].ToString();
                                                        ItemLine.Amount = strAmount;
                                                    }
                                                }
                                                else
                                                {
                                                    string strAmount = dr["SalesTax"].ToString();
                                                    ItemLine.Amount = strAmount;
                                                }
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {                                                               
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            ItemLine.Amount = string.Format("{0:000000.00}", rate);
                                                        }
                                                    }
                                                    //Check if InvoiceLine.Rate is null
                                                    if (ItemLine.Amount == null)
                                                    {
                                                        ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                                }

                                            }
                                        }
                                        Check.ItemLineAdd.Add(ItemLine);
                                        #endregion
                                    }
                                }


                                #endregion
                                refcnt = 0;

                            }



                            #endregion
                        }
                    }

                    else
                    {
                        CheckEntry Check = new CheckEntry();

                        #region Check whether Statement Import functionality
                        if (CommonUtilities.GetInstance().IsStatementFeature())
                        {
                            Check.IsTaxIncluded = "true";
                        }
                        #endregion
                        #region Without Adding ref number
                        if (dt.Columns.Contains("AccountFullName"))
                        {
                            #region Validations of Account Full Name
                            if (dr["AccountFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["AccountFullName"].ToString();
                                if (strAccount.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Account fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                            if (Check.AccountRef.FullName == null)
                                            {
                                                Check.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                            if (Check.AccountRef.FullName == null)
                                            {
                                                Check.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                        if (Check.AccountRef.FullName == null)
                                        {
                                            Check.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Check.AccountRef = new AccountRef(dr["AccountFullName"].ToString());

                                    if (Check.AccountRef.FullName == null)
                                    {
                                        Check.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("PayeeEntityFullName"))
                        {
                            #region Validations of Payee Entity Full Name
                            if (dr["PayeeEntityFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["PayeeEntityFullName"].ToString();
                                if (strAccount.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PayeeEntity fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                            if (Check.PayeeEntityRef.FullName == null)
                                            {
                                                Check.PayeeEntityRef.FullName = null;
                                            }
                                            else
                                            {
                                                Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                            if (Check.PayeeEntityRef.FullName == null)
                                            {
                                                Check.PayeeEntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                        if (Check.PayeeEntityRef.FullName == null)
                                        {
                                            Check.PayeeEntityRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Check.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());

                                    if (Check.PayeeEntityRef.FullName == null)
                                    {
                                        Check.PayeeEntityRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        //Axis 617 
                        if (dt.Columns.Contains("Currency"))
                        {
                            #region Validations of Currency Full name
                            if (dr["Currency"].ToString() != string.Empty)
                            {
                                string strCust = dr["Currency"].ToString();
                                if (strCust.Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Check.CurrencyRef.FullName == null)
                                            {
                                                Check.CurrencyRef.FullName = null;
                                            }
                                            else
                                            {
                                                Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (Check.CurrencyRef.FullName == null)
                                            {
                                                Check.CurrencyRef.FullName = null;
                                            }
                                        }


                                    }
                                    else
                                    {
                                        Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        if (Check.CurrencyRef.FullName == null)
                                        {
                                            Check.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Check.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (Check.CurrencyRef.FullName == null)
                                    {
                                        Check.CurrencyRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis 617 ends
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out CheckDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Check.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Check.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        CheckDt = dttest;
                                        Check.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    CheckDt = Convert.ToDateTime(datevalue);
                                    Check.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref Number
                            if (datevalue != string.Empty)
                                Check.CheckDate = CheckDt;

                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["RefNumber"].ToString();
                                if (strRefNum.Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.RefNumber = dr["RefNumber"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.RefNumber = dr["RefNumber"].ToString();
                                    }

                                }
                                else
                                    Check.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                            Check.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            Check.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Check.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    Check.Memo = strMemo;
                                }
                            }

                            #endregion

                        }

                        QuickBookEntities.Address Address = new Address();
                        if (dt.Columns.Contains("Addr1"))
                        {
                            #region Validations of Addr1
                            if (dr["Addr1"].ToString() != string.Empty)
                            {
                                if (dr["Addr1"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Addr1 (" + dr["Addr1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Addr1 = dr["Addr1"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Addr1 = dr["Addr1"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr1 = dr["Addr1"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Addr1 = dr["Addr1"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("Addr2"))
                        {
                            #region Validations of Addr2
                            if (dr["Addr2"].ToString() != string.Empty)
                            {
                                if (dr["Addr2"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Addr2 (" + dr["Addr2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Addr2 = dr["Addr2"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Addr2 = dr["Addr2"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr2 = dr["Addr2"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Addr2 = dr["Addr2"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("Addr3"))
                        {
                            #region Validations of Addr3
                            if (dr["Addr3"].ToString() != string.Empty)
                            {
                                if (dr["Addr3"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Addr3 (" + dr["Addr3"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Addr3 = dr["Addr3"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Addr3 = dr["Addr3"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr3 = dr["Addr3"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Addr3 = dr["Addr3"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Addr4"))
                        {
                            #region Validations of Addr4
                            if (dr["Addr4"].ToString() != string.Empty)
                            {
                                if (dr["Addr4"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Addr4 (" + dr["Addr4"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Addr4 = dr["Addr4"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Addr4 = dr["Addr4"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr4 = dr["Addr4"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Addr4 = dr["Addr4"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("Addr5"))
                        {
                            #region Validations of Addr5
                            if (dr["Addr5"].ToString() != string.Empty)
                            {
                                if (dr["Addr5"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Addr5 (" + dr["Addr5"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Addr5 = dr["Addr5"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Addr5 = dr["Addr5"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Addr5 = dr["Addr5"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Addr5 = dr["Addr5"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("City"))
                        {
                            #region Validations of City
                            if (dr["City"].ToString() != string.Empty)
                            {
                                if (dr["City"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This City (" + dr["City"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.City = dr["City"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.City = dr["City"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.City = dr["City"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.City = dr["City"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("State"))
                        {
                            #region Validations of State
                            if (dr["State"].ToString() != string.Empty)
                            {
                                if (dr["State"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This State (" + dr["State"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.State = dr["State"].ToString().Substring(0, 31);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.State = dr["State"].ToString();

                                        }
                                    }
                                    else
                                    {
                                        Address.State = dr["State"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.State = dr["State"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("PostalCode"))
                        {
                            #region Validations of Postal Code
                            if (dr["PostalCode"].ToString() != string.Empty)
                            {
                                if (dr["PostalCode"].ToString().Length > 13)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Postal Code (" + dr["PostalCode"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.PostalCode = dr["PostalCode"].ToString().Substring(0, 13);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.PostalCode = dr["PostalCode"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.PostalCode = dr["PostalCode"].ToString();

                                    }
                                }
                                else
                                {
                                    Address.PostalCode = dr["PostalCode"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("Country"))
                        {
                            #region Validations of Country
                            if (dr["Country"].ToString() != string.Empty)
                            {
                                if (dr["Country"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Country (" + dr["Country"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Country = dr["Country"].ToString().Substring(0, 31);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Country = dr["Country"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Country = dr["Country"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Country = dr["Country"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Note"))
                        {
                            #region Validations of Note
                            if (dr["Note"].ToString() != string.Empty)
                            {
                                if (dr["Note"].ToString().Length > 41)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Note (" + dr["Note"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Address.Note = dr["Note"].ToString().Substring(0, 41);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Address.Note = dr["Note"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Address.Note = dr["Note"].ToString();
                                    }
                                }
                                else
                                {
                                    Address.Note = dr["Note"].ToString();
                                }
                            }
                            #endregion

                        }
                        if (Address.Addr1 != null || Address.Addr2 != null || Address.Addr3 != null || Address.Addr4 != null || Address.Addr5 != null
                            || Address.City != null || Address.Country != null || Address.PostalCode != null || Address.State != null || Address.Note != null)
                            Check.Address.Add(Address);

                        if (dt.Columns.Contains("Phone"))
                        {
                            #region Validations of Phone
                            if (dr["Phone"].ToString() != string.Empty)
                            {
                                if (dr["Phone"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Phone (" + dr["Phone"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.Phone = dr["Phone"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.Phone = dr["Phone"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.Phone = dr["Phone"].ToString();
                                    }
                                }
                                else
                                {
                                    Check.Phone = dr["Phone"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Fax"))
                        {
                            #region Validations of Fax
                            if (dr["Fax"].ToString() != string.Empty)
                            {
                                if (dr["Fax"].ToString().Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Fax (" + dr["Fax"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.Fax = dr["Fax"].ToString().Substring(0, 21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.Fax = dr["Fax"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.Fax = dr["Fax"].ToString();
                                    }
                                }
                                else
                                {
                                    Check.Fax = dr["Fax"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Email"))
                        {
                            #region Validations of Email
                            if (dr["Email"].ToString() != string.Empty)
                            {
                                if (dr["Email"].ToString().Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Email (" + dr["Email"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.Email = dr["Email"].ToString().Substring(0, 100);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.Email = dr["Email"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.Email = dr["Email"].ToString();
                                    }
                                }
                                else
                                {
                                    Check.Email = dr["Email"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("IsToBePrinted"))
                        {
                            #region Validations of IsToBePrinted
                            if (dr["IsToBePrinted"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                {
                                    Check.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                    {
                                        Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else

                                            Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                            }
                                        }
                                        else
                                        {
                                            Check.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("IsTaxIncluded"))
                        {
                            #region Validations of IsTaxIncluded
                            if (dr["IsTaxIncluded"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                {

                                    Check.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                    {
                                        Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }
                                        else
                                        {
                                            Check.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("SalesTaxCodeFullName"))
                        {
                            #region Validations of SalesTaxCode Full name
                            if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["SalesTaxCodeFullName"].ToString();
                                if (strSales.Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Check.SalesTaxCodeRef.FullName == null)
                                            {
                                                Check.SalesTaxCodeRef.FullName = null;
                                            }
                                            else
                                            {
                                                Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (Check.SalesTaxCodeRef.FullName == null)
                                            {
                                                Check.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (Check.SalesTaxCodeRef.FullName == null)
                                        {
                                            Check.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Check.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());

                                    if (Check.SalesTaxCodeRef.FullName == null)
                                    {
                                        Check.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Check.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Check.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Check.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                   // Check.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    Check.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();

                                }
                            }

                            #endregion

                        }
                        //CHANGES MADE BY MAYURA
                        //Create an object of ApplyCheckTOTxnAdd only when dataTable contain atleast one value from TxnID and Amount.

                        //P Axis 13.1 : issue 689
                        #region Adding ApplyCheckToTxnAdd
                        QuickBookEntities.ApplyCheckToTxnAdd appliedchktotxn = new ApplyCheckToTxnAdd();
                        if (dt.Columns.Contains("ApplyCheckToTxnID"))
                        {
                            #region Validations of ApplyCheckToTxnID
                            if (dr["ApplyCheckToTxnID"].ToString() != string.Empty)
                            {
                                Hashtable creditmemoRefno = new Hashtable();

                                //to check whether ref number already exsist
                                creditmemoRefno = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("CreditMemo", dr["ApplyCheckToTxnID"].ToString(), CommonUtilities.GetInstance().CompanyFile);

                                if (creditmemoRefno.Count == 0)
                                {
                                    //if no creditmemoRefno get
                                   // MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG0144"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    //return null;
                                    string strMessages = "ApplyToCheckTxn  does not exist in Credit Memo";
                                    DialogResult result = CommonUtilities.ShowLinkToTxnMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        appliedchktotxn.TxnID = "Skip";
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        return null;
                                    }
                                }
                                else
                                {
                                    foreach (DictionaryEntry de in creditmemoRefno)
                                    {
                                        appliedchktotxn.TxnID = de.Value.ToString();
                                    }
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ApplyCheckToTxnAmount"))
                        {
                            #region Validations of ApplyCheckToTxnAmount
                            if (dr["ApplyCheckToTxnAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ApplyCheckToTxnAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ApplyCheckToTxn Amount ( " + dr["ApplyCheckToTxnAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            appliedchktotxn.Amount = dr["ApplyCheckToTxnAmount"].ToString();
                                            if (appliedchktotxn.Amount == null)
                                            {
                                                appliedchktotxn.Amount = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            appliedchktotxn.Amount = dr["ApplyCheckToTxnAmount"].ToString();
                                            if (appliedchktotxn.Amount == null)
                                            {
                                                appliedchktotxn.Amount = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        appliedchktotxn.Amount = dr["ApplyCheckToTxnAmount"].ToString();
                                        if (appliedchktotxn.Amount == null)
                                        {
                                            appliedchktotxn.Amount = null;
                                        }
                                    }
                                }
                                else
                                {
                                    //Check.ApplyCheckToTxnAdd = new QuickBookEntities.ApplyCheckToTxnAdd(string.Empty, string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["ApplyCheckToTxnAmount"].ToString()))));
                                    appliedchktotxn.Amount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["ApplyCheckToTxnAmount"].ToString())));

                                    if (appliedchktotxn.Amount == null)
                                    {
                                        appliedchktotxn.Amount = null;
                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion
                        if (appliedchktotxn.TxnID != null || appliedchktotxn.Amount != null)
                        {
                            Check.ApplyCheckToTxnAdd.Add(appliedchktotxn);
                        }

                        #region Expense Line Add
                        DataProcessingBlocks.CheckModule.ExpenseLineAdd ExpLine = new DataProcessingBlocks.CheckModule.ExpenseLineAdd();

                        #region Checking and setting SalesTaxCode
                        if (CommonUtilities.GetInstance().BtnPreview == false)
                        {
                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                        }
                       
                        string TaxRateValue = string.Empty;
                        string ItemSaleTaxFullName = string.Empty;

                        //if default settings contain checkBoxGrossToNet checked.
                        if (CommonUtilities.GetInstance().BtnPreview == false)
                        {
                            if (defaultSettings != null)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                    {
                                        if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                            // IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (dt.Columns.Contains("ExpenseAccountFullName"))
                        {
                            #region Validations of Account Full name
                            if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["ExpenseAccountFullName"].ToString();
                                if (strAccount.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                        if (ExpLine.AccountRef.FullName == null)
                                        {
                                            ExpLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());

                                    if (ExpLine.AccountRef.FullName == null)
                                    {
                                        ExpLine.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseAmount"))
                        {
                            #region Validations for ExpenseAmount
                            if (dr["ExpenseAmount"].ToString() != string.Empty)
                            {
                                decimal amount = 0;

                                if (!decimal.TryParse(dr["ExpenseAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseAmount ( " + dr["ExpenseAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["ExpenseAmount"].ToString();
                                        ExpLine.Amount = strAmount;
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        if (CommonUtilities.GetInstance().BtnPreview == false)
                                        {
                                            if (defaultSettings == null)
                                            {
                                                ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                            }
                                            else
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Amount = Convert.ToDecimal(dr["ExpenseAmount"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                //decimal TaxRate = 10;
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                amount = Amount / (1 + (TaxRate / 100));
                                                            }

                                                            ExpLine.Amount = string.Format("{0:000000.00}", amount);
                                                        }
                                                    }
                                                    //Check if ExpLine.Amount is null
                                                    if (ExpLine.Amount == null)
                                                    {
                                                        ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                    }
                                                }
                                                else
                                                {
                                                    ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                                }

                                            }
                                        }
                                        else { ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString())); }
                                    }
                                    catch (Exception ex)
                                    {
                                        ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                    }

                                }


                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseMemo"))
                        {
                            #region Validations for Memo
                            if (dr["ExpenseMemo"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseMemo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseMemo ( " + dr["ExpenseMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["ExpenseMemo"].ToString().Substring(0, 4000);
                                            ExpLine.Memo = strMemo;

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["ExpenseMemo"].ToString();
                                            ExpLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["ExpenseMemo"].ToString();
                                        ExpLine.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["ExpenseMemo"].ToString();
                                    ExpLine.Memo = strMemo;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseCustomerFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ExpenseCustomerFullName"].ToString();
                                if (strCust.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseCustomer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                        if (ExpLine.CustomerRef.FullName == null)
                                        {
                                            ExpLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());

                                    if (ExpLine.CustomerRef.FullName == null)
                                    {
                                        ExpLine.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseClassFullName"))
                        {
                            #region Validations of ExpenseClass Full name
                            if (dr["ExpenseClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseClassFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseClass name (" + dr["ExpenseClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                            if (ExpLine.ClassRef.FullName == null)
                                            {
                                                ExpLine.ClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                            if (ExpLine.ClassRef.FullName == null)
                                            {
                                                ExpLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                        if (ExpLine.ClassRef.FullName == null)
                                        {
                                            ExpLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());

                                    if (ExpLine.ClassRef.FullName == null)
                                    {
                                        ExpLine.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                        {
                            #region Validations of SalesTaxCode Full name
                            if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                if (strSales.Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                        if (ExpLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ExpLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());

                                    if (ExpLine.SalesTaxCodeRef.FullName == null)
                                    {
                                        ExpLine.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseBillableStatus"))
                        {
                            #region validations of  Expense Billable Status
                            if (dr["ExpenseBillableStatus"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ExpLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseBillableStatus"].ToString(), true));
                                }
                                catch
                                {
                                    ExpLine.BillableStatus = dr["ExpenseBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Improvement::548
                        if (dt.Columns.Contains("SalesRepRefFullName"))
                        {
                            #region Validations of SalesRepRef Full name
                            if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["SalesRepRefFullName"].ToString();
                                if (strSales.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                        if (ExpLine.SalesRepRef.FullName == null)
                                        {
                                            ExpLine.SalesRepRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                    if (ExpLine.SalesRepRef.FullName == null)
                                    {
                                        ExpLine.SalesRepRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("OwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["OwnerID1"].ToString() != string.Empty)
                        //    {
                        //        ExpLine.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                        //        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                        //        {
                        //            ExpLine.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt1 == null)
                                            ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                        {
                                            ExpLine.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ExpLine.DataExt1 == null)
                                        ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                    else
                                        ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                    if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                    {
                                        ExpLine.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue1"].ToString() != string.Empty)
                            {
                                if (ExpLine.DataExt1 == null)
                                    ExpLine.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                else
                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, ExpLine.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                {
                                    ExpLine.DataExt1 = null;
                                }

                            }

                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("OwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["OwnerID2"].ToString() != string.Empty)
                        //    {
                        //        ExpLine.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                        //        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                        //        {
                        //            ExpLine.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt2 == null)
                                            ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                        {
                                            ExpLine.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ExpLine.DataExt2 == null)
                                        ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                    else
                                        ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                    if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                    {
                                        ExpLine.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue2"].ToString() != string.Empty)
                            {
                                if (ExpLine.DataExt2 == null)
                                    ExpLine.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                else
                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, ExpLine.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                {
                                    ExpLine.DataExt2 = null;
                                }

                            }

                            #endregion

                        }

                       if (ExpLine.AccountRef != null || ExpLine.Amount != null || ExpLine.BillableStatus != null || ExpLine.ClassRef != null || ExpLine.CustomerRef != null || ExpLine.Memo != null || ExpLine.SalesTaxCodeRef != null)
                        {
                            Check.ExpenseLineAdd.Add(ExpLine);
                        }

                        #endregion

                        #region Item Line Add
                        DataProcessingBlocks.CheckModule.ItemLineAdd ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();
                        #region Checking and setting SalesTaxCode
                        if (CommonUtilities.GetInstance().BtnPreview == false)
                        {
                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                        }
                       
                        TaxRateValue = string.Empty;
                        ItemSaleTaxFullName = string.Empty;
                        //if default settings contain checkBoxGrossToNet checked.
                        if (CommonUtilities.GetInstance().BtnPreview == false)
                        {
                            if (defaultSettings != null)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                    {
                                        if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                            //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (dt.Columns.Contains("ItemFullName"))
                        {
                            #region Validations of item Full name
                            if (dr["ItemFullName"].ToString() != string.Empty)
                            {
                                ItemLine.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                if (ItemLine.ItemRef.FullName == null)
                                    ItemLine.ItemRef.FullName = null;

                            }
                            #endregion

                        }
						//Bug 528
                        if (dt.Columns.Contains("ItemInventorySiteFullName"))
                        {
                            #region Validations of Inventory SiteRef Full name
                            if (dr["ItemInventorySiteFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ItemInventorySiteFullName"].ToString();
                                if (strCust.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString().Substring(0, 31));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());
                                        if (ItemLine.InventorySiteRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.InventorySiteRef = new InventorySiteRef(dr["ItemInventorySiteFullName"].ToString());

                                    if (ItemLine.InventorySiteRef.FullName == null)
                                    {
                                        ItemLine.InventorySiteRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("ItemInventorySiteLocationFullName"))
                        {
                            #region Validations of Inventory SiteRef Full name
                            if (dr["ItemInventorySiteLocationFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ItemInventorySiteLocationFullName"].ToString();
                                if (strCust.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                            if (ItemLine.InventorySiteLocationRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteLocationRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString().Substring(0, 31));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                            if (ItemLine.InventorySiteLocationRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteLocationRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());
                                        if (ItemLine.InventorySiteLocationRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteLocationRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.InventorySiteLocationRef = new InventorySiteLocationRef(dr["ItemInventorySiteLocationFullName"].ToString());

                                    if (ItemLine.InventorySiteLocationRef.FullName == null)
                                    {
                                        ItemLine.InventorySiteLocationRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }


                        if (dt.Columns.Contains("Description"))
                        {
                            #region Validations for Description
                            if (dr["Description"].ToString() != string.Empty)
                            {
                                if (dr["Description"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["Description"].ToString().Substring(0, 4095);
                                            ItemLine.Desc = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["Description"].ToString();
                                            ItemLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        ItemLine.Desc = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["Description"].ToString();
                                    ItemLine.Desc = strDesc;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("Quantity"))
                        {
                            #region Validations for Quantity
                            if (dr["Quantity"].ToString() != string.Empty)
                            {
                                string strQuantity = dr["Quantity"].ToString();
                                ItemLine.Quantity = strQuantity;
                            }

                            #endregion

                        }

                        //axis 10.0 chnages
                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of ItemLine SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SerialNumber"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LotNumber"))
                        {
                            #region Validations of ItemLine LotNumber
                            if (dr["LotNumber"].ToString() != string.Empty)
                            {
                                if (dr["LotNumber"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemLine.LotNumber = dr["LotNumber"].ToString();
                                }
                            }
                            #endregion
                        }
                        // axis 10.0 chnages ends

                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations for UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0, 31);
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }
                                else
                                {
                                    string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("Cost"))
                        {
                            #region Validations for Cost
                            if (dr["Cost"].ToString() != string.Empty)
                            {                             
                                decimal cost = 0;
                                if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Cost ( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["Cost"].ToString();
                                        ItemLine.Cost = strRate;
                                    }
                                }
                                else
                                {
                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                    {
                                        if (defaultSettings != null)
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                    {
                                                        decimal Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {                                                           
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            cost = Cost / (1 + (TaxRate / 100));
                                                        }

                                                        ItemLine.Cost = Convert.ToString(Math.Round(cost, 5));
                                                    }
                                                }
                                                //Check if ItemLine.Amount is null
                                                if (ItemLine.Cost == null)
                                                {
                                                    ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                    }
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ItemAmount"))
                        {
                            #region Validations for ItemAmount
                            if (dr["ItemAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ItemAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemAmount ( " + dr["ItemAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["ItemAmount"].ToString();
                                        ItemLine.Amount = strAmount;
                                    }
                                }
                                else
                                {
                                    if (CommonUtilities.GetInstance().BtnPreview == false)
                                    {
                                        if (defaultSettings != null)
                                        {
                                            if (defaultSettings.GrossToNet == "1")
                                            {
                                                if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                {
                                                    if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                    {
                                                        decimal Amount = Convert.ToDecimal(dr["ItemAmount"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            amount = Amount / (1 + (TaxRate / 100));
                                                        }

                                                        ItemLine.Amount = string.Format("{0:000000.00}", amount);
                                                    }
                                                }
                                                //Check if ItemLine.Amount is null
                                                if (ItemLine.Amount == null)
                                                {
                                                    ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                    }
                                }
                            }

                            #endregion
                        }
                        if (dt.Columns.Contains("ItemCustomerFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ItemCustomerFullName"].ToString();
                                if (strCust.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                        if (ItemLine.CustomerRef.FullName == null)
                                        {
                                            ItemLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());

                                    if (ItemLine.CustomerRef.FullName == null)
                                    {
                                        ItemLine.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ItemClassFullName"))
                        {
                            #region Validations of Class Full name
                            if (dr["ItemClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemClassFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Class name (" + dr["ItemClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                        if (ItemLine.ClassRef.FullName == null)
                                        {
                                            ItemLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());

                                    if (ItemLine.ClassRef.FullName == null)
                                    {
                                        ItemLine.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                        {
                            #region Validations of SalesTaxCode Full name
                            if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["ItemSalesTaxCodeFullName"].ToString();
                                if (strSales.Length > 3)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                        if (ItemLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ItemLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());

                                    if (ItemLine.SalesTaxCodeRef.FullName == null)
                                    {
                                        ItemLine.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ItemBillableStatus"))
                        {
                            #region validations of Billable Status
                            if (dr["ItemBillableStatus"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemBillableStatus"].ToString(), true));
                                }
                                catch
                                {
                                    ItemLine.BillableStatus = dr["ItemBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Improvement::548
                        if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                        {
                            #region Validations of ItemSalesRepRef Full name
                            if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                if (strSales.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                        if (ItemLine.SalesRepRef.FullName == null)
                                        {
                                            ItemLine.SalesRepRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                    if (ItemLine.SalesRepRef.FullName == null)
                                    {
                                        ItemLine.SalesRepRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemOwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                        //    {
                        //        ItemLine.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                        //        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                        //        {
                        //            ItemLine.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemDataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["ItemDataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt1 == null)
                                            ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                        {
                                            ItemLine.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ItemLine.DataExt1 == null)
                                        ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                    else
                                        ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                    if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                    {
                                        ItemLine.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                            {
                                if (ItemLine.DataExt1 == null)
                                    ItemLine.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                else
                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, ItemLine.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                {
                                    ItemLine.DataExt1 = null;
                                }

                            }

                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemOwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                        //    {
                        //        ItemLine.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                        //        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                        //        {
                        //            ItemLine.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemDataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["ItemDataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt2 == null)
                                            ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                        {
                                            ItemLine.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ItemLine.DataExt2 == null)
                                        ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                    else
                                        ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                    if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                    {
                                        ItemLine.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                            {
                                if (ItemLine.DataExt2 == null)
                                    ItemLine.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                else
                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, ItemLine.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                {
                                    ItemLine.DataExt2 = null;
                                }

                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("OverrideItemAccountFullName"))
                        {
                            #region Validations of OverrideItemAccount Full name
                            if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["OverrideItemAccountFullName"].ToString();
                                if (strSales.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This OverrideItemAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        if (ItemLine.OverrideItemAccountRef.FullName == null)
                                        {
                                            ItemLine.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());

                                    if (ItemLine.OverrideItemAccountRef.FullName == null)
                                    {
                                        ItemLine.OverrideItemAccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        #endregion
                       
                        Check.ItemLineAdd.Add(ItemLine);                       

                        coll.Add(Check);

                        // Axis 10.0
                        #region Item Line add for Freight

                        if (dt.Columns.Contains("Freight"))
                        {
                            if (defaultSettings != null)
                            {
                                if (!string.IsNullOrEmpty(defaultSettings.Frieght) && dr["Freight"].ToString() != string.Empty)
                                {
                                    ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();
                                    //Adding freight charge item to estimate line.
                                    ItemLine.ItemRef = new ItemRef(defaultSettings.Frieght);
                                    ItemLine.ItemRef.FullName = defaultSettings.Frieght;
                                    //Adding freight charge amount to estimate line.


                                    #region Validations for Rate
                                    if (dr["Freight"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        if (!decimal.TryParse(dr["Freight"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Freight ( " + dr["Freight"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                    ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                    ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Freight"].ToString());
                                                ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            if (CommonUtilities.GetInstance().BtnPreview == false)
                                            {
                                                if (defaultSettings != null)
                                                {
                                                    if (defaultSettings.GrossToNet == "1")
                                                    {
                                                        if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                        {
                                                            if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                            {
                                                                decimal Rate = Convert.ToDecimal(dr["Freight"].ToString());
                                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                {
                                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                    rate = Rate / (1 + (TaxRate / 100));
                                                                }

                                                                ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                            }
                                                        }
                                                        //Check if EstLine.Rate is null
                                                        if (ItemLine.Cost == null)
                                                        {
                                                            ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Freight"]), 5));
                                            }
                                        }
                                    }
                                    Check.ItemLineAdd.Add(ItemLine);
                                    #endregion

                                }
                            }
                        }

                        #endregion

                        #region Item Line add for Insurance

                        if (dt.Columns.Contains("Insurance"))
                        {
                            if (defaultSettings != null)
                            {
                                if (!string.IsNullOrEmpty(defaultSettings.Insurance) && dr["Insurance"].ToString() != string.Empty)
                                {
                                    ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                    //Adding Insurance charge item to estimate line.
                                    ItemLine.ItemRef = new ItemRef(defaultSettings.Insurance);
                                    ItemLine.ItemRef.FullName = defaultSettings.Insurance;
                                    //Adding Insurance charge amount to estimate line.


                                    #region Validations for Rate
                                    if (dr["Insurance"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        if (!decimal.TryParse(dr["Insurance"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Insurance ( " + dr["Insurance"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                    ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                    ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            if (CommonUtilities.GetInstance().BtnPreview == false)
                                            {
                                                if (defaultSettings.GrossToNet == "1")
                                                {
                                                    if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                    {
                                                        if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                        {
                                                            decimal Rate = Convert.ToDecimal(dr["Insurance"].ToString());
                                                            if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                CommonUtilities.GetInstance().CountryVersion == "CA")
                                                            {
                                                                decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                rate = Rate / (1 + (TaxRate / 100));
                                                            }

                                                            ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                        }
                                                    }
                                                    //Check if EstLine.Rate is null
                                                    if (ItemLine.Cost == null)
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                    }
                                                }
                                                else
                                                {
                                                    ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Insurance"]), 5));
                                            }
                                        }
                                    }
                                    Check.ItemLineAdd.Add(ItemLine);
                                    #endregion
                                }
                            }
                        }

                        #endregion

                        #region Item Line Add  for Discount

                        if (dt.Columns.Contains("Discount"))
                        {
                            if (defaultSettings != null)
                            {
                                if (!string.IsNullOrEmpty(defaultSettings.Discount) && dr["Discount"].ToString() != string.Empty)
                                {
                                    ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                    //Adding Discount charge item to estimate line.
                                    ItemLine.ItemRef = new ItemRef(defaultSettings.Discount);
                                    ItemLine.ItemRef.FullName = defaultSettings.Discount;
                                    //Adding Discount charge amount to estimate line.

                                    #region Validations for Rate
                                    if (dr["Discount"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        //decimal amount;
                                        if (!decimal.TryParse(dr["Discount"].ToString(), out rate))
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Discount ( " + dr["Discount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                    ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                    ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                                }
                                            }
                                            else
                                            {
                                                decimal strRate = Convert.ToDecimal(dr["Discount"].ToString());
                                                ItemLine.Cost = Convert.ToString(Math.Round(strRate, 5));
                                            }
                                        }
                                        else
                                        {
                                            if (CommonUtilities.GetInstance().BtnPreview == false)
                                            {
                                                if (defaultSettings != null)
                                                {
                                                    if (defaultSettings.GrossToNet == "1")
                                                    {
                                                        if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                                        {
                                                            if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                            {
                                                                decimal Rate = Convert.ToDecimal(dr["Discount"].ToString());
                                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                {
                                                                    //decimal TaxRate = 10;
                                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                    rate = Rate / (1 + (TaxRate / 100));
                                                                }

                                                                ItemLine.Cost = Convert.ToString(Math.Round(rate, 5));
                                                            }
                                                        }
                                                        //Check if EstLine.Rate is null
                                                        if (ItemLine.Cost == null)
                                                        {
                                                            ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.Cost = Convert.ToString(Math.Round(Convert.ToDecimal(dr["Discount"]), 5));
                                            }
                                        }
                                    }
                                    Check.ItemLineAdd.Add(ItemLine);
                                    #endregion
                                }
                            }
                        }

                        #endregion

                        //bug no. 410
                        #region check Line add for SalesTax

                        if (dt.Columns.Contains("SalesTax"))
                        {
                            if (!string.IsNullOrEmpty(defaultSettings.SalesTax) && dr["SalesTax"].ToString() != string.Empty)
                            {
                                ItemLine = new DataProcessingBlocks.CheckModule.ItemLineAdd();

                                //Adding SalesTax charge item to check line.                                          

                                ItemLine.ItemRef = new ItemRef(defaultSettings.SalesTax);
                                ItemLine.ItemRef.FullName = defaultSettings.SalesTax;

                                //Adding SalesTax charge amount to check line.

                                #region Validations for Rate
                                if (dr["SalesTax"].ToString() != string.Empty)
                                {
                                    decimal rate = 0;
                                   
                                    if (!decimal.TryParse(dr["SalesTax"].ToString(), out rate))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTax Rate ( " + dr["SalesTax"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["SalesTax"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["SalesTax"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["SalesTax"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && Check.IsTaxIncluded != null && Check.IsTaxIncluded != string.Empty)
                                            {
                                                if (Check.IsTaxIncluded == "true" || Check.IsTaxIncluded == "1")
                                                {
                                                    decimal Rate = Convert.ToDecimal(dr["SalesTax"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLine.Amount = string.Format("{0:000000.00}", rate);
                                                }
                                            }
                                            //Check if InvoiceLine.Rate is null
                                            if (ItemLine.Amount == null)
                                            {
                                                ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesTax"].ToString()));
                                        }

                                    }
                                }
                                Check.ItemLineAdd.Add(ItemLine);
                                #endregion
                            }
                        }


                        #endregion

                        #endregion
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            #region Customer,Item and Account Query and Add Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                        #region Add Customer if not present
                        if (dt.Columns.Contains("ExpenseCustomerFullName"))
                        {

                            if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                            {
                                string customerName = dr["ExpenseCustomerFullName"].ToString();
                                string[] arr = new string[15];
                                if (customerName.Contains(":"))
                                {
                                    arr = customerName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ExpenseCustomerFullName"].ToString();
                                }
                                #region Set Customer Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                        qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                        CustomerQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    CustomerQueryRq.AppendChild(FullName);
                                                }

                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            CustomerQueryRq.AppendChild(FullName);
                                        }
                                        string pinput = pxmldoc.OuterXml;

                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }

                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {

                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                                }
                                                outputXMLDoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                                {

                                                    #region Customer Add Query

                                                    XmlDocument xmldocadd = new XmlDocument();
                                                    xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                    xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                    xmldocadd.AppendChild(qbXMLcust);
                                                    XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                    qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                    qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                    XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                    qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                    CustomerAddRq.SetAttribute("requestID", "1");
                                                    XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                    CustomerAddRq.AppendChild(CustomerAdd);
                                                    XmlElement Name = xmldocadd.CreateElement("Name");
                                                    Name.InnerText = arr[i];
                                                    CustomerAdd.AppendChild(Name);

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                                CustomerAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }

                                                    #region Adding Bill Address of Customer.
                                                    if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                        (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                    {
                                                        XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                        CustomerAdd.AppendChild(BillAddress);
                                                        if (dt.Columns.Contains("BillAddr1"))
                                                        {

                                                            if (dr["BillAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                                BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                                BillAddress.AppendChild(BillAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr2"))
                                                        {
                                                            if (dr["BillAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                                BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                                BillAddress.AppendChild(BillAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr3"))
                                                        {
                                                            if (dr["BillAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                                BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                                BillAddress.AppendChild(BillAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr4"))
                                                        {
                                                            if (dr["BillAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                                BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                                BillAddress.AppendChild(BillAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr5"))
                                                        {
                                                            if (dr["BillAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                                BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                                BillAddress.AppendChild(BillAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCity"))
                                                        {
                                                            if (dr["BillCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCity = xmldocadd.CreateElement("City");
                                                                BillCity.InnerText = dr["BillCity"].ToString();
                                                                BillAddress.AppendChild(BillCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillState"))
                                                        {
                                                            if (dr["BillState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillState = xmldocadd.CreateElement("State");
                                                                BillState.InnerText = dr["BillState"].ToString();
                                                                BillAddress.AppendChild(BillState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillPostalCode"))
                                                        {
                                                            if (dr["BillPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                                BillAddress.AppendChild(BillPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCountry"))
                                                        {
                                                            if (dr["BillCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                                BillCountry.InnerText = dr["BillCountry"].ToString();
                                                                BillAddress.AppendChild(BillCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillNote"))
                                                        {
                                                            if (dr["BillNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                                BillNote.InnerText = dr["BillNote"].ToString();
                                                                BillAddress.AppendChild(BillNote);
                                                            }
                                                        }


                                                    }

                                                    #endregion

                                                    #region Adding Ship Address of Customer.
                                                    if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                      (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                    {
                                                        XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                        CustomerAdd.AppendChild(ShipAddress);
                                                        if (dt.Columns.Contains("ShipAddr1"))
                                                        {

                                                            if (dr["ShipAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                                ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr2"))
                                                        {
                                                            if (dr["ShipAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                                ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr3"))
                                                        {
                                                            if (dr["ShipAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                                ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr4"))
                                                        {
                                                            if (dr["ShipAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                                ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr5"))
                                                        {
                                                            if (dr["ShipAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                                ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCity"))
                                                        {
                                                            if (dr["ShipCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                                ShipCity.InnerText = dr["ShipCity"].ToString();
                                                                ShipAddress.AppendChild(ShipCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipState"))
                                                        {
                                                            if (dr["ShipState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipState = xmldocadd.CreateElement("State");
                                                                ShipState.InnerText = dr["ShipState"].ToString();
                                                                ShipAddress.AppendChild(ShipState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipPostalCode"))
                                                        {
                                                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                                ShipAddress.AppendChild(ShipPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCountry"))
                                                        {
                                                            if (dr["ShipCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                                ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                                ShipAddress.AppendChild(ShipCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipNote"))
                                                        {
                                                            if (dr["ShipNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                                ShipNote.InnerText = dr["ShipNote"].ToString();
                                                                ShipAddress.AppendChild(ShipNote);
                                                            }
                                                        }

                                                    }
                                                    #endregion

                                                    if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                                    {
                                                        if (dt.Columns.Contains("Phone"))
                                                        {
                                                            if (dr["Phone"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                                Phone.InnerText = dr["Phone"].ToString();
                                                                CustomerAdd.AppendChild(Phone);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Fax"))
                                                        {
                                                            if (dr["Fax"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                                Fax.InnerText = dr["Fax"].ToString();
                                                                CustomerAdd.AppendChild(Fax);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Email"))
                                                        {
                                                            if (dr["Email"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Email = xmldocadd.CreateElement("Email");
                                                                Email.InnerText = dr["Email"].ToString();
                                                                CustomerAdd.AppendChild(Email);
                                                            }
                                                        }
                                                    }
                                                    string custinput = xmldocadd.OuterXml;
                                                    string respcust = string.Empty;
                                                    try
                                                    {
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                        }

                                                        else
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    finally
                                                    {
                                                        if (respcust != string.Empty)
                                                        {
                                                            System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                            outputcustXMLDoc.LoadXml(respcust);
                                                            foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                            {
                                                                string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                                if (statusSeveritycust == "Error")
                                                                {
                                                                    string msg = "New Customer could not be created into QuickBooks \n ";
                                                                    msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                  
                                                                    //Task 1435 (Axis 6.0):
                                                                    ErrorSummary summary = new ErrorSummary(msg);
                                                                    summary.ShowDialog();
                                                                    CommonUtilities.WriteErrorLog(msg);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                            }

                                        }

                                    }
                                #endregion
                                }
                            }
                        }
                        #endregion

                        #region Add ItemCustomerFullName if not present
                        if (dt.Columns.Contains("ItemCustomerFullName"))
                        {

                            if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                            {
                                #region Set Customer Query
                                XmlDocument pxmldoc = new XmlDocument();
                                pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                pxmldoc.AppendChild(qbXML);
                                XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                qbXML.AppendChild(qbXMLMsgsRq);
                                qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                CustomerQueryRq.SetAttribute("requestID", "1");
                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                FullName.InnerText = dr["ItemCustomerFullName"].ToString();
                                CustomerQueryRq.AppendChild(FullName);

                                string pinput = pxmldoc.OuterXml;

                                string resp = string.Empty;
                                try
                                {
                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                    }

                                    else
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                }
                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                finally
                                {
                                    if (resp != string.Empty)
                                    {

                                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                        outputXMLDoc.LoadXml(resp);
                                        string statusSeverity = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                        {
                                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                        }
                                        outputXMLDoc.RemoveAll();
                                        if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                        {

                                            #region Customer Add Query

                                            XmlDocument xmldocadd = new XmlDocument();
                                            xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                            xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                            XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                            xmldocadd.AppendChild(qbXMLcust);
                                            XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                            qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                            qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                            XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                            qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                            CustomerAddRq.SetAttribute("requestID", "1");
                                            XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                            CustomerAddRq.AppendChild(CustomerAdd);
                                            XmlElement Name = xmldocadd.CreateElement("Name");
                                            Name.InnerText = dr["ItemCustomerFullName"].ToString();
                                            CustomerAdd.AppendChild(Name);


                                            #region Adding Bill Address of Customer.
                                            if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                            {
                                                XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                CustomerAdd.AppendChild(BillAddress);
                                                if (dt.Columns.Contains("BillAddr1"))
                                                {

                                                    if (dr["BillAddr1"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                        BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                        BillAddress.AppendChild(BillAdd1);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr2"))
                                                {
                                                    if (dr["BillAddr2"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                        BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                        BillAddress.AppendChild(BillAdd2);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr3"))
                                                {
                                                    if (dr["BillAddr3"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                        BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                        BillAddress.AppendChild(BillAdd3);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr4"))
                                                {
                                                    if (dr["BillAddr4"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                        BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                        BillAddress.AppendChild(BillAdd4);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr5"))
                                                {
                                                    if (dr["BillAddr5"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                        BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                        BillAddress.AppendChild(BillAdd5);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillCity"))
                                                {
                                                    if (dr["BillCity"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillCity = xmldocadd.CreateElement("City");
                                                        BillCity.InnerText = dr["BillCity"].ToString();
                                                        BillAddress.AppendChild(BillCity);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillState"))
                                                {
                                                    if (dr["BillState"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillState = xmldocadd.CreateElement("State");
                                                        BillState.InnerText = dr["BillState"].ToString();
                                                        BillAddress.AppendChild(BillState);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillPostalCode"))
                                                {
                                                    if (dr["BillPostalCode"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                        BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                        BillAddress.AppendChild(BillPostalCode);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillCountry"))
                                                {
                                                    if (dr["BillCountry"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                        BillCountry.InnerText = dr["BillCountry"].ToString();
                                                        BillAddress.AppendChild(BillCountry);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillNote"))
                                                {
                                                    if (dr["BillNote"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                        BillNote.InnerText = dr["BillNote"].ToString();
                                                        BillAddress.AppendChild(BillNote);
                                                    }
                                                }

                                            }


                                            #endregion

                                            #region Adding Ship Address of Customer.
                                            if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                              (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                            {

                                                XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                CustomerAdd.AppendChild(ShipAddress);
                                                if (dt.Columns.Contains("ShipAddr1"))
                                                {

                                                    if (dr["ShipAddr1"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                        ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd1);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr2"))
                                                {
                                                    if (dr["ShipAddr2"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                        ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd2);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr3"))
                                                {
                                                    if (dr["ShipAddr3"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                        ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd3);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr4"))
                                                {
                                                    if (dr["ShipAddr4"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                        ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd4);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr5"))
                                                {
                                                    if (dr["ShipAddr5"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                        ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd5);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipCity"))
                                                {
                                                    if (dr["ShipCity"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                        ShipCity.InnerText = dr["ShipCity"].ToString();
                                                        ShipAddress.AppendChild(ShipCity);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipState"))
                                                {
                                                    if (dr["ShipState"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipState = xmldocadd.CreateElement("State");
                                                        ShipState.InnerText = dr["ShipState"].ToString();
                                                        ShipAddress.AppendChild(ShipState);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipPostalCode"))
                                                {
                                                    if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                        ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                        ShipAddress.AppendChild(ShipPostalCode);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipCountry"))
                                                {
                                                    if (dr["ShipCountry"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                        ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                        ShipAddress.AppendChild(ShipCountry);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipNote"))
                                                {
                                                    if (dr["ShipNote"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                        ShipNote.InnerText = dr["ShipNote"].ToString();
                                                        ShipAddress.AppendChild(ShipNote);
                                                    }
                                                }

                                            }
                                            #endregion

                                            if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                            {
                                                if (dt.Columns.Contains("Phone"))
                                                {
                                                    if (dr["Phone"].ToString() != string.Empty)
                                                    {
                                                        XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                        Phone.InnerText = dr["Phone"].ToString();
                                                        CustomerAdd.AppendChild(Phone);
                                                    }
                                                }
                                                if (dt.Columns.Contains("Fax"))
                                                {
                                                    if (dr["Fax"].ToString() != string.Empty)
                                                    {
                                                        XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                        Fax.InnerText = dr["Fax"].ToString();
                                                        CustomerAdd.AppendChild(Fax);
                                                    }
                                                }
                                                if (dt.Columns.Contains("Email"))
                                                {
                                                    if (dr["Email"].ToString() != string.Empty)
                                                    {
                                                        XmlElement Email = xmldocadd.CreateElement("Email");
                                                        Email.InnerText = dr["Email"].ToString();
                                                        CustomerAdd.AppendChild(Email);
                                                    }
                                                }
                                            }

                                            string custinput = xmldocadd.OuterXml;
                                            string respcust = string.Empty;
                                            try
                                            {
                                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                {
                                                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                    respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                }

                                                else
                                                    respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonUtilities.WriteErrorLog(ex.Message);
                                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            }
                                            finally
                                            {
                                                if (respcust != string.Empty)
                                                {
                                                    System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                    outputcustXMLDoc.LoadXml(respcust);
                                                    foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                    {
                                                        string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                        if (statusSeveritycust == "Error")
                                                        {
                                                            string msg = "New Customer could not be created into QuickBooks \n ";
                                                            msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                            //MessageBox.Show(msg);
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(msg);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(msg);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                        }
                                    }

                                }

                                #endregion
                            }

                        }
                        #endregion

                        //Solution for BUG 633
                        //Create an Item
                        #region Add ItemFullName if not present
                        if (dt.Columns.Contains("ItemFullName"))
                        {
                            if (dr["ItemFullName"].ToString() != string.Empty)
                            {
                                //Code to check whether Item Name conatins ":"
                                string ItemName = dr["ItemFullName"].ToString();
                                string[] arr = new string[15];
                                if (ItemName.Contains(":"))
                                {
                                    arr = ItemName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ItemFullName"].ToString();
                                }

                                #region Setting SalesTaxCode and IsTaxIncluded
                                if (CommonUtilities.GetInstance().BtnPreview == false)
                                {
                                    if (defaultSettings == null)
                                    {
                                        CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        return null;

                                    }
                                }
                               
                                string TaxRateValue = string.Empty;
                                string IsTaxIncluded = string.Empty;
                                string netCost = string.Empty;
                                string ItemSaleTaxFullName = string.Empty;
                                //if default settings contain checkBoxGrossToNet checked.
                                if (CommonUtilities.GetInstance().BtnPreview == false)
                                {
                                    if (defaultSettings != null)
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                            {
                                                if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                                {
                                                    string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                                    //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                                    ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);


                                                    TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                                }
                                            }

                                            //validate IsTaxInluded value if present
                                            if (dt.Columns.Contains("IsTaxIncluded"))
                                            {
                                                if (dr["IsTaxIncluded"].ToString() != string.Empty && dr["IsTaxIncluded"].ToString() != "<None>")
                                                {
                                                    int result = 0;
                                                    if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                                    {
                                                        IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                                    }
                                                    else
                                                    {
                                                        string strvalid = string.Empty;
                                                        if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                                        {
                                                            IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                        }
                                                        else
                                                        {
                                                            IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                        }
                                                    }

                                                }
                                            }

                                            //Calculate cost
                                            if (dt.Columns.Contains("Cost"))
                                            {
                                                if (dr["Cost"].ToString() != string.Empty)
                                                {
                                                    decimal cost = 0;
                                                    if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                                    {
                                                        if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                                        {
                                                            decimal Cost;
                                                            if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                                            {
                                                                //Rate = 0;
                                                                netCost = dr["Cost"].ToString();
                                                            }
                                                            else
                                                            {
                                                                Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                                {
                                                                    //decimal TaxRate = 10;
                                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                                    cost = Cost / (1 + (TaxRate / 100));
                                                                }
                                                                netCost = Convert.ToString(Math.Round(cost, 5));
                                                            }
                                                        }
                                                    }

                                                    if (netCost == string.Empty)
                                                    {
                                                        netCost = dr["Cost"].ToString();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (dt.Columns.Contains("Cost"))
                                            {
                                                if (dr["Cost"].ToString() != string.Empty)
                                                {
                                                    netCost = dr["Cost"].ToString();
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion

                                #region Set Item Query

                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        #region Passing Items Query
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                        qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                        ItemQueryRq.SetAttribute("requestID", "1");

                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");


                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    ItemQueryRq.AppendChild(FullName);
                                                }

                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];                                           
                                            ItemQueryRq.AppendChild(FullName);
                                        }

                                        string pinput = pxmldoc.OuterXml;

                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }

                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {

                                            if (resp != string.Empty)
                                            {
                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                }
                                                outputXMLDoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                                {
                                                    //two types of when item is not prssent in QuickBook and we create item using hardcoded values
                                                    if (defaultSettings != null)
                                                    {
                                                        if (defaultSettings.Type == "NonInventoryPart")
                                                        {
                                                            #region Item NonInventory Add Query

                                                            XmlDocument ItemNonInvendoc = new XmlDocument();
                                                            ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                            ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                            
                                                            XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                            ItemNonInvendoc.AppendChild(qbXMLINI);
                                                            XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                            qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                            qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                            XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                            qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                            ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                            XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                            ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                            XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                            ININame.InnerText = arr[i];
                                                            ItemNonInventoryAdd.AppendChild(ININame);

                                                            //Solution for BUG 633
                                                            if (i > 0)
                                                            {
                                                                if (arr[i] != null && arr[i] != string.Empty)
                                                                {
                                                                    XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                                    for (a = 0; a <= i - 1; a++)
                                                                    {
                                                                        if (arr[a].Trim() != string.Empty)
                                                                        {
                                                                            INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                        }
                                                                    }
                                                                    if (INIChildFullName.InnerText != string.Empty)
                                                                    {
                                                                        //Adding Parent
                                                                        XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                        ItemNonInventoryAdd.AppendChild(INIParent);

                                                                        INIParent.AppendChild(INIChildFullName);
                                                                    }
                                                                }
                                                            }

                                                            //Adding Tax Code Element.
                                                            if (defaultSettings != null)
                                                            {
                                                                if (defaultSettings.TaxCode != string.Empty)
                                                                {
                                                                    if (defaultSettings.TaxCode.Length < 4)
                                                                    {
                                                                        XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                                        ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                                        XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                                        INIFullName.InnerText = defaultSettings.TaxCode;
                                                                        INISalesTaxCodeRef.AppendChild(INIFullName);
                                                                    }
                                                                }
                                                            }

                                                            XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                            bool IsPresent = false;
                                                            //Adding Desc And Price
                                                            //Solution for BUG 631 and 632
                                                            if (dt.Columns.Contains("Description"))
                                                            {
                                                                if (dr["Description"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement INIDesc = ItemNonInvendoc.CreateElement("Desc");
                                                                    INIDesc.InnerText = dr["Description"].ToString();
                                                                    INISalesAndPurchase.AppendChild(INIDesc);
                                                                    IsPresent = true;
                                                                }
                                                            }

                                                            if (dt.Columns.Contains("Cost"))
                                                            {
                                                                if (dr["Cost"].ToString() != string.Empty)
                                                                {
                                                                    XmlElement INIRate = ItemNonInvendoc.CreateElement("Price");
                                                                    INIRate.InnerText = netCost;
                                                                    INISalesAndPurchase.AppendChild(INIRate);
                                                                    IsPresent = true;
                                                                }
                                                            }

                                                            if (defaultSettings != null)
                                                            {
                                                                if (defaultSettings.IncomeAccount != string.Empty)
                                                                {
                                                                    XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                                    INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                                    XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                                   
                                                                    INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                                    INIIncomeAccountRef.AppendChild(INIAccountRefFullName);

                                                                    IsPresent = true;
                                                                }
                                                            }

                                                            if (IsPresent == true)
                                                            {
                                                                ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                            }

                                                            string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                                          
                                                            string respItemNonInvendoc = string.Empty;
                                                            try
                                                            {
                                                                
                                                                //Axis 10.2(bug no 66)
                                                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                                {
                                                                    respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                                }
                                                                else
                                                                {
                                                                    respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemNonInvendocinput);
                                                                }
                                                                //End Changes

                                                                System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                                outputXML.LoadXml(respItemNonInvendoc);
                                                                string StatusSeverity = string.Empty;
                                                                string statusMessage = string.Empty;
                                                                foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                                {
                                                                    StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                                    statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                                }
                                                                outputXML.RemoveAll();
                                                                if (StatusSeverity == "Error" || StatusSeverity == "Warn" || statusSeverity == "Warning")
                                                                {
                                                                    //Task 1435 (Axis 6.0):
                                                                    ErrorSummary summary = new ErrorSummary(statusMessage);
                                                                    summary.ShowDialog();
                                                                    CommonUtilities.WriteErrorLog(statusMessage);
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                CommonUtilities.WriteErrorLog(ex.Message);
                                                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                            }
                                                            string strtest2 = respItemNonInvendoc;

                                                        #endregion
                                                    }
                                                    else if (defaultSettings.Type == "Service")
                                                    {
                                                        #region Item Service Add Query

                                                        XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                        XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                        ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                        XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                        XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                        qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                        ItemServiceAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                    ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                    XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];                                                 
                                                    ItemServiceAdd.AppendChild(NameIS);
                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                ItemServiceAdd.AppendChild(INIParent);

                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }

                                                        //Adding Tax code Element.
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.TaxCode != string.Empty)
                                                            {
                                                                if (defaultSettings.TaxCode.Length < 4)
                                                                {
                                                                    XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                                    ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                                    XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                                    INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                                    INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                                }
                                                            }
                                                        }


                                                        XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                        bool IsPresent = false;
                                                      
                                                        //Solution for BUG 631 nad 632
                                                        //Adding Desc and Rate
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                ISSalesAndPurchase.AppendChild(ISDesc);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Cost"))
                                                        {
                                                            if (dr["Cost"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISRate = ItemServiceAdddoc.CreateElement("Price");
                                                                ISRate.InnerText = netCost;
                                                                ISSalesAndPurchase.AppendChild(ISRate);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.IncomeAccount != string.Empty)
                                                            {
                                                                XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                                ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                                //Adding IncomeAccount FullName.
                                                                XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                                ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                                ISIncomeAccountRef.AppendChild(ISFullName);

                                                                IsPresent = true;
                                                            }
                                                        }

                                                    if (IsPresent == true)
                                                    {
                                                        ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                    }
                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                  
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                       
                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemServiceAddinput);
                                                        }
                                                        //End Changes

                                                        
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                    ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                    XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                    NameIS.InnerText = arr[i];                                                  
                                                    ItemInventoryAdd.AppendChild(NameIS);

                                                    //Solution for BUG 633
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                //Adding Parent
                                                                XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                ItemInventoryAdd.AppendChild(INIParent);

                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }
                                                        //Adding Tax code Element. if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.TaxCode != string.Empty)
                                                            {
                                                                if (defaultSettings.TaxCode.Length < 4)
                                                                {
                                                                    XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                                    ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                                    XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                                    INIFullName.InnerText = defaultSettings.TaxCode;
                                                                    INISalesTaxCodeRef.AppendChild(INIFullName);
                                                                }
                                                            }
                                                        }

                                                    //Adding Desc for Item
                                                    //Solution for BUG 632
                                                    if (dt.Columns.Contains("Description"))
                                                    {
                                                        if (dr["Description"].ToString() != string.Empty)
                                                        {
                                                            XmlElement INIDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                            INIDesc.InnerText = dr["Description"].ToString();
                                                            ItemInventoryAdd.AppendChild(INIDesc);
                                                        }
                                                    }

                                                        //Adding SalesPrice for Item
                                                        if (dt.Columns.Contains("Cost"))
                                                        {
                                                            if (dr["Cost"].ToString() != string.Empty)
                                                            {
                                                                XmlElement INISalesPrice = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                                INISalesPrice.InnerText = netCost;
                                                                ItemInventoryAdd.AppendChild(INISalesPrice);
                                                            }
                                                        }
                                                        //Adding IncomeAccountRef
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.IncomeAccount != string.Empty)
                                                            {
                                                                XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                                ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                                XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                                INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                                INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                            }
                                                        }
                                                        if (defaultSettings != null)
                                                        {

                                                    //Adding COGSAccountRef
                                                    if (defaultSettings.COGSAccount != string.Empty)
                                                    {
                                                        XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                        ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                        XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                        INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                        INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                    }

                                                        }
                                                        //Adding AssetAccountRef
                                                        if (defaultSettings != null)
                                                        {
                                                            if (defaultSettings.AssetAccount != string.Empty)
                                                            {
                                                                XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                                ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                                XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                                INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                                INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                            }
                                                        }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemInventoryAddinput);
                                                        }
                                                        //End Changes


                                                            System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                            outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                            string StatusSeverity = string.Empty;
                                                            string statusMessage = string.Empty;
                                                            foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                            {
                                                                StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                                statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                            }
                                                            outputXML.RemoveAll();
                                                            if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                            {
                                                                //Task 1435 (Axis 6.0):
                                                                ErrorSummary summary = new ErrorSummary(statusMessage);
                                                                summary.ShowDialog();
                                                                CommonUtilities.WriteErrorLog(statusMessage);
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            CommonUtilities.WriteErrorLog(ex.Message);
                                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                        }
                                                        string strTest4 = respItemInventoryAddinputdoc;
                                                        #endregion
                                                    }
                                                }

                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                      }
                    
                        #endregion

                        #region Add PayeeEntityFullName(Vendor) if not present
                        if (dt.Columns.Contains("PayeeEntityFullName"))
                        {
                          if (dr["PayeeEntityFullName"].ToString() != string.Empty)
                          {
                                //Axis 617 
                                string[] CurrencyArr = new string[10];
                                if (dt.Columns.Contains("Currency"))
                                {
                                    if (dr["Currency"].ToString() != string.Empty)
                                    {
                                        string Currency = dr["Currency"].ToString();

                                        CurrencyArr[0] = dr["Currency"].ToString();
                                    }
                                }
                                //Axis 617 ends

                                #region Vendor Add Query
                                XmlDocument Vendordoc = new XmlDocument();
                            Vendordoc.AppendChild(Vendordoc.CreateXmlDeclaration("1.0", null, null));
                            Vendordoc.AppendChild(Vendordoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                            XmlElement VendorqbXML = Vendordoc.CreateElement("QBXML");
                            Vendordoc.AppendChild(VendorqbXML);
                            XmlElement VendorqbXMLMsgsRq = Vendordoc.CreateElement("QBXMLMsgsRq");
                            VendorqbXML.AppendChild(VendorqbXMLMsgsRq);
                            VendorqbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                            XmlElement VendorAddRq = Vendordoc.CreateElement("VendorAddRq");
                            VendorqbXMLMsgsRq.AppendChild(VendorAddRq);
                            VendorAddRq.SetAttribute("requestID", "1");

                            XmlElement VendorAdd = Vendordoc.CreateElement("VendorAdd");
                            VendorAddRq.AppendChild(VendorAdd);

                            XmlElement VName = Vendordoc.CreateElement("Name");
                            VName.InnerText = dr["PayeeEntityFullName"].ToString();
                            VendorAdd.AppendChild(VName);

                            #region Adding Vendor Address.

                            if ((dt.Columns.Contains("Addr1") || dt.Columns.Contains("Addr2")) || (dt.Columns.Contains("Addr3") || dt.Columns.Contains("Addr4")) || (dt.Columns.Contains("Addr5") || dt.Columns.Contains("City")) ||
                              (dt.Columns.Contains("State") || dt.Columns.Contains("PostalCode")) || (dt.Columns.Contains("Country") || dt.Columns.Contains("Note") || dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email")))
                            {
                                XmlElement vendorAddress = Vendordoc.CreateElement("VendorAddress");
                                VendorAdd.AppendChild(vendorAddress);
                                if (dt.Columns.Contains("Addr1"))
                                {

                                    if (dr["Addr1"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorAdd1 = Vendordoc.CreateElement("Addr1");
                                        VendorAdd1.InnerText = dr["Addr1"].ToString();
                                        vendorAddress.AppendChild(VendorAdd1);
                                    }
                                }
                                if (dt.Columns.Contains("Addr2"))
                                {
                                    if (dr["Addr2"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorAdd2 = Vendordoc.CreateElement("Addr2");
                                        VendorAdd2.InnerText = dr["Addr2"].ToString();
                                        vendorAddress.AppendChild(VendorAdd2);
                                    }
                                }
                                if (dt.Columns.Contains("Addr3"))
                                {
                                    if (dr["Addr3"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorAdd3 = Vendordoc.CreateElement("Addr3");
                                        VendorAdd3.InnerText = dr["Addr3"].ToString();
                                        vendorAddress.AppendChild(VendorAdd3);
                                    }
                                }
                                if (dt.Columns.Contains("Addr4"))
                                {
                                    if (dr["Addr4"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorAdd4 = Vendordoc.CreateElement("Addr4");
                                        VendorAdd4.InnerText = dr["Addr4"].ToString();
                                        vendorAddress.AppendChild(VendorAdd4);
                                    }

                                }
                                if (dt.Columns.Contains("Addr5"))
                                {
                                    if (dr["Addr5"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorAdd5 = Vendordoc.CreateElement("Addr5");
                                        VendorAdd5.InnerText = dr["Addr5"].ToString();
                                        vendorAddress.AppendChild(VendorAdd5);
                                    }
                                }
                                if (dt.Columns.Contains("City"))
                                {
                                    if (dr["City"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorCity = Vendordoc.CreateElement("City");
                                        VendorCity.InnerText = dr["City"].ToString();
                                        vendorAddress.AppendChild(VendorCity);
                                    }
                                }
                                if (dt.Columns.Contains("State"))
                                {
                                    if (dr["State"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorState = Vendordoc.CreateElement("State");
                                        VendorState.InnerText = dr["State"].ToString();
                                        vendorAddress.AppendChild(VendorState);
                                    }
                                }
                                if (dt.Columns.Contains("PostalCode"))
                                {
                                    if (dr["PostalCode"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorPostalCode = Vendordoc.CreateElement("PostalCode");
                                        VendorPostalCode.InnerText = dr["PostalCode"].ToString();
                                        vendorAddress.AppendChild(VendorPostalCode);
                                    }
                                }
                                if (dt.Columns.Contains("Country"))
                                {
                                    if (dr["Country"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorCountry = Vendordoc.CreateElement("Country");
                                        VendorCountry.InnerText = dr["Country"].ToString();
                                        vendorAddress.AppendChild(VendorCountry);
                                    }
                                }
                                if (dt.Columns.Contains("Note"))
                                {
                                    if (dr["Note"].ToString() != string.Empty)
                                    {
                                        XmlElement VendorNote = Vendordoc.CreateElement("Note");
                                        VendorNote.InnerText = dr["Note"].ToString();
                                        vendorAddress.AppendChild(VendorNote);
                                    }
                                }
                                if (dt.Columns.Contains("Phone"))
                                {
                                    if (dr["Phone"].ToString() != string.Empty)
                                    {
                                        XmlElement Phone = Vendordoc.CreateElement("Phone");
                                        Phone.InnerText = dr["Phone"].ToString();
                                        VendorAdd.AppendChild(Phone);
                                    }
                                }
                                if (dt.Columns.Contains("Fax"))
                                {
                                    if (dr["Fax"].ToString() != string.Empty)
                                    {
                                        XmlElement Fax = Vendordoc.CreateElement("Fax");
                                        Fax.InnerText = dr["Fax"].ToString();
                                        VendorAdd.AppendChild(Fax);
                                    }
                                }
                                if (dt.Columns.Contains("Email"))
                                {
                                    if (dr["Email"].ToString() != string.Empty)
                                    {
                                        XmlElement Email = Vendordoc.CreateElement("Email");
                                        Email.InnerText = dr["Email"].ToString();
                                        VendorAdd.AppendChild(Email);
                                    }
                                }

                            }

                                #endregion

                                //Axis 617 
                                #region Currency of Customer.  
                                if (CurrencyArr[0] != null && CurrencyArr[0] != "")
                                {
                                    XmlElement CurrencyRef = Vendordoc.CreateElement("CurrencyRef");
                                    XmlElement VendorFullName = Vendordoc.CreateElement("FullName");
                                    VendorFullName.InnerText = CurrencyArr[0];
                                    CurrencyRef.AppendChild(VendorFullName);
                                    VendorAdd.AppendChild(CurrencyRef);
                                }
                                #endregion
                                //Axis 617 ends

                                string Vendordocinput = Vendordoc.OuterXml;
                            string respVendorDoc = string.Empty;

                            try
                            {
                                //Axis 10.2(bug no 66)
                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                {
                                    respVendorDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, Vendordocinput);
                                }
                                else
                                {
                                    respVendorDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, Vendordocinput);
                                }
                                //End Changes 
                                
                                System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                outputXML.LoadXml(respVendorDoc);
                                string StatusSeverity = string.Empty;
                                string statusMessage = string.Empty;
                                foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/VendorAddRs"))
                                {
                                    StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                    statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                }
                                outputXML.RemoveAll();
                                if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                {
                                    //Task 1435 (Axis 6.0):
                                    //ErrorSummary summary = new ErrorSummary(statusMessage);
                                    //summary.ShowDialog();
                                    CommonUtilities.WriteErrorLog(statusMessage);
                                }
                            }
                            catch (Exception ex)
                            {
                                CommonUtilities.WriteErrorLog(ex.Message);
                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                            }

                            #endregion
                          }

                        }
                        #endregion

                        #region Add Accounts if not present

                        #region Adding ExpenseAccount (of type Expense) name in QuickBooks


                        if (dt.Columns.Contains("ExpenseAccountFullName"))
                        {
                            if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                            {

                                string AccountName = dr["ExpenseAccountFullName"].ToString();
                                string[] arr = new string[15];
                                if (AccountName.Contains(":"))
                                {
                                    arr = AccountName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ExpenseAccountFullName"].ToString();
                                }

                                #region Set Expense Account Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument xmldoc = new XmlDocument();
                                        xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
                                        xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = xmldoc.CreateElement("QBXML");
                                        xmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                        XmlElement AccountQueryRq = xmldoc.CreateElement("AccountQueryRq");
                                        qbXMLMsgsRq.AppendChild(AccountQueryRq);
                                        AccountQueryRq.SetAttribute("requestID", "1");


                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = xmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    AccountQueryRq.AppendChild(FullName);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = xmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            AccountQueryRq.AppendChild(FullName);
                                        }


                                        string XMLinput = xmldoc.OuterXml;
                                        string resp = string.Empty;

                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, XMLinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, XMLinput);
                                        }

                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {
                                                XmlDocument outputXMLdoc = new XmlDocument();
                                                outputXMLdoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (XmlNode Onode in outputXMLdoc.SelectNodes("/QBXML/QBXMLMsgsRs/AccountQueryRs"))
                                                {
                                                    statusSeverity = Onode.Attributes["statusSeverity"].Value.ToString();
                                                }

                                                outputXMLdoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                                {
                                                    #region Expense Account Add Query

                                                    XmlDocument ExpAccountdoc = new XmlDocument();
                                                    ExpAccountdoc.AppendChild(ExpAccountdoc.CreateXmlDeclaration("1.0", null, null));
                                                    ExpAccountdoc.AppendChild(ExpAccountdoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement ExpAccountqbXML = ExpAccountdoc.CreateElement("QBXML");
                                                    ExpAccountdoc.AppendChild(ExpAccountqbXML);
                                                    XmlElement ExpAccountqbXMLMsgsRq = ExpAccountdoc.CreateElement("QBXMLMsgsRq");
                                                    ExpAccountqbXML.AppendChild(ExpAccountqbXMLMsgsRq);
                                                    ExpAccountqbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                                    XmlElement ExpAccountAddRq = ExpAccountdoc.CreateElement("AccountAddRq");
                                                    ExpAccountqbXMLMsgsRq.AppendChild(ExpAccountAddRq);
                                                    ExpAccountAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ExpAccountAdd = ExpAccountdoc.CreateElement("AccountAdd");
                                                    ExpAccountAddRq.AppendChild(ExpAccountAdd);

                                                    XmlElement VName = ExpAccountdoc.CreateElement("Name");
                                                    VName.InnerText = arr[i];
                                                    ExpAccountAdd.AppendChild(VName);

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ExpAccountdoc.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = ExpAccountdoc.CreateElement("ParentRef");
                                                                // ExpAccountdoc.AppendChild(INIParent);
                                                                ExpAccountAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }



                                                    XmlElement ExpaccountType = ExpAccountdoc.CreateElement("AccountType");
                                                    ExpaccountType.InnerText = "Expense";
                                                    ExpAccountAdd.AppendChild(ExpaccountType);

                                                    string ExpAccountDocInput = ExpAccountdoc.OuterXml;
                                                    string respExpAccountDoc = string.Empty;

                                                    try
                                                    {
                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respExpAccountDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ExpAccountDocInput);
                                                        }
                                                        else
                                                        {
                                                            respExpAccountDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ExpAccountDocInput);
                                                        }
                                                        //End Changes
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respExpAccountDoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/AccountAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch { }
                                                    #endregion
                                                }
                                            }
                                        }

                                    }
                                   
                                }

                               
                            }
                        }

                        #endregion


                        #region Axis10.2 Adding Check Account (of type Bank) name in QuickBooks


                    if (dt.Columns.Contains("AccountFullName"))
                    {
                        if (dr["AccountFullName"].ToString() != string.Empty)
                        {

                            string AccountName = dr["AccountFullName"].ToString();
                            string[] arr = new string[15];
                            if (AccountName.Contains(":"))
                            {
                                arr = AccountName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["AccountFullName"].ToString();
                            }

                            #region Account Query
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int a = 0;
                                int item = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    XmlDocument xmldoc = new XmlDocument();
                                    xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
                                    xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = xmldoc.CreateElement("QBXML");
                                    xmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                    XmlElement AccountQueryRq = xmldoc.CreateElement("AccountQueryRq");
                                    qbXMLMsgsRq.AppendChild(AccountQueryRq);
                                    AccountQueryRq.SetAttribute("requestID", "1");


                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = xmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                AccountQueryRq.AppendChild(FullName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = xmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        AccountQueryRq.AppendChild(FullName);
                                    }


                                    string XMLinput = xmldoc.OuterXml;
                                    string resp = string.Empty;

                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, XMLinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, XMLinput);
                                    }

                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {
                                            XmlDocument outputXMLdoc = new XmlDocument();
                                            outputXMLdoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (XmlNode Onode in outputXMLdoc.SelectNodes("/QBXML/QBXMLMsgsRs/AccountQueryRs"))
                                            {
                                                statusSeverity = Onode.Attributes["statusSeverity"].Value.ToString();
                                            }

                                            outputXMLdoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn")
                                            {
                                                #region Bank Account QBXML Add Request

                                                XmlDocument ExpAccountdoc = new XmlDocument();
                                                ExpAccountdoc.AppendChild(ExpAccountdoc.CreateXmlDeclaration("1.0", null, null));
                                                ExpAccountdoc.AppendChild(ExpAccountdoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement ExpAccountqbXML = ExpAccountdoc.CreateElement("QBXML");
                                                ExpAccountdoc.AppendChild(ExpAccountqbXML);
                                                XmlElement ExpAccountqbXMLMsgsRq = ExpAccountdoc.CreateElement("QBXMLMsgsRq");
                                                ExpAccountqbXML.AppendChild(ExpAccountqbXMLMsgsRq);
                                                ExpAccountqbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                                XmlElement ExpAccountAddRq = ExpAccountdoc.CreateElement("AccountAddRq");
                                                ExpAccountqbXMLMsgsRq.AppendChild(ExpAccountAddRq);
                                                ExpAccountAddRq.SetAttribute("requestID", "1");

                                                XmlElement ExpAccountAdd = ExpAccountdoc.CreateElement("AccountAdd");
                                                ExpAccountAddRq.AppendChild(ExpAccountAdd);

                                                XmlElement VName = ExpAccountdoc.CreateElement("Name");
                                                VName.InnerText = arr[i];
                                                ExpAccountAdd.AppendChild(VName);

                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = ExpAccountdoc.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            XmlElement INIParent = ExpAccountdoc.CreateElement("ParentRef");
                                                          
                                                            ExpAccountAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }



                                                XmlElement ExpaccountType = ExpAccountdoc.CreateElement("AccountType");
                                                ExpaccountType.InnerText = "Bank";
                                                ExpAccountAdd.AppendChild(ExpaccountType);

                                                string ExpAccountDocInput = ExpAccountdoc.OuterXml;
                                                string respExpAccountDoc = string.Empty;

                                                try
                                                {
                                                    respExpAccountDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ExpAccountDocInput);
                                                    System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                    outputXML.LoadXml(respExpAccountDoc);
                                                    string StatusSeverity = string.Empty;
                                                    string statusMessage = string.Empty;
                                                    foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/AccountAddRs"))
                                                    {
                                                        StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                        statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    }
                                                    outputXML.RemoveAll();
                                                    if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                    {
                                                        //Task 1435 (Axis 6.0):
                                                        ErrorSummary summary = new ErrorSummary(statusMessage);
                                                        summary.ShowDialog();
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }

                                                #endregion

                                            }
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    #endregion

                        #endregion

                        #region COGS Account Add Query

                    XmlDocument COGSAccountdoc = new XmlDocument();
                    COGSAccountdoc.AppendChild(COGSAccountdoc.CreateXmlDeclaration("1.0", null, null));
                    COGSAccountdoc.AppendChild(COGSAccountdoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                    XmlElement COGSAccountqbXML = COGSAccountdoc.CreateElement("QBXML");
                    COGSAccountdoc.AppendChild(COGSAccountqbXML);
                    XmlElement COGSAccountqbXMLMsgsRq = COGSAccountdoc.CreateElement("QBXMLMsgsRq");
                    COGSAccountqbXML.AppendChild(COGSAccountqbXMLMsgsRq);
                    COGSAccountqbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                    XmlElement COGSAccountAddRq = COGSAccountdoc.CreateElement("AccountAddRq");
                    COGSAccountqbXMLMsgsRq.AppendChild(COGSAccountAddRq);
                    COGSAccountAddRq.SetAttribute("requestID", "1");

                    XmlElement COGSAccountAdd = COGSAccountdoc.CreateElement("AccountAdd");
                    COGSAccountAddRq.AppendChild(COGSAccountAdd);

                    XmlElement CName = COGSAccountdoc.CreateElement("Name");
                    CName.InnerText = dr["AccountFullName"].ToString();
                    COGSAccountAdd.AppendChild(CName);

                    XmlElement COGSaccountType = COGSAccountdoc.CreateElement("AccountType");
                    COGSaccountType.InnerText = "Cost of Goods Sold";
                    CName.AppendChild(COGSaccountType);

                    string COGSAccountDocInput = COGSAccountdoc.OuterXml;
                    string respCOGSAccountDoc = string.Empty;

                    try
                    {
                        respCOGSAccountDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, COGSAccountDocInput);
                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                        outputXML.LoadXml(respCOGSAccountDoc);
                        string StatusSeverity = string.Empty;
                        string statusMessage = string.Empty;
                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/AccountAddRs"))
                        {
                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                        }
                        outputXML.RemoveAll();
                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                        {
                            //Task 1435 (Axis 6.0):
                            ErrorSummary summary = new ErrorSummary(statusMessage);
                            summary.ShowDialog();
                            CommonUtilities.WriteErrorLog(statusMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }

                  #endregion
                        #endregion
                    }
                }
                else
                {
                    return null;
                }

            }
            #endregion

            return coll;
        }
    }
}
