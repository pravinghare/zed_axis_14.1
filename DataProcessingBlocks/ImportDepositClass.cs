using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportDepositClass
    {
        private static ImportDepositClass m_ImportDepositClass;
        public bool isIgnoreAll = false;

        #region Constuctor
        public ImportDepositClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import Deposit class
        /// </summary>
        /// <returns></returns>
        public static ImportDepositClass GetInstance()
        {
            if (m_ImportDepositClass == null)
                m_ImportDepositClass = new ImportDepositClass();
            return m_ImportDepositClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Deposit QuickBooks collection </returns>
        public DataProcessingBlocks.DepositQBEntryCollection ImportDepositData(string QBFileName, DataTable dt, ref string logDirectory)
        {
            DataProcessingBlocks.DepositQBEntryCollection coll = new DepositQBEntryCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    DepositQBEntry Deposit = new DepositQBEntry();
                    DateTime DepositDt = new DateTime();
                    string datevalue = string.Empty;
                    //For axis 9.0


                    if (dt.Columns.Contains("TxnID"))
                    {
                        #region 1st txnid

                        Deposit = coll.FindDepositEntry(dr["TxnID"].ToString());

                        if (Deposit == null)
                        {
                            Deposit = new DepositQBEntry();
                            #region Validations for TxnID
                            if (dr["TxnID"].ToString() != string.Empty)
                            {
                                if (dr["TxnID"].ToString().Length > 36)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnID ( " + dr["TxnID"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strtxnid = dr["TxnID"].ToString().Substring(0, 36);
                                            Deposit.TxnID = strtxnid;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strtxnid = dr["TxnID"].ToString();
                                            Deposit.TxnID = strtxnid;
                                        }
                                    }
                                    else
                                    {
                                        string strtxnid = dr["TxnID"].ToString();
                                        Deposit.TxnID = strtxnid;
                                    }

                                }
                                else
                                {
                                    string strtxnid = dr["TxnID"].ToString();

                                    Deposit.TxnID = strtxnid;
                                }
                            }

                            #endregion


                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out DepositDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;

                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Deposit.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Deposit.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Deposit.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            DepositDt = dttest;
                                            Deposit.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        DepositDt = Convert.ToDateTime(datevalue);
                                        Deposit.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("DepositToAccountRefFullName"))
                            {
                                #region Validations of DepositToAccountRef Full name
                                if (dr["DepositToAccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["DepositToAccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DepositToAccountRef full name (" + dr["DepositToAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                                if (Deposit.DepositToAccountRef.FullName == null)
                                                    Deposit.DepositToAccountRef.FullName = null;
                                                else
                                                    Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                                if (Deposit.DepositToAccountRef.FullName == null)
                                                    Deposit.DepositToAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                            if (Deposit.DepositToAccountRef.FullName == null)
                                                Deposit.DepositToAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                        if (Deposit.DepositToAccountRef.FullName == null)
                                            Deposit.DepositToAccountRef.FullName = null;
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0, 4095);
                                                Deposit.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                Deposit.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            Deposit.Memo = strMemo;
                                        }

                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Deposit.Memo = strMemo;
                                    }
                                }

                                #endregion
                            }

                            #region Adding CashBackInfo
                            DataProcessingBlocks.CashBackInfoAdd cashBackInfo = new CashBackInfoAdd();

                            if (dt.Columns.Contains("CashBackAccountRefFullName"))
                            {
                                #region Validations of CashBackAccountRefFullName
                                if (dr["CashBackAccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CashBackAccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashBackAccountRef Full name (" + dr["CashBackAccountRefFullName"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                                if (cashBackInfo.AccountRef.FullName == null)
                                                {
                                                    cashBackInfo.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                                if (cashBackInfo.AccountRef.FullName == null)
                                                {
                                                    cashBackInfo.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                            if (cashBackInfo.AccountRef.FullName == null)
                                            {
                                                cashBackInfo.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                        if (cashBackInfo.AccountRef.FullName == null)
                                        {
                                            cashBackInfo.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CashBackMemo"))
                            {
                                #region Validations for CashBackMemo
                                if (dr["CashBackMemo"].ToString() != string.Empty)
                                {
                                    if (dr["CashBackMemo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashBackInfo ( " + dr["CashBackMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["CashBackMemo"].ToString().Substring(0, 4095);
                                                cashBackInfo.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["CashBackMemo"].ToString();
                                                cashBackInfo.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["CashBackMemo"].ToString();
                                            cashBackInfo.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["CashBackMemo"].ToString();
                                        cashBackInfo.Memo = strMemo;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("CashBackAmount"))
                            {
                                #region Validations for CashBackAmount
                                if (dr["CashBackAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["CashBackAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CashBack amount ( " + dr["CashBackAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["CashBackAmount"].ToString();
                                                cashBackInfo.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["CashBackAmount"].ToString();
                                                cashBackInfo.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["CashBackAmount"].ToString();
                                            cashBackInfo.Amount = strAmount;
                                        }

                                    }
                                    else
                                    {
                                        cashBackInfo.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["CashBackAmount"].ToString()));
                                    }
                                }
                                #endregion
                            }

                            if (cashBackInfo.AccountRef != null || cashBackInfo.Amount != null || cashBackInfo.Memo != null)
                            {
                                Deposit.CashBankInfoAdd.Add(cashBackInfo);
                            }
                           
                            #endregion

                            if (dt.Columns.Contains("CurrencyRefFullName"))
                            {
                                #region Validations of Currency Full name
                                if (dr["CurrencyRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["CurrencyRefFullName"].ToString().Length > 64)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency full name (" + dr["CurrencyRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.CurrencyRef = new CurrencyRef(dr["CurrencyRefFullName"].ToString());
                                                if (Deposit.CurrencyRef.FullName == null)
                                                    Deposit.CurrencyRef = null;
                                                else
                                                    Deposit.CurrencyRef = new CurrencyRef(dr["CurrencyRefFullName"].ToString().Substring(0, 64));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.CurrencyRef = new CurrencyRef(dr["CurrencyRefFullName"].ToString());
                                                if (Deposit.CurrencyRef.FullName == null)
                                                    Deposit.CurrencyRef = null;
                                            }
                                        }
                                        else
                                        {
                                            Deposit.CurrencyRef = new CurrencyRef(dr["CurrencyRefFullName"].ToString());
                                            if (Deposit.CurrencyRef.FullName == null)
                                                Deposit.CurrencyRef = null;
                                        }
                                    }
                                    else
                                    {
                                        Deposit.CurrencyRef = new CurrencyRef(dr["CurrencyRefFullName"].ToString());
                                        if (Deposit.CurrencyRef.FullName == null)
                                            Deposit.CurrencyRef = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Deposit.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        //Deposit.ExchangeRate = string.Format("{0:00000000.00}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                        Deposit.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();

                                    }
                                }

                                #endregion
                            }

                            #region Adding Deposit Line

                            DataProcessingBlocks.DepositLineAdd DepositLine = new DepositLineAdd();

                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                                {
                                                    DepositLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                                {
                                                    DepositLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                            {
                                                DepositLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());

                                        if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                        {
                                            DepositLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("CheckNumber"))
                            {
                                #region Validations of checkNumber
                                if (dr["CheckNumber"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CheckNumber"].ToString();
                                    if (strCust.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CheckNumber (" + dr["CheckNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.CheckNumber = dr["CheckNumber"].ToString().Substring(0, 11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("EntityRefFullName"))
                            {
                                #region Validations of Entity Full name
                                if (dr["EntityRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["EntityRefFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Entity name (" + dr["EntityRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                                if (DepositLine.EntityRef.FullName == null)
                                                {
                                                    DepositLine.EntityRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString().Substring(0, 209));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                                if (DepositLine.EntityRef.FullName == null)
                                                {
                                                    DepositLine.EntityRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                            if (DepositLine.EntityRef.FullName == null)
                                            {
                                                DepositLine.EntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                        if (DepositLine.EntityRef.FullName == null)
                                        {
                                            DepositLine.EntityRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("AccountRefFullName"))
                            {
                                #region Validations of AccountRef Full name
                                if (dr["AccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["AccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AccountRef name (" + dr["AccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                                if (DepositLine.AccountRef.FullName == null)
                                                {
                                                    DepositLine.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                                if (DepositLine.AccountRef.FullName == null)
                                                {
                                                    DepositLine.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                            if (DepositLine.AccountRef.FullName == null)
                                            {
                                                DepositLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                        if (DepositLine.AccountRef.FullName == null)
                                        {
                                            DepositLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("PaymentMethodRefFullName"))
                            {
                                #region Validations of PaymentMethodRef Full name
                                if (dr["PaymentMethodRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["PaymentMethodRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentMethodRef name (" + dr["PaymentMethodRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                                if (DepositLine.PaymentMethodRef.FullName == null)
                                                {
                                                    DepositLine.PaymentMethodRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                                if (DepositLine.PaymentMethodRef.FullName == null)
                                                {
                                                    DepositLine.PaymentMethodRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (DepositLine.PaymentMethodRef.FullName == null)
                                            {
                                                DepositLine.PaymentMethodRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                        if (DepositLine.PaymentMethodRef.FullName == null)
                                        {
                                            DepositLine.PaymentMethodRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                DepositLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["Amount"].ToString();
                                                DepositLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            DepositLine.Amount = strAmount;
                                        }

                                    }
                                    else
                                    {

                                        DepositLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));

                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("DepositLineMemo"))
                            {
                                #region Validations for Memo
                                if (dr["DepositLineMemo"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineMemo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["DepositLineMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["DepositLineMemo"].ToString().Substring(0, 4095);
                                                DepositLine.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["DepositLineMemo"].ToString();
                                                DepositLine.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["DepositLineMemo"].ToString();
                                            DepositLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["DepositLineMemo"].ToString();
                                        DepositLine.Memo = strMemo;
                                    }
                                }

                                #endregion
                            }

                            if (DepositLine.AccountRef != null || DepositLine.PaymentMethodRef != null || DepositLine.Memo != null ||
                                     DepositLine.EntityRef != null || DepositLine.ClassRef != null ||
                                     DepositLine.Amount != null || DepositLine.CheckNumber != null)
                                Deposit.DepositLineAdd.Add(DepositLine);
                            coll.Add(Deposit);
                            #endregion
                        }
                        else
                        {
                            #region Adding Deposit Line

                            DataProcessingBlocks.DepositLineAdd DepositLine = new DepositLineAdd();

                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                                {
                                                    DepositLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                                if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                                {
                                                    DepositLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                            {
                                                DepositLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());

                                        if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                        {
                                            DepositLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("CheckNumber"))
                            {
                                #region Validations of checkNumber
                                if (dr["CheckNumber"].ToString() != string.Empty)
                                {
                                    string strCust = dr["CheckNumber"].ToString();
                                    if (strCust.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This CheckNumber (" + dr["CheckNumber"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.CheckNumber = dr["CheckNumber"].ToString().Substring(0, 11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("EntityRefFullName"))
                            {
                                #region Validations of Entity Full name
                                if (dr["EntityRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["EntityRefFullName"].ToString().Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Entity name (" + dr["EntityRefFullName"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                                if (DepositLine.EntityRef.FullName == null)
                                                {
                                                    DepositLine.EntityRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString().Substring(0, 209));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                                if (DepositLine.EntityRef.FullName == null)
                                                {
                                                    DepositLine.EntityRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                            if (DepositLine.EntityRef.FullName == null)
                                            {
                                                DepositLine.EntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                        if (DepositLine.EntityRef.FullName == null)
                                        {
                                            DepositLine.EntityRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("AccountRefFullName"))
                            {
                                #region Validations of AccountRef Full name
                                if (dr["AccountRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["AccountRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AccountRef name (" + dr["AccountRefFullName"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                                if (DepositLine.AccountRef.FullName == null)
                                                {
                                                    DepositLine.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString().Substring(0, 159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                                if (DepositLine.AccountRef.FullName == null)
                                                {
                                                    DepositLine.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                            if (DepositLine.AccountRef.FullName == null)
                                            {
                                                DepositLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                        if (DepositLine.AccountRef.FullName == null)
                                        {
                                            DepositLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("PaymentMethodRefFullName"))
                            {
                                #region Validations of PaymentMethodRef Full name
                                if (dr["PaymentMethodRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["PaymentMethodRefFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentMethodRef name (" + dr["PaymentMethodRefFullName"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                                if (DepositLine.PaymentMethodRef.FullName == null)
                                                {
                                                    DepositLine.PaymentMethodRef.FullName = null;
                                                }
                                                else
                                                {
                                                    DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                                if (DepositLine.PaymentMethodRef.FullName == null)
                                                {
                                                    DepositLine.PaymentMethodRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (DepositLine.PaymentMethodRef.FullName == null)
                                            {
                                                DepositLine.PaymentMethodRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                        if (DepositLine.PaymentMethodRef.FullName == null)
                                        {
                                            DepositLine.PaymentMethodRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Amount"))
                            {
                                #region Validations for Amount
                                if (dr["Amount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Amount ( " + dr["Amount"].ToString() + "  is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["Amount"].ToString();
                                                DepositLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["Amount"].ToString();
                                                DepositLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            DepositLine.Amount = strAmount;
                                        }

                                    }
                                    else
                                    {

                                        DepositLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));

                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("DepositLineMemo"))
                            {
                                #region Validations for Memo
                                if (dr["DepositLineMemo"].ToString() != string.Empty)
                                {
                                    if (dr["DepositLineMemo"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["DepositLineMemo"].ToString() + "  is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["DepositLineMemo"].ToString().Substring(0, 4095);
                                                DepositLine.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["DepositLineMemo"].ToString();
                                                DepositLine.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["DepositLineMemo"].ToString();
                                            DepositLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["DepositLineMemo"].ToString();
                                        DepositLine.Memo = strMemo;
                                    }
                                }

                                #endregion
                            }
                            Deposit.DepositLineAdd.Add(DepositLine);

                            #endregion
                        }

                        #endregion 1st txnid                        
                    }
                    else
                    {
                        #region existing txnid                  
                        if (dt.Columns.Contains("DepositToAccountRefFullName"))
                        {
                            #region Validations of DepositToAccountRef Full name
                            if (dr["DepositToAccountRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["DepositToAccountRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositToAccountRef full name (" + dr["DepositToAccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                            if (Deposit.DepositToAccountRef.FullName == null)
                                                Deposit.DepositToAccountRef.FullName = null;
                                            else
                                                Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString().Substring(0, 159));
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                            if (Deposit.DepositToAccountRef.FullName == null)
                                                Deposit.DepositToAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                        if (Deposit.DepositToAccountRef.FullName == null)
                                            Deposit.DepositToAccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    Deposit.DepositToAccountRef = new DepositToAccountRef(dr["DepositToAccountRefFullName"].ToString());
                                    if (Deposit.DepositToAccountRef.FullName == null)
                                        Deposit.DepositToAccountRef.FullName = null;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out DepositDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Deposit.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Deposit.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Deposit.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        DepositDt = dttest;
                                        Deposit.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    DepositDt = Convert.ToDateTime(datevalue);
                                    Deposit.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion
                        }

                        #region Adding CashBackInfo
                        DataProcessingBlocks.CashBackInfoAdd cashBackInfo = new CashBackInfoAdd();

                        if (dt.Columns.Contains("CashBackAccountRefFullName"))
                        {
                            #region Validations of CashBackAccountRefFullName
                            if (dr["CashBackAccountRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["CashBackAccountRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashBackAccountRef Full name (" + dr["CashBackAccountRefFullName"].ToString() + " is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                            if (cashBackInfo.AccountRef.FullName == null)
                                            {
                                                cashBackInfo.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                            if (cashBackInfo.AccountRef.FullName == null)
                                            {
                                                cashBackInfo.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                        if (cashBackInfo.AccountRef.FullName == null)
                                        {
                                            cashBackInfo.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    cashBackInfo.AccountRef = new AccountRef(string.Empty, dr["CashBackAccountRefFullName"].ToString());
                                    if (cashBackInfo.AccountRef.FullName == null)
                                    {
                                        cashBackInfo.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CashBackMemo"))
                        {
                            #region Validations for CashBackMemo
                            if (dr["CashBackMemo"].ToString() != string.Empty)
                            {
                                if (dr["CashBackMemo"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashBackInfo ( " + dr["CashBackMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["CashBackMemo"].ToString().Substring(0, 4095);
                                            cashBackInfo.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["CashBackMemo"].ToString();
                                            cashBackInfo.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["CashBackMemo"].ToString();
                                        cashBackInfo.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["CashBackMemo"].ToString();
                                    cashBackInfo.Memo = strMemo;
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("CashBackAmount"))
                        {
                            #region Validations for CashBackAmount
                            if (dr["CashBackAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["CashBackAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CashBack amount ( " + dr["CashBackAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["CashBackAmount"].ToString();
                                            cashBackInfo.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["CashBackAmount"].ToString();
                                            cashBackInfo.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["CashBackAmount"].ToString();
                                        cashBackInfo.Amount = strAmount;
                                    }

                                }
                                else
                                {
                                    cashBackInfo.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["CashBackAmount"].ToString()));
                                }
                            }
                            #endregion
                        }

                        if (cashBackInfo.AccountRef != null || cashBackInfo.Amount != null || cashBackInfo.Memo != null)
                        {
                            Deposit.CashBankInfoAdd.Add(cashBackInfo);
                        }

                        coll.Add(Deposit);

                        #endregion

                        #region Adding Deposit Line

                        DataProcessingBlocks.DepositLineAdd DepositLine = new DepositLineAdd();

                        if (dt.Columns.Contains("ClassRefFullName"))
                        {
                            #region Validations of Class Full name
                            if (dr["ClassRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["ClassRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Class name (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                            {
                                                DepositLine.ClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString().Substring(0, 159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                            if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                            {
                                                DepositLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                        if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                        {
                                            DepositLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    DepositLine.ClassRef = new ClassRef(string.Empty, dr["ClassRefFullName"].ToString());
                                    if (DepositLine.ClassRef.FullName == null && DepositLine.ClassRef.ListID == null)
                                    {
                                        DepositLine.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("CheckNumber"))
                        {
                            #region Validations of checkNumber
                            if (dr["CheckNumber"].ToString() != string.Empty)
                            {
                                string strCust = dr["CheckNumber"].ToString();
                                if (strCust.Length > 11)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This CheckNumber (" + dr["CheckNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLine.CheckNumber = dr["CheckNumber"].ToString().Substring(0, 11);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    DepositLine.CheckNumber = dr["CheckNumber"].ToString();
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("EntityRefFullName"))
                        {
                            #region Validations of Entity Full name
                            if (dr["EntityRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["EntityRefFullName"].ToString().Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Entity name (" + dr["EntityRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                            if (DepositLine.EntityRef.FullName == null)
                                            {
                                                DepositLine.EntityRef.FullName = null;
                                            }
                                            else
                                            {
                                                DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString().Substring(0, 209));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                            if (DepositLine.EntityRef.FullName == null)
                                            {
                                                DepositLine.EntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                        if (DepositLine.EntityRef.FullName == null)
                                        {
                                            DepositLine.EntityRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    DepositLine.EntityRef = new EntityRef(string.Empty, dr["EntityRefFullName"].ToString());
                                    if (DepositLine.EntityRef.FullName == null)
                                    {
                                        DepositLine.EntityRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("AccountRefFullName"))
                        {
                            #region Validations of AccountRef Full name
                            if (dr["AccountRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["AccountRefFullName"].ToString().Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AccountRef name (" + dr["AccountRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                            if (DepositLine.AccountRef.FullName == null)
                                            {
                                                DepositLine.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString().Substring(0, 159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                            if (DepositLine.AccountRef.FullName == null)
                                            {
                                                DepositLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                        if (DepositLine.AccountRef.FullName == null)
                                        {
                                            DepositLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    DepositLine.AccountRef = new AccountRef(string.Empty, dr["AccountRefFullName"].ToString());
                                    if (DepositLine.AccountRef.FullName == null)
                                    {
                                        DepositLine.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("PaymentMethodRefFullName"))
                        {
                            #region Validations of PaymentMethodRef Full name
                            if (dr["PaymentMethodRefFullName"].ToString() != string.Empty)
                            {
                                if (dr["PaymentMethodRefFullName"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentMethodRef name (" + dr["PaymentMethodRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (DepositLine.PaymentMethodRef.FullName == null)
                                            {
                                                DepositLine.PaymentMethodRef.FullName = null;
                                            }
                                            else
                                            {
                                                DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString().Substring(0, 31));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                            if (DepositLine.PaymentMethodRef.FullName == null)
                                            {
                                                DepositLine.PaymentMethodRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                        if (DepositLine.PaymentMethodRef.FullName == null)
                                        {
                                            DepositLine.PaymentMethodRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    DepositLine.PaymentMethodRef = new PaymentMethodRef(dr["PaymentMethodRefFullName"].ToString());
                                    if (DepositLine.PaymentMethodRef.FullName == null)
                                    {
                                        DepositLine.PaymentMethodRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Amount"))
                        {
                            #region Validations for Amount
                            if (dr["Amount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["Amount"].ToString();
                                            DepositLine.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["Amount"].ToString();
                                            DepositLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["Amount"].ToString();
                                        DepositLine.Amount = strAmount;
                                    }

                                }
                                else
                                {

                                    DepositLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["Amount"].ToString()));

                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("DepositLineMemo"))
                        {
                            #region Validations for Memo
                            if (dr["DepositLineMemo"].ToString() != string.Empty)
                            {
                                if (dr["DepositLineMemo"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DepositLineMemo ( " + dr["DepositLineMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["DepositLineMemo"].ToString().Substring(0, 4095);
                                            DepositLine.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["DepositLineMemo"].ToString();
                                            DepositLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["DepositLineMemo"].ToString();
                                        DepositLine.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["DepositLineMemo"].ToString();
                                    DepositLine.Memo = strMemo;
                                }
                            }
                            #endregion
                        }

                        if (DepositLine.AccountRef != null || DepositLine.PaymentMethodRef != null || DepositLine.Memo != null ||
                         DepositLine.EntityRef != null || DepositLine.ClassRef != null ||
                         DepositLine.Amount != null || DepositLine.CheckNumber != null)
                            Deposit.DepositLineAdd.Add(DepositLine);

                        coll.Add(Deposit);

                        #endregion

                        #endregion existing txnid
                    }
                }
                else
                {
                    return null;
                }

            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);

                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                        if (dt.Columns.Contains("EntityRefFullName"))
                        {
                            if (dr["EntityRefFullName"].ToString() != string.Empty)
                            {
                                string customerName = dr["EntityRefFullName"].ToString();
                                string[] arr = new string[15];
                                if (customerName.Contains(":"))
                                {
                                    arr = customerName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["EntityRefFullName"].ToString();
                                }
                                #region Set Customer Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                        qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                        CustomerQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    CustomerQueryRq.AppendChild(FullName);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            CustomerQueryRq.AppendChild(FullName);
                                        }
                                        
                                        string pinput = pxmldoc.OuterXml;
                                        //pxmldoc.Save("C://Test.xml");
                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {

                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                                }
                                                outputXMLDoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                                {
                                                    #region Customer Add Query

                                                    XmlDocument xmldocadd = new XmlDocument();
                                                    xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                    xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                    xmldocadd.AppendChild(qbXMLcust);
                                                    XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                    qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                    qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                    XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                    qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                    CustomerAddRq.SetAttribute("requestID", "1");
                                                    XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                    CustomerAddRq.AppendChild(CustomerAdd);
                                                    XmlElement Name = xmldocadd.CreateElement("Name");
                                                    Name.InnerText = arr[i];
                                                    CustomerAdd.AppendChild(Name);

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                                CustomerAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }
                                                    #region Adding Bill Address of Customer.
                                                    if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                    {
                                                        XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                        CustomerAdd.AppendChild(BillAddress);
                                                        if (dt.Columns.Contains("BillAddr1"))
                                                        {

                                                            if (dr["BillAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                                BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                                BillAddress.AppendChild(BillAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr2"))
                                                        {
                                                            if (dr["BillAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                                BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                                BillAddress.AppendChild(BillAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr3"))
                                                        {
                                                            if (dr["BillAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                                BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                                BillAddress.AppendChild(BillAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr4"))
                                                        {
                                                            if (dr["BillAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                                BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                                BillAddress.AppendChild(BillAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr5"))
                                                        {
                                                            if (dr["BillAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                                BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                                BillAddress.AppendChild(BillAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCity"))
                                                        {
                                                            if (dr["BillCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCity = xmldocadd.CreateElement("City");
                                                                BillCity.InnerText = dr["BillCity"].ToString();
                                                                BillAddress.AppendChild(BillCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillState"))
                                                        {
                                                            if (dr["BillState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillState = xmldocadd.CreateElement("State");
                                                                BillState.InnerText = dr["BillState"].ToString();
                                                                BillAddress.AppendChild(BillState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillPostalCode"))
                                                        {
                                                            if (dr["BillPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                                BillAddress.AppendChild(BillPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCountry"))
                                                        {
                                                            if (dr["BillCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                                BillCountry.InnerText = dr["BillCountry"].ToString();
                                                                BillAddress.AppendChild(BillCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillNote"))
                                                        {
                                                            if (dr["BillNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                                BillNote.InnerText = dr["BillNote"].ToString();
                                                                BillAddress.AppendChild(BillNote);
                                                            }
                                                        }
                                                    }


                                                    #endregion

                                                    #region Adding Ship Address of Customer.

                                                    if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                      (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                    {
                                                        XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                        CustomerAdd.AppendChild(ShipAddress);
                                                        if (dt.Columns.Contains("ShipAddr1"))
                                                        {

                                                            if (dr["ShipAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                                ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr2"))
                                                        {
                                                            if (dr["ShipAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                                ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr3"))
                                                        {
                                                            if (dr["ShipAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                                ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr4"))
                                                        {
                                                            if (dr["ShipAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                                ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr5"))
                                                        {
                                                            if (dr["ShipAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                                ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCity"))
                                                        {
                                                            if (dr["ShipCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                                ShipCity.InnerText = dr["ShipCity"].ToString();
                                                                ShipAddress.AppendChild(ShipCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipState"))
                                                        {
                                                            if (dr["ShipState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipState = xmldocadd.CreateElement("State");
                                                                ShipState.InnerText = dr["ShipState"].ToString();
                                                                ShipAddress.AppendChild(ShipState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipPostalCode"))
                                                        {
                                                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                                ShipAddress.AppendChild(ShipPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCountry"))
                                                        {
                                                            if (dr["ShipCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                                ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                                ShipAddress.AppendChild(ShipCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipNote"))
                                                        {
                                                            if (dr["ShipNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                                ShipNote.InnerText = dr["ShipNote"].ToString();
                                                                ShipAddress.AppendChild(ShipNote);
                                                            }
                                                        }
                                                    }



                                                    #endregion

                                                    if (dt.Columns.Contains("Phone") || dt.Columns.Contains("Fax") || dt.Columns.Contains("Email"))
                                                    {
                                                        if (dt.Columns.Contains("Phone"))
                                                        {
                                                            if (dr["Phone"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Phone = xmldocadd.CreateElement("Phone");
                                                                Phone.InnerText = dr["Phone"].ToString();
                                                                CustomerAdd.AppendChild(Phone);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Fax"))
                                                        {
                                                            if (dr["Fax"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Fax = xmldocadd.CreateElement("Fax");
                                                                Fax.InnerText = dr["Fax"].ToString();
                                                                CustomerAdd.AppendChild(Fax);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Email"))
                                                        {
                                                            if (dr["Email"].ToString() != string.Empty)
                                                            {
                                                                XmlElement Email = xmldocadd.CreateElement("Email");
                                                                Email.InnerText = dr["Email"].ToString();
                                                                CustomerAdd.AppendChild(Email);
                                                            }
                                                        }
                                                    }

                                                    string custinput = xmldocadd.OuterXml;
                                                    string respcust = string.Empty;
                                                    try
                                                    {
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                        }

                                                        else
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                    }

                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    finally
                                                    {
                                                        if (respcust != string.Empty)
                                                        {
                                                            System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                            outputcustXMLDoc.LoadXml(respcust);
                                                            foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                            {
                                                                string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                                if (statusSeveritycust == "Error")
                                                                {
                                                                    string msg = "New Customer could not be created into QuickBooks \n ";
                                                                    msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                    ErrorSummary summary = new ErrorSummary(msg);
                                                                    summary.ShowDialog();
                                                                    CommonUtilities.WriteErrorLog(oNodecust.Attributes["statusMessage"].Value.ToString());
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                            }

                                        }


                                    }

                                }
                                #endregion
                            }
                        }
                    }

                }
            }
           
            return coll;
        }
    }
}
