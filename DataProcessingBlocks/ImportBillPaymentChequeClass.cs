using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using System.ComponentModel;
using EDI.Constant;
using System.Collections;

namespace DataProcessingBlocks
{
    public class ImportBillPaymentChequeClass
    {
        private static ImportBillPaymentChequeClass m_ImportBillPaymentChequeClass;
        public bool isIgnoreAll = false;

        #region Constuctor
        public ImportBillPaymentChequeClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import Bill Payment Cheque class
        /// </summary>
        /// <returns></returns>
        public static ImportBillPaymentChequeClass GetInstance()
        {
            if (m_ImportBillPaymentChequeClass == null)
                m_ImportBillPaymentChequeClass = new ImportBillPaymentChequeClass();
            return m_ImportBillPaymentChequeClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Bill Payment Cheque QuickBooks collection </returns>
        public DataProcessingBlocks.BillPaymentCheckQBEntryCollection ImportBillPaymentChequeData(DataTable dt, ref string logDirectory)
        {
            DataProcessingBlocks.BillPaymentCheckQBEntryCollection coll = new BillPaymentCheckQBEntryCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                    DateTime BillDt = new DateTime();
                    string datevalue = string.Empty;
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Adding ref number

                        BillPaymentCheckQBEntry Bill = new BillPaymentCheckQBEntry();

                        Bill = coll.FindBillPaymentCheckQBEntry(dr["RefNumber"].ToString());
                        if (Bill == null)
                        {

                            Bill = new BillPaymentCheckQBEntry();
                            if (dt.Columns.Contains("PayeeEntityRefFullName"))
                            {
                                #region Validations of PayeeEntityRef Full Name
                                if (dr["PayeeEntityRefFullName"].ToString() != string.Empty)
                                {
                                    string strVendor = dr["PayeeEntityRefFullName"].ToString();
                                    if (strVendor.Length > 209)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PayeeEntityRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                                if (Bill.PayeeEntityRef.FullName == null)
                                                {
                                                    Bill.PayeeEntityRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString().Substring(0,209));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                                if (Bill.PayeeEntityRef.FullName == null)
                                                {
                                                    Bill.PayeeEntityRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                            if (Bill.PayeeEntityRef.FullName == null)
                                            {
                                                Bill.PayeeEntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                        if (Bill.PayeeEntityRef.FullName == null)
                                        {
                                            Bill.PayeeEntityRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("APAccountRefFullName"))
                            {
                                #region Validations of APAccount Full name
                                if (dr["APAccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strAPAcc = dr["APAccountRefFullName"].ToString();
                                    if (strAPAcc.Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This APAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());
                                                if (Bill.APAccountRef.FullName == null)
                                                {
                                                    Bill.APAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString().Substring(0,159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());
                                                if (Bill.APAccountRef.FullName == null)
                                                {
                                                    Bill.APAccountRef.FullName = null;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());
                                            if (Bill.APAccountRef.FullName == null)
                                            {
                                                Bill.APAccountRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());

                                        if (Bill.APAccountRef.FullName == null)
                                        {
                                            Bill.APAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out BillDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    Bill.TxnDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Bill.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                Bill.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            BillDt = dttest;
                                            Bill.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        BillDt = Convert.ToDateTime(datevalue);
                                        Bill.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }

                            if (dt.Columns.Contains("IsToBePrinted"))
                            {
                                #region Validations of IsToBePrinted
                                if (dr["IsToBePrinted"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                    {
                                        Bill.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                        {
                                            Bill.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                Bill.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    Bill.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    Bill.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                Bill.IsToBePrinted = dr["IsToBePrinted"].ToString();

                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    Bill.BillDate = BillDt;

                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 11)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Bill.RefNumber = dr["RefNumber"].ToString().Substring(0,11);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Bill.RefNumber = dr["RefNumber"].ToString();
                                        }


                                    }
                                    else
                                        Bill.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }

                            //for ApplyToBillRef
                            if (dt.Columns.Contains("ApplyToBillRef"))
                            {
                                #region Validations of ApplyToInvoiceRef
                                

                                if (dr["ApplyToBillRef"].ToString() != string.Empty)
                                {
                                
                                
                                


                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BankAccountRefFullName"))
                            {
                                #region Validations of BankAccountRef Full name
                                if (dr["BankAccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["BankAccountRefFullName"].ToString();
                                    if (strTerms.Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BankAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());
                                                if (Bill.BankAccountRef.FullName == null)
                                                {
                                                    Bill.BankAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString().Substring(0,159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());
                                                if (Bill.BankAccountRef.FullName == null)
                                                {
                                                    Bill.BankAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());
                                            if (Bill.BankAccountRef.FullName == null)
                                            {
                                                Bill.BankAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());

                                        if (Bill.BankAccountRef.FullName == null)
                                        {
                                            Bill.BankAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0,4000);
                                                Bill.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                Bill.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            Bill.Memo = strMemo;

                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Bill.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {

                                      //  Bill.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                      Bill.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                    }
                                }

                                #endregion

                            }

                            #region SetCredit Add

                            DataProcessingBlocks.SetCredit setCreditLine = new SetCredit();
                            if (dt.Columns.Contains("CreditTxnID"))
                            {
                                #region Validations of creditTxnID
                                if (dr["CreditTxnID"].ToString() != string.Empty)
                                {
                                    setCreditLine.CreditTxnID = (dr["CreditTxnID"].ToString());
                                    if (setCreditLine.CreditTxnID == null)
                                    {
                                        setCreditLine.CreditTxnID = null;
                                    }

                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AppliedAmount"))
                            {
                                #region Validations for AppliedAmount

                                if (dr["AppliedAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["AppliedAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AppliedAmount ( " + dr["AppliedAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["AppliedAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["AppliedAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["AppliedAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                    }
                                    else
                                    {

                                        setCreditLine.AppliedAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["AppliedAmount"].ToString())));

                                    }
                                }

                                #endregion

                            }
                            #endregion

                            #region AppliedToTxn Line Add
                            DataProcessingBlocks.AppliedToTxnAdd ExpLine = new AppliedToTxnAdd();
                            if (dt.Columns.Contains("AppliedToTxnID"))
                            {
                                #region Validations of AppliedToTxnID
                                if (dr["AppliedToTxnID"].ToString() != string.Empty)
                                {
                                    ExpLine.TxnID = (dr["AppliedToTxnID"].ToString());
                                    if (ExpLine.TxnID == null)
                                    {
                                        ExpLine.TxnID = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ApplyToBillRef"))
                            {
                                #region Validations of AppliedToBillRef
                                if (dr["ApplyToBillRef"].ToString() != string.Empty)
                                {
                                    Hashtable BillId = new Hashtable();
                                    BillId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("Bill", dr["ApplyToBillRef"].ToString(),CommonUtilities.GetInstance().CompanyFile);//growLabelQBCompanyFile.Text);
                                    if (BillId.Count == 0)
                                    {
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG126"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return null;
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in BillId)
                                        {
                                            ExpLine.TxnID = de.Value.ToString();   
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("PaymentAmount"))
                            {
                                #region Validations for PaymentAmount
                                if (dr["PaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentAmount ( " + dr["PaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["PaymentAmount"].ToString();
                                                ExpLine.PaymentAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["PaymentAmount"].ToString();
                                                ExpLine.PaymentAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["PaymentAmount"].ToString();
                                            ExpLine.PaymentAmount = strAmount;
                                        }
                                    }
                                    else
                                    {

                                        ExpLine.PaymentAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["PaymentAmount"].ToString())));

                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("DiscountAmount"))
                            {
                                #region Validations for DiscountAmount
                                if (dr["DiscountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAmount ( " + dr["DiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                ExpLine.DiscountAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                ExpLine.DiscountAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            ExpLine.DiscountAmount = strAmount;

                                        }
                                    }
                                    else
                                    {

                                        ExpLine.DiscountAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["DiscountAmount"].ToString())));

                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("DiscountAccountRefFullName"))
                            {
                                #region Validations of DiscountAccountRef Full name
                                if (dr["DiscountAccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["DiscountAccountRefFullName"].ToString();
                                    if (strTerms.Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                                if (ExpLine.DiscountAccountRef.FullName == null)
                                                {
                                                    ExpLine.DiscountAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString().Substring(0,159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                                if (ExpLine.DiscountAccountRef.FullName == null)
                                                {
                                                    ExpLine.DiscountAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                            if (ExpLine.DiscountAccountRef.FullName == null)
                                            {
                                                ExpLine.DiscountAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());

                                        if (ExpLine.DiscountAccountRef.FullName == null)
                                        {
                                            ExpLine.DiscountAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (ExpLine.TxnID != null || ExpLine.PaymentAmount != null || ExpLine.DiscountAmount != null ||
                                ExpLine.DiscountAccountRef != null)
                            {
                                if (setCreditLine.CreditTxnID != null || setCreditLine.AppliedAmount != null)
                                {
                                    ExpLine.SetCredit.Add(setCreditLine);
                                }
                                Bill.AppliedToTxnAdd.Add(ExpLine);
                            }

                            #endregion

                            coll.Add(Bill);
                        }
                        else
                        {

                            #region SetCredit Add

                            DataProcessingBlocks.SetCredit setCreditLine = new SetCredit();
                            if (dt.Columns.Contains("CreditTxnID"))
                            {
                                #region Validations of creditTxnID
                                if (dr["CreditTxnID"].ToString() != string.Empty)
                                {
                                    setCreditLine.CreditTxnID = (dr["CreditTxnID"].ToString());
                                    if (setCreditLine.CreditTxnID == null)
                                    {
                                        setCreditLine.CreditTxnID = null;
                                    }

                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AppliedAmount"))
                            {
                                #region Validations for AppliedAmount

                                if (dr["AppliedAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["AppliedAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AppliedAmount ( " + dr["AppliedAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["AppliedAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["AppliedAmount"].ToString();
                                                setCreditLine.AppliedAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["AppliedAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                    }
                                    else
                                    {

                                        setCreditLine.AppliedAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["AppliedAmount"].ToString())));

                                    }
                                }

                                #endregion

                            }
                            #endregion

                            #region AppliedToTxn Line Add
                            DataProcessingBlocks.AppliedToTxnAdd ExpLine = new AppliedToTxnAdd();
                            if (dt.Columns.Contains("AppliedToTxnID"))
                            {
                                #region Validations of AppliedToTxnID
                                if (dr["AppliedToTxnID"].ToString() != string.Empty)
                                {
                                    ExpLine.TxnID = (dr["AppliedToTxnID"].ToString());
                                    if (ExpLine.TxnID == null)
                                    {
                                        ExpLine.TxnID = null;
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("ApplyToBillRef"))
                            {
                                #region Validations of AppliedToBillRef
                                if (dr["ApplyToBillRef"].ToString() != string.Empty)
                                {
                                    Hashtable BillId = new Hashtable();
                                    BillId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("Bill", dr["ApplyToBillRef"].ToString(),CommonUtilities.GetInstance().CompanyFile);// growLabelQBCompanyFile.Text);
                                    if (BillId.Count == 0)
                                    {
                                        MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG126"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return null;
                                    }
                                    else
                                    {
                                        foreach (DictionaryEntry de in BillId)
                                        {
                                            ExpLine.TxnID = de.Value.ToString();
                                    }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("PaymentAmount"))
                            {
                                #region Validations for PaymentAmount
                                if (dr["PaymentAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["PaymentAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PaymentAmount ( " + dr["PaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["PaymentAmount"].ToString();
                                                ExpLine.PaymentAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["PaymentAmount"].ToString();
                                                ExpLine.PaymentAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["PaymentAmount"].ToString();
                                            ExpLine.PaymentAmount = strAmount;
                                        }
                                    }
                                    else
                                    {

                                        ExpLine.PaymentAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["PaymentAmount"].ToString())));

                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("DiscountAmount"))
                            {
                                #region Validations for DiscountAmount
                                if (dr["DiscountAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["DiscountAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAmount ( " + dr["DiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                ExpLine.DiscountAmount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["DiscountAmount"].ToString();
                                                ExpLine.DiscountAmount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            ExpLine.DiscountAmount = strAmount;

                                        }
                                    }
                                    else
                                    {

                                        ExpLine.DiscountAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["DiscountAmount"].ToString())));

                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("DiscountAccountRefFullName"))
                            {
                                #region Validations of DiscountAccountRef Full name
                                if (dr["DiscountAccountRefFullName"].ToString() != string.Empty)
                                {
                                    string strTerms = dr["DiscountAccountRefFullName"].ToString();
                                    if (strTerms.Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DiscountAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                                if (ExpLine.DiscountAccountRef.FullName == null)
                                                {
                                                    ExpLine.DiscountAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString().Substring(0,159));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                                if (ExpLine.DiscountAccountRef.FullName == null)
                                                {
                                                    ExpLine.DiscountAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                            if (ExpLine.DiscountAccountRef.FullName == null)
                                            {
                                                ExpLine.DiscountAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());

                                        if (ExpLine.DiscountAccountRef.FullName == null)
                                        {
                                            ExpLine.DiscountAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (ExpLine.TxnID != null || ExpLine.PaymentAmount != null || ExpLine.DiscountAmount != null ||
                                ExpLine.DiscountAccountRef != null)
                            {
                                if (setCreditLine.CreditTxnID != null || setCreditLine.AppliedAmount != null)
                                {
                                    ExpLine.SetCredit.Add(setCreditLine);
                                }
                                Bill.AppliedToTxnAdd.Add(ExpLine);
                            }

                            #endregion

                        }


                        #endregion
                    }
                    else
                    {

                        BillPaymentCheckQBEntry Bill = new BillPaymentCheckQBEntry();

                        #region Without Adding ref number
                        if (dt.Columns.Contains("PayeeEntityRefFullName"))
                        {
                            #region Validations of PayeeEntityRef Full Name
                            if (dr["PayeeEntityRefFullName"].ToString() != string.Empty)
                            {
                                string strVendor = dr["PayeeEntityRefFullName"].ToString();
                                if (strVendor.Length > 209)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PayeeEntityRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;

                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                            if (Bill.PayeeEntityRef.FullName == null)
                                            {
                                                Bill.PayeeEntityRef.FullName = null;
                                            }
                                            else
                                            {
                                                Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString().Substring(0,209));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                            if (Bill.PayeeEntityRef.FullName == null)
                                            {
                                                Bill.PayeeEntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                        if (Bill.PayeeEntityRef.FullName == null)
                                        {
                                            Bill.PayeeEntityRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Bill.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityRefFullName"].ToString());
                                    if (Bill.PayeeEntityRef.FullName == null)
                                    {
                                        Bill.PayeeEntityRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("APAccountRefFullName"))
                        {
                            #region Validations of APAccount Full name
                            if (dr["APAccountRefFullName"].ToString() != string.Empty)
                            {
                                string strAPAcc = dr["APAccountRefFullName"].ToString();
                                if (strAPAcc.Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This APAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());
                                            if (Bill.APAccountRef.FullName == null)
                                            {
                                                Bill.APAccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString().Substring(0,159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());
                                            if (Bill.APAccountRef.FullName == null)
                                            {
                                                Bill.APAccountRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());
                                        if (Bill.APAccountRef.FullName == null)
                                        {
                                            Bill.APAccountRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    Bill.APAccountRef = new APAccountRef(dr["APAccountRefFullName"].ToString());

                                    if (Bill.APAccountRef.FullName == null)
                                    {
                                        Bill.APAccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out BillDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                Bill.TxnDate = datevalue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            Bill.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        BillDt = dttest;
                                        Bill.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    BillDt = Convert.ToDateTime(datevalue);
                                    Bill.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }

                        if (dt.Columns.Contains("IsToBePrinted"))
                        {
                            #region Validations of IsToBePrinted
                            if (dr["IsToBePrinted"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsToBePrinted"].ToString(), out result))
                                {
                                    Bill.IsToBePrinted = Convert.ToInt32(dr["IsToBePrinted"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsToBePrinted"].ToString().ToLower() == "true")
                                    {
                                        Bill.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsToBePrinted"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            Bill.IsToBePrinted = dr["IsToBePrinted"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsToBePrinted (" + dr["IsToBePrinted"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                Bill.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                Bill.IsToBePrinted = dr["IsToBePrinted"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Bill.IsToBePrinted = dr["IsToBePrinted"].ToString();

                                        }
                                    }

                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref Number
                            if (datevalue != string.Empty)
                                Bill.BillDate = BillDt;

                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["RefNumber"].ToString();
                                if (strRefNum.Length > 11)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Bill.RefNumber = dr["RefNumber"].ToString().Substring(0,11);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Bill.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Bill.RefNumber = dr["RefNumber"].ToString();
                                    }


                                }
                                else
                                    Bill.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }

                        //for ApplyToBillRef
                        if (dt.Columns.Contains("ApplyToBillRef"))
                        {
                            #region Validations of ApplyToInvoiceRef
                           



                            #endregion
                        }


                        if (dt.Columns.Contains("BankAccountRefFullName"))
                        {
                            #region Validations of BankAccountRef Full name
                            if (dr["BankAccountRefFullName"].ToString() != string.Empty)
                            {
                                string strTerms = dr["BankAccountRefFullName"].ToString();
                                if (strTerms.Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This BankAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());
                                            if (Bill.BankAccountRef.FullName == null)
                                            {
                                                Bill.BankAccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString().Substring(0,159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());
                                            if (Bill.BankAccountRef.FullName == null)
                                            {
                                                Bill.BankAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());
                                        if (Bill.BankAccountRef.FullName == null)
                                        {
                                            Bill.BankAccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    Bill.BankAccountRef = new BankAccountRef(dr["BankAccountRefFullName"].ToString());

                                    if (Bill.BankAccountRef.FullName == null)
                                    {
                                        Bill.BankAccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString().Substring(0,4000);
                                            Bill.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            Bill.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        Bill.Memo = strMemo;

                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    Bill.Memo = strMemo;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            Bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            Bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        Bill.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {

                                   // Bill.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                 Bill.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();
                                }
                            }

                            #endregion

                        }

                        #region SetCredit Add

                        DataProcessingBlocks.SetCredit setCreditLine = new SetCredit();
                        if (dt.Columns.Contains("CreditTxnID"))
                        {
                            #region Validations of creditTxnID
                            if (dr["CreditTxnID"].ToString() != string.Empty)
                            {
                                setCreditLine.CreditTxnID = (dr["CreditTxnID"].ToString());
                                if (setCreditLine.CreditTxnID == null)
                                {
                                    setCreditLine.CreditTxnID = null;
                                }

                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("AppliedAmount"))
                        {
                            #region Validations for AppliedAmount

                            if (dr["AppliedAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["AppliedAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This AppliedAmount ( " + dr["AppliedAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["AppliedAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["AppliedAmount"].ToString();
                                            setCreditLine.AppliedAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["AppliedAmount"].ToString();
                                        setCreditLine.AppliedAmount = strAmount;
                                    }
                                }
                                else
                                {

                                    setCreditLine.AppliedAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["AppliedAmount"].ToString())));

                                }
                            }

                            #endregion

                        }
                        #endregion

                        #region AppliedToTxn Line Add
                        DataProcessingBlocks.AppliedToTxnAdd ExpLine = new AppliedToTxnAdd();
                        if (dt.Columns.Contains("AppliedToTxnID"))
                        {
                            #region Validations of AppliedToTxnID
                            if (dr["AppliedToTxnID"].ToString() != string.Empty)
                            {
                                ExpLine.TxnID = (dr["AppliedToTxnID"].ToString());
                                if (ExpLine.TxnID == null)
                                {
                                    ExpLine.TxnID = null;
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("ApplyToBillRef"))
                        {
                            #region Validations of AppliedToBillRef
                            if (dr["ApplyToBillRef"].ToString() != string.Empty)
                            {
                                Hashtable BillId = new Hashtable();
                                BillId = CommonUtilities.GetInstance().CheckAndGetRefNoExistInQucikBookForApplyToInvoiceRef("Bill", dr["ApplyToBillRef"].ToString(), CommonUtilities.GetInstance().CompanyFile);//growLabelQBCompanyFile.Text);
                                if (BillId.Count == 0)
                                {
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG126"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return null;
                                }
                                else
                                {
                                    foreach (DictionaryEntry de in BillId)
                                    {
                                        ExpLine.TxnID = de.Value.ToString();
                                }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("PaymentAmount"))
                        {
                            #region Validations for PaymentAmount
                            if (dr["PaymentAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["PaymentAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PaymentAmount ( " + dr["PaymentAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["PaymentAmount"].ToString();
                                            ExpLine.PaymentAmount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["PaymentAmount"].ToString();
                                            ExpLine.PaymentAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["PaymentAmount"].ToString();
                                        ExpLine.PaymentAmount = strAmount;
                                    }
                                }
                                else
                                {

                                    ExpLine.PaymentAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["PaymentAmount"].ToString())));

                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("DiscountAmount"))
                        {
                            #region Validations for DiscountAmount
                            if (dr["DiscountAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["DiscountAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountAmount ( " + dr["DiscountAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            ExpLine.DiscountAmount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["DiscountAmount"].ToString();
                                            ExpLine.DiscountAmount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["DiscountAmount"].ToString();
                                        ExpLine.DiscountAmount = strAmount;

                                    }
                                }
                                else
                                {

                                    ExpLine.DiscountAmount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["DiscountAmount"].ToString())));

                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("DiscountAccountRefFullName"))
                        {
                            #region Validations of DiscountAccountRef Full name
                            if (dr["DiscountAccountRefFullName"].ToString() != string.Empty)
                            {
                                string strTerms = dr["DiscountAccountRefFullName"].ToString();
                                if (strTerms.Length > 159)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DiscountAccountRef fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                            if (ExpLine.DiscountAccountRef.FullName == null)
                                            {
                                                ExpLine.DiscountAccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString().Substring(0,159));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                            if (ExpLine.DiscountAccountRef.FullName == null)
                                            {
                                                ExpLine.DiscountAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());
                                        if (ExpLine.DiscountAccountRef.FullName == null)
                                        {
                                            ExpLine.DiscountAccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.DiscountAccountRef = new DiscountAccountRef(dr["DiscountAccountRefFullName"].ToString());

                                    if (ExpLine.DiscountAccountRef.FullName == null)
                                    {
                                        ExpLine.DiscountAccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        if (ExpLine.TxnID != null || ExpLine.PaymentAmount != null || ExpLine.DiscountAmount != null ||
                            ExpLine.DiscountAccountRef != null)
                        {
                            if (setCreditLine.CreditTxnID != null || setCreditLine.AppliedAmount != null)
                            {
                                ExpLine.SetCredit.Add(setCreditLine);
                            }
                            Bill.AppliedToTxnAdd.Add(ExpLine);
                        }

                        #endregion

                        coll.Add(Bill);
                        #endregion
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                }
            }

            return coll;
        }
    }
}
