// ==============================================================================================
// 
// InvoiceQBEntry.cs
//
// This file contains the implementations of the Invoice Qb Entry private members , 
// Properties, Constructors and Methods for QuickBooks Invoice Entry Imports.
//         Invoice Qb Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping fields (IsFinanceCharge and ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;


namespace DataProcessingBlocks
{
    [XmlRootAttribute("InVoiceQBEntry", Namespace = "", IsNullable = false)]
    public class InvoiceQBEntry
    {
        #region  Private Member Variable
        private CustomerRef m_CustomerRef;
        /// <summary>
        ///bug 442 11.4
        /// </summary>
        private CurrencyRef m_CurrencyRef;
        private ClassRef m_ClassRef;
        private ARAccountRef m_ARAccountRef;
        private TemplateRef m_TemplateRef;
        private string m_TxnDate;
        private string m_RefNumber;
        //invoice mapping version 6.0
        //private string m_ApplyToInvoiceRef;
        private Collection<BillAddress> m_BillAddress = new Collection<BillAddress>();
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        private string m_IsPending;
        private string m_IsFinanceCharge;
        private string m_PONumber;
        private TermsRef m_TermsRef;
        private string m_DueDate;
        private SalesRepRef m_SalesRepRef;
        private string m_FOB;
        private string m_ShipDate;
        private ShipMethodRef m_ShipMethodRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_Memo;
        private CustomerMsgRef m_CustomerMsgRef;
        private string m_IsToBePrinted;
        private string m_IsToBeEmailed;
        private string m_IsTaxIncluded;
        private CustomerSalesTaxCodeRef m_CustomerSalesTaxCodeRef;
        private string m_Others;
        private string m_ExchangeRate;
        private string m_LinkToTxnID;
        private Collection<InvoiceLineAdd> m_InvoiceLineAdd = new Collection<InvoiceLineAdd>();
        //Axis 10.0
        private Collection<DiscountLineAdd> m_DiscountLineAdd = new Collection<DiscountLineAdd>();
        private Collection<ShippingLineAdd> m_ShippingLineAdd = new Collection<ShippingLineAdd>();

        // private Collection<InvoiceLineGroupAdd> m_InvoiceLineGroupAdd = new Collection<InvoiceLineGroupAdd>();

        //changes in version 6.0
        private Collection<InvoiceLineGroupAdd> m_InvoiceLineGroupAdd = new Collection<InvoiceLineGroupAdd>();

        private string m_IncludeRetElement;
        private DateTime m_InvoiceDate;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;     



        #endregion

        #region Construtor

        public InvoiceQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }
        /// <summary>
        /// 11.4  bug no 442
        /// </summary>
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public ARAccountRef ARAccountRef
        {
            get { return m_ARAccountRef; }
            set { m_ARAccountRef = value; }
        }

        public TemplateRef TemplateRef
        {
            get { return m_TemplateRef; }
            set { m_TemplateRef = value; }
        }


     


        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }

            }
            set
            {
                this.m_TxnDate = value;
            }
        }

        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }




        [XmlArray("BillAddressREM")]
        public Collection<BillAddress> BillAddress
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }


        public string IsPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }

        public string IsFinanceCharge
        {
            get { return m_IsFinanceCharge; }
            set { m_IsFinanceCharge = value; }
        }

        public string PONumber
        {
            get { return m_PONumber; }
            set { m_PONumber = value; }
        }

        public TermsRef TermsRef
        {
            get { return m_TermsRef; }
            set { m_TermsRef = value; }
        }
        [XmlElement(DataType = "string")]
        public string DueDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }
        }

        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }
        [XmlElement(DataType = "string")]
        public string ShipDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_ShipDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_ShipDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_ShipDate = value;
            }
        }

        public ShipMethodRef ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }

        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return m_ItemSalesTaxRef; }
            set { m_ItemSalesTaxRef = value; }
        }


        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public CustomerMsgRef CustomerMsgRef
        {
            get { return m_CustomerMsgRef; }
            set { m_CustomerMsgRef = value; }
        }

        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }


        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public CustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get { return m_CustomerSalesTaxCodeRef; }
            set { m_CustomerSalesTaxCodeRef = value; }
        }

        public string Other
        {
            get { return m_Others; }
            set { m_Others = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }


        public string LinkToTxnID
        {
            get { return m_LinkToTxnID; }
            set { m_LinkToTxnID = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        [XmlArray("InvoiceLineAddREM")]
        public Collection<InvoiceLineAdd> InvoiceLineAdd
        {
            get { return m_InvoiceLineAdd; }
            set { m_InvoiceLineAdd = value; }
        }

        //new changes in version 6.0
        [XmlArray("InvoiceLineGroupAddREM")]
        public Collection<InvoiceLineGroupAdd> InvoiceLineGroupAdd
        {
            get { return m_InvoiceLineGroupAdd; }
            set { m_InvoiceLineGroupAdd = value; }
        }



        [XmlIgnoreAttribute()]
        public string IncludeRetElement
        {
            get { return m_IncludeRetElement; }
            set { m_IncludeRetElement = value; }
        }
        [XmlIgnoreAttribute()]
        public DateTime InvoiceDate
        {
            get { return m_InvoiceDate; }
            set { m_InvoiceDate = value; }
        }

        // Axis 10.0

        [XmlArray("DiscountLineAddREM")]
        public Collection<DiscountLineAdd> DiscountLineAdd
        {
            get
            {
                return m_DiscountLineAdd;
            }
            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {

                    m_DiscountLineAdd = value;
                }
            }
        }

        [XmlArray("ShippingLineAddREM")]
        public Collection<ShippingLineAdd> ShippingLineAdd
        {
            get { return m_ShippingLineAdd; }

            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    m_ShippingLineAdd = value;
                }
            }
        }
        #endregion

        #region Public Methods
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {

                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InvoiceQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();
            try
            {
                System.IO.File.Delete(fileName);

            }
            catch (Exception ex)
            {
               // throw;
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));


            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceAddRq = requestXmlDoc.CreateElement("InvoiceAddRq");
            inner.AppendChild(InvoiceAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceAdd = requestXmlDoc.CreateElement("InvoiceAdd");

            InvoiceAddRq.AppendChild(InvoiceAdd);
            requestXML = requestXML.Replace("<InvoiceLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<InvoiceLineGroupAdd />", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<InvoiceLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineGroupAddREM>", string.Empty);

            
            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            
                requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
                requestXML = requestXML.Replace("<DiscountLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("</DiscountLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);
                requestXML = requestXML.Replace("<ShippingLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("</ShippingLineAddREM>", string.Empty);

                requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
                requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
                requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
                requestXML = requestXML.Replace("<DataExt />", string.Empty);

                requestXML = requestXML.Replace("<LinkToTxnREM />", string.Empty);
                requestXML = requestXML.Replace("<LinkToTxnREM>", string.Empty);
                requestXML = requestXML.Replace("</LinkToTxnREM>", string.Empty);
                requestXML = requestXML.Replace("<LinkToTxn />", string.Empty);
            
            InvoiceAdd.InnerXml = requestXML;

            //For add request id to track error messages
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/Phone");
                    InvoiceAdd.RemoveChild(childNode);
                }
            }
            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/CurrencyRef");
                    InvoiceAdd.RemoveChild(childNode);
                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/InventorySiteLocationRef");
                    node.ParentNode.RemoveChild(node);
                    //  requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                    node.RemoveAll();
                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/Fax");
                    InvoiceAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/Email");
                    InvoiceAdd.RemoveChild(childNode);
                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }
            }


            if (requeststring != string.Empty)
                InvoiceAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                InvoiceAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            //For TxnID.Remove LinkToTxn
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/InvoiceLineAdd"))
            {
                if (oNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/LinkToTxnID");
                    if(node != null)
                    InvoiceAdd.RemoveChild(node);

                    XmlNode node1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/InvoiceLineAdd/ItemRef");
                    if (node1 != null)
                    {
                        node1.ParentNode.RemoveChild(node1);
                        node1.RemoveAll();
                    }
                }
            }
            //For Remove LinkToPurchaseOrder
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/InvoiceLineAdd"))
            {
                if (oNode.SelectSingleNode("LinkToSalesOrder") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/InvoiceLineAdd/LinkToSalesOrder");
                    if (node != null)
                    {
                        node.ParentNode.RemoveChild(node);
                        node.RemoveAll();
                    }

                }
            }


            //try
            //{
            //    //For LinkToTxnID : 
            //    for (int index = 0; index < this.InvoiceLineAdd.Count; index++)
            //    {
            //        if (this.InvoiceLineAdd[index].LinkToTxn.TxnID != null)
            //        {
            //            InvoiceAdd.SetAttribute("defMacro", "TxnID:" + this.InvoiceLineAdd[index].LinkToTxn.TxnID);
            //        }
            //    }
            //}
            //catch { }

            //Bug#1493 Resolved(Axis6.0).
            if (Convert.ToInt32(CommonUtilities.GetInstance().MajorVersion.Trim()) <= 18)
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
                {
                    if (oNode.SelectSingleNode("IsFinanceCharge") != null)
                    {
                        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/IsFinanceCharge");
                        node.ParentNode.RemoveChild(node);
                    }
                }
            }
            //Bug 966
            if (Convert.ToInt32(Convert.ToDouble(CommonUtilities.GetInstance().SuportedVersion)) < 8.0)
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
                {
                    if (oNode.SelectSingleNode("IsFinanceCharge") != null)
                    {
                        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/IsFinanceCharge");
                        node.ParentNode.RemoveChild(node);
                    }
                }
            }

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    if (CommonUtilities.GetInstance().QBSessionTicket == null || CommonUtilities.GetInstance().QBSessionTicket == "")
                    {
                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                        //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    }
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                            
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceAddRs/InvoiceRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInvoice(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Invoice ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Invoice Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create InvoiceQueryRq  aggregate and fill in field values for it
            XmlElement InvoiceQueryRq = requestXmlDoc.CreateElement("InvoiceQueryRq");
            inner.AppendChild(InvoiceQueryRq);

            //Create Refno aggregate and fill in field values for it
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            InvoiceQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            InvoiceQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating Invoice information
        /// of existing Invoice with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateInvoiceInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {

                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InvoiceQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceQBEntryModRq = requestXmlDoc.CreateElement("InvoiceModRq");
            inner.AppendChild(InvoiceQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceMod = requestXmlDoc.CreateElement("InvoiceMod");
            InvoiceQBEntryModRq.AppendChild(InvoiceMod);

            requestXML = requestXML.Replace("<InvoicePerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoicePerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineAddREM />", string.Empty);

            requestXML = requestXML.Replace("<InvoiceLineAdd>", "<InvoiceLineMod>");
            requestXML = requestXML.Replace("</InvoiceLineAdd>", "</InvoiceLineMod>");
            requestXML = requestXML.Replace("<InvoiceLineAdd />", string.Empty);


            requestXML = requestXML.Replace("<InvoiceLineGroupAdd>", "<InvoiceLineGroupMod>");
            requestXML = requestXML.Replace("</InvoiceLineGroupAdd>", "</InvoiceLineGroupMod>");
            requestXML = requestXML.Replace("<InvoiceLineGroupAdd />", string.Empty);


            requestXML = requestXML.Replace("<InvoiceLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<InvoiceLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineGroupAddREM>", string.Empty);

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            requestXML = requestXML.Replace("<LinkToTxnREM />", string.Empty);
            requestXML = requestXML.Replace("<LinkToTxnREM>", string.Empty);
            requestXML = requestXML.Replace("</LinkToTxnREM>", string.Empty);
            requestXML = requestXML.Replace("<LinkToTxn />", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            //Axis 10.2 bug no 70
            requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);
            
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");

            InvoiceMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/Phone");
                    InvoiceMod.RemoveChild(childNode);
                }
            }

            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/CurrencyRef");
                    InvoiceMod.RemoveChild(childNode);
                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/Fax");
                    InvoiceMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/Email");
                    InvoiceMod.RemoveChild(childNode);
                }
            }

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("LinkToTxnID") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/LinkToTxnID");
                    InvoiceMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InventorySiteLocationRef");

                    requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                InvoiceQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By InvoiceRefNumber) : " + rowcount.ToString());
            else
                InvoiceQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod"))
            {
                if (oNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod/LinkToTxn");
                    childNode.ParentNode.RemoveChild(childNode);
                    childNode.RemoveAll();
                }
            }

            //Bug#1493 Resolved(Axis6.0).
            if (Convert.ToInt32(CommonUtilities.GetInstance().MajorVersion.Trim()) <= 18)
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
                {
                    if (oNode.SelectSingleNode("IsFinanceCharge") != null)
                    {
                        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/IsFinanceCharge");
                        node.ParentNode.RemoveChild(node);
                    }
                }
            }
            //Bug 966
            if (Convert.ToInt32(Convert.ToDouble(CommonUtilities.GetInstance().SuportedVersion)) < 8.0)
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
                {
                    if (oNode.SelectSingleNode("IsFinanceCharge") != null)
                    {
                        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/IsFinanceCharge");
                        node.ParentNode.RemoveChild(node);
                    }
                }
            }


            //For Remove LinkToSalesOrder
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod"))
            {
                if (oNode.SelectSingleNode("LinkToSalesOrder") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod/LinkToSalesOrder");
                    if (node != null)
                    {
                        node.ParentNode.RemoveChild(node);
                        node.RemoveAll();
                    }

                }
            }


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {

                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceModRs/InvoiceRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInvoice(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        //Bug No.412
        public bool AppendInvoiceInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {

                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.InvoiceQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceQBEntryModRq = requestXmlDoc.CreateElement("InvoiceModRq");
            inner.AppendChild(InvoiceQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement InvoiceMod = requestXmlDoc.CreateElement("InvoiceMod");
            InvoiceQBEntryModRq.AppendChild(InvoiceMod);

            // Code for getting myList count  of TxnLineID
                int Listcnt = 0;
                foreach(var item in txnLineIDList)
                {
                    if (item != null)
                    {
                        Listcnt++;
                    }
                }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
			//Axis 706
            List<string> GroupTxnLineID = new List<string>();
            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                if (!txnLineIDList[i].Contains("GroupTxnLineID_"))
				{
                    addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></InvoiceLineMod><InvoiceLineMod>";


                    XmlNode InvoiceLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod");
                    System.Xml.XmlElement InvoiceLineGroupMod = requestXmlDoc.CreateElement("InvoiceLineMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = txnLineIDList[i];
                    InvoiceLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(InvoiceLineGroupMod, InvoiceLineMod.ChildNodes[i==0?i:i-1]);
                }
                else
                {
                    string GroupTxnID = txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty);
                    GroupTxnLineID.Add(txnLineIDList[i].Replace("GroupTxnLineID_", string.Empty));
                    XmlNode InvoiceMod1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod");
                    //XmlNode InvoiceLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod");
                    System.Xml.XmlElement InvoiceLineGroupMod = requestXmlDoc.CreateElement("InvoiceLineGroupMod");
                    System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                    TxnLineID.InnerText = GroupTxnID;
                    InvoiceLineGroupMod.AppendChild(TxnLineID);
                    requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(InvoiceLineGroupMod, InvoiceMod1.ChildNodes[i == 0 ? i : i - 1]);
                }
            }
            
            XmlNode InvoiceMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod");

            System.Xml.XmlElement InvoiceAdd = requestXmlDoc.CreateElement("InvoiceLine");

               
            requestXML = requestXML.Replace("<InvoicePerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoicePerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineAddREM />", string.Empty);

            requestXML = requestXML.Replace("<InvoiceLineAdd>", "<InvoiceLineMod>");
            requestXML = requestXML.Replace("</InvoiceLineAdd>", "</InvoiceLineMod>");
            requestXML = requestXML.Replace("<InvoiceLineAdd />", string.Empty);


            requestXML = requestXML.Replace("<InvoiceLineGroupAdd>", "<InvoiceLineGroupMod>");
            requestXML = requestXML.Replace("</InvoiceLineGroupAdd>", "</InvoiceLineGroupMod>");
            requestXML = requestXML.Replace("<InvoiceLineGroupAdd />", string.Empty);


            requestXML = requestXML.Replace("<InvoiceLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<InvoiceLineGroupAddREM />", string.Empty);
            requestXML = requestXML.Replace("<InvoiceLineGroupAddREM>", string.Empty);
            requestXML = requestXML.Replace("</InvoiceLineGroupAddREM>", string.Empty);

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            //Axis 10.2 bug no 70
            requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);

            //bug 470
            requestXML = requestXML.Replace("<LinkToTxnREM />", string.Empty);
			//Axis 706
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ItemGroupRef>", "<TxnLineID>-1</TxnLineID><ItemGroupRef>");

            string[] InvoiceLineModFragment;

            int indexInvoiceLine = requestXML.IndexOf(@"<InvoiceLineMod>");
            int indexInvoiceLineGroup = requestXML.IndexOf(@"<InvoiceLineGroupMod>");

            if(indexInvoiceLineGroup<=0 ||  indexInvoiceLine < indexInvoiceLineGroup)
            {
                InvoiceLineModFragment = requestXML.Split(new string[] { "<InvoiceLineMod>" }, StringSplitOptions.None);
            }
            else
            {
                InvoiceLineModFragment = requestXML.Split(new string[] { "<InvoiceLineGroupMod>" }, StringSplitOptions.None);
            }

            if(InvoiceLineModFragment.Length>0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = InvoiceLineModFragment[0];


                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").FirstChild;

              
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertBefore(xfrag1, firstChild1);

                requestXML= requestXML.Replace(InvoiceLineModFragment[0], string.Empty);

              //  requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(xfrag1, InvoiceMod2.LastChild);

            }



            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
              requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(xfrag, InvoiceMod2.LastChild);

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/Phone");
                    InvoiceMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/Fax");
                    InvoiceMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/Email");
                    InvoiceMod.RemoveChild(childNode);
                }
            }
            //remove ApplyToInvoiceRef
            //foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
            //{
            //    if (oNode.SelectSingleNode("ApplyToInvoiceRef") != null)
            //    {
            //        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/ApplyToInvoiceRef");
            //        InvoiceMod.RemoveChild(childNode);
            //    }
            //}
            ////requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();


            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/InventorySiteLocationRef");
                    //  node.ParentNode.RemoveChild(node);
                    //  node.RemoveAll();
                    requeststring = oNode.SelectSingleNode("InventorySiteLocationRef").InnerText.ToString();
                }

            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                InvoiceQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By InvoiceRefNumber) : " + rowcount.ToString());
            else
                InvoiceQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            //For TxnID.Remove LinkToTxn
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod"))
            {
                if (oNode.SelectSingleNode("LinkToTxn") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/InvoiceLineMod/LinkToTxn");
                    node.ParentNode.RemoveChild(node);

                }
            }

            //Bug#1493 Resolved(Axis6.0).
            if (Convert.ToInt32(CommonUtilities.GetInstance().MajorVersion.Trim()) <= 18)
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod"))
                {
                    if (oNode.SelectSingleNode("IsFinanceCharge") != null)
                    {
                        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod/IsFinanceCharge");
                        node.ParentNode.RemoveChild(node);
                    }
                }
            }
            //Bug 966
            if (Convert.ToInt32(Convert.ToDouble(CommonUtilities.GetInstance().SuportedVersion)) < 8.0)
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd"))
                {
                    if (oNode.SelectSingleNode("IsFinanceCharge") != null)
                    {
                        XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceAddRq/InvoiceAdd/IsFinanceCharge");
                        node.ParentNode.RemoveChild(node);
                    }
                }
            }

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/InvoiceModRq/InvoiceMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {

                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/InvoiceModRs/InvoiceRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofInvoice(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion
    }


    public class InVoiceQBEntryCollection : Collection<InvoiceQBEntry>
    {
        /// <summary>
        /// /// This method is used for getting existing Invoice date, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public InvoiceQBEntry FindInvoiceEntry(DateTime date)
        {
            foreach (InvoiceQBEntry item in this)
            {
                if (item.InvoiceDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        public static int inv_cnt = 0;
        public static string inv_number = "";
        /// <summary>
        /// This method is used for getting existing invoice refnumber, If not exists then it return null.
        /// </summary>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public InvoiceQBEntry FindInvoiceEntry(string refNumber)
        {
            inv_number = refNumber;
            foreach (InvoiceQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    inv_cnt++;
                    return item;
                }
            }
            inv_cnt = 0;
            return null;
        }
        /// This method is used for getting existing invoice date, refnumber, If not exists then it return null.
        public InvoiceQBEntry FindInvoiceEntry(DateTime date, string refNumber)
        {
            foreach (InvoiceQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.InvoiceDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }



    [XmlRootAttribute("InvoiceLineAdd", Namespace = "", IsNullable = false)]
    public class InvoiceLineAdd
    {
        #region Private Member Variables

        private ItemRef m_ItemRef;
        private string m_Desc;
        private string m_Quantity;

           
        private string m_UnitOfMeasure;
        //601
        private PriceLevelRef m_PriceLevelFullName;
        private string m_Rate;
        //private string m_RatePercent;
        private ClassRef m_ClassRef;
        private string m_Amount;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;

        private string m_ServiceDate;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_istaxable;
        //private PriceLevelRef m_PriceLevelRef;
        private OverrideItemAccountRef m_OverrideAccountRef;
        private string m_Other1;
        private string m_Other2;
        private Collection<LinkToTxn> m_LinkToTxn = new Collection<LinkToTxn>();
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();
        private string m_SerialNumber;
        private string m_LotNumber;
        //Axis 11.4
        private string m_LinkToSalesOrder;
        #endregion

        #region  Constructors

        public InvoiceLineAdd()
        {

        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        //601
        public PriceLevelRef PriceLevelRef
        {
            get { return m_PriceLevelFullName; }
            set { m_PriceLevelFullName = value; }
        }


        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
                //this.m_RatePercent = null;
            }
        }

       
        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

       
        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }

        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set
            {
                this.m_InventorySiteLocationRef = value;
            }
        }

        [XmlElement(DataType = "string")]
        public string ServiceDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_ServiceDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_ServiceDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_ServiceDate = value;
            }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

       

        public string IsTaxable
        {
            get { return this.m_istaxable; }
            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    this.m_istaxable = value;
                }
            }
        }


        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideAccountRef; }
            set { this.m_OverrideAccountRef = value; }
        }

        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        public string Other2
        {
            get { return this.m_Other2; }
            set { this.m_Other2 = value; }
        }
        /// <summary>
        /// 11.4 bug no 444
        /// </summary>
        [XmlArray("LinkToTxnREM")]
        public Collection<LinkToTxn> LinkToTxn
        {
            get { return m_LinkToTxn; }
            set { m_LinkToTxn = value; }
        }

        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { this.m_SerialNumber = value; }
        }

        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }

        [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }
        //11.4
        public string LinkToSalesOrder
        {
            get { return m_LinkToSalesOrder; }
            set { m_LinkToSalesOrder = value; }
        }
        #endregion
    }

    //changes in version 6.0
    /// <summary>
    /// creating new Line for invoice
    /// </summary>
    [XmlRootAttribute("InvoiceLineGroupAdd", Namespace = "", IsNullable = false)]
    public class InvoiceLineGroupAdd
    {
        #region Private Member variable
        private ItemGroupRef m_ItemGroupRef;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        //Bug no. 331
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();
        //end Bug no. 331
        #endregion

        #region constructor

        public InvoiceLineGroupAdd()
        {

        } 
        #endregion

        #region Public properties

        public ItemGroupRef ItemGroupRef
        {
            get { return this.m_ItemGroupRef; }
            set { this.m_ItemGroupRef = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        //Bug no. 331
        [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }
        //Bug no. 331
        #endregion
    }

}