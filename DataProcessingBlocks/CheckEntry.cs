// ==============================================================================================
// 
// CheckEntry.cs
//
// This file contains the implementations of the Cheque Entry private members , 
// Properties, Constructors and Methods for QuickBooks Cheque Entry Imports.
//         Cheque Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping field (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using DataProcessingBlocks.CheckModule;
using EDI.Constant;

namespace DataProcessingBlocks.CheckModule
{
    [XmlRootAttribute("CheckEntry", Namespace = "", IsNullable = false)]
    public class CheckEntry
    {
        #region  Private Member Variable
        private AccountRef m_AccountRef;
        private PayeeEntityRef m_PayeeEntityRef;
        //Axis 617 
        private CurrencyRef m_CurrencyRef;
        //Axis 617 ends

        private string m_RefNumber;
        private string m_TxnDate;
        private string m_Memo;
        private Collection<Address> m_Address = new Collection<Address>();
        private string m_IsToBePrinted;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_ExchangeRate;
        //P Axis 13.1 : issue 689
        private Collection<ApplyCheckToTxnAdd> m_ApplyCheckToTxnAdd = new Collection<ApplyCheckToTxnAdd>();

        private Collection<ExpenseLineAdd> m_ExpenseLineAdd = new Collection<ExpenseLineAdd>();
        private Collection<ItemLineAdd> m_ItemLineAdd = new Collection<ItemLineAdd>();
        
        private DateTime m_CheckDate;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;
        private DataExt m_DataExt;


        #endregion

        #region Constructors

        public CheckEntry()
        {

        }

        #endregion

        #region Public Properties

        public AccountRef AccountRef
        {
            get { return this.m_AccountRef; }
            set { this.m_AccountRef = value; }
        }

        public PayeeEntityRef PayeeEntityRef
        {
            get { return this.m_PayeeEntityRef; }
            set { this.m_PayeeEntityRef = value; }
        }

        //Axis 617 
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        //Axis 617 ends
        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }
               
      
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }
        [XmlArray("AddressREM")]
        public Collection<Address> Address
        {
            get { return this.m_Address; }
            set { this.m_Address = value; }
        }

        public string IsToBePrinted
        {
            get { return this.m_IsToBePrinted; }
            set { this.m_IsToBePrinted = value; }
        }

        public string IsTaxIncluded
        {
            get { return this.m_IsTaxIncluded; }
            set { this.m_IsTaxIncluded = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public string ExchangeRate
        {
            get { return this.m_ExchangeRate; }
            set { this.m_ExchangeRate = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        //P Axis 13.1 : issue 689
        [XmlArray("ApplyCheckToTxnAddREM")]
        public Collection<ApplyCheckToTxnAdd> ApplyCheckToTxnAdd
        {
            get { return m_ApplyCheckToTxnAdd; }
            set { m_ApplyCheckToTxnAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime CheckDate
        {
            get
            {
                try
                {
                    if (Convert.ToDateTime(this.m_CheckDate) <= DateTime.MinValue)
                        return this.m_CheckDate;
                    else

                        return this.m_CheckDate;
                }
                catch
                {
                    return this.m_CheckDate;
                }
            }
            set { this.m_CheckDate = value; }
        }



        [XmlArray("ExpenseLineAddREM")]
        public Collection<ExpenseLineAdd> ExpenseLineAdd
        {
            get { return m_ExpenseLineAdd; }
            set { m_ExpenseLineAdd = value; }
        }

        [XmlArray("ItemLineAddREM")]
        public Collection<ItemLineAdd> ItemLineAdd
        {
            get { return m_ItemLineAdd; }
            set { m_ItemLineAdd = value; }
        }

        #endregion

        #region Public Methods

        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                } 
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CheckModule.CheckEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create CheckAddRq aggregate and fill in field values for it
            System.Xml.XmlElement CheckAddRq = requestXmlDoc.CreateElement("CheckAddRq");
            inner.AppendChild(CheckAddRq);

            //Create CheckEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement CheckAdd = requestXmlDoc.CreateElement("CheckAdd");

            CheckAddRq.AppendChild(CheckAdd);

            //P Axis 13.1 : issue 689
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ApplyCheckToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM />", string.Empty);
            //requestXML = requestXML.Replace("</ApplyCheckToTxnAdd>", "</ApplyCheckToTxnMod>");
            //requestXML = requestXML.Replace("<ApplyCheckToTxnAdd>", "<ApplyCheckToTxnMod>");
            //requestXML = requestXML.Replace("<ApplyCheckToTxnAdd/>", "<ApplyCheckToTxnMod/>");

            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);

            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);

            requestXML = requestXML.Replace("<AddressREM>", string.Empty);
            requestXML = requestXML.Replace("</AddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AddressREM />", string.Empty);

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);


            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);


            CheckAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/Phone");
                    if(childNode != null)
                    CheckAdd.RemoveChild(childNode);
                }
            }
            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/CurrencyRef");
                    if (childNode != null)
                        CheckAdd.RemoveChild(childNode);
                }
            }
            //Axis 617 ends
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/Fax");
                    if(childNode != null)
                    CheckAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/Email");
                    if(childNode != null)
                    CheckAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (CommonUtilities.GetInstance().IsStatementFeature())
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/ExpenseLineAdd"))
                {
                    if (oNode.SelectSingleNode("CustomerRef") == null)
                    {
                        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/ExpenseLineAdd/BillableStatus");
                        if(childNode != null)
                        oNode.RemoveChild(childNode);
                    }
                }
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/ItemLineAdd"))
                {
                    if (oNode.SelectSingleNode("CustomerRef") == null)
                    {
                        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/ItemLineAdd/BillableStatus");
                        if(childNode != null)
                        oNode.RemoveChild(childNode);
                    }
                }
            }
            if (requeststring != string.Empty)
                CheckAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());
            else
                CheckAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CheckAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CheckAddRs/CheckRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCheck(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Check ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="Check Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create CheckQueryRq aggregate and fill in field values for it
            XmlElement CheckQueryRq = requestXmlDoc.CreateElement("CheckQueryRq");
            inner.AppendChild(CheckQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            CheckQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            CheckQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {              
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating Check information
        /// of existing Check with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateCheckInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CheckModule.CheckEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement CheckQBEntryModRq = requestXmlDoc.CreateElement("CheckModRq");
            inner.AppendChild(CheckQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement CheckMod = requestXmlDoc.CreateElement("CheckMod");
            CheckQBEntryModRq.AppendChild(CheckMod);

            //P Axis 13.1 : issue 689
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ApplyCheckToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM />", string.Empty);
            requestXML = requestXML.Replace("</ApplyCheckToTxnAdd>", "</ApplyCheckToTxnMod>");
            requestXML = requestXML.Replace("<ApplyCheckToTxnAdd>", "<ApplyCheckToTxnMod>");
            requestXML = requestXML.Replace("<ApplyCheckToTxnAdd/>", "<ApplyCheckToTxnMod/>");

            requestXML = requestXML.Replace("<CheckPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</CheckPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<AddressREM>", string.Empty);
            requestXML = requestXML.Replace("</AddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AddressREM />", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<CheckLineAdd>","<CheckLineMod>");
            requestXML = requestXML.Replace("</CheckLineAdd>","</CheckLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");

            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);


            CheckMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/Phone");
                    CheckMod.RemoveChild(childNode);
                }
            }

            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/CurrencyRef");
                    if (childNode != null)
                        CheckMod.RemoveChild(childNode);
                }
            }
            //Axis 617 ends

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/Fax");
                    CheckMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/Email");
                    CheckMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                CheckQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By CheckRefNumber) : " + rowcount.ToString());
            else
                CheckQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());
            if (CommonUtilities.GetInstance().IsStatementFeature())
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ExpenseLineMod"))
                {
                    if (oNode.SelectSingleNode("CustomerRef") == null)
                    {
                        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ExpenseLineMod/BillableStatus");
                        oNode.RemoveChild(childNode);
                    }
                }
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ItemLineMod"))
                {
                    if (oNode.SelectSingleNode("CustomerRef") == null)
                    {
                        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ItemLineMod/BillableStatus");
                        oNode.RemoveChild(childNode);
                    }
                }
            }


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CheckModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CheckModRs/CheckRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCheck(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

//Bug No. 412
        /// <summary>
        /// This method is used for append data to existing transaction 
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        /// 
        public bool AppendCheckInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CheckModule.CheckEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement CheckQBEntryModRq = requestXmlDoc.CreateElement("CheckModRq");
            inner.AppendChild(CheckQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement CheckMod = requestXmlDoc.CreateElement("CheckMod");
            CheckQBEntryModRq.AppendChild(CheckMod);


            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
            //

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.

            string[] request = requestXML.Split(new string[] { "</ItemRef>" }, StringSplitOptions.None);
            string resultString = "";
            string subResultString = "";
            int stringCnt = 1;
            int subStringCnt = 1;
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></ItemLineMod><ItemLineMod>";
            }
            for (int i = 0; i < request.Length; i++)
            {
                if (Listcnt != 0)
                {
                    if (subStringCnt==1)
                    {
                   // subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></CheckLineMod><CheckLineMod><TxnLineID>-1</TxnLineID><ItemRef>");
                    subResultString = request[i].Replace("<ItemRef>", addString + "<TxnLineID>-1</TxnLineID><ItemRef>");
                    subStringCnt++;
                    }
                    else
                    {
                        subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                    }
                }
                else
                {
                    subResultString = request[i].Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
                }

                if (stringCnt == request.Length)
                {
                    resultString += subResultString;
                }
                else
                {
                    resultString += subResultString + "</ItemRef>";
                }
                stringCnt++;
                Listcnt--;
            }
            requestXML = resultString;

            //P Axis 13.1 : issue 689
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ApplyCheckToTxnAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ApplyCheckToTxnAddREM />", string.Empty);
            requestXML = requestXML.Replace("</ApplyCheckToTxnAdd>", "</ApplyCheckToTxnMod>");
            requestXML = requestXML.Replace("<ApplyCheckToTxnAdd>", "<ApplyCheckToTxnMod>");
            requestXML = requestXML.Replace("<ApplyCheckToTxnAdd/>", "<ApplyCheckToTxnMod/>");

            requestXML = requestXML.Replace("<CheckPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</CheckPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ExpenseLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</ItemLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM/>", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ItemLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<AddressREM>", string.Empty);
            requestXML = requestXML.Replace("</AddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AddressREM />", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<CheckLineAdd>", "<CheckLineMod>");
            requestXML = requestXML.Replace("</CheckLineAdd>", "</CheckLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");

            requestXML = requestXML.Replace("<ExpenseLineAdd>", "<ExpenseLineMod>");
            requestXML = requestXML.Replace("</ExpenseLineAdd>", "</ExpenseLineMod>");
            //requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
            requestXML = requestXML.Replace("<ExpenseLineMod>", "<ExpenseLineMod><TxnLineID>-1</TxnLineID>");

            requestXML = requestXML.Replace("<ItemLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<ExpenseLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<DataExt1>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt1>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt2>", "<DataExt>");
            requestXML = requestXML.Replace("</DataExt2>", "</DataExt>");
            requestXML = requestXML.Replace("<DataExt />", string.Empty);

            CheckMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/Phone");
                    CheckMod.RemoveChild(childNode);
                }
            }

            //Axis 617 
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd/CurrencyRef");
                    if (childNode != null)
                        CheckMod.RemoveChild(childNode);
                }
            }
            //Axis 617 ends

            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/Fax");
                    CheckMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/Email");
                    CheckMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckAddRq/CheckAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                CheckQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By CheckRefNumber) : " + rowcount.ToString());
            else
                CheckQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());
            if (CommonUtilities.GetInstance().IsStatementFeature())
            {
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ExpenseLineMod"))
                {
                    if (oNode.SelectSingleNode("CustomerRef") == null)
                    {
                        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ExpenseLineMod/BillableStatus");
                        oNode.RemoveChild(childNode);
                    }
                }
                foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ItemLineMod"))
                {
                    if (oNode.SelectSingleNode("CustomerRef") == null)
                    {
                        XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod/ItemLineMod/BillableStatus");
                        oNode.RemoveChild(childNode);
                    }
                }
            }


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CheckModRq/CheckMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CheckModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CheckModRs/CheckRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofCheck(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
 

        #endregion
    }


    public class CheckEntryCollection : Collection<CheckEntry>
    {
        /// <summary>
        ///  This method is used for getting existing check entry date, If not exists then it return null.
        /// </summary>
        public CheckEntry FindCheckEntry(DateTime date)
        {
            foreach (CheckEntry item in this)
            {
                if (item.CheckDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        public static int chk_cnt = 0;
        public static string chk_number = "";
        /// <summary>
        ///  This method is used for getting existing check entry refnumber, If not exists then it return null.
        /// </summary>
        public CheckEntry FindCheckEntry(string refNumber)
        {
            chk_number = refNumber;
            foreach (CheckEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    chk_cnt++;
                    return item;
                }
            }
            chk_cnt = 0;
            return null;
        }
        /// <summary>
        ///  This method is used for getting existing check entry date and refnumber, If not exists then it return null.
        /// </summary>
        public CheckEntry FindCheckEntry(DateTime date, string refNumber)
        {
            foreach (CheckEntry item in this)
            {
                if (item.RefNumber == refNumber && item.CheckDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }


    [XmlRootAttribute("ExpenseLineAdd", Namespace = "", IsNullable = false)]
    public class ExpenseLineAdd
    {
        private AccountRef m_AccountRef;
        private string m_Amount;
        private string m_Memo;
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_BillableStatus;
       
        //Improvement::548
        private SalesRepRef m_SalesRepRef;
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

        public ExpenseLineAdd()
        { }


        public AccountRef AccountRef
        {
            get
            {
                return m_AccountRef;
            }
            set
            {
                m_AccountRef = value;
            }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }

        public string Memo
        {
            get
            {
                return this.m_Memo;
            }
            set
            {
                this.m_Memo = value;
            }
        }

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }
        public string BillableStatus
        {
            get
            {
                return this.m_BillableStatus;
            }
            set
            {
                this.m_BillableStatus = value;
            }
        }

        //Improvement::548
        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
        
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }


        
    }


    [XmlRootAttribute("ItemLineAdd", Namespace = "", IsNullable = false)]
    public class ItemLineAdd
    {
        private ItemRef m_ItemRef;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_Description;
        private string m_Quantity;
        private string m_UnitOfMeasure;
        private string m_Cost;
        private string m_Amount;
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private string m_BillableStatus;
        private OverrideItemAccountRef m_OverrideItemAccount;

        //Improvement::548
        private SalesRepRef m_SalesRepRef;
        private DataExt m_DataExt1;
        private DataExt m_DataExt2;

        public ItemLineAdd()
        { }

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }


        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set { this.m_InventorySiteLocationRef = value; }
        }

        public string Desc
        {
            get { return this.m_Description; }
            set { this.m_Description = value; }
        }

        public string SerialNumber
        {
            get { return m_SerialNumber; }
            set
            {
                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    m_SerialNumber = value;
                }
            }
        }

        public string LotNumber
        {
            get { return m_LotNumber; }
            set
            {
                // axis 10.0 changes for desktop connection condition
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    m_LotNumber = value;
                }
            }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }
        public string Cost
        {
            get { return this.m_Cost; }
            set { this.m_Cost = value; }
        }

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }


        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public string BillableStatus
        {
            get { return this.m_BillableStatus; }
            set { this.m_BillableStatus = value; }
        }

        //Improvement::548
        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }
        
        public DataExt DataExt1
        {
            get { return this.m_DataExt1; }
            set { this.m_DataExt1 = value; }
        }
        public DataExt DataExt2
        {
            get { return this.m_DataExt2; }
            set { this.m_DataExt2 = value; }
        }


        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideItemAccount; }
            set { this.m_OverrideItemAccount = value; }
        }

    }

    public enum BillableStatus
    {
        Billable,
        NotBillable,
        HasBeenBilled
    }
}
