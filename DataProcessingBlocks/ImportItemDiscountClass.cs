using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemDiscountClass
    {
        private static ImportItemDiscountClass m_ImportItemDiscountClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemDiscountClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemDiscount class
        /// </summary>
        /// <returns></returns>
        public static ImportItemDiscountClass GetInstance()
        {
            if (m_ImportItemDiscountClass == null)
                m_ImportItemDiscountClass = new ImportItemDiscountClass();
            return m_ImportItemDiscountClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemDiscount and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemDiscount QuickBooks collection </returns>
        public DataProcessingBlocks.ItemDiscountQBEntryCollection ImportItemDiscountData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemDiscount Entry collections.
            DataProcessingBlocks.ItemDiscountQBEntryCollection coll = new ItemDiscountQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemDiscount Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemDiscountDt = new DateTime();
                    string datevalue = string.Empty;

                    //ItemDiscount Validation
                    DataProcessingBlocks.ItemDiscountQBEntry ItemDiscount = new ItemDiscountQBEntry();

                    if (dt.Columns.Contains("Name"))
                    {
                        #region Validations of Name
                        if (dr["Name"].ToString() != string.Empty)
                        {
                            if (dr["Name"].ToString().Length > 31)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.Name = dr["Name"].ToString().Substring(0, 31);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.Name = dr["Name"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemDiscount.Name = dr["Name"].ToString();
                                }
                            }
                            else
                            {
                                ItemDiscount.Name = dr["Name"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("BarCodeValue"))
                    {
                        #region Validations of BarCodeValue
                        if (dr["BarCodeValue"].ToString() != string.Empty)
                        {
                            if (dr["BarCodeValue"].ToString().Length > 50)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This BarCodeValue (" + dr["BarCodeValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemDiscount.BarCode.BarCodeValue == null)
                                            ItemDiscount = null;
                                        else
                                            ItemDiscount.BarCode = new BarCode(dr["BarCodeValue"].ToString().Substring(0, 50));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        if (ItemDiscount.BarCode.BarCodeValue == null)
                                            ItemDiscount = null;
                                    }
                                }
                                else
                                {
                                    ItemDiscount.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                }
                            }
                            else
                            {
                                ItemDiscount.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                            }
                        }
                        #endregion
                    }
                    
                    if (dt.Columns.Contains("IsActive"))
                    {
                        #region Validations of IsActive
                        if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                        {

                            int result = 0;
                            if (int.TryParse(dr["IsActive"].ToString(), out result))
                            {
                                ItemDiscount.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                            }
                            else
                            {
                                string strvalid = string.Empty;
                                if (dr["IsActive"].ToString().ToLower() == "true")
                                {
                                    ItemDiscount.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                else
                                {
                                    if (dr["IsActive"].ToString().ToLower() != "false")
                                    {
                                        strvalid = "invalid";
                                    }
                                    else
                                        ItemDiscount.IsActive = dr["IsActive"].ToString().ToLower();
                                }
                                if (strvalid != string.Empty)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(results) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(results) == "Ignore")
                                        {
                                            ItemDiscount.IsActive = dr["IsActive"].ToString();
                                        }
                                        if (Convert.ToString(results) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemDiscount.IsActive = dr["IsActive"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemDiscount.IsActive = dr["IsActive"].ToString();
                                    }
                                }

                            }
                        }
                        #endregion
                    }

                    //P Axis 13.1 : issue 651
                    if (dt.Columns.Contains("ClassRefFullName"))
                    {
                        #region Validations of Class FullName
                        if (dr["ClassRefFullName"].ToString() != string.Empty)
                        {
                            if (dr["ClassRefFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class FullName (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemDiscount.ClassRef.FullName == null)
                                            ItemDiscount.ClassRef.FullName = null;
                                        else
                                            ItemDiscount.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemDiscount.ClassRef.FullName == null)
                                            ItemDiscount.ClassRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemDiscount.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                    if (ItemDiscount.ClassRef.FullName == null)
                                        ItemDiscount.ClassRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemDiscount.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                if (ItemDiscount.ClassRef.FullName == null)
                                    ItemDiscount.ClassRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        #region Validations of Parent FullName
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            if (dr["ParentFullName"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Parent Full Name (" + dr["ParentFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (ItemDiscount.ParentRef.FullName == null)
                                            ItemDiscount.ParentRef.FullName = null;
                                        else
                                            ItemDiscount.ParentRef = new ParentRef(dr["ParentFullName"].ToString().Substring(0, 100));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (ItemDiscount.ParentRef.FullName == null)
                                            ItemDiscount.ParentRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemDiscount.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                    if (ItemDiscount.ParentRef.FullName == null)
                                        ItemDiscount.ParentRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemDiscount.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                if (ItemDiscount.ParentRef.FullName == null)
                                    ItemDiscount.ParentRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("ItemDesc"))
                    {
                        #region Validations of ItemDesc
                        if (dr["ItemDesc"].ToString() != string.Empty)
                        {
                            if (dr["ItemDesc"].ToString().Length > 4095)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This ItemDesc (" + dr["ItemDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.ItemDesc = dr["ItemDesc"].ToString().Substring(0, 4095);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.ItemDesc = dr["ItemDesc"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemDiscount.ItemDesc = dr["ItemDesc"].ToString();
                                }
                            }
                            else
                            {
                                ItemDiscount.ItemDesc = dr["ItemDesc"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("SalesTaxCodeFullName"))
                    {
                        #region Validations of SalesTaxCodeFullName
                        if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                        {
                            if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This SalesTaxCode FullName (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemDiscount.SalesTaxCodeRef.FullName == null)
                                            ItemDiscount.SalesTaxCodeRef.FullName = null;
                                        else
                                            ItemDiscount.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemDiscount.SalesTaxCodeRef.FullName == null)
                                            ItemDiscount.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemDiscount.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                    if (ItemDiscount.SalesTaxCodeRef.FullName == null)
                                        ItemDiscount.SalesTaxCodeRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemDiscount.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                if (ItemDiscount.SalesTaxCodeRef.FullName == null)
                                    ItemDiscount.SalesTaxCodeRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("DiscountRate"))
                    {
                        #region Validations for DiscountRate
                        if (dr["DiscountRate"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["DiscountRate"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Discount Rate( " + dr["DiscountRate"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.DiscountRate = dr["DiscountRate"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.DiscountRate = dr["DiscountRate"].ToString();
                                    }
                                }
                                else
                                    ItemDiscount.DiscountRate = dr["DiscountRate"].ToString();
                            }
                            else
                            {
                                ItemDiscount.DiscountRate = string.Format("{0:00000000.00}", Convert.ToDouble(dr["DiscountRate"].ToString()));
                            }
                        }

                        #endregion

                        
                    }

                    if (dt.Columns.Contains("DiscountRateParent"))
                    {
                        #region Validations of Discount RateParent
                        if (dr["DiscountRateParent"].ToString() != string.Empty)
                        {
                            if (dr["DiscountRateParent"].ToString().Length > 15)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This DiscountRateParent (" + dr["DiscountRateParent"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.DiscountRatePercent = dr["DiscountRateParent"].ToString().Substring(0, 15);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.DiscountRatePercent = dr["DiscountRateParent"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemDiscount.DiscountRatePercent = dr["DiscountRateParent"].ToString();
                                }
                            }
                            else
                            {
                                ItemDiscount.DiscountRatePercent = dr["DiscountRateParent"].ToString();
                            }
                        }
                        #endregion
                    }


                    if (dt.Columns.Contains("AccountFullName"))
                    {
                        #region Validations of Account FullName
                        if (dr["AccountFullName"].ToString() != string.Empty)
                        {
                            if (dr["AccountFullName"].ToString().Length > 159)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Account FullName (" + dr["AccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        ItemDiscount.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                        if (ItemDiscount.AccountRef.FullName == null)
                                            ItemDiscount.AccountRef.FullName = null;
                                        else
                                            ItemDiscount.AccountRef = new AccountRef(dr["AccountFullName"].ToString().Substring(0, 159));
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        ItemDiscount.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                        if (ItemDiscount.AccountRef.FullName == null)
                                            ItemDiscount.AccountRef.FullName = null;
                                    }
                                }
                                else
                                {
                                    ItemDiscount.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                    if (ItemDiscount.AccountRef.FullName == null)
                                        ItemDiscount.AccountRef.FullName = null;
                                }
                            }
                            else
                            {
                                ItemDiscount.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                if (ItemDiscount.AccountRef.FullName == null)
                                    ItemDiscount.AccountRef.FullName = null;
                            }
                        }
                        #endregion
                    }

                   
                    coll.Add(ItemDiscount);

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion

            #region Create Parent for item

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    listCount++;
                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            //Code to check whether Item Name conatins ":"
                            string ItemName = dr["ParentFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ParentFullName"].ToString();
                            }

                            #region Setting SalesTaxCode and IsTaxIncluded

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string IsTaxIncluded = string.Empty;
                            string netRate = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            string AccountFullName = string.Empty;

                            if (dt.Columns.Contains("AccountFullName"))
                            {
                                if (dr["AccountFullName"].ToString() != string.Empty)
                                {
                                    AccountFullName = dr["AccountFullName"].ToString();
                                }
                            }

                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);


                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }

                               

                                //Calculate cost
                                if (dt.Columns.Contains("DiscountRate"))
                                {
                                    if (dr["DiscountRate"].ToString() != string.Empty)
                                    {
                                        decimal rate = 0;
                                        if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                        {
                                            if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                            {
                                                decimal Rate;
                                                if (!decimal.TryParse(dr["DiscountRate"].ToString(), out rate))
                                                {
                                                    //Rate = 0;
                                                    netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["DiscountRate"]), 5));
                                                }
                                                else
                                                {
                                                    Rate = Convert.ToDecimal(dr["DiscountRate"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        rate = Rate / (1 + (TaxRate / 100));
                                                    }

                                                    netRate = Convert.ToString(Math.Round(rate, 5));
                                                }
                                            }
                                        }
                                        if (netRate == string.Empty)
                                        {
                                            netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["DiscountRate"]), 5));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dt.Columns.Contains("DiscountRate"))
                                {
                                    if (dr["DiscountRate"].ToString() != string.Empty)
                                    {
                                        netRate = Convert.ToString(Math.Round(Convert.ToDecimal(dr["DiscountRate"]), 5));
                                    }
                                }
                            }

                            #endregion

                            #region Set Item Query

                            for (int i = 0; i < arr.Length; i++)
                            {
                                int item = 0;
                                int a = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");


                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");

                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }

                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        ItemQueryRq.AppendChild(FullName);
                                    }
                                    
                                    string pinput = pxmldoc.OuterXml;

                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }


                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {

                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {
                                                #region Item Discount Add Query

                                                XmlDocument ItemDiscountAdddoc = new XmlDocument();
                                                ItemDiscountAdddoc.AppendChild(ItemDiscountAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                ItemDiscountAdddoc.AppendChild(ItemDiscountAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                //ItemDiscountAdddoc.AppendChild(ItemDiscountAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                XmlElement qbXMLIS = ItemDiscountAdddoc.CreateElement("QBXML");
                                                ItemDiscountAdddoc.AppendChild(qbXMLIS);
                                                XmlElement qbXMLMsgsRqIS = ItemDiscountAdddoc.CreateElement("QBXMLMsgsRq");
                                                qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                XmlElement ItemDiscountAddRq = ItemDiscountAdddoc.CreateElement("ItemDiscountAddRq");
                                                qbXMLMsgsRqIS.AppendChild(ItemDiscountAddRq);
                                                ItemDiscountAddRq.SetAttribute("requestID", "1");

                                                XmlElement ItemDiscountAdd = ItemDiscountAdddoc.CreateElement("ItemDiscountAdd");
                                                ItemDiscountAddRq.AppendChild(ItemDiscountAdd);

                                                XmlElement NameIS = ItemDiscountAdddoc.CreateElement("Name");
                                                NameIS.InnerText = arr[i];
                                                
                                                ItemDiscountAdd.AppendChild(NameIS);

                                                //Solution for BUG 633
                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {

                                                        XmlElement INIChildFullName = ItemDiscountAdddoc.CreateElement("FullName");

                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            //Adding Parent
                                                            XmlElement INIParent = ItemDiscountAdddoc.CreateElement("ParentRef");
                                                            ItemDiscountAdd.AppendChild(INIParent);

                                                            INIParent.AppendChild(INIChildFullName);
                                                        }

                                                    }
                                                }
                                                //Adding Tax code Element.
                                                if (defaultSettings.TaxCode != string.Empty)
                                                {
                                                    if (defaultSettings.TaxCode.Length < 4)
                                                    {
                                                        XmlElement INISalesTaxCodeRef = ItemDiscountAdddoc.CreateElement("SalesTaxCodeRef");
                                                        ItemDiscountAdd.AppendChild(INISalesTaxCodeRef);

                                                        XmlElement INISTCodeRefFullName = ItemDiscountAdddoc.CreateElement("FullName");
                                                        INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                        INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                    }
                                                }

                                                if (defaultSettings.IncomeAccount != string.Empty)
                                                {
                                                    XmlElement ISIncomeAccountRef = ItemDiscountAdddoc.CreateElement("AccountRef");
                                                    ItemDiscountAdd.AppendChild(ISIncomeAccountRef);
                                                    //Adding IncomeAccount FullName.
                                                    XmlElement ISFullName = ItemDiscountAdddoc.CreateElement("FullName");

                                                    if (AccountFullName != string.Empty)
                                                        ISFullName.InnerText = AccountFullName;
                                                    else
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                                                                        
                                                    ISIncomeAccountRef.AppendChild(ISFullName);
                                                }


                                                string ItemDiscountAddinput = ItemDiscountAdddoc.OuterXml;

                                                string respItemDiscountAddinputdoc = string.Empty;
                                                try
                                                {

                                                    //Axis 10.2(bug no 66)
                                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                    {
                                                        respItemDiscountAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemDiscountAddinput);
                                                    }
                                                    else
                                                    {
                                                        respItemDiscountAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemDiscountAddinput);
                                                    }
                                                    //End Changes
                                                    
                                                    System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                    outputXML.LoadXml(respItemDiscountAddinputdoc);
                                                    string StatusSeverity = string.Empty;
                                                    string statusMessage = string.Empty;
                                                    foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemDiscountAddRs"))
                                                    {
                                                        StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                        statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    }
                                                    outputXML.RemoveAll();
                                                    if (StatusSeverity == "Error" || StatusSeverity == "Warn" || statusSeverity == "Warning")
                                                    {
                                                        //Task 1435 (Axis 6.0):
                                                        ErrorSummary summary = new ErrorSummary(statusMessage);
                                                        summary.ShowDialog();
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                string strTest3 = respItemDiscountAddinputdoc;
                                                #endregion
                                            }
                                        }

                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    return null;
                }
            }

            #endregion
            return coll;
        }

    }
}
