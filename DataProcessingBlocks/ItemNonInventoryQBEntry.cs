using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using QuickBookEntities;
using System.Xml;
using System.Windows.Forms;
using System.Xml.Schema;
using System.Collections;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("ItemNonInventoryQBEntry", Namespace = "", IsNullable = false)]
    public class ItemNonInventoryQBEntry
    {
        #region Private Member Variable

        private string m_Name;
        private BarCode m_BarCode;
        private string m_IsActive;
        private ParentRef m_ParentRef;
        private string m_ManufacturerPartNumber;
        private UnitOfMeasureSetRef m_UnitOfMeasureSetRef;
        private string m_IsTaxIncluded;
        private SalesTaxCodeRef m_SalesTaxCodeRef;
        private Collection<SalesOrPurchase> m_SalesOrPurchase=new Collection<SalesOrPurchase>();
        private Collection<SalesAndPurchase> m_SalesAndPurchase=new Collection<SalesAndPurchase>();
        //P Axis 13.1 : issue 651
        private ClassRef m_ClassRef;
        #endregion

        #region Constructor
        public ItemNonInventoryQBEntry()
        {
        }
        #endregion

        #region Public Properties

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        public BarCode BarCode
        {
            get { return m_BarCode; }
            set { m_BarCode = value; }
        }

        public string IsActive
        {
            get { return m_IsActive; }
            set { m_IsActive = value; }
        }

        //P Axis 13.1 : issue 651
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }
        public ParentRef ParentRef
        {
            get { return m_ParentRef; }
            set { m_ParentRef = value; }
        }

        public string ManufacturerPartNumber
        {
            get { return m_ManufacturerPartNumber; }
            set { m_ManufacturerPartNumber = value; }
        }

        public UnitOfMeasureSetRef UnitOfMeasureSetRef
        {
            get { return m_UnitOfMeasureSetRef; }
            set { m_UnitOfMeasureSetRef = value; }
        }

        public string IsTaxIncluded
        {
            get { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return m_SalesTaxCodeRef; }
            set { m_SalesTaxCodeRef = value; }
        }

        [XmlArray("SalesOrPurchaseREM")]
        public Collection<SalesOrPurchase> SalesOrPurchase
        {
            get { return m_SalesOrPurchase; }
            set { m_SalesOrPurchase = value; }
        }

        [XmlArray("SalesAndPurchaseREM")]
        public Collection<SalesAndPurchase> SalesAndPurchase
        {
            get { return m_SalesAndPurchase; }
            set { m_SalesAndPurchase = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        ///  Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemNonInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create JournalEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemNonInventoryAddRq = requestXmlDoc.CreateElement("ItemNonInventoryAddRq");
            inner.AppendChild(ItemNonInventoryAddRq);

            //Create JournalEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement ItemNonInventoryAdd = requestXmlDoc.CreateElement("ItemNonInventoryAdd");

            ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

            requestXML = requestXML.Replace("<SalesOrPurchaseREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrPurchaseREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrPurchaseREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesAndPurchaseREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesAndPurchaseREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesAndPurchaseREM>", string.Empty);

            ItemNonInventoryAdd.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;



            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs/ItemNonInventoryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemNonInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for updating ItemNonInventoryQBEntry information
        /// of existing ItemNonInventoryQBEntry with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateItemNonInventoryInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }

                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }     
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.ItemNonInventoryQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement ItemNonInventoryQBEntryModRq = requestXmlDoc.CreateElement("ItemNonInventoryModRq");
            inner.AppendChild(ItemNonInventoryQBEntryModRq);

            //Create EstimateMod aggregate and fill in field values for it
            System.Xml.XmlElement ItemNonInventoryMod = requestXmlDoc.CreateElement("ItemNonInventoryMod");
            ItemNonInventoryQBEntryModRq.AppendChild(ItemNonInventoryMod);

            requestXML = requestXML.Replace("<SalesOrPurchaseREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesOrPurchaseREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesOrPurchaseREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesAndPurchaseREM />", string.Empty);
            requestXML = requestXML.Replace("<SalesAndPurchaseREM>", string.Empty);
            requestXML = requestXML.Replace("</SalesAndPurchaseREM>", string.Empty);

            requestXML = requestXML.Replace("<SalesAndPurchase>", "<SalesAndPurchaseMod>");
            requestXML = requestXML.Replace("</SalesAndPurchase>", "</SalesAndPurchaseMod>");
            requestXML = requestXML.Replace("<SalesAndPurchase></SalesAndPurchase>",string.Empty);


            requestXML = requestXML.Replace("<SalesOrPurchase>", "<SalesOrPurchaseMod>");
            requestXML = requestXML.Replace("</SalesOrPurchase>", "</SalesOrPurchaseMod>");
            requestXML = requestXML.Replace("<SalesOrPurchase></SalesOrPurchase>", string.Empty);





            ItemNonInventoryMod.InnerXml = requestXML;


            //For add request id to track error message
            string requeststring = string.Empty;

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemNonInventoryModRq/ItemNonInventoryMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("ListID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemNonInventoryModRq/ItemNonInventoryMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/ItemNonInventoryModRq/ItemNonInventoryMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryModRs/ItemNonInventoryRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["ListID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofItemNonInventory(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }


    public class ItemNonInventoryQBEntryCollection : Collection<ItemNonInventoryQBEntry>
    {

    }
    /// <summary>
    /// This class used for declare properties and variables for sales or purchase
    /// </summary>
    [XmlRootAttribute("SalesOrPurchase", Namespace = "", IsNullable = false)]
    public class SalesOrPurchase
    {
        #region Private Member Variable
        private string m_Desc;
        private string m_Price;
        private string m_PricePercent;
        private AccountRef m_AccountRef;
        #endregion

        #region Constructor
        public SalesOrPurchase() { }
        #endregion

        #region Public Properties

        public string Desc
        {
            get { return m_Desc; }
            set { m_Desc = value; }
        }

        public string Price
        {
            get { return m_Price; }
            set { m_Price = value; }
        }

        public string PricePercent
        {
            get { return m_PricePercent; }
            set { m_PricePercent = value; }
        }

        public AccountRef AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }

        #endregion
    }
    /// <summary>
    ///  This class used for declare properties and variables for sales and purchase
    /// </summary>
    [XmlRootAttribute("SalesAndPurchase", Namespace = "", IsNullable = false)]
    public class SalesAndPurchase
    {
        #region Private Member Variable
        
        private string m_SalesDesc;
        private string m_SalesPrice;
        private IncomeAccountRef m_IncomeAccountRef;
        private string m_PurchaseDesc;
        private string m_PurchaseCost;
        private PurchaseTaxCodeRef m_PurchaseTaxCodeRef;
        private ExpenseAccountRef m_ExpenseAccountRef;
        private PrefVendorRef m_PrefVendorRef;

        #endregion

        #region Constructor
        public SalesAndPurchase()
        { }
        #endregion

        #region Public Properties

        public string SalesDesc
        {
            get { return m_SalesDesc; }
            set { m_SalesDesc = value; }
        }

        public string SalesPrice
        {
            get { return m_SalesPrice; }
            set { m_SalesPrice = value; }
        }

        public IncomeAccountRef IncomeAccountRef
        {
            get { return m_IncomeAccountRef; }
            set { m_IncomeAccountRef = value; }
        }

        public string PurchaseDesc
        {
            get { return m_PurchaseDesc; }
            set { m_PurchaseDesc = value; }
        }

        public string PurchaseCost
        {
            get { return m_PurchaseCost; }
            set { m_PurchaseCost = value; }
        }

        public PurchaseTaxCodeRef PurchaseTaxCodeRef
        {
            get { return m_PurchaseTaxCodeRef; }
            set { m_PurchaseTaxCodeRef = value; }
        }

        public ExpenseAccountRef ExpenseAccountRef
        {
            get { return m_ExpenseAccountRef; }
            set { m_ExpenseAccountRef = value; }
        }

        public PrefVendorRef PrefVendorRef
        {
            get { return m_PrefVendorRef; }
            set { m_PrefVendorRef = value; }
        }


        #endregion
    }

}
