// ==============================================================================================
// 
// CreditMemoQbEntry.cs
//
// This file contains the implementations of the Credit Memo Entry private members , 
// Properties, Constructors and Methods for QuickBooks Credit Memo Entry Imports.
//         Credit Memo Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping field (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("CreditMemoQBEntry", Namespace = "", IsNullable = false)]
    public class CreditMemoQBEntry
    {
        #region  Private Member Variable
        private CustomerRef m_CustomerRef;
        private ClassRef m_ClassRef;
        private ARAccountRef m_ARAccountRef;
        private TemplateRef m_TemplateRef;
        private string m_TxnDate;
        private string m_RefNumber;
        private string m_IsPending;
        private string m_PONumber;
        private TermsRef m_TermRef;
        private string m_DueDate;
        private SalesRepRef m_SalesRepRef;
        private string m_FOB;
        private string m_ShipDate;
        private ShipMethodRef m_ShipMethodRef;
        private ItemSalesTaxRef m_ItemSalesTaxRef;
        private string m_Memo;
        private CustomerMsgRef m_CustomerMsgRef;
        private string m_IsToBePrinted;
        private string m_IsToBeEmailed;
        private string m_IsTaxIncluded;
        private CustomerSalesTaxCodeRef m_CustomerSalesTaxCodeRef;
        private string m_Other;
        private string m_Others;
        private string m_ExchangeRate;
        private Collection<BillAddress> m_BillAddress = new Collection<BillAddress>();
        private Collection<ShipAddress> m_ShipAddress = new Collection<ShipAddress>();
        private Collection<CreditMemoLineAdd> m_CreditMemoLineAdd = new Collection<CreditMemoLineAdd>();
        private string m_IncludeRetElement;
        private DateTime m_CreditMemotDate;
        private string m_Phone;
        private string m_Fax;
        private string m_Email;
        private Collection<DiscountLineAdd> m_DiscountLineAdd = new Collection<DiscountLineAdd>();
        private Collection<ShippingLineAdd> m_ShippingLineAdd = new Collection<ShippingLineAdd>();
        /// <summary>
        ///bug 442 11.4
        /// </summary>
        private CurrencyRef m_CurrencyRef;

        #endregion

        #region Construtor

        public CreditMemoQBEntry()
        {

        }

        #endregion

        #region Public Properties

        public CustomerRef CustomerRef
        {
            get { return m_CustomerRef; }
            set { m_CustomerRef = value; }
        }
        /// <summary>
        /// 11.4  bug no 442
        /// </summary>
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }
        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public ARAccountRef ARAccountRef
        {
            get { return m_ARAccountRef; }
            set { m_ARAccountRef = value; }
        }

        public TemplateRef TemplateRef
        {
            get { return m_TemplateRef; }
            set { m_TemplateRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }



        public string RefNumber
        {
            get { return m_RefNumber; }
            set { m_RefNumber = value; }
        }

        [XmlArray("BillAddressREM")]
        public Collection<BillAddress> BillAddress
        {
            get { return m_BillAddress; }
            set { m_BillAddress = value; }
        }

        [XmlArray("ShipAddressREM")]
        public Collection<ShipAddress> ShipAddress
        {
            get { return m_ShipAddress; }
            set { m_ShipAddress = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsPending
        {
            get { return m_IsPending; }
            set { m_IsPending = value; }
        }


        public string PONumber
        {
            get { return m_PONumber; }
            set { m_PONumber = value; }
        }

        public TermsRef TermsRef
        {
            get { return m_TermRef; }
            set { m_TermRef = value; }
        }

        [XmlElement(DataType = "string")]
        public String DueDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_DueDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_DueDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_DueDate = value;
            }
        }


        public SalesRepRef SalesRepRef
        {
            get { return m_SalesRepRef; }
            set { m_SalesRepRef = value; }
        }

        public string FOB
        {
            get { return m_FOB; }
            set { m_FOB = value; }
        }

        [XmlElement(DataType = "string")]
        public string ShipDate
        {
            get { return m_ShipDate; }
            set { m_ShipDate = value; }

        }

        public ShipMethodRef ShipMethodRef
        {
            get { return m_ShipMethodRef; }
            set { m_ShipMethodRef = value; }
        }

        public ItemSalesTaxRef ItemSalesTaxRef
        {
            get { return m_ItemSalesTaxRef; }
            set { m_ItemSalesTaxRef = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }


        public CustomerMsgRef CustomerMsgRef
        {
            get { return m_CustomerMsgRef; }
            set { m_CustomerMsgRef = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBePrinted
        {
            get { return m_IsToBePrinted; }
            set { m_IsToBePrinted = value; }
        }

        [XmlElement(DataType = "string")]
        public string IsToBeEmailed
        {
            get { return m_IsToBeEmailed; }
            set { m_IsToBeEmailed = value; }
        }
        [XmlElement(DataType = "string")]
        public string IsTaxIncluded
        {
            get
            { return m_IsTaxIncluded; }
            set { m_IsTaxIncluded = value; }
        }

        public CustomerSalesTaxCodeRef CustomerSalesTaxCodeRef
        {
            get { return m_CustomerSalesTaxCodeRef; }
            set { m_CustomerSalesTaxCodeRef = value; }
        }

        //Other
        public string Other
        {
            get { return m_Other; }
            set { m_Other = value; }
        }

        public string Others
        {
            get { return m_Others; }
            set { m_Others = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }
        public string Phone
        {
            get { return m_Phone; }
            set { m_Phone = value; }
        }
        public string Fax
        {
            get { return m_Fax; }
            set { m_Fax = value; }
        }
        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }

        [XmlArray("CreditMemoLineAddREM")]
        public Collection<CreditMemoLineAdd> CreditMemoLineAdd
        {
            get { return m_CreditMemoLineAdd; }
            set { m_CreditMemoLineAdd = value; }
        }


        [XmlIgnoreAttribute()]
        public string IncludeRetElement
        {
            get { return m_IncludeRetElement; }
            set { m_IncludeRetElement = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime CreditMemoDate
        {
            get { return m_CreditMemotDate; }
            set { m_CreditMemotDate = value; }
        }

        // Axis 10.0

        [XmlArray("DiscountLineAddREM")]
        public Collection<DiscountLineAdd> DiscountLineAdd
        {
            get { return m_DiscountLineAdd; }

            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    m_DiscountLineAdd = value;
                }
            }
        }

        [XmlArray("ShippingLineAddREM")]
        public Collection<ShippingLineAdd> ShippingLineAdd
        {
            get { return m_ShippingLineAdd; }

            set
            {
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
                {
                    m_ShippingLineAdd = value;
                }
            }
        }
       
        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CreditMemoQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            //requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", null, null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create SalesReceiptEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement CreditMemoAddRq = requestXmlDoc.CreateElement("CreditMemoAddRq");
            inner.AppendChild(CreditMemoAddRq);

            //Create SalesReceiptEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement CreditMemoAdd = requestXmlDoc.CreateElement("CreditMemoAdd");

            CreditMemoAddRq.AppendChild(CreditMemoAdd);

            requestXML = requestXML.Replace("<CreditMemoLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditMemoLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);

            //if (TransactionImporter.TransactionImporter.rdbdesktopbutton == false)
            //{
                requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
                requestXML = requestXML.Replace("<DiscountLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("</DiscountLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);
                requestXML = requestXML.Replace("<ShippingLineAddREM>", string.Empty);
                requestXML = requestXML.Replace("</ShippingLineAddREM>", string.Empty);

                ///
                //Axis 11.4(Bug No 443)
                requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
                requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
                requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
                //End Changes 

            CreditMemoAdd.InnerXml = requestXML;

            //For add request id to track error message
            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/CurrencyRef");
                    CreditMemoAdd.RemoveChild(childNode);
                }
            }

            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/Phone");
                    CreditMemoAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/Fax");
                    CreditMemoAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/Email");
                    CreditMemoAdd.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/InventorySiteLocationRef");
                    CreditMemoAdd.ParentNode.RemoveChild(node);
                    CreditMemoAdd.RemoveAll();
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                CreditMemoAddRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By Refnumber) : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CreditMemoAddRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {

                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CreditMemoAddRs/CreditMemoRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }

            }

            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfCreditMemo(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        /// <summary>
        /// This method is used for getting existing Credit Memo ref no.
        /// If not exists then it return null.
        /// </summary>
        /// <param name="VCredit Memo Ref No"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public string CheckAndGetRefNoExistsInQuickBooks(string RefNo, string AppName)
        {
            XmlDocument requestXmlDoc = new XmlDocument();

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create CreditMemoQueryRq aggregate and fill in field values for it
            XmlElement CreditMemoQueryRq = requestXmlDoc.CreateElement("CreditMemoQueryRq");
            inner.AppendChild(CreditMemoQueryRq);

            //Create Refno aggregate and fill in field values for it.
            XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            RefNumber.InnerText = RefNo;
            CreditMemoQueryRq.AppendChild(RefNumber);

            //Create IncludeRetElement for fast execution.
            XmlElement IncludeRetElement = requestXmlDoc.CreateElement("IncludeRetElement");
            IncludeRetElement.InnerText = "TxnID";
            CreditMemoQueryRq.AppendChild(IncludeRetElement);

            string resp = string.Empty;
            try
            {
                //responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }

            if (resp == string.Empty)
            {
                return string.Empty;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;

                }
                else if (resp.Contains("statusSeverity=\"Warn\""))
                {
                    //Returning means there is no REf NO exists.
                    return string.Empty;
                }
                else
                    return resp;
            }
        }

        /// <summary>
        /// This method is used for updating CreditMemo information
        /// of existing CreditMemo with listid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>
        public bool UpdateCreditMemoInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CreditMemoQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create EstimateModRq aggregate and fill in field values for it
            System.Xml.XmlElement CreditMemoQBEntryModRq = requestXmlDoc.CreateElement("CreditMemoModRq");
            inner.AppendChild(CreditMemoQBEntryModRq);

            //Create InvoiceMod aggregate and fill in field values for it
            System.Xml.XmlElement CreditMemoMod = requestXmlDoc.CreateElement("CreditMemoMod");
            CreditMemoQBEntryModRq.AppendChild(CreditMemoMod);

            requestXML = requestXML.Replace("<CreditMemoPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditMemoPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditMemoLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<CreditMemoLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");

            requestXML = requestXML.Replace("<CreditMemoLineAdd>", "<CreditMemoLineMod>");
            requestXML = requestXML.Replace("</CreditMemoLineAdd>", "</CreditMemoLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<ItemRef>", "<TxnLineID>-1</TxnLineID><ItemRef>");
          
            //Axis 11.0 Bug No.138
            requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);

            ///
            //Axis 11.4(Bug No 443)
            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            //End Changes 

            CreditMemoMod.InnerXml = requestXML;

            //For add request id to track error message

            ///11.4 bug no 442
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("CurrencyRef") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/CurrencyRef");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/Phone");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/Fax");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/Email");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/InventorySiteLocationRef");
                    CreditMemoMod.ParentNode.RemoveChild(node);
                    CreditMemoMod.RemoveAll();
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                CreditMemoQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By CreditMemoRefNumber) : " + rowcount.ToString());
            else
                CreditMemoQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;


            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            //ListID.InnerText = listID;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //PriceLevelMod.AppendChild(ListID).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            //EditSequence.InnerText = editSequence;
            //PriceLevelMod.AppendChild(EditSequence).InnerText = editSequence;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);

                    //CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CreditMemoModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CreditMemoModRs/CreditMemoRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfCreditMemo(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        //Bug No. 412
        /// <summary>
        /// append data to existing one
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <param name="txnLineIDList"></param>
        /// <returns></returns>
        public bool AppendCreditMemoInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string listID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.CreditMemoQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);
            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            System.IO.File.Delete(fileName);

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create CreditMemoModRq aggregate and fill in field values for it
            System.Xml.XmlElement CreditMemoQBEntryModRq = requestXmlDoc.CreateElement("CreditMemoModRq");
            inner.AppendChild(CreditMemoQBEntryModRq);

            //Create CreditMemoMod aggregate and fill in field values for it
            System.Xml.XmlElement CreditMemoMod = requestXmlDoc.CreateElement("CreditMemoMod");
            CreditMemoQBEntryModRq.AppendChild(CreditMemoMod);


             // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }

            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></CreditMemoLineMod><CreditMemoLineMod>";

                XmlNode CreditMemoLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod");
                System.Xml.XmlElement CreditMemoLineGroupMod = requestXmlDoc.CreateElement("CreditMemoLineMod");
                System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                TxnLineID.InnerText = txnLineIDList[i];
                CreditMemoLineGroupMod.AppendChild(TxnLineID);
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertAfter(CreditMemoLineGroupMod, CreditMemoLineMod.ChildNodes[i == 0 ? i : i - 1]);
            }

            XmlNode CreditMemoMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod");

            requestXML = requestXML.Replace("<TxnID>" + listID + "</TxnID>", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditMemoPerItemREM>", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<CreditMemoLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</CreditMemoLineAddREM>", string.Empty);

            requestXML = requestXML.Replace("<CreditMemoLineAdd />", string.Empty);

            requestXML = requestXML.Replace("<BillAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<BillAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</BillAddressREM>", string.Empty);

            requestXML = requestXML.Replace("<ShipAddressREM />", string.Empty);
            requestXML = requestXML.Replace("<ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("</ShipAddressREM>", string.Empty);
            requestXML = requestXML.Replace("<AppliedToTxnAdd>", "<AppliedToTxnMod>");
            requestXML = requestXML.Replace("</AppliedToTxnAdd>", "</AppliedToTxnMod>");
            requestXML = requestXML.Replace("<CreditMemoLineAdd>", "<CreditMemoLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("</CreditMemoLineAdd>", "</CreditMemoLineMod>");
            requestXML = requestXML.Replace("<ItemLineAdd>", "<ItemLineMod>");
            requestXML = requestXML.Replace("</ItemLineAdd>", "</ItemLineMod>");
            requestXML = requestXML.Replace("<DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("</DataExtREM>", string.Empty);
            requestXML = requestXML.Replace("<DataExtREM />", string.Empty);

            //Axis 11.0 Bug No.138
            requestXML = requestXML.Replace("<DiscountLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<ShippingLineAddREM />", string.Empty);

            string[] CreditMemoLineModFragment;
            int indexCreditMemoLine = requestXML.IndexOf(@"<CreditMemoLineMod>");
            CreditMemoLineModFragment = requestXML.Split(new string[] { "<CreditMemoLineMod>" }, StringSplitOptions.None);
            if (CreditMemoLineModFragment.Length > 0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = CreditMemoLineModFragment[0];
                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").FirstChild;
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertBefore(xfrag1, firstChild1);
                requestXML = requestXML.Replace(CreditMemoLineModFragment[0], string.Empty);
            }

            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertAfter(xfrag, CreditMemoMod2.LastChild);

            //For add request id to track error message
            string requeststring = string.Empty;
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("Phone") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/Phone");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("Fax") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/Fax");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod"))
            {
                if (oNode.SelectSingleNode("Email") != null)
                {
                    XmlNode childNode = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod/Email");
                    CreditMemoMod.RemoveChild(childNode);
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("InventorySiteLocationRef") != null)
                {
                    XmlNode node = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd/InventorySiteLocationRef");
                    CreditMemoMod.ParentNode.RemoveChild(node);
                    CreditMemoMod.RemoveAll();
                }
            }
            foreach (System.Xml.XmlNode oNode in requestXmlDoc.SelectNodes("/QBXML/QBXMLMsgsRq/CreditMemoAddRq/CreditMemoAdd"))
            {
                if (oNode.SelectSingleNode("RefNumber") != null)
                {
                    requeststring = oNode.SelectSingleNode("RefNumber").InnerText.ToString();
                }

            }
            if (requeststring != string.Empty)
                CreditMemoQBEntryModRq.SetAttribute("requestID", "RefNumber :" + requeststring + " RowNumber (By CreditMemoRefNumber) : " + rowcount.ToString());
            else
                CreditMemoQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertBefore(ListID, firstChild).InnerText = listID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/CreditMemoModRq/CreditMemoMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CreditMemoModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CreditMemoModRs/CreditMemoRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfCreditMemo(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }
        #endregion

    }


    public class CreditMemoQBEntryCollection : Collection<CreditMemoQBEntry>
    {
        public CreditMemoQBEntry FindCreditMemoEntry(DateTime date)
        {
            foreach (CreditMemoQBEntry item in this)
            {
                if (item.CreditMemoDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
        public static int creditmemo_cnt = 0;
        public static string creditmemo_number = "";
        /// <summary>
        /// This method is used for getting existing creditmemo refnumber, If not exists then it return null.
        /// </summary>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public CreditMemoQBEntry FindCreditMemoEntry(string refNumber)
        {
            creditmemo_number = refNumber;
            foreach (CreditMemoQBEntry item in this)
            {
                if (item.RefNumber == refNumber)
                {
                    creditmemo_cnt++;
                    return item;
                }
            }
            creditmemo_cnt = 0;
            return null;
        }
        /// <summary>
        ///  /// This method is used for getting existing creditmemo date and refnumber, If not exists then it return null.
        /// </summary>
        /// <param name="date"></param>
        /// <param name="refNumber"></param>
        /// <returns></returns>
        public CreditMemoQBEntry FindCreditMemoEntry(DateTime date, string refNumber)
        {
            foreach (CreditMemoQBEntry item in this)
            {
                if (item.RefNumber == refNumber && item.CreditMemoDate.Date == date.Date)
                {
                    return item;
                }
            }
            return null;
        }
    }



    [XmlRootAttribute("CreditMemoLineAdd", Namespace = "", IsNullable = false)]
    public class CreditMemoLineAdd
    {
        #region Private Member Variables

        private ItemRef m_ItemRef;
        private string m_Desc;
        private string m_Quantity;
        private string m_SerialNumber;
        private string m_LotNumber;
        private string m_UnitOfMeasure;
        //601
        private PriceLevelRef m_PriceLevelFullName;
        //Axis 723
        private ClassRef m_ClassRef;
        //Axis 723 ends
        private string m_Rate;
        private string m_RatePercent;
        //private PriceLevelRef m_PriceLevelRef;
       
        private string m_Amount;
        private InventorySiteRef m_InventorySiteRef;
        private InventorySiteLocationRef m_InventorySiteLocationRef;

        private SalesTaxCodeRef m_SalesTaxCodeRef;
        
        private OverrideItemAccountRef m_OverrideAccountRef;
        private string m_Other1;
        private string m_Other2;

        //Bug no.443     
        private Collection<QuickBookStreams.DataExt> m_DataExt = new Collection<QuickBookStreams.DataExt>();

        //end Bug no. 443
        #endregion

        #region  Constructors

        public CreditMemoLineAdd()
        {

        }
        #endregion

        #region Public Properties

        public ItemRef ItemRef
        {
            get { return this.m_ItemRef; }
            set { this.m_ItemRef = value; }
        }

        public string Desc
        {
            get { return this.m_Desc; }
            set { this.m_Desc = value; }
        }

        public string Quantity
        {
            get { return this.m_Quantity; }
            set { this.m_Quantity = value; }
        }

        public string Rate
        {
            get { return this.m_Rate; }
            set
            {
                this.m_Rate = value;
                this.m_RatePercent = null;
            }
        }

        public string SerialNumber
        {
            get { return this.m_SerialNumber; }
            set { this.m_SerialNumber = value; }
        }


        public string LotNumber
        {
            get { return this.m_LotNumber; }
            set { this.m_LotNumber = value; }
        }


        public string UnitOfMeasure
        {
            get { return this.m_UnitOfMeasure; }
            set { this.m_UnitOfMeasure = value; }
        }

        //601
        public PriceLevelRef PriceLevelRef
        {
            get { return this.m_PriceLevelFullName; }
            set { this.m_PriceLevelFullName = value; }
        }

        //Axis 723
        public ClassRef ClassRef
        {
            get { return this.m_ClassRef; }
            set { this.m_ClassRef = value; }
        }
        //Axis 723 ends

        public string RatePercent
        {
            get { return this.m_RatePercent; }
            set
            {
                this.m_RatePercent = value;
                this.m_Rate = null;
            }
        }

        //public PriceLevelRef PriceLevelRef
        //{
        //    get { return m_PriceLevelRef; }
        //    set { m_PriceLevelRef = value; }
        //}      

        public string Amount
        {
            get { return this.m_Amount; }
            set { this.m_Amount = value; }
        }
        public InventorySiteRef InventorySiteRef
        {
            get { return this.m_InventorySiteRef; }
            set { this.m_InventorySiteRef = value; }
        }

        public InventorySiteLocationRef InventorySiteLocationRef
        {
            get { return this.m_InventorySiteLocationRef; }
            set
            {
                this.m_InventorySiteLocationRef = value;
            }
        }

        public SalesTaxCodeRef SalesTaxCodeRef
        {
            get { return this.m_SalesTaxCodeRef; }
            set { this.m_SalesTaxCodeRef = value; }
        }

        public OverrideItemAccountRef OverrideItemAccountRef
        {
            get { return this.m_OverrideAccountRef; }
            set { this.m_OverrideAccountRef = value; }
        }

        public string Other1
        {
            get { return this.m_Other1; }
            set { this.m_Other1 = value; }
        }

        public string Other2
        {
            get { return this.m_Other2; }
            set { this.m_Other2 = value; }
        }

    
        //bug no. 443
        [XmlArray("DataExtREM")]
        public Collection<QuickBookStreams.DataExt> DataExt
        {
            get { return this.m_DataExt; }
            set { this.m_DataExt = value; }
        }

        #endregion
    }
}
