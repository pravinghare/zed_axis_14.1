// ==============================================================================================
// 
// DepositEntry.cs
//
// This file contains the implementations of the Deposit Entry private members , 
// Properties, Constructors and Methods for QuickBooks Deposit Entry Imports.
//         Deposit Entry includes new added Intuit QuickBooks(2009-2010) SDK 8.0 
// Mapping field (ExchangeRate) 
// Developed By : Sandeep Patil.
// Date : 
// Modified By : Sandeep Patil.
// Date : 
// ==============================================================================================

using System;
using System.Collections.Generic;
using System.Text;
using QuickBookEntities;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.Xml;
using EDI.Constant;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("DepositQBEntry", Namespace = "", IsNullable = false)]
    public class DepositQBEntry
    {
        #region  Private Member Variable

        private string m_TxnDate;
         
        // For Axis 9.0
        private string m_TxnID;

        private DepositToAccountRef m_DepositToAccountRef;
        private string m_Memo;
        private CashBackInfoAdd m_CashBackInfo;
        private CurrencyRef m_CurrencyRef;
        private string m_ExchangeRate;
     
        private Collection<DepositLineAdd> m_DepositLineAdd = new Collection<DepositLineAdd>();
        private Collection<CashBackInfoAdd> m_CashBackInfoAdd = new Collection<CashBackInfoAdd>();
        private DateTime m_DepositDate;


        #endregion

        #region Construtor

        public DepositQBEntry()
        {

        }

        #endregion

        #region Public Properties
        [XmlElement(DataType = "string")]
        
        // For Axis 9.0

        public string TxnID
        {
            get { return m_TxnID; }
            set { m_TxnID = value; }
        }

        public string TxnDate
        {
            get
            {
                try
                {

                    if (Convert.ToDateTime(this.m_TxnDate) <= DateTime.MinValue)
                    {
                        return null;
                    }
                    else
                        return Convert.ToString(this.m_TxnDate);
                }
                catch
                {
                    return null;
                }
            }
            set
            {
                this.m_TxnDate = value;
            }
        }
        public DepositToAccountRef DepositToAccountRef
        {
            get { return m_DepositToAccountRef; }
            set { m_DepositToAccountRef = value; }
        }
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public CashBackInfoAdd CashBackInfoAdd
        {
            get { return m_CashBackInfo; }
            set { m_CashBackInfo = value; }
        }
      
        public CurrencyRef CurrencyRef
        {
            get { return m_CurrencyRef; }
            set { m_CurrencyRef = value; }
        }

        public string ExchangeRate
        {
            get { return m_ExchangeRate; }
            set { m_ExchangeRate = value; }
        }

        [XmlArray("CashBackInfoAddREM")]
        public Collection<CashBackInfoAdd> CashBankInfoAdd
        {
            get { return m_CashBackInfoAdd; }
            set { m_CashBackInfoAdd = value; }
        }

        [XmlArray("DepositLineAddREM")]
        public Collection<DepositLineAdd> DepositLineAdd
        {
            get { return m_DepositLineAdd; }
            set { m_DepositLineAdd = value; }
        }

        [XmlIgnoreAttribute()]
        public DateTime DepositDate
        {
            get { return m_DepositDate; }
            set { m_DepositDate = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Creating request file for exporting data to quickbook.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <returns></returns>
        public bool ExportToQuickBooks(ref string statusMessage, ref string requestText,int rowcount,string AppName)
        {
            int startIndex = 0;
            int lastIndex = 0;
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.DepositQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
          
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create DepositEntryAddRq aggregate and fill in field values for it
            System.Xml.XmlElement DepositAddRq = requestXmlDoc.CreateElement("DepositAddRq");
            inner.AppendChild(DepositAddRq);

            //Create DepositEntryAdd aggregate and fill in field values for it
            System.Xml.XmlElement DepositAdd = requestXmlDoc.CreateElement("DepositAdd");

            DepositAddRq.AppendChild(DepositAdd);

            requestXML = requestXML.Replace("<DepositLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<DepositLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</DepositLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<CashBackInfoAddREM />", string.Empty);
            requestXML = requestXML.Replace("<CashBackInfoAddREM>", string.Empty);
            requestXML = requestXML.Replace("</CashBackInfoAddREM>", string.Empty);
            requestXML = requestXML.Replace("<TxnID />", string.Empty);
            if (requestXML.Contains("TxnID"))
            {
                startIndex = requestXML.IndexOf("<TxnID>");
                lastIndex = requestXML.IndexOf("</TxnID>");
                requestXML = requestXML.Remove(startIndex, lastIndex);
                requestXML = requestXML.Replace("</TxnID>", string.Empty);
            }            
            
            DepositAdd.InnerXml = requestXML;

            DepositAddRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());

            requestText = requestXmlDoc.OuterXml;
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if(TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);                 
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/DepositAddRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/DepositAddRs/DepositRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);
                }
            }
            if (resp == string.Empty)
            {
                statusMessage += "\n";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfDeposit(this);
                statusMessage += "\n";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;

            }
        }

        /// <summary>
        /// This method is used for updating Deposite information
        /// of existing deposite with txnid and edit sequence.
        /// </summary>
        /// <param name="statusMessage"></param>
        /// <param name="requestText"></param>
        /// <param name="rowcount"></param>
        /// <param name="AppName"></param>
        /// <param name="listID"></param>
        /// <param name="editSequence"></param>
        /// <returns></returns>

        public bool UpdateDepositeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string txnID, string editSequence)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }

            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.DepositQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);


            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create Depositemod req and fill in field values for it
            System.Xml.XmlElement DepositQBEntryModRq = requestXmlDoc.CreateElement("DepositModRq");
            inner.AppendChild(DepositQBEntryModRq);

            //Create depositeeMod aggregate and fill in field values for it
            System.Xml.XmlElement DepositMod = requestXmlDoc.CreateElement("DepositMod");
           
            DepositQBEntryModRq.AppendChild(DepositMod);

            requestXML = requestXML.Replace("<DepositLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<DepositLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</DepositLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<DepositAdd>", "<DepositMod>");
            requestXML = requestXML.Replace("</DepositAdd>", "</DepositMod>");
            requestXML = requestXML.Replace("<DepositLineAdd>", "<DepositLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("</DepositLineAdd>", "</DepositLineMod>");
            
            requestXML = requestXML.Replace("<CashBackInfoAddREM />", string.Empty);
            requestXML = requestXML.Replace("<CashBackInfoAddREM>", string.Empty);
            requestXML = requestXML.Replace("</CashBackInfoAddREM>", string.Empty);
            requestXML = requestXML.Replace("<CashBackInfoAdd>", "<CashBackInfoMod>");
            requestXML = requestXML.Replace("</CashBackInfoAdd>", "</CashBackInfoMod>");
            requestXML = requestXML.Replace("</CashBackInfoAdd>", "</CashBackInfoMod>");

            DepositMod.InnerXml = requestXML;

            //For add request id to track error message
            string requeststring = string.Empty;

            if (requeststring != string.Empty)

                DepositQBEntryModRq.SetAttribute("requestID", "Row Number : " + rowcount.ToString());


            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").FirstChild;

            //Axis 674
            //Create TxnID aggregate and fill in field values for it
            //System.Xml.XmlElement TxnID = requestXmlDoc.CreateElement("TxnID");
            //requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertBefore(TxnID, firstChild).InnerText = txnID;
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertAfter(EditSequence, firstChild).InnerText = editSequence;
            //Axis 674 end
            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);

            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/DepositModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;
                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/DepositModRs/DepositRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfDeposit(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        public bool AppendDepositeInQuickBooks(ref string statusMessage, ref string requestText, int rowcount, string AppName, string txnID, string editSequence, List<string> txnLineIDList)
        {
            string fileName = System.Windows.Forms.Application.StartupPath + System.IO.Path.DirectorySeparatorChar.ToString() + DateTime.Now.Ticks.ToString() + ".xml";
            try
            {
                if (CommonUtilities.GetInstance().OSName.Contains(Constants.xpOSName))
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", Constants.xpPath);
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", Constants.xpPath);
                    }
                }
                else
                {
                    if (fileName.Contains("Program Files (x86)"))
                    {
                        fileName = fileName.Replace("Program Files (x86)", "Users\\Public\\Documents");
                    }
                    else
                    {
                        fileName = fileName.Replace("Program Files", "Users\\Public\\Documents");
                    }
                }
            }
            catch { }
            try
            {
                DataProcessingBlocks.ObjectXMLSerializer<DataProcessingBlocks.DepositQBEntry>.Save(this, fileName);
            }
            catch
            {
                statusMessage += "\n ";
                statusMessage += "Mapping contains invalid data.Application can not send data to QuickBooks.";
                return false;
            }

            System.Xml.XmlDocument requestXmlDoc = new System.Xml.XmlDocument();
            requestXmlDoc.Load(fileName);

            string requestXML = ((System.Xml.XmlNode)requestXmlDoc.DocumentElement).InnerXml;

            requestXmlDoc = new System.Xml.XmlDocument();

            try
            {
                System.IO.File.Delete(fileName);
            }
            catch (Exception ex)
            {
            }

            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            System.Xml.XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            System.Xml.XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create DepositModRq aggregate and fill in field values for it
            System.Xml.XmlElement DepositQBEntryModRq = requestXmlDoc.CreateElement("DepositModRq");
            inner.AppendChild(DepositQBEntryModRq);

            //Create DepositMod aggregate and fill in field values for it
            System.Xml.XmlElement DepositMod = requestXmlDoc.CreateElement("DepositMod");
            DepositQBEntryModRq.AppendChild(DepositMod);

            // Code for getting myList count  of TxnLineID
            int Listcnt = 0;
            foreach (var item in txnLineIDList)
            {
                if (item != null)
                {
                    Listcnt++;
                }
            }
           
            // Assign Existing TxnLineID for Existing Records And Assign -1 for new Record.
            List<string> GroupTxnLineID = new List<string>();
            string addString = "";

            for (int i = 0; i < txnLineIDList.Count; i++)
            {
                addString += "<TxnLineID>" + txnLineIDList[i] + "</TxnLineID></DepositLineMod><DepositLineMod>";

                XmlNode DepositLineMod = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod");
                System.Xml.XmlElement DepositLineGroupMod = requestXmlDoc.CreateElement("DepositLineMod");
                System.Xml.XmlElement TxnLineID = requestXmlDoc.CreateElement("TxnLineID");
                TxnLineID.InnerText = txnLineIDList[i];
                DepositLineGroupMod.AppendChild(TxnLineID);
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertAfter(DepositLineGroupMod, DepositLineMod.ChildNodes[i == 0 ? i : i - 1]);
            }

            XmlNode DepositMod2 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod");

            System.Xml.XmlElement DepositAdd = requestXmlDoc.CreateElement("DepositLine");

            requestXML = requestXML.Replace("<TxnID>"+ txnID + "</TxnID>", string.Empty);
            requestXML = requestXML.Replace("<DepositLineAdd />", string.Empty);
            requestXML = requestXML.Replace("<DepositLineAddREM />", string.Empty);
            requestXML = requestXML.Replace("<DepositLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("</DepositLineAddREM>", string.Empty);
            requestXML = requestXML.Replace("<DepositAdd>", "<DepositMod>");
            requestXML = requestXML.Replace("</DepositAdd>", "</DepositMod>");
            requestXML = requestXML.Replace("<DepositLineAdd>", "<DepositLineMod><TxnLineID>-1</TxnLineID>");
            requestXML = requestXML.Replace("</DepositLineAdd>", "</DepositLineMod>");
            requestXML = requestXML.Replace("<CashBackInfoAddREM />", string.Empty);
            requestXML = requestXML.Replace("<CashBackInfoAddREM>", string.Empty);
            requestXML = requestXML.Replace("</CashBackInfoAddREM>", string.Empty);
            requestXML = requestXML.Replace("<CashBackInfoAdd>", "<CashBackInfoMod>");
            requestXML = requestXML.Replace("</CashBackInfoAdd>", "</CashBackInfoMod>");
            requestXML = requestXML.Replace("</CashBackInfoAdd>", "</CashBackInfoMod>");

            string[] DepositLineModFragment;

            int indexDepositLine = requestXML.IndexOf(@"<DepositLineMod>");
            DepositLineModFragment = requestXML.Split(new string[] { "<DepositLineMod>" }, StringSplitOptions.None);
            if (DepositLineModFragment.Length > 0)
            {
                XmlDocumentFragment xfrag1 = requestXmlDoc.CreateDocumentFragment();
                xfrag1.InnerXml = DepositLineModFragment[0];
                XmlNode firstChild1 = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").FirstChild;
                requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertBefore(xfrag1, firstChild1);
                requestXML = requestXML.Replace(DepositLineModFragment[0], string.Empty);
            }

            XmlDocumentFragment xfrag = requestXmlDoc.CreateDocumentFragment();
            xfrag.InnerXml = requestXML;
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertAfter(xfrag, DepositMod2.LastChild);

            requestText = requestXmlDoc.OuterXml;

            XmlNode firstChild = requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").FirstChild;

            //Create ListID aggregate and fill in field values for it
            System.Xml.XmlElement ListID = requestXmlDoc.CreateElement("TxnID");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertBefore(ListID, firstChild).InnerText = txnID;
            //Create EditSequnce aggregate and fill in field values for it
            System.Xml.XmlElement EditSequence = requestXmlDoc.CreateElement("EditSequence");
            requestXmlDoc.SelectSingleNode("/QBXML/QBXMLMsgsRq/DepositModRq/DepositMod").InsertAfter(EditSequence, ListID).InnerText = editSequence;

            string responseFile = string.Empty;
            string resp = string.Empty;
            try
            {
                responseFile = CommonUtilities.GetInstance().SaveRequestFile(requestXmlDoc);
                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                {
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(AppName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
            
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {

                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/DepositModRs"))
                    {
                        string statusSeverity = string.Empty;
                        try
                        {
                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        }
                        catch
                        { }
                        if (statusSeverity == "Error")
                        {
                            string requesterror = string.Empty;
                            try
                            {
                                requesterror = oNode.Attributes["requestID"].Value.ToString();
                            }
                            catch
                            { }
                            string[] array_msg = oNode.Attributes["statusMessage"].Value.ToString().Split('.');
                            foreach (string s in array_msg)
                            {
                                if (s != "")
                                { statusMessage += s + ".\n"; }
                                if (s == "")
                                { statusMessage += s; }
                            }
                            statusMessage += "The Error location is " + requesterror;

                        }
                    }
                    foreach (System.Xml.XmlNode oTxn in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/DepositModRs/DepositRet"))
                    {
                        CommonUtilities.GetInstance().TxnId = oTxn["TxnID"].InnerText;
                    }
                    CommonUtilities.GetInstance().SaveResponseFile(resp, responseFile);

                }

            }
            if (resp == string.Empty)
            {
                statusMessage += "\n ";
                statusMessage += DataProcessingBlocks.CommonUtilities.GetInstance().ValidateMessageOfDeposit(this);
                statusMessage += "\n ";
                return false;
            }
            else
            {
                if (resp.Contains("statusSeverity=\"Error\""))
                {
                    return false;

                }
                else
                    return true;
            }
        }

        #endregion
    }

    public class DepositQBEntryCollection : Collection<DepositQBEntry>
    {
        //Axis 20
        /// <summary>
        /// find deposite entry by using txnId
        /// </summary>
        /// <param name="TxnID"></param>
        /// <returns></returns>
        public DepositQBEntry FindDepositEntry(string TxnID)
        {
            foreach (DepositQBEntry item in this)
            {
                if (item.TxnID == TxnID)
                {
                    return item;
                }
            }
            return null;
        }
        // end axis 20
    }

    [XmlRootAttribute("DepositLineAdd", Namespace = "", IsNullable = false)]
    public class DepositLineAdd
    {
        private EntityRef m_EntityRef;
        private AccountRef m_AccountRef;
        private string m_Memo;
        private string m_CheckNumber;
        private PaymentMethodRef m_PaymentMethodRef;
        private ClassRef m_ClassRef;
        private string m_Amount;

        #region Construtor
        public DepositLineAdd()
        {

        }

        #endregion

        #region Public Properties

        public EntityRef EntityRef
        {
            get { return m_EntityRef; }
            set { m_EntityRef = value; }
        }
        public AccountRef AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }

        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string CheckNumber
        {
            get { return m_CheckNumber; }
            set { m_CheckNumber = value; }
        }

        public PaymentMethodRef PaymentMethodRef
        {
            get { return m_PaymentMethodRef; }
            set { m_PaymentMethodRef = value; }
        }

        public ClassRef ClassRef
        {
            get { return m_ClassRef; }
            set { m_ClassRef = value; }
        }

        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }
        
        #endregion


    }

    [XmlRootAttribute("CashBackInfoAdd", Namespace = "", IsNullable = false)]
    public class CashBackInfoAdd
    {
        private AccountRef m_AccountRef;
        private string m_Memo;
        private string m_Amount;

        #region Construtor
        public CashBackInfoAdd()
        {

        }
        #endregion

        #region Public Properties

        public AccountRef AccountRef
        {
            get { return m_AccountRef; }
            set { m_AccountRef = value; }
        }
        public string Memo
        {
            get { return m_Memo; }
            set { m_Memo = value; }
        }

        public string Amount
        {
            get { return m_Amount; }
            set { m_Amount = value; }
        }

        #endregion
    }
}
