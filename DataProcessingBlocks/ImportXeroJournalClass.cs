﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using System.ComponentModel;
using EDI.Constant;
using Xero.NetStandard.OAuth2.Model;
using System.Collections.Generic;

namespace DataProcessingBlocks
{
    class ImportXeroJournalClass
    {
        private static ImportXeroJournalClass m_ImportXeroJournalClass;
        
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportXeroJournalClass()
        {

        }

        #endregion

        /// <summary>
        /// Create an instance of Import Customer class
        /// </summary>
        /// <returns></returns>
        public static ImportXeroJournalClass GetInstance()
        {
            if (m_ImportXeroJournalClass == null)
                m_ImportXeroJournalClass = new ImportXeroJournalClass();
            return m_ImportXeroJournalClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Journal QuickBooks collection </returns>

        public ManualJournals ImportXeroData(DataTable dt, ref string logDirectory)
        {
            ManualJournals listjournal = new ManualJournals();
            ManualJournal journal = new ManualJournal();

            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;

            // for conversion of date to valid format
            DateTime JournalDt = new DateTime();
            string datevalue = string.Empty;
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.LongDatePattern = "yyyy-MM-dd'T'HH':'mm':'ss'";
            dateInfo.ShortDatePattern = "yyyy-MM-dd";

            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For Journal Entry

            #region Checking Validations

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }
                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }

                    string journalnarration = "";

                    if (dt.Columns.Contains("Narration"))
                    {
                        #region Validations of Narration
                        if (dr["Narration"].ToString() != string.Empty)
                        {
                            if (dr["Narration"].ToString().Length > 500)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Narration ( " + dr["Narration"].ToString() + " )  has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        journalnarration = dr["Narration"].ToString().Substring(0, 100);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        journalnarration = dr["Narration"].ToString();
                                    }
                                }
                                else
                                {
                                    journalnarration = dr["Narration"].ToString();
                                }
                            }
                            else
                            {
                                journalnarration = dr["Narration"].ToString();
                            }
                        }
                        if (journal.Narration != null && journal.Narration.Equals(journalnarration))
                        {
                            journal.Narration = journalnarration;
                        }
                        else
                        {
                            journal = new ManualJournal();
                            journal.Narration = journalnarration;
                            journal.JournalLines = new List<ManualJournalLine>();

                            if (listjournal._ManualJournals == null)
                            {
                                listjournal._ManualJournals = new List<ManualJournal>();
                            }
                            listjournal._ManualJournals.Add(journal);
                        }
                        #endregion
                    }

                    ManualJournalLine jline = new ManualJournalLine();            

                    if (dt.Columns.Contains("JournalLinesLineAmount"))
                    {
                        #region Validations of JournalLinesLineAmount
                        double lineamt = 0;
                        if (dr["JournalLinesLineAmount"].ToString() != string.Empty)
                        {
                            if (dr["JournalLinesLineAmount"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JournalLinesLineAmount ( " + dr["JournalLinesLineAmount"].ToString() + " )  has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        lineamt = Convert.ToDouble(dr["JournalLinesLineAmount"].ToString().Substring(0, 25));
                                        jline.LineAmount = lineamt;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        lineamt = Convert.ToDouble(dr["JournalLinesLineAmount"].ToString());
                                        jline.LineAmount = (double?)lineamt;
                                    }
                                }
                                else
                                {
                                    lineamt = Convert.ToDouble(dr["JournalLinesLineAmount"].ToString());
                                    jline.LineAmount = (double?)lineamt;
                                }
                            }
                            else
                            {
                                lineamt = Convert.ToDouble(dr["JournalLinesLineAmount"].ToString());
                                jline.LineAmount = (double?)lineamt;
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("JournalLinesAccountCode"))
                    {
                        #region Validations of JournalLinesAccountCode
                        if (dr["JournalLinesAccountCode"].ToString() != string.Empty)
                        {
                            if (dr["JournalLinesAccountCode"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JournalLinesAccountCode ( " + dr["JournalLinesAccountCode"].ToString() + " )  has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        jline.AccountCode = dr["JournalLinesAccountCode"].ToString().Substring(0, 25);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        jline.AccountCode = dr["JournalLinesAccountCode"].ToString();
                                    }
                                }
                                else
                                {
                                    jline.AccountCode = dr["JournalLinesAccountCode"].ToString();
                                }
                            }
                            else
                            {
                                jline.AccountCode = dr["JournalLinesAccountCode"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("JournalLinesDescription"))
                    {
                        #region Validations of JournalLinesDescription
                        if (dr["JournalLinesDescription"].ToString() != string.Empty)
                        {
                            if (dr["JournalLinesDescription"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JournalLinesDescription ( " + dr["JournalLinesDescription"].ToString() + " )  has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        jline.Description = dr["JournalLinesDescription"].ToString().Substring(0, 25);
                                        // journal.JournalLines = mul;

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        jline.Description = dr["JournalLinesDescription"].ToString();
                                        //journal.JournalLines = mul;
                                    }
                                }
                                else
                                {
                                    jline.Description = dr["JournalLinesDescription"].ToString();
                                    //journal.JournalLines = mul;
                                }
                            }
                            else
                            {
                                jline.Description = dr["JournalLinesDescription"].ToString();
                                // journal.JournalLines = mul;
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("JournalLinesTaxType"))
                    {
                        #region Validations for JournalLinesTaxType
                        if (dr["JournalLinesTaxType"].ToString() != string.Empty)
                        {
                            if (dr["JournalLinesTaxType"].ToString().Length > 20)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This JournalTaxType ( " + dr["JournalLinesTaxType"].ToString() + " )  has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        jline.TaxType = dr["JournalLinesTaxType"].ToString();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        jline.TaxType = dr["JournalLinesTaxType"].ToString();
                                    }
                                }
                            }
                            else
                            {
                                jline.TaxType = dr["JournalLinesTaxType"].ToString();
                            }
                        }
                        #endregion
                    }

                    //Axis 141 
                    if (dt.Columns.Contains("TrackingName1"))
                    {
                        #region Validations of TrackingName1
                        Guid trackingCategoryGuid1 = new Guid();
                        string trackingCategoryName1 = "";
                        string optionName1 = "";

                        if (dr["TrackingName1"].ToString() != string.Empty)
                        {
                            if (dr["TrackingName1"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TrackingName1 ( " + dr["TrackingName1"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        trackingCategoryName1 = dr["TrackingName1"].ToString();

                                        if (dr["TrackingOption1"].ToString() != string.Empty)
                                        {
                                            optionName1 = dr["TrackingOption1"].ToString();
                                        }

                                        if (jline.Tracking == null)
                                        {
                                            jline.Tracking = new List<TrackingCategory>();
                                        }
                                        jline.Tracking.Add(new TrackingCategory()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid1,
                                            Name = trackingCategoryName1,
                                            Option = optionName1
                                        });
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        trackingCategoryName1 = dr["TrackingName1"].ToString();

                                        if (dr["TrackingOption1"].ToString() != string.Empty)
                                        {
                                            optionName1 = dr["TrackingOption1"].ToString();
                                        }

                                        if (jline.Tracking == null)
                                        {
                                            jline.Tracking = new List<TrackingCategory>();
                                        }
                                        jline.Tracking.Add(new TrackingCategory()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid1,
                                            Name = trackingCategoryName1,
                                            Option = optionName1
                                        });
                                    }
                                }
                                else
                                {
                                    trackingCategoryName1 = dr["TrackingName1"].ToString();

                                    if (dr["TrackingOption1"].ToString() != string.Empty)
                                    {
                                        optionName1 = dr["TrackingOption1"].ToString();
                                    }

                                    if (jline.Tracking == null)
                                    {
                                        jline.Tracking = new List<TrackingCategory>();
                                    }
                                    jline.Tracking.Add(new TrackingCategory()
                                    {
                                        TrackingCategoryID = trackingCategoryGuid1,
                                        Name = trackingCategoryName1,
                                        Option = optionName1
                                    });
                                }
                            }
                            else
                            {
                                trackingCategoryName1 = dr["TrackingName1"].ToString();

                                if (dr["TrackingOption1"].ToString() != string.Empty)
                                {
                                    optionName1 = dr["TrackingOption1"].ToString();
                                }

                                if (jline.Tracking == null)
                                {
                                    jline.Tracking = new List<TrackingCategory>();
                                }
                                jline.Tracking.Add(new TrackingCategory()
                                {
                                    TrackingCategoryID = trackingCategoryGuid1,
                                    Name = trackingCategoryName1,
                                    Option = optionName1
                                });
                            }
                        }


                        #endregion
                    }

                    if (dt.Columns.Contains("TrackingName2"))
                    {
                        #region Validations of TrackingName2
                        Guid trackingCategoryGuid2 = new Guid();
                        string trackingCategoryName2 = "";
                        string optionName2 = "";

                        if (dr["TrackingName2"].ToString() != string.Empty)
                        {
                            if (dr["TrackingName2"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TrackingName2 ( " + dr["TrackingName2"].ToString() + " )  is exceeded maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        trackingCategoryName2 = dr["TrackingName2"].ToString();

                                        if (dr["TrackingOption2"].ToString() != string.Empty)
                                        {
                                            optionName2 = dr["TrackingOption2"].ToString();
                                        }

                                        if (jline.Tracking == null)
                                        {
                                            jline.Tracking = new List<TrackingCategory>();
                                        }
                                        jline.Tracking.Add(new TrackingCategory()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid2,
                                            Name = trackingCategoryName2,
                                            Option = optionName2
                                        });
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        trackingCategoryName2 = dr["TrackingName2"].ToString();

                                        if (dr["TrackingOption2"].ToString() != string.Empty)
                                        {
                                            optionName2 = dr["TrackingOption2"].ToString();
                                        }

                                        if (jline.Tracking == null)
                                        {
                                            jline.Tracking = new List<TrackingCategory>();
                                        }
                                        jline.Tracking.Add(new TrackingCategory()
                                        {
                                            TrackingCategoryID = trackingCategoryGuid2,
                                            Name = trackingCategoryName2,
                                            Option = optionName2
                                        });
                                    }
                                }
                                else
                                {
                                    trackingCategoryName2 = dr["TrackingName2"].ToString();

                                    if (dr["TrackingOption2"].ToString() != string.Empty)
                                    {
                                        optionName2 = dr["TrackingOption2"].ToString();
                                    }

                                    if (jline.Tracking == null)
                                    {
                                        jline.Tracking = new List<TrackingCategory>();
                                    }
                                    jline.Tracking.Add(new TrackingCategory()
                                    {
                                        TrackingCategoryID = trackingCategoryGuid2,
                                        Name = trackingCategoryName2,
                                        Option = optionName2
                                    });
                                }
                            }
                            else
                            {
                                trackingCategoryName2 = dr["TrackingName2"].ToString();

                                if (dr["TrackingOption2"].ToString() != string.Empty)
                                {
                                    optionName2 = dr["TrackingOption2"].ToString();
                                }

                                if (jline.Tracking == null)
                                {
                                    jline.Tracking = new List<TrackingCategory>();
                                }
                                jline.Tracking.Add(new TrackingCategory()
                                {
                                    TrackingCategoryID = trackingCategoryGuid2,
                                    Name = trackingCategoryName2,
                                    Option = optionName2
                                });
                            }
                        }
                        #endregion
                    }
                    //Axis 141 ends
                    journal.JournalLines.Add(jline);

                    if (dt.Columns.Contains("Date"))
                    {
                        #region Validation of Date
                        if (dr["Date"].ToString() != "<None>" || dr["Date"].ToString() != string.Empty)
                        {

                            datevalue = dr["Date"].ToString();
                            datevalue = datevalue.Trim();
                            if (datevalue.Contains(" "))//time present
                            {
                                int pos = datevalue.IndexOf(" ");
                                datevalue = datevalue.Substring(0, pos);
                            }
                            DateTime temp_date = new DateTime();
                            bool flg1 = false;
                            string[] date_formats = { "d-M-yyyy", "d/M/yyyy", "d-MM-yyyy", "d/MM/yyyy", "dd-M-yyyy", "dd/M/yyyy", "dd-MM-yyyy", "dd/MM/yyyy",  //for date-month-year
                                                  "M-d-yyyy","M/d/yyyy", "MM-d-yyyy","MM/d/yyyy", "M-dd-yyyy", "M/dd/yyyy", "MM-dd-yyyy", "MM/dd/yyyy",    //for month-date-year
                                                  "yyyy-M-d", "yyyy/M/d", "yyyy-MM-d", "yyyy/MM/d", "yyyy-M-dd", "yyyy/M/dd", "yyyy-MM-dd", "yyyy/MM/dd"   //for year-month-date
                                                 };

                            for (int i = 0; i < date_formats.Length; i++)
                            {
                                flg1 = DateTime.TryParseExact(datevalue, date_formats[i], CultureInfo.InvariantCulture, DateTimeStyles.None, out temp_date);
                                if (flg1 == true)
                                {
                                    break;
                                }
                            }
                            if (flg1 == true)
                            {
                                JournalDt = new DateTime(temp_date.Year, temp_date.Month, temp_date.Day);
                                journal.Date = JournalDt;
                            }
                            else
                            {
                                DateTime dttest = DateTime.Today;
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Date (" + datevalue + ") is not valid for this mapping.Please Enter Date in format of (yyyy-MM-dd'T'HH':'mm':'ss).If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        journal.Date = dttest;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        journal.Date = dttest;
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("Status"))
                    {
                        #region Validations of Status
                        if (dr["Status"].ToString() != string.Empty && dr["Status"].ToString() != null)
                        {
                            string journalStatus = string.Empty;
                            if (dr["Status"].ToString().Length > 25)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Status (" + dr["Status"].ToString() + ") has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string status = dr["Status"].ToString().Substring(0, 25);
                                        journalStatus = status.ToUpper();
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string status = dr["Status"].ToString();
                                        journalStatus = status.ToUpper();
                                    }
                                }
                                else
                                {
                                    string status = dr["Status"].ToString();
                                    journalStatus = status.ToUpper();
                                }
                            }
                            else
                            {
                                string status = dr["Status"].ToString();
                                journalStatus = status.ToUpper();
                            }

                            bool checkFlag = Enum.IsDefined(typeof(ManualJournal.StatusEnum), journalStatus);
                            if (checkFlag)
                            {
                                journal.Status = (ManualJournal.StatusEnum)Enum.Parse(typeof(ManualJournal.StatusEnum), journalStatus);
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("Url"))
                    {
                        #region Validations of Url
                        if (dr["Url"].ToString() != string.Empty)
                        {
                            if (dr["Url"].ToString().Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Url ( " + dr["Url"].ToString() + " ) has exceeded the maximum length of Xero .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        journal.Url = dr["Url"].ToString().Substring(0, 25);

                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        journal.Url = dr["Url"].ToString();
                                    }
                                }
                                else
                                {
                                    journal.Url = dr["Url"].ToString();
                                }
                            }
                            else
                            {
                                journal.Url = dr["Url"].ToString();
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("LineAmountTypes"))
                    {
                        string valid_line = dr["LineAmountTypes"].ToString().ToLower().Trim();

                        #region valdation for LineAmountTypes
                        if (valid_line != string.Empty)
                        {
                            if (valid_line.Equals("exclusive"))
                            {
                                journal.LineAmountTypes = LineAmountTypes.Exclusive;
                            }
                            else if (valid_line.Equals("inclusive"))
                            {
                                journal.LineAmountTypes = LineAmountTypes.Inclusive;
                            }
                            else if (valid_line.Equals("notax"))
                            {
                                journal.LineAmountTypes = LineAmountTypes.NoTax;
                            }
                            else
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This LineAmountTypes value ( " + dr["LineAmountTypes"].ToString() + " )  is invalid .If you press cancel this record will not be added to Xero.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        journal.LineAmountTypes = LineAmountTypes.Exclusive;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        journal.LineAmountTypes = LineAmountTypes.Exclusive;
                                    }
                                }
                                else
                                {
                                    journal.LineAmountTypes = LineAmountTypes.Exclusive;
                                }

                            }
                        }

                        #endregion
                    }
                    
                }
                else
                {
                    return null;
                }
            }

            #endregion

            #region Create ParentRef

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {

                    }
                    listCount++;
                }
            }

            #endregion

            #endregion

            return listjournal;
        }
    }
}
