using System;
using System.Collections.Generic;
using System.Text;

namespace DataProcessingBlocks
{
    /// <summary>
    /// Defines the file types
    /// </summary>
    public enum FileType
    {
        XLS,
        XLSX,
        ODS,
        CSV,
        TXT,
        XML,
        IIF,
        QIF,
        QFX,
        OFX,
        QBO
    }

  public enum Module
    {
        Connection,
        About,
        Auto,
        Automate,
        Export,
        Import
    }
    public enum MappingMode
    {
        New,
        Select,
        Edit
    }

    public enum ImportErrorType
    {
        IncludeError,
        ExcludeError
    }
    public enum ImportType
    {

        JournalEntry,
        Estimate,
        SalesOrder,
        Customer,
        Vendor,
        Employee,
        Invoice,
        SalesReceipt,
        CreditMemo,
        BillPaymentCreditCard,
        BillPaymentCheck,
        Deposit,
        Bill,
        CreditCardCharge,
        CreditCardCredit,
        Check,
        CustomFieldList,
        VendorCredits,
        TimeTracking,
        ReceivePayment,
        PriceLevel,
        InventoryAdjustment,
        PurchaseOrder,
        ItemDiscount,
        ItemFixedAsset,
        ItemGroup,
        ItemInventory,
        ItemInventoryAssembly,
        ItemNonInventory,
        ItemOtherCharge,
        ItemPayment,
        ItemReceipt,
        ItemSalesTax,
        ItemSalesTaxGroup,
        ItemService,
        ItemSubtotal,
        TransferInventory,
        VehicleMileage,
        StatementCharges,
        

        Transfer,
        BuildAssembly,
        InventorySite,
        Class,
        OtherName,
        ListMerge,
        ClearedStatus,
        BillingRate,
        Account,
        Charge,
        Currency,
        CustomerMsg,
        CustomerType,
        DataExtDef,
        DateDriventTerms,
        JobType,
        ListDisplay,
        PaymentMethod,
        PayrollItemWage,
        SalesRep,
        SalesTaxCode,
        ShipMethod,
        SpecialAccount,
        SpecialItem,
        StandardTerms,
        ToDo,
        TxnDisplay,
        UnitOfMeasureSet,
        Vehicle,
       // VehicleMileage,
        VendorType,
        WorkersCompCode
        
    }



    public enum ImportTypeTXT
    {
        Journal,
        Estimate,
        SalesOrder,
        Customer,
        Vendor,
        Employee,
        Invoice,
        SalesReceipt,
        CreditMemo,
        BillPaymentCreditCard,
        BillPaymentCheck,
        Deposit,
        Bill,
        CreditCardCharge,
        CreditCardCredit,
        Check,
        CustomFieldList,
        VendorCredit,
        TimeTracking,
        ReceivePayment,
        PriceLevel,
        InventoryAdjustment,
        PurchaseOrder,
        ItemDiscount,
        ItemFixedAsset,
        ItemGroup,
        ItemInventory,
        ItemInventoryAssembly,
        ItemNonInventory,
        ItemOtherCharge,
        ItemPayment,
        ItemReceipt,
        ItemSalesTax,
        ItemSalesTaxGroup,
        ItemService,
        ItemSubtotal,
        TransferInventory,
        VehicleMileage,
        StatementCharges,
        Transfer,
        BuildAssembly,
        InventorySite,
        Class,
        OtherName,
        ListMerge,
        ClearedStatus,
        BillingRate
    }

    //axis pos 11

    public enum POSImportType
    {
        Customer,
        Employee,
        ItemInventory,
        PriceDiscount,
        PriceAdjustment,
        CustomFieldList,
        SalesOrder,
        PurchaseOrder,
        SalesReceipt,
        TimeEntry,
        Voucher,
        InventoryCostAdjustment,
        InventoryQtyAdjustment,
        TransferSlip

    }


    public enum POSImportTypeTXT
    {
        Customer,
        Employee,
        ItemInventory,
        PriceDiscount,
        PriceAdjustment,
        CustomFieldList,
        SalesOrder,
        PurchaseOrder,
        SalesReceipt,
        TimeEntry,
        Voucher,
        InventoryCostAdjustment,
        InventoryQtyAdjustment,
        TransferSlip
    }
    //Online
    public enum OnlineImportType
    {
        SalesReceipt,
        Invoice,
        Bill,
        JournalEntry,
        TimeActivity,
        PurchaseOrder,
        CashPurchase,
        CheckPurchase,
        CreditCardPurchase,
        Estimate,
        Payment,
        VendorCredit,
        BillPayment,
        CreditMemo,
        Employee,
        Items,
        Customer, //bug no. 404
        Deposit, //bug no 436
        Account,
	    RefundReceipt,
        Transfer,
        Vendor,
        Attachment
    }
    public enum OnlineImportTypeTXT
    {
        SalesReceipt,
        Invoice,
        Bill,
        JournalEntry,
        TimeActivity,
        PurchaseOrder,
        CashPurchase,
        CheckPurchase,
        CreditCardPurchase,
        Estimate,
        Payment,
        VendorCredit,
        BillPayment,
        CreditMemo,
        Employee,
        Items,
        Customer, //bug no. 404 
        Deposit, //bug no. 436
        Account,
		RefundReceipt,
        Transfer,
        Vendor,
        Attachment

    }
   
    public enum XeroImportType
    {
        Contacts,
        Journal,
        Items,
        BankSpend,
        BankReceive,
        InvoiceCustomer,
        InvoiceSupplier

    }

    public enum AccountType
    {
        Service,
        InventoryPart,
        NonInventoryPart
    }

    public enum OnlineAccountType
    {
        Service,
        Inventory,
        NonInventory
    }

    public enum ExportResultColumns
    {
        ColumnSalesCustomer,
        ColumnSalesRefNo,
        ColumnSalesQuantity,
        ColumnSalesItem,
        ColumnSalesDate
    }


    public enum MailboxFolders
    {
        Mailbox,
        Inbox,
        Outbox,
        SentItems
    }

    public enum QBXmlAccountList
    {
        AccountRet,
        FullName,
        AccountType

    }

    public enum MailboxColumns
    {
        MessageID,
        Attachment,
        To,
        From,
        Subject,
        Date,
        Status,
        ReadFlag
    }

    public enum QBXmlTransferInventoryList
    {
        TransferInventoryRet,
        TransferInventoryLineRet,
        ItemRef,
        ListID,
        TxnID,
        TxnNumber,
        TxnDate,
        RefNumber,
        FromInventorySiteRef,
        ToInventorySiteRef,
        TxnLineID,
        FullName,
        FromInventorySiteLocationRef,
        ToInventorySiteLocationRef,
        SerialNumber,
        LotNumber,
        Memo,
        QuantityTransferred,
    }

    public enum QBXmlClassList
    {
        Name,
        ClassRet,
        FullName,
        ListID,
        IsActive,
        ParentRef,
        Sublevel,
    }

    public enum QBXmlCustomerList
    {
        Name,
        CustomerRet,
        FullName,
        ListID,
        CompanyName,
        Contact,
        Phone,
        BillAddress,
        ShipAddress,
        Addr1,
        Addr2,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        IsActive,
        ParentRef,
        Sublevel,
        Salutation,
        FirstName,
        MiddleName,
        LastName,
        Addr3,
        Note,
        CustomerTypeRef,
        TermsRef,
        SalesRepRef,
        Balance,
        TotalBalance,
        SalesTaxCodeRef,
        ItemSalesTaxRef,
        //Bug no. 1428
        CreditCardInfo,
        CreditCardNumber,
        ExpirationMonth,
        ExpirationYear,
        NameOnCard,
        CreditCardAddress,
        CreditCardPostalCode,

        JobStatus,
        JobStartDate,
        JobProjectedEndDate,
        JobEndDate,
        JobTypeRef,
        Notes,
        PriceLevelRef,
        CurrencyRef,
        AltPhone,
        Fax,
        Email,
        AltContact,
        ResaleNumber,
        AccountNumber,
        CreditLimit,
        PreferredPaymentMethodRef,
        DataExtRet,


    }

    public enum QBXmlInventorySiteList
    {
        Name,
        InventorySiteRet,
        FullName,
        ListID,
        IsActive,
        ParentSiteRef,
        SiteDesc,
        Contact,
        Phone,
        Fax,
        Email,
        SiteAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        DataExtRet,
        IsDefaultSite,
    }

    //Online Axis 

    public enum QBOnlineSalesReceiptList
    {
        Id,
        SyncToken,
        CreateTime,
        LastUpdatedTime,
        LineId,
        LineNumber,
        LineDescription,
        LineAmount,
        LineDetailType,
        LineSalesItemName,
        LineSalesItemUnitPrice,
        LineSalesItemQty,
        LineSalesItemTaxCodeRef,
        TotalTax,
        CustomerRefName,
        BillId,
        BillLine1,
        BillLine2,
        BillCity,
        BillCountry,
        BillSubDivisionCode,
        BillPostalCode,
        TxnDate,
        AutoDocNumber,
        TotalAmt,
        PrintStatus,
        EmailStatus,
        Balance,
        ApplyTaxAfterDiscount,
        DiscountToAccountName,
        SalesReceipt
    }
    //Axis11 POs
    public enum QBPOSCustomerList
    {
        ListID,
        TimeCreated,
        TimeModified,
        CompanyName,
        CustomerID,
        CustomerDiscPercent,
        CustomerDiscType,
        CustomerType,
        EMail,
        IsOkToEMail,
        FirstName,
        FullName,
        IsAcceptingChecks,
        IsUsingChargeAccount,
        IsUsingWithQB,
        IsRewardsMember,
        IsNoShipToBilling,
        LastName,
        LastSale,
        Notes,
        Phone,
        Phone2,
        Phone3,
        Phone4,
        PriceLevelNumber,
        Salutation,
        StoreExchangeStatus,
        TaxCategory,
        WebNumber,
        BillAddress,
        ShipAddress,
        DefaultShipAddress,
        City,
        State,
        PostalCode,
        Country,
        Street,
        Street2,
        AddressName,
        DataExtRet,
        OwnerID,
        DataExtName,
        DataExtType,
        DataExtValue,
        CustomerRet
    }

    public enum QBPOSEmployeeList
    {
        ListID,
        TimeCreated,
        TimeModified,
        City,
        CommissionPercent,
        Country,
        EMail,
        FirstName,
        IsTrackingHours,
        LastName,
        LoginName,
        Notes,
        Phone,
        Phone2,
        Phone3,
        Phone4,
        PostalCode,
        State,
        Street,
        Street2,
        SecurityGroup,
        DataExtName,
        DataExtType,
        DataExtValue,
        EmployeeRet

    }

    public enum QBPOSVendorList
    {
        ListID,
        TimeCreated,
        TimeModified,
        City,
        CompanyName,
        Country,
        EMail,
        FirstName,
        IsInactive,
        LastName,
        Notes,
        Phone,
        Phone2,
        Phone3,
        Phone4,
        PostalCode,
        State,
        Street,
        Street2,
        DataExtName,
        DataExtType,
        DataExtValue,
        VendorRet

    }
    public enum QBPOSTimeEntryList
    {
        ListID,
        ClockInTime,
        ClockOutTime,
        CreatedBy,
        EmployeeListID,
        EmployeeLoginName,
        FirstName,
        LastName,
        StoreNumber,
        TimeEntryRet
    }

    public enum QBPOSItemInventoryList
    {
        ListID,
        TimeCreated,
        TimeModified,
        ALU,
        Attribute,
        COGSAccount,
        Cost,
        DepartmentListID,
        Desc1,
        Desc2,
        IncomeAccount,
        IsEligibleForCommission,
        IsPrintingTags,
        IsUnorderable,
        IsEligibleForRewards,
        IsWebItem,
        ItemType,
        MarginPercent,
        MarkupPercent,
        MSRP,
        OnHandStore01,
        ReorderPointStore01,
        OrderByUnit,
        OrderCost,
        Price1,
        ReorderPoint,
        SellByUnit,
        Size,
        TaxCode,
        UnitOfMeasure,
        UPC,
        VendorListID,
        Manufacturer,
        Weight,
        UnitOfMeasure1,
        UOMALU,
        UOMMSRP,
        UOMNumberOfBaseUnits,
        UOMPrice1,
        UOMUnitOfMeasure,
        UOMUPC,
        VendorInfo,
        VendALU,
        VendOrderCost,
        VendUPC,
        VendVendorListID,
        ItemInventoryRet
    }

    public enum QBPOSDepartmentList
    {
        ListID,
        TimeCreated,
        TimeModified,
        DefaultMarginPercent,
        DefaultMarkupPercent,
        DepartmentCode,
        DepartmentName,
        StoreExchangeStatus,
        TaxCode,
        DepartmentRet

    }

    public enum QBPOSPriceAdjustmentList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        DateApplied,
        DateRestored,
        PriceAdjustmentName,
        Comments,
        Associate,
        AppliedBy,
        RestoredBy,
        ItemsCount,
        PriceAdjustmentStatus,
        PriceLevelNumber,
        StoreExchangeStatus,
        PriceAdjustmentItemRet,
        LineListID,
        LineTxnLineID,
        LineNewPrice,
        LineOldPrice,
        LineOldCost,
        PriceAdjustmentRet
    }

    public enum QBPOSPriceDiscountList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        StoreExchangeStatus,
        PriceDiscountName,
        PriceDiscountReason,
        Comments,
        Associate,
        LastAssociate,
        PriceDiscountType,
        IsInactive,
        PriceDiscountPriceLevels,
        PriceDiscountXValue,
        PriceDiscountYValue,
        IsApplicableOverXValue,
        StartDate,
        StopDate,
        ItemsCount,
        LineListID,
        LineTxnLineID,
        LineUnitOfMeasure,
        PriceDiscountItemRet,
        PriceDiscountRet

    }

    public enum QBPOSSalesOrderList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        Associate,
        Cashier,
        CustomerListID,
        DepositBalance,
        Discount,
        DiscountPercent,
        Instructions,
        ItemsCount,
        PriceLevelNumber,
        PromoCode,
        Qty,
        SalesOrderNumber,
        SalesOrderStatusDesc,
        SalesOrderType,
        Subtotal,
        TaxAmount,
        TaxCategory,
        TaxPercentage,
        Total,
        TxnDate,

        ShipAddressName,
        ShipCity,
        ShipCompanyName,
        ShipCountry,
        ShipFullName,
        ShipPhone,
        ShipPhone2,
        ShipPhone3,
        ShipPhone4,
        ShipPostalCode,
        ShipBy,
        Shipping,
        ShipState,
        ShipStreet,
        ShipStreet2,

        ListID,
        TxnLineID,
        ALU,
        //     Associate,
        Attribute,
        Commission,
        Desc1,
        Desc2,
        //  Discount,
        // DiscountPercent,
        DiscountType,
        DiscountSource,
        ExtendedPrice,
        ExtendedTax,
        ItemNumber,
        NumberOfBaseUnits,
        Price,
        // PriceLevelNumber,
        QtySold,
        SerialNumber,
        Size,
        //  TaxAmount,
        TaxCode,
        // TaxPercentage,
        UnitOfMeasure,
        UPC,
        Manufacturer,
        Weight,
        ShippingInformation,
        SalesOrderItemRet,
        SalesOrderRet
    }


    public enum QBPOSInventoryCostAdjustmentList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        Associate,
        Comments,
        CostDifference,
        HistoryDocStatus,
        InventoryAdjustmentNumber,
        ItemsCount,
        NewCost,
        OldCost,
        Reason,
        StoreExchangeStatus,
        StoreNumber,
        TxnDate,
        TxnState,
        Workstation,
        ListID,
        NumberOfBaseUnits,
        UnitOfMeasure,
        InventoryCostAdjustmentRet,
        InventoryCostAdjustmentItemRet
    }

    public enum QBPOSInventoryQtyAdjustmentList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        Associate,
        Comments,
        CostDifference,
        HistoryDocStatus,
        InventoryAdjustmentNumber,
        InventoryAdjustmentSource,
        ItemsCount,
        NewQuantity,
        OldQuantity,
        QtyDifference,
        Reason,
        StoreExchangeStatus,
        StoreNumber,
        TxnDate,
        TxnState,
        Workstation,
        ListID,
        NumberOfBaseUnits,
        UnitOfMeasure,
        SerialNumber,
        InventoryQtyAdjustmentRet,
        InventoryQtyAdjustmentItemRet
    }
    public enum QBPOSPurchaseOrderList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        Associate,
        CancelDate,
        CompanyName,
        Discount,
        DiscountPercent,
        Fee,
        Instructions,
        PurchaseOrderNumber,
        PurchaseOrderStatusDesc,
        QtyDue,
        QtyOrdered,
        QtyReceived,
        SalesOrderNumber,
        ShipToStoreNumber,
        StartShipDate,
        StoreNumber,
        Subtotal,
        Terms,
        TermsDiscount,
        TermsDiscountDays,
        TermsNetDays,
        Total,
        TxnDate,
        UnfilledPercent,
        VendorListID,
        VendorCode,
        ListID,
        ALU,
        Attribute,
        Cost,
        Desc1,
        Desc2,
        ExtendedCost,
        NumberOfBaseUnits,
        ItemNumber,
        Qty,
        LineQtyReceived,
        Size,
        UnitOfMeasure,
        UPC,
        TxnLineID,
        PurchaseOrderItemRet,
        PurchaseOrderRet

    }

    public enum QBPOSSalesReceiptList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        Cashier,
        Comments,
        CustomerListID,
        ItemsCount,
        PromoCode,
        SalesOrderTxnID,
        SalesReceiptNumber,
        SalesReceiptType,
        ShipDate,
        StoreNumber,
        Subtotal,
        TaxCategory,
        TenderType,
        Total,
        TrackingNumber,
        TxnDate,
        TxnState,
        Workstation,

        ListID,
        ALU,
        Associate,
        Attribute,
        Commission,
        Cost,
        Desc1,
        Desc2,
        Discount,
        DiscountPercent,
        DiscountType,
        DiscountSource,
        ExtendedPrice,
        ExtendedTax,
        ItemNumber,
        NumberOfBaseUnits,
        Price,
        PriceLevelNumber,
        Qty,
        SerialNumber,
        Size,
        TaxAmount,
        TaxCode,
        TaxPercentage,
        UnitOfMeasure,
        UPC,
        WebDesc,
        Manufacturer,
        Weight,

        TenderAmount,
        TipAmount,
        CheckNumber,
        CardName,
        Cashback,
        GiftCertificateNumber,

        SalesReceiptRet,
        SalesReceiptItemRet,
        TenderAccountRet,
        TenderCashRet,
        TenderCheckRet,
        TenderCreditCardRet,
        TenderDebitCardRet,
        TenderDepositRet,
        TenderGiftRet,
        TenderGiftCardRet

    }

    public enum QBPOSVoucherList
    {
        TxnID,
        TimeCreated,
        TimeModified,
        Comments,
        CompanyName,
        Discount,
        DiscountPercent,
        Fee,
        Freight,
        HistoryDocStatus,
        ItemsCount,
        PayeeCode,
        PayeeListID,
        PayeeName,
        StoreExchangeStatus,
        StoreNumber,
        Subtotal,
        TermsDiscount,
        TermsDiscountDays,
        TermsNetDays,
        Total,
        TotalQty,
        TxnDate,
        TxnState,
        VendorCode,
        VendorListID,
        VoucherNumber,
        VoucherType,
        Workstation,
        VoucherItemRet,
        ListID,
        ALU,
        Attribute,
        Cost,
        Desc1,
        Desc2,
        ExtendedCost,
        ItemNumber,
        NumberOfBaseUnits,
        OriginalOrderQty,
        QtyReceived,
        SerialNumber,
        Size,
        UPC,
        VoucherRet

    }
    //Axis 9.0

    public enum QBXmlDepositList
    {
        DepositRet,
        TxnID,
        TimeCreated,
        TimeModified,
        EditSequence,
        TxnDate,
        DepositToAccountRef,
        Memo,
        FullName,
        CurrencyRef,
        ExchangeRate,
        DepositTotalInHomeCurrency,
        CashBackInfoRet,
        AccountRef,
        TxnLineID,
        Amount,
        ListID,
        EntityRef,
        CheckNumber,
        PaymentMethodRef,
        ClassRef,
        DepositLineRet,
        DepositTotal
    }

    public enum QBXmlBillingRateList
    {
        // For BillingRateRet:
        BillingRateRet,
        ListID,
        TimeCreated,
        TimeModified,
        EditSequence,
        Name,
        BillingRateType,
        FixedBillingRate,

        //For BillingRatePerItemRet:
        BillingRatePerItemRet,
        ItemRef,
        FullName,
        CustomRate,
        CustomRatePercent
    }

    //new changes in version 6.0

    public enum QBXmlVendorList
    {
        VendorRet,
        //FullName,
        FirstName,
        LastName,
        ListID,
        CompanyName,
        Contact,
        Phone,
        VendorAddress,
        Addr1,
        Addr2,
        City,
        State,
        PostalCode,
        Country,
        Email,
        DataExtRet

    }

    //new changtes in version 6.0
    public enum QBXmlEmployeeList
    {
        EmployeeRet,
        //FullName,
        Name,
        ListID,
        CompanyName,
        Contact,
        Phone,
        EmployeeAddress,
        Addr1,
        Addr2,
        City,
        State,
        PostalCode,
        Country,
        Email
    }

    //new changes in version 6.0
    public enum QBXmlOtherList
    {
        OtherNameRet,
        Name,
        ListID,
        CompanyName,
        Contact,
        Phone,
        OtherNameAddress,
        Addr1,
        Addr2,
        City,
        State,
        PostalCode,
        Country,
        Email
    }

    public enum QBXmlItemList
    {
        ItemNonInventoryRet,
        ItemRet,
        ParentRef,
        ItemDiscountRet,
        ItemPaymentRet,
        ItemOtherChargeRet,
        ManufacturerPartNumber,
        PurchaseCost,
        ReorderPoint,
        QuantityOnHand,
        AverageCost,
        QuantityOnOrder,
        QuantityOnSalesOrder,
        SalesTaxCodeRef,
        IncomeAccountRef,
        COGSAccountRef,
        PrefVendorRef,
        AssetAccountRef,
        ItemInventoryAssemblyRet,
        ItemInventoryRet,
        ItemServiceRet,
        ItemFixedAssetRet,
        ItemSalesTaxRet,
        ItemSalesTaxGroupRet,
        ItemGroupRet,
        SalesAndPurchase,
        FixedAssetSalesInfo,
        ItemSubtotalRet,
        FullName,
        Name,
        BarCodeValue,
        Sublevel,
        ListID,
        TimeCreated,
        TimeModified,
        DataExtName,
        //UnitOfMeasureFullName,
        SalesDesc,
        PurchaseDesc,
        SalesPrice,
        DataExtRet,
        DataExtValue,
        UnitOfMeasureSetRef,
        IsActive
    }



    public enum QBXmlNonInventoryItemList
    {
        ItemNonInventoryRet,
        ListID,
        TimeCreated,
        TimeModified,
        Name,
        FullName,
        BarCodeValue,
        IsActive,
        ParentRef,
        Sublevel,
        ManufacturerPartNumber,
        UnitOfMeasureSetRef,
        SalesTaxCodeRef,
        SalesOrPurchase,
        Desc,
        Price,
        AccountRef,
        SalesDesc,
        SalesPrice,
        IncomeAccountRef,
        PurchaseDesc,
        PurchaseCost,
        ExpenseAccountRef,
        PrefVendorRef,
        SalesAndPurchase,
        DataExtRet,
        DataExtName,
        DataExtType,
        DataExtValue

    }
    //Axis 9.0
    public enum QBXmlItemInventoryAssemblyList
    {
        ItemInventoryAssemblyRet,
        ListID,
        TimeCreated,
        TimeModified,
        Name,
        FullName,
        BarCodeValue,
        IsActive,
        ParentRef,
        Sublevel,
        UnitOfMeasureSetRef,
        SalesTaxCodeRef,
        SalesDesc,
        SalesPrice,
        IncomeAccountRef,
        PurchaseDesc,
        PurchaseCost,
        COGSAccountRef,
        PrefVendorRef,
        AssetAccountRef,
        BuildPoint,
        QuantityOnHand,
        AverageCost,
        QuantityOnOrder,
        QuantityOnSalesOrder,
        ItemInventoryAssemblyLine,
        ItemInventoryRef,
        ItemInventoryListID,
        Quantity,
        DataExtRet,
        DataExtName,
        DataExtType,
        DataExtValue

    }


    public enum QBXmlItemServiceList
    {
        ItemServiceRet,
        ListID,
        TimeCreated,
        TimeModified,
        Name,
        FullName,
        BarCodeValue,
        IsActive,
        ParentRef,
        Sublevel,
        UnitOfMeasureSetRef,
        SalesTaxCodeRef,
        SalesOrPurchase,
        Desc,
        Price,
        PricePercentage,
        AccountRef,
        SalesDesc,
        SalesPrice,
        IncomeAccountRef,
        PurchaseDesc,
        PurchaseCost,
        PurchaseTaxCodeRef,
        ExpenseAccountRef,
        PrefVendorRef,
        SalesAndPurchase,
        DataExtRet,
        DataExtName,
        DataExtType,
        DataExtValue

    }

    //public enum QBXmlSalesOrderList
    //{
    //    SalesOrderRet,
    //    CustomerRef,
    //    ListID,
    //    FullName,
    //    RefNumber,
    //    ShipDate,
    //    TxnDate,
    //    ShipAddress,
    //    Addr1,
    //    Addr2,
    //    City,
    //    State,
    //    PostalCode,
    //    Country,
    //    Note,
    //    ShipMethodRef,
    //    PONumber,
    //    Memo,
    //    SalesOrderLineRet,
    //    TxnLineID,
    //    ItemRef,
    //    Quantity,
    //    TxnID
    //}

    //new changes in version 6.0 by dj
    public enum QBXmlSalesOrderList
    {
        SalesOrderRet,
        CustomerRef,
        ListID,
        FullName,
        RefNumber,
        ShipDate,
        TxnDate,
        BillAddress,
        ShipAddress,
        Addr1,
        Addr2,
        Addr3,
        City,
        State,
        PostalCode,
        Country,
        Note,
        ShipMethodRef,
        PONumber,
        Memo,
        SalesOrderLineRet,
        TxnLineID,
        ItemRef,
        Quantity,
        TxnID,
        ClassRef,
        TemplateRef,
        TermsRef,
        DueDate,
        SalesRepRef,
        FOB,
        Subtotal,
        ItemSalesTaxRef,
        SalesTaxPercentage,
        SalesTaxTotal,
        TotalAmount,
        CurrencyRef,
        ExchangeRate,
        TotalAmountInHomeCurrency,
        IsManuallyClosed,
        IsFullyInvoiced,
        CustomerMsgRef,
        IsToBePrinted,
        IsToBeEmailed,
        CustomerSalesTaxCodeRef,
        Other,
        Desc,
        UnitOfMeasure,
        Amount,
        InventorySiteRef,
        SalesTaxCodeRef,
        SalesOrderLineGroupRet,
        ItemGroupRef,
        LinkedTxn,
        Invoiced,

        //For InvoiceLineRet
        InvoiceLineRet,
        OverrideUOMSetRef,
        Rate,
        ServiceDate,
        Other1,
        Other2,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends
        //InvoiceLineGroupRet
        InvoiceLineGroupRet,
        IsPrintItemsInGroup,
        DataExtRet
    }

    /// <summary>
    /// Define List of Fields used for OtherList.
    /// </summary>
    public enum QBOtherList
    {
        OtherNameRet,
        FullName,
        ListID,
        Name,
        IsActive,
        CompanyName,
        Salutation,
        FirstName,
        MiddleName,
        LastName,
        OtherNameAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        Note,
        Phone,
        AltPhone,
        Fax,
        Email,
        Contact,
        AltContact,
        AccountNumber,
        Notes,
        DataExtRet,
        OwnerID,
        DataExtName,
        DataExtType,
        ExternalGUID


    }





    /// <summary>
    /// Define List of Fields used for VendorList.
    /// </summary>
    public enum QBVendorList
    {
        VendorRet,
        FullName,
        ListID,
        Name,
        IsActive,
        CompanyName,
        Salutation,
        FirstName,
        MiddleName,
        LastName,
        VendorAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        Note,
        Phone,
        AltPhone,
        Fax,
        Email,
        Contact,
        AltContact,
        NameOnCheck,
        AccountNumber,
        Notes,
        VendorTypeRef,
        TermsRef,
        CreditLimit,
        //Bug No.396
        VendorTaxIdent,
        //
        IsVendorEligibleFor1099,
        Balance,
        BillingRateRef,
        DataExtRet,
        OwnerID,
        DataExtName,
        DataExtType,
        ExternalGUID,
        PrefillAccountRef,
        CurrencyRef



    }

    public enum QBClassList
    {
        ClassRet,
        ListID,
        Name,
        FullName,
        IsActive,
        CompanyName,
        ParentRefFullName
    }
    /// <summary>
    /// Define List of Fields used for EmployeeList.
    /// </summary>
    public enum QBEmployeeList
    {
        EmployeeRet,
        ListID,
        FullName,
        Name,
        IsActive,
        Salutation,
        FirstName,
        MiddleName,
        LastName,
        EmployeeAddress,
        Addr1,
        Addr2,
        City,
        State,
        PostalCode,
        PrintAs,
        Phone,
        Mobile,
        Pager,
        PagerPin,
        AltPhone,
        Fax,
        SSN,
        Email,
        EmployeeType,
        Gender,
        HiredDate,
        ReleasedDate,
        BirthDate,
        AccountNumber,
        Notes,
        BillingRateRef,
        EmployeePayrollInfo,
        PayPeriod,
        ClassRef,
        ClearEarnings,
        Earnings,
        PayrollItemWageRef,
        Rate,
        IsUsingTimeDataToCreatePaychecks,
        UseTimeDataToCreatePaychecks,
        ExternalGUID,
        SickHours,
        HoursAvailable,
        AccrualPeriod,
        HoursAccrued,
        MaximumHours,
        IsResettingHoursEachNewYear,
        HoursUsed,
        AccrualStartDate,
        VacationHours,
        DataExtRet,
        OwnerID,
        DataExtName,
        DataExtType

    }

    /// <summary>
    /// Define List of Fields used for InvoiceList.
    /// </summary>
    public enum QBInvoicesList
    {
        InvoiceRet,
        TxnID,
        TxnNumber,
        CustomerRef,
        ListID,
        FullName,
        ClassRef,
        ARAccountRef,
        TemplateRef,
        TxnDate,
        RefNumber,
        BillAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        ShipAddress,
        IsPending,
        IsFinanceCharge,
        PONumber,
        TermsRef,
        DueDate,
        SalesRepRef,
        FOB,
        ShipDate,
        ShipMethodRef,
        ItemSalesTaxRef,
        SalesTaxPercentage,
        AppliedAmount,
        BalanceRemaining,
        CurrencyRef,
        ExchangeRate,
        BalanceRemainingInHomeCurrency,
        Memo,
        IsPaid,
        CustomerMsgRef,
        IsToBePrinted,
        IsToBeEmailed,
        IsTaxIncluded,
        CustomerSalesTaxCodeRef,
        SuggestedDiscountAmount,
        SuggestedDiscountDate,
        Other,
        LinkedTxn,

        //For InvoiceLineRet
        InvoiceLineRet,
        TxnLineID,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        OverrideUOMSetRef,
        Rate,
        Amount,
        InventorySiteRef,
        ServiceDate,
        SalesTaxCodeRef,
        Other1,
        Other2,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends
        //InvoiceLineGroupRet
        InvoiceLineGroupRet,
        ItemGroupRef,
        TotalAmount,
        IsPrintItemsInGroup,
        DataExtRet
    }

    /// <summary>
    /// Define List of Fields used for SalesReceipt.
    /// </summary>
    public enum QBSalesReceiptList
    {
        SalesReceiptRet,
        TxnID,
        TxnNumber,
        ListID,
        FullName,
        CustomerRef,
        ClassRef,
        TemplateRef,
        TxnDate,
        RefNumber,
        BillAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        ShipAddress,
        IsPending,
        CheckNumber,
        PaymentMethodRef,
        DueDate,
        SalesRepRef,
        ShipDate,
        ShipMethodRef,
        FOB,
        ItemSalesTaxRef,
        Memo,
        SalesTaxPercentage,
        TotalAmount,
        CurrencyRef,
        ExchangeRate,
        CustomerMsgRef,
        IsToBePrinted,
        IsToBeEmailed,
        IsTaxIncluded,
        CustomerSalesTaxCodeRef,
        DepositToAccountRef,
        Other,

        //For SalesReceiptLineRet
        SalesReceiptLineRet,
        TxnLineID,
        ItemRef,
        Desc,
        UnitOfMeasure,
        OverrideUOMSetRef,
        Quantity,
        Rate,
        Amount,
        InventorySiteRef,
        ServiceDate,
        SalesTaxCodeRef,
        Other1,
        Other2,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends
        //For SalesReceiptGroupLineRet
        SalesReceiptLineGroupRet,
        ItemGroupRef,
        IsPrintItemsInGroup,
        DataExtRet
    }
	
  //Improvment no 502
    public enum QBOnlineRefundReceiptList
    {
        SalesReceiptRet,
        TxnID,
        TxnNumber,
        ListID,
        FullName,
        CustomerRef,
        ClassRef,
        TemplateRef,
        TxnDate,
        RefNumber,
        BillAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        ShipAddress,
        IsPending,
        CheckNumber,
        PaymentMethodRef,
        DueDate,
        SalesRepRef,
        ShipDate,
        ShipMethodRef,
        FOB,
        ItemSalesTaxRef,
        Memo,
        SalesTaxPercentage,
        TotalAmount,
        CurrencyRef,
        ExchangeRate,
        CustomerMsgRef,
        IsToBePrinted,
        IsToBeEmailed,
        IsTaxIncluded,
        CustomerSalesTaxCodeRef,
        DepositToAccountRef,
        Other,

        //For SalesReceiptLineRet
        SalesReceiptLineRet,
        TxnLineID,
        ItemRef,
        Desc,
        UnitOfMeasure,
        OverrideUOMSetRef,
        Quantity,
        Rate,
        Amount,
        InventorySiteRef,
        ServiceDate,
        SalesTaxCodeRef,
        Other1,
        Other2,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends
        //For SalesReceiptGroupLineRet
        SalesReceiptLineGroupRet,
        ItemGroupRef,
        IsPrintItemsInGroup,
        DataExtRet
    }

    /// <summary>
    /// Define List of Fields used for CreditMemo.
    /// </summary>
    public enum QBCreditMemoList
    {
        CreditMemoRet,
        TxnID,
        TxnNumber,
        CustomerRef,
        FullName,
        ClassRef,
        ARAccountRef,
        TemplateRef,
        TxnDate,
        RefNumber,
        BillAddress,
        Addr1,
        Addr2,
        City,
        State,
        PostalCode,
        Country,
        ShipAddress,
        IsPending,
        PONumber,
        TermsRef,
        DueDate,
        SalesRepRef,
        FOB,
        ShipDate,
        ShipMethodRef,
        ItemSalesTaxRef,
        SalesTaxPercentage,
        TotalAmount,
        CreditRemaining,
        CurrencyRef,
        ExchangeRate,
        CreditRemainingInHomeCurrency,
        Memo,
        CustomerMsgRef,
        IsToBePrinted,
        IsToBeEmailed,
        IsTaxIncluded,
        CustomerSalesTaxCodeRef,
        Other,
        LinkedTxn,

        //For CreditMemoLineRet
        CreditMemoLineRet,
        TxnLineID,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        OverrideUOMSetRef,
        Rate,
        Amount,
        InventorySiteRef,
        ServiceDate,
        SalesTaxCodeRef,
        Other1,
        Other2,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends
        //CreditMemoLineGroupRet
        CreditMemoLineGroupRet,
        ItemGroupRef,
        IsPrintItemsInGroup,

        //For other fields
        GroupLineOther1,
        GroupLineOther2,
        DataExtRet
    }

    /// <summary>
    /// Define List of Fields used for ReceivePayment.
    /// </summary>
    public enum QBReceivePayments
    {
        ReceivePaymentRet,
        TxnID,
        TxnNumber,
        FullName,
        CustomerRef,
        ARAccountRef,
        TxnDate,
        RefNumber,
        TotalAmount,
        CurrencyRef,
        ExchangeRate,
        TotalAmountInHomeCurrency,
        PaymentMethodRef,
        Memo,
        DepositToAccountRef,
        UnusedPayment,
        UnusedCredits,

        //For AppliedToTxnRet
        AppliedToTxnRet,
        TxnType,
        BalanceRemaining,
        Amount,
        DiscountAmount,
        DiscountAccountRef
    }

    /// <summary>
    /// Define List of Fields for Bill Payment Check.
    /// </summary>
    public enum BillPayCheck
    {
        BillPaymentCheckRet,
        TxnID,
        TxnNumber,
        TxnDate,
        FullName,
        PayeeEntityRef,
        APAccountRef,
        BankAccountRef,
        CurrencyRef,
        ExchangeRate,
        AmountInHomeCurrency,
        RefNumber,
        Memo,
        Address,
        Addr1,
        Addr2,
        City,
        State,
        PostalCode,
        Country,
        IsToBePrinted,
        AppliedToTxnRet,
        TxnType,
        BalanceRemaining,
        Amount,
        DiscountAmount,
        DiscountAccountRef,
        ATRTxnAmount
    }

    public enum BillPayCreditCardList
    {
        BillPaymentCreditCardRet,
        TxnID,
        TxnNumber,
        TxnDate,
        FullName,
        PayeeEntityRef,
        APAccountRef,
        RefNumber,
        CurrencyRef,
        ExchangeRate,
        AmountInHomeCurrency,
        Memo,
        Amount,
        AppliedToTxnRet,
        CreditCardAccountRef,

        ATRTxnID,
        ATRRefNumber,
        ATRAmount
    }

    /// <summary>
    /// Define List of Fields for BillList.
    /// </summary>
    public enum QBBillList
    {
        BillRet,
        TxnID,
        VendorRef,
        FullName,
        CustomerRef,
        APAccountRef,
        TxnDate,
        DueDate,
        AmountDue,
        CurrencyRef,
        ExchangeRate,
        AmountDueInHomeCurrency,
        RefNumber,
        TermsRef,
        Memo,
        IsTaxIncluded,
        SalesTaxCodeRef,
        IsPaid,
        LinkedTxn,
        OpenAmount,


        //Axis Bug No 144 
        TxnType,
        LinkType,
        //End Axis Bug No 144                       

        //For ExpenseLineRet, optional repeat
        ExpenseLineRet,
        TxnLineID,
        AccountRef,
        Amount,
        ClassRef,
        BillableStatus,
        SalesRepRef,

        //For ItemLineRet
        ItemLineRet,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        OverrideUOMSetRef,
        Cost,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends

        //For ItemGroupLineRet
        ItemGroupLineRet,
        ItemGroupRef,
        TotalAmount,
        DataExtRet
    }
    //bug 485 axis 12.0
    #region bug 485 axis 12.0
    
    public enum QBVehiclemileageList
    {
        VehicleMileageRet,
        TxnLineID,
        TxnID,
        TimeCreated,
        TimeModified,
        VehicleRef,
        FullName,
        CustomerRef,
        ItemRef,
        ClassRef,
        TripStartDate,
        TripEndDate,
        OdometerStart,
        OdometerEnd,
        TotalMiles,
        Notes,
        BillableStatus,
        StandardMileageRate,
        StandardMileageTotalAmount,
        BillableRate,
        BillableAmount,
          }

    public enum QBItemReceiptList
    {
        ItemReceiptRet,
        TxnLineID,
        TxnID,
        TxnNumber,
        FullName,
        ClassRef,
        ItemRef,
        CustomerRef,
        APAccountRef,
        LiabilityAccountRef,
        TxnDate,
        TotalAmount,
        CurrencyRef,
        ExchangeRate,
        TotalAmountInHomeCurrency,
        RefNumber,
        Memo,
        ExternalGUID,

        DataExtRet,
        DataExtName,
        DataExtType,
        DataExtValue,

        OwnerID,
        Name,
        Type,
        Value,
        LinkType,

        LinkedTxn,
        TxnType,

        ExpenseLineRet,
        AccountRef,
        Cost,
        Amount,
        SalesTax,
        SalesRepRef,


        ItemLineRet,
        InventorySiteRef,
        InventorySiteLocationRef,
        SerialNumber,
        LotNumber,
        Quantity,
        UnitOfMeasure,
        OverrideUOMSetRef,



        BillableStatus,


        ItemGroupLineRet,
        ItemGroupRef,
        Desc,
        ExpensesLineDataExtDataExtValue,
        ExpensesLineDataExtDataExtName,
        ExpensesLineDataExtDataExtType,
        VendorRef

    }

#endregion
    /// <summary>
    /// Define List of Fields for CheckList
    /// </summary>
    public enum QBCheckList
    {
        CheckRet,
        TxnID,
        ListID,
        FullName,
        AccountRef,
        PayeeEntityRef,
        RefNumber,
        TxnDate,
        Amount,
        CurrencyRef,
        ExchangeRate,
        AmountInHomeCurrency,
        Memo,
        Address,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        IsToBePrinted,
        IsTaxIncluded,
        LinkedTxn,

        //For ExpenseLineRet
        ExpenseLineRet,
        TxnLineID,
        CustomerRef,
        ClassRef,
        SalesTaxCodeRef,
        BillableStatus,
        SalesRepRef,

        //ForItemLineRet
        ItemLineRet,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        Cost,
        Freight,
        Insurance,
        Discount,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends

        //For GroupLineRet
        ItemGroupLineRet,
        ItemGroupRef,
        DataExtRet
    }

    /// <summary>
    /// Define fields of CreditCardCharge
    /// </summary>
    public enum QBCreditCardChargeList
    {
        CreditCardChargeRet,
        TxnID,
        ListID,
        FullName,
        AccountRef,
        PayeeEntityRef,
        TxnDate,
        Amount,
        CurrencyRef,
        ExchangeRate,
        AmountInHomeCurrency,
        RefNumber,
        Memo,
        IsTaxIncluded,
        SalesTaxCodeRef,

        //For ExpenseLineRet
        ExpenseLineRet,
        TxnLineID,
        CustomerRef,
        ClassRef,
        BillableStatus,
        SalesRepRef,
        //ForItemLineRet
        ItemLineRet,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        Cost,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends

        //For GroupLineRet
        ItemGroupLineRet,
        ItemGroupRef,
        DataExtRet

    }



    /// <summary>
    /// Define fields of CreditCardCredit
    /// </summary>
    public enum QBCreditCardCreditList
    {
        CreditCardCreditRet,
        TxnID,
        ListID,
        FullName,
        AccountRef,
        PayeeEntityRef,
        TxnDate,
        Amount,
        CurrencyRef,
        ExchangeRate,
        AmountInHomeCurrency,
        RefNumber,
        Memo,
        IsTaxIncluded,
        SalesTaxCodeRef,

        //For ExpenseLineRet
        ExpenseLineRet,
        TxnLineID,
        CustomerRef,
        ClassRef,
        BillableStatus,

        //ForItemLineRet
        ItemLineRet,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        Cost,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends

        //For GroupLineRet
        ItemGroupLineRet,
        ItemGroupRef

    }

    public enum QBInventorySiteList
    {
        IsDefaultSite,
        InventorySiteRet,
        ListID,
        TimeCreated,
        TimeModified,
        EditSequence,
        Name,
        FullName,
        IsActive,
        ParentSiteRef,
        SiteDesc,
        Contact,
        Phone,
        Fax,
        Email,
        SiteAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        SiteAddressBlock

    }

    // Axis 12
    public enum QBVendorCreditList
    {
        VendorCreditRet,
        TxnID,
        ListID,
        FullName,
        VendorRef,
        APAccountRef,
        TxnDate,
        CreditAmount,
        CurrencyRef,
        ExchangeRate,
        CreditAmountInHomeCurrency,
        RefNumber,
        Memo,
        ExternalGUID,
        AccountRef,
        Amount,
        //For ExpenseLineRet
        ExpenseLineRet,
        TxnLineID,
        CustomerRef,
        ClassRef,
        BillableStatus,
        SalesRepRef,
        //ForItemLineRet
        ItemLineRet,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        Cost,
        // axis 10.0 changes
        SerialNumber,
        LotNumber,
        //Ax is 10.0 changes ends

        //For GroupLineRet
        ItemGroupLineRet,
        ItemGroupRef

    }

    public enum QBInventoryAdjustmentList
    {
        InventoryAdjustmentRet,      
        TxnID,
        ListID,
        FullName, 
        AccountRef, InventorySiteRef,
        TxnDate ,
        RefNumber ,
        CustomerRef,
        ClassRef,
        Memo ,
        ExternalGUID ,
        InventoryAdjustmentLineRet,
        TxnLineID, ItemRef, SerialNumber,  LotNumber ,
        InventorySiteLocationRef,
        QuantityDifference,
        ValueDifference
    }

    /// <summary>
    /// Define Fields of Journal Entries.
    /// </summary>
    public enum QBJournalEntry
    {
        JournalEntryRet,
        TxnID,
        TxnNumber,
        TxnDate,
        RefNumber,
        IsAdjustment,
        IsHomeCurrencyAdjustment,
        IsAmountEnteredInHomeCurrecny,
        CurrencyRef,
        FullName,
        ExchangeRate,
        JournalDebitLine,
        TxnLineID,
        AccountRef,
        Amount,
        Memo,
        EntityRef,
        ClassRef,
        BillableStatus,
        JournalCreditLine,
        ItemSalesTaxRef

    }
    /// <summary>
    /// Define fields for Time Entries.
    /// </summary>
    public enum QBTimeEntry
    {
        TimeTrackingRet,
        TxnID,
        TxnNumber,
        TxnDate,
        EntityRef,
        FullName,
        CustomerRef,
        ItemServiceRef,
        Duration,
        ClassRef,
        PayrollItemWageRef,
        Notes,
        BillableStatus,
        IsBillable
    }


    public enum QBPriceLevelList
    {
        // For PriceLevelRet:
        PriceLevelRet,
        ListID,
        TimeCreated,
        TimeModified,
        EditSequence,
        Name,
        IsActive,
        PriceLevelType,
        PriceLevelFixedPercentage,

        //For PriceLevelPerItemRet:
        PriceLevelPerItemRet,
        ItemRef,
        FullName,
        CustomPrice,
        CustomPricePercent,
        CurrencyRef,
    }


    public enum QBXmlPurchaseOrderList
    {
        PurchaseOrderRet,
        RefNumber,
        TxnID,
        TxnNumber,
        FOB,
        ShipDate,
        TxnDate,
        DueDate,
        TotalAmount,
        VendorAddress,
        ShipAddress,
        Addr1,
        Addr2,
        Addr3,
        City,
        State,
        PostalCode,
        Country,
        Note,
        CurrencyRef,
        ExchangeRate,
        ShipMethodRef,
        TotalAmountInHomeCurrency,
        ServiceDate,
        ReceivedQuantity,
        IsManuallyClosed,
        IsFullyReceived,
        Memo,
        VendorMsg,
        CustomerMsgRef,
        IsToBePrinted,
        IsToBeEmailed,
        VendorRef,
        Other1,
        Other2,
        LinkedTxn,
        TermsRef,
        ClassRef,
        InventorySiteRef,
        ShipToEntityRef,
        TemplateRef,
        ListID,
        ExpectedDate,
        PurchaseOrderLineRet,
        PurchaseOrderLineGroupRet,
        ItemRef,
        ItemGroupRef,
        FullName,
        Desc,
        TxnLineID,
        CustomerRef,
        OverrideUOMSetRef,
        Rate,
        Amount,
        Quantity,
        UnitOfMeasure,
        ManufacturerPartNumber,
        DataExtRet

    }
	// Bug 551
    public enum QBItemGroupList
    {
        ItemGroupRet,
        ListID,
        TimeCreated,
        TimeModified,
        EditSequence,
        Name,
        BarCodeValue,
        IsActive,
        ItemDesc,


        UnitOfMessureSetRefListID,
        UnitOfMessureSetRefFullName,

        IsPrintItemsInGroup,
        SpecialItemType,
        ExternalGUID,


        ItemGroupLine,
        ItemRef,

        DataExtOwnerID,
        DataExtName,
        DataExtType,
        DataExtValue,
        UnitOfMeasure,
        Quantity,
        FullName,
        ItemGroupRef,
        DataExtRet,
       
    }

    public enum QBXmlInventorySiteLocationList
    {
        InventorySiteLocationRet, 
        ListID
    }

    public enum ItemColumns
    {
        ItemName,
        SalesDesc,
        PurchaseDesc,
        SalesPrice,
        SalesUnitMeasure
    }

    public enum SalesOrderColumns
    {
        InvoiceNumber,
        CustomerID,
        InvoiceDate,
        ItemNumber,
        Quantity
    }

    public enum PurchaseOrderColumns
    {
        PurchaseNumber,
        SupplierID,
        ExpectedDate,
        ItemNumber,
        Quantity
    }

    public enum TabPages
    {
        tabPageConnection,
        tabPageFileImport,
        tabPageExport,
        tabPageEDITab,
        tabPageAbout

    }

    public enum MessageStatus
    {
        Draft = 1,
        NotRecognized,
        Recieved,
        Sent,
        Processed,
        Archived,
        Failed,
        Dispatched,
        Rejected,
        Pending
    }

    public enum MessageColumns
    {
        MessageID,
        MessageDate,
        ArchiveDate,
        FromAddress,
        ToAddress,
        Cc,
        RefMessageID,
        Subject,
        Body,
        Status,
        ReadFlag,
        LastUpdatedDate,
        MailProtocolId,
        TradingPartnerID
    }

    public enum MailProtocolColumns
    {
        MailProtocolID,
        MailProtocol
    }

    public enum TradingPartnerColumns
    {
        TradingPartnerID,
        TPName,
        MailProtocolID,
        EmailID,
        AlternateEmailID,
        ClientID,
        XSDLocation,
        UserName,
        Password,
        eBayAuthToken,
        eBayDevId,
        eBayCertId,
        eBayAppId,
        eBayItemMapping,
        eBayItem,
        eBayDiscountItem,
        OSCWebStoreUrl,
        OSCDatabase,
        AccountNumber,
        CompanyName
    }
    public enum MailSetupColumns
    {
        MailSetupID,
        MailProtocolID,
        InPortNo,
        OutPortNo,
        IsSSL,
        IncommingServer,
        OutgoingServer,
        EmailID,
        UserName,
        Password,
        AutomaticFlag,
        OutboundFlag,
        InboundFlag,
        ScheduleInMin
    }

    public enum Tables
    {
        export_filter,
        mail_attachment,
        mail_protocol_schema,
        mail_setup_schema,
        message_schema,
        message_status_schema,
        trading_partner_schema,
        message_rule,
        message_monitor,
        orders_status_history,
        order_status,
        orders

    }
    public enum Orders
    {
        orders_status,
        orders_id
    }
    public enum OrderStatus
    {
        order_status_id,
        order_status_name
    }
    public enum OrdersStatusHistory
    {
        orders_status_id,
        orders_id,
        date_added,
        comments
    }
    public enum MessageRulesColumn
    {
        RulesId,
        Name,
        TradingPartnerID,
        CondSubEquals,
        CondSubMatches,
        CondFileEquals,
        CondFileMatches,
        ActTransType,
        ActUsingMappingId,
        OnCompleteStatusID,
        OnCompleteErrStatusId,
        Status,
        SoundChime,
        SoundAlarm
    }
    /// <summary>
    /// This enum contains Message Monitor columns.
    /// </summary>
    public enum MessageMonitorColumns
    {
        MonitorId,
        Name,
        Folder,
        Period,
        IsActive
    }

    public enum MailAttachmentColumns
    {
        AttachID,
        AttachName,
        AttachFile,
        MessageID,
        Status
    }
    
    /// <summary>
    /// This enum contains Filter column names.
    /// </summary>
    public enum ExportFilterColumns
    {
        FilterID,
        FilterDataType,
        FilterDate
    }

    public enum MessageStatusColumns
    {
        StatusId,
        Name,
        Descs
    }

    //Define fields for Shipping Request Items.
    public enum ShippingRequestColumns
    {
        itemName,
        Quantity,
        length,
        width,
        height,
        volume,
        weight,
        DangerousGood
    }

    //Define fields for Import settings.
    public enum Import
    {
        Location,
        HeaderRow,
        TextFileOption,
        MappingName

    }

    //Define fields for Export settings.
    public enum Export
    {
        DataType,
        LastExportDate,
        DateRange,
        FromDate,
        ToDate,
        RefNumber,
        FromRefnum,
        ToRefum,
        PaidStatus,
        NewItems,
        NewNames,
        CustomerID,
        FromCustomerID,
        ToCustomerID,
        ItemID,
        FromItemID,
        ToItemID,
        InvoiceNo,
        FromInvoice,
        ToInvoice,
        PurchaseOrder,
        FromPurchase,
        ToPurchase,
        PaidStatusValue,
        //bug 485
        VehicleMileage
    }

    /// <summary>
    /// Allied Express State DDL.
    /// </summary>
    public enum AlliedExpressState
    {
        NSW,
        VIC,
        QLD,
        SA,
        WA,
        TAS,
        NT
    }

    //Axis 7 Changes
    /// <summary>
    /// Define fields for Transfer Inventory
    /// </summary>
    public enum QBTransferInventory
    {
        TxnDate,
        RefNumber,
        FromInventorySiteRef,
        ListID,
        FullName,
        ToInventorySiteRef,
        Memo,
        ExternalGUID,
        TransferInventoryLineRet,
        ItemRef,
        QuantityToTransfer,
        IncludeRetElement

    }

    //Axis 8
    /// <summary>
    /// Define fields for General Detail Report.
    /// </summary>
    public enum QBGeneralDetailReport
    {
        ReportRet,
        ReportTitle,
        ReportSubtitle,
        ReportBasis,
        NumRows,
        NumColumns,
        NumColTitleRows,
        ColDesc,
        ColTitle,
        ColType,
        ReportData,
        DataRow,
        RowData,
        ColData,
        TextRow,
        SubtotalRow,
        TotalRow,
        colID,
        dataType,
        titleRow,
        value,
        rowType,
        rowNumber
    }

    /// <summary>
    /// Define fields for General Summary Report.
    /// </summary>
    public enum QBGeneralSummaryReport
    {
        ReportRet,
        ReportTitle,
        ReportSubtitle,
        ReportBasis,
        NumRows,
        NumColumns,
        NumColTitleRows,
        ColDesc,
        ColTitle,
        ColType,
        ReportData,
        DataRow,
        RowData,
        ColData,
        TextRow,
        SubtotalRow,
        TotalRow,
        colID,
        dataType,
        titleRow,
        value,
        rowType,
        rowNumber
    }

    /// <summary>
    /// Account Names List
    /// </summary>
    public enum QBAccountList
    {
        AccountRet,
        FullName
    }




    /*****************Axis 9.0 Code Added by Akanksha*******************/

    //<summary>
    //Define List of Fields used for Estimate Export.
    //</summary>

    public enum QBXmlEstimateList
    {
        EstimateRet,
        TxnID,
        TimeCreated,
        TimeModified,
        EditSequence,
        TxnNumber,

        CustomerRef,
        ListID,
        FullName,


        ClassRef,
        ClasRefFullName,

        TemplateRef,
        TemplateRefFullName,
        TxnDate,
        RefNumber,

        BillAddress,
        BillAddress1,
        BillAddress2,
        BillAddress3,
        BillAddress4,
        BillAddress5,
        BillAddressCity,
        BillAddressState,
        BillAddressPostalCode,
        BillAddressCountry,
        BillAddressNote,


        BillAddressBlock,
        BillAddressBlock1,
        BillAddressBlock2,
        BillAddressBlock3,
        BillAddressBlock4,
        BillAddressBlock5,

        ShipAddress,
        Addr1,
        Addr2,
        Addr3,
        Addr4,
        Addr5,
        City,
        State,
        PostalCode,
        Country,
        Note,

        ShipAddressBlock,
        ShipAddressBlock1,
        ShipAddressBlock2,
        ShipAddressBlock3,
        ShipAddressBlock4,
        ShipAddressBlock5,

        IsActive,
        PONumber,

        TermsRef,
        TermsRefFullName,
        DueDate,

        SalesRepRef,
        SalesRepRefFullName,
        FOB,
        Subtotal,

        ItemSalesTaxRef,
        SalesTaxPercentage,
        SalesTaxTotal,
        TotalAmount,

        CurrencyRef,
        ExchangeRate,
        TotalAmountInHomeCurrency,
        Memo,

        CustomerMsgRef,
        IsToBeEmailed,
        IsTaxIncluded,

        CustomerSalesTaxCodeRef,
        Other,
        ExternalGUID,

        LinkedTxn,
        TxnType,
        LinkType,
        Amount,

        DataExtRate,
        OwnerID,
        DataExtName,
        DataExtType,
        DataExtValue,

        //For ExpenseLineRet, optional repeat
        EstimateLineRet,
        TxnLineID,
        ItemRef,
        Desc,
        Quantity,
        UnitOfMeasure,
        OverrideUOMSiteRef,
        Rate,
        InventorySiteRef,
        SalesTaxCodeRef,
        MarkupRate,
        MarkupRatePercent,
        Other1,
        Other2,


        //For EstimateGroupLineRet
        EstimateLineGroupRet,
        //EstimateLineGrouptxnLineID,
        ItemGroupRef,
        IsPrintItemInGroup,
        //adding new custom field
        DataExtRet



    }


    public enum QBXmlBankTransfers
    {
        TransferRet,
        TxnID,
        TimeCreated,
        TimeModified,
        EditSequence,
        TxnNumber,
        TxnDate,
        FullName,
        TransferFromAccountRef,
        FromAccountBalance,
        TransferToAccountRef,
        ToAccountBalance,
        ClassRef,
        Amount,
        Memo,
        IncludeRetElement

    }


    public enum QBBuildAssemblyList
    {
        BuildAssemblyRet,
        TxnID,
        TxnNumber,
        ItemInventoryAssemblyRef,
        FullName,
        TxnDate,
        RefNumber,
        IsPending,
        QuantityToBuild,
        QuantityCanBuild,
        QuantityOnHand,
        QuantityOnSalesOrder,
        ComponentItemLineRet,
        TxnLineID,
        ItemRef,
        Name,
        Desc,
        QuantityNeeded,
        ListID,
        Memo,
        InventorySiteRef,
        InventorySiteLocationRef,
        SerialNumber,
        LotNumber

    }

    public enum ClearedStatus
    {
        Cleared,
        NotCleared,
        Pending
    }

    public enum CustomTransactionLocationValuesForIndia : int
    {
        ANDAMAN_AND_NICOBAR_ISLANDS = 35,
        ANDHRA_PRADESH = 37,
        ARUNACHAL_PRADESH = 12,
        ASSAM = 18,
        BIHAR = 10,
        CHANDIGARH = 04,
        CHHATTISGARH = 22,
        DADRA_AND_NAGAR_HAVELI = 26,
        DAMAN_AND_DIU = 25,
        DELHI = 07,
        GOA = 30,
        GUJARAT = 24,
        HARYANA = 06,
        HIMACHAL_PRADESH = 02,
        JAMMU_AND_KASHMIR = 01,
        JHARKHAND = 20,
        KARNATAKA = 29,
        KERALA = 32,
        LAKSHADWEEP = 31,
        MADHYA_PRADESH = 23,
        MAHARASHTRA = 27,
        MANIPUR = 14,
        MEGHALAYA = 17,
        MIZORAM = 15,
        NAGALAND = 13,
        ODISHA = 21,
        PONDICHERRY = 34,
        PUNJAB = 03,
        RAJASTHAN = 08,
        SIKKIM = 11,
        TAMIL_NADU = 33,
        TELANGANA = 36,
        TRIPURA = 16,
        UTTAR_PRADESH = 09,
        UTTARAKHAND = 05,
        WEST_BENGAL = 19,
        OUTSIDE_INDIA = 97
    }

   


    }
