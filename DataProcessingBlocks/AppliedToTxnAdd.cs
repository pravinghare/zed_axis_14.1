using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using QuickBookEntities;
using System.Collections.ObjectModel;

namespace DataProcessingBlocks
{
    [XmlRootAttribute("AppliedToTxnAdd", Namespace = "", IsNullable = false)]
    /// class to set the credit and applied amount
    public class AppliedToTxnAdd
    {
        #region Private Member Variables

        private string m_TxnID;
        private string m_PaymentAmount;
        private Collection<SetCredit> m_SetCredit = new Collection<SetCredit>();
        private string m_DiscountAmount;
        private DiscountAccountRef m_DiscountAccountRef;
        private DiscountClassRef m_DiscountClassRef;

        #endregion

        #region Constructors

        public AppliedToTxnAdd()
        {

        }
        #endregion

        #region Public Properties

        public string TxnID
        {
            get { return this.m_TxnID; }
            set { this.m_TxnID = value; }
        }

        public string PaymentAmount
        {
            get { return this.m_PaymentAmount; }
            set { this.m_PaymentAmount = value; }
        }

        [XmlArray("SetCreditREM")]
        public Collection<SetCredit> SetCredit
        {
            get { return this.m_SetCredit; }
            set { this.m_SetCredit = value; }
        }

        public string DiscountAmount
        {
            get { return this.m_DiscountAmount; }
            set { this.m_DiscountAmount = value; }
        }

        public DiscountAccountRef DiscountAccountRef
        {
            get { return this.m_DiscountAccountRef; }
            set { this.m_DiscountAccountRef = value; }
        }

        public DiscountClassRef DiscountClassRef
        {
            get { return this.m_DiscountClassRef; }
            set { this.m_DiscountClassRef = value; }
        }

        #endregion
    }

    [XmlRootAttribute("SetCredit", Namespace = "", IsNullable = false)]
    public class SetCredit
    {
         #region Private Member Variables

        private string m_CreditTxnID;
        private string m_AppliedAmount;
        private string m_Override;

        #endregion

        #region Constructors

        public SetCredit()
        {

        }
        #endregion

        #region Public Properties

        public string CreditTxnID
        {
            get { return this.m_CreditTxnID; }
            set { this.m_CreditTxnID = value; }
        }

        public string AppliedAmount
        {
            get { return this.m_AppliedAmount; }
            set { this.m_AppliedAmount = value; }
        }

        public string Override
        {
            get { return this.m_Override; }
            set { this.m_Override = value; }
        }

        #endregion
    }


    [XmlRootAttribute("DiscountClassRef", Namespace = "", IsNullable = false)]
    public class DiscountClassRef
    {
        private string m_ListID;
        private string m_FullName;

        public DiscountClassRef()
        {

        }

        public DiscountClassRef(string listID, string fullName)
        {
            if (listID != string.Empty)
                this.m_ListID = listID;
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public DiscountClassRef(string fullName)
        {
            if (fullName != string.Empty)
                this.m_FullName = fullName;
        }
        public string ListID
        {
            get { return this.m_ListID; }
            set { this.m_ListID = value; }
        }

        public string FullName
        {
            get { return this.m_FullName; }
            set { this.m_FullName = value; }
        }
    }

}
