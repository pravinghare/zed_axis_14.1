﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    class ImportBankTransfersClass
    {
        private static ImportBankTransfersClass m_ImportBankTransfersClass;
        public bool isIgnoreAll = false;

        #region Constuctor
        public ImportBankTransfersClass()
        {
        }
        #endregion

        /// <summary>
        /// creating class instance
        /// </summary>
        /// <returns></returns>
        public static ImportBankTransfersClass GetInstance()
        {
            if (m_ImportBankTransfersClass == null)
                m_ImportBankTransfersClass = new ImportBankTransfersClass();
            return m_ImportBankTransfersClass;
        }
        /// <summary>
        ///Creating collection for bank transfer transaction in quickbook. 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="logDirectory"></param>
        /// <returns></returns>

        public DataProcessingBlocks.BankTransfersQBEntryCollection ImportBankTransferData( DataTable dt, ref string logDirectory)
        {
            DataProcessingBlocks.BankTransfersQBEntryCollection coll = new BankTransfersQBEntryCollection();
            isIgnoreAll = false;
            int validateRowCount = 1;         
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
          

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
               
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }
                
                    DateTime BillDt = new DateTime();
                    string datevalue = string.Empty;
                    BankTransfersQBEntry transferData = new BankTransfersQBEntry();

                    #region Adding transfers
                    if (dt.Columns.Contains("TxnID"))
                    {
                        #region Validations of TxnID
                        if (dr["TxnID"].ToString() != string.Empty)
                        {
                            string strTxnID = dr["TxnID"].ToString();
                            if (strTxnID.Length > 100)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This TxnID (" + dr["TxnID"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        transferData.TxnID = dr["TxnID"].ToString().Substring(0, 4000);
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        transferData.TxnID = dr["TxnID"].ToString();
                                    }
                                }
                                else
                                {
                                    transferData.TxnID = dr["TxnID"].ToString();
                                }
                            }
                            else
                            {
                                transferData.TxnID = dr["TxnID"].ToString();
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TxnDate"))
                    {
                        #region validations of TxnDate
                        if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                        {
                            datevalue = dr["TxnDate"].ToString();
                            if (!DateTime.TryParse(datevalue, out BillDt))
                            {
                                DateTime dttest = new DateTime();
                                bool IsValid = false;

                                try
                                {
                                    dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    IsValid = true;
                                }
                                catch
                                {
                                    IsValid = false;
                                }
                                if (IsValid == false)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            transferData.TxnDate = datevalue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            transferData.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        transferData.TxnDate = datevalue;
                                    }
                                }
                                else
                                {
                                    BillDt = dttest;
                                    transferData.TxnDate = dttest.ToString("yyyy-MM-dd");
                                }

                            }
                            else
                            {
                                BillDt = Convert.ToDateTime(datevalue);
                                transferData.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                            }
                        }
                        #endregion
                    }

                    if (dt.Columns.Contains("TransferFromAccountRef"))
                    {
                        #region Validations of transferFrom Full Name
                        if (dr["TransferFromAccountRef"].ToString() != string.Empty)
                        {
                            string strVendor = dr["TransferFromAccountRef"].ToString();
                            if (strVendor.Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        transferData.TransferFromAccountRef = new TransferFromAccountRef(dr["TransferFromAccountRef"].ToString());
                                        if (transferData.TransferFromAccountRef.FullName == null)
                                        {
                                            transferData.TransferFromAccountRef.FullName = null;
                                        }
                                        else
                                        {
                                            transferData.TransferFromAccountRef = new TransferFromAccountRef(dr["TransferFromAccountRef"].ToString().Substring(0, 1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        transferData.TransferFromAccountRef = new TransferFromAccountRef(dr["TransferFromAccountRef"].ToString());
                                        if (transferData.TransferFromAccountRef.FullName == null)
                                        {
                                            transferData.TransferFromAccountRef.FullName = null;
                                        }
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                }
                                else
                                {
                                    transferData.TransferFromAccountRef = new TransferFromAccountRef(dr["TransferFromAccountRef"].ToString());
                                    if (transferData.TransferFromAccountRef.FullName == null)
                                    {
                                        transferData.TransferFromAccountRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                transferData.TransferFromAccountRef = new TransferFromAccountRef(dr["TransferFromAccountRef"].ToString());
                                //string strCustomerFullname = string.Empty;
                                if (transferData.TransferFromAccountRef.FullName == null)
                                {
                                    transferData.TransferFromAccountRef.FullName = null;
                                }
                            }
                        }
                        #endregion
                    }
                    if (dt.Columns.Contains("TransferToAccountRef"))
                    {
                        #region Validations of transferTo Full Name
                        if (dr["TransferToAccountRef"].ToString() != string.Empty)
                        {
                            string strTransfer = dr["TransferToAccountRef"].ToString();
                            if (strTransfer.Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        transferData.TransferToAccountRef = new TransferToAccountRef(dr["TransferToAccountRef"].ToString());
                                        if (transferData.TransferToAccountRef.FullName == null)
                                        {
                                            transferData.TransferToAccountRef.FullName = null;
                                        }
                                        else
                                        {
                                            transferData.TransferToAccountRef = new TransferToAccountRef(dr["TransferToAccountRef"].ToString().Substring(0, 1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        transferData.TransferToAccountRef = new TransferToAccountRef(dr["TransferToAccountRef"].ToString());
                                        if (transferData.TransferToAccountRef.FullName == null)
                                        {
                                            transferData.TransferToAccountRef.FullName = null;
                                        }
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                }
                                else
                                {
                                    transferData.TransferToAccountRef = new TransferToAccountRef(dr["TransferToAccountRef"].ToString());
                                    if (transferData.TransferToAccountRef.FullName == null)
                                    {
                                        transferData.TransferToAccountRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                transferData.TransferToAccountRef = new TransferToAccountRef(dr["TransferToAccountRef"].ToString());                              
                                if (transferData.TransferToAccountRef.FullName == null)
                                {
                                    transferData.TransferToAccountRef.FullName = null;
                                }
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("ClassRef"))
                    {
                        #region Validations of Class Full name
                        if (dr["ClassRef"].ToString() != string.Empty)
                        {
                            if (dr["ClassRef"].ToString().Length > 1000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Class name (" + dr["ClassRef"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        transferData.ClassRef = new ClassRef(dr["ClassRef"].ToString());
                                        if (transferData.ClassRef.FullName == null)
                                        {
                                            transferData.ClassRef.FullName = null;
                                        }
                                        else
                                        {
                                            transferData.ClassRef = new ClassRef(dr["ClassRef"].ToString().Substring(0, 1000));
                                        }
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        transferData.ClassRef = new ClassRef(dr["ClassRef"].ToString());
                                        if (transferData.ClassRef.FullName == null)
                                        {
                                            transferData.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    transferData.ClassRef = new ClassRef(dr["ClassRef"].ToString());
                                    if (transferData.ClassRef.FullName == null)
                                    {
                                        transferData.ClassRef.FullName = null;
                                    }
                                }
                            }
                            else
                            {
                                transferData.ClassRef = new ClassRef(dr["ClassRef"].ToString());                                
                                if (transferData.ClassRef.FullName == null)
                                {
                                    transferData.ClassRef.FullName = null;
                                }
                            }
                        }
                        #endregion

                    }

                    if (dt.Columns.Contains("Amount"))
                    {
                        #region Validations for Amount
                        if (dr["Amount"].ToString() != string.Empty)
                        {
                            decimal amount;
                            if (!decimal.TryParse(dr["Amount"].ToString(), out amount))
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Amount ( " + dr["Amount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string strAmount = dr["Amount"].ToString();
                                        transferData.Amount = strAmount;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string strAmount = dr["Amount"].ToString();
                                        transferData.Amount = strAmount;
                                    }
                                }
                                else
                                {
                                    string strAmount = dr["Amount"].ToString();
                                    transferData.Amount = strAmount;
                                }
                            }
                            else
                            {
                                transferData.Amount = string.Format("{0:000000.00}", Math.Abs(Convert.ToDouble(dr["Amount"].ToString())));
                            }
                        }


                        #endregion
                    }
                    if (dt.Columns.Contains("Memo"))
                    {
                        #region Validations for Memo
                        if (dr["Memo"].ToString() != string.Empty)
                        {
                            if (dr["Memo"].ToString().Length > 4000)
                            {
                                if (isIgnoreAll == false)
                                {
                                    string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                    DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                    if (Convert.ToString(result) == "Cancel")
                                    {
                                        continue;
                                    }
                                    if (Convert.ToString(result) == "No")
                                    {
                                        return null;
                                    }
                                    if (Convert.ToString(result) == "Ignore")
                                    {
                                        string strMemo = dr["Memo"].ToString().Substring(0, 4000);
                                        transferData.Memo = strMemo;
                                    }
                                    if (Convert.ToString(result) == "Abort")
                                    {
                                        isIgnoreAll = true;
                                        string strMemo = dr["Memo"].ToString();
                                        transferData.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    transferData.Memo = dr["Memo"].ToString();
                                }
                            }
                            else
                            {
                                string strMemo = dr["Memo"].ToString();
                                transferData.Memo = strMemo;
                            }
                        }

                        #endregion
                    }

                    #endregion
                    coll.Add(transferData);

                }
            }
            #endregion            

            return coll;
        }
    }

}
