using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;

namespace DataProcessingBlocks
{
    public class ImportItemInventoryAssemblyClass
    {
        private static ImportItemInventoryAssemblyClass m_ImportItemInventoryAssemblyClass;
        public bool isIgnoreAll = false;
        public bool isQuit = false;

        #region Constructor

        public ImportItemInventoryAssemblyClass()
        {

        }

        #endregion
        /// <summary>
        /// Create an instance of Import ItemInventoryAssembly class
        /// </summary>
        /// <returns></returns>
        public static ImportItemInventoryAssemblyClass GetInstance()
        {
            if (m_ImportItemInventoryAssemblyClass == null)
                m_ImportItemInventoryAssemblyClass = new ImportItemInventoryAssemblyClass();
            return m_ImportItemInventoryAssemblyClass;
        }


        /// <summary>
        /// This method is used for validating import data and create ItemInventoryAssembly and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>ItemInventoryAssembly QuickBooks collection </returns>
        public DataProcessingBlocks.ItemInventoryAssemblyQBEntryCollection ImportItemInventoryAssemblyData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {

            //Create an instance of ItemInventoryAssembly Entry collections.
            DataProcessingBlocks.ItemInventoryAssemblyQBEntryCollection coll = new ItemInventoryAssemblyQBEntryCollection();
            isIgnoreAll = false;
            isQuit = false;
            int validateRowCount = 1;
            int listCount = 1;
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;

            #region For ItemInventoryAssembly Entry

            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch { }
                    DateTime ItemInventoryAssemblyDt = new DateTime();
                    string datevalue = string.Empty;

                    if (dt.Columns.Contains("Name"))
                    {
                        //ItemInventoryAssembly Validation
                        DataProcessingBlocks.ItemInventoryAssemblyQBEntry ItemInventoryAssembly = new ItemInventoryAssemblyQBEntry();
                        ItemInventoryAssembly = coll.FindItemInventoryAssemblyEntry(dr["Name"].ToString());

                        if (ItemInventoryAssembly == null)
                        {
                            ItemInventoryAssembly = new ItemInventoryAssemblyQBEntry();

                            if (dt.Columns.Contains("Name"))
                            {
                                #region Validations of Name
                                if (dr["Name"].ToString() != string.Empty)
                                {
                                    if (dr["Name"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Name (" + dr["Name"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.Name = dr["Name"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.Name = dr["Name"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.Name = dr["Name"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.Name = dr["Name"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BarCodeValue"))
                            {
                                #region Validations of BarCodeValue
                                if (dr["BarCodeValue"].ToString() != string.Empty)
                                {
                                    if (dr["BarCodeValue"].ToString().Length > 50)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This BarCodeValue (" + dr["BarCodeValue"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                                if (ItemInventoryAssembly.BarCode.BarCodeValue == null)
                                                    ItemInventoryAssembly = null;
                                                else
                                                    ItemInventoryAssembly.BarCode = new BarCode(dr["BarCodeValue"].ToString().Substring(0, 50));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                                if (ItemInventoryAssembly.BarCode.BarCodeValue == null)
                                                    ItemInventoryAssembly = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.BarCode = new BarCode(dr["BarCodeValue"].ToString());
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsActive"))
                            {
                                #region Validations of IsActive
                                if (dr["IsActive"].ToString() != "<None>" || dr["IsActive"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsActive"].ToString(), out result))
                                    {
                                        ItemInventoryAssembly.IsActive = Convert.ToInt32(dr["IsActive"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsActive"].ToString().ToLower() == "true")
                                        {
                                            ItemInventoryAssembly.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsActive"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemInventoryAssembly.IsActive = dr["IsActive"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsActive (" + dr["IsActive"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemInventoryAssembly.IsActive = dr["IsActive"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemInventoryAssembly.IsActive = dr["IsActive"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemInventoryAssembly.IsActive = dr["IsActive"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ParentFullName"))
                            {
                                #region Validations of ParentRef FullName
                                if (dr["ParentFullName"].ToString() != string.Empty)
                                {
                                    //Axis 68
                                    if (dr["ParentFullName"].ToString().Length > 255)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Parent FullName (" + dr["ParentFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                                if (ItemInventoryAssembly.ParentRef.FullName == null)
                                                    ItemInventoryAssembly.ParentRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.ParentRef = new ParentRef(dr["ParentFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                                if (ItemInventoryAssembly.ParentRef.FullName == null)
                                                    ItemInventoryAssembly.ParentRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                            if (ItemInventoryAssembly.ParentRef.FullName == null)
                                                ItemInventoryAssembly.ParentRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.ParentRef = new ParentRef(dr["ParentFullName"].ToString());
                                        if (ItemInventoryAssembly.ParentRef.FullName == null)
                                            ItemInventoryAssembly.ParentRef.FullName = null;
                                    }
                                }
                                #endregion
                            }




                            #region axis 12.0  bug 476
                            //bug 476 axis 12.0
                            if (dt.Columns.Contains("ClassRefFullName"))
                            {
                                #region Validations of Class FullName
                                if (dr["ClassRefFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ClassRefFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Class FullName (" + dr["ClassRefFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                                if (ItemInventoryAssembly.ClassRef.FullName == null)
                                                    ItemInventoryAssembly.ClassRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                                if (ItemInventoryAssembly.ClassRef.FullName == null)
                                                    ItemInventoryAssembly.ClassRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                            if (ItemInventoryAssembly.ClassRef.FullName == null)
                                                ItemInventoryAssembly.ClassRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.ClassRef = new ClassRef(dr["ClassRefFullName"].ToString());
                                        if (ItemInventoryAssembly.ClassRef.FullName == null)
                                            ItemInventoryAssembly.ClassRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Max"))
                            {
                                #region Validations for QuantityOnHead
                                if (dr["Max"].ToString() != string.Empty)
                                {
                                    string strQuantityOnHand = dr["Max"].ToString();
                                    ItemInventoryAssembly.Max = strQuantityOnHand;

                                }

                                #endregion
                            }


                            if (dt.Columns.Contains("ManufacturerPartNumber"))
                            {
                                #region Validations of ManufacturerPartNumber
                                if (dr["ManufacturerPartNumber"].ToString() != string.Empty)
                                {
                                    if (dr["ManufacturerPartNumber"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ManufacturerPartNumber (" + dr["ManufacturerPartNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString().Substring(0, 31);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.ManufacturerPartNumber = dr["ManufacturerPartNumber"].ToString();
                                    }
                                }
                                #endregion
                            }



                            #endregion
                            if (dt.Columns.Contains("UnitOfMeasureSetFullName"))
                            {
                                #region Validations of UnitOfMeasureSet FullName
                                if (dr["UnitOfMeasureSetFullName"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasureSetFullName"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasureSet FullName (" + dr["UnitOfMeasureSetFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                                if (ItemInventoryAssembly.UnitOfMeasureSetRef.FullName == null)
                                                    ItemInventoryAssembly.UnitOfMeasureSetRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString().Substring(0, 31));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                                if (ItemInventoryAssembly.UnitOfMeasureSetRef.FullName == null)
                                                    ItemInventoryAssembly.UnitOfMeasureSetRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                            if (ItemInventoryAssembly.UnitOfMeasureSetRef.FullName == null)
                                                ItemInventoryAssembly.UnitOfMeasureSetRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.UnitOfMeasureSetRef = new UnitOfMeasureSetRef(dr["UnitOfMeasureSetFullName"].ToString());
                                        if (ItemInventoryAssembly.UnitOfMeasureSetRef.FullName == null)
                                            ItemInventoryAssembly.UnitOfMeasureSetRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("IsTaxInclude"))
                            {
                                #region Validations of IsTaxInclude
                                if (dr["IsTaxInclude"].ToString() != "<None>" || dr["IsTaxInclude"].ToString() != string.Empty)
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxInclude"].ToString(), out result))
                                    {
                                        ItemInventoryAssembly.IsTaxIncluded = Convert.ToInt32(dr["IsTaxInclude"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxInclude"].ToString().ToLower() == "true")
                                        {
                                            ItemInventoryAssembly.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxInclude"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                ItemInventoryAssembly.IsTaxIncluded = dr["IsTaxInclude"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxInclude (" + dr["IsTaxInclude"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    ItemInventoryAssembly.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemInventoryAssembly.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                                }
                                            }
                                            else
                                            {
                                                ItemInventoryAssembly.IsTaxIncluded = dr["IsTaxInclude"].ToString();
                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode FullName
                                if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesTaxCode FullName (" + dr["SalesTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (ItemInventoryAssembly.SalesTaxCodeRef.FullName == null)
                                                    ItemInventoryAssembly.SalesTaxCodeRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (ItemInventoryAssembly.SalesTaxCodeRef.FullName == null)
                                                    ItemInventoryAssembly.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (ItemInventoryAssembly.SalesTaxCodeRef.FullName == null)
                                                ItemInventoryAssembly.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                        if (ItemInventoryAssembly.SalesTaxCodeRef.FullName == null)
                                            ItemInventoryAssembly.SalesTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("SalesDesc"))
                            {
                                #region Validations of SalesDesc
                                if (dr["SalesDesc"].ToString() != string.Empty)
                                {
                                    if (dr["SalesDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesDesc (" + dr["SalesDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.SalesDesc = dr["SalesDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.SalesDesc = dr["SalesDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.SalesDesc = dr["SalesDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.SalesDesc = dr["SalesDesc"].ToString();
                                    }
                                }
                                #endregion
                            }


                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion


                            if (dt.Columns.Contains("SalesPrice"))
                            {
                                #region Validations for SalesPrice
                                if (dr["SalesPrice"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["SalesPrice"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SalesPrice ( " + dr["SalesPrice"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                //Bug 484 Sales Price decimal problem
                                                var strRate = Convert.ToDecimal(dr["SalesPrice"].ToString());
                                                ItemInventoryAssembly.SalesPrice = string.Format("{0:.00000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                                //string strRate = dr["SalesPrice"].ToString();
                                                //ItemInventoryAssembly.SalesPrice = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                //Bug 484 Sales Price decimal problem
                                                var strRate = Convert.ToDecimal(dr["SalesPrice"].ToString());
                                                ItemInventoryAssembly.SalesPrice = string.Format("{0:.00000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                                //string strRate = dr["SalesPrice"].ToString();
                                                //ItemInventoryAssembly.SalesPrice = strRate;
                                            }
                                        }
                                        else
                                        {
                                            //Bug 484 Sales Price decimal problem
                                            var strRate = Convert.ToDecimal(dr["SalesPrice"].ToString());
                                            ItemInventoryAssembly.SalesPrice = string.Format("{0:.00000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                            //string strRate = dr["SalesPrice"].ToString();
                                            //ItemInventoryAssembly.SalesPrice = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemInventoryAssembly.IsTaxIncluded != null && ItemInventoryAssembly.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemInventoryAssembly.IsTaxIncluded == "true" || ItemInventoryAssembly.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["SalesPrice"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemInventoryAssembly.SalesPrice = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemInventoryAssembly.SalesPrice == null)
                                            {
                                                //Bug 484 Sales Price decimal problem
                                                ItemInventoryAssembly.SalesPrice = string.Format("{0:.00000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                                //ItemInventoryAssembly.SalesPrice = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            //Bug 484 Sales Price decimal problem
                                            ItemInventoryAssembly.SalesPrice = string.Format("{0:.00000}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                            //ItemInventoryAssembly.SalesPrice = string.Format("{0:000000.00}", Convert.ToDouble(dr["SalesPrice"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("IncomeAccountFullName"))
                            {
                                #region Validations of IncomeAccount FullName
                                if (dr["IncomeAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["IncomeAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IncomeAccount FullName (" + dr["IncomeAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.IncomeAccountRef = new IncomeAccountRef(dr["IncomeAccountFullName"].ToString());
                                                if (ItemInventoryAssembly.IncomeAccountRef.FullName == null)
                                                    ItemInventoryAssembly.IncomeAccountRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.IncomeAccountRef = new IncomeAccountRef(dr["IncomeAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.IncomeAccountRef = new IncomeAccountRef(dr["IncomeAccountFullName"].ToString());
                                                if (ItemInventoryAssembly.IncomeAccountRef.FullName == null)
                                                    ItemInventoryAssembly.IncomeAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.IncomeAccountRef = new IncomeAccountRef(dr["IncomeAccountFullName"].ToString());
                                            if (ItemInventoryAssembly.IncomeAccountRef.FullName == null)
                                                ItemInventoryAssembly.IncomeAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.IncomeAccountRef = new IncomeAccountRef(dr["IncomeAccountFullName"].ToString());
                                        if (ItemInventoryAssembly.IncomeAccountRef.FullName == null)
                                            ItemInventoryAssembly.IncomeAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PurchaseDesc"))
                            {
                                #region Validations of SalesDesc
                                if (dr["PurchaseDesc"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseDesc"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseDesc (" + dr["PurchaseDesc"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.PurchaseDesc = dr["PurchaseDesc"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.PurchaseDesc = dr["PurchaseDesc"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PurchaseCost"))
                            {
                                #region Validations for PurchaseCost
                                if (dr["PurchaseCost"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["PurchaseCost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseCost ( " + dr["PurchaseCost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["PurchaseCost"].ToString();
                                                ItemInventoryAssembly.PurchaseCost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["PurchaseCost"].ToString();
                                                ItemInventoryAssembly.PurchaseCost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["PurchaseCost"].ToString();
                                            ItemInventoryAssembly.PurchaseCost = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemInventoryAssembly.IsTaxIncluded != null && ItemInventoryAssembly.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemInventoryAssembly.IsTaxIncluded == "true" || ItemInventoryAssembly.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["PurchaseCost"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemInventoryAssembly.PurchaseCost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemInventoryAssembly.PurchaseCost == null)
                                            { //bug 484
                                                ItemInventoryAssembly.PurchaseCost = string.Format("{0:.00000}", Convert.ToDouble(dr["PurchaseCost"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            //bug 484
                                            ItemInventoryAssembly.PurchaseCost = string.Format("{0:.00000}", Convert.ToDouble(dr["PurchaseCost"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("PurchaseTaxCodeFullName"))
                            {
                                #region Validations of PurchaseTaxCode FullName
                                if (dr["PurchaseTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    if (dr["PurchaseTaxCodeFullName"].ToString().Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PurchaseTaxCode FullName (" + dr["PurchaseTaxCodeFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["PurchaseTaxCodeFullName"].ToString());
                                                if (ItemInventoryAssembly.PurchaseTaxCodeRef.FullName == null)
                                                    ItemInventoryAssembly.PurchaseTaxCodeRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["PurchaseTaxCodeFullName"].ToString().Substring(0, 3));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["PurchaseTaxCodeFullName"].ToString());
                                                if (ItemInventoryAssembly.PurchaseTaxCodeRef.FullName == null)
                                                    ItemInventoryAssembly.PurchaseTaxCodeRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["PurchaseTaxCodeFullName"].ToString());
                                            if (ItemInventoryAssembly.PurchaseTaxCodeRef.FullName == null)
                                                ItemInventoryAssembly.PurchaseTaxCodeRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.PurchaseTaxCodeRef = new PurchaseTaxCodeRef(dr["PurchaseTaxCodeFullName"].ToString());
                                        if (ItemInventoryAssembly.PurchaseTaxCodeRef.FullName == null)
                                            ItemInventoryAssembly.PurchaseTaxCodeRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("COGSAccountFullName"))
                            {
                                #region Validations of COGSAccount FullName
                                if (dr["COGSAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["COGSAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This COGSAccount FullName (" + dr["COGSAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.COGSAccountRef = new COGSAccountRef(dr["COGSAccountFullName"].ToString());
                                                if (ItemInventoryAssembly.COGSAccountRef.FullName == null)
                                                    ItemInventoryAssembly.COGSAccountRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.COGSAccountRef = new COGSAccountRef(dr["COGSAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.COGSAccountRef = new COGSAccountRef(dr["COGSAccountFullName"].ToString());
                                                if (ItemInventoryAssembly.COGSAccountRef.FullName == null)
                                                    ItemInventoryAssembly.COGSAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.COGSAccountRef = new COGSAccountRef(dr["COGSAccountFullName"].ToString());
                                            if (ItemInventoryAssembly.COGSAccountRef.FullName == null)
                                                ItemInventoryAssembly.COGSAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.COGSAccountRef = new COGSAccountRef(dr["COGSAccountFullName"].ToString());
                                        if (ItemInventoryAssembly.COGSAccountRef.FullName == null)
                                            ItemInventoryAssembly.COGSAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("PrefVendorFullName"))
                            {
                                #region Validations of PrefVendor FullName
                                if (dr["PrefVendorFullName"].ToString() != string.Empty)
                                {
                                    if (dr["PrefVendorFullName"].ToString().Length > 41)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PrefVendor FullName (" + dr["PrefVendorFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.PrefVendorRef = new PrefVendorRef(dr["PrefVendorFullName"].ToString());
                                                if (ItemInventoryAssembly.PrefVendorRef.FullName == null)
                                                    ItemInventoryAssembly.PrefVendorRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.PrefVendorRef = new PrefVendorRef(dr["PrefVendorFullName"].ToString().Substring(0, 41));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.PrefVendorRef = new PrefVendorRef(dr["PrefVendorFullName"].ToString());
                                                if (ItemInventoryAssembly.PrefVendorRef.FullName == null)
                                                    ItemInventoryAssembly.PrefVendorRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.PrefVendorRef = new PrefVendorRef(dr["PrefVendorFullName"].ToString());
                                            if (ItemInventoryAssembly.PrefVendorRef.FullName == null)
                                                ItemInventoryAssembly.PrefVendorRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.PrefVendorRef = new PrefVendorRef(dr["PrefVendorFullName"].ToString());
                                        if (ItemInventoryAssembly.PrefVendorRef.FullName == null)
                                            ItemInventoryAssembly.PrefVendorRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("AssetAccountFullName"))
                            {
                                #region Validations of AssetAccount FullName
                                if (dr["AssetAccountFullName"].ToString() != string.Empty)
                                {
                                    if (dr["AssetAccountFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This AssetAccountRef FullName (" + dr["AssetAccountFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemInventoryAssembly.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                                if (ItemInventoryAssembly.AssetAccountRef.FullName == null)
                                                    ItemInventoryAssembly.AssetAccountRef.FullName = null;
                                                else
                                                    ItemInventoryAssembly.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemInventoryAssembly.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                                if (ItemInventoryAssembly.AssetAccountRef.FullName == null)
                                                    ItemInventoryAssembly.AssetAccountRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                            if (ItemInventoryAssembly.AssetAccountRef.FullName == null)
                                                ItemInventoryAssembly.AssetAccountRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        ItemInventoryAssembly.AssetAccountRef = new AssetAccountRef(dr["AssetAccountFullName"].ToString());
                                        if (ItemInventoryAssembly.AssetAccountRef.FullName == null)
                                            ItemInventoryAssembly.AssetAccountRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("BuildPoint"))
                            {
                                #region Validations for ReorderPoint
                                if (dr["BuildPoint"].ToString() != string.Empty)
                                {
                                    string strBuildPoint = dr["BuildPoint"].ToString();
                                    ItemInventoryAssembly.BuildPoint = strBuildPoint;

                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("QuantityOnHand"))
                            {
                                #region Validations for QuantityOnHead
                                if (dr["QuantityOnHand"].ToString() != string.Empty)
                                {
                                    string strQuantityOnHand = dr["QuantityOnHand"].ToString();
                                    ItemInventoryAssembly.QuantityOnHand = strQuantityOnHand;

                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("TotalValue"))
                            {
                                #region Validations for TotalValue
                                if (dr["TotalValue"].ToString() != string.Empty)
                                {
                                    //decimal amount;
                                    decimal cost = 0;
                                    if (!decimal.TryParse(dr["TotalValue"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TotalValue ( " + dr["TotalValue"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["TotalValue"].ToString();
                                                ItemInventoryAssembly.TotalValue = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["TotalValue"].ToString();
                                                ItemInventoryAssembly.TotalValue = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["TotalValue"].ToString();
                                            ItemInventoryAssembly.TotalValue = strRate;
                                        }
                                    }
                                    else
                                    {

                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && ItemInventoryAssembly.IsTaxIncluded != null && ItemInventoryAssembly.IsTaxIncluded != string.Empty)
                                            {
                                                if (ItemInventoryAssembly.IsTaxIncluded == "true" || ItemInventoryAssembly.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["TotalValue"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemInventoryAssembly.TotalValue = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemInventoryAssembly.TotalValue == null)
                                            {
                                                ItemInventoryAssembly.TotalValue = string.Format("{0:000000.00}", Convert.ToDouble(dr["TotalValue"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssembly.TotalValue = string.Format("{0:000000.00}", Convert.ToDouble(dr["TotalValue"].ToString()));
                                        }

                                    }
                                }

                                #endregion
                            }

                            if (dt.Columns.Contains("InventoryDate"))
                            {
                                #region validations of InventoryDate
                                if (dr["InventoryDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["InventoryDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out ItemInventoryAssemblyDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This InventoryDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemInventoryAssembly.InventoryDate = datevalue;
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemInventoryAssembly.InventoryDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                ItemInventoryAssembly.InventoryDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            ItemInventoryAssemblyDt = dttest;
                                            ItemInventoryAssembly.InventoryDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        ItemInventoryAssemblyDt = Convert.ToDateTime(datevalue);
                                        ItemInventoryAssembly.InventoryDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion
                            }

                            #region Item Inventory Assembly Line Item

                            ItemInventoryAssemblyLine itemInventoryAssemblyLineItem = new ItemInventoryAssemblyLine();

                            if (dt.Columns.Contains("ItemInventoryAssemblyLineItemInventoryFullName"))
                            {
                                #region Validations of ItemInventoryAssemblyLine ItemInventory FullName
                                if (dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemInventoryAssemblyLine ItemInventory FullName (" + dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                                if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                                    itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                                else
                                                    itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                                if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                                    itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                            if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                                itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                        if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                            itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryAssemblyLineQuantity"))
                            {
                                #region Validations for AssemblyLineQuantity
                                if (dr["ItemInventoryAssemblyLineQuantity"].ToString() != string.Empty)
                                {
                                    string strQuantityOnHand = dr["ItemInventoryAssemblyLineQuantity"].ToString();
                                    itemInventoryAssemblyLineItem.Quantity = strQuantityOnHand;

                                }

                                #endregion
                            }


                            if (itemInventoryAssemblyLineItem.ItemInventoryRef != null)
                                ItemInventoryAssembly.ItemInventoryAssemblyLine.Add(itemInventoryAssemblyLineItem);
                            #endregion



                            coll.Add(ItemInventoryAssembly);
                        }
                        else
                        {
                            #region Item Inventory Assembly Line Item

                            ItemInventoryAssemblyLine itemInventoryAssemblyLineItem = new ItemInventoryAssemblyLine();

                            if (dt.Columns.Contains("ItemInventoryAssemblyLineItemInventoryFullName"))
                            {
                                #region Validations of ItemInventoryAssemblyLine ItemInventory FullName
                                if (dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString().Length > 159)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemInventoryAssemblyLine ItemInventory FullName (" + dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                                if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                                    itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                                else
                                                    itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString().Substring(0, 159));
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                                if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                                    itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                            }
                                        }
                                        else
                                        {
                                            itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                            if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                                itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                        }
                                    }
                                    else
                                    {
                                        itemInventoryAssemblyLineItem.ItemInventoryRef = new ItemInventoryRef(dr["ItemInventoryAssemblyLineItemInventoryFullName"].ToString());
                                        if (itemInventoryAssemblyLineItem.ItemInventoryRef.FullName == null)
                                            itemInventoryAssemblyLineItem.ItemInventoryRef.FullName = null;
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("ItemInventoryAssemblyLineQuantity"))
                            {
                                #region Validations for AssemblyLineQuantity
                                if (dr["ItemInventoryAssemblyLineQuantity"].ToString() != string.Empty)
                                {
                                    string strQuantityOnHand = dr["ItemInventoryAssemblyLineQuantity"].ToString();
                                    itemInventoryAssemblyLineItem.Quantity = strQuantityOnHand;

                                }

                                #endregion
                            }


                            if (itemInventoryAssemblyLineItem.ItemInventoryRef != null)
                                ItemInventoryAssembly.ItemInventoryAssemblyLine.Add(itemInventoryAssemblyLineItem);

                            #endregion
                        }

                    }
                    else
                    {
                    }

                }
                else
                {
                    return null;
                }
            }
            #endregion
            
            #endregion

            #region Create Parent for item
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    try
                    {
                        bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                    }
                    catch (Exception ex)
                    {
                    }
                    listCount++;
                    if (dt.Columns.Contains("ParentFullName"))
                    {
                        if (dr["ParentFullName"].ToString() != string.Empty)
                        {
                            string ItemName = dr["ParentFullName"].ToString();
                            string[] arr = new string[15];
                            if (ItemName.Contains(":"))
                            {
                                arr = ItemName.Split(':');
                            }
                            else
                            {
                                arr[0] = dr["ParentFullName"].ToString();
                            }
                            #region Setting SalesTaxCode and IsTaxIncluded
                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;
                            }
                            string TaxRateValue = string.Empty;
                            string IsTaxIncluded = string.Empty;
                            string netRate = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            string IncomeAccountFullName = string.Empty;
                            string AssetAccountFullName = string.Empty;
                            string COGSAccountFullName = string.Empty;
                            if (dt.Columns.Contains("IncomeAccountFullName"))
                            {
                                if (dr["IncomeAccountFullName"].ToString() != string.Empty)
                                {
                                    IncomeAccountFullName = dr["IncomeAccountFullName"].ToString();
                                }
                            }
                            if (dt.Columns.Contains("AssetAccountFullName"))
                            {
                                if (dr["AssetAccountFullName"].ToString() != string.Empty)
                                {
                                    AssetAccountFullName = dr["AssetAccountFullName"].ToString();
                                }
                            }
                            if (dt.Columns.Contains("COGSAccountFullName"))
                            {
                                if (dr["COGSAccountFullName"].ToString() != string.Empty)
                                {
                                    COGSAccountFullName = dr["COGSAccountFullName"].ToString();
                                }
                            }
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["SalesTaxCodeFullName"].ToString();
                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemSaleTaxFullName(QBFileName, FullName);
                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                            #endregion
                            #region Set Item Query
                            for (int i = 0; i < arr.Length; i++)
                            {
                                int item = 0;
                                int a = 0;
                                if (arr[i] != null && arr[i] != string.Empty)
                                {
                                    #region Passing Items Query
                                    XmlDocument pxmldoc = new XmlDocument();
                                    pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                    pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                    XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                    pxmldoc.AppendChild(qbXML);
                                    XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                    qbXML.AppendChild(qbXMLMsgsRq);
                                    qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                    XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                    qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                    ItemQueryRq.SetAttribute("requestID", "1");
                                    if (i > 0)
                                    {
                                        if (arr[i] != null && arr[i] != string.Empty)
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            for (item = 0; item <= i; item++)
                                            {
                                                if (arr[item].Trim() != string.Empty)
                                                {
                                                    FullName.InnerText += arr[item].Trim() + ":";
                                                }
                                            }
                                            if (FullName.InnerText != string.Empty)
                                            {
                                                ItemQueryRq.AppendChild(FullName);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        XmlElement FullName = pxmldoc.CreateElement("FullName");
                                        FullName.InnerText = arr[i];
                                        ItemQueryRq.AppendChild(FullName);
                                    }
                                    string pinput = pxmldoc.OuterXml;
                                    string resp = string.Empty;
                                    try
                                    {
                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                        }

                                        else
                                            resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                    }


                                    catch (Exception ex)
                                    {
                                        CommonUtilities.WriteErrorLog(ex.Message);
                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                    }
                                    finally
                                    {
                                        if (resp != string.Empty)
                                        {
                                            System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                            outputXMLDoc.LoadXml(resp);
                                            string statusSeverity = string.Empty;
                                            foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                            {
                                                statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {
                                                #region Item Inventory Add Query
                                                XmlDocument ItemInventoryAssemblyAdddoc = new XmlDocument();
                                                ItemInventoryAssemblyAdddoc.AppendChild(ItemInventoryAssemblyAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                ItemInventoryAssemblyAdddoc.AppendChild(ItemInventoryAssemblyAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                XmlElement qbXMLIS = ItemInventoryAssemblyAdddoc.CreateElement("QBXML");
                                                ItemInventoryAssemblyAdddoc.AppendChild(qbXMLIS);
                                                XmlElement qbXMLMsgsRqIS = ItemInventoryAssemblyAdddoc.CreateElement("QBXMLMsgsRq");
                                                qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                XmlElement ItemInventoryAssemblyAddRq = ItemInventoryAssemblyAdddoc.CreateElement("ItemInventoryAssemblyAddRq");
                                                qbXMLMsgsRqIS.AppendChild(ItemInventoryAssemblyAddRq);
                                                ItemInventoryAssemblyAddRq.SetAttribute("requestID", "1");
                                                XmlElement ItemInventoryAssemblyAdd = ItemInventoryAssemblyAdddoc.CreateElement("ItemInventoryAssemblyAdd");
                                                ItemInventoryAssemblyAddRq.AppendChild(ItemInventoryAssemblyAdd);
                                                XmlElement NameIS = ItemInventoryAssemblyAdddoc.CreateElement("Name");
                                                NameIS.InnerText = arr[i];
                                                ItemInventoryAssemblyAdd.AppendChild(NameIS);
                                                if (i > 0)
                                                {
                                                    if (arr[i] != null && arr[i] != string.Empty)
                                                    {
                                                        XmlElement INIChildFullName = ItemInventoryAssemblyAdddoc.CreateElement("FullName");
                                                        for (a = 0; a <= i - 1; a++)
                                                        {
                                                            if (arr[a].Trim() != string.Empty)
                                                            {
                                                                INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                            }
                                                        }
                                                        if (INIChildFullName.InnerText != string.Empty)
                                                        {
                                                            XmlElement INIParent = ItemInventoryAssemblyAdddoc.CreateElement("ParentRef");
                                                            ItemInventoryAssemblyAdd.AppendChild(INIParent);
                                                            INIParent.AppendChild(INIChildFullName);
                                                        }
                                                    }
                                                }
                                                if (defaultSettings.TaxCode != string.Empty)
                                                {
                                                    if (defaultSettings.TaxCode.Length < 4)
                                                    {
                                                        XmlElement INISalesTaxCodeRef = ItemInventoryAssemblyAdddoc.CreateElement("SalesTaxCodeRef");
                                                        ItemInventoryAssemblyAdd.AppendChild(INISalesTaxCodeRef);
                                                        XmlElement INISTCodeRefFullName = ItemInventoryAssemblyAdddoc.CreateElement("FullName");
                                                        INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                        INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                    }
                                                }
                                                if (defaultSettings.IncomeAccount != string.Empty || IncomeAccountFullName != string.Empty)
                                                {
                                                    XmlElement ISIncomeAccountRef = ItemInventoryAssemblyAdddoc.CreateElement("IncomeAccountRef");
                                                    ItemInventoryAssemblyAdd.AppendChild(ISIncomeAccountRef);
                                                    XmlElement ISFullName = ItemInventoryAssemblyAdddoc.CreateElement("FullName");
                                                    if (IncomeAccountFullName != string.Empty)
                                                        ISFullName.InnerText = IncomeAccountFullName;
                                                    else
                                                        ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                    ISIncomeAccountRef.AppendChild(ISFullName);
                                                }
                                                if (defaultSettings.COGSAccount != string.Empty || COGSAccountFullName != string.Empty)
                                                {
                                                    XmlElement ISCOGSAccountRef = ItemInventoryAssemblyAdddoc.CreateElement("COGSAccountRef");
                                                    ItemInventoryAssemblyAdd.AppendChild(ISCOGSAccountRef);
                                                    XmlElement ISFullName = ItemInventoryAssemblyAdddoc.CreateElement("FullName");
                                                    if (COGSAccountFullName != string.Empty)
                                                        ISFullName.InnerText = COGSAccountFullName;
                                                    else
                                                        ISFullName.InnerText = defaultSettings.COGSAccount;
                                                    ISCOGSAccountRef.AppendChild(ISFullName);
                                                }
                                                if (defaultSettings.AssetAccount != string.Empty || AssetAccountFullName != string.Empty)
                                                {
                                                    XmlElement ISAssetAccountRef = ItemInventoryAssemblyAdddoc.CreateElement("AssetAccountRef");
                                                    ItemInventoryAssemblyAdd.AppendChild(ISAssetAccountRef);
                                                    XmlElement ISFullName = ItemInventoryAssemblyAdddoc.CreateElement("FullName");
                                                    if (AssetAccountFullName != string.Empty)
                                                        ISFullName.InnerText = AssetAccountFullName;
                                                    else
                                                        ISFullName.InnerText = defaultSettings.AssetAccount;
                                                    ISAssetAccountRef.AppendChild(ISFullName);
                                                }
                                                string ItemInventoryAssemblyAddinput = ItemInventoryAssemblyAdddoc.OuterXml;
                                                string respItemInventoryAssemblyAddinputdoc = string.Empty;
                                                try
                                                {

                                                    //Axis 10.2(bug no 66)
                                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                    {
                                                        respItemInventoryAssemblyAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAssemblyAddinput);
                                                    }
                                                    else
                                                    {
                                                        respItemInventoryAssemblyAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemInventoryAssemblyAddinput);
                                                    }
                                                    //End Changes
                                                    
                                                    System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                    outputXML.LoadXml(respItemInventoryAssemblyAddinputdoc);
                                                    string StatusSeverity = string.Empty;
                                                    string statusMessage = string.Empty;
                                                    foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAssemblyAddRs"))
                                                    {
                                                        StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                        statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    }
                                                    outputXML.RemoveAll();
                                                    if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                    {
                                                        ErrorSummary summary = new ErrorSummary(statusMessage);
                                                        summary.ShowDialog();
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    CommonUtilities.WriteErrorLog(ex.Message);
                                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                }
                                                string strTest3 = respItemInventoryAssemblyAddinputdoc;
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
            return coll;
        }

    }
}
