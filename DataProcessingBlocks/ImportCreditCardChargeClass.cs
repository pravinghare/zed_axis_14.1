using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using QuickBookEntities;
using TransactionImporter;
using System.Xml;
using Streams;
using System.ComponentModel;
using EDI.Constant;


namespace DataProcessingBlocks

{
    public class ImportCreditCardChargeClass
    {
        private static ImportCreditCardChargeClass m_ImportCreditCardChargeClass;
        public bool isIgnoreAll = false;

        #region Constuctor
        public ImportCreditCardChargeClass()
        {
        }
        #endregion

        /// <summary>
        /// Create an instance of Import Credit card charge class
        /// </summary>
        /// <returns></returns>
        public static ImportCreditCardChargeClass GetInstance()
        {
            if (m_ImportCreditCardChargeClass == null)
                m_ImportCreditCardChargeClass = new ImportCreditCardChargeClass();
            return m_ImportCreditCardChargeClass;
        }

        /// <summary>
        /// This method is used for validating import data and create customer and items request
        /// import data into QuickBooks.
        /// </summary>
        /// <param name="dt">Passing Import file data</param>
        /// <param name="logDirectory">Created log directory for generate qbxml file.</param>
        /// <returns>Credit Card Charge QuickBooks collection </returns>
        public DataProcessingBlocks.CreditModule.CreditCardChargeEntryCollection ImportCreditCardChargeData(string QBFileName, DataTable dt, ref string logDirectory, DefaultAccountSettings defaultSettings)
        {
            DataProcessingBlocks.CreditModule.CreditCardChargeEntryCollection coll = new DataProcessingBlocks.CreditModule.CreditCardChargeEntryCollection();
            TransactionImporter.TransactionImporter axisForm = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
            int validateRowCount = 1;
            int listCount = 1;
            #region Checking Validations
            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    //Bug 1434
                    System.Threading.Thread.Sleep(100);
                    //bug 501
                    if (CommonUtilities.GetInstance().IsStatementFeature() == false)
                    {
                        try
                        {
                            bkWorker.ReportProgress(validateRowCount * 100 / dt.Rows.Count);
                        }
                        catch (Exception ex)
                        {
                            
                        }
                    }
                    else
                    {
                        CommonUtilities.GetInstance().ValidateFlag = true;
                        CommonUtilities.GetInstance().ListFlag = false;
                    }
                     
                    CommonUtilities.GetInstance().ValidateRowCount = +(validateRowCount) + " of " + dt.Rows.Count + " records";
                    validateRowCount = validateRowCount + 1;
                    try
                    {
                        int counterrows = 0;
                        bool resultrows = false;
                        for (int l = 0; l < dt.Columns.Count; l++)
                        {
                            resultrows = dr.IsNull(dt.Columns[l]);
                            if (resultrows)
                            {
                                counterrows = counterrows + 1;
                                if (counterrows != dt.Columns.Count)
                                {
                                    resultrows = false;
                                }
                            }

                        }
                        if (resultrows == true && counterrows == dt.Columns.Count)
                        {
                            continue;
                        }
                    }
                    catch
                    { }

                    DateTime CreditCardChargeDt = new DateTime();
                    string datevalue = string.Empty;
                    if (dt.Columns.Contains("RefNumber"))
                    {
                        #region Adding ref number
                        DataProcessingBlocks.CreditModule.CreditCardChargeEntry CreditCardCharge = new DataProcessingBlocks.CreditModule.CreditCardChargeEntry();
                        #region Check whether Statement Import functionality
                        if (CommonUtilities.GetInstance().IsStatementFeature())
                        {
                                CreditCardCharge.IsTaxIncluded = "true";
                        }
                        #endregion
                        CreditCardCharge = coll.FindCreditCardChargeEntry(dr["RefNumber"].ToString());
                        if (CreditCardCharge == null)
                        {
                            CreditCardCharge = new DataProcessingBlocks.CreditModule.CreditCardChargeEntry();
                            #region Check whether statement import functionality
                            if (CommonUtilities.GetInstance().IsStatementFeature())
                            {
                                CreditCardCharge.IsTaxIncluded = "true";
                            }
                            #endregion
                            if (dt.Columns.Contains("AccountFullName"))
                            {
                                #region Validations of Account Full Name
                                if (dr["AccountFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["AccountFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Account fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                                if (CreditCardCharge.AccountRef.FullName == null)
                                                {
                                                    CreditCardCharge.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString().Substring(0,1000));                                                     
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                                if (CreditCardCharge.AccountRef.FullName == null)
                                                {
                                                    CreditCardCharge.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                            if (CreditCardCharge.AccountRef.FullName == null)
                                            {
                                                CreditCardCharge.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());

                                        if (CreditCardCharge.AccountRef.FullName == null)
                                        {
                                            CreditCardCharge.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("PayeeEntityFullName"))
                            {
                                #region Validations of Payee Entity Full Name
                                if (dr["PayeeEntityFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["PayeeEntityFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This PayeeEntity fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                                if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                                {
                                                    CreditCardCharge.PayeeEntityRef.FullName = null;
                                                }
                                                else
                                                {
                                                    CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString().Substring(0,1000));
                                                }

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                                if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                                {
                                                    CreditCardCharge.PayeeEntityRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                            if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                            {
                                                CreditCardCharge.PayeeEntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());

                                        if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                        {
                                            CreditCardCharge.PayeeEntityRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            //Axis 617 
                            if (dt.Columns.Contains("Currency"))
                            {
                                #region Validations of Currency Full name
                                if (dr["Currency"].ToString() != string.Empty)
                                {
                                    string strCust = dr["Currency"].ToString();
                                    if (strCust.Length > 100)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (CreditCardCharge.CurrencyRef.FullName == null)
                                                {
                                                    CreditCardCharge.CurrencyRef.FullName = null;
                                                }
                                                else
                                                {
                                                    CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                                if (CreditCardCharge.CurrencyRef.FullName == null)
                                                {
                                                    CreditCardCharge.CurrencyRef.FullName = null;
                                                }
                                            }


                                        }
                                        else
                                        {
                                            CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (CreditCardCharge.CurrencyRef.FullName == null)
                                            {
                                                CreditCardCharge.CurrencyRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (CreditCardCharge.CurrencyRef.FullName == null)
                                        {
                                            CreditCardCharge.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion
                            }
                            //Axis 617 ends
                            if (dt.Columns.Contains("TxnDate"))
                            {
                                #region validations of TxnDate
                                if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                                {
                                    datevalue = dr["TxnDate"].ToString();
                                    if (!DateTime.TryParse(datevalue, out CreditCardChargeDt))
                                    {
                                        DateTime dttest = new DateTime();
                                        bool IsValid = false;

                                        try
                                        {
                                            dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            IsValid = true;
                                        }
                                        catch
                                        {
                                            IsValid = false;
                                        }
                                        if (IsValid == false)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    CreditCardCharge.TxnDate = datevalue;

                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    CreditCardCharge.TxnDate = datevalue;
                                                }
                                            }
                                            else
                                            {
                                                CreditCardCharge.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            CreditCardChargeDt = dttest;
                                            CreditCardCharge.TxnDate = dttest.ToString("yyyy-MM-dd");
                                        }

                                    }
                                    else
                                    {
                                        CreditCardChargeDt = Convert.ToDateTime(datevalue);
                                        CreditCardCharge.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("RefNumber"))
                            {
                                #region Validations of Ref Number
                                if (datevalue != string.Empty)
                                    CreditCardCharge.CreditCardChargeDate = CreditCardChargeDt;

                                if (dr["RefNumber"].ToString() != string.Empty)
                                {
                                    string strRefNum = dr["RefNumber"].ToString();
                                    if (strRefNum.Length > 21)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.RefNumber = dr["RefNumber"].ToString().Substring(0,21);
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.RefNumber = dr["RefNumber"].ToString();
                                            }
                                        }
                                        else
                                            CreditCardCharge.RefNumber = dr["RefNumber"].ToString();

                                    }
                                    else
                                        CreditCardCharge.RefNumber = dr["RefNumber"].ToString();
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("Memo"))
                            {
                                #region Validations for Memo
                                if (dr["Memo"].ToString() != string.Empty)
                                {
                                    if (dr["Memo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["Memo"].ToString().Substring(0,4000);
                                                CreditCardCharge.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["Memo"].ToString();
                                                CreditCardCharge.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["Memo"].ToString();
                                            CreditCardCharge.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        CreditCardCharge.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("IsTaxIncluded"))
                            {
                                #region Validations of IsTaxIncluded
                                if (dr["IsTaxIncluded"].ToString() != "<None>")
                                {

                                    int result = 0;
                                    if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                    {

                                        CreditCardCharge.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                    }
                                    else
                                    {
                                        string strvalid = string.Empty;
                                        if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                        {
                                            CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        else
                                        {
                                            if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                            {
                                                strvalid = "invalid";
                                            }
                                            else
                                                CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                        }
                                        if (strvalid != string.Empty)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(results) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(results) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(results) == "Ignore")
                                                {
                                                    CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                                if (Convert.ToString(results) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                            }
                                            else
                                            {
                                                CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();

                                            }
                                        }

                                    }
                                }
                                #endregion
                            }

                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("SalesTaxCodeFullName"))
                                {
                                    #region Validations of SalesTaxCode Full name

                                    if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string strSales = dr["SalesTaxCodeFullName"].ToString();
                                        if (strSales.Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                    if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                    if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                                {
                                                    CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());

                                            if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                            {
                                                CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (dt.Columns.Contains("ExchangeRate"))
                            {
                                #region Validations for ExchangeRate
                                if (dr["ExchangeRate"].ToString() != string.Empty)
                                {

                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + ") is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.ExchangeRate = dr["ExchangeRate"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        //CreditCardCharge.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                        CreditCardCharge.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();

                                    }
                                }

                                #endregion

                            }


                            #region Expense Line Add
                            DataProcessingBlocks.CreditModule.ExpenseLineAdd ExpLine = new DataProcessingBlocks.CreditModule.ExpenseLineAdd();

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                    {
                                        if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                            //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                            #endregion


                            if (dt.Columns.Contains("ExpenseAccountFullName"))
                            {
                                #region Validations of Account Full name
                                if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["ExpenseAccountFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                        
                                        if (ExpLine.AccountRef.FullName == null)
                                        {
                                            ExpLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseAmount"))
                            {
                                #region Validations for ExpenseAmount
                                if (dr["ExpenseAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExpenseAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAmount ( " + dr["ExpenseAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                            {
                                                if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["ExpenseAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                         CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                         CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    ExpLine.Amount = string.Format("{0:000000.00}", amount);

                                                }
                                            }
                                            //Check if ExpLine.Amount is null
                                            if (ExpLine.Amount == null)
                                            {
                                                ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseMemo"))
                            {
                                #region Validations for Memo
                                if (dr["ExpenseMemo"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseMemo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseMemo ( " + dr["ExpenseMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["ExpenseMemo"].ToString().Substring(0,4000);
                                                ExpLine.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["ExpenseMemo"].ToString();
                                                ExpLine.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["ExpenseMemo"].ToString();
                                            ExpLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["ExpenseMemo"].ToString();
                                        ExpLine.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ExpenseCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseCustomer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ExpLine.CustomerRef.FullName == null)
                                        {
                                            ExpLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseClassFullName"))
                            {
                                #region Validations of ExpenseClass Full name
                                if (dr["ExpenseClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseClass name (" + dr["ExpenseClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {

                                        }

                                    }
                                    else
                                    {
                                        ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (ExpLine.ClassRef.FullName == null)
                                        {
                                            ExpLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                {
                                    #region Validations of SalesTaxCode Full name
                                    if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string strSales = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                        if (strSales.Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Expense Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                    if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ExpLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                    if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ExpLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (dt.Columns.Contains("ExpenseBillableStatus"))
                            {
                                #region validations of  Expense Billable Status
                                if (dr["ExpenseBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ExpLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ExpLine.BillableStatus = dr["ExpenseBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                        if (ExpLine.SalesRepRef.FullName == null)
                                        {
                                            ExpLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt1 == null)
                                            ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                        {
                                            ExpLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt1 == null)
                                        ExpLine.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                    else
                                        ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, ExpLine.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                    if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                    {
                                        ExpLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ExpLine.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                            //        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                            //        {
                            //            ExpLine.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt2 == null)
                                            ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                        {
                                            ExpLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt2 == null)
                                        ExpLine.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                    else
                                        ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, ExpLine.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                    if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                    {
                                        ExpLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }


                            if (ExpLine.AccountRef != null || ExpLine.Amount != null || ExpLine.BillableStatus != null || ExpLine.ClassRef != null || ExpLine.CustomerRef != null || ExpLine.Memo != null || ExpLine.SalesTaxCodeRef != null /*|| ExpLine.SalesRepRef != null || ExpLine.DataExt.DataExtName != null || ExpLine.DataExt.DataExtValue != null*/)
                            {
                                CreditCardCharge.ExpenseLineAdd.Add(ExpLine);
                            }

                            #endregion

                            #region Item Line Add
                            DataProcessingBlocks.CreditModule.ItemLineAdd ItemLine = new DataProcessingBlocks.CreditModule.ItemLineAdd();

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //IsTaxable = string.Empty;
                            TaxRateValue = string.Empty;
                            ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                    {
                                        if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ItemFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemFullName"].ToString() != string.Empty)
                                {
                                    ItemLine.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                    if (ItemLine.ItemRef.FullName == null)
                                        ItemLine.ItemRef.FullName = null;

                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("InventorySiteFullName"))
                            {
                                #region Validations of InventorySiteFullName
                                if (dr["InventorySiteFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["InventorySiteFullName"].ToString();
                                    if (strAccount.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This InventorySite Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.InventorySiteRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Description"].ToString().Substring(0,4095);
                                                ItemLine.Desc = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Description"].ToString();
                                                ItemLine.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            ItemLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        //EstLine.Desc = strDesc;
                                        ItemLine.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                               ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    ItemLine.Quantity = strQuantity;
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0,31);
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    decimal cost = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cost ( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                            {
                                                if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLine.Cost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLine.Cost == null)
                                            {
                                                ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ItemAmount"))
                            {
                                #region Validations for ItemAmount
                                if (dr["ItemAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ItemAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemAmount ( " + dr["ItemAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                            {
                                                if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["ItemAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLine.Amount = string.Format("{0:000000.00}", amount);

                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLine.Amount == null)
                                            {
                                                ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion


                            }
                            if (dt.Columns.Contains("ItemCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString().Substring(0,1000));
                                                }

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.CustomerRef.FullName == null)
                                        {
                                            ItemLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemClassFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ItemClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Class name (" + dr["ItemClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                                {
                                                    ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (ItemLine.ClassRef.FullName == null)
                                        {
                                            ItemLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                {
                                    #region Validations of SalesTaxCode Full name
                                    if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string strSales = dr["ItemSalesTaxCodeFullName"].ToString();
                                        if (strSales.Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                    if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ItemLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                    if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ItemLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (dt.Columns.Contains("ItemBillableStatus"))
                            {
                                #region validations of Billable Status
                                if (dr["ItemBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemLine.BillableStatus = dr["ItemBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of OverrideItemAccount Full name
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["OverrideItemAccountFullName"].ToString();
                                    if (strSales.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This OverrideItemAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.OverrideItemAccountRef.FullName == null)
                                        {
                                            ItemLine.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            //Improvement::548
                            if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                            {
                                #region Validations of ItemSalesRepRef Full name
                                if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                        if (ItemLine.SalesRepRef.FullName == null)
                                        {
                                            ItemLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ItemLine.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                            //        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                            //        {
                            //            ItemLine.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt1 == null)
                                            ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                        {
                                            ItemLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt1 == null)
                                        ItemLine.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                    else
                                        ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, ItemLine.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                    if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                    {
                                        ItemLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ItemLine.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                            //        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                            //        {
                            //            ItemLine.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt2 == null)
                                            ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                        {
                                            ItemLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt2 == null)
                                        ItemLine.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                    else
                                        ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, ItemLine.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                    if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                    {
                                        ItemLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }


                            if (ItemLine.Amount != null || ItemLine.BillableStatus != null || ItemLine.ClassRef != null || ItemLine.Cost != null || ItemLine.CustomerRef != null || ItemLine.Desc != null || ItemLine.ItemRef != null || ItemLine.Quantity != null || ItemLine.SalesTaxCodeRef != null || ItemLine.UnitOfMeasure != null /*|| ItemLine.SalesRepRef != null || ItemLine.DataExt.DataExtName != null || ItemLine.DataExt.DataExtValue != null*/)
                            {
                                CreditCardCharge.ItemLineAdd.Add(ItemLine);
                            }

                            #endregion

                            coll.Add(CreditCardCharge);
                        }
                        else
                        {
                            #region Expense Line Add
                            DataProcessingBlocks.CreditModule.ExpenseLineAdd ExpLine = new DataProcessingBlocks.CreditModule.ExpenseLineAdd();

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //string IsTaxable = string.Empty;
                            string TaxRateValue = string.Empty;
                            string ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                    {
                                        if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                            //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ExpenseAccountFullName"))
                            {
                                #region Validations of Account Full name
                                if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["ExpenseAccountFullName"].ToString();
                                    if (strAccount.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                                if (ExpLine.AccountRef.FullName == null)
                                                {
                                                    ExpLine.AccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ExpLine.AccountRef.FullName == null)
                                        {
                                            ExpLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseAmount"))
                            {
                                #region Validations for ExpenseAmount
                                if (dr["ExpenseAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ExpenseAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseAmount ( " + dr["ExpenseAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ExpenseAmount"].ToString();
                                                ExpLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                            {
                                                if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["ExpenseAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                         CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                         CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    ExpLine.Amount = string.Format("{0:000000.00}", amount);

                                                }
                                            }
                                            //Check if ExpLine.Amount is null
                                            if (ExpLine.Amount == null)
                                            {
                                                ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseMemo"))
                            {
                                #region Validations for Memo
                                if (dr["ExpenseMemo"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseMemo"].ToString().Length > 4000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseMemo ( " + dr["ExpenseMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strMemo = dr["ExpenseMemo"].ToString().Substring(0,4000);
                                                ExpLine.Memo = strMemo;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strMemo = dr["ExpenseMemo"].ToString();
                                                ExpLine.Memo = strMemo;
                                            }
                                        }
                                        else
                                        {
                                            string strMemo = dr["ExpenseMemo"].ToString();
                                            ExpLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["ExpenseMemo"].ToString();
                                        ExpLine.Memo = strMemo;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ExpenseCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseCustomer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                                if (ExpLine.CustomerRef.FullName == null)
                                                {
                                                    ExpLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ExpLine.CustomerRef.FullName == null)
                                        {
                                            ExpLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ExpenseClassFullName"))
                            {
                                #region Validations of ExpenseClass Full name
                                if (dr["ExpenseClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ExpenseClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ExpenseClass name (" + dr["ExpenseClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                                if (ExpLine.ClassRef.FullName == null)
                                                {
                                                    ExpLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {

                                        }

                                    }
                                    else
                                    {
                                        ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (ExpLine.ClassRef.FullName == null)
                                        {
                                            ExpLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                {
                                    #region Validations of SalesTaxCode Full name
                                    if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string strSales = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                        if (strSales.Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Expense Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                    if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ExpLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                    if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ExpLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                                if (dt.Columns.Contains("ExpenseBillableStatus"))
                                {
                                    #region validations of  Expense Billable Status
                                    if (dr["ExpenseBillableStatus"].ToString() != string.Empty)
                                    {
                                        try
                                        {
                                            ExpLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseBillableStatus"].ToString(), true));
                                        }
                                        catch
                                        {
                                            ExpLine.BillableStatus = dr["ExpenseBillableStatus"].ToString();
                                        }
                                    }
                                    #endregion
                                }
                            }

                            //Improvement::548
                            if (dt.Columns.Contains("SalesRepRefFullName"))
                            {
                                #region Validations of SalesRepRef Full name
                                if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                                if (ExpLine.SalesRepRef.FullName == null)
                                                {
                                                    ExpLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                        if (ExpLine.SalesRepRef.FullName == null)
                                        {
                                            ExpLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ExpLine.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                            //        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                            //        {
                            //            ExpLine.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt1 == null)
                                                    ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                                else
                                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt1 == null)
                                            ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                        {
                                            ExpLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt1 == null)
                                        ExpLine.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                    else
                                        ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, ExpLine.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                    if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                    {
                                        ExpLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("OwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["OwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ExpLine.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                            //        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                            //        {
                            //            ExpLine.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["DataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["DataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ExpLine.DataExt2 == null)
                                                    ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                                else
                                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                                {
                                                    ExpLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt2 == null)
                                            ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                        {
                                            ExpLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("DataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["DataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ExpLine.DataExt2 == null)
                                        ExpLine.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                    else
                                        ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, ExpLine.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                    if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                    {
                                        ExpLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (ExpLine.AccountRef != null || ExpLine.Amount != null || ExpLine.BillableStatus != null || ExpLine.ClassRef != null || ExpLine.CustomerRef != null || ExpLine.Memo != null || ExpLine.SalesTaxCodeRef != null /*|| ExpLine.SalesRepRef != null || ExpLine.DataExt.DataExtName != null || ExpLine.DataExt.DataExtValue != null*/)
                            {
                                CreditCardCharge.ExpenseLineAdd.Add(ExpLine);
                            }

                            #endregion

                            #region Item Line Add
                            DataProcessingBlocks.CreditModule.ItemLineAdd ItemLine = new DataProcessingBlocks.CreditModule.ItemLineAdd();

                            #region Checking and setting SalesTaxCode

                            if (defaultSettings == null)
                            {
                                CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                return null;

                            }
                            //IsTaxable = string.Empty;
                            TaxRateValue = string.Empty;
                            ItemSaleTaxFullName = string.Empty;
                            //if default settings contain checkBoxGrossToNet checked.
                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                    {
                                        if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                        {
                                            string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                            ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                            TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                        }
                                    }
                                }
                            }
                            #endregion

                            if (dt.Columns.Contains("ItemFullName"))
                            {
                                #region Validations of item Full name
                                if (dr["ItemFullName"].ToString() != string.Empty)
                                {
                                    ItemLine.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                    if (ItemLine.ItemRef.FullName == null)
                                        ItemLine.ItemRef.FullName = null;

                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("InventorySiteFullName"))
                            {
                                #region Validations of InventorySiteFullName
                                if (dr["InventorySiteFullName"].ToString() != string.Empty)
                                {
                                    string strAccount = dr["InventorySiteFullName"].ToString();
                                    if (strAccount.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This InventorySite Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString().Substring(0, 31));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                                if (ItemLine.InventorySiteRef.FullName == null)
                                                {
                                                    ItemLine.InventorySiteRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.InventorySiteRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("Description"))
                            {
                                #region Validations for Description
                                if (dr["Description"].ToString() != string.Empty)
                                {
                                    if (dr["Description"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;

                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strDesc = dr["Description"].ToString().Substring(0,4095);
                                                ItemLine.Desc = strDesc;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strDesc = dr["Description"].ToString();
                                                ItemLine.Desc = strDesc;
                                            }
                                        }
                                        else
                                        {
                                            string strDesc = dr["Description"].ToString();
                                            ItemLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        //EstLine.Desc = strDesc;
                                        ItemLine.Desc = strDesc;
                                    }
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("SerialNumber"))
                            {
                                #region Validations of ItemLine SerialNumber
                                if (dr["SerialNumber"].ToString() != string.Empty)
                                {
                                    if (dr["SerialNumber"].ToString().Length > 4095)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("LotNumber"))
                            {
                                #region Validations of ItemLine LotNumber
                                if (dr["LotNumber"].ToString() != string.Empty)
                                {
                                    if (dr["LotNumber"].ToString().Length > 40)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.LotNumber = dr["LotNumber"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                #endregion
                            }

                            if (dt.Columns.Contains("Quantity"))
                            {
                                #region Validations for Quantity
                                if (dr["Quantity"].ToString() != string.Empty)
                                {
                                    string strQuantity = dr["Quantity"].ToString();
                                    ItemLine.Quantity = strQuantity;
                                }

                                #endregion

                            }

                            if (dt.Columns.Contains("UnitOfMeasure"))
                            {
                                #region Validations for UnitOfMeasure
                                if (dr["UnitOfMeasure"].ToString() != string.Empty)
                                {
                                    if (dr["UnitOfMeasure"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0,31);
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                                ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                            }
                                        }
                                        else
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("Cost"))
                            {
                                #region Validations for Cost
                                if (dr["Cost"].ToString() != string.Empty)
                                {
                                    decimal cost = 0;
                                    //decimal amount;
                                    if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Cost ( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strRate = dr["Cost"].ToString();
                                                ItemLine.Cost = strRate;
                                            }
                                        }
                                        else
                                        {
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                            {
                                                if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                                {
                                                    decimal Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        cost = Cost / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLine.Cost = Convert.ToString(Math.Round(cost, 5));
                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLine.Cost == null)
                                            {
                                                ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                        }
                                    }
                                }

                                #endregion

                            }
                            if (dt.Columns.Contains("ItemAmount"))
                            {
                                #region Validations for ItemAmount
                                if (dr["ItemAmount"].ToString() != string.Empty)
                                {
                                    decimal amount;
                                    if (!decimal.TryParse(dr["ItemAmount"].ToString(), out amount))
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemAmount ( " + dr["ItemAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                string strAmount = dr["ItemAmount"].ToString();
                                                ItemLine.Amount = strAmount;
                                            }
                                        }
                                        else
                                        {
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        if (defaultSettings.GrossToNet == "1")
                                        {
                                            if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                            {
                                                if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                                {
                                                    decimal Amount = Convert.ToDecimal(dr["ItemAmount"].ToString());
                                                    if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                        CommonUtilities.GetInstance().CountryVersion == "CA")
                                                    {
                                                        //decimal TaxRate = 10;
                                                        decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                        amount = Amount / (1 + (TaxRate / 100));
                                                    }

                                                    ItemLine.Amount = string.Format("{0:000000.00}", amount);

                                                }
                                            }
                                            //Check if ItemLine.Amount is null
                                            if (ItemLine.Amount == null)
                                            {
                                                ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                        }
                                    }
                                }

                                #endregion


                            }
                            if (dt.Columns.Contains("ItemCustomerFullName"))
                            {
                                #region Validations of Customer Full name
                                if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                                {
                                    string strCust = dr["ItemCustomerFullName"].ToString();
                                    if (strCust.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString().Substring(0,1000));
                                                }

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                                if (ItemLine.CustomerRef.FullName == null)
                                                {
                                                    ItemLine.CustomerRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.CustomerRef.FullName == null)
                                        {
                                            ItemLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (dt.Columns.Contains("ItemClassFullName"))
                            {
                                #region Validations of Class Full name
                                if (dr["ItemClassFullName"].ToString() != string.Empty)
                                {
                                    if (dr["ItemClassFullName"].ToString().Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Class name (" + dr["ItemClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                                if (ItemLine.ClassRef.FullName == null)
                                                {
                                                    ItemLine.ClassRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                        //string strClassFullName = string.Empty;
                                        if (ItemLine.ClassRef.FullName == null)
                                        {
                                            ItemLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                            {
                                if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                {
                                    #region Validations of SalesTaxCode Full name
                                    if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string strSales = dr["ItemSalesTaxCodeFullName"].ToString();
                                        if (strSales.Length > 3)
                                        {
                                            if (isIgnoreAll == false)
                                            {
                                                string strMessages = "This Item Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                                DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                                if (Convert.ToString(result) == "Cancel")
                                                {
                                                    continue;
                                                }
                                                if (Convert.ToString(result) == "No")
                                                {
                                                    return null;
                                                }
                                                if (Convert.ToString(result) == "Ignore")
                                                {
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                    if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ItemLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                    else
                                                    {
                                                        ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                    }
                                                }
                                                if (Convert.ToString(result) == "Abort")
                                                {
                                                    isIgnoreAll = true;
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                    if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                    {
                                                        ItemLine.SalesTaxCodeRef.FullName = null;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            //string strCustomerFullname = string.Empty;
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    #endregion

                                }
                            }
                            if (dt.Columns.Contains("ItemBillableStatus"))
                            {
                                #region validations of Billable Status
                                if (dr["ItemBillableStatus"].ToString() != string.Empty)
                                {
                                    try
                                    {
                                        ItemLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemBillableStatus"].ToString(), true));
                                    }
                                    catch
                                    {
                                        ItemLine.BillableStatus = dr["ItemBillableStatus"].ToString();
                                    }
                                }
                                #endregion
                            }
                            if (dt.Columns.Contains("OverrideItemAccountFullName"))
                            {
                                #region Validations of OverrideItemAccount Full name
                                if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["OverrideItemAccountFullName"].ToString();
                                    if (strSales.Length > 1000)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This OverrideItemAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0,1000));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                                if (ItemLine.OverrideItemAccountRef.FullName == null)
                                                {
                                                    ItemLine.OverrideItemAccountRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.OverrideItemAccountRef.FullName == null)
                                        {
                                            ItemLine.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            //Improvement::548
                            if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                            {
                                #region Validations of ItemSalesRepRef Full name
                                if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                    if (strSales.Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                                if (ItemLine.SalesRepRef.FullName == null)
                                                {
                                                    ItemLine.SalesRepRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                        if (ItemLine.SalesRepRef.FullName == null)
                                        {
                                            ItemLine.SalesRepRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID1"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                            //    {
                            //        ItemLine.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                            //        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                            //        {
                            //            ItemLine.DataExt1 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName1"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName1"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName1"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt1 == null)
                                                    ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                                else
                                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt1 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt1 == null)
                                            ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                        {
                                            ItemLine.DataExt1 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue1"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt1 == null)
                                        ItemLine.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                    else
                                        ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, ItemLine.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                    if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                    {
                                        ItemLine.DataExt1 = null;
                                    }

                                }

                                #endregion

                            }

                            ////Improvement::548
                            //DataExt.Dispose();
                            //if (dt.Columns.Contains("ItemOwnerID2"))
                            //{
                            //    #region Validations of OwnerID
                            //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                            //    {
                            //        ItemLine.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                            //        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                            //        {
                            //            ItemLine.DataExt2 = null;
                            //        }
                            //    }
                            //    #endregion
                            //}
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtName2"))
                            {
                                #region Validations of DataExtName
                                if (dr["ItemDataExtName2"].ToString() != string.Empty)
                                {
                                    if (dr["ItemDataExtName2"].ToString().Length > 31)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                if (ItemLine.DataExt2 == null)
                                                    ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                                else
                                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                                {
                                                    ItemLine.DataExt2 = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt2 == null)
                                            ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                        {
                                            ItemLine.DataExt2 = null;
                                        }
                                    }
                                }

                                #endregion

                            }
                            //Improvement::548
                            DataExt.Dispose();
                            if (dt.Columns.Contains("ItemDataExtValue2"))
                            {
                                #region Validations of DataExtValue
                                if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                                {
                                    if (ItemLine.DataExt2 == null)
                                        ItemLine.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                    else
                                        ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, ItemLine.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                    if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                    {
                                        ItemLine.DataExt2 = null;
                                    }

                                }

                                #endregion

                            }

                            if (ItemLine.Amount != null || ItemLine.BillableStatus != null || ItemLine.ClassRef != null || ItemLine.Cost != null || ItemLine.CustomerRef != null || ItemLine.Desc != null || ItemLine.ItemRef != null || ItemLine.Quantity != null || ItemLine.SalesTaxCodeRef != null || ItemLine.UnitOfMeasure != null /*|| ItemLine.SalesRepRef != null || ItemLine.DataExt.DataExtName != null || ItemLine.DataExt.DataExtValue != null*/)
                            {
                                CreditCardCharge.ItemLineAdd.Add(ItemLine);
                            }

                            #endregion
                            //coll.Add(Estimate);
                        }


                        #endregion
                    }
                    else
                    {
                        DataProcessingBlocks.CreditModule.CreditCardChargeEntry CreditCardCharge = new DataProcessingBlocks.CreditModule.CreditCardChargeEntry();

                        #region Check whether Statement Import functionality
                        if (CommonUtilities.GetInstance().IsStatementFeature())
                        {
                            CreditCardCharge.IsTaxIncluded = "true";
                        }
                        #endregion
                        #region Without Adding ref number
                        if (dt.Columns.Contains("AccountFullName"))
                        {
                            #region Validations of Account Full Name
                            if (dr["AccountFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["AccountFullName"].ToString();
                                if (strAccount.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Account fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                            if (CreditCardCharge.AccountRef.FullName == null)
                                            {
                                                CreditCardCharge.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                            if (CreditCardCharge.AccountRef.FullName == null)
                                            {
                                                CreditCardCharge.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());
                                        if (CreditCardCharge.AccountRef.FullName == null)
                                        {
                                            CreditCardCharge.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    CreditCardCharge.AccountRef = new AccountRef(dr["AccountFullName"].ToString());

                                    if (CreditCardCharge.AccountRef.FullName == null)
                                    {
                                        CreditCardCharge.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("PayeeEntityFullName"))
                        {
                            #region Validations of Payee Entity Full Name
                            if (dr["PayeeEntityFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["PayeeEntityFullName"].ToString();
                                if (strAccount.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This PayeeEntity fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                            if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                            {
                                                CreditCardCharge.PayeeEntityRef.FullName = null;
                                            }
                                            else
                                            {
                                                CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString().Substring(0,1000));
                                            }

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                            if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                            {
                                                CreditCardCharge.PayeeEntityRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());
                                        if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                        {
                                            CreditCardCharge.PayeeEntityRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    CreditCardCharge.PayeeEntityRef = new PayeeEntityRef(dr["PayeeEntityFullName"].ToString());

                                    if (CreditCardCharge.PayeeEntityRef.FullName == null)
                                    {
                                        CreditCardCharge.PayeeEntityRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }
                        //Axis 617 
                        if (dt.Columns.Contains("Currency"))
                        {
                            #region Validations of Currency Full name
                            if (dr["Currency"].ToString() != string.Empty)
                            {
                                string strCust = dr["Currency"].ToString();
                                if (strCust.Length > 100)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Currency is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (CreditCardCharge.CurrencyRef.FullName == null)
                                            {
                                                CreditCardCharge.CurrencyRef.FullName = null;
                                            }
                                            else
                                            {
                                                CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString().Substring(0, 1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                            if (CreditCardCharge.CurrencyRef.FullName == null)
                                            {
                                                CreditCardCharge.CurrencyRef.FullName = null;
                                            }
                                        }


                                    }
                                    else
                                    {
                                        CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                        if (CreditCardCharge.CurrencyRef.FullName == null)
                                        {
                                            CreditCardCharge.CurrencyRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    CreditCardCharge.CurrencyRef = new CurrencyRef(dr["Currency"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (CreditCardCharge.CurrencyRef.FullName == null)
                                    {
                                        CreditCardCharge.CurrencyRef.FullName = null;
                                    }
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("TxnDate"))
                        {
                            #region validations of TxnDate
                            if (dr["TxnDate"].ToString() != "<None>" || dr["TxnDate"].ToString() != string.Empty)
                            {
                                datevalue = dr["TxnDate"].ToString();
                                if (!DateTime.TryParse(datevalue, out CreditCardChargeDt))
                                {
                                    DateTime dttest = new DateTime();
                                    bool IsValid = false;

                                    try
                                    {
                                        dttest = DateTime.ParseExact(datevalue, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        IsValid = true;
                                    }
                                    catch
                                    {
                                        IsValid = false;
                                    }
                                    if (IsValid == false)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This TxnDate (" + datevalue + ") is not valid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.TxnDate = datevalue;

                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.TxnDate = datevalue;
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.TxnDate = datevalue;
                                        }
                                    }
                                    else
                                    {
                                        CreditCardChargeDt = dttest;
                                        CreditCardCharge.TxnDate = dttest.ToString("yyyy-MM-dd");
                                    }

                                }
                                else
                                {
                                    CreditCardChargeDt = Convert.ToDateTime(datevalue);
                                    CreditCardCharge.TxnDate = DateTime.Parse(datevalue).ToString("yyyy-MM-dd");
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("RefNumber"))
                        {
                            #region Validations of Ref Number
                            if (datevalue != string.Empty)
                                CreditCardCharge.CreditCardChargeDate = CreditCardChargeDt;

                            if (dr["RefNumber"].ToString() != string.Empty)
                            {
                                string strRefNum = dr["RefNumber"].ToString();
                                if (strRefNum.Length > 21)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Ref Number (" + dr["RefNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CreditCardCharge.RefNumber = dr["RefNumber"].ToString().Substring(0,21);
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CreditCardCharge.RefNumber = dr["RefNumber"].ToString();
                                        }
                                    }
                                    else
                                        CreditCardCharge.RefNumber = dr["RefNumber"].ToString();

                                }
                                else
                                    CreditCardCharge.RefNumber = dr["RefNumber"].ToString();
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("Memo"))
                        {
                            #region Validations for Memo
                            if (dr["Memo"].ToString() != string.Empty)
                            {
                                if (dr["Memo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Memo ( " + dr["Memo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["Memo"].ToString().Substring(0,4000);
                                            CreditCardCharge.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["Memo"].ToString();
                                            CreditCardCharge.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["Memo"].ToString();
                                        CreditCardCharge.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["Memo"].ToString();
                                    CreditCardCharge.Memo = strMemo;
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("IsTaxIncluded"))
                        {
                            #region Validations of IsTaxIncluded
                            if (dr["IsTaxIncluded"].ToString() != "<None>")
                            {

                                int result = 0;
                                if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                {

                                    CreditCardCharge.IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                }
                                else
                                {
                                    string strvalid = string.Empty;
                                    if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                    {
                                        CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    else
                                    {
                                        if (dr["IsTaxIncluded"].ToString().ToLower() != "false")
                                        {
                                            strvalid = "invalid";
                                        }
                                        else
                                            CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                    }
                                    if (strvalid != string.Empty)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This IsTaxIncluded (" + dr["IsTaxIncluded"].ToString() + ") is invalid for this mapping.If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult results = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(results) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(results) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(results) == "Ignore")
                                            {
                                                CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                            if (Convert.ToString(results) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();

                                        }
                                    }

                                }
                            }
                            #endregion
                        }
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (dt.Columns.Contains("SalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name

                                if (dr["SalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["SalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                                {
                                                    CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                                if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                                {
                                                    CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());
                                            if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                            {
                                                CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.SalesTaxCodeRef = new SalesTaxCodeRef(dr["SalesTaxCodeFullName"].ToString());

                                        if (CreditCardCharge.SalesTaxCodeRef.FullName == null)
                                        {
                                            CreditCardCharge.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                        }
                        if (dt.Columns.Contains("ExchangeRate"))
                        {
                            #region Validations for ExchangeRate
                            if (dr["ExchangeRate"].ToString() != string.Empty)
                            {

                                decimal amount;
                                if (!decimal.TryParse(dr["ExchangeRate"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExchangeRate ( " + dr["ExchangeRate"].ToString() + ") is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            CreditCardCharge.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            CreditCardCharge.ExchangeRate = dr["ExchangeRate"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        CreditCardCharge.ExchangeRate = dr["ExchangeRate"].ToString();
                                    }
                                }
                                else
                                {
                                   // CreditCardCharge.ExchangeRate = string.Format("{0:00000000.000000}", Convert.ToDouble(dr["ExchangeRate"].ToString()));
                                    CreditCardCharge.ExchangeRate = Math.Round(Convert.ToDecimal(dr["ExchangeRate"].ToString()), 5).ToString();

                                }
                            }

                            #endregion

                        }

                        #region Expense Line Add
                        DataProcessingBlocks.CreditModule.ExpenseLineAdd ExpLine = new DataProcessingBlocks.CreditModule.ExpenseLineAdd();

                        #region Checking and setting SalesTaxCode

                        if (defaultSettings == null)
                        {
                            CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return null;

                        }
                        //string IsTaxable = string.Empty;
                        string TaxRateValue = string.Empty;
                        string ItemSaleTaxFullName = string.Empty;
                        //if default settings contain checkBoxGrossToNet checked.
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                                {
                                    if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                        //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);
                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);
                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                        }
                        #endregion


                        if (dt.Columns.Contains("ExpenseAccountFullName"))
                        {
                            #region Validations of Account Full name
                            if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["ExpenseAccountFullName"].ToString();
                                if (strAccount.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                            if (ExpLine.AccountRef.FullName == null)
                                            {
                                                ExpLine.AccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                        if (ExpLine.AccountRef.FullName == null)
                                        {
                                            ExpLine.AccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.AccountRef = new AccountRef(dr["ExpenseAccountFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (ExpLine.AccountRef.FullName == null)
                                    {
                                        ExpLine.AccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseAmount"))
                        {
                            #region Validations for ExpenseAmount
                            if (dr["ExpenseAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ExpenseAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseAmount ( " + dr["ExpenseAmount"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["ExpenseAmount"].ToString();
                                            ExpLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["ExpenseAmount"].ToString();
                                        ExpLine.Amount = strAmount;
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                        {
                                            if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                            {
                                                decimal Amount = Convert.ToDecimal(dr["ExpenseAmount"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                     CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                     CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    amount = Amount / (1 + (TaxRate / 100));
                                                }

                                                ExpLine.Amount = string.Format("{0:000000.00}", amount);

                                            }
                                        }
                                        //Check if ExpLine.Amount is null
                                        if (ExpLine.Amount == null)
                                        {
                                            ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ExpenseAmount"].ToString()));
                                    }
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseMemo"))
                        {
                            #region Validations for Memo
                            if (dr["ExpenseMemo"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseMemo"].ToString().Length > 4000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseMemo ( " + dr["ExpenseMemo"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strMemo = dr["ExpenseMemo"].ToString().Substring(0,4000);
                                            ExpLine.Memo = strMemo;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strMemo = dr["ExpenseMemo"].ToString();
                                            ExpLine.Memo = strMemo;
                                        }
                                    }
                                    else
                                    {
                                        string strMemo = dr["ExpenseMemo"].ToString();
                                        ExpLine.Memo = strMemo;
                                    }
                                }
                                else
                                {
                                    string strMemo = dr["ExpenseMemo"].ToString();
                                    ExpLine.Memo = strMemo;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseCustomerFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ExpenseCustomerFullName"].ToString();
                                if (strCust.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseCustomer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                            if (ExpLine.CustomerRef.FullName == null)
                                            {
                                                ExpLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                        if (ExpLine.CustomerRef.FullName == null)
                                        {
                                            ExpLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ExpLine.CustomerRef = new CustomerRef(dr["ExpenseCustomerFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (ExpLine.CustomerRef.FullName == null)
                                    {
                                        ExpLine.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ExpenseClassFullName"))
                        {
                            #region Validations of ExpenseClass Full name
                            if (dr["ExpenseClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["ExpenseClassFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ExpenseClass name (" + dr["ExpenseClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                            if (ExpLine.ClassRef.FullName == null)
                                            {
                                                ExpLine.ClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                            if (ExpLine.ClassRef.FullName == null)
                                            {
                                                ExpLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {

                                    }

                                }
                                else
                                {
                                    ExpLine.ClassRef = new ClassRef(dr["ExpenseClassFullName"].ToString());
                                    //string strClassFullName = string.Empty;
                                    if (ExpLine.ClassRef.FullName == null)
                                    {
                                        ExpLine.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (dt.Columns.Contains("ExpenseSalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["ExpenseSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ExpenseSalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Expense Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                                if (ExpLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ExpLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                            if (ExpLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ExpLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ExpenseSalesTaxCodeFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ExpLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ExpLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                        }
                        if (dt.Columns.Contains("ExpenseBillableStatus"))
                        {
                            #region validations of  Expense Billable Status
                            if (dr["ExpenseBillableStatus"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ExpLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ExpenseBillableStatus"].ToString(), true));
                                }
                                catch
                                {
                                    ExpLine.BillableStatus = dr["ExpenseBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }

                        //Improvement::548
                        if (dt.Columns.Contains("SalesRepRefFullName"))
                        {
                            #region Validations of SalesRepRef Full name
                            if (dr["SalesRepRefFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["SalesRepRefFullName"].ToString();
                                if (strSales.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense SalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }
                                            else
                                            {
                                                ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                            if (ExpLine.SalesRepRef.FullName == null)
                                            {
                                                ExpLine.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());
                                        if (ExpLine.SalesRepRef.FullName == null)
                                        {
                                            ExpLine.SalesRepRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    ExpLine.SalesRepRef = new SalesRepRef(dr["SalesRepRefFullName"].ToString());

                                    if (ExpLine.SalesRepRef.FullName == null)
                                    {
                                        ExpLine.SalesRepRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("OwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["OwnerID1"].ToString() != string.Empty)
                        //    {
                        //        ExpLine.DataExt1 = DataExt.GetInstance(dr["OwnerID1"].ToString(), null, null);
                        //        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                        //        {
                        //            ExpLine.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}

                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName1 (" + dr["DataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString().Substring(0, 31), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ExpLine.DataExt1 == null)
                                                ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                            else
                                                ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                            if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                            {
                                                ExpLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt1 == null)
                                            ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                        else
                                            ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                        if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                        {
                                            ExpLine.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ExpLine.DataExt1 == null)
                                        ExpLine.DataExt1 = DataExt.GetInstance(null, dr["DataExtName1"].ToString(), null);
                                    else
                                        ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, dr["DataExtName1"].ToString(), ExpLine.DataExt1.DataExtValue);
                                    if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                    {
                                        ExpLine.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue1"].ToString() != string.Empty)
                            {
                                if (ExpLine.DataExt1 == null)
                                    ExpLine.DataExt1 = DataExt.GetInstance(null, null, dr["DataExtValue1"].ToString());
                                else
                                    ExpLine.DataExt1 = DataExt.GetInstance(ExpLine.DataExt1.OwnerID == null ? "0" : ExpLine.DataExt1.OwnerID, ExpLine.DataExt1.DataExtName, dr["DataExtValue1"].ToString());
                                if (ExpLine.DataExt1.OwnerID == null && ExpLine.DataExt1.DataExtName == null && ExpLine.DataExt1.DataExtValue == null)
                                {
                                    ExpLine.DataExt1 = null;
                                }

                            }

                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("OwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["OwnerID2"].ToString() != string.Empty)
                        //    {
                        //        ExpLine.DataExt2 = DataExt.GetInstance(dr["OwnerID2"].ToString(), null, null);
                        //        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                        //        {
                        //            ExpLine.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["DataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["DataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This DataExtName2 (" + dr["DataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString().Substring(0, 31), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ExpLine.DataExt2 == null)
                                                ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                            else
                                                ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                            if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                            {
                                                ExpLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ExpLine.DataExt2 == null)
                                            ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                        else
                                            ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                        if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                        {
                                            ExpLine.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ExpLine.DataExt2 == null)
                                        ExpLine.DataExt2 = DataExt.GetInstance(null, dr["DataExtName2"].ToString(), null);
                                    else
                                        ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, dr["DataExtName2"].ToString(), ExpLine.DataExt2.DataExtValue);
                                    if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                    {
                                        ExpLine.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("DataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["DataExtValue2"].ToString() != string.Empty)
                            {
                                if (ExpLine.DataExt2 == null)
                                    ExpLine.DataExt2 = DataExt.GetInstance(null, null, dr["DataExtValue2"].ToString());
                                else
                                    ExpLine.DataExt2 = DataExt.GetInstance(ExpLine.DataExt2.OwnerID == null ? "0" : ExpLine.DataExt2.OwnerID, ExpLine.DataExt2.DataExtName, dr["DataExtValue2"].ToString());
                                if (ExpLine.DataExt2.OwnerID == null && ExpLine.DataExt2.DataExtName == null && ExpLine.DataExt2.DataExtValue == null)
                                {
                                    ExpLine.DataExt2 = null;
                                }

                            }

                            #endregion

                        }

                        if (ExpLine.AccountRef != null || ExpLine.Amount != null || ExpLine.BillableStatus != null || ExpLine.ClassRef != null || ExpLine.CustomerRef != null || ExpLine.Memo != null || ExpLine.SalesTaxCodeRef != null /*|| ExpLine.SalesRepRef != null || ExpLine.DataExt.DataExtName != null || ExpLine.DataExt.DataExtValue != null*/)
                        {
                            CreditCardCharge.ExpenseLineAdd.Add(ExpLine);
                        }

                        #endregion

                        #region Item Line Add
                        DataProcessingBlocks.CreditModule.ItemLineAdd ItemLine = new DataProcessingBlocks.CreditModule.ItemLineAdd();

                        #region Checking and setting SalesTaxCode

                        if (defaultSettings == null)
                        {
                            CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                            MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return null;

                        }
                        //IsTaxable = string.Empty;
                        TaxRateValue = string.Empty;
                        ItemSaleTaxFullName = string.Empty;
                        //if default settings contain checkBoxGrossToNet checked.
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (defaultSettings.GrossToNet == "1")
                            {
                                if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                {
                                    if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                    {
                                        string FullName = dr["ItemSalesTaxCodeFullName"].ToString();

                                        ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                        TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                    }
                                }
                            }
                        }
                        #endregion

                        if (dt.Columns.Contains("ItemFullName"))
                        {
                            #region Validations of item Full name
                            if (dr["ItemFullName"].ToString() != string.Empty)
                            {
                                ItemLine.ItemRef = new ItemRef(dr["ItemFullName"].ToString());
                                if (ItemLine.ItemRef.FullName == null)
                                    ItemLine.ItemRef.FullName = null;

                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("InventorySiteFullName"))
                        {
                            #region Validations of InventorySiteFullName
                            if (dr["InventorySiteFullName"].ToString() != string.Empty)
                            {
                                string strAccount = dr["InventorySiteFullName"].ToString();
                                if (strAccount.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This InventorySite Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString().Substring(0, 31));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                            if (ItemLine.InventorySiteRef.FullName == null)
                                            {
                                                ItemLine.InventorySiteRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                        if (ItemLine.InventorySiteRef.FullName == null)
                                        {
                                            ItemLine.InventorySiteRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.InventorySiteRef = new InventorySiteRef(dr["InventorySiteFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (ItemLine.InventorySiteRef.FullName == null)
                                    {
                                        ItemLine.InventorySiteRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("Description"))
                        {
                            #region Validations for Description
                            if (dr["Description"].ToString() != string.Empty)
                            {
                                if (dr["Description"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Description ( " + dr["Description"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;

                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strDesc = dr["Description"].ToString().Substring(0,4095);
                                            ItemLine.Desc = strDesc;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strDesc = dr["Description"].ToString();
                                            ItemLine.Desc = strDesc;
                                        }
                                    }
                                    else
                                    {
                                        string strDesc = dr["Description"].ToString();
                                        ItemLine.Desc = strDesc;
                                    }
                                }
                                else
                                {
                                    string strDesc = dr["Description"].ToString();
                                    //EstLine.Desc = strDesc;
                                    ItemLine.Desc = strDesc;
                                }
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("SerialNumber"))
                        {
                            #region Validations of ItemLine SerialNumber
                            if (dr["SerialNumber"].ToString() != string.Empty)
                            {
                                if (dr["SerialNumber"].ToString().Length > 4095)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This SerialNumber (" + dr["SerialNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString().Substring(0, 4095);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemLine.SerialNumber = dr["SerialNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("LotNumber"))
                        {
                            #region Validations of ItemLine LotNumber
                            if (dr["LotNumber"].ToString() != string.Empty)
                            {
                                if (dr["LotNumber"].ToString().Length > 40)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This LotNumber (" + dr["LotNumber"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.LotNumber = dr["LotNumber"].ToString().Substring(0, 40);

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.LotNumber = dr["LotNumber"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.LotNumber = dr["LotNumber"].ToString();
                                    }
                                }
                                else
                                {
                                    ItemLine.LotNumber = dr["LotNumber"].ToString();
                                }
                            }
                            #endregion
                        }

                        if (dt.Columns.Contains("Quantity"))
                        {
                            #region Validations for Quantity
                            if (dr["Quantity"].ToString() != string.Empty)
                            {
                                string strQuantity = dr["Quantity"].ToString();
                                ItemLine.Quantity = strQuantity;
                            }

                            #endregion

                        }

                        if (dt.Columns.Contains("UnitOfMeasure"))
                        {
                            #region Validations for UnitOfMeasure
                            if (dr["UnitOfMeasure"].ToString() != string.Empty)
                            {
                                if (dr["UnitOfMeasure"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This UnitOfMeasure ( " + dr["UnitOfMeasure"].ToString() + " ) is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString().Substring(0,31);
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                            ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                        }
                                    }
                                    else
                                    {
                                        string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                        ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                    }
                                }
                                else
                                {
                                    string strUnitOfMeasure = dr["UnitOfMeasure"].ToString();
                                    ItemLine.UnitOfMeasure = strUnitOfMeasure;
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("Cost"))
                        {
                            #region Validations for Cost
                            if (dr["Cost"].ToString() != string.Empty)
                            {
                                decimal cost = 0;
                                //decimal amount;
                                if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Cost ( " + dr["Cost"].ToString() + " ) is not valid for quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strRate = dr["Cost"].ToString();
                                            ItemLine.Cost = strRate;
                                        }
                                    }
                                    else
                                    {
                                        string strRate = dr["Cost"].ToString();
                                        ItemLine.Cost = strRate;
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                        {
                                            if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                            {
                                                decimal Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    cost = Cost / (1 + (TaxRate / 100));
                                                }

                                                ItemLine.Cost = Convert.ToString(Math.Round(cost, 5));
                                            }
                                        }
                                        //Check if ItemLine.Amount is null
                                        if (ItemLine.Cost == null)
                                        {
                                            ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.Cost = string.Format("{0:000000.00}", Convert.ToDouble(dr["Cost"].ToString()));
                                    }
                                }
                            }

                            #endregion

                        }
                        if (dt.Columns.Contains("ItemAmount"))
                        {
                            #region Validations for ItemAmount
                            if (dr["ItemAmount"].ToString() != string.Empty)
                            {
                                decimal amount;
                                if (!decimal.TryParse(dr["ItemAmount"].ToString(), out amount))
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemAmount ( " + dr["ItemAmount"].ToString() + " ) is not valid .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            string strAmount = dr["ItemAmount"].ToString();
                                            ItemLine.Amount = strAmount;
                                        }
                                    }
                                    else
                                    {
                                        string strAmount = dr["ItemAmount"].ToString();
                                        ItemLine.Amount = strAmount;
                                    }
                                }
                                else
                                {
                                    if (defaultSettings.GrossToNet == "1")
                                    {
                                        if (TaxRateValue != string.Empty && CreditCardCharge.IsTaxIncluded != null && CreditCardCharge.IsTaxIncluded != string.Empty)
                                        {
                                            if (CreditCardCharge.IsTaxIncluded == "true" || CreditCardCharge.IsTaxIncluded == "1")
                                            {
                                                decimal Amount = Convert.ToDecimal(dr["ItemAmount"].ToString());
                                                if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                    CommonUtilities.GetInstance().CountryVersion == "CA")
                                                {
                                                    //decimal TaxRate = 10;
                                                    decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                    amount = Amount / (1 + (TaxRate / 100));
                                                }

                                                ItemLine.Amount = string.Format("{0:000000.00}", amount);

                                            }
                                        }
                                        //Check if ItemLine.Amount is null
                                        if (ItemLine.Amount == null)
                                        {
                                            ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.Amount = string.Format("{0:000000.00}", Convert.ToDouble(dr["ItemAmount"].ToString()));
                                    }
                                }
                            }

                            #endregion


                        }
                        if (dt.Columns.Contains("ItemCustomerFullName"))
                        {
                            #region Validations of Customer Full name
                            if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                            {
                                string strCust = dr["ItemCustomerFullName"].ToString();
                                if (strCust.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Customer fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString().Substring(0,1000));
                                            }

                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                            if (ItemLine.CustomerRef.FullName == null)
                                            {
                                                ItemLine.CustomerRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                        if (ItemLine.CustomerRef.FullName == null)
                                        {
                                            ItemLine.CustomerRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.CustomerRef = new CustomerRef(dr["ItemCustomerFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (ItemLine.CustomerRef.FullName == null)
                                    {
                                        ItemLine.CustomerRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (dt.Columns.Contains("ItemClassFullName"))
                        {
                            #region Validations of Class Full name
                            if (dr["ItemClassFullName"].ToString() != string.Empty)
                            {
                                if (dr["ItemClassFullName"].ToString().Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Item Class name (" + dr["ItemClassFullName"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                            if (ItemLine.ClassRef.FullName == null)
                                            {
                                                ItemLine.ClassRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                        if (ItemLine.ClassRef.FullName == null)
                                        {
                                            ItemLine.ClassRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.ClassRef = new ClassRef(dr["ItemClassFullName"].ToString());
                                    //string strClassFullName = string.Empty;
                                    if (ItemLine.ClassRef.FullName == null)
                                    {
                                        ItemLine.ClassRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }
                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                        {
                            if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                            {
                                #region Validations of SalesTaxCode Full name
                                if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                {
                                    string strSales = dr["ItemSalesTaxCodeFullName"].ToString();
                                    if (strSales.Length > 3)
                                    {
                                        if (isIgnoreAll == false)
                                        {
                                            string strMessages = "This Item Sales Tax Code Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                            DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                            if (Convert.ToString(result) == "Cancel")
                                            {
                                                continue;
                                            }
                                            if (Convert.ToString(result) == "No")
                                            {
                                                return null;
                                            }
                                            if (Convert.ToString(result) == "Ignore")
                                            {
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                                else
                                                {
                                                    ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString().Substring(0, 3));
                                                }
                                            }
                                            if (Convert.ToString(result) == "Abort")
                                            {
                                                isIgnoreAll = true;
                                                ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                                if (ItemLine.SalesTaxCodeRef.FullName == null)
                                                {
                                                    ItemLine.SalesTaxCodeRef.FullName = null;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                            if (ItemLine.SalesTaxCodeRef.FullName == null)
                                            {
                                                ItemLine.SalesTaxCodeRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesTaxCodeRef = new SalesTaxCodeRef(dr["ItemSalesTaxCodeFullName"].ToString());
                                        //string strCustomerFullname = string.Empty;
                                        if (ItemLine.SalesTaxCodeRef.FullName == null)
                                        {
                                            ItemLine.SalesTaxCodeRef.FullName = null;
                                        }
                                    }
                                }
                                #endregion

                            }
                        }
                        if (dt.Columns.Contains("ItemBillableStatus"))
                        {
                            #region validations of Billable Status
                            if (dr["ItemBillableStatus"].ToString() != string.Empty)
                            {
                                try
                                {
                                    ItemLine.BillableStatus = Convert.ToString((DataProcessingBlocks.BillableStatus)Enum.Parse(typeof(DataProcessingBlocks.BillableStatus), dr["ItemBillableStatus"].ToString(), true));
                                }
                                catch
                                {
                                    ItemLine.BillableStatus = dr["ItemBillableStatus"].ToString();
                                }
                            }
                            #endregion
                        }
                        if (dt.Columns.Contains("OverrideItemAccountFullName"))
                        {
                            #region Validations of OverrideItemAccount Full name
                            if (dr["OverrideItemAccountFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["OverrideItemAccountFullName"].ToString();
                                if (strSales.Length > 1000)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This OverrideItemAccount Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString().Substring(0,1000));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                            if (ItemLine.OverrideItemAccountRef.FullName == null)
                                            {
                                                ItemLine.OverrideItemAccountRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                        if (ItemLine.OverrideItemAccountRef.FullName == null)
                                        {
                                            ItemLine.OverrideItemAccountRef.FullName = null;
                                        }
                                    }
                                }
                                else
                                {
                                    ItemLine.OverrideItemAccountRef = new OverrideItemAccountRef(dr["OverrideItemAccountFullName"].ToString());
                                    //string strCustomerFullname = string.Empty;
                                    if (ItemLine.OverrideItemAccountRef.FullName == null)
                                    {
                                        ItemLine.OverrideItemAccountRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        //Improvement::548
                        if (dt.Columns.Contains("ItemSalesRepRefFullName"))
                        {
                            #region Validations of ItemSalesRepRef Full name
                            if (dr["ItemSalesRepRefFullName"].ToString() != string.Empty)
                            {
                                string strSales = dr["ItemSalesRepRefFullName"].ToString();
                                if (strSales.Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This Expense ItemSalesRepRef Fullname is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }
                                            else
                                            {
                                                ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString().Substring(0, 3));
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                            if (ItemLine.SalesRepRef.FullName == null)
                                            {
                                                ItemLine.SalesRepRef.FullName = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());
                                        if (ItemLine.SalesRepRef.FullName == null)
                                        {
                                            ItemLine.SalesRepRef.FullName = null;
                                        }

                                    }
                                }
                                else
                                {
                                    ItemLine.SalesRepRef = new SalesRepRef(dr["ItemSalesRepRefFullName"].ToString());

                                    if (ItemLine.SalesRepRef.FullName == null)
                                    {
                                        ItemLine.SalesRepRef.FullName = null;
                                    }
                                }
                            }
                            #endregion

                        }

                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemOwnerID1"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemOwnerID1"].ToString() != string.Empty)
                        //    {
                        //        ItemLine.DataExt1 = DataExt.GetInstance(dr["ItemOwnerID1"].ToString(), null, null);
                        //        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                        //        {
                        //            ItemLine.DataExt1 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtName1"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemDataExtName1"].ToString() != string.Empty)
                            {
                                if (dr["ItemDataExtName1"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemDataExtName1 (" + dr["ItemDataExtName1"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString().Substring(0, 31), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ItemLine.DataExt1 == null)
                                                ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                            else
                                                ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                            if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                            {
                                                ItemLine.DataExt1 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt1 == null)
                                            ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                        else
                                            ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                        if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                        {
                                            ItemLine.DataExt1 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ItemLine.DataExt1 == null)
                                        ItemLine.DataExt1 = DataExt.GetInstance(null, dr["ItemDataExtName1"].ToString(), null);
                                    else
                                        ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, dr["ItemDataExtName1"].ToString(), ItemLine.DataExt1.DataExtValue);
                                    if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                    {
                                        ItemLine.DataExt1 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtValue1"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemDataExtValue1"].ToString() != string.Empty)
                            {
                                if (ItemLine.DataExt1 == null)
                                    ItemLine.DataExt1 = DataExt.GetInstance(null, null, dr["ItemDataExtValue1"].ToString());
                                else
                                    ItemLine.DataExt1 = DataExt.GetInstance(ItemLine.DataExt1.OwnerID == null ? "0" : ItemLine.DataExt1.OwnerID, ItemLine.DataExt1.DataExtName, dr["ItemDataExtValue1"].ToString());
                                if (ItemLine.DataExt1.OwnerID == null && ItemLine.DataExt1.DataExtName == null && ItemLine.DataExt1.DataExtValue == null)
                                {
                                    ItemLine.DataExt1 = null;
                                }

                            }

                            #endregion

                        }
                        ////Improvement::548
                        //DataExt.Dispose();
                        //if (dt.Columns.Contains("ItemOwnerID2"))
                        //{
                        //    #region Validations of OwnerID
                        //    if (dr["ItemOwnerID2"].ToString() != string.Empty)
                        //    {
                        //        ItemLine.DataExt2 = DataExt.GetInstance(dr["ItemOwnerID2"].ToString(), null, null);
                        //        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                        //        {
                        //            ItemLine.DataExt2 = null;
                        //        }
                        //    }
                        //    #endregion
                        //}
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtName2"))
                        {
                            #region Validations of DataExtName
                            if (dr["ItemDataExtName2"].ToString() != string.Empty)
                            {
                                if (dr["ItemDataExtName2"].ToString().Length > 31)
                                {
                                    if (isIgnoreAll == false)
                                    {
                                        string strMessages = "This ItemDataExtName2 (" + dr["ItemDataExtName2"].ToString() + ") is exceeded maximum length of quickbooks .If you press cancel this record will not be added to QuickBooks.If Ignore it will proceed.";
                                        DialogResult result = CommonUtilities.ShowMessage(strMessages, MessageBoxIcon.Warning);
                                        if (Convert.ToString(result) == "Cancel")
                                        {
                                            continue;
                                        }
                                        if (Convert.ToString(result) == "No")
                                        {
                                            return null;
                                        }
                                        if (Convert.ToString(result) == "Ignore")
                                        {
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString().Substring(0, 31), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                        if (Convert.ToString(result) == "Abort")
                                        {
                                            isIgnoreAll = true;
                                            if (ItemLine.DataExt2 == null)
                                                ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                            else
                                                ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                            if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                            {
                                                ItemLine.DataExt2 = null;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (ItemLine.DataExt2 == null)
                                            ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                        else
                                            ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                        if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                        {
                                            ItemLine.DataExt2 = null;
                                        }
                                    }
                                }
                                else
                                {
                                    if (ItemLine.DataExt2 == null)
                                        ItemLine.DataExt2 = DataExt.GetInstance(null, dr["ItemDataExtName2"].ToString(), null);
                                    else
                                        ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, dr["ItemDataExtName2"].ToString(), ItemLine.DataExt2.DataExtValue);
                                    if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                    {
                                        ItemLine.DataExt2 = null;
                                    }
                                }
                            }

                            #endregion

                        }
                        //Improvement::548
                        DataExt.Dispose();
                        if (dt.Columns.Contains("ItemDataExtValue2"))
                        {
                            #region Validations of DataExtValue
                            if (dr["ItemDataExtValue2"].ToString() != string.Empty)
                            {
                                if (ItemLine.DataExt2 == null)
                                    ItemLine.DataExt2 = DataExt.GetInstance(null, null, dr["ItemDataExtValue2"].ToString());
                                else
                                    ItemLine.DataExt2 = DataExt.GetInstance(ItemLine.DataExt2.OwnerID == null ? "0" : ItemLine.DataExt2.OwnerID, ItemLine.DataExt2.DataExtName, dr["ItemDataExtValue2"].ToString());
                                if (ItemLine.DataExt2.OwnerID == null && ItemLine.DataExt2.DataExtName == null && ItemLine.DataExt2.DataExtValue == null)
                                {
                                    ItemLine.DataExt2 = null;
                                }

                            }

                            #endregion

                        }


                        if (ItemLine.Amount != null || ItemLine.BillableStatus != null || ItemLine.ClassRef != null || ItemLine.Cost != null || ItemLine.CustomerRef != null || ItemLine.Desc != null || ItemLine.ItemRef != null || ItemLine.Quantity != null || ItemLine.SalesTaxCodeRef != null || ItemLine.UnitOfMeasure != null /*|| ItemLine.SalesRepRef != null || ItemLine.DataExt.DataExtName != null || ItemLine.DataExt.DataExtValue != null*/)
                        {
                            CreditCardCharge.ItemLineAdd.Add(ItemLine);
                        }

                        #endregion

                        coll.Add(CreditCardCharge);


                        #endregion
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion              

            #region Customer,Item and Account Requests

            foreach (DataRow dr in dt.Rows)
            {
                if (bkWorker.CancellationPending != true)
                {
                    System.Threading.Thread.Sleep(100);
                    //bug 501
                    if (CommonUtilities.GetInstance().IsStatementFeature() == false)
                    {
                        try
                        {
                            bkWorker.ReportProgress(listCount * 100 / dt.Rows.Count);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    else 
                    {
                        CommonUtilities.GetInstance().ValidateFlag = true;
                        CommonUtilities.GetInstance().ListFlag = false;
                    }
                    
                    listCount++;
                    if (CommonUtilities.GetInstance().SkipListFlag == false)
                    {
                        #region PayeeEntityFullName if not present
                        if (dt.Columns.Contains("PayeeEntityFullName"))
                        {
                            if (dr["PayeeEntityFullName"].ToString() != string.Empty)
                            {
                                //Axis 617 
                                string[] CurrencyArr = new string[10];
                                if (dt.Columns.Contains("Currency"))
                                {
                                    if (dr["Currency"].ToString() != string.Empty)
                                    {
                                        string Currency = dr["Currency"].ToString();

                                        CurrencyArr[0] = dr["Currency"].ToString();
                                    }
                                }
                                //Axis 617 ends

                                #region Set Supplier Query
                                XmlDocument pxmldoc = new XmlDocument();
                                pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                pxmldoc.AppendChild(qbXML);
                                XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                qbXML.AppendChild(qbXMLMsgsRq);
                                qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                XmlElement VendorQueryRq = pxmldoc.CreateElement("VendorQueryRq");
                                qbXMLMsgsRq.AppendChild(VendorQueryRq);
                                VendorQueryRq.SetAttribute("requestID", "1");
                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                FullName.InnerText = dr["PayeeEntityFullName"].ToString();
                                VendorQueryRq.AppendChild(FullName);

                                string pinput = pxmldoc.OuterXml;

                                string resp = string.Empty;
                                try
                                {
                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                    }

                                    else
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                }

                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                finally
                                {
                                    if (resp != string.Empty)
                                    {

                                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                        outputXMLDoc.LoadXml(resp);
                                        string statusSeverity = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorQueryRs"))
                                        {
                                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                    }
                                    outputXMLDoc.RemoveAll();
                                    if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                    {
                                        #region Vendor Add Query

                                            XmlDocument xmldocadd = new XmlDocument();
                                            xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                            xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                            XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                            xmldocadd.AppendChild(qbXMLcust);
                                            XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                            qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                            qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                            XmlElement VendorAddRq = xmldocadd.CreateElement("VendorAddRq");
                                            qbXMLMsgsRqcust.AppendChild(VendorAddRq);
                                            VendorAddRq.SetAttribute("requestID", "1");
                                            XmlElement VendorAdd = xmldocadd.CreateElement("VendorAdd");
                                            VendorAddRq.AppendChild(VendorAdd);
                                            XmlElement Name = xmldocadd.CreateElement("Name");
                                            Name.InnerText = dr["PayeeEntityFullName"].ToString();
                                            VendorAdd.AppendChild(Name);

                                            //Axis 617 
                                            #region Currency of Customer.  
                                            if (CurrencyArr[0] != null && CurrencyArr[0] != "")
                                            {
                                                XmlElement CurrencyRef = xmldocadd.CreateElement("CurrencyRef");
                                                XmlElement VendorFullName = xmldocadd.CreateElement("FullName");
                                                VendorFullName.InnerText = CurrencyArr[0];
                                                CurrencyRef.AppendChild(VendorFullName);
                                                VendorAdd.AppendChild(CurrencyRef);
                                            }
                                            #endregion
                                            //Axis 617 ends

                                            string vendinput = xmldocadd.OuterXml;
                                            string respVend = string.Empty;
                                            try
                                            {
                                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                {
                                                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                    respVend = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, vendinput);

                                                }

                                                else
                                                    respVend = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, vendinput);
                                            }
                                            catch (Exception ex)
                                            {
                                                CommonUtilities.WriteErrorLog(ex.Message);
                                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            }
                                            finally
                                            {
                                                if (respVend != string.Empty)
                                                {
                                                    System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                    outputcustXMLDoc.LoadXml(respVend);
                                                    foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/VendorAddRs"))
                                                    {
                                                        string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                        if (statusSeveritycust == "Error")
                                                        {
                                                            string msg = "New Vendor(PayeeEntity) could not be created into QuickBooks \n ";
                                                            msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                            //MessageBox.Show(msg);
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(msg);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(msg);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion
                                        }
                                    }

                                }

                                #endregion
                            }

                    }
                    if (dt.Columns.Contains("ExpenseCustomerFullName"))
                    {

                            if (dr["ExpenseCustomerFullName"].ToString() != string.Empty)
                            {
                                string customerName = dr["ExpenseCustomerFullName"].ToString();
                                string[] arr = new string[15];
                                if (customerName.Contains(":"))
                                {
                                    arr = customerName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ExpenseCustomerFullName"].ToString();
                                }
                                #region Set Customer Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                        qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                        CustomerQueryRq.SetAttribute("requestID", "1");
                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    CustomerQueryRq.AppendChild(FullName);
                                                }

                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            CustomerQueryRq.AppendChild(FullName);
                                        }
                                        string pinput = pxmldoc.OuterXml;

                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {

                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                    }
                                    outputXMLDoc.RemoveAll();
                                    if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                    {

                                                    #region Customer Add Query

                                                    XmlDocument xmldocadd = new XmlDocument();
                                                    xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                                    xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                                    xmldocadd.AppendChild(qbXMLcust);
                                                    XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                                    qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                                    qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                                    XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                                    qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                                    CustomerAddRq.SetAttribute("requestID", "1");
                                                    XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                                    CustomerAddRq.AppendChild(CustomerAdd);
                                                    XmlElement Name = xmldocadd.CreateElement("Name");
                                                    Name.InnerText = arr[i];
                                                    CustomerAdd.AppendChild(Name);
                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = xmldocadd.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = xmldocadd.CreateElement("ParentRef");
                                                                CustomerAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }

                                                    #region Adding Bill Address of Customer.
                                                    if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                        (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                                    {
                                                        XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                        CustomerAdd.AppendChild(BillAddress);
                                                        if (dt.Columns.Contains("BillAddr1"))
                                                        {

                                                            if (dr["BillAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                                BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                                BillAddress.AppendChild(BillAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr2"))
                                                        {
                                                            if (dr["BillAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                                BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                                BillAddress.AppendChild(BillAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr3"))
                                                        {
                                                            if (dr["BillAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                                BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                                BillAddress.AppendChild(BillAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr4"))
                                                        {
                                                            if (dr["BillAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                                BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                                BillAddress.AppendChild(BillAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillAddr5"))
                                                        {
                                                            if (dr["BillAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                                BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                                BillAddress.AppendChild(BillAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCity"))
                                                        {
                                                            if (dr["BillCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCity = xmldocadd.CreateElement("City");
                                                                BillCity.InnerText = dr["BillCity"].ToString();
                                                                BillAddress.AppendChild(BillCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillState"))
                                                        {
                                                            if (dr["BillState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillState = xmldocadd.CreateElement("State");
                                                                BillState.InnerText = dr["BillState"].ToString();
                                                                BillAddress.AppendChild(BillState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillPostalCode"))
                                                        {
                                                            if (dr["BillPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                                BillAddress.AppendChild(BillPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillCountry"))
                                                        {
                                                            if (dr["BillCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                                BillCountry.InnerText = dr["BillCountry"].ToString();
                                                                BillAddress.AppendChild(BillCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("BillNote"))
                                                        {
                                                            if (dr["BillNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                                BillNote.InnerText = dr["BillNote"].ToString();
                                                                BillAddress.AppendChild(BillNote);
                                                            }
                                                        }

                                                    }

                                                    #endregion

                                                    #region Adding Ship Address of Customer.
                                                    if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                                      (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                                    {
                                                        XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                        CustomerAdd.AppendChild(ShipAddress);
                                                        if (dt.Columns.Contains("ShipAddr1"))
                                                        {

                                                            if (dr["ShipAddr1"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                                ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd1);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr2"))
                                                        {
                                                            if (dr["ShipAddr2"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                                ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd2);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr3"))
                                                        {
                                                            if (dr["ShipAddr3"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                                ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd3);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr4"))
                                                        {
                                                            if (dr["ShipAddr4"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                                ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd4);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipAddr5"))
                                                        {
                                                            if (dr["ShipAddr5"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                                ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                                ShipAddress.AppendChild(ShipAdd5);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCity"))
                                                        {
                                                            if (dr["ShipCity"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                                ShipCity.InnerText = dr["ShipCity"].ToString();
                                                                ShipAddress.AppendChild(ShipCity);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipState"))
                                                        {
                                                            if (dr["ShipState"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipState = xmldocadd.CreateElement("State");
                                                                ShipState.InnerText = dr["ShipState"].ToString();
                                                                ShipAddress.AppendChild(ShipState);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipPostalCode"))
                                                        {
                                                            if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                                ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                                ShipAddress.AppendChild(ShipPostalCode);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipCountry"))
                                                        {
                                                            if (dr["ShipCountry"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                                ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                                ShipAddress.AppendChild(ShipCountry);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("ShipNote"))
                                                        {
                                                            if (dr["ShipNote"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                                ShipNote.InnerText = dr["ShipNote"].ToString();
                                                                ShipAddress.AppendChild(ShipNote);
                                                            }
                                                        }
                                                    }

                                                    #endregion
                                                    string custinput = xmldocadd.OuterXml;
                                                    string respcust = string.Empty;
                                                    try
                                                    {
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                        }

                                                        else
                                                            respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                                    }

                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    finally
                                                    {
                                                        if (respcust != string.Empty)
                                                        {
                                                            System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                            outputcustXMLDoc.LoadXml(respcust);
                                                            foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                            {
                                                                string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                                if (statusSeveritycust == "Error")
                                                                {
                                                                    string msg = "New Customer could not be created into QuickBooks \n ";
                                                                    msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                                    //MessageBox.Show(msg);
                                                                    //Task 1435 (Axis 6.0):
                                                                    ErrorSummary summary = new ErrorSummary(msg);
                                                                    summary.ShowDialog();
                                                                    CommonUtilities.WriteErrorLog(msg);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion

                                                }
                                            }

                                        }

                                    }
                                #endregion
                                }
                            }
                        }
                        #endregion

                        #region ItemCustomerFullName if not present
                        if (dt.Columns.Contains("ItemCustomerFullName"))
                        {
                            if (dr["ItemCustomerFullName"].ToString() != string.Empty)
                            {
                                #region Set Customer Query
                                XmlDocument pxmldoc = new XmlDocument();
                                pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                pxmldoc.AppendChild(qbXML);
                                XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                qbXML.AppendChild(qbXMLMsgsRq);
                                qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                XmlElement CustomerQueryRq = pxmldoc.CreateElement("CustomerQueryRq");
                                qbXMLMsgsRq.AppendChild(CustomerQueryRq);
                                CustomerQueryRq.SetAttribute("requestID", "1");
                                XmlElement FullName = pxmldoc.CreateElement("FullName");
                                FullName.InnerText = dr["ItemCustomerFullName"].ToString();
                                CustomerQueryRq.AppendChild(FullName);

                                string pinput = pxmldoc.OuterXml;

                                string resp = string.Empty;
                                try
                                {
                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                    {
                                        CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                    }

                                    else
                                        resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                }





                                catch (Exception ex)
                                {
                                    CommonUtilities.WriteErrorLog(ex.Message);
                                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                                }
                                finally
                                {
                                    if (resp != string.Empty)
                                    {

                                        System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                        outputXMLDoc.LoadXml(resp);
                                        string statusSeverity = string.Empty;
                                        foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerQueryRs"))
                                        {
                                            statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                    }
                                    outputXMLDoc.RemoveAll();
                                    if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                    {

                                            #region Customer Add Query

                                            XmlDocument xmldocadd = new XmlDocument();
                                            xmldocadd.AppendChild(xmldocadd.CreateXmlDeclaration("1.0", null, null));
                                            xmldocadd.AppendChild(xmldocadd.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                            XmlElement qbXMLcust = xmldocadd.CreateElement("QBXML");
                                            xmldocadd.AppendChild(qbXMLcust);
                                            XmlElement qbXMLMsgsRqcust = xmldocadd.CreateElement("QBXMLMsgsRq");
                                            qbXMLcust.AppendChild(qbXMLMsgsRqcust);
                                            qbXMLMsgsRqcust.SetAttribute("onError", "stopOnError");
                                            XmlElement CustomerAddRq = xmldocadd.CreateElement("CustomerAddRq");
                                            qbXMLMsgsRqcust.AppendChild(CustomerAddRq);
                                            CustomerAddRq.SetAttribute("requestID", "1");
                                            XmlElement CustomerAdd = xmldocadd.CreateElement("CustomerAdd");
                                            CustomerAddRq.AppendChild(CustomerAdd);
                                            XmlElement Name = xmldocadd.CreateElement("Name");
                                            Name.InnerText = dr["ItemCustomerFullName"].ToString();
                                            CustomerAdd.AppendChild(Name);


                                            #region Adding Bill Address of Customer.
                                            if ((dt.Columns.Contains("BillAddr1") || dt.Columns.Contains("BillAddr2")) || (dt.Columns.Contains("BillAddr3") || dt.Columns.Contains("BillAddr4")) || (dt.Columns.Contains("BillAddr5") || dt.Columns.Contains("BillCity")) ||
                                                (dt.Columns.Contains("BillState") || dt.Columns.Contains("BillPostalCode")) || (dt.Columns.Contains("BillCountry") || dt.Columns.Contains("BillNote")))
                                            {
                                                XmlElement BillAddress = xmldocadd.CreateElement("BillAddress");
                                                CustomerAdd.AppendChild(BillAddress);
                                                if (dt.Columns.Contains("BillAddr1"))
                                                {

                                                    if (dr["BillAddr1"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd1 = xmldocadd.CreateElement("Addr1");
                                                        BillAdd1.InnerText = dr["BillAddr1"].ToString();
                                                        BillAddress.AppendChild(BillAdd1);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr2"))
                                                {
                                                    if (dr["BillAddr2"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd2 = xmldocadd.CreateElement("Addr2");
                                                        BillAdd2.InnerText = dr["BillAddr2"].ToString();
                                                        BillAddress.AppendChild(BillAdd2);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr3"))
                                                {
                                                    if (dr["BillAddr3"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd3 = xmldocadd.CreateElement("Addr3");
                                                        BillAdd3.InnerText = dr["BillAddr3"].ToString();
                                                        BillAddress.AppendChild(BillAdd3);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr4"))
                                                {
                                                    if (dr["BillAddr4"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd4 = xmldocadd.CreateElement("Addr4");
                                                        BillAdd4.InnerText = dr["BillAddr4"].ToString();
                                                        BillAddress.AppendChild(BillAdd4);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillAddr5"))
                                                {
                                                    if (dr["BillAddr5"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillAdd5 = xmldocadd.CreateElement("Addr5");
                                                        BillAdd5.InnerText = dr["BillAddr5"].ToString();
                                                        BillAddress.AppendChild(BillAdd5);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillCity"))
                                                {
                                                    if (dr["BillCity"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillCity = xmldocadd.CreateElement("City");
                                                        BillCity.InnerText = dr["BillCity"].ToString();
                                                        BillAddress.AppendChild(BillCity);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillState"))
                                                {
                                                    if (dr["BillState"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillState = xmldocadd.CreateElement("State");
                                                        BillState.InnerText = dr["BillState"].ToString();
                                                        BillAddress.AppendChild(BillState);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillPostalCode"))
                                                {
                                                    if (dr["BillPostalCode"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillPostalCode = xmldocadd.CreateElement("PostalCode");
                                                        BillPostalCode.InnerText = dr["BillPostalCode"].ToString();
                                                        BillAddress.AppendChild(BillPostalCode);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillCountry"))
                                                {
                                                    if (dr["BillCountry"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillCountry = xmldocadd.CreateElement("Country");
                                                        BillCountry.InnerText = dr["BillCountry"].ToString();
                                                        BillAddress.AppendChild(BillCountry);
                                                    }
                                                }
                                                if (dt.Columns.Contains("BillNote"))
                                                {
                                                    if (dr["BillNote"].ToString() != string.Empty)
                                                    {
                                                        XmlElement BillNote = xmldocadd.CreateElement("Note");
                                                        BillNote.InnerText = dr["BillNote"].ToString();
                                                        BillAddress.AppendChild(BillNote);
                                                    }
                                                }


                                            }
                                            #endregion

                                            #region Adding Ship Address of Customer.
                                            if ((dt.Columns.Contains("ShipAddr1") || dt.Columns.Contains("ShipAddr2")) || (dt.Columns.Contains("ShipAddr3") || dt.Columns.Contains("ShipAddr4")) || (dt.Columns.Contains("ShipAddr5") || dt.Columns.Contains("ShipCity")) ||
                                              (dt.Columns.Contains("ShipState") || dt.Columns.Contains("ShipPostalCode")) || (dt.Columns.Contains("ShipCountry") || dt.Columns.Contains("ShipNote")))
                                            {
                                                XmlElement ShipAddress = xmldocadd.CreateElement("ShipAddress");
                                                CustomerAdd.AppendChild(ShipAddress);
                                                if (dt.Columns.Contains("ShipAddr1"))
                                                {

                                                    if (dr["ShipAddr1"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd1 = xmldocadd.CreateElement("Addr1");
                                                        ShipAdd1.InnerText = dr["ShipAddr1"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd1);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr2"))
                                                {
                                                    if (dr["ShipAddr2"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd2 = xmldocadd.CreateElement("Addr2");
                                                        ShipAdd2.InnerText = dr["ShipAddr2"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd2);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr3"))
                                                {
                                                    if (dr["ShipAddr3"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd3 = xmldocadd.CreateElement("Addr3");
                                                        ShipAdd3.InnerText = dr["ShipAddr3"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd3);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr4"))
                                                {
                                                    if (dr["ShipAddr4"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd4 = xmldocadd.CreateElement("Addr4");
                                                        ShipAdd4.InnerText = dr["ShipAddr4"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd4);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipAddr5"))
                                                {
                                                    if (dr["ShipAddr5"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipAdd5 = xmldocadd.CreateElement("Addr5");
                                                        ShipAdd5.InnerText = dr["ShipAddr5"].ToString();
                                                        ShipAddress.AppendChild(ShipAdd5);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipCity"))
                                                {
                                                    if (dr["ShipCity"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipCity = xmldocadd.CreateElement("City");
                                                        ShipCity.InnerText = dr["ShipCity"].ToString();
                                                        ShipAddress.AppendChild(ShipCity);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipState"))
                                                {
                                                    if (dr["ShipState"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipState = xmldocadd.CreateElement("State");
                                                        ShipState.InnerText = dr["ShipState"].ToString();
                                                        ShipAddress.AppendChild(ShipState);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipPostalCode"))
                                                {
                                                    if (dr["ShipPostalCode"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipPostalCode = xmldocadd.CreateElement("PostalCode");
                                                        ShipPostalCode.InnerText = dr["ShipPostalCode"].ToString();
                                                        ShipAddress.AppendChild(ShipPostalCode);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipCountry"))
                                                {
                                                    if (dr["ShipCountry"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipCountry = xmldocadd.CreateElement("Country");
                                                        ShipCountry.InnerText = dr["ShipCountry"].ToString();
                                                        ShipAddress.AppendChild(ShipCountry);
                                                    }
                                                }
                                                if (dt.Columns.Contains("ShipNote"))
                                                {
                                                    if (dr["ShipNote"].ToString() != string.Empty)
                                                    {
                                                        XmlElement ShipNote = xmldocadd.CreateElement("Note");
                                                        ShipNote.InnerText = dr["ShipNote"].ToString();
                                                        ShipAddress.AppendChild(ShipNote);
                                                    }
                                                }
                                            }

                                            #endregion
                                            string custinput = xmldocadd.OuterXml;
                                            string respcust = string.Empty;
                                            try
                                            {
                                                if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                {
                                                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                    respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, custinput);

                                                }

                                                else
                                                    respcust = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, custinput);
                                            }

                                            catch (Exception ex)
                                            {
                                                CommonUtilities.WriteErrorLog(ex.Message);
                                                CommonUtilities.WriteErrorLog(ex.StackTrace);
                                            }
                                            finally
                                            {
                                                if (respcust != string.Empty)
                                                {
                                                    System.Xml.XmlDocument outputcustXMLDoc = new System.Xml.XmlDocument();
                                                    outputcustXMLDoc.LoadXml(respcust);
                                                    foreach (System.Xml.XmlNode oNodecust in outputcustXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/CustomerAddRs"))
                                                    {
                                                        string statusSeveritycust = oNodecust.Attributes["statusSeverity"].Value.ToString();
                                                        if (statusSeveritycust == "Error")
                                                        {
                                                            string msg = "New Customer could not be created into QuickBooks \n ";
                                                            msg += oNodecust.Attributes["statusMessage"].Value.ToString() + "\n";
                                                            //MessageBox.Show(msg);
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(msg);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(msg);
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion

                                        }
                                    }

                                }

                                #endregion
                            }
                        }
                        #endregion

                        #region ItemFullName if not present Solution for BUG 633
                        if (dt.Columns.Contains("ItemFullName"))
                        {
                            if (dr["ItemFullName"].ToString() != string.Empty)
                            {
                                //Code to check whether Item Name conatins ":"
                                string ItemName = dr["ItemFullName"].ToString();
                                string[] arr = new string[15];
                                if (ItemName.Contains(":"))
                                {
                                    arr = ItemName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ItemFullName"].ToString();
                                }

                                #region Setting SalesTaxCode and IsTaxIncluded

                                if (defaultSettings == null)
                                {
                                    CommonUtilities.WriteErrorLog(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"));
                                    MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG098"), "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    return null;

                                }
                                //string IsTaxable = string.Empty;
                                string TaxRateValue = string.Empty;
                                string IsTaxIncluded = string.Empty;
                                string netCost = string.Empty;
                                string ItemSaleTaxFullName = string.Empty;
                                //if default settings contain checkBoxGrossToNet checked.
                                if (defaultSettings.GrossToNet == "1")
                                {
                                    if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                    {
                                        if (dt.Columns.Contains("ItemSalesTaxCodeFullName"))
                                        {
                                            if (dr["ItemSalesTaxCodeFullName"].ToString() != string.Empty)
                                            {
                                                string FullName = dr["ItemSalesTaxCodeFullName"].ToString();
                                                //IsTaxable = QBCommonUtilities.GetIsTaxableFromSalesTaxCode(QBFileName, FullName);

                                                ItemSaleTaxFullName = QBCommonUtilities.GetItemPurchaseTaxFullName(QBFileName, FullName);

                                                TaxRateValue = QBCommonUtilities.GetTaxRateFromSalesTaxCode(QBFileName, ItemSaleTaxFullName);
                                            }
                                        }
                                    }

                                    //validate IsTaxInluded value if present
                                    if (dt.Columns.Contains("IsTaxIncluded"))
                                    {
                                        if (dr["IsTaxIncluded"].ToString() != string.Empty && dr["IsTaxIncluded"].ToString() != "<None>")
                                        {
                                            int result = 0;
                                            if (int.TryParse(dr["IsTaxIncluded"].ToString(), out result))
                                            {
                                                IsTaxIncluded = Convert.ToInt32(dr["IsTaxIncluded"].ToString()) > 0 ? "true" : "false";
                                            }
                                            else
                                            {
                                                string strvalid = string.Empty;
                                                if (dr["IsTaxIncluded"].ToString().ToLower() == "true")
                                                {
                                                    IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                                else
                                                {
                                                    IsTaxIncluded = dr["IsTaxIncluded"].ToString().ToLower();
                                                }
                                            }

                                        }
                                    }

                                    //Calculate cost
                                    if (dt.Columns.Contains("Cost"))
                                    {
                                        if (dr["Cost"].ToString() != string.Empty)
                                        {
                                            decimal cost = 0;
                                            if (TaxRateValue != string.Empty && IsTaxIncluded != string.Empty)
                                            {
                                                if (IsTaxIncluded == "true" || IsTaxIncluded == "1")
                                                {
                                                    decimal Cost;
                                                    if (!decimal.TryParse(dr["Cost"].ToString(), out cost))
                                                    {
                                                        //Rate = 0;
                                                        netCost = dr["Cost"].ToString();
                                                    }
                                                    else
                                                    {
                                                        Cost = Convert.ToDecimal(dr["Cost"].ToString());
                                                        if (CommonUtilities.GetInstance().CountryVersion == "AU" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "UK" ||
                                                            CommonUtilities.GetInstance().CountryVersion == "CA")
                                                        {
                                                            //decimal TaxRate = 10;
                                                            decimal TaxRate = Convert.ToDecimal(TaxRateValue);
                                                            cost = Cost / (1 + (TaxRate / 100));
                                                        }

                                                        netCost = Convert.ToString(Math.Round(cost, 5));
                                                    }
                                                }
                                            }

                                            if (netCost == string.Empty)
                                            {
                                                netCost = dr["Cost"].ToString();
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (dt.Columns.Contains("Cost"))
                                    {
                                        if (dr["Cost"].ToString() != string.Empty)
                                        {
                                            netCost = dr["Cost"].ToString();
                                        }
                                    }
                                }

                                #endregion

                                #region Set Item Query

                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        #region Passing Items Query
                                        XmlDocument pxmldoc = new XmlDocument();
                                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                                        pxmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                                        qbXMLMsgsRq.AppendChild(ItemQueryRq);
                                        ItemQueryRq.SetAttribute("requestID", "1");


                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = pxmldoc.CreateElement("FullName");


                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    ItemQueryRq.AppendChild(FullName);
                                                }

                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            //FullName.InnerText = dr["ItemFullName"].ToString();
                                            ItemQueryRq.AppendChild(FullName);
                                        }

                                        string pinput = pxmldoc.OuterXml;

                                        string resp = string.Empty;
                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, pinput);
                                        }

                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {

                                            if (resp != string.Empty)
                                            {
                                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                                outputXMLDoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();

                                            }
                                            outputXMLDoc.RemoveAll();
                                            if (statusSeverity == "Error" || statusSeverity == "Warn" || statusSeverity == "Warning")
                                            {

                                                    if (defaultSettings.Type == "NonInventoryPart")
                                                    {
                                                        #region Item NonInventory Add Query

                                                        XmlDocument ItemNonInvendoc = new XmlDocument();
                                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                                        XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                                        ItemNonInvendoc.AppendChild(qbXMLINI);
                                                        XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                                        qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                                        qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                                        XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                                        qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                                        ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                                        XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                                        ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                                        XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                                        ININame.InnerText = arr[i];
                                                        //ININame.InnerText = dr["ItemFullName"].ToString();
                                                        ItemNonInventoryAdd.AppendChild(ININame);

                                                        //Solution for BUG 633
                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    //Adding Parent
                                                                    XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                                    ItemNonInventoryAdd.AppendChild(INIParent);

                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }

                                                        //Adding Tax Code Element.
                                                        if (defaultSettings.TaxCode != string.Empty)
                                                        {
                                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                            {
                                                                if (defaultSettings.TaxCode.Length < 4)
                                                                {
                                                                    XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                                    ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                                    XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                                    INIFullName.InnerText = defaultSettings.TaxCode;
                                                                    INISalesTaxCodeRef.AppendChild(INIFullName);
                                                                }
                                                            }
                                                        }

                                                        XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                                        bool IsPresent = false;
                                                        //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);

                                                        //Adding Desc and Rate
                                                        //Solution for BUG 631 nad 632
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemNonInvendoc.CreateElement("Desc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                INISalesAndPurchase.AppendChild(ISDesc);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Cost"))
                                                        {
                                                            if (dr["Cost"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISCost = ItemNonInvendoc.CreateElement("Price");
                                                                ISCost.InnerText = netCost;
                                                                INISalesAndPurchase.AppendChild(ISCost);
                                                                IsPresent = true;
                                                            }
                                                        }

                                                        if (defaultSettings.IncomeAccount != string.Empty)
                                                        {
                                                            XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                                            INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                                            XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                                            //INIFullName.InnerText = "Sales";
                                                            INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                                            INIIncomeAccountRef.AppendChild(INIAccountRefFullName);

                                                            IsPresent = true;
                                                        }

                                                        if (IsPresent == true)
                                                        {
                                                            ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                                        }

                                                        string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                                    //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                                    string respItemNonInvendoc = string.Empty;
                                                    try
                                                    {
                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                                        }
                                                        else
                                                        {
                                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemNonInvendocinput);
                                                        }
                                                        //End Changes
                                                        
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemNonInvendoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strtest2 = respItemNonInvendoc;

                                                        #endregion
                                                    }
                                                    else if (defaultSettings.Type == "Service")
                                                    {
                                                        #region Item Service Add Query

                                                        XmlDocument ItemServiceAdddoc = new XmlDocument();
                                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                        //ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));
                                                        XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                                        ItemServiceAdddoc.AppendChild(qbXMLIS);
                                                        XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                        XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                                        qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                                        ItemServiceAddRq.SetAttribute("requestID", "1");

                                                        XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                                        ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                                        XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                                        NameIS.InnerText = arr[i];
                                                        //NameIS.InnerText = dr["ItemFullName"].ToString();
                                                        ItemServiceAdd.AppendChild(NameIS);

                                                        //Solution for BUG 633
                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    //Adding Parent
                                                                    XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                                    ItemServiceAdd.AppendChild(INIParent);

                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }


                                                        //Adding Tax code Element.
                                                        if (defaultSettings.TaxCode != string.Empty)
                                                        {
                                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                            {
                                                                if (defaultSettings.TaxCode.Length < 4)
                                                                {
                                                                    XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                                    ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                                    XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                                    INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                                    INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                                                }
                                                            }
                                                        }


                                                        XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                                        bool IsPresent = false;
                                                        //ItemServiceAdd.AppendChild(ISSalesAndPurchase);

                                                        //Adding Desc and Rate
                                                        //Solution for BUG 631 nad 632
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemServiceAdddoc.CreateElement("Desc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                ISSalesAndPurchase.AppendChild(ISDesc);
                                                                IsPresent = true;
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Cost"))
                                                        {
                                                            if (dr["Cost"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISCost = ItemServiceAdddoc.CreateElement("Price");
                                                                ISCost.InnerText = netCost;
                                                                ISSalesAndPurchase.AppendChild(ISCost);
                                                                IsPresent = true;
                                                            }
                                                        }

                                                        if (defaultSettings.IncomeAccount != string.Empty)
                                                        {
                                                            XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                                            ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                                            //Adding IncomeAccount FullName.
                                                            XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                                            ISIncomeAccountRef.AppendChild(ISFullName);

                                                            IsPresent = true;
                                                        }

                                                        if (IsPresent == true)
                                                        {
                                                            ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                                        }

                                                    string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                                    //ItemServiceAdddoc.Save("C://ItemServiceAdddoc.xml");
                                                    string respItemServiceAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemServiceAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemServiceAddinput);
                                                        }
                                                        //End Changes

                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemServiceAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest3 = respItemServiceAddinputdoc;
                                                    #endregion
                                                }
                                                else if (defaultSettings.Type == "InventoryPart")
                                                {
                                                    #region Inventory Add Query
                                                    XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                                    ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                                    ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                                    XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                                    qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                                    qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                                    XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                                    qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                                    ItemInventoryAddRq.SetAttribute("requestID", "1");

                                                        XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                                        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                                        XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                                        NameIS.InnerText = arr[i];
                                                        //NameIS.InnerText = dr["ItemFullName"].ToString();
                                                        ItemInventoryAdd.AppendChild(NameIS);

                                                        //Solution for BUG 633
                                                        if (i > 0)
                                                        {
                                                            if (arr[i] != null && arr[i] != string.Empty)
                                                            {
                                                                XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                                for (a = 0; a <= i - 1; a++)
                                                                {
                                                                    if (arr[a].Trim() != string.Empty)
                                                                    {
                                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                    }
                                                                }
                                                                if (INIChildFullName.InnerText != string.Empty)
                                                                {
                                                                    //Adding Parent
                                                                    XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                                    ItemInventoryAdd.AppendChild(INIParent);

                                                                    INIParent.AppendChild(INIChildFullName);
                                                                }
                                                            }
                                                        }


                                                        if (defaultSettings.TaxCode != string.Empty)
                                                        {
                                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                            {
                                                                if (defaultSettings.TaxCode.Length < 4)
                                                                {
                                                                    //Adding Tax code Element.
                                                                    XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                                    ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                                    XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                                    INIFullName.InnerText = defaultSettings.TaxCode;
                                                                    INISalesTaxCodeRef.AppendChild(INIFullName);
                                                                }
                                                            }
                                                        }
                                                        //Adding Desc and Rate
                                                        //Solution for BUG 631 nad 632
                                                        if (dt.Columns.Contains("Description"))
                                                        {
                                                            if (dr["Description"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISDesc = ItemInventoryAdddoc.CreateElement("SalesDesc");
                                                                ISDesc.InnerText = dr["Description"].ToString();
                                                                ItemInventoryAdd.AppendChild(ISDesc);
                                                            }
                                                        }
                                                        if (dt.Columns.Contains("Cost"))
                                                        {
                                                            if (dr["Cost"].ToString() != string.Empty)
                                                            {
                                                                XmlElement ISCost = ItemInventoryAdddoc.CreateElement("SalesPrice");
                                                                ISCost.InnerText = netCost;
                                                                ItemInventoryAdd.AppendChild(ISCost);
                                                            }
                                                        }

                                                        //Adding IncomeAccountRef
                                                        if (defaultSettings.IncomeAccount != string.Empty)
                                                        {
                                                            XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                                            ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                                            XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                                            INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                                        }

                                                        //Adding COGSAccountRef
                                                        if (defaultSettings.COGSAccount != string.Empty)
                                                        {
                                                            XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                                            ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                                            XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                                            INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                                        }

                                                        //Adding AssetAccountRef
                                                        if (defaultSettings.AssetAccount != string.Empty)
                                                        {
                                                            XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                                            ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                                            XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                            INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                                            INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                                        }

                                                    string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                                    string respItemInventoryAddinputdoc = string.Empty;
                                                    try
                                                    {
                                                        //Axis 10.2(bug no 66)
                                                        if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                                        }
                                                        else
                                                        {
                                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, ItemInventoryAddinput);
                                                        }
                                                        //End Changes
                                                       
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respItemInventoryAddinputdoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn" || StatusSeverity == "Warning")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }
                                                    string strTest4 = respItemInventoryAddinputdoc;
                                                    #endregion
                                                }
                                            }
                                        }

                                        }

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }
                        #endregion

                        #region Add accounts if not present

                        #region  axis 10 Adding CreditCard Account name in QuickBooks


                        if (dt.Columns.Contains("AccountFullName"))
                        {
                            if (dr["AccountFullName"].ToString() != string.Empty)
                            {

                                string AccountName = dr["AccountFullName"].ToString();
                                string[] arr = new string[15];
                                if (AccountName.Contains(":"))
                                {
                                    arr = AccountName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["AccountFullName"].ToString();
                                }

                                #region Set CreditCard Account Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument xmldoc = new XmlDocument();
                                        xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
                                        xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = xmldoc.CreateElement("QBXML");
                                        xmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                        XmlElement AccountQueryRq = xmldoc.CreateElement("AccountQueryRq");
                                        qbXMLMsgsRq.AppendChild(AccountQueryRq);
                                        AccountQueryRq.SetAttribute("requestID", "1");


                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = xmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    AccountQueryRq.AppendChild(FullName);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = xmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            AccountQueryRq.AppendChild(FullName);
                                        }


                                        string XMLinput = xmldoc.OuterXml;
                                        string resp = string.Empty;

                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, XMLinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, XMLinput);
                                        }

                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {
                                                XmlDocument outputXMLdoc = new XmlDocument();
                                                outputXMLdoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (XmlNode Onode in outputXMLdoc.SelectNodes("/QBXML/QBXMLMsgsRs/AccountQueryRs"))
                                                {
                                                    statusSeverity = Onode.Attributes["statusSeverity"].Value.ToString();
                                                }

                                                outputXMLdoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                {
                                                    #region Credit Card Account Add Query

                                                    XmlDocument ExpAccountdoc = new XmlDocument();
                                                    ExpAccountdoc.AppendChild(ExpAccountdoc.CreateXmlDeclaration("1.0", null, null));
                                                    ExpAccountdoc.AppendChild(ExpAccountdoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement ExpAccountqbXML = ExpAccountdoc.CreateElement("QBXML");
                                                    ExpAccountdoc.AppendChild(ExpAccountqbXML);
                                                    XmlElement ExpAccountqbXMLMsgsRq = ExpAccountdoc.CreateElement("QBXMLMsgsRq");
                                                    ExpAccountqbXML.AppendChild(ExpAccountqbXMLMsgsRq);
                                                    ExpAccountqbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                                    XmlElement ExpAccountAddRq = ExpAccountdoc.CreateElement("AccountAddRq");
                                                    ExpAccountqbXMLMsgsRq.AppendChild(ExpAccountAddRq);
                                                    ExpAccountAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ExpAccountAdd = ExpAccountdoc.CreateElement("AccountAdd");
                                                    ExpAccountAddRq.AppendChild(ExpAccountAdd);

                                                    XmlElement VName = ExpAccountdoc.CreateElement("Name");
                                                    VName.InnerText = arr[i];
                                                    ExpAccountAdd.AppendChild(VName);

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ExpAccountdoc.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = ExpAccountdoc.CreateElement("ParentRef");
                                                               // ExpAccountdoc.AppendChild(INIParent);
                                                                ExpAccountAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }

                                                

                                                    XmlElement ExpaccountType = ExpAccountdoc.CreateElement("AccountType");
                                                    ExpaccountType.InnerText = "CreditCard";
                                                    ExpAccountAdd.AppendChild(ExpaccountType);

                                                    string ExpAccountDocInput = ExpAccountdoc.OuterXml;
                                                    string respExpAccountDoc = string.Empty;

                                                    try
                                                    {
                                                        respExpAccountDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ExpAccountDocInput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respExpAccountDoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/AccountAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }

                                                    #endregion

                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Adding ExpenseAccount (of type Expense) name in QuickBooks


                        if (dt.Columns.Contains("ExpenseAccountFullName"))
                        {
                            if (dr["ExpenseAccountFullName"].ToString() != string.Empty)
                            {

                                string AccountName = dr["ExpenseAccountFullName"].ToString();
                                string[] arr = new string[15];
                                if (AccountName.Contains(":"))
                                {
                                    arr = AccountName.Split(':');
                                }
                                else
                                {
                                    arr[0] = dr["ExpenseAccountFullName"].ToString();
                                }

                                #region Set Expense Account Query
                                for (int i = 0; i < arr.Length; i++)
                                {
                                    int a = 0;
                                    int item = 0;
                                    if (arr[i] != null && arr[i] != string.Empty)
                                    {
                                        XmlDocument xmldoc = new XmlDocument();
                                        xmldoc.AppendChild(xmldoc.CreateXmlDeclaration("1.0", null, null));
                                        xmldoc.AppendChild(xmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXML = xmldoc.CreateElement("QBXML");
                                        xmldoc.AppendChild(qbXML);
                                        XmlElement qbXMLMsgsRq = xmldoc.CreateElement("QBXMLMsgsRq");
                                        qbXML.AppendChild(qbXMLMsgsRq);
                                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                        XmlElement AccountQueryRq = xmldoc.CreateElement("AccountQueryRq");
                                        qbXMLMsgsRq.AppendChild(AccountQueryRq);
                                        AccountQueryRq.SetAttribute("requestID", "1");


                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement FullName = xmldoc.CreateElement("FullName");
                                                for (item = 0; item <= i; item++)
                                                {
                                                    if (arr[item].Trim() != string.Empty)
                                                    {
                                                        FullName.InnerText += arr[item].Trim() + ":";
                                                    }
                                                }
                                                if (FullName.InnerText != string.Empty)
                                                {
                                                    AccountQueryRq.AppendChild(FullName);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            XmlElement FullName = xmldoc.CreateElement("FullName");
                                            FullName.InnerText = arr[i];
                                            AccountQueryRq.AppendChild(FullName);
                                        }


                                        string XMLinput = xmldoc.OuterXml;
                                        string resp = string.Empty;

                                        try
                                        {
                                            if (TransactionImporter.TransactionImporter.rdbdesktopbutton == true)
                                            {
                                                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, XMLinput);

                                            }

                                            else
                                                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, XMLinput);
                                        }

                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                        finally
                                        {
                                            if (resp != string.Empty)
                                            {
                                                XmlDocument outputXMLdoc = new XmlDocument();
                                                outputXMLdoc.LoadXml(resp);
                                                string statusSeverity = string.Empty;
                                                foreach (XmlNode Onode in outputXMLdoc.SelectNodes("/QBXML/QBXMLMsgsRs/AccountQueryRs"))
                                                {
                                                    statusSeverity = Onode.Attributes["statusSeverity"].Value.ToString();
                                                }

                                                outputXMLdoc.RemoveAll();
                                                if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                {
                                                    #region Expense Account Add Query

                                                    XmlDocument ExpAccountdoc = new XmlDocument();
                                                    ExpAccountdoc.AppendChild(ExpAccountdoc.CreateXmlDeclaration("1.0", null, null));
                                                    ExpAccountdoc.AppendChild(ExpAccountdoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                                    XmlElement ExpAccountqbXML = ExpAccountdoc.CreateElement("QBXML");
                                                    ExpAccountdoc.AppendChild(ExpAccountqbXML);
                                                    XmlElement ExpAccountqbXMLMsgsRq = ExpAccountdoc.CreateElement("QBXMLMsgsRq");
                                                    ExpAccountqbXML.AppendChild(ExpAccountqbXMLMsgsRq);
                                                    ExpAccountqbXMLMsgsRq.SetAttribute("onError", "stopOnError");

                                                    XmlElement ExpAccountAddRq = ExpAccountdoc.CreateElement("AccountAddRq");
                                                    ExpAccountqbXMLMsgsRq.AppendChild(ExpAccountAddRq);
                                                    ExpAccountAddRq.SetAttribute("requestID", "1");

                                                    XmlElement ExpAccountAdd = ExpAccountdoc.CreateElement("AccountAdd");
                                                    ExpAccountAddRq.AppendChild(ExpAccountAdd);

                                                    XmlElement VName = ExpAccountdoc.CreateElement("Name");
                                                    VName.InnerText = arr[i];
                                                    ExpAccountAdd.AppendChild(VName);

                                                    if (i > 0)
                                                    {
                                                        if (arr[i] != null && arr[i] != string.Empty)
                                                        {
                                                            XmlElement INIChildFullName = ExpAccountdoc.CreateElement("FullName");
                                                            for (a = 0; a <= i - 1; a++)
                                                            {
                                                                if (arr[a].Trim() != string.Empty)
                                                                {
                                                                    INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                                }
                                                            }
                                                            if (INIChildFullName.InnerText != string.Empty)
                                                            {
                                                                XmlElement INIParent = ExpAccountdoc.CreateElement("ParentRef");                                                            
                                                                ExpAccountAdd.AppendChild(INIParent);
                                                                INIParent.AppendChild(INIChildFullName);
                                                            }
                                                        }
                                                    }



                                                    XmlElement ExpaccountType = ExpAccountdoc.CreateElement("AccountType");
                                                    ExpaccountType.InnerText = "Expense";
                                                    ExpAccountAdd.AppendChild(ExpaccountType);

                                                    string ExpAccountDocInput = ExpAccountdoc.OuterXml;
                                                    string respExpAccountDoc = string.Empty;

                                                    try
                                                    {
                                                        respExpAccountDoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ExpAccountDocInput);
                                                        System.Xml.XmlDocument outputXML = new System.Xml.XmlDocument();
                                                        outputXML.LoadXml(respExpAccountDoc);
                                                        string StatusSeverity = string.Empty;
                                                        string statusMessage = string.Empty;
                                                        foreach (System.Xml.XmlNode oNode in outputXML.SelectNodes("/QBXML/QBXMLMsgsRs/AccountAddRs"))
                                                        {
                                                            StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                            statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                        }
                                                        outputXML.RemoveAll();
                                                        if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                                        {
                                                            //Task 1435 (Axis 6.0):
                                                            ErrorSummary summary = new ErrorSummary(statusMessage);
                                                            summary.ShowDialog();
                                                            CommonUtilities.WriteErrorLog(statusMessage);
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        CommonUtilities.WriteErrorLog(ex.Message);
                                                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                                                    }

                                                    #endregion

                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #endregion
                    }
                }
                else
                {
                    return null;
                }
            }
            #endregion
                
            return coll;
        }
    }
}
