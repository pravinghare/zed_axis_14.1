﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Online_BaseStreams
{
  public class Online_BaseSalesReceiptList
    {
        protected string m_Id;
        protected string m_SyncToken;
        protected string m_CreateTime;
        protected string m_LastUpdatedTime;

        protected string m_LineId;
        protected string m_LineNumber;
        protected string m_LineDescription;
        protected string m_LineAmount;
        protected string m_LineDetailType;
        protected string m_LineSalesItemName;
        protected string m_LineSalesItemUnitPrice;
        protected string m_LineSalesItemQty;
        protected string m_LineSalesItemTaxCodeRef;

        protected string m_TotalTax;
        protected string m_CustomerRefName;
        protected string m_BillId;
        protected string m_BillLine1;
        protected string m_BillLine2;

        protected string m_BillCity;
        protected string m_BillCountry;
        protected string m_BillSubDivisionCode;
        protected string m_BillPostalCode;

        protected string m_TxnDate;
        protected string m_AutoDocNumber;
        protected string m_TotalAmt;
        protected string m_PrintStatus;
        protected string m_EmailStatus;
        protected string m_Balance;
        protected string m_ApplyTaxAfterDiscount;
        protected string m_DepositToAccountName;  
    }
}
