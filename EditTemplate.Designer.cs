namespace DataProcessingBlocks
{
    partial class EditTemplate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTemplate));
            this.contextMenuStripDeleteRows = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteRowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBoxTemplate = new System.Windows.Forms.ComboBox();
            this.labelTemplate = new System.Windows.Forms.Label();
            this.textBoxTemplate = new System.Windows.Forms.TextBox();
            this.panelEditTemp = new System.Windows.Forms.Panel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.radGridviewforEditTemplate = new Telerik.WinControls.UI.RadGridView();
            this.contextMenuStripDeleteRows.SuspendLayout();
            this.panelEditTemp.SuspendLayout();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridviewforEditTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridviewforEditTemplate.MasterTemplate)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStripDeleteRows
            // 
            this.contextMenuStripDeleteRows.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRowToolStripMenuItem});
            this.contextMenuStripDeleteRows.Name = "contextMenuStripDeleteRows";
            this.contextMenuStripDeleteRows.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStripDeleteRows.Size = new System.Drawing.Size(139, 26);
            this.contextMenuStripDeleteRows.Text = "Delete Rows";
            // 
            // deleteRowToolStripMenuItem
            // 
            this.deleteRowToolStripMenuItem.Name = "deleteRowToolStripMenuItem";
            this.deleteRowToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.deleteRowToolStripMenuItem.Text = "Delete Rows";
            this.deleteRowToolStripMenuItem.Click += new System.EventHandler(this.deleteRowsToolStripMenuItem_Click);
            // 
            // comboBoxTemplate
            // 
            this.comboBoxTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTemplate.Enabled = false;
            this.comboBoxTemplate.FormattingEnabled = true;
            this.comboBoxTemplate.Location = new System.Drawing.Point(78, 6);
            this.comboBoxTemplate.Name = "comboBoxTemplate";
            this.comboBoxTemplate.Size = new System.Drawing.Size(232, 21);
            this.comboBoxTemplate.TabIndex = 3;
            // 
            // labelTemplate
            // 
            this.labelTemplate.AutoSize = true;
            this.labelTemplate.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTemplate.Location = new System.Drawing.Point(12, 9);
            this.labelTemplate.Name = "labelTemplate";
            this.labelTemplate.Size = new System.Drawing.Size(60, 13);
            this.labelTemplate.TabIndex = 4;
            this.labelTemplate.Text = "Template";
            // 
            // textBoxTemplate
            // 
            this.textBoxTemplate.Location = new System.Drawing.Point(78, 7);
            this.textBoxTemplate.Name = "textBoxTemplate";
            this.textBoxTemplate.Size = new System.Drawing.Size(176, 20);
            this.textBoxTemplate.TabIndex = 5;
            // 
            // panelEditTemp
            // 
            this.panelEditTemp.Controls.Add(this.buttonCancel);
            this.panelEditTemp.Controls.Add(this.comboBoxTemplate);
            this.panelEditTemp.Controls.Add(this.labelTemplate);
            this.panelEditTemp.Controls.Add(this.buttonSave);
            this.panelEditTemp.Controls.Add(this.buttonNew);
            this.panelEditTemp.Controls.Add(this.textBoxTemplate);
            this.panelEditTemp.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEditTemp.Location = new System.Drawing.Point(0, 0);
            this.panelEditTemp.Name = "panelEditTemp";
            this.panelEditTemp.Size = new System.Drawing.Size(855, 33);
            this.panelEditTemp.TabIndex = 6;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(763, 5);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 15;
            this.buttonCancel.Text = "OK";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(670, 5);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 14;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Visible = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(575, 5);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(75, 23);
            this.buttonNew.TabIndex = 13;
            this.buttonNew.Text = "New";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Visible = false;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // panelGrid
            // 
            this.panelGrid.Controls.Add(this.radGridviewforEditTemplate);
            this.panelGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelGrid.Location = new System.Drawing.Point(0, 33);
            this.panelGrid.Name = "panelGrid";
            this.panelGrid.Size = new System.Drawing.Size(855, 518);
            this.panelGrid.TabIndex = 7;
            // 
            // radGridviewforEditTemplate
            // 
            this.radGridviewforEditTemplate.AutoScroll = true;
            this.radGridviewforEditTemplate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridviewforEditTemplate.EnableKineticScrolling = true;
            this.radGridviewforEditTemplate.Location = new System.Drawing.Point(0, 0);
            // 
            // radGridviewforEditTemplate
            // 
            this.radGridviewforEditTemplate.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.radGridviewforEditTemplate.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.radGridviewforEditTemplate.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridviewforEditTemplate.Name = "radGridviewforEditTemplate";
            this.radGridviewforEditTemplate.Size = new System.Drawing.Size(855, 518);
            this.radGridviewforEditTemplate.TabIndex = 0;
            this.radGridviewforEditTemplate.Text = "radGridView1";
            this.radGridviewforEditTemplate.CreateRow += new Telerik.WinControls.UI.GridViewCreateRowEventHandler(this.radGridviewforEditTemplate_CreateRow);
            // 
            // EditTemplate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(855, 551);
            this.Controls.Add(this.panelGrid);
            this.Controls.Add(this.panelEditTemp);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "EditTemplate";
            this.Text = "Edit Coding Template";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditTemplate_FormClosed);
            this.Load += new System.EventHandler(this.EditTemplate_Load);
            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.EditTemplate_ControlRemoved);
            this.contextMenuStripDeleteRows.ResumeLayout(false);
            this.panelEditTemp.ResumeLayout(false);
            this.panelEditTemp.PerformLayout();
            this.panelGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridviewforEditTemplate.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridviewforEditTemplate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelTemplate;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripDeleteRows;
        private System.Windows.Forms.ToolStripMenuItem deleteRowToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxTemplate;
        public System.Windows.Forms.ComboBox comboBoxTemplate;
        private System.Windows.Forms.Panel panelEditTemp;
        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonNew;
        public Telerik.WinControls.UI.RadGridView radGridviewforEditTemplate;
    }
}