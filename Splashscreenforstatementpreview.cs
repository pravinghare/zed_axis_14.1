﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using DataProcessingBlocks;
namespace TransactionImporter
{
    public partial class Splashscreenforstatementpreview : Form
    {

        #region Static Members
        public static Splashscreenforstatementpreview m_frmSplash = null;
        static Thread m_Thread = null;
        //static Button m_Cancel = null;
        static string m_Status = "";
        #endregion


        public string MessageToShow
        {
            set
            {
                this.labelMeessage.Text = value;
            }
            get
            {
                return this.labelMeessage.Text;
            }
        }

        public int PercentageComplete
        {
            set
            {
                this.progressBarStatusBar.Value = value;
            }
        }

        public bool IsEndLessLoop
        {
            set
            {
                if (value)
                {
                    this.timerStatus.Start();
                }
                else
                {
                    this.timerStatus.Stop();
                }
            }
        }

        public bool CancelSplash
        {
            set
            {
                this.buttonCancelSplashScreen.Visible = value;
            }
        }

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Splashscreenforstatementpreview()
        {
            InitializeComponent();
            this.timerStatus.Interval = 50;
            Cursor.Current = Cursors.WaitCursor;

            //this.timerStatus.Start();
        }

        #endregion

        #region Static Methods
        /// <summary>
        /// Shows splash screen
        /// </summary>
        static public void ShowSplashScreen1()
        {
            // Make sure it's only launched once.
            if (m_frmSplash != null)
                return;
            m_Thread = new Thread(new ThreadStart(Splashscreenforstatementpreview.ShowForm));
            m_Thread.IsBackground = true;
            m_Thread.SetApartmentState(ApartmentState.STA);
            m_Thread.Start();
        }

        /// <summary>
        /// Sets the message for Splash screen.
        /// </summary>
        /// <param name="statusMessage"></param>
        static public void SetSatusString(string statusMessage)
        {
            m_Status = statusMessage;
        }

        /// <summary>
        /// A private entry point for the thread.
        /// </summary>
        static private void ShowForm()
        {
            m_frmSplash = new Splashscreenforstatementpreview();
            Application.Run(m_frmSplash);
        }

        /// <summary>
        /// Close the Splash screen
        /// </summary>
        static public void CloseForm()
        {
            if (m_frmSplash != null)
            {
#if DEBUG
            
#else
                {
                    m_frmSplash.Close();
                }
#endif
                if (m_frmSplash != null && m_frmSplash.IsDisposed == false)
                {
                    // Make it start going away.
                }
                m_Thread = null;	// we don't need these any more.
                m_frmSplash = null;
            }
            else
            {
                m_frmSplash = new Splashscreenforstatementpreview();
                m_frmSplash.Close();
                m_Thread = null;
                m_frmSplash = null;

            }

        }
        #endregion


     


        private void buttonCancelSplashScreen_Click_1(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            TransactionImporter axisForm = (TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;
            if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
            {
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                bkWorker.CancelAsync();
                DataProcessingBlocks.CommonUtilities.CloseSplash();
            }
            if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                bkWorker.CancelAsync();
            }
            if (axisForm.backgroundWorkerProcessLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseSplash();
            }
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            if (this.progressBarStatusBar.Value == this.progressBarStatusBar.Maximum)
            {
                this.progressBarStatusBar.Value = 0;
            }
            this.progressBarStatusBar.Value += 1;
            if (this.progressBarStatusBar.Value == 100)
            {

                // CloseForm();
                CommonUtilities.screenpreviewProcessing.Close();

            }
        }

        private void Splashscreenforstatementpreview_Load(object sender, EventArgs e)
        {

        }

        private void labelMeessage_Click(object sender, EventArgs e)
        {

        }

    }
}