﻿using System.ComponentModel;
using Telerik.WinControls.UI;

namespace DataProcessingBlocks
{
    partial class ReplaceFunction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplaceFunction));
            this.labelType = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.chkExactMatch = new System.Windows.Forms.CheckBox();
            this.labelFunctionName = new System.Windows.Forms.Label();
            this.comboBoxFunctionName = new System.Windows.Forms.ComboBox();
            this.textboxFunctionName = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridViewFunction = new Telerik.WinControls.UI.RadGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCancel = new Telerik.WinControls.UI.RadButton();
            this.buttonOk = new Telerik.WinControls.UI.RadButton();
            this.buttonSaveAsXml = new Telerik.WinControls.UI.RadButton();
            this.saveFileDialogExportFunction = new System.Windows.Forms.SaveFileDialog();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFunction)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFunction.MasterTemplate)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSaveAsXml)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // labelType
            // 
            this.labelType.AutoSize = true;
            this.labelType.Location = new System.Drawing.Point(7, 16);
            this.labelType.Name = "labelType";
            this.labelType.Size = new System.Drawing.Size(29, 13);
            this.labelType.TabIndex = 15;
            this.labelType.Text = "Type";
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Items.AddRange(new object[] {
            "Search and Replace",
            "Concatenate Function"});
            this.comboBoxType.Location = new System.Drawing.Point(59, 12);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(178, 21);
            this.comboBoxType.TabIndex = 14;
            this.comboBoxType.SelectedIndexChanged += new System.EventHandler(this.comboBoxType_SelectedIndexChanged);
            // 
            // chkExactMatch
            // 
            this.chkExactMatch.AutoSize = true;
            this.chkExactMatch.Location = new System.Drawing.Point(270, 16);
            this.chkExactMatch.Name = "chkExactMatch";
            this.chkExactMatch.Size = new System.Drawing.Size(98, 17);
            this.chkExactMatch.TabIndex = 13;
            this.chkExactMatch.Text = "Exact Matches";
            this.chkExactMatch.UseVisualStyleBackColor = true;
            this.chkExactMatch.CheckedChanged += new System.EventHandler(this.chkExactMatch_CheckedChanged);
            // 
            // labelFunctionName
            // 
            this.labelFunctionName.AutoSize = true;
            this.labelFunctionName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFunctionName.Location = new System.Drawing.Point(4, 57);
            this.labelFunctionName.Name = "labelFunctionName";
            this.labelFunctionName.Size = new System.Drawing.Size(49, 13);
            this.labelFunctionName.TabIndex = 10;
            this.labelFunctionName.Text = "Name :";
            // 
            // comboBoxFunctionName
            // 
            this.comboBoxFunctionName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFunctionName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxFunctionName.FormattingEnabled = true;
            this.comboBoxFunctionName.Location = new System.Drawing.Point(59, 54);
            this.comboBoxFunctionName.Name = "comboBoxFunctionName";
            this.comboBoxFunctionName.Size = new System.Drawing.Size(178, 21);
            this.comboBoxFunctionName.TabIndex = 12;
            this.comboBoxFunctionName.SelectedIndexChanged += new System.EventHandler(this.comboBoxFunctionName_SelectedIndexChanged);
            // 
            // textboxFunctionName
            // 
            this.textboxFunctionName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxFunctionName.Location = new System.Drawing.Point(59, 54);
            this.textboxFunctionName.Name = "textboxFunctionName";
            this.textboxFunctionName.Size = new System.Drawing.Size(178, 21);
            this.textboxFunctionName.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridViewFunction);
            this.panel2.Location = new System.Drawing.Point(2, 112);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(723, 297);
            this.panel2.TabIndex = 17;
            // 
            // dataGridViewFunction
            // 
            this.dataGridViewFunction.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.dataGridViewFunction.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
            this.dataGridViewFunction.AllowColumnResize = false;
            this.dataGridViewFunction.AllowDragToGroup = false;
            this.dataGridViewFunction.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewFunction.EnableAlternatingRowColor = true;
            this.dataGridViewFunction.ViewDefinition = tableViewDefinition1;
            this.dataGridViewFunction.Name = "dataGridViewFunction";
            this.dataGridViewFunction.Size = new System.Drawing.Size(717, 281);
            this.dataGridViewFunction.TabIndex = 1;
            this.dataGridViewFunction.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;

            this.dataGridViewFunction.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.dataGridViewFunction_CellFormatting);
            this.dataGridViewFunction.CellEndEdit += new Telerik.WinControls.UI.GridViewCellEventHandler(this.dataGridViewFunction_CellLeave);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Controls.Add(this.buttonOk);
            this.panel1.Controls.Add(this.buttonSaveAsXml);
            this.panel1.Location = new System.Drawing.Point(7, 412);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(715, 54);
            this.panel1.TabIndex = 18;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(616, 19);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 24);
            this.buttonCancel.TabIndex = 27;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(530, 19);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(80, 24);
            this.buttonOk.TabIndex = 26;
            this.buttonOk.Text = "Ok";
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonSaveAsXml
            // 
            this.buttonSaveAsXml.Location = new System.Drawing.Point(405, 19);
            this.buttonSaveAsXml.Name = "buttonSaveAsXml";
            this.buttonSaveAsXml.Size = new System.Drawing.Size(110, 24);
            this.buttonSaveAsXml.TabIndex = 25;
            this.buttonSaveAsXml.Text = "Save as XML";
            this.buttonSaveAsXml.Click += new System.EventHandler(this.buttonSaveAsXml_Click);
            // 
            // saveFileDialogExportFunction
            // 
            this.saveFileDialogExportFunction.FileOk += new System.ComponentModel.CancelEventHandler(this.ExportMapping);
            // 
            // ReplaceFunction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(726, 467);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.labelType);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.chkExactMatch);
            this.Controls.Add(this.labelFunctionName);
            this.Controls.Add(this.comboBoxFunctionName);
            this.Controls.Add(this.textboxFunctionName);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReplaceFunction";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Function";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReplaceFunction_FormClosing);
            this.Load += new System.EventHandler(this.ReplaceFunction_Load);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFunction.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFunction)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonSaveAsXml)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelType;
        public System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.CheckBox chkExactMatch;
        private System.Windows.Forms.Label labelFunctionName;
        private System.Windows.Forms.ComboBox comboBoxFunctionName;
        private System.Windows.Forms.TextBox textboxFunctionName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SaveFileDialog saveFileDialogExportFunction;
        private Telerik.WinControls.UI.RadGridView dataGridViewFunction;
        private Telerik.WinControls.UI.RadButton buttonSaveAsXml;
        private Telerik.WinControls.UI.RadButton buttonOk;
        private Telerik.WinControls.UI.RadButton buttonCancel;
    }
}
