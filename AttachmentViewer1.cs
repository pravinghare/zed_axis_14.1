using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;

namespace DataProcessingBlocks
{
    public partial class RadFormAttachmentViewer : Telerik.WinControls.UI.RadForm
    {
        private string m_attchmentContent;

        public string AttachmentContent
        {
            get { return AttachmentContent; }
            set { m_attchmentContent = value; }
        }

        public RadFormAttachmentViewer()
        {
            InitializeComponent();
            m_attchmentContent = "Empty";
        }

        private void AttachmentViewer_Load(object sender, EventArgs e)
        {
            radTextBoxAttachment.Text = m_attchmentContent;
        }
    }
}
