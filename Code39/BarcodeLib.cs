﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Drawing2D;

namespace Barcode
{   
    /// <summary>
    /// This class was designed to give developers and easy way to generate a barcode image from a string of data.
    /// </summary>
    public class BarcodeLib : IDisposable
    {
        #region Enums
        public enum TYPE : int { UNSPECIFIED, UPCA, UPCE, UPC_SUPPLEMENTAL_2DIGIT, UPC_SUPPLEMENTAL_5DIGIT, EAN13, EAN8, Interleaved2of5, Standard2of5, Industrial2of5, CODE39, CODE39Extended, Codabar, PostNet, BOOKLAND, ISBN, JAN13, MSI_Mod10, MSI_2Mod10, MSI_Mod11, MSI_Mod11_Mod10, Modified_Plessey, CODE11, USD8, UCC12, UCC13, LOGMARS, CODE128, CODE128A, CODE128B, CODE128C, ITF14, CODE93 };
        public enum SaveTypes : int { JPG, BMP, PNG, GIF, TIFF, UNSPECIFIED };
        #endregion

        #region Variables
        //private IBarcode ibarcode = new BarcodeLib.Symbologies.Blank();
        private IBarcode ibarcode = new Barcode.Blank();
        private string Raw_Data = "";
        private string Encoded_Value = "";
        private Color m_ForeColor = Color.Black;
        private Color m_BackColor = Color.White;
        //Values required for creating label.
        private string m_fromComapnyName;
        private string m_fromPhone;
        private string m_receiverContact;
        private string m_receiverPhone;
        private string m_orderRef;
        private string m_noOfTotal;
        private string m_serviceType;
        private string m_toWeight;
        private string m_receiverAddress1;
        private string m_receiverAddress2;
        private string m_suburb;
        private string m_state;
        private string m_PostCode;
        private string m_instruction1;
        private string m_instruction2;
        private string m_instruction3;
        private string m_ConsolidatedCode;
        private string m_HeadPort;
        private string m_cannoteNumber;

        private int m_Width = 520;
        private int m_Height = 350;        
        private int startPos = 0;
        private int endPos = 0;
        private int endpoint = 0;
        private int position;
        private int startPoint = 0;
        Bitmap b = null;

        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.  Does not populate the raw data.  MUST be done via the RawData property before encoding.
        /// </summary>
        public BarcodeLib()
        {
            //constructor
        }//Barcode
       
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the raw data to encode.
        /// </summary>
        public string RawData
        {
            get { return Raw_Data; }
            set { Raw_Data = value; }
        }

        /// <summary>
        /// Gets the encoded value.
        /// </summary>
        public string EncodedValue
        {
            get { return Encoded_Value; }
        }  
        
        /// <summary>
        /// Gets or sets the color of the bars. (Default is black)
        /// </summary>
        public Color ForeColor
        {
            get { return this.m_ForeColor; }
            set { this.m_ForeColor = value; }
        }

        /// <summary>
        /// Gets or sets the background color. (Default is white)
        /// </summary>
        public Color BackColor
        {
            get { return this.m_BackColor; }
            set { this.m_BackColor = value; }
        }

        public string FromCompany
        {
            get { return m_fromComapnyName; }
            set { m_fromComapnyName = value; }
        }

        public string FromPhone
        {
            get { return m_fromPhone; }
            set { m_fromPhone = value; }
        }

        public string ReceiverContact
        {
            get { return m_receiverContact; }
            set { m_receiverContact = value; }
        }

        public string ReceiverPhone
        {
            get { return m_receiverPhone; }
            set { m_receiverPhone = value; }
        }     

        public string OrderRef
        {
            get { return m_orderRef; }
            set { m_orderRef = value; }
        }

        public string NoOfTotal
        {
            get { return m_noOfTotal; }
            set { m_noOfTotal = value;}
        }

        public string ServiceType
        {
            get { return m_serviceType; }
            set { m_serviceType = value; }
        }

        public string ToWeight
        {
            get { return m_toWeight; }
            set { m_toWeight = value; }
        }

        public string Address1
        {
            get { return m_receiverAddress1; }
            set { m_receiverAddress1 = value; }
        }
        public string Address2
        {
            get { return m_receiverAddress2; }
            set { m_receiverAddress2 = value; }
        }

        public string Suburb
        {
            get { return m_suburb;}
            set { m_suburb = value;}
        }

        public string State
        {
            get { return m_state; }
            set { m_state = value; }
        }

        public string Postcode
        {
            get { return m_PostCode; }
            set { m_PostCode = value; }
        }

        public string Instruction1
        {
            get { return m_instruction1; }
            set { m_instruction1 = value; }
        }

        public string Instruction2
        {
            get { return m_instruction2; }
            set { m_instruction2 = value; }
        }

        public string Instruction3
        {
            get { return m_instruction3; }
            set { m_instruction3 = value; }
        }

        public string ConsolidatedCode
        {
            get { return m_ConsolidatedCode; }
            set { m_ConsolidatedCode = value;}
        }
        public string HeaderPort
        {
            get { return m_HeadPort; }
            set { m_HeadPort = value; }
        }

        public string CannoteNumber
        {
            get { return m_cannoteNumber; }
            set { m_cannoteNumber = value; }
        }

        /// <summary>
        /// Gets or sets the width of the image to be drawn. (Default is 520 pixels)
        /// </summary>
        public int Width
        {
            get { return m_Width; }
            set { m_Width = value; }
        }
        /// <summary>
        /// Gets or sets the height of the image to be drawn. (Default is 350 pixels)
        /// </summary>
        public int Height
        {
            get { return m_Height; }
            set { m_Height = value; }
        }              
       
        #endregion

        #region Public Methods

        #region General Encode
        
        /// <summary>
        /// Encodes the raw data into binary form representing bars and spaces.  Also generates an Image of the barcode.
        /// </summary>
        /// <param name="StringToEncode">Raw data to encode.</param>
        /// <returns>Image representing the barcode.</returns>
        public Image Encode(string StringToEncode)
        {           
            this.BackColor = System.Drawing.Color.White;
            this.ForeColor = System.Drawing.Color.Black;
            Raw_Data = StringToEncode;
            return Encode();
        }
      
        /// <summary>
        /// Encodes the raw data into binary form representing bars and spaces.
        /// </summary>
        internal Image Encode()
        {
            ibarcode.Errors.Clear();
            //make sure there is something to encode
            if (Raw_Data.Trim() == "")
                throw new Exception("EENCODE-1: Input data not allowed to be blank.");

            this.Encoded_Value = "";
            ibarcode = new Code39(Raw_Data);

            this.Encoded_Value = ibarcode.Encoded_Value;
            this.Raw_Data = ibarcode.RawData;     
         
            return (Image)Generate_Image();;
        }//Encode
        #endregion

        #region Image Functions
        /// <summary>
        /// Gets a bitmap representation of the encoded data.
        /// </summary>
        /// <returns>Bitmap of encoded value.</returns>
        private Bitmap Generate_Image()
        {
            if (Encoded_Value == "") throw new Exception("EGENERATE_IMAGE-1: Must be encoded first.");
           
            b = new Bitmap(Width, Height);
            Width = 400;
            Height = 65;
           
            int iBarWidth =  Width / Encoded_Value.Length;
            int shiftAdjustment = (Width % Encoded_Value.Length) / 2;

            if (iBarWidth <= 0)
                throw new Exception("EGENERATE_IMAGE-2: Image size specified not large enough to draw image. (Bar size determined to be less than 1 pixel)");

            //draw image
            int pos = 0;

            using (Graphics g = Graphics.FromImage(b))
            {
                //clears the image and colors the entire background
                g.Clear(BackColor);

                //lines are fBarWidth wide so draw the appropriate color line vertically
                using (Pen pen = new Pen(ForeColor, iBarWidth))
                {
                    //Barcode (starting coordinate for barcode image to be created)
                    startPoint = 19;
                    pen.Alignment = PenAlignment.Right;
                    position = EncodedValue.IndexOf('1');
                    startPos = position * iBarWidth + startPoint;
                    endPos = 70;
                    endpoint = 75;
                    while (pos < Encoded_Value.Length)
                    {
                        if (Encoded_Value[pos] == '1')
                        {
                            if (pos == 0)
                            {
                                g.DrawLine(pen, new Point(startPoint, endpoint), new Point(startPoint, endpoint + Height));                               
                            }
                            else
                            {
                                g.DrawLine(pen, new Point(pos * iBarWidth + startPoint, endpoint), new Point(pos * iBarWidth + startPoint, endpoint + Height));
                            }
                        }
                        pos++;
                    }//while
                }//using
            }//using
           
            //Generate an whole image after creating barcode image. 
            Label_Generic((Image)b);
           // Encoded_Image = (Image)b;

            return b;             
        }      
        

        #region Label Generation
      
        /// <summary>
        /// Create an image with labels around barcode image created.
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private Image Label_Generic(Image img)
        {
            try
            {
                Height = 70;
                Bitmap bi = new Bitmap(500, 400);
                string fontStyle = "Arial";

                float margin = startPos - 10;              
                string manDate = DateTime.Now.ToString("dd/MM/yyyy");
                using (Graphics g = Graphics.FromImage(img))
                {
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.CompositingQuality = CompositingQuality.HighQuality;
                                
                    Brush brush = new SolidBrush(this.ForeColor);
                    Brush whiteBrush = new SolidBrush(this.BackColor);
                    //color a background color box at the bottom of the barcode to hold the string of data
                    g.FillRectangle(new SolidBrush(this.BackColor), new Rectangle(0, Width ,Height , 16));

                    // draw datastring under the barcode image
                    Font headerFont = new Font(fontStyle, 16, FontStyle.Bold);
                    Font font = new Font(fontStyle, 8, FontStyle.Regular);
                    Font f7 = new Font(fontStyle, 7, FontStyle.Regular);
                    Font f10 = new Font(fontStyle, 10, FontStyle.Regular);
                    Font fontBold = new Font(fontStyle, 14, FontStyle.Bold);
                    Font f12 = new Font(fontStyle, 12, FontStyle.Regular);
                    Font f14 = new Font(fontStyle, 14, FontStyle.Regular);
                    float size = font.Size;
                    float fontheight = font.GetHeight();
                    string strLabelText = this.RawData;
                    //cannote number.
                    g.DrawString("ConNote: ", font, brush, margin, endPos - fontheight);
                    float lengthOfCannote = size * 7;
                    g.DrawString(CannoteNumber, f10, brush, margin + lengthOfCannote, endPos - fontheight);
                    //Receiver Contact.
                    g.DrawString("Contact :", font, brush, margin, endPos - (2 * fontheight));
                    g.DrawString(ReceiverContact, f10, brush, margin + (size * 7), endPos - (2 * fontheight));

                    //From Company.
                    g.DrawString("From:", font, brush, margin, endPos - (3 * fontheight));
                    float lengthOfFrom = size * 6;
                    g.DrawString(FromCompany.ToUpper(), f10, brush, margin + lengthOfFrom, endPos - (3 * fontheight));
                    //Header
                    g.DrawString("Allied Express", headerFont, brush, margin, endPos - ((4 * fontheight) + fontheight));
                    //orderRef
                    g.DrawString("OrderRef:", font, brush, b.Width / 2, endPos - fontheight);
                    g.DrawString(OrderRef, f12, brush, ((b.Width / 2) + (size * 7)), endPos - fontheight);
                    //Phone
                    g.DrawString("Phone:", font, brush, ((b.Width / 2) + (size * 2)), endPos - (3 * fontheight));
                    g.DrawString(FromPhone, f10, brush, ((b.Width / 2) + (size * 7)), endPos - (3 * fontheight));
                    //Receiver Phone
                    g.DrawString("Phone:", font, brush, ((b.Width / 2) + (size * 2)), endPos - (2 * fontheight));
                    g.DrawString(ReceiverPhone, f10, brush, ((b.Width / 2) + (size * 7)), endPos - (2 * fontheight));
                    //Date
                    g.DrawString("Date:", font, brush, ((b.Width / 2) + (size * 19)) ,(endPos - (3 * fontheight) - (fontheight/2)));
                    g.DrawString(manDate, f10, brush, ((b.Width / 2) + (size * 23)), (endPos - (3 * fontheight) - (fontheight / 2)));
                    //Package
                    g.DrawString("Package:", font, brush, ((b.Width / 2) + (size * 20)), ((endPos - fontheight) - (fontheight / 3)));
                    g.DrawString(NoOfTotal, f12, brush, ((b.Width / 2) + (size * 25)), ((endPos - fontheight) - (fontheight / 3)));
                    //Parcel ID
                    g.DrawString(strLabelText, f14, brush, margin, endPos + Height);

                    //To
                    float yCoordinate = endPos + Height + f14.GetHeight();
                    g.DrawString("To:", font, brush, margin, yCoordinate);
                    g.DrawString(Address1, f10, brush, (margin + (size * 2)), yCoordinate  + fontheight);
                    g.DrawString(Address2, f10, brush, (margin + (size * 2)),  yCoordinate + (2 * fontheight));
                   
                    //Suburb Name
                    g.DrawString(Suburb.ToUpper(), fontBold , brush, (margin + (size * 2)), yCoordinate + (4 * fontheight));
                    //State
                    g.DrawString(State, fontBold, brush, (margin + (fontBold.Size * (Suburb.Length + 2))), yCoordinate + (4 * fontheight));

                    //Insrtructions
                    float yCordinateOfInstructions = yCoordinate + ((4 * fontheight) + fontBold.GetHeight());
                    g.DrawString("Special Instructions:", font, brush, margin, yCordinateOfInstructions);
                    g.DrawString(Instruction1, f10, brush, margin, yCordinateOfInstructions + fontheight);
                    g.DrawString(Instruction2, f10, brush, margin, yCordinateOfInstructions + (2 * fontheight));
                    g.DrawString(Instruction3, f10, brush, margin, yCordinateOfInstructions + (3 * fontheight));
                    
                    //Service
                    g.DrawString("Service Type:", font, brush, Width / 2 - (size * 6), yCoordinate - fontheight);
                    g.DrawString(ServiceType, fontBold, brush, Width / 2 + (size * 2), yCoordinate - fontheight);
                    
                    //Weight
                    int weightLength = ToWeight.Length - 2;
                    float weightSize = size * weightLength;
                    g.DrawString("Weight:", font, brush, ((b.Width / 2) + (size * 24)) - weightSize, endPos + Height);
                    g.DrawString(ToWeight, font, brush, ((b.Width / 2) + (size * 29)) - weightSize, endPos + Height);

                    //Package
                    g.DrawString("Package:", font, brush, ((b.Width / 2) + (size * 20)), endPos + Height + (2 *fontheight));
                    g.DrawString(NoOfTotal, f12, brush, ((b.Width / 2) + (size * 25)), endPos + Height + (2 * fontheight));
                                        
                    //Consolidated Date
                    int consolidatedLength = ConsolidatedCode.Length - 4;
                    float ConsolidatedSize = fontBold.Size * consolidatedLength;
                    g.FillRectangle(new SolidBrush(this.ForeColor), new Rectangle(Convert.ToInt32((b.Width / 2 + (size * 11)) - ConsolidatedSize), Convert.ToInt32(yCoordinate + (2 * fontheight)), Convert.ToInt32((fontBold.Size * ConsolidatedCode.Length) + 2), Convert.ToInt32(fontBold.Height)));
                    g.DrawString(ConsolidatedCode, fontBold, whiteBrush, (b.Width/2 + (size * 11) - ConsolidatedSize),yCoordinate + (2 * fontheight));

                    //HeadPort LENGTH  = 7
                    g.DrawString(HeaderPort, fontBold, brush,(b.Width / 2 + (size * 11) - ConsolidatedSize), yCoordinate + (2 * fontheight) + fontBold.GetHeight());

                    //Date/ManDate
                    g.DrawString("Date:", font, brush, ((b.Width / 2) + (size * 23)), yCoordinate + (2 * fontheight) + fontBold.GetHeight());
                    g.DrawString(manDate, f10, brush, ((b.Width / 2) + (size * 23)), yCoordinate + (3 * fontheight) + fontBold.GetHeight());

                    //PostCode
                    int postalLength = Postcode.Length - 4;
                    float postalSize = fontBold.Size * postalLength;
                    
                    g.DrawString(Postcode, fontBold, brush, ((b.Width / 2) + (size * 26)) - postalSize, yCoordinate + (4 * fontheight) + fontBold.GetHeight());                    

                    //Bottom Part
                    Pen p = new Pen(Color.Black, 2);
                    g.DrawString("Date", font, brush, ((b.Width / 2) + (size * 13)), yCordinateOfInstructions + (3 * fontheight));
                    g.DrawLine(p, ((b.Width / 2) + (size * 17)), yCordinateOfInstructions + (4 * fontheight), ((b.Width / 2) + (size * 30)), yCordinateOfInstructions + (4 * fontheight));
                    
                    //Time
                    g.DrawString("Time", font, brush, ((b.Width / 2) + (size * 13)), yCordinateOfInstructions + (5 * fontheight));
                    g.DrawLine(p, ((b.Width / 2) + (size * 17)), yCordinateOfInstructions + (6 * fontheight), ((b.Width / 2) + (size * 30)), yCordinateOfInstructions + (6 * fontheight));

                    //Name
                    g.DrawString("Name", font, brush, margin, yCordinateOfInstructions + (5 * fontheight));
                    g.DrawLine(p, margin + (size * 5), yCordinateOfInstructions + (6 * fontheight), margin + (size * 20), yCordinateOfInstructions + (6 * fontheight));

                    //Signature
                    g.DrawString("Signature", font, brush, margin + (size * 22), yCordinateOfInstructions + (5 * fontheight));
                    g.DrawLine(p, margin + (size * 29), yCordinateOfInstructions + (6 * fontheight), margin + (size * 43), yCordinateOfInstructions + (6 * fontheight));
                    
                    //Peel off section
                    //Draw Line
                    g.DrawLine(p, margin - 4, yCordinateOfInstructions + (7 * fontheight), ((b.Width / 2) + (size * 31)), yCordinateOfInstructions + (7 * fontheight));
                    g.DrawString("Allied Express", font, brush, margin , yCordinateOfInstructions + (7 * fontheight) + 2);
                    string peelOffStat = "RECEIVED IN GOOD CONDITION. SUBJECT TO CARRIER TERMS AND CONDITIONS.";                    
                    g.DrawString(peelOffStat, f7, brush, margin + (size * 14), yCordinateOfInstructions + (7 * fontheight) + 2);                    

                    g.Save();
                }//using
                return img;
            }//try
            catch (Exception ex)
            {
                throw new Exception("ELABEL_GENERIC-1: " + ex.Message);
            }//catch
        }//Label_Generic
        #endregion

        #endregion              

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
            }//try
            catch (Exception ex)
            {
                throw new Exception("EDISPOSE-1: " + ex.Message);
            }//catch
        }

        #endregion
    }
}
