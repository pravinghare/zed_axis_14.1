﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Management;
namespace Barcode
{
    class Code39 : BarcodeCommon, IBarcode
    {
        private System.Collections.Hashtable C39_Code = new System.Collections.Hashtable(); //is initialized by init_Code39()

        

        /// <summary>
        /// Encodes with Code39.
        /// </summary>
        /// <param name="input">Data to encode.</param>
        public Code39(string input)
        {
            Raw_Data = input;
        }//Code39

        /// <summary>
        /// Encodes with Code39.
        /// </summary>
        /// <param name="input">Data to encode.</param>
        /// <param name="AllowExtended">Allow Extended Code 39 (Full Ascii mode).</param>
        public Code39(string input, bool AllowExtended)
        {
            Raw_Data = input;            
        }

        /// <summary>
        /// Encode the raw data using the Code 39 algorithm.
        /// </summary>
        private string Encode_Code39()
        {
            this.init_Code39();
          
            string strFormattedData = "*" + Raw_Data.Replace("*", "") + "*";
         
            //this.FormattedData = strFormattedData;

            string result = "";
            //foreach (char c in this.FormattedData)
            foreach (char c in strFormattedData)
            {
                try
                {
                    result += C39_Code[c].ToString();
                    result += "0";//whitespace
                }//try
                catch
                {
                   Error("EC39-1: Invalid data.");
                }//catch
            }//foreach

            result = result.Substring(0, result.Length - 1);

            //clear the hashtable so it no longer takes up memory
            this.C39_Code.Clear();

            return result;
        }//Encode_Code39
        private void init_Code39()
        {
            C39_Code.Clear();
            C39_Code.Add('0', "101001101101");
            C39_Code.Add('1', "110100101011");
            C39_Code.Add('2', "101100101011");
            C39_Code.Add('3', "110110010101");
            C39_Code.Add('4', "101001101011");
            C39_Code.Add('5', "110100110101");
            C39_Code.Add('6', "101100110101");
            C39_Code.Add('7', "101001011011");
            C39_Code.Add('8', "110100101101");
            C39_Code.Add('9', "101100101101");
            C39_Code.Add('A', "110101001011");
            C39_Code.Add('B', "101101001011");
            C39_Code.Add('C', "110110100101");
            C39_Code.Add('D', "101011001011");
            C39_Code.Add('E', "110101100101");
            C39_Code.Add('F', "101101100101");
            C39_Code.Add('G', "101010011011");
            C39_Code.Add('H', "110101001101");
            C39_Code.Add('I', "101101001101");
            C39_Code.Add('J', "101011001101");
            C39_Code.Add('K', "110101010011");
            C39_Code.Add('L', "101101010011");
            C39_Code.Add('M', "110110101001");
            C39_Code.Add('N', "101011010011");
            C39_Code.Add('O', "110101101001");
            C39_Code.Add('P', "101101101001");
            C39_Code.Add('Q', "101010110011");
            C39_Code.Add('R', "110101011001");
            C39_Code.Add('S', "101101011001");
            C39_Code.Add('T', "101011011001");
            C39_Code.Add('U', "110010101011");
            C39_Code.Add('V', "100110101011");
            C39_Code.Add('W', "110011010101");
            C39_Code.Add('X', "100101101011");
            C39_Code.Add('Y', "110010110101");
            C39_Code.Add('Z', "100110110101");
            C39_Code.Add('-', "100101011011");
            C39_Code.Add('.', "110010101101");
            C39_Code.Add(' ', "100110101101");
            C39_Code.Add('$', "100100100101");
            C39_Code.Add('/', "100100101001");
            C39_Code.Add('+', "100101001001");
            C39_Code.Add('%', "101001001001");
            C39_Code.Add('*', "100101101101");
        }//init_Code39
     
        private void InsertExtendedCharsIfNeeded(ref string FormattedData)
        {
            string output = "";
            foreach (char c in Raw_Data)
            {
                try
                {
                    string s = C39_Code[c].ToString();
                    output += c;
                }//try
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    //insert extended substitution
                    //object oTrans = ExtC39_Translation[c.ToString()];
                    //output += oTrans.ToString();
                }//catch
            }//foreach

            FormattedData = output;
        }

        #region IBarcode Members

        public string Encoded_Value
        {
            get { return Encode_Code39(); }
        }

        #endregion
    }
}
