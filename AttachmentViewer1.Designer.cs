namespace DataProcessingBlocks
{
    partial class RadFormAttachmentViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radTextBoxAttachment = new Telerik.WinControls.UI.RadTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radTextBoxAttachment
            // 
            this.radTextBoxAttachment.Location = new System.Drawing.Point(12, 22);
            this.radTextBoxAttachment.Multiline = true;
            this.radTextBoxAttachment.Name = "radTextBoxAttachment";
            this.radTextBoxAttachment.ReadOnly = true;
            // 
            // 
            // 
            this.radTextBoxAttachment.RootElement.StretchVertically = true;
            this.radTextBoxAttachment.Size = new System.Drawing.Size(948, 497);
            this.radTextBoxAttachment.TabIndex = 0;
            this.radTextBoxAttachment.TabStop = false;
            // 
            // RadFormAttachmentViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(972, 531);
            this.Controls.Add(this.radTextBoxAttachment);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "RadFormAttachmentViewer";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.Text = "Attachment Viewer";
            this.ThemeName = "ControlDefault";
            this.Load += new System.EventHandler(this.AttachmentViewer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.UI.RadTextBox radTextBoxAttachment;
    }
}

