using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TransactionImporter
{
    public partial class Password : Form
    {
        /// <summary>
        /// Email password
        /// </summary>
        private string m_passwordText;

        /// <summary>
        /// Get or Set email password
        /// </summary>
        public string PasswordText
        {
            get { return m_passwordText; }
            set { m_passwordText = value; }
        }
        
        public Password()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (textBoxPassword.Text != string.Empty)
            {
                PasswordText = textBoxPassword.Text;
                this.Close();
            }
        }
    }
}