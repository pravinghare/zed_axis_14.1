using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    public partial class QBandMYOBMessage : Form
    {
        System.Threading.Timer myTimer;
        System.Threading.TimerCallback timerCallBK;
        
        public QBandMYOBMessage(string message)
        {
            InitializeComponent();
            this.textBoxMessage.Text = message;
           
            timerCallBK = new System.Threading.TimerCallback(MyTimerEvent);
            
            
        }

        private void QBandMYOBMessage_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            myTimer = new System.Threading.Timer(timerCallBK, null, 1000, 5000);

        }


        private void MyTimerEvent(object state)
        {
            try
            {
                this.Invoke(new MethodInvoker(delegate { this.Close(); }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

      

        private void QBandMYOBMessage_FormClosing(object sender, FormClosingEventArgs e)
        {
            myTimer.Dispose();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}