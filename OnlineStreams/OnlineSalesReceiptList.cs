﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Online_BaseStreams;
using QBPOSStreams;
using System.Collections.ObjectModel;
using Streams;
using System.Xml;
using System.ComponentModel;
using System.Windows.Forms;
using DataProcessingBlocks;
using Intuit.Ipp.Security;
using Intuit.Ipp.Core;
using Intuit.Ipp.DataService;
using Intuit.Ipp.QueryFilter;
using System.Data;
namespace OnlineStreams
{
  public class OnlineSalesReceiptList: Online_BaseSalesReceiptList
    {


        #region properties
        public string Id
        {
            get { return m_Id; }
            set { m_Id = value; }
        }

        public string SyncToken
        {
            get { return m_SyncToken; }
            set { m_SyncToken = value; }
        }

        public string CreateTime
        {
            get { return m_CreateTime; }
            set { m_CreateTime = value; }
        }

        public string LastUpdatedTime
        {
            get { return m_LastUpdatedTime; }
            set { m_LastUpdatedTime = value; }
        }

        public string LineId
        {
            get { return m_LineId; }
            set { m_LineId = value; }
        }

        public string LineNumber
        {
            get { return m_LineNumber; }
            set { m_LineNumber = value; }
        }

        public string LineDescription
        {
            get { return m_LineDescription; }
            set { m_LineDescription = value; }
        }

        public string LineAmount
        {
            get { return m_LineAmount; }
            set { m_LineAmount = value; }
        }


        public string LineDetailType
        {
            get { return m_LineDetailType; }
            set { m_LineDetailType = value; }
        }

        public string LineSalesItemName
        {
            get { return m_LineSalesItemName; }
            set { m_LineSalesItemName = value; }
        }

        public string LineSalesItemUnitPrice
        {
            get { return m_LineSalesItemUnitPrice; }
            set { m_LineSalesItemUnitPrice = value; }
        }

        public string LineSalesItemQty
        {
            get { return m_LineSalesItemQty; }
            set { m_LineSalesItemQty = value; }
        }

        public string LineSalesItemTaxCodeRef
        {
            get { return m_LineSalesItemTaxCodeRef; }
            set { m_LineSalesItemTaxCodeRef = value; }
        }

        public string TotalTax
        {
            get { return m_TotalTax; }
            set { m_TotalTax = value; }
        }

        public string CustomerRefName
        {
            get { return m_CustomerRefName; }
            set { m_CustomerRefName = value; }
        }

        public string BillId
        {
            get { return m_BillId; }
            set { m_BillId = value; }
        }

        public string BillLine1
        {
            get { return m_BillLine1; }
            set { m_BillLine1 = value; }
        }


        public string BillLine2
        {
            get { return m_BillLine2; }
            set { m_BillLine2 = value; }
        }


        public string BillCity
        {
            get { return m_BillCity; }
            set { m_BillCity = value; }
        }

        public string BillCountry
        {
            get { return m_BillCountry; }
            set { m_BillCountry = value; }
        }

        public string BillSubDivisionCode
        {
            get { return m_BillSubDivisionCode; }
            set { m_BillSubDivisionCode = value; }
        }

        public string BillPostalCode
        {
            get { return m_BillPostalCode; }
            set { m_BillPostalCode = value; }
        }


        public string TxnDate
        {
            get { return m_TxnDate; }
            set { m_TxnDate = value; }
        }


        public string AutoDocNumber
        {
            get { return m_AutoDocNumber; }
            set { m_AutoDocNumber = value; }
        }

        public string TotalAmt
        {
            get { return m_TotalAmt; }
            set { m_TotalAmt = value; }
        }

        public string PrintStatus
        {
            get { return m_PrintStatus; }
            set { m_PrintStatus = value; }
        }

        public string EmailStatus
        {
            get { return m_EmailStatus; }
            set { m_EmailStatus = value; }
        }

        public string Balance
        {
            get { return m_Balance; }
            set { m_Balance = value; }
        }

        public string ApplyTaxAfterDiscount
        {
            get { return m_ApplyTaxAfterDiscount; }
            set { m_ApplyTaxAfterDiscount = value; }
        }      

        public string DepositToAccountName
        {
            get { return m_DepositToAccountName; }
            set { m_DepositToAccountName = value; }
        }  
        #endregion

        public async  void  Getmethod()
        {
          
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            DataService dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.SalesReceipt> customerQueryService = new QueryService<Intuit.Ipp.Data.SalesReceipt>(serviceContext);

            DataTable dtSalesReceipt = new DataTable();
            ReadOnlyCollection<Intuit.Ipp.Data.SalesReceipt> data = new ReadOnlyCollection<Intuit.Ipp.Data.SalesReceipt>(customerQueryService.ExecuteIdsQuery("select *  from SalesReceipt").ToList());
            dtSalesReceipt = GenerateExportData.GetInstance().CreateOnlineSalesReceiptDataTable(data);
            
            TransactionImporter.TransactionImporter frmImporter = (TransactionImporter.TransactionImporter)Application.OpenForms["TransactionImporter"];
            //DataGridView datagrid = (DataGridView)frmImporter.dataGridViewResult;
            //datagrid.DataSource = dtSalesReceipt;

            Intuit.Ipp.Utility.RequestLogElement ee = new Intuit.Ipp.Utility.RequestLogElement();
            ee.RequestResponseLoggingDirectory.LastOrDefault();
            MessageBox.Show(ee.ToString());
        }
    }  
}
