﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using DataProcessingBlocks;
using EDI.Constant;
using Telerik.WinControls.UI;
using Telerik.WinControls;
using Intuit.Ipp.Core;
using Intuit.Ipp.QueryFilter;
using Intuit.Ipp.DataService;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using DataProcessingBlocks.XeroConnection;
using Xero.NetStandard.OAuth2.Api;

namespace TransactionImporter
{
    public partial class ImportSplashScreen : Telerik.WinControls.UI.RadForm
    {
        //600
        bool display = false;
        #region Static and Private Members

        //Static
        private static ImportSplashScreen m_frmSplash = null;
        static Thread m_Thread = null;
        static string m_Status = "";
        public static bool setFlag;
        //Export import errors Axis 11.0 -131
        private static DataSet exportExcel = null;
        private static string name_column = null;
        private static string ref_column = null;

        //Private
        private string m_failMSG;
        private string m_succesMSG;
        private string m_skipped;
        private bool flag = false;
        private string Type = string.Empty;
        //Rad Data Grid Axis 11.0 -124
        private delegate void FillValueImportDelegate(string name, string refno, string statusMsg, string msg, string txn);
        private GridViewTextBoxColumn radTextBoxColumn;
        private GridViewHyperlinkColumn radLinkColumn;

        string importType = string.Empty;
        string refno = string.Empty;
        string tId = string.Empty;
        string syncToken = string.Empty;
        string accountid = string.Empty;
        string txnList = string.Empty;
        public bool deleteFlag = false;
        public bool deleteRecordFlag = false;
        QuickBookAccountSettings objQBAccountSettings = new QuickBookAccountSettings();
        //Repository repository = null; //Axis 10.1

        //Public
        public bool skipFlag = false;
        public bool skipAllFlag = false;
        public static bool sucess = false;
        public double amount;
        public string txnId;
        public string accountName;
        public string finalResult;
        //bug 499
        public bool is_imported = false;
        ImportSummary summary = ImportSummary.GetInstance();
        public int statusbarPercentage = 0;
        #endregion

        public string FailMsg
        {
            set { this.m_failMSG = value; }
            //get { return this.labelFailure.Text; }
        }

        public string SuccesMsg
        {
            set { this.m_succesMSG = value; }
            //get { return this.labelSuccess.Text; }
        }

        public string MessageToShow
        {
            set
            {
                this.labelImportingRow.Text = value;
            }
            get
            {
                return this.labelImportingRow.Text;
            }
        }

        public string MessageToShowValidate
        {
            set
            {
                this.labelValidateRow.Text = value;
            }
            get
            {
                return this.labelValidateRow.Text;
            }
        }

        public string Skipped
        {
            set { this.m_skipped = value; }
            // get { return this.labelSkipped.Text; }
        }

        public bool SkipOneFlag()
        {
            if (skipFlag == true)
                return true;
            else
                return false;
        }

        public bool SkipFlag()
        {
            if (skipAllFlag == true)
                return true;
            else
                return false;
        }

        public void SetMessage(string message)
        {
            this.MessageToShow = message;
        }

        public void SetMessageValidate(string message)
        {
            this.MessageToShowValidate = message;
        }

        /// <summary>
        /// Axis 11.0 -131 Set the reference number column or name column to match error message to imported data
        /// </summary>
        /// <param name="type">Import type</param>
        public void setColumn(string type)
        {
            string rcolumn = "RefNumber";
            string ncolumn = "Name";
            string connectedsw = CommonUtilities.GetInstance().ConnectedSoftware;
            if (connectedsw == EDI.Constant.Constants.QBstring)
            {
                #region quickbooks
                //customer, employee, price level, vendor & all itemtypes
                if (type == DataProcessingBlocks.ImportType.Customer.ToString() || type == DataProcessingBlocks.ImportType.Employee.ToString() 
                    || type == DataProcessingBlocks.ImportType.Vendor.ToString() || type == DataProcessingBlocks.ImportType.PriceLevel.ToString() 
                    || type.Contains("Item") || type == DataProcessingBlocks.ImportType.Class.ToString() 
                    || type == DataProcessingBlocks.ImportType.OtherName.ToString() 
                    || type == DataProcessingBlocks.ImportType.InventorySite.ToString())
                {
                    ncolumn = "Name";
                    rcolumn = "";
                }
                else if(type == DataProcessingBlocks.ImportType.BillingRate.ToString())
                {
                    ncolumn = "BillingRateName";
                    rcolumn = "";
                }
                else if(type == DataProcessingBlocks.ImportType.ClearedStatus.ToString())
                {
                    ncolumn = "";
                    rcolumn = "TxnID";
                }
                else if (type == DataProcessingBlocks.ImportType.ListMerge.ToString())
                {
                    ncolumn = "MergeTo";
                    rcolumn = "";
                }
                else if (type == DataProcessingBlocks.ImportType.Check.ToString() || type == DataProcessingBlocks.ImportType.CreditCardCharge.ToString() || type == DataProcessingBlocks.ImportType.CreditCardCredit.ToString())
                {
                    ncolumn = "AccountFullName";
                }
                else if (type == DataProcessingBlocks.ImportType.Check.ToString() || type == DataProcessingBlocks.ImportType.CreditCardCharge.ToString())
                {
                    ncolumn = "APAccountRefFullName";
                }
                else if (type == DataProcessingBlocks.ImportType.CustomFieldList.ToString())
                {
                    ncolumn = "DataExtName";
                    rcolumn = "DataExtValue";
                }
                //Credit Memo,Estimate,Invoice,InventoryAdjustment,StatementCharges,SalesOrder,SalesReciept,ReceivePayment
                else if (type == DataProcessingBlocks.ImportType.CreditMemo.ToString() || type == DataProcessingBlocks.ImportType.Estimate.ToString() || type == DataProcessingBlocks.ImportType.Invoice.ToString() || type == DataProcessingBlocks.ImportType.StatementCharges.ToString() || type == DataProcessingBlocks.ImportType.SalesOrder.ToString() || type == DataProcessingBlocks.ImportType.SalesReceipt.ToString() || type == DataProcessingBlocks.ImportType.ReceivePayment.ToString())
                {
                    ncolumn = "CustomerRefFullName";
                    rcolumn = type + rcolumn;
                }
                else if (type == DataProcessingBlocks.ImportType.InventoryAdjustment.ToString())
                {
                    ncolumn = "CustomerRefFullName";
                }
                else if (type == DataProcessingBlocks.ImportType.TimeTracking.ToString())
                {
                    ncolumn = "CustomerFullName";
                    rcolumn = "";
                }
                else if (type == DataProcessingBlocks.ImportType.Deposit.ToString())
                {
                    ncolumn = "DepositToAccountRefFullName";
                    rcolumn = "TxnID";
                }
                else if (type == DataProcessingBlocks.ImportType.JournalEntry.ToString())
                {
                    ncolumn = "CurrencyRefFullName";
                }
                else if (type == DataProcessingBlocks.ImportType.TransferInventory.ToString())
                {
                    ncolumn = "FromInventorySiteRefFullName";
                }
                else if (type == DataProcessingBlocks.ImportType.VehicleMileage.ToString())
                {
                    ncolumn = "VehicleFullName";
                    rcolumn = "";
                }
                else if (type == DataProcessingBlocks.ImportType.PurchaseOrder.ToString())
                {
                    ncolumn = "VendorFullName";
                    rcolumn = type + rcolumn;
                }
                else if (type == DataProcessingBlocks.ImportType.Bill.ToString() || type == DataProcessingBlocks.ImportType.VendorCredits.ToString())
                {
                    ncolumn = "VendorFullName";
                }
                #endregion
            }
            else if (connectedsw == EDI.Constant.Constants.QBOnlinestring)
            {
                rcolumn = "DocNumber";
                if (type == DataProcessingBlocks.OnlineImportType.TimeActivity.ToString())
                {
                    ncolumn = "NameOf";
                    rcolumn = "";
                }
                else if (type == DataProcessingBlocks.OnlineImportType.Customer.ToString() || type == DataProcessingBlocks.OnlineImportType.Items.ToString())
                {
                    ncolumn = "FullyQualifiedName";
                    rcolumn = "";
                }
                else if (type == DataProcessingBlocks.OnlineImportType.Employee.ToString())
                {
                    ncolumn = "DisplayName";
                    rcolumn = "";
                }

            }
            else if (connectedsw == EDI.Constant.Constants.QBPOSstring)
            {
                if (type == DataProcessingBlocks.POSImportType.Customer.ToString())
                {
                    ncolumn = "LastName";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.Employee.ToString())
                {
                    ncolumn = "LoginName";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.InventoryCostAdjustment.ToString() || type == DataProcessingBlocks.POSImportType.InventoryQtyAdjustment.ToString())
                {
                    ncolumn = "Reason";
                    rcolumn = "";
                }

                if (type == DataProcessingBlocks.POSImportType.ItemInventory.ToString())
                {
                    ncolumn = "Desc1";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.PriceAdjustment.ToString())
                {
                    ncolumn = "PriceAdjustmentName";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.PriceDiscount.ToString())
                {
                    ncolumn = "PriceDiscountName";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.PurchaseOrder.ToString())
                {
                    ncolumn = "";
                    rcolumn = "PurchaseOrderNumber";
                }
                if (type == DataProcessingBlocks.POSImportType.SalesOrder.ToString())
                {
                    ncolumn = "";
                    rcolumn = "SalesOrderNumber";
                }
                if (type == DataProcessingBlocks.POSImportType.SalesReceipt.ToString())
                {
                    ncolumn = "";
                    rcolumn = "SalesReceiptNumber";
                }
                if (type == DataProcessingBlocks.POSImportType.TimeEntry.ToString())
                {
                    ncolumn = "CreatedBy";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.TransferSlip.ToString())
                {
                    ncolumn = "FromStoreNumber";
                    rcolumn = "";
                }
                if (type == DataProcessingBlocks.POSImportType.Voucher.ToString())
                {
                    ncolumn = "";
                    rcolumn = "InvoiceNumber";
                }
            }
            else
            {
                #region xero
                if (type == DataProcessingBlocks.XeroImportType.Contacts.ToString())
                {
                    ncolumn = "Name";
                    rcolumn = "";
                }
                else if (type == DataProcessingBlocks.XeroImportType.InvoiceCustomer.ToString() || type == DataProcessingBlocks.XeroImportType.InvoiceSupplier.ToString())
                {
                    rcolumn = "InvoiceNumber";
                    ncolumn = "ContactName";
                }
                else if (type == DataProcessingBlocks.XeroImportType.BankReceive.ToString() || type == DataProcessingBlocks.XeroImportType.BankSpend.ToString())
                {
                    rcolumn = "Reference";
                    ncolumn = "ContactName";
                }
                else if (type == DataProcessingBlocks.XeroImportType.Items.ToString())
                {
                    ncolumn = "Code";
                    rcolumn = "";
                }
                #endregion
            }
            ref_column = rcolumn;
            name_column = ncolumn;
        }

        #region Progress bars
        //bug 499
        //public int PercentageComplete
        //{
        //    set
        //    {
        //        try
        //        {
        //            if (value <= 100)
        //            {
        //                this.progressBarImporting.Value = value;
        //            }
        //            else
        //            {
        //                this.progressBarImporting.Value = 100;
        //            }
        //        }
        //        catch
        //        {                    

        //        }

        //    }
        //}


        //727
        public int PercentageCompleteValidate
        {
            set
            {
                this.radProgressBarStatusBar.Value1 = value;
            }
        }

        public int PercentageCompleteListUpdate
        {
            set
            {
                this.radProgressBarListUpdate.Value1 = value;
                radProgressBarListUpdate.Text = value.ToString() + "%";
            }
        }

        public void SetImportPercentageComplete(int percentageComplete)
        {

            //try
            //{
            //    if (percentageComplete <= 100)
            //    {
            //        this.PercentageComplete = percentageComplete;
            //    }
            //    else
            //    {
            //        this.PercentageComplete = 100;
            //    }
            //}
            //catch 
            //{               

            //}

            //bug 499
            if (is_imported == true)
            {
                //727
                this.radprogressBarImporting.Value1 = percentageComplete;
                if (this.radprogressBarImporting.Value1 != 0)
                {
                    radprogressBarImporting.Text = percentageComplete.ToString() + "%";
                }
            }

            // this.PercentageComplete = percentageComplete;
        }
        public void SetValidPercentageComplete(int percentageComplete)
        {
            this.PercentageCompleteValidate = percentageComplete;
        }
        public void SetListPercentageComplete(int percentageComplete)
        {
            this.PercentageCompleteListUpdate = percentageComplete;
        }
        #endregion

        #region Import Splash
        public static ImportSplashScreen GetInstance()
        {
            m_frmSplash = new ImportSplashScreen();
            exportExcel = new DataSet();

            name_column = null;
            return m_frmSplash;
        }

        public bool IsEndLessLoop
        {
            set
            {
                if (value)
                {
                    this.timerStatus.Start();
                }
                else
                {
                    this.timerStatus.Stop();
                }
            }
        }

        public bool CancelSplash
        {
            set
            {
                this.radbuttonCancelImportSplashScreen.Visible = value;
            }
        }

        public void CloseImportSplash()
        {
            CommonUtilities.GetInstance().is_statement_preview = false;
            CommonUtilities.GetInstance().ValidateFlag = false;
            CommonUtilities.GetInstance().ListFlag = false;
            this.Close();
        }

        public void ShowImportSplash(string message, bool isEndlessLoop)
        {
            try
            {
                if (this.IsDisposed)
                {
                    m_frmSplash = null;
                }

            }
            catch { }
            try
            {
                //splashScreen = null;
            }
            catch { }

            if (m_frmSplash == null)
            {
                m_frmSplash = new ImportSplashScreen();
            }
            //Assigning message to splash screen.
            this.MessageToShow = message;
            this.MessageToShowValidate = message;
            //Assigning loop value to splash.
            this.IsEndLessLoop = isEndlessLoop;
            //if (message.Equals(EDI.Constant.Constants.QBflashmessage.ToString()) || message.Equals(EDI.Constant.Constants.ExportingDataFlashMessage.ToString()))
            //    splashScreen.CancelSplash = true;
            //else
            //    splashScreen.CancelSplash = false;
            this.CancelSplash = true;
            try
            {
                this.ShowDialog();

                //splashScreen.Show();
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ImportSplashScreen()
        {
            InitializeComponent();
            this.timerStatus.Interval = 50;
            Cursor.Current = Cursors.WaitCursor;

            //this.timerStatus.Start();
        }
        public ImportSplashScreen(bool flag)
        {
            setFlag = flag;
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Shows splash screen
        /// </summary>
        static public void ShowSplashScreen()
        {
            // Make sure it's only launched once.
            if (m_frmSplash != null)
                return;
            m_Thread = new Thread(new ThreadStart(ImportSplashScreen.ShowForm));
            m_Thread.IsBackground = true;
            m_Thread.SetApartmentState(ApartmentState.STA);
            m_Thread.Start();
        }

        /// <summary>
        /// Sets the message for Splash screen.
        /// </summary>
        /// <param name="statusMessage"></param>
        static public void SetSatusString(string statusMessage)
        {
            m_Status = statusMessage;
        }

        /// <summary>
        /// A private entry point for the thread.
        /// </summary>
        static private void ShowForm()
        {
            m_frmSplash = new ImportSplashScreen();
            Application.Run(m_frmSplash);
        }

        /// <summary>
        /// Close the Splash screen
        /// </summary>
        static public void CloseForm()
        {
#if DEBUG
           
#else
            {
                m_frmSplash.Close();
            }
#endif
            if (m_frmSplash != null && m_frmSplash.IsDisposed == false)
            {
                // Make it start going away.
            }
            m_Thread = null;	// we don't need these any more.
            m_frmSplash = null;


        }

        /// <summary>
        /// Axis 11.0 -131 Add data table to exportExcel dataset
        /// </summary>
        static public void addDataTable(DataTable data_tab)
        {
            exportExcel.Tables.Add(data_tab.Copy());

        }

        /// <summary>
        /// Axis 11.0 -131 Delete data tables from exportExcel dataset by creating new dataset
        /// </summary>
        static public void deleteDataTables()
        {
            while (exportExcel.Tables.Count > 0)
            {
                DataTable table = exportExcel.Tables[0];
                if (exportExcel.Tables.CanRemove(table))
                {
                    exportExcel.Tables.Remove(table);
                }
            }
            exportExcel = new DataSet();
        }
        #endregion


        #region Event Handlers
        private void ImportSplashScreen_Load(object sender, EventArgs e)
        {
            this.FormElement.TitleBar.FillPrimitive.BackColor = Color.White;
            this.FormElement.TitleBar.FillPrimitive.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            this.FormElement.TitleBar.TitlePrimitive.ForeColor = Color.Black;
            this.FormElement.TitleBar.MaximizeButton.Padding = new System.Windows.Forms.Padding(14, 0, 14, 0);
            
            timerStatus.Enabled = true;
            //bug 499

            //727
            radprogressBarImporting.Value1 = 0;
            radProgressBarListUpdate.Value1 = 0;
            radProgressBarStatusBar.Value1 = 0;
            radProgressBarListUpdate.Text = "";
            radprogressBarImporting.Text = "";
            if (setFlag == true)
            {
                radProgressBarListUpdate.Hide();
                labelListUpdate.Hide();
            }
            if (setFlag == false)
            {
                radProgressBarListUpdate.Show();
                labelListUpdate.Show();
            }

            flag = false;
            this.labelSuccess.Text = string.Empty;
            this.labelSkipped.Text = string.Empty;
            this.labelFailure.Text = string.Empty;
            if (CommonUtilities.GetInstance().SelectedMapping != null)
            {
                this.Type = CommonUtilities.GetInstance().SelectedMapping.ImType;
            }

            radGridViewImportStatus.Rows.Clear();
            FillRadGridViewHeader();
            skipAllFlag = false;
            skipFlag = false;
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        private void ImportSplashScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            //dataGridViewImportStatus.Rows.Clear();
            radGridViewImportStatus.Rows.Clear();
            Cursor.Current = Cursors.WaitCursor;
            TransactionImporter axisForm = (TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;
            if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
            {
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseImportSplash();
                this.CloseImportSplash();
            }
            if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                bkWorker.CancelAsync();
            }
            if (axisForm.backgroundWorkerProcessLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseSplash();
            }
            CodingTemplatePreview.GetInstance().Close();
            CodingTemplatePreview.GetInstance().radGridCodingTemplate = CommonUtilities.GetInstance().radgrd;
            string[] arr = CommonUtilities.GetInstance().arrayCommonutility;
            // GridViewDataRowInfo rowInfo = new GridViewDataRowInfo(CodingTemplatePreview.GetInstance().radGridCodingTemplate.MasterView);

            for (int cnt1 = 0; cnt1 < CommonUtilities.GetInstance().radgrd.RowCount; cnt1++)
            {
                for (int cnt = 0; cnt < arr.Count(); cnt++)
                {
                    // CommonUtilities.GetInstance().radgrd.Rows[cnt1].Cells[2].Value.ToString() +"++++" + arr[cnt]);
                    string val = CommonUtilities.GetInstance().radgrd.Rows[cnt1].Cells[2].Value.ToString();
                    if (!val.Contains("X"))
                    {
                        if (val != "")
                        {
                            int t = 1;
                            string q = null;
                            q = ReverseString(val);
                            while (q.Length > 11)
                            {
                                q = q.Remove(q.Length - t);
                            }
                            val = ReverseString(q);
                            // MessageBox.Show(val.Substring(0, 10) + "sdfsd"+ arr[cnt]);
                            if (val == arr[cnt])
                            {
                                CommonUtilities.GetInstance().radgrd.Rows[cnt1].Cells[13].Value = "Imported";
                                for (int i = 0; i < 14; i++)
                                {
                                    CommonUtilities.GetInstance().radgrd.Rows[cnt1].Cells[i].Style.BackColor = Color.Red;

                                    // CodingTemplatePreview.GetInstance().CellStyleRed(rowInfo.Cells[i]);
                                }
                                break;
                            }
                            else
                            {
                                CommonUtilities.GetInstance().radgrd.Rows[cnt1].Cells[13].Value = "Unmatched";
                            }
                        }
                        else
                        {
                            CommonUtilities.GetInstance().radgrd.Rows[cnt1].Cells[13].Value = "Unmatched";
                        }
                    }

                }
            }
        }
        #endregion

        #region Axis 11.0 -124 Telerik rad data grid

        #region Fill Grid
        public void FillRadGridViewHeader()
        {
            try
            {
                if (radGridViewImportStatus.Columns.Count <= 0)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        switch (i)
                        {
                            case 0:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "Type";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                radGridViewImportStatus.Columns[0].AllowFiltering = false;
                                radGridViewImportStatus.Columns[0].Width = 50;
                                break;
                            case 1:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "Ref";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                radGridViewImportStatus.Columns[1].Width = 40;
                                break;
                            case 2:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "Name";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                radGridViewImportStatus.Columns[2].Width = 100;
                                break;
                            case 3:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "Status";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                radGridViewImportStatus.Columns[3].Width = 50;
                                break;
                            case 4:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "Message";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                radGridViewImportStatus.Columns[4].AllowFiltering = false;
                                radGridViewImportStatus.Columns[4].Width = 270;
                                radGridViewImportStatus.TableElement.ScrollToColumn(4);
                                break;
                            case 5:
                                radLinkColumn = new GridViewHyperlinkColumn();
                                radLinkColumn.HeaderText = "View";
                                radGridViewImportStatus.Columns.Add(radLinkColumn);
                                radGridViewImportStatus.Columns[5].AllowFiltering = false;
                                radGridViewImportStatus.Columns[5].Width = 50;
                                break;
                            case 6:
                                radLinkColumn = new GridViewHyperlinkColumn();
                                radLinkColumn.HeaderText = "Undo";
                                radGridViewImportStatus.Columns.Add(radLinkColumn);
                                radGridViewImportStatus.Columns[6].AllowFiltering = false;
                                radGridViewImportStatus.Columns[6].Width = 80;
                                break;
                            case 7:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "TxnID";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                break;
                            case 8:
                                radTextBoxColumn = new GridViewTextBoxColumn();
                                radTextBoxColumn.HeaderText = "AccountID";
                                radGridViewImportStatus.Columns.Add(radTextBoxColumn);
                                break;
                        }

                    }
                    if (Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.QBstring || Mappings.GetInstance().ConnectedSoft == EDI.Constant.Constants.Xerostring)
                    {
                        radGridViewImportStatus.Columns[7].IsVisible = false;
                        radGridViewImportStatus.Columns[8].IsVisible = false;
                    }
                    if (TransactionImporter.rdbQBPOSbutton == true || TransactionImporter.rdbQBOnlinebutton == true)
                    {
                        radGridViewImportStatus.Columns[7].IsVisible = false;
                        radGridViewImportStatus.Columns[8].IsVisible = false;
                    }
                 
                    //radGridViewImportStatus.TableElement.AlternatingRowColor = Color.LightSteelBlue;
                    //radGridViewImportStatus.TableElement.RowHeight = 60;

                    radGridViewImportStatus.AutoScroll = true;
                    radGridViewImportStatus.EnableGrouping = false;

                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("Exp" + Ex.Message);
            }
        }

        public void FillRadGridViewValues(string name, string refno, string statusMsg, string msg, string txn)
        {
            if (radGridViewImportStatus.InvokeRequired)
            {
                // This is a worker thread so delegate the task. 
                radGridViewImportStatus.Invoke(new FillValueImportDelegate(this.FillRadGridViewValues), new object[] { name, refno, statusMsg, msg, txn });
            }
            else
            {
                if (CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qif")
                                  || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qfx")
                                  || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("ofx")
                                  || CommonUtilities.GetInstance().BrowseFileExtension.ToLower().Equals("qbo"))
                {
                    this.Type = CommonUtilities.GetInstance().ImportType;
                }
                try
                {
                    string viewValue = "";
                    string undoValue = "";
                    //Axis 11 pos
                    if (Mappings.GetInstance().ConnectedSoft == Constants.QBstring || Mappings.GetInstance().ConnectedSoft == Constants.Xerostring)
                    {
                        radGridViewImportStatus.Columns[5].IsVisible = true;

                        if (statusMsg == "Imported")
                        {
                            if (Mappings.GetInstance().ConnectedSoft == Constants.Xerostring)
                            {
                                is_imported = true;
                            }
                            viewValue = "view";

                        }
                        else
                        {
                            viewValue = "";
                        }
                        if (statusMsg == "Imported")
                        {
                            undoValue = "undo";

                        }
                        else
                        {
                            undoValue = "";
                        }
                    }
                    else
                    {
                        viewValue = "";
                        undoValue = "";
                    }
                    //Axis 11 pos
                    if (TransactionImporter.rdbQBPOSbutton == true || TransactionImporter.rdbQBOnlinebutton == true)
                    {
                        //  radGridViewImportStatus.Columns[5].IsVisible = false;
                        if (statusMsg == "Imported")
                        {
                            is_imported = true;
                            undoValue = "undo";

                        }
                        else
                        {
                            undoValue = "";
                        }
                        if (statusMsg == "Imported")
                        {
                            viewValue = "view";

                        }
                        else
                        {
                            viewValue = "";
                        }
                    }
                    else
                    {
                        undoValue = "";
                    }
                    if (CommonUtilities.GetInstance().ListMergeType != "")
                    {
                        if (Mappings.GetInstance().ConnectedSoft == Constants.QBstring)
                        {
                            if (refno == "Customer" || refno == "Vendor" || refno == "ItemNonInventory")
                            {
                                radGridViewImportStatus.Rows.Add(refno, "", name, statusMsg, msg, viewValue, undoValue, txn);
                            }
                            else
                            {
                                radGridViewImportStatus.Rows.Add(refno, "", name, statusMsg, msg, "", "", txn);
                            }
                        }
                    }
                    else if (Mappings.GetInstance().ConnectedSoft == Constants.QBstring || Mappings.GetInstance().ConnectedSoft == Constants.Xerostring)
                    {
                        radGridViewImportStatus.Rows.Add(this.Type, refno, name, statusMsg, msg, viewValue, undoValue, txn);
                    }
                    else if (TransactionImporter.rdbQBOnlinebutton == true)
                    {
                        radGridViewImportStatus.Rows.Add(this.Type, refno, name, statusMsg, msg, viewValue, undoValue, txn);
                    }
                    else if (TransactionImporter.rdbQBPOSbutton == true || TransactionImporter.rdbQBOnlinebutton == true)
                    {
                        radGridViewImportStatus.Rows.Add(this.Type, refno, name, statusMsg, msg, undoValue, txn);
                    }

                    if (radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "Class" || 
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "InventorySite" || 
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "ClearedStatus" ||
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "BillingRate" ||
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "ItemGroup" ||
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "Attachment" ||
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "TransferInventory")
                    {
                        radGridViewImportStatus.Columns[5].IsVisible = false;
                    }
                    if (CommonUtilities.GetInstance().ListMergeType != "" ||
                        radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "ClearedStatus")
                    {
                        radbuttonUndoAll.Enabled = false;
                        radGridViewImportStatus.Columns[6].IsVisible = false;
                    }
                    else
                    {
                        if (radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "Items" && TransactionImporter.rdbQBOnlinebutton == true)
                        {
                            radGridViewImportStatus.Columns[6].IsVisible = false;
                        }
                        else if(Mappings.GetInstance().ConnectedSoft == Constants.Xerostring)
                        {
                            radGridViewImportStatus.Columns[5].IsVisible = false;
                            radGridViewImportStatus.Columns[6].IsVisible = false;
                        }
                        else if(Mappings.GetInstance().ConnectedSoft == Constants.QBPOSstring)
                        {
                            radGridViewImportStatus.Columns[5].IsVisible = false;
                        }
                        else
                        {
                            radGridViewImportStatus.Columns[6].IsVisible = true;
                            radbuttonUndoAll.Enabled = true;
                        }
                    }
                }
                catch (Exception e)
                {
                    
                }
            }
        }
        #endregion

        /// <summary>
        /// Undo Single Transaction (on click of Undo linkButton in )
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void radGridViewImportStatus_CellClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
            {
                return;
            }

            importType = radGridViewImportStatus.Rows[0].Cells[0].Value.ToString();

            if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBstring)
            {
                if (importType == "Account" || importType == "Customer" || importType == "Employee" || importType == "Item" ||
                    importType == "OtherName" || importType == "Vendor" || importType == "ItemSalesTax" || 
                    importType == "ItemNonInventory" || importType == "ItemSalesTaxGroup" || importType == "Class" || 
                    importType == "OtherName" || importType == "InventorySite" || importType == "BillingRate" || 
                    importType == "ItemGroup" || importType == "ItemInventory" || importType == "ItemPayment" ||
                    importType == "ItemFixedAsset")
                {
                    txnList = "List";
                }
                else
                {
                    txnList = "Txn";
                }
            }
            
            for (int i = 0; i < radGridViewImportStatus.RowCount; i++)
            {
                if (radGridViewImportStatus.Rows[i].Cells[5].IsSelected == true)
                {
                    importType = radGridViewImportStatus.Rows[i].Cells[0].Value.ToString();
                    //View transaction using TxnDisplayMod or ListDisplayMod
                    //axis 11 pos
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBstring)
                    {
                        tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                        GetTransactionViewRequest(importType, tId, txnList);
                    }
                    else if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBOnlinestring)
                    {
                        tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                        GetOnlineQBOTransactionViewRequest(importType, tId);
                    }
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.Xerostring)
                    {
                        importType = CommonUtilities.GetInstance().Type;

                        tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();

                        if (importType == "Bank")
                        {
                            accountid = radGridViewImportStatus.Rows[i].Cells[8].Value.ToString();
                            GetTransactionViewRequestForXero(importType, tId, accountid);
                        }
                        else
                        {
                            GetTransactionViewRequestForXero(importType, tId, "");
                        }
                    }
                }
                else if (radGridViewImportStatus.Rows[i].Cells[6].IsSelected == true)
                {
                    tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();

                    //Delete transaction from QuickBook using TxnDelRq or ListDelRq
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBstring)
                    {
                        if (importType == "Class")
                        {
                            string parentName = "";
                            int parentNameCount = 0;
                            List<Tuple<string, string>> listParentFullName = new List<Tuple<string, string>>();
                            listParentFullName = CommonUtilities.GetInstance().listParentClass;
                            for (int listIndex = 0; listIndex < (listParentFullName.Count); listIndex++)
                            {
                                if (listParentFullName[listIndex].Item1.Contains(tId))
                                {
                                    parentName = listParentFullName[listIndex].Item2;
                                    break;
                                }
                            }
                            for (int listIndex = 0; listIndex < (listParentFullName.Count); listIndex++)
                            {
                                if (listParentFullName[listIndex].Item2.Contains(parentName))
                                {
                                    parentNameCount += 1;
                                }
                            }
                        
                            listParentFullName = listParentFullName.OrderByDescending(x => x.Item2.Length).ToList();

                            for (int listIndex = 0; listIndex < (listParentFullName.Count); listIndex++)
                            {
                                if (listParentFullName[listIndex].Item2.Contains(parentName))
                                {
                                    if (parentNameCount > 1)
                                    {
                                        deleteFlag = true;
                                        GetTransactionDeleteRequest(importType, listParentFullName[listIndex].Item1, true, txnList);
                                        parentNameCount -= 1;
                                    }
                                    else
                                    {
                                        deleteFlag = false;
                                        GetTransactionDeleteRequest(importType, listParentFullName[listIndex].Item1, true, txnList);
                                    }
                                }
                            }
                        }
                        else if (importType == "InventorySite")
                        {
                            string siteName = "";
                            int parentNameCount = 0;
                            List<Tuple<string, string, string>> listInventorySite = new List<Tuple<string, string, string>>();
                            listInventorySite = CommonUtilities.GetInstance().listParentWithChild;
                            List<Tuple<string, string, string>> listInventorySiteId = new List<Tuple<string, string, string>>();
                            for (int listIndex = 0; listIndex < (listInventorySite.Count); listIndex++)
                            {
                                if (listInventorySite[listIndex].Item1.Contains(tId))
                                {
                                    siteName = listInventorySite[listIndex].Item2;
                                    break;
                                }
                            }
                            for (int listIndex = 0; listIndex < (listInventorySite.Count); listIndex++)
                            {
                                if (listInventorySite[listIndex].Item2.Contains(siteName) || listInventorySite[listIndex].Item3.Contains(siteName))
                                {
                                    parentNameCount += 1;
                                    listInventorySiteId.Add(new Tuple<string, string, string>(listInventorySite[listIndex].Item1, listInventorySite[listIndex].Item2, listInventorySite[listIndex].Item3));
                                }
                            }

                            listInventorySiteId = listInventorySiteId.OrderByDescending(x => x.Item3.Length).ToList();

                            for (int listIndex = 0; listIndex < (listInventorySiteId.Count); listIndex++)
                            {
                                if (parentNameCount > 1)
                                {
                                    deleteFlag = true;
                                    GetTransactionDeleteRequest(importType, listInventorySiteId[listIndex].Item1, true, txnList);
                                    parentNameCount -= 1;
                                }
                                else
                                {
                                    deleteFlag = false;
                                    GetTransactionDeleteRequest(importType, listInventorySiteId[listIndex].Item1, true, txnList);
                                }
                            }
                        }
                        else
                        {
                            deleteFlag = false;
                            GetTransactionDeleteRequest(importType, tId, true, txnList);
                        }
                    }
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.Xerostring)
                    {
                        importType = CommonUtilities.GetInstance().Type;
                        tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                        GetTransactionDeleteRequestbyXero(importType, tId, true);
                    }
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBPOSstring)
                    {
                        if (importType == "Customer" || importType == "Employee" || importType == "ItemInventory" ||
                            importType == "TimeEntry" || importType == "SalesReceipt" || importType == "TransferSlip" ||
                            importType == "InventoryQtyAdjustment" || importType == "InventoryCostAdjustment" || importType == "Voucher" ||
                            importType == "SalesOrder" || importType == "PurchaseOrder" || importType == "PriceDiscount" ||
                                importType == "PriceAdjustment" || importType == "CustomFieldList")
                        {
                            txnList = "List";
                        }
                        else
                        {
                            txnList = "Txn";
                        }
                        GetPOSTransactionDeleteRequest(importType, tId, true, txnList);
                    }
                    if (TransactionImporter.rdbQBOnlinebutton == true)
                    {

                        if (importType == "SalesReceipt")
                        {
                           await GetOnlineTransactionDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Bill")
                        {
                            await GetOnlineBillTransactionDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "BillPayment")
                        {
                            await GetOnlineBillPaymentTransactionDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "JournalEntry")
                        {
                            await GetOnlineJournalTransactionDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Invoice")
                        {
                            await GetOnlineInvoiceDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "TimeActivity")
                        {
                            await GetOnlineTimeActivityDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "PurchaseOrder")
                        {
                            await GetOnlinePurchaseOrderDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Estimate")
                        {
                            await GetOnlineEstimateDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Payment")
                        {
                            await GetOnlinePaymentDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "VendorCredit")
                        {
                            await GetOnlineVendorCreditDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "CreditMemo")
                        {
                            await GetOnlineCreditMemoDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "CashPurchase" || importType == "CheckPurchase" || importType == "CreditCardPurchase")
                        {
                            await GetOnlinePurchaseDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Employee")
                        {
                            await GetOnlineEmployeeDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Customer")
                        {
                            await GetOnlineCustomerDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Deposit")
                        {
                            await GetOnlineDepositDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Account")
                        {
                            await GetOnlineAccountDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "RefundReceipt")
                        {
                            await GetOnlineRefundReceiptDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Transfer")
                        {
                            await GetOnlineTransferDeleteRequest(importType, tId, true);
                        }
                        else if (importType == "Attachment")
                        {
                            await GetOnlineAttachmentDeleteRequest(importType, tId, true);
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Changes background color of error row to light yellow
        /// </summary>
        private void radGridViewImportStatus_RowFormatting(object sender, Telerik.WinControls.UI.RowFormattingEventArgs e)
        {
            if (e.RowElement.RowInfo.Cells[3] != null)
            {
                int index = e.RowElement.RowInfo.Index;
                if (e.RowElement.RowInfo.Cells[3].Value.ToString() == "Error")
                {
                    e.RowElement.DrawFill = true;
                    e.RowElement.GradientStyle = GradientStyles.Solid;
                    e.RowElement.BackColor = Color.LightYellow;
                }
                else
                {
                    if (index % 2 == 0)
                    {
                        e.RowElement.ResetValue(LightVisualElement.BackColorProperty, ValueResetFlags.Local);
                        e.RowElement.ResetValue(LightVisualElement.GradientStyleProperty, ValueResetFlags.Local);
                        e.RowElement.ResetValue(LightVisualElement.DrawFillProperty, ValueResetFlags.Local);

                    }
                    else
                    {
                        e.RowElement.DrawFill = true;
                        e.RowElement.GradientStyle = GradientStyles.Solid;
                        //e.RowElement.BackColor = Color.LightSteelBlue;
                    }
                }
            }
        }

        #region Change Status of import row
        public string ChangeStatus(string status, string transID, string errorMsg)
        {
            for (int i = 0; i < radGridViewImportStatus.RowCount; i++)
            {
                if (radGridViewImportStatus.Rows[i].Cells[3].Value.ToString() == "Pending")
                {
                    System.Threading.Thread.Sleep(1500);
                    radGridViewImportStatus.Rows[i].Cells[3].Value = status;
                    radGridViewImportStatus.Rows[i].Cells[7].Value = transID;
                    if (status == "Error")
                    {
                        radGridViewImportStatus.Rows[i].Cells[4].Value = errorMsg;
                        CommonUtilities.GetInstance().getError = string.Empty;
                    }
                    else if (status == "Imported")
                    {
                        is_imported = true;
                        radGridViewImportStatus.Rows[i].Cells[4].Value = "";
                        if (CommonUtilities.GetInstance().ListMergeType != "")
                        {
                            string transType = "";
                            transType = radGridViewImportStatus.Rows[i].Cells[0].Value.ToString();
                            if (transType == "Customer" || transType == "Vendor" || transType == "ItemNonInventory")
                            {
                                radGridViewImportStatus.Rows[i].Cells[5].Value = "view";
                            }
                            else
                            {
                                radGridViewImportStatus.Rows[i].Cells[5].Value = "";
                            }
                        }
                        else
                        {
                            radGridViewImportStatus.Rows[i].Cells[5].Value = "view";
                        }
                        radGridViewImportStatus.Rows[i].Cells[6].Value = "undo";
                    }
                }
            }
            return status;
        }

        /// <summary>
        ///  This function is written for bank transcation for xero.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public string ChangeStatusforbank(string status, string transID, string accountid, string errorMsg)
        {
            for (int i = 0; i < radGridViewImportStatus.RowCount; i++)
            {
                if (radGridViewImportStatus.Rows[i].Cells[3].Value.ToString() == "Pending")
                {
                    System.Threading.Thread.Sleep(1500);
                    radGridViewImportStatus.Rows[i].Cells[3].Value = status;
                    radGridViewImportStatus.Rows[i].Cells[7].Value = transID;
                    radGridViewImportStatus.Rows[i].Cells[8].Value = accountid;
                    if (status == "Error")
                    {
                        radGridViewImportStatus.Rows[i].Cells[4].Value = errorMsg;
                    }
                    else if (status == "Imported")
                    {
                        //bug 499
                        is_imported = true;
                        radGridViewImportStatus.Rows[i].Cells[4].Value = "";
                        radGridViewImportStatus.Rows[i].Cells[5].Value = "view";
                        radGridViewImportStatus.Rows[i].Cells[6].Value = "undo";
                    }
                }
            }
            return status;
        }
        #endregion

        #endregion

        #region View and Undo for Quickbooks and Xero
        public static void GetTransaction(string txID, string TxnList)
        {
            //Hashtable TransTypeTable = new Hashtable();            
            XmlDocument requestXmlDoc = new XmlDocument();
            string statusMessage = string.Empty;
            string sb = string.Empty;
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create QueryRq aggregate and fill in field values for it
            XmlElement QueryRq = requestXmlDoc.CreateElement(TxnList + "TransactionQueryRq");
            inner.AppendChild(QueryRq);



            //Create aggregate and fill in field values for it
            XmlElement txnId = requestXmlDoc.CreateElement(TxnList + "ID");
            txnId.InnerText = txID;
            QueryRq.AppendChild(txnId);


            //Create aggregate and fill in field values for it
            //XmlElement refNo= requestXmlDoc.CreateElement(TxnList + "RefNumber");
            //refNo.InnerText = RefNo;
            //QueryRq.AppendChild(refNo);

            string resp = string.Empty;
            try
            {
                //Sending request for checking Vendor in QuickBooks.
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                //Getting response.
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);

                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TxnList + "DisplayModRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            statusMessage += "\n";
                            statusMessage += "Record does not exist";
                            CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                        }

                    }
                }
            }
            if (resp == string.Empty)
            {
                sb += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofqbXML(requestXmlDoc.OuterXml);
            }
        }

        public void GetTransactionDeleteRequest(string TranType, string txID, bool f, string TxnList)
        {
            //Hashtable TransTypeTable = new Hashtable();                      
            XmlDocument requestXmlDoc = new XmlDocument();
            string statusMessage = string.Empty;
            string sb = string.Empty;
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create QueryRq aggregate and fill in field values for it
            XmlElement QueryRq = requestXmlDoc.CreateElement(TxnList + "DelRq");
            inner.AppendChild(QueryRq);

            //Create aggregate and fill in field values for it
            XmlElement type = requestXmlDoc.CreateElement(TxnList + "DelType");
            type.InnerText = TranType;
            QueryRq.AppendChild(type);

            //Create aggregate and fill in field values for it
            XmlElement txnId = requestXmlDoc.CreateElement(TxnList + "ID");
            txnId.InnerText = txID;
            QueryRq.AppendChild(txnId);

            string resp = string.Empty;
            try
            {
                if (TransactionImporter.rdbdesktopbutton == true)
                {
                    //Sending request for checking Vendor in QuickBooks.
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    //Getting response.

                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);
                   
                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TxnList + "DelRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            //statusMessage += "\n";
                            //statusMessage += "Record does not exist.It is already deleted from QuickBooks.";
                        }
                        else if (statusSeverity == "Info")
                        {
                            if (deleteFlag == false)
                            {
                                statusMessage += "Record Deleted Successfully from QuickBooks";
                                CommonUtilities.GetInstance().succesmg = statusMessage;
                                CommonUtilities.GetInstance().barstatusflag1 = true;
                                deleteRecordFlag = true;
                            }
                        }
                        if (f == true)
                        {
                            if (deleteFlag == false)
                            {
                                if (deleteRecordFlag == true)
                                {
                                    statusMessage = "Record Deleted Successfully from QuickBooks";
                                    CommonUtilities.GetInstance().succesmg = statusMessage;
                                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                                    deleteRecordFlag = false;
                                }
                                else
                                {
                                    CommonUtilities.GetInstance().succesmg = statusMessage;
                                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                                }
                                CommonUtilities.GetInstance().barstatusflag1 = true;
                            }
                        }
                        else
                        {
                            ImportSummary.GetInstance().barstatusflag = false;
                        }
                    }
                }

            }

            if (resp == string.Empty)
            {
                sb += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofqbXML(requestXmlDoc.OuterXml);
            }
        }

        public void GetPOSTransactionDeleteRequest(string TranType, string txID, bool f, string TxnList)
        {
            //Hashtable TransTypeTable = new Hashtable();                      
            XmlDocument requestXmlDoc = new XmlDocument();
            string statusMessage = string.Empty;
            string sb = string.Empty;
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbposxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBPOSXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBPOSXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create QueryRq aggregate and fill in field values for it
            XmlElement QueryRq = requestXmlDoc.CreateElement(TxnList + "DelRq");
            inner.AppendChild(QueryRq);

            //Create aggregate and fill in field values for it
            XmlElement type = requestXmlDoc.CreateElement(TxnList + "DelType");
            if (TranType == "SalesReceipt")
            {
                type.InnerText = "HeldSalesReceipt";
            }
            else if (TranType == "InventoryCostAdjustment")
            {
                type.InnerText = "HeldInventoryCostAdjustment";
            }
            else if (TranType == "TransferSlip")
            {
                type.InnerText = "HeldTransferSlip";
            }
            else if (TranType == "InventoryQtyAdjustment")
            {
                type.InnerText = "HeldInventoryQtyAdjustment";
            }
            else if (TranType == "Voucher")
            {
                type.InnerText = "HeldVoucher";
            }
            else
            {
                type.InnerText = TranType;
            }
            QueryRq.AppendChild(type);

            //Create aggregate and fill in field values for it
            XmlElement txnId = requestXmlDoc.CreateElement(TxnList + "ID");
            txnId.InnerText = txID;
            QueryRq.AppendChild(txnId);

            string resp = string.Empty;
            try
            {
                if (TransactionImporter.rdbQBPOSbutton == true)
                {

                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
                }
                else
                {
                    resp = CommonUtilities.GetInstance().QBPOSRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().ticketvalue, requestXmlDoc.OuterXml);
                }
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);

                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBPOSXML/QBPOSXMLMsgsRs/" + TxnList + "DelRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            statusMessage += "\n";
                            statusMessage += "Record does not exist.It is already deleted from QuickBooks.";
                        }
                        else if (statusSeverity == "Info")
                        {
                            statusMessage += "Record Deleted Successfully from QuickBooks";
                            CommonUtilities.GetInstance().succesmg = statusMessage;
                            CommonUtilities.GetInstance().barstatusflag1 = true;
                        }
                        if (f == true)
                        {
                            CommonUtilities.GetInstance().succesmg = statusMessage;
                            CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            CommonUtilities.GetInstance().barstatusflag1 = true;
                        }
                        else
                        {
                            CommonUtilities.GetInstance().barstatusflag1 = false;
                        }

                        CommonUtilities.GetInstance().succesmg = statusMessage;
                    }
                }
            }

            if (resp == string.Empty)
            {
                sb += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofqbXML(requestXmlDoc.OuterXml);
            }
        }

        public static void GetTransactionViewRequest(string TranType, string txID, string TxnList)
        {
            //Hashtable TransTypeTable = new Hashtable();            
            XmlDocument requestXmlDoc = new XmlDocument();
            string statusMessage = string.Empty;
            string sb = string.Empty;
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create QueryRq aggregate and fill in field values for it
            XmlElement QueryRq = requestXmlDoc.CreateElement(TxnList + "DisplayModRq");
            inner.AppendChild(QueryRq);

            //Create aggregate and fill in field values for it
            XmlElement type = requestXmlDoc.CreateElement(TxnList + "DisplayModType");

            //Axis 646
            if (TranType == "ItemNonInventory" || TranType == "ItemSalesTaxGroup" || TranType == "ItemInventory" || TranType == "ItemPayment" ||
                TranType == "ItemFixedAsset")
            {
                TranType = "Item";
            }
            //Axis 646 END

            type.InnerText = TranType;
            QueryRq.AppendChild(type);

            //Create aggregate and fill in field values for it
            XmlElement txnId = requestXmlDoc.CreateElement(TxnList + "ID");
            //  XmlElement type = requestXmlDoc.CreateElement(TxnList+ "ModRq");
            txnId.InnerText = txID;
            QueryRq.AppendChild(txnId);

            string resp = string.Empty;
            try
            {
                //Sending request for checking Vendor in QuickBooks.
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                //Getting response.
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);

                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + TxnList + "DisplayModRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        if (statusSeverity == "Error")
                        {
                            statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            statusMessage += "\n";
                            statusMessage += "Record does not exist";
                            CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                        }
                        else if (statusSeverity == "Info")
                        {
                            statusMessage += "Record is already exists in Quickbook.";
                            sucess = true;
                        }

                    }
                }
            }
            if (resp == string.Empty)
            {
                sb += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofqbXML(requestXmlDoc.OuterXml);
            }
        }
        //602

        public static void GetOnlineQBOTransactionViewRequest(string TransType, string txID)
        {
            string url = string.Empty;
            if (TransType == "JournalEntry") TransType = "journal";
            else if (TransType == "Vendor") TransType = "vendordetail";
            else if (TransType == "Customer") TransType = "customerdetail";
            else if (TransType == "Payment") TransType = "recvpayment";
            else if (TransType == "CheckPurchase" || TransType == "CashPurchase" || TransType == "CreditCardPurchase") TransType = "expense";
            else
                TransType = TransType.ToLower();
            if (TransType == "vendordetail" || TransType == "customerdetail")
            {
                url = "https://qbo.intuit.com/app/" + TransType + "?nameId=" + txID;
            }
            else if (TransType == "timeactivity")
            {
                url = "https://qbo.intuit.com/app/" + TransType + "?id=" + txID;
            }
            else
            {
                url = "https://qbo.intuit.com/app/" + TransType + "?txnId=" + txID;
            }
            System.Diagnostics.Process.Start(url);
        }
        public string GetTransactionInformation(string TranType, string refNumber, DateTime fromDate, DateTime toDate, string accountType, string accountToImport, string payment)
        {
            txnId = string.Empty;
            amount = 0.0;
            finalResult = txnId = string.Empty;
            if (payment != null)
            {
                payment = payment.Trim();
            }

            //string matchCrieterion = "Contains";

            // string accountName = string.Empty;

            //Hashtable TransTypeTable = new Hashtable();            
            XmlDocument requestXmlDoc = new XmlDocument();
            string statusMessage = string.Empty;
            string sb = string.Empty;
            //Add the prolog processing instructions
            requestXmlDoc.AppendChild(requestXmlDoc.CreateXmlDeclaration("1.0", "ISO-8859-1", null));
            requestXmlDoc.AppendChild(requestXmlDoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));

            //Create the outer request envelope tag
            XmlElement outer = requestXmlDoc.CreateElement("QBXML");
            requestXmlDoc.AppendChild(outer);

            //Create the inner request envelope & any needed attributes
            XmlElement inner = requestXmlDoc.CreateElement("QBXMLMsgsRq");
            outer.AppendChild(inner);
            inner.SetAttribute("onError", "stopOnError");

            //Create QueryRq aggregate and fill in field values for it
            XmlElement QueryRq = requestXmlDoc.CreateElement("TransactionQueryRq");
            inner.AppendChild(QueryRq);
            QueryRq.SetAttribute("requestID", "1");

            //XmlElement TxnID = requestXmlDoc.CreateElement("TxnID");
            //QueryRq.AppendChild(TxnID);
            //TxnID.InnerText = txnId.ToString();

            //XmlElement RefNumberFilter = requestXmlDoc.CreateElement("RefNumberFilter");
            //QueryRq.AppendChild(RefNumberFilter);

            //XmlElement matchCrieteria = requestXmlDoc.CreateElement("MatchCriterion");
            //RefNumberFilter.AppendChild(matchCrieteria).InnerText = matchCrieterion.ToString();

            //XmlElement RefNumber = requestXmlDoc.CreateElement("RefNumber");
            //RefNumberFilter.AppendChild(RefNumber).InnerText = refNumber.ToString();


            //XmlElement ModifiedDateRangeFilter = requestXmlDoc.CreateElement("TransactionModifiedDateRangeFilter");
            //QueryRq.AppendChild(ModifiedDateRangeFilter);
            //XmlElement FromModifiedDate1 = requestXmlDoc.CreateElement("FromModifiedDate");
            //ModifiedDateRangeFilter.AppendChild(FromModifiedDate1).InnerText = fromDate.ToString("yyyy-MM-dd");
            //XmlElement ToModifiedDate1 = requestXmlDoc.CreateElement("ToModifiedDate");
            //ModifiedDateRangeFilter.AppendChild(ToModifiedDate1).InnerText = toDate.ToString("yyyy-MM-dd");

            XmlElement DateRangeFilter = requestXmlDoc.CreateElement("TransactionDateRangeFilter");
            QueryRq.AppendChild(DateRangeFilter);
            XmlElement FromModifiedDate = requestXmlDoc.CreateElement("FromTxnDate");
            DateRangeFilter.AppendChild(FromModifiedDate).InnerText = fromDate.ToString("yyyy-MM-dd");
            XmlElement ToModifiedDate = requestXmlDoc.CreateElement("ToTxnDate");
            DateRangeFilter.AppendChild(ToModifiedDate).InnerText = toDate.ToString("yyyy-MM-dd");


            XmlElement TranAccountFilter = requestXmlDoc.CreateElement("TransactionAccountFilter");
            QueryRq.AppendChild(TranAccountFilter);
            //XmlElement AccountType = requestXmlDoc.CreateElement("AccountTypeFilter");
            //TranAccountFilter.AppendChild(AccountType).InnerText = accountType.ToString();

            XmlElement AccountFullName = requestXmlDoc.CreateElement("FullName");
            TranAccountFilter.AppendChild(AccountFullName).InnerText = accountToImport.ToString();


            //XmlElement TranAccFilter = requestXmlDoc.CreateElement("TransactionAccountFilter");
            //QueryRq.AppendChild(TranAccFilter);
            //XmlElement AccountType = requestXmlDoc.CreateElement("AccountTypeFilter");
            //TranAccFilter.AppendChild(AccountType).InnerText = accountType.ToString();

            XmlElement TranTypeFilter = requestXmlDoc.CreateElement("TransactionTypeFilter");
            QueryRq.AppendChild(TranTypeFilter);
            XmlElement TxnType = requestXmlDoc.CreateElement("TxnTypeFilter");
            TranTypeFilter.AppendChild(TxnType).InnerText = TranType.ToString();

            //XmlElement TxnDetail = requestXmlDoc.CreateElement("TransactionDetailLevelFilter");
            //QueryRq.AppendChild(TxnDetail);
            //TxnDetail.InnerText = txnDetail.ToString();

            string resp = string.Empty;
            string oldSessionTicket = string.Empty;
            string newSessionTicket = string.Empty;
            int amtFlag = 0;
            int txnIDFlag = 0;
            int accountFlag = 0;

            try
            {
                if (CommonUtilities.GetInstance().QBSessionTicket == null)
                {
                    oldSessionTicket = "";
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    newSessionTicket = CommonUtilities.GetInstance().QBSessionTicket;

                }
                else
                {
                    oldSessionTicket = CommonUtilities.GetInstance().QBSessionTicket;
                    CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                    CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(CommonUtilities.GetInstance().CompanyFile, Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                    newSessionTicket = CommonUtilities.GetInstance().QBSessionTicket;
                }

                //Getting response.
                //if(resp =="")
                if (!oldSessionTicket.Equals(newSessionTicket))
                    resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, requestXmlDoc.OuterXml);
            }
            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                if (resp != string.Empty)
                {
                    System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                    outputXMLDoc.LoadXml(resp);


                    XmlNodeList amountList = outputXMLDoc.GetElementsByTagName("TransactionRet");


                    XmlNode amountnode = outputXMLDoc.SelectSingleNode("//Amount");
                    XmlNode txnIDnode = outputXMLDoc.SelectSingleNode("//TxnID");
                    XmlNodeList accountRef = outputXMLDoc.SelectNodes("//AccountRef");

                    accountName = string.Empty;

                    if (amountList.Count != 0)
                    {

                        foreach (XmlNode node in amountList)
                        {

                            XmlElement companyElement = (XmlElement)node;

                            if (txnIDnode != null || amountnode != null || accountRef.Count != 0)
                            {


                                string amt = companyElement.GetElementsByTagName("Amount")[0].InnerText.ToString();

                                if (amt != "" && amt.Equals(payment))
                                {

                                    amount = Convert.ToDouble(companyElement.GetElementsByTagName("Amount")[0].InnerText.ToString());
                                    amtFlag++;

                                }
                                if (companyElement.GetElementsByTagName("TxnID")[0].InnerText.ToString() != "" && companyElement.GetElementsByTagName("Amount")[0].InnerText.ToString().Equals(payment))
                                {
                                    if (txnIDFlag < 1)
                                    {
                                        txnId = companyElement.GetElementsByTagName("TxnID")[0].InnerText.ToString();
                                        txnIDFlag++;
                                    }
                                }

                                foreach (XmlNode node1 in accountRef)
                                {
                                    //
                                    if (node1["FullName"].InnerText != "" && node1["FullName"].InnerText.Equals(accountToImport))
                                    {
                                        if (accountFlag < 1)
                                        {
                                            accountName = node1["FullName"].InnerText;
                                            accountFlag++;

                                        }
                                    }

                                }

                                finalResult = txnId + ":" + amount.ToString() + ":" + accountName;
                            }
                            else
                                finalResult = " " + ":" + amount.ToString() + ":" + " ";

                        }
                    }
                    else
                        finalResult = " " + ":" + amount.ToString() + ":" + " ";



                    XmlNodeList dataNodes = outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + "TransactionQueryRs");

                    foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/" + "TransactionQueryRs"))
                    {
                        string statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();


                        if (statusSeverity == "Error")
                        {
                            statusMessage += oNode.Attributes["statusMessage"].Value.ToString();
                            statusMessage += "\n";
                            statusMessage += "Record does not exist";
                            CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                        }
                        else if (statusSeverity == "Info")
                        {
                            statusMessage += "Record found in Quickbook.";
                            sucess = true;
                        }

                    }
                }
                else
                {
                    finalResult = "abcd" + ":" + amount.ToString() + ":" + "abcd";
                }

            }

            if (resp == string.Empty)
            {
                sb += DataProcessingBlocks.CommonUtilities.GetInstance().ValidMessageofqbXML(requestXmlDoc.OuterXml);
            }

            return finalResult;
        }

        #region Undo methods for Online
        public async System.Threading.Tasks.Task GetOnlineTransactionDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.SalesReceipt> dataSalesReceipt = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.SalesReceipt> customerQueryService = new QueryService<Intuit.Ipp.Data.SalesReceipt>(serviceContext);

            dataSalesReceipt = new ReadOnlyCollection<Intuit.Ipp.Data.SalesReceipt>(customerQueryService.ExecuteIdsQuery("select * from SalesReceipt where Id= '" + txID + "'"));
            Intuit.Ipp.Data.SalesReceipt SalesReceipt = new Intuit.Ipp.Data.SalesReceipt();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();

            if (dataSalesReceipt.Count > 0)
            {
                foreach (var sales in dataSalesReceipt)
                {
                    SalesReceipt.Id = sales.Id;
                    SalesReceipt.SyncToken = sales.SyncToken;
                    if (sales.Line != null)
                    {
                        foreach (var deleteLine in sales.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    SalesReceipt.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    Intuit.Ipp.Data.SalesReceipt salesReceiptAdded = dataService.Delete<Intuit.Ipp.Data.SalesReceipt>(SalesReceipt);

                    if (salesReceiptAdded != null)
                    {
                        if (salesReceiptAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataSalesReceipt.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
            #endregion
        }

        public async System.Threading.Tasks.Task GetOnlineTimeActivityDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.TimeActivity> dataTimeActivity = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.TimeActivity> customerQueryService = new QueryService<Intuit.Ipp.Data.TimeActivity>(serviceContext);

            dataTimeActivity = new ReadOnlyCollection<Intuit.Ipp.Data.TimeActivity>(customerQueryService.ExecuteIdsQuery("select * from TimeActivity where Id= '" + txID + "'"));
            Intuit.Ipp.Data.TimeActivity TimeActivity = new Intuit.Ipp.Data.TimeActivity();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataTimeActivity.Count > 0)
            {
                foreach (var timeActivity in dataTimeActivity)
                {
                    TimeActivity.Id = timeActivity.Id;
                    TimeActivity.SyncToken = timeActivity.SyncToken;

                }
                try
                {
                    Intuit.Ipp.Data.TimeActivity timeActivityAdded = dataService.Delete<Intuit.Ipp.Data.TimeActivity>(TimeActivity);
                    if (timeActivityAdded != null)
                    {
                        if (timeActivityAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataTimeActivity.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }



            #endregion
        }

        public async System.Threading.Tasks.Task GetOnlineBillTransactionDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Bill> dataBill = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Bill> customerQueryService = new QueryService<Intuit.Ipp.Data.Bill>(serviceContext);

            dataBill = new ReadOnlyCollection<Intuit.Ipp.Data.Bill>(customerQueryService.ExecuteIdsQuery("select * from Bill where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Bill Bill = new Intuit.Ipp.Data.Bill();
            Intuit.Ipp.Data.Bill BillAdded = null;
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();

            if (dataBill.Count > 0)
            {
                foreach (var bills in dataBill)
                {
                    Bill.Id = bills.Id;
                    Bill.SyncToken = bills.SyncToken;

                    if (bills.Line != null)
                    {
                        foreach (var deleteLine in bills.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    Bill.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    BillAdded = dataService.Delete<Intuit.Ipp.Data.Bill>(Bill);
                    if (BillAdded != null)
                    {
                        if (BillAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataBill.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }


            #endregion
        }

        public async System.Threading.Tasks.Task GetOnlineBillPaymentTransactionDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.BillPayment> dataBill = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.BillPayment> customerQueryService = new QueryService<Intuit.Ipp.Data.BillPayment>(serviceContext);

            dataBill = new ReadOnlyCollection<Intuit.Ipp.Data.BillPayment>(customerQueryService.ExecuteIdsQuery("select * from BillPayment where Id= '" + txID + "'"));
            Intuit.Ipp.Data.BillPayment Bill = new Intuit.Ipp.Data.BillPayment();
            Intuit.Ipp.Data.BillPayment BillAdded = null;
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataBill.Count > 0)
            {
                foreach (var bills in dataBill)
                {
                    Bill.Id = bills.Id;
                    Bill.SyncToken = bills.SyncToken;

                    if (bills.Line != null)
                    {
                        foreach (var deleteLine in bills.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    Bill.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    BillAdded = dataService.Delete<Intuit.Ipp.Data.BillPayment>(Bill);
                    if (BillAdded != null)
                    {
                        if (BillAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataBill.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
            #endregion
        }

        public async System.Threading.Tasks.Task GetOnlineJournalTransactionDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.JournalEntry> dataJournalEntry = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.JournalEntry> JournalEntryQueryService = new QueryService<Intuit.Ipp.Data.JournalEntry>(serviceContext);

            dataJournalEntry = new ReadOnlyCollection<Intuit.Ipp.Data.JournalEntry>(JournalEntryQueryService.ExecuteIdsQuery("select * from JournalEntry where Id= '" + txID + "'"));
            Intuit.Ipp.Data.JournalEntry JournalEntry = new Intuit.Ipp.Data.JournalEntry();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();

            if (dataJournalEntry.Count > 0)
            {
                foreach (var Entry in dataJournalEntry)
                {
                    JournalEntry.SyncToken = Entry.SyncToken;
                    JournalEntry.Id = Entry.Id;

                    if (Entry.Line != null)
                    {
                        foreach (var deleteLine in Entry.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    JournalEntry.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }
                try
                {
                    Intuit.Ipp.Data.JournalEntry JournalEntryAdded = dataService.Delete<Intuit.Ipp.Data.JournalEntry>(JournalEntry);
                    if (JournalEntryAdded != null)
                    {
                        if (JournalEntryAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataJournalEntry.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
            #endregion

        }

        public async System.Threading.Tasks.Task GetOnlineInvoiceDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Invoice> dataInvoice = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Invoice> customerQueryService = new QueryService<Intuit.Ipp.Data.Invoice>(serviceContext);

            dataInvoice = new ReadOnlyCollection<Intuit.Ipp.Data.Invoice>(customerQueryService.ExecuteIdsQuery("select * from Invoice where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Invoice Invoice = new Intuit.Ipp.Data.Invoice();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataInvoice.Count > 0)
            {
                foreach (var invoice in dataInvoice)
                {
                    Invoice.Id = invoice.Id;
                    Invoice.SyncToken = invoice.SyncToken;

                    if (invoice.Line != null)
                    {
                        foreach (var deleteLine in invoice.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    Invoice.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }
                try
                {
                    Intuit.Ipp.Data.Invoice invoiceAdded = dataService.Delete<Intuit.Ipp.Data.Invoice>(Invoice);
                    if (invoiceAdded != null)
                    {
                        if (invoiceAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataInvoice.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlinePurchaseOrderDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.PurchaseOrder> dataInvoice = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.PurchaseOrder> customerQueryService = new QueryService<Intuit.Ipp.Data.PurchaseOrder>(serviceContext);

            dataInvoice = new ReadOnlyCollection<Intuit.Ipp.Data.PurchaseOrder>(customerQueryService.ExecuteIdsQuery("select * from PurchaseOrder where Id= '" + txID + "'"));
            Intuit.Ipp.Data.PurchaseOrder purchaseOrder = new Intuit.Ipp.Data.PurchaseOrder();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataInvoice.Count > 0)
            {
                foreach (var POrder in dataInvoice)
                {
                    purchaseOrder.Id = POrder.Id;
                    purchaseOrder.SyncToken = POrder.SyncToken;

                    if (POrder.Line != null)
                    {
                        foreach (var deleteLine in POrder.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    purchaseOrder.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }
                try
                {
                    Intuit.Ipp.Data.PurchaseOrder POrderAdded = dataService.Delete<Intuit.Ipp.Data.PurchaseOrder>(purchaseOrder);
                    if (POrderAdded != null)
                    {
                        if (POrderAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataInvoice.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlineVendorCreditDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.VendorCredit> dataInvoice = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.VendorCredit> customerQueryService = new QueryService<Intuit.Ipp.Data.VendorCredit>(serviceContext);

            dataInvoice = new ReadOnlyCollection<Intuit.Ipp.Data.VendorCredit>(customerQueryService.ExecuteIdsQuery("select * from VendorCredit where Id= '" + txID + "'"));
            Intuit.Ipp.Data.VendorCredit vendorCredit = new Intuit.Ipp.Data.VendorCredit();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataInvoice.Count > 0)
            {
                foreach (var POrder in dataInvoice)
                {
                    vendorCredit.Id = POrder.Id;
                    vendorCredit.SyncToken = POrder.SyncToken;
                    if (POrder.Line != null)
                    {
                        foreach (var deleteLine in POrder.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    vendorCredit.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    Intuit.Ipp.Data.VendorCredit vendorCreditDelete = dataService.Delete<Intuit.Ipp.Data.VendorCredit>(vendorCredit);
                    if (vendorCreditDelete != null)
                    {
                        if (vendorCreditDelete.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataInvoice.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlineEstimateDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Estimate> dataEstimate = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Estimate> customerQueryService = new QueryService<Intuit.Ipp.Data.Estimate>(serviceContext);

            dataEstimate = new ReadOnlyCollection<Intuit.Ipp.Data.Estimate>(customerQueryService.ExecuteIdsQuery("select * from Estimate where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Estimate Estimate = new Intuit.Ipp.Data.Estimate();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataEstimate.Count > 0)
            {
                foreach (var estimate in dataEstimate)
                {
                    Estimate.Id = estimate.Id;
                    Estimate.SyncToken = estimate.SyncToken;

                    if (estimate.Line != null)
                    {
                        foreach (var deleteLine in estimate.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    Estimate.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }
                try
                {
                    Intuit.Ipp.Data.Estimate estimateAdded = dataService.Delete<Intuit.Ipp.Data.Estimate>(Estimate);
                    if (estimateAdded != null)
                    {
                        if (estimateAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataEstimate.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlinePurchaseDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Purchase> dataPurchase = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Purchase> customerQueryService = new QueryService<Intuit.Ipp.Data.Purchase>(serviceContext);

            dataPurchase = new ReadOnlyCollection<Intuit.Ipp.Data.Purchase>(customerQueryService.ExecuteIdsQuery("select * from Purchase where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Purchase Purchase = new Intuit.Ipp.Data.Purchase();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataPurchase.Count > 0)
            {
                foreach (var purchase in dataPurchase)
                {
                    Purchase.Id = purchase.Id;
                    Purchase.SyncToken = purchase.SyncToken;

                    if (purchase.Line != null)
                    {
                        foreach (var deleteLine in purchase.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    Purchase.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    Intuit.Ipp.Data.Purchase purchaseAdded = dataService.Delete<Intuit.Ipp.Data.Purchase>(Purchase);
                    if (purchaseAdded != null)
                    {
                        if (purchaseAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataPurchase.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlinePaymentDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Payment> dataPayment = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Payment> PaymentQueryService = new QueryService<Intuit.Ipp.Data.Payment>(serviceContext);

            dataPayment = new ReadOnlyCollection<Intuit.Ipp.Data.Payment>(PaymentQueryService.ExecuteIdsQuery("select * from Payment where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Payment Payment = new Intuit.Ipp.Data.Payment();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataPayment.Count > 0)
            {
                foreach (var pay in dataPayment)
                {
                    Payment.Id = pay.Id;
                    Payment.SyncToken = pay.SyncToken;

                    if (pay.Line != null)
                    {
                        foreach (var deleteLine in pay.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    Payment.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    Intuit.Ipp.Data.Payment PaymentAdded = dataService.Delete<Intuit.Ipp.Data.Payment>(Payment);
                    if (PaymentAdded != null)
                    {
                        if (PaymentAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataPayment.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlineCreditMemoDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.CreditMemo> dataCreditMemo = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.CreditMemo> CreditMemoQueryService = new QueryService<Intuit.Ipp.Data.CreditMemo>(serviceContext);

            dataCreditMemo = new ReadOnlyCollection<Intuit.Ipp.Data.CreditMemo>(CreditMemoQueryService.ExecuteIdsQuery("select * from CreditMemo where Id= '" + txID + "'"));
            Intuit.Ipp.Data.CreditMemo CreditMemo = new Intuit.Ipp.Data.CreditMemo();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataCreditMemo.Count > 0)
            {
                foreach (var CMemo in dataCreditMemo)
                {
                    CreditMemo.Id = CMemo.Id;
                    CreditMemo.SyncToken = CMemo.SyncToken;

                    if (CMemo.Line != null)
                    {
                        foreach (var deleteLine in CMemo.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    CreditMemo.Line = new Intuit.Ipp.Data.Line[] { Line1 };

                }

                try
                {
                    Intuit.Ipp.Data.CreditMemo CreditMemoAdded = dataService.Delete<Intuit.Ipp.Data.CreditMemo>(CreditMemo);
                    if (CreditMemoAdded != null)
                    {
                        if (CreditMemoAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataCreditMemo.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlineEmployeeDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Employee> dataEmployee = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Employee> EmployeeQueryService = new QueryService<Intuit.Ipp.Data.Employee>(serviceContext);

            dataEmployee = new ReadOnlyCollection<Intuit.Ipp.Data.Employee>(EmployeeQueryService.ExecuteIdsQuery("select * from Employee where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Employee Employee = new Intuit.Ipp.Data.Employee();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataEmployee.Count > 0)
            {
                foreach (var Emp in dataEmployee)
                {
                    Employee.Id = Emp.Id;
                    Employee.SyncToken = Emp.SyncToken;
                }

                try
                {
                    Intuit.Ipp.Data.Employee EmployeeAdded = dataService.Delete<Intuit.Ipp.Data.Employee>(Employee);
                    if (EmployeeAdded != null)
                    {
                        if (EmployeeAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataEmployee.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }
        //11.4 
        public async System.Threading.Tasks.Task GetOnlineAccountDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Account> dataAcconut = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Account> EmployeeQueryService = new QueryService<Intuit.Ipp.Data.Account>(serviceContext);

            dataAcconut = new ReadOnlyCollection<Intuit.Ipp.Data.Account>(EmployeeQueryService.ExecuteIdsQuery("select * from Account where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Account Account = new Intuit.Ipp.Data.Account();
            if (dataAcconut.Count > 0)
            {
                foreach (var Emp in dataAcconut)
                {
                    Account.Id = Emp.Id;
                    Account.SyncToken = Emp.SyncToken;
                }

                try
                {
                    Intuit.Ipp.Data.Account AccountAdded = dataService.Delete<Intuit.Ipp.Data.Account>(Account);
                    if (AccountAdded != null)
                    {
                        if (AccountAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataAcconut.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }
        public async System.Threading.Tasks.Task GetOnlineCustomerDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Customer> dataCustomer = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Customer> CustomerQueryService = new QueryService<Intuit.Ipp.Data.Customer>(serviceContext);

            dataCustomer = new ReadOnlyCollection<Intuit.Ipp.Data.Customer>(CustomerQueryService.ExecuteIdsQuery("select * from Customer where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Customer Customer = new Intuit.Ipp.Data.Customer();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataCustomer.Count > 0)
            {
                foreach (var Emp in dataCustomer)
                {
                    Customer.Id = Emp.Id;
                    Customer.SyncToken = Emp.SyncToken;
                }

                try
                {
                    Intuit.Ipp.Data.Customer CustomerAdded = dataService.Delete<Intuit.Ipp.Data.Customer>(Customer);
                    if (CustomerAdded != null)
                    {
                        if (CustomerAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataCustomer.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlineDepositDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Deposit> dataDeposit = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Deposit> CustomerQueryService = new QueryService<Intuit.Ipp.Data.Deposit>(serviceContext);

            dataDeposit = new ReadOnlyCollection<Intuit.Ipp.Data.Deposit>(CustomerQueryService.ExecuteIdsQuery("select * from Deposit where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Deposit Deposit = new Intuit.Ipp.Data.Deposit();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();
            if (dataDeposit.Count > 0)
            {
                foreach (var Emp in dataDeposit)
                {
                    Deposit.Id = Emp.Id;
                    Deposit.SyncToken = Emp.SyncToken;
                }

                try
                {
                    Intuit.Ipp.Data.Deposit DepositAdded = dataService.Delete<Intuit.Ipp.Data.Deposit>(Deposit);
                    if (DepositAdded != null)
                    {
                        if (DepositAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
                #endregion
            }
            else if (dataDeposit.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        public async System.Threading.Tasks.Task GetOnlineRefundReceiptDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.RefundReceipt> dataRefundReceipt = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.RefundReceipt> customerQueryService = new QueryService<Intuit.Ipp.Data.RefundReceipt>(serviceContext);

            dataRefundReceipt = new ReadOnlyCollection<Intuit.Ipp.Data.RefundReceipt>(customerQueryService.ExecuteIdsQuery("select * from RefundReceipt where Id= '" + txID + "'"));
            Intuit.Ipp.Data.RefundReceipt refundReceipt = new Intuit.Ipp.Data.RefundReceipt();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();

            if (dataRefundReceipt.Count > 0)
            {
                foreach (var refund in dataRefundReceipt)
                {
                    refundReceipt.Id = refund.Id;
                    refundReceipt.SyncToken = refund.SyncToken;
                    if (refund.Line != null)
                    {
                        foreach (var deleteLine in refund.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    refundReceipt.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    Intuit.Ipp.Data.RefundReceipt RefundReceiptAdded = dataService.Delete<Intuit.Ipp.Data.RefundReceipt>(refundReceipt);

                    if (RefundReceiptAdded != null)
                    {
                        if (RefundReceiptAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataRefundReceipt.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
            #endregion
        }

        //592
        public async System.Threading.Tasks.Task GetOnlineTransferDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Transfer> dataTransfer = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Transfer> customerQueryService = new QueryService<Intuit.Ipp.Data.Transfer>(serviceContext);

            dataTransfer = new ReadOnlyCollection<Intuit.Ipp.Data.Transfer>(customerQueryService.ExecuteIdsQuery("select * from RefundReceipt where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Transfer transferdata = new Intuit.Ipp.Data.Transfer();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();

            if (dataTransfer.Count > 0)
            {
                foreach (var trnfr in dataTransfer)
                {
                    transferdata.Id = trnfr.Id;
                    transferdata.SyncToken = trnfr.SyncToken;
                    if (trnfr.Line != null)
                    {
                        foreach (var deleteLine in trnfr.Line)
                        {
                            Line1.Id = deleteLine.Id;
                            if (Line1.Id == null)
                            {
                                continue;
                            }
                            else
                            {
                                break;
                            }
                        }

                    }
                    transferdata.Line = new Intuit.Ipp.Data.Line[] { Line1 };
                }

                try
                {
                    Intuit.Ipp.Data.Transfer RefundReceiptAdded = dataService.Delete<Intuit.Ipp.Data.Transfer>(transferdata);

                    if (RefundReceiptAdded != null)
                    {
                        if (RefundReceiptAdded.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataTransfer.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
            #endregion
        }


        public async System.Threading.Tasks.Task GetOnlineAttachmentDeleteRequest(string TranType, string txID, bool f)
        {
            ReadOnlyCollection<Intuit.Ipp.Data.Attachable> dataTransfer = null;
            DataService dataService = null;
            ServiceContext serviceContext = await CommonUtilities.GetQBOServiceContext();
            dataService = new DataService(serviceContext);
            QueryService<Intuit.Ipp.Data.Attachable> customerQueryService = new QueryService<Intuit.Ipp.Data.Attachable>(serviceContext);

            dataTransfer = new ReadOnlyCollection<Intuit.Ipp.Data.Attachable>(customerQueryService.ExecuteIdsQuery("select * from Attachable where Id= '" + txID + "'"));
            Intuit.Ipp.Data.Attachable attachmentData = new Intuit.Ipp.Data.Attachable();
            Intuit.Ipp.Data.Line Line1 = new Intuit.Ipp.Data.Line();

            if (dataTransfer.Count > 0)
            {
                foreach (var trnfr in dataTransfer)
                {
                    attachmentData.Id = trnfr.Id;
                    attachmentData.SyncToken = trnfr.SyncToken;
                }

                try
                {
                    Intuit.Ipp.Data.Attachable deleteAttachment = dataService.Delete<Intuit.Ipp.Data.Attachable>(attachmentData);

                    if (deleteAttachment != null)
                    {
                        if (deleteAttachment.status.ToString() == "Deleted")
                        {
                            string statusMessage = "Record Deleted Successfully from QuickBooks";
                            if (f == true)
                            {
                                CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                            }
                        }
                    }
                }
                #region error
                catch (Intuit.Ipp.Exception.IdsException ex)
                {
                    string GetError = string.Empty;
                    GetError = (((Intuit.Ipp.Exception.IdsException)(((Intuit.Ipp.Exception.IdsException)(ex)).InnerException)).InnerExceptions[0].Detail).ToString();
                    CommonUtilities.GetInstance().DisplaySummary(GetError);
                }
            }
            else if (dataTransfer.Count == 0)
            {
                string statusMessage = "Record does not exist.It is already deleted from QuickBooks.";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
            #endregion
        }

        #endregion

        //Axis 10.1
        /// <summary>
        /// This function is used to delete the perticular record from xero.
        /// </summary>
        /// <param name="TranType">Type of transaction</param>
        /// <param name="txID">ID of transcation</param>
        /// <param name="f"></param>
        public void GetTransactionDeleteRequestbyXero(string TranType, string txID, bool f)
        {
            string statusMessage = string.Empty;
            //repository = PublicApplicationRunner.CreateRepository();
            //Guid tid = new Guid();
            //tid = Guid.Parse(txID);
            try
            {
                //if (TranType == "Account Receivable" || TranType == "Account Payable")
                //{
                //    ICollection<XeroApi.Model.Invoice> invoices;

                //    var remove = (from invoice in repository.Invoices
                //                  where invoice.InvoiceID == tid
                //                  select invoice);

                //    invoices = remove.ToList();

                //    if (invoices != null)
                //    {
                //        foreach (XeroApi.Model.Invoice inv in invoices.ToList())
                //        {
                //            if (inv.Status == "DRAFT")
                //            {
                //                inv.Status = "DELETED";

                //                repository.UpdateOrCreate(inv);
                //            }
                //            if (inv.Status == "POSTED")
                //            {
                //                inv.Status = "VOIDED";

                //                repository.UpdateOrCreate(inv);
                //            }
                //        }
                //    }
                //}
                //if (TranType == "Contact")
                //{
                //    ICollection<XeroApi.Model.Contact> contacts;

                //    var remove = (from contact in repository.Contacts
                //                  where contact.ContactID == tid
                //                  select contact);

                //    contacts = remove.ToList();
                //    if (contacts != null)
                //    {
                //        foreach (XeroApi.Model.Contact contact in contacts.ToList())
                //        {

                //            contact.ContactStatus = "DELETED";

                //            repository.UpdateOrCreate(contact);
                //        }
                //    }

                //}

                //if (TranType == "Bank")
                //{
                //    ICollection<XeroApi.Model.BankTransaction> Banks;

                //    var remove = (from bank in repository.BankTransactions
                //                  where bank.BankTransactionID == tid
                //                  select bank);

                //    Banks = remove.ToList();
                //    if (Banks != null)
                //    {
                //        foreach (XeroApi.Model.BankTransaction bank in Banks.ToList())
                //        {

                //            bank.Status = "DELETED";

                //            repository.UpdateOrCreate(bank);
                //        }
                //    }

                //}
                //if (TranType == "Journal")
                //{
                //    ICollection<XeroApi.Model.ManualJournal> Journals;

                //    var remove = (from journal in repository.ManualJournals
                //                  where journal.ManualJournalID == tid
                //                  select journal);

                //    Journals = remove.ToList();
                //    if (Journals != null)
                //    {
                //        foreach (XeroApi.Model.ManualJournal journal in Journals.ToList())
                //        {
                //            if (journal.Status == "DRAFT")
                //            {
                //                journal.Status = "DELETED";

                //                repository.UpdateOrCreate(journal);
                //            }
                //            if (journal.Status == "POSTED")
                //            {
                //                journal.Status = "VOIDED";

                //                repository.UpdateOrCreate(journal);
                //            }
                //        }
                //    }
                //}
            }

            catch (Exception ex)
            {
                //Catch the exceptions and store into log files.
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                statusMessage += "Record Deleted Successfully from Xero";
                if (f == true)
                {
                    CommonUtilities.GetInstance().DisplaySummary(statusMessage);
                }
            }
        }

        /// <summary>
        /// This Function is used to view the transcation which in imported in xero.
        /// </summary>
        /// <param name="TranType">Type of Transaction</param>
        /// <param name="txID">ID of transcation</param>
        /// <returns></returns>
        public static void GetTransactionViewRequestForXero(string TranType, string txID, string accountid)
        {
            string uri = string.Empty;
            if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.Contacts)
            {
                uri = " https://go.xero.com/Contacts/View.aspx?contactID=" + txID;
                System.Diagnostics.Process.Start(uri);
            }
            if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.InvoiceCustomer)
            {
                uri = "https://go.xero.com/AccountsReceivable/Edit.aspx?InvoiceID=" + txID;
                System.Diagnostics.Process.Start(uri);
            }
            if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.InvoiceSupplier)
            {
                uri = "https://go.xero.com/AccountsPayable/Edit.aspx?InvoiceID=" + txID;
                System.Diagnostics.Process.Start(uri);
            }

            if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.BankReceive || CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.BankSpend)
            {
                uri = "https://go.xero.com/Bank/ViewTransaction.aspx?bankTransactionID=" + txID + "&accountID=" + accountid;
                System.Diagnostics.Process.Start(uri);

            }
            if (CommonUtilities.GetInstance().SelectedXeroImportType == DataProcessingBlocks.XeroImportType.Journal)
            {
                uri = "https://go.xero.com/Journal/View.aspx?invoiceID=" + txID;
                System.Diagnostics.Process.Start(uri);
            }

            Uri tempuri = new Uri(uri);
            string sQuery = tempuri.Query;


        }

        #endregion

        #region  Axis 11.0 -131 Export Import errors to Excel
        public DataTable GetDataTableImport(RadGridView dataGridView)
        {
            try
            {
                DataTable dt = new DataTable();
                if (dataGridView.RowCount > 0)
                {
                    dt.Columns.Add("Transaction Type");
                    dt.Columns.Add("Mapping Name");
                    dt.Columns.Add("Status");
                    dt.Columns.Add("Message");
                    dt.Columns.Add("Name");
                    dt.Columns.Add("Ref");
                    dt.Columns.Add("TxnID");
                    for (int row = 0; row < dataGridView.RowCount; row++)
                    {
                        DataRow datarw = dt.NewRow();
                        datarw[0] = dataGridView.Rows[row].Cells[0].Value.ToString();

                        if (name_column == null && ref_column == null)
                        {
                            if (CommonUtilities.GetInstance().ListMergeType != "")
                            {
                                setColumn("ListMerge");
                            }
                            else
                            {
                                setColumn(dataGridView.Rows[row].Cells[0].Value.ToString());
                            }
                        }

                        datarw[1] = CommonUtilities.GetInstance().SelectedMappingName;
                        datarw[2] = dataGridView.Rows[row].Cells[3].Value.ToString();
                        if (dataGridView.Rows[row].Cells[4].Value != null)
                        {
                            datarw[3] = dataGridView.Rows[row].Cells[4].Value.ToString();
                        }
                        else
                        {
                            datarw[3] = string.Empty;
                        }
                        if (dataGridView.Rows[row].Cells[2].Value != null)
                        {
                            datarw[4] = dataGridView.Rows[row].Cells[2].Value.ToString();
                        }
                        else
                        {
                            datarw[4] = string.Empty;
                        }
                        if (dataGridView.Rows[row].Cells[1].Value != null)
                        {
                            datarw[5] = dataGridView.Rows[row].Cells[1].Value.ToString();
                        }
                        else
                        {
                            datarw[5] = string.Empty;
                            ref_column = "";
                        }
                        if (dataGridView.Rows[row].Cells[7].Value != null)
                        {
                            datarw[6] = dataGridView.Rows[row].Cells[7].Value.ToString();
                        }
                        else
                        {
                            datarw[6] = string.Empty;
                        }
                        dt.Rows.Add(datarw);
                    }
                }
                else
                    dt = null;

                return dt;
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
                return null;
            }
        }

        public void ExportImportSummaryErrors(object sender, CancelEventArgs e)
        {
            if (e.Cancel == false)
            {
                ExportQuickBooksData.GetInstance().ExportFilePath = saveFileDialog_ImportErrors.FileName;

                string matchcol = "";
                if (ref_column == "")
                {
                    matchcol = name_column;
                }
                else
                {
                    matchcol = ref_column;
                }
                ExportQuickBooksData.GetInstance().ExportImportErrors(exportExcel, matchcol);
                saveFileDialog_ImportErrors.Dispose();
                name_column = null;
                ref_column = null;
                return;
            }
        }
        #endregion

        public void DisplayGrid(RadGridView dataImport)
        {
            this.radGridViewImportStatus.DataSource = dataImport.DataSource;
            this.radGridViewImportStatus.Visible = true;
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        public void DisplayImportSummaryPopUp(int FailureCount, int SuccesCount, StringBuilder sb, DataTable datatable, int SkipCount)
        {
           
                this.radDesktopAlert1.CaptionText = "Zed Axis Import Complete";
                this.radDesktopAlert1.ContentText = "Import complete " + SuccesCount + " Records added " + SkipCount + " skipped and " + FailureCount + " errors encountered";
                radBtnOk.Click += radBtnOk_Click;
                this.radDesktopAlert1.ContentImage = Image.FromFile(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Resources/DAlert.png"));
                //radBtnOk.Location = new Point(this.Location.X + 50, this.Location.Y + 50);
                //this.radDesktopAlert1.ScreenPosition = AlertScreenPosition.Manual;
                //this.radDesktopAlert1.Popup.Location = new Point(this.Location.X + 250, this.Location.Y + 250);
                this.radDesktopAlert1.Show();
                display = true;
          

        }

        public void DisplayImportSummary(int FailureCount, int SuccesCount, StringBuilder sb, DataTable datatable, int SkipCount)
        {
            //600
            if (display != true)
            {
                this.radDesktopAlert1.CaptionText = "Zed Axis Import Complete";
                this.radDesktopAlert1.ContentText = "Import complete " + SuccesCount + " Records added " + SkipCount + " skipped and " + FailureCount + " errors encountered";
                radBtnOk.Click += radBtnOk_Click;
                this.radDesktopAlert1.ContentImage = Image.FromFile(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Resources/DAlert.png"));
                //radBtnOk.Location = new Point(this.Location.X + 50, this.Location.Y + 50);
                //this.radDesktopAlert1.ScreenPosition = AlertScreenPosition.Manual;
                //this.radDesktopAlert1.Popup.Location = new Point(this.Location.X + 250, this.Location.Y + 250);
                this.radDesktopAlert1.Show();
                display = true;
            }
            else
            {
                display = false;
            }

            //727
            string failMsgString = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG020"), FailureCount);
            this.FailMsg = failMsgString.Substring(0, failMsgString.Length - 1);

            string successMsgString = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG019"), SuccesCount);
            this.SuccesMsg = successMsgString.Substring(0, successMsgString.Length - 1);

            string skippMsgString = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSGSkip"), SkipCount);
            this.Skipped = skippMsgString.Substring(0, skippMsgString.Length - 1);

            labelSuccess.Text = this.m_succesMSG;
            labelSkipped.Text = this.m_skipped;
            labelFailure.Text = this.m_failMSG;

            //Stop backgroundworker
            Cursor.Current = Cursors.WaitCursor;
            TransactionImporter axisForm = (TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;
            if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
            {
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                bkWorker.CancelAsync();
                this.CloseImportSplash();
            }
            if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                bkWorker.CancelAsync();
            }
            if (axisForm.backgroundWorkerProcessLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
            }

        }

        public void radBtnOk_Click(object sender, EventArgs e)
        {
            checkForReviewsAndRatings();
            radBtnOk.Click -= radBtnOk_Click;
            this.radDesktopAlert1.Hide();
            this.CloseImportSplash();
        }

        public void checkForReviewsAndRatings()
        {
            try
            {
                DataProcessingBlocks.RatingAndReview reviewNrating = new DataProcessingBlocks.RatingAndReview();
                //for rating and review popup
                reviewNrating.getRating();
                if (reviewNrating.GivenFeedback == false && clsProtectCS.LPIsActivated() == 1 && clsProtectCS.IsOnlineActivatedLP() == 1)
                {
                    string messageString = string.Empty;
                    StringBuilder productName = new StringBuilder(255);
                    StringBuilder productDecription = new StringBuilder(255);
                    StringBuilder expirationDate = new StringBuilder(255);
                    int DaysLeft = -1;
                    int AllowedRuns = -1;
                    int RealRuns = -1;
                    int AllowedHours = -1;
                    int RealMinutes = -1;

                    // Retrive product saas information if it is saas product
                    clsProtectCS.GetLicenseProductInfoLP(productName,
                      productDecription,
                      expirationDate,
                      ref DaysLeft,
                      ref AllowedRuns,
                      ref RealRuns,
                      ref AllowedHours,
                      ref RealMinutes);
                    //MessageBox.Show("RealRuns   :" + RealRuns);
                    if (RealRuns > 20)
                    {

                        //  RatingAndReview rateReview = new RatingAndReview();
                        DialogResult dialogresult = reviewNrating.ShowDialog();
                        if (DialogResult == DialogResult.OK)
                        {
                            Console.WriteLine("Ok");
                        }
                        reviewNrating.Dispose();
                        reviewNrating.Close();
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void radbuttonSkip_Click(object sender, EventArgs e)
        {
            skipFlag = true;
            SkipOneFlag();
        }

        private void radbuttonSkip_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                skipFlag = true;
                SkipOneFlag();
            }
        }

        private void radbuttonSkipAll_Click(object sender, EventArgs e)
        {
            skipAllFlag = true;
            SkipFlag();
        }

        private void radbuttonQuit_Click(object sender, EventArgs e)
        {
            radGridViewImportStatus.Rows.Clear();
            Cursor.Current = Cursors.WaitCursor;
            TransactionImporter axisForm = (TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;
            if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
            {
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseImportSplash();
                this.CloseImportSplash();
            }
            if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                bkWorker.CancelAsync();
            }
            if (axisForm.backgroundWorkerProcessLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseSplash();
            }
            // TransactionImporter.is_statement_preview_selected = false;  //bug 501
            this.CloseImportSplash();
        }

        private void radbuttonUndoAll_Click(object sender, EventArgs e)
        {
            flag = false;

            //if (CommonUtilities.GetInstance().setCount == 1)
            //{
            //    DataProcessingBlocks.CommonUtilities.ShowpreviewSplashcodingtemp(EDI.Constant.Constants.statementDeleteRecords, true);
            // CommonUtilities.GetInstance().setCount = 0;
            //} 

            if ((radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "Class" || radGridViewImportStatus.Rows[0].Cells[0].Value.ToString() == "InventorySite") && CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBstring)
            {
                txnList = "List";
                importType = radGridViewImportStatus.Rows[0].Cells[0].Value.ToString();
                List<Tuple<string, string>> listParentFullName = new List<Tuple<string, string>>();
                listParentFullName = CommonUtilities.GetInstance().listParentClass;
                listParentFullName = listParentFullName.OrderByDescending(x => x.Item2.Length).ToList();

                for (int listIndex = 0; listIndex < (listParentFullName.Count); listIndex++)
                {
                    if (listIndex == (listParentFullName.Count - 1))
                    {
                        flag = true;
                    }
                    GetTransactionDeleteRequest(importType, listParentFullName[listIndex].Item1, flag, txnList);
                }
            }
            else
            {
                for (int i = 0; i < radGridViewImportStatus.RowCount; i++)
                {
                    if (i == (radGridViewImportStatus.Rows.Count - 1))
                    {
                        flag = true;
                    }
                    if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBstring)
                    {
                        if (radGridViewImportStatus.Rows[i].Cells[3].Value.ToString() == "Imported")
                        {
                            is_imported = true;
                            tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                        }
                        importType = radGridViewImportStatus.Rows[0].Cells[0].Value.ToString();
                        if (importType == "Account" || importType == "Customer" || importType == "Employee" || importType == "Item" ||
                            importType == "OtherName" || importType == "Vendor" || importType == "ItemSalesTax" ||
                            importType == "ItemNonInventory " || importType == "ItemSalesTaxGroup" || importType == "OtherName" || 
                            importType == "BillingRate" || importType == "ItemGroup" || importType == "ItemInventory"
                            || importType == "ItemPayment" || importType == "ItemFixedAsset")
                        {
                            txnList = "List";
                        }
                        else
                        {
                            txnList = "Txn";
                        }
                        deleteFlag = false;
                        GetTransactionDeleteRequest(importType, tId, flag, txnList);
                    }
                    if (radGridViewImportStatus.Rows[i].Cells[3].Value.ToString() == "Imported")
                    {
                        is_imported = true;
                        if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBPOSstring)
                        {
                            #region
                            tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                            importType = radGridViewImportStatus.Rows[0].Cells[0].Value.ToString();

                            if (importType == "Customer" || importType == "Employee" || importType == "ItemInventory" ||
                                importType == "TimeEntry" || importType == "SalesReceipt" || importType == "TransferSlip" ||
                                importType == "InventoryQtyAdjustment" || importType == "InventoryCostAdjustment" ||
                                importType == "Voucher" || importType == "SalesOrder" || importType == "PurchaseOrder" ||
                                importType == "PriceDiscount" || importType == "PriceAdjustment" || importType == "CustomFieldList")
                            {
                                txnList = "List";
                            }
                            else
                            {
                                txnList = "Txn";
                            }
                            GetPOSTransactionDeleteRequest(importType, tId, flag, txnList);

                            #endregion
                        }

                        if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBOnlinestring)
                        {
                            #region
                            tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                            importType = radGridViewImportStatus.Rows[0].Cells[0].Value.ToString();

                            if (importType == "SalesReceipt")
                            {
                                GetOnlineTransactionDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "Bill")
                            {
                                 GetOnlineBillTransactionDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "JournalEntry")
                            {
                                 GetOnlineJournalTransactionDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "Invoice")
                            {
                                 GetOnlineInvoiceDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "TimeActivity")
                            {
                                 GetOnlineTimeActivityDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "PurchaseOrder")
                            {
                                 GetOnlinePurchaseOrderDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "Estimate")
                            {
                                 GetOnlineEstimateDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "Payment")
                            {
                                 GetOnlinePaymentDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "VendorCredit")
                            {
                                 GetOnlineVendorCreditDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "CreditMemo")
                            {
                                 GetOnlineCreditMemoDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "CashPurchase" || importType == "CheckPurchase" || importType == "CreditCardPurchase")
                            {
                                 GetOnlinePurchaseDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "Employee")
                            {
                                 GetOnlineEmployeeDeleteRequest(importType, tId, flag);
                            }
                            //11.4
                            else if (importType == "Customer")
                            {
                                 GetOnlineCustomerDeleteRequest(importType, tId, flag);
                            }
                            else if (importType == "Deposit")
                            {
                                 GetOnlineDepositDeleteRequest(importType, tId, true);
                            }
                            else if (importType == "Account")
                            {
                                 GetOnlineAccountDeleteRequest(importType, tId, true);
                            }
                            else if (importType == "RefundReceipt")
                            {
                                 GetOnlineRefundReceiptDeleteRequest(importType, tId, true);
                            }
                            else if (importType == "Transfer")
                            {
                                 GetOnlineTransferDeleteRequest(importType, tId, true);
                            }
                            else if (importType == "Attachment")
                            {
                              GetOnlineAttachmentDeleteRequest(importType, tId, true);
                            }
                            
                            #endregion
                        }

                        if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.Xerostring)
                        {
                            #region
                            importType = CommonUtilities.GetInstance().Type;
                            tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                            GetTransactionDeleteRequestbyXero(importType, tId, flag);
                            #endregion
                        }
                        if (CommonUtilities.GetInstance().ConnectedSoftware == EDI.Constant.Constants.QBOnlinestring)
                        {
                            tId = radGridViewImportStatus.Rows[i].Cells[7].Value.ToString();
                        }
                    }
                }
            }
        }

        private void radbuttonExport_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = GetDataTableImport(radGridViewImportStatus);
                bool isError = false;
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["Status"].ToString() == "Error")
                        {
                            isError = true;
                            break;
                        }
                    }

                    if (isError == true)
                    {
                        addDataTable(dt);
                        saveFileDialog_ImportErrors.FileName = string.Empty;
                        saveFileDialog_ImportErrors.Filter = "Excel File | *.xls";
                        saveFileDialog_ImportErrors.FileOk += new CancelEventHandler(ExportImportSummaryErrors);
                        saveFileDialog_ImportErrors.ShowDialog();
                    }
                    else
                    {
                        name_column = null;
                        MessageBox.Show("Sorry there are no errors in the import to export.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                else
                {
                    name_column = null;
                    MessageBox.Show("Sorry cannot export the errors to excel.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message.ToString());
                //CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        private void radbuttonCancelImportSplashScreen_Click(object sender, EventArgs e)
        {
            //588
            checkForReviewsAndRatings();
            radGridViewImportStatus.Rows.Clear();
            Cursor.Current = Cursors.WaitCursor;
            TransactionImporter axisForm = (TransactionImporter)Application.OpenForms["TransactionImporter"];
            BackgroundWorker bkWorker;
            if (axisForm.backgroundWorkerStartupProcessLoader.IsBusy)
            {
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerStartupProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseImportSplash();
                this.CloseImportSplash();
            }
            if (axisForm.backgroundWorkerQBItemLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerQBItemLoader;
                bkWorker.CancelAsync();
            }
            if (axisForm.backgroundWorkerProcessLoader.IsBusy)
            {
                axisForm.IsBusy = false;
                bkWorker = (System.ComponentModel.BackgroundWorker)axisForm.backgroundWorkerProcessLoader;
                bkWorker.CancelAsync();
                //DataProcessingBlocks.CommonUtilities.CloseSplash();
            }
            //	TransactionImporter.is_statement_preview_selected = false;  //bug 501
            this.CloseImportSplash();
        }

        private void timerStatus_Tick(object sender, EventArgs e)
        {
            //727
            statusbarPercentage++;
            radProgressBarStatusBar.Value1 = statusbarPercentage;
            radProgressBarStatusBar.Text = statusbarPercentage.ToString() + "%";
            if (this.radProgressBarStatusBar.Value1 == this.radProgressBarStatusBar.Maximum)
            {
                timerStatus.Enabled = false;
                statusbarPercentage = 0;
            }
        }
    }
}
