using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EDI.Message;

namespace DataProcessingBlocks
{
    public partial class MailViewer : Form
    {
        #region Private Members

        /// <summary>
        /// Message Id of the message
        /// </summary>
        private int m_MessageId;

        private int m_messageStatus1;

        private int m_messageStatus2;

        private int m_messageStatus3;

        private int m_messageStatus4;
        private MailboxFolders m_type;

        #endregion

        #region Public Properties

        /// <summary>
        /// 
        /// </summary>
        public int MessageId
        {
            get { return m_MessageId; }
        }
        #endregion

        public MailViewer(int messageId, int messageStatus1, int messageStatus2, int messageStatus3, int messageStatus4, MailboxFolders type)
        {
            InitializeComponent();
            m_MessageId = messageId;
            m_messageStatus1 = messageStatus1;
            m_messageStatus2 = messageStatus2;
            m_messageStatus3 = messageStatus3;
            m_messageStatus4 = messageStatus4;
            m_type = type;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MailViewer_Load(object sender, EventArgs e)
        {
            try
            {
                using (DataSet dataSetMessage = new MessageController().GetMessage(m_MessageId, m_messageStatus1, m_messageStatus2,m_messageStatus3,m_messageStatus4))
                {
                    if (dataSetMessage != null && dataSetMessage.Tables[0].Rows.Count == 1)
                    {
                        DataRow messageRow = dataSetMessage.Tables[0].Rows[0];
                        if (m_type == MailboxFolders.Inbox)
                        {
                            labelHeaderTo.Visible = false;
                            labelHeaderFrom.Visible = true;
                            labelAddresses.Text = messageRow.ItemArray[0].ToString();
                        }
                        else if (m_type == MailboxFolders.SentItems)
                        {
                            labelHeaderTo.Visible = true;
                            labelHeaderFrom.Visible =false;
                            labelAddresses.Text = messageRow.ItemArray[1].ToString();
                        }
                        labelCc.Text = messageRow.ItemArray[6].ToString();
                        richTextBoxBody.Text = messageRow.ItemArray[3].ToString();
                        labelSubject.Text = messageRow.ItemArray[2].ToString();
                    }
                    else
                    {
                        MessageBox.Show("Error loading message. Please refer log for details.", "Zed Axis", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        this.Close();
                    }
                }
                using (DataSet dataSetNames = new MessageController().GetAttachmentNames(m_MessageId, m_messageStatus1,m_messageStatus2,m_messageStatus3,m_messageStatus4))
                {
                    int count = dataSetNames.Tables[0].Rows.Count;
                    if (dataSetNames != null && count > 0)
                    {
                        int x = 98;
                        int y = 95;
                        for (int index = 0; index < count; index++)
                        {
                            string fileName = dataSetNames.Tables[0].Rows[index].ItemArray[0].ToString();
                            LinkLabel labelIndex = new LinkLabel();
                            labelIndex.Text = fileName;
                            labelIndex.Location = new Point(x, y);
                            labelIndex.Visible = true;
                            labelIndex.Width = fileName.Length * 8;
                            labelIndex.Name = fileName;
                            labelIndex.Tag = dataSetNames.Tables[0].Rows[index].ItemArray[2].ToString();
                            labelIndex.Height = 18;
                            labelIndex.ForeColor = Color.Blue;
                            labelIndex.Click += new EventHandler(labelIndex_Click);
                            this.Controls.Add(labelIndex);
                            labelIndex.BringToFront();
                            if (y < 134)
                            {
                                y += 18;
                            }
                            else if (x < 355)
                            {
                                x += 135; y = 109;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelIndex_Click(object sender, EventArgs e)
        {
            try
            {
                LinkLabel linkLabel = (LinkLabel)sender;
                AttachmentViewer attachmentViewer = new AttachmentViewer();
                object attachment = new MessageController().GetAttachmentFile(Convert.ToInt32(linkLabel.Tag), linkLabel.Text, m_messageStatus1,m_messageStatus2,m_messageStatus3,m_messageStatus4);
                attachmentViewer.AttachmentContent = ASCIIEncoding.ASCII.GetString((byte[])attachment);
                attachmentViewer.ShowDialog();
            }
            catch (Exception ex)
            {
                //CommonUtilities.WriteErrorLog(ex.Message);
                //CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

    }
}