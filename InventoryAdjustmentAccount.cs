using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EDI.Constant;
using System.Xml;
using DataProcessingBlocks;
using Interop.QBXMLRP2Lib;
using Streams;
using System.Globalization;
using System.Collections;
using EDI.Message;

namespace TransactionImporter
{
    // ===============================================================================
    // 
    // InventoryAdjustmentAccount.cs
    //
    // This file provide the setting for exporting inventory adjustment receive into QuickBooks.
    // Its populating the AccountRefFullName from the Quick Books on form load event.
    //
    // Developed By : K.Gouraw.
    // Date : 06-05-2009
    // ==============================================================================

    public partial class InventoryAdjustmentAccount : Form
    {
        //class variable
        static string QBFileName;            //QuickBook company file name.
        static string attachmentContent;     //store the .ism file as string.
        static string acctRefFullName; //store the account ref full name after select it from combo box.
        static int messageId;
        ImportSplashScreen iss = ImportSplashScreen.GetInstance();
        public InventoryAdjustmentAccount(string fileName,string attachment,int MessageId)
        {
            InitializeComponent();
            QBFileName = fileName;
            attachmentContent = attachment;
            messageId = MessageId;
        }

        #region private methods
        /// <summary>
        /// Getting the xml list of Account from Quick Books.
        /// </summary>
        /// <returns>xml list</returns>
        private string GetAccountRef()
        {
            //Create a xml document for Item List
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            //Adding process instruction.
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            //Append child nodes.
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement accountQueryRq = pxmldoc.CreateElement("AccountQueryRq");
            qbXMLMsgsRq.AppendChild(accountQueryRq);
            accountQueryRq.SetAttribute("requestID", "1");

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string responseOfAccount = string.Empty;
            try
            {

                DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);

                //Getting ticket for request.
                ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                //Processing the request.
                responseOfAccount = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);



            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                throw new Exception(ex.Message);
            }

            return responseOfAccount;
        }
        #endregion

        #region EventHandler
        private void InventoryAdjustmentAccount_Load(object sender, EventArgs e)
        {
            comboBoxAccountRef.Items.Clear();
            comboBoxAccountRef.Items.Add(Constants.selectstring);
            //calling the GetAccountRef method
            string accountXmlList = GetAccountRef();                
             XmlDocument outputXMLDoc = new XmlDocument();
            outputXMLDoc.LoadXml(accountXmlList);
           //Getting list of node of AccountRefFullName.
            XmlNodeList AccountNodeList = outputXMLDoc.GetElementsByTagName("FullName");
            foreach (XmlNode fullName in AccountNodeList)
            {
                comboBoxAccountRef.Items.Add(fullName.InnerText);
            }
            comboBoxAccountRef.SelectedIndex = 0;
        }
      
        private void InventoryAdjustmentAccount_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = false;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            if (comboBoxAccountRef.SelectedIndex.Equals(0))
            {
                MessageBox.Show("Please select account ref full name", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            acctRefFullName=comboBoxAccountRef.SelectedItem.ToString();
           
            System.Threading.Thread.Sleep(500);
            this.backgroundWorkerProcess.RunWorkerAsync();
            DataProcessingBlocks.CommonUtilities.ShowSplash("Exporting into Quick Books", true);
            this.Close();
        }

           
        private void backgroundWorkerProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            InventoryAdjustmentReceiveCollection inventRecieveCollection = new InventoryAdjustmentReceiveCollection();
            int startIndex = attachmentContent.IndexOf(Constants.NEWLINE) + Constants.NEWLINE.Length;
            int endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;

            #region Parsing and grouping according to txndate the attachment

            InventoryAdjusttReceive inventReceive=null;
            List<string> dateList = new List<string>();
            try
            {
                while (endIndex <= (attachmentContent.Length))
                {
                    string[] outerStream = attachmentContent.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());

                    if (outerStream[0].Trim().Equals("SA"))
                    {
                        if (string.IsNullOrEmpty(outerStream[1].Trim()))
                        {
                            MessageBox.Show("TxnDate field is not provided in this file, this is required field."+
                            "\nPlease correct and try again for further process or contact support",Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            break;
                        }
                        bool breakFlag= false;                         
                        string datevalue = outerStream[1].Trim();
                        DateTime dttest = DateTime.ParseExact(datevalue, Constants.ISMFileDateTime, CultureInfo.InvariantCulture);
                        string mTxnDate = dttest.ToString("yyyy-MM-dd");
                        if (dateList.Contains(mTxnDate))
                        {
                            startIndex = endIndex + Constants.NEWLINE.Length;
                            if (startIndex < attachmentContent.Length)
                            {
                                endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
                                endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                            }
                            else
                            {
                                break;
                            }
                            continue;
                        }
                        else
                        {
                            dateList.Add(mTxnDate);
                            inventReceive = new InventoryAdjusttReceive();
                            inventReceive.AccountRef.FullName = acctRefFullName;
                            inventReceive.TxnDate = mTxnDate;
                            if (!string.IsNullOrEmpty(outerStream[5].Trim()))
                                inventReceive.Memo = outerStream[5].Trim();

                            int startInnerIndex = attachmentContent.IndexOf(Constants.NEWLINE) + Constants.NEWLINE.Length;
                            int endInnerIndex = attachmentContent.IndexOf(Constants.NEWLINE, startInnerIndex);
                            endInnerIndex = endInnerIndex == -1 ? attachmentContent.Length : endInnerIndex;
                            while (endInnerIndex <= (attachmentContent.Length))
                            {
                                string[] stream = attachmentContent.Substring(startInnerIndex, endInnerIndex - startInnerIndex).Split("|".ToCharArray());
                                if (stream[0].Trim().Equals("SA"))
                                {
                                    if (string.IsNullOrEmpty(stream[1].Trim()))
                                    {
                                        MessageBox.Show("TxnDate field is not provided in this file, this is required field." +
                                             "\nPlease correct and try again for further process or contact support", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        breakFlag = true;
                                        break;
                                    }
                                    if (string.IsNullOrEmpty(stream[2].Trim()))
                                    {
                                        MessageBox.Show("ItemRef:FullName field is not provided in this file, this is required field." +
                                              "\nPlease correct and try again for further process or contact support", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        breakFlag = true;
                                        break;
                                    }
                                    if (string.IsNullOrEmpty(stream[3].Trim()) || string.IsNullOrEmpty(stream[4].Trim()))
                                    {
                                        MessageBox.Show("QuantityDifference field is not provided in this file, this is required field." +
                                             "\nPlease correct and try again for further process or contact support", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        breakFlag = true;
                                        break;
                                    }

                                    string date = stream[1].Trim();
                                    DateTime datetest = DateTime.ParseExact(date, Constants.ISMFileDateTime, CultureInfo.InvariantCulture);
                                    string txnDate = datetest.ToString("yyyy-MM-dd");
                                    if (txnDate == mTxnDate)
                                    {
                                        Streams.InventoryAdjustmentLineAdd lineAdd = new Streams.InventoryAdjustmentLineAdd();
                                        string qtySign = string.Empty;
                                        lineAdd.ItemRef.FullName = stream[2].Trim();
                                        qtySign = stream[3].Trim();
                                        lineAdd.QuantityAdjustment.QuantityDifference = qtySign + stream[4].Trim();
                                        inventReceive.InventoryAdjustmentLineAdd.Add(lineAdd);
                                    }

                                }
                                startInnerIndex = endInnerIndex + Constants.NEWLINE.Length;
                                if (startInnerIndex < attachmentContent.Length)
                                {
                                    endInnerIndex = attachmentContent.IndexOf(Constants.NEWLINE, startInnerIndex);
                                    endInnerIndex = endInnerIndex == -1 ? attachmentContent.Length : endInnerIndex;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            inventRecieveCollection.Add(inventReceive);
                            if (breakFlag)
                            {
                                break;
                            }
                        
                        }
                        
                    }
                    startIndex = endIndex + Constants.NEWLINE.Length;
                    if (startIndex < attachmentContent.Length)
                    {
                        endIndex = attachmentContent.IndexOf(Constants.NEWLINE, startIndex);
                        endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch
            {
                 throw new Exception("Error in parsing SP stream in the attachment");
            }
            
            #endregion

            StringBuilder sb = new StringBuilder();
            int SuccesCount = 0;
            int FailureCount = 0;
         
            foreach (InventoryAdjusttReceive entry in inventRecieveCollection)
            {
                string message = string.Empty;
                if (!entry.ExportToQuickBooks(ref message,QBFileName))
                {
                    FailureCount++;
                    sb.Append(message);
                }
                else
                    SuccesCount++;
               
                backgroundWorkerProcess.ReportProgress((SuccesCount + FailureCount) * 100 / inventRecieveCollection.Count);
             }
             #region Display Summary of Errors

             if (CommonUtilities.GetInstance().IsInboundFlag == true)
             {
                 CommonUtilities.WriteErrorLog(sb.ToString());
                 if (FailureCount > 0)
                     CommonUtilities.GetInstance().FailureCount = CommonUtilities.GetInstance().FailureCount + 1;
                 else if (SuccesCount > 0)
                     CommonUtilities.GetInstance().ProcessedCount = CommonUtilities.GetInstance().ProcessedCount + 1;

             }
             else
             {
                 ImportSummary summry = new ImportSummary();
                 summry.FailMsg = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG020"), FailureCount);
                 summry.SuccesMsg = string.Format(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG019"), SuccesCount);
                 if (sb.Length > 0)
                     summry.SDKMSG = DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG021") + "\n" + sb.ToString();
                 summry.ShowDialog();
                 summry.Dispose();
             }
             if (FailureCount > 0)
             {
                 new MessageController().ChangeMessageStatus(messageId, (int)MessageStatus.Failed);
                 new MessageController().ChangeMessageStatus(messageId, (int)MessageStatus.Failed);
                 //CommonUtilities.WriteErrorLog(sb.ToString());
             }
             else
             {
                 new MessageController().ChangeMessageStatus(messageId, (int)MessageStatus.Processed);
                 new MessageController().ChangeAttachmentStatus(messageId, (int)MessageStatus.Processed);
             }

             #endregion
          
        }

        private void backgroundWorkerProcess_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            iss.SetImportPercentageComplete(e.ProgressPercentage);
            //DataProcessingBlocks.CommonUtilities.SetImportPercentageComplete(e.ProgressPercentage);
        }

        private void backgroundWorkerProcess_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //DataProcessingBlocks.CommonUtilities.CloseImportSplash();
            iss.CloseImportSplash();
            
        }
        #endregion

    }
}