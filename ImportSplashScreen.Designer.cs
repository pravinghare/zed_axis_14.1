﻿namespace TransactionImporter
{
    partial class ImportSplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportSplashScreen));
            this.radGridViewImportStatus = new Telerik.WinControls.UI.RadGridView();
            this.labelImportingRow = new Telerik.WinControls.UI.RadLabel();
            this.labelValidateRow = new Telerik.WinControls.UI.RadLabel();
            this.labelFailure = new Telerik.WinControls.UI.RadLabel();
            this.labelSkipped = new Telerik.WinControls.UI.RadLabel();
            this.labelSuccess = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.labelImporting = new Telerik.WinControls.UI.RadLabel();
            this.labelListUpdate = new Telerik.WinControls.UI.RadLabel();
            this.labelValidating = new Telerik.WinControls.UI.RadLabel();
            this.radbuttonCancelImportSplashScreen = new Telerik.WinControls.UI.RadButton();
            this.radbuttonExport = new Telerik.WinControls.UI.RadButton();
            this.radbuttonUndoAll = new Telerik.WinControls.UI.RadButton();
            this.radbuttonQuit = new Telerik.WinControls.UI.RadButton();
            this.radbuttonSkipAll = new Telerik.WinControls.UI.RadButton();
            this.radbuttonSkip = new Telerik.WinControls.UI.RadButton();
            this.radprogressBarImporting = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBarListUpdate = new Telerik.WinControls.UI.RadProgressBar();
            this.radProgressBarStatusBar = new Telerik.WinControls.UI.RadProgressBar();
            this.timerStatus = new System.Windows.Forms.Timer(this.components);
            this.bindingSourceImportSplash = new System.Windows.Forms.BindingSource(this.components);
            this.office2007SilverTheme1 = new Telerik.WinControls.Themes.Office2007SilverTheme();
            this.saveFileDialog_ImportErrors = new System.Windows.Forms.SaveFileDialog();
            this.radDesktopAlert1 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            this.radBtnOk = new Telerik.WinControls.UI.RadButtonElement();
            this.eBayAPIInterfaceService1 = new GeteBayOfficialTime.com.ebay.developer.eBayAPIInterfaceService();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewImportStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewImportStatus.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImportingRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelValidateRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFailure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSkipped)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSuccess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelListUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelValidating)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonCancelImportSplashScreen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonUndoAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonQuit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonSkipAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonSkip)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radprogressBarImporting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBarListUpdate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBarStatusBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceImportSplash)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridViewImportStatus
            // 
            this.radGridViewImportStatus.AllowDrop = true;
            this.radGridViewImportStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewImportStatus.AutoScroll = true;
            this.radGridViewImportStatus.AutoSizeRows = true;
            this.radGridViewImportStatus.BackColor = System.Drawing.Color.LightSteelBlue;
            this.radGridViewImportStatus.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridViewImportStatus.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridViewImportStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridViewImportStatus.Location = new System.Drawing.Point(-3, 0);
            // 
            // 
            // 
            this.radGridViewImportStatus.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewImportStatus.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewImportStatus.MasterTemplate.AllowEditRow = false;
            this.radGridViewImportStatus.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.radGridViewImportStatus.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewImportStatus.MasterTemplate.EnableFiltering = true;
            this.radGridViewImportStatus.MasterTemplate.EnableGrouping = false;
            this.radGridViewImportStatus.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            this.radGridViewImportStatus.MasterTemplate.ShowFilteringRow = false;
            this.radGridViewImportStatus.MasterTemplate.ShowHeaderCellButtons = true;
            this.radGridViewImportStatus.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridViewImportStatus.Name = "radGridViewImportStatus";
            this.radGridViewImportStatus.Padding = new System.Windows.Forms.Padding(0, 0, 0, 1);
            this.radGridViewImportStatus.ReadOnly = true;
            this.radGridViewImportStatus.ShowGroupPanel = false;
            this.radGridViewImportStatus.ShowHeaderCellButtons = true;
            this.radGridViewImportStatus.Size = new System.Drawing.Size(1062, 371);
            this.radGridViewImportStatus.TabIndex = 19;
            this.radGridViewImportStatus.ThemeName = "ControlDefault";
            this.radGridViewImportStatus.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.radGridViewImportStatus_RowFormatting);
            this.radGridViewImportStatus.CellClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGridViewImportStatus_CellClick);
            // 
            // labelImportingRow
            // 
            this.labelImportingRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImportingRow.AutoSize = false;
            this.labelImportingRow.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelImportingRow.Location = new System.Drawing.Point(245, 452);
            this.labelImportingRow.Name = "labelImportingRow";
            this.labelImportingRow.Size = new System.Drawing.Size(140, 17);
            this.labelImportingRow.TabIndex = 8;
            // 
            // labelValidateRow
            // 
            this.labelValidateRow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelValidateRow.AutoSize = false;
            this.labelValidateRow.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelValidateRow.Location = new System.Drawing.Point(243, 392);
            this.labelValidateRow.Name = "labelValidateRow";
            this.labelValidateRow.Size = new System.Drawing.Size(140, 17);
            this.labelValidateRow.TabIndex = 7;
            // 
            // labelFailure
            // 
            this.labelFailure.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelFailure.AutoSize = false;
            this.labelFailure.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelFailure.Location = new System.Drawing.Point(391, 453);
            this.labelFailure.Name = "labelFailure";
            this.labelFailure.Size = new System.Drawing.Size(158, 17);
            this.labelFailure.TabIndex = 12;
            // 
            // labelSkipped
            // 
            this.labelSkipped.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSkipped.AutoSize = false;
            this.labelSkipped.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelSkipped.Location = new System.Drawing.Point(391, 435);
            this.labelSkipped.Name = "labelSkipped";
            this.labelSkipped.Size = new System.Drawing.Size(158, 17);
            this.labelSkipped.TabIndex = 11;
            // 
            // labelSuccess
            // 
            this.labelSuccess.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelSuccess.AutoSize = false;
            this.labelSuccess.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelSuccess.Location = new System.Drawing.Point(391, 415);
            this.labelSuccess.Name = "labelSuccess";
            this.labelSuccess.Size = new System.Drawing.Size(158, 17);
            this.labelSuccess.TabIndex = 10;
            // 
            // radLabel1
            // 
            this.radLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radLabel1.AutoSize = false;
            this.radLabel1.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.radLabel1.Location = new System.Drawing.Point(391, 391);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(125, 22);
            this.radLabel1.TabIndex = 9;
            this.radLabel1.Text = "Summary";
            // 
            // labelImporting
            // 
            this.labelImporting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelImporting.AutoSize = false;
            this.labelImporting.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelImporting.Location = new System.Drawing.Point(10, 449);
            this.labelImporting.Name = "labelImporting";
            this.labelImporting.Size = new System.Drawing.Size(77, 22);
            this.labelImporting.TabIndex = 3;
            this.labelImporting.Text = "Importing";
            // 
            // labelListUpdate
            // 
            this.labelListUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelListUpdate.AutoSize = false;
            this.labelListUpdate.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelListUpdate.Location = new System.Drawing.Point(10, 423);
            this.labelListUpdate.Name = "labelListUpdate";
            this.labelListUpdate.Size = new System.Drawing.Size(70, 16);
            this.labelListUpdate.TabIndex = 2;
            this.labelListUpdate.Text = "List update";
            // 
            // labelValidating
            // 
            this.labelValidating.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelValidating.AutoSize = false;
            this.labelValidating.Font = new System.Drawing.Font("Verdana", 8.25F);
            this.labelValidating.Location = new System.Drawing.Point(10, 387);
            this.labelValidating.Name = "labelValidating";
            this.labelValidating.Size = new System.Drawing.Size(74, 27);
            this.labelValidating.TabIndex = 1;
            this.labelValidating.Text = "Validating";
            // 
            // radbuttonCancelImportSplashScreen
            // 
            this.radbuttonCancelImportSplashScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radbuttonCancelImportSplashScreen.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radbuttonCancelImportSplashScreen.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radbuttonCancelImportSplashScreen.Location = new System.Drawing.Point(975, 447);
            this.radbuttonCancelImportSplashScreen.Name = "radbuttonCancelImportSplashScreen";
            this.radbuttonCancelImportSplashScreen.Size = new System.Drawing.Size(74, 25);
            this.radbuttonCancelImportSplashScreen.TabIndex = 18;
            this.radbuttonCancelImportSplashScreen.Text = "Close";
            this.radbuttonCancelImportSplashScreen.Click += new System.EventHandler(this.radbuttonCancelImportSplashScreen_Click);
            // 
            // radbuttonExport
            // 
            this.radbuttonExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radbuttonExport.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radbuttonExport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radbuttonExport.Location = new System.Drawing.Point(895, 447);
            this.radbuttonExport.Name = "radbuttonExport";
            this.radbuttonExport.Size = new System.Drawing.Size(74, 25);
            this.radbuttonExport.TabIndex = 17;
            this.radbuttonExport.Text = "Export";
            this.radbuttonExport.Click += new System.EventHandler(this.radbuttonExport_Click);
            // 
            // radbuttonUndoAll
            // 
            this.radbuttonUndoAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radbuttonUndoAll.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radbuttonUndoAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radbuttonUndoAll.Location = new System.Drawing.Point(813, 447);
            this.radbuttonUndoAll.Name = "radbuttonUndoAll";
            this.radbuttonUndoAll.Size = new System.Drawing.Size(74, 25);
            this.radbuttonUndoAll.TabIndex = 16;
            this.radbuttonUndoAll.Text = "Undo all";
            this.radbuttonUndoAll.Click += new System.EventHandler(this.radbuttonUndoAll_Click);
            // 
            // radbuttonQuit
            // 
            this.radbuttonQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radbuttonQuit.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radbuttonQuit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radbuttonQuit.Location = new System.Drawing.Point(731, 447);
            this.radbuttonQuit.Name = "radbuttonQuit";
            this.radbuttonQuit.Size = new System.Drawing.Size(74, 25);
            this.radbuttonQuit.TabIndex = 15;
            this.radbuttonQuit.Text = "Quit";
            this.radbuttonQuit.Click += new System.EventHandler(this.radbuttonQuit_Click);
            // 
            // radbuttonSkipAll
            // 
            this.radbuttonSkipAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radbuttonSkipAll.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radbuttonSkipAll.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radbuttonSkipAll.Location = new System.Drawing.Point(649, 447);
            this.radbuttonSkipAll.Name = "radbuttonSkipAll";
            this.radbuttonSkipAll.Size = new System.Drawing.Size(74, 25);
            this.radbuttonSkipAll.TabIndex = 14;
            this.radbuttonSkipAll.Text = "Skip all";
            this.radbuttonSkipAll.Click += new System.EventHandler(this.radbuttonSkipAll_Click);
            // 
            // radbuttonSkip
            // 
            this.radbuttonSkip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.radbuttonSkip.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radbuttonSkip.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(66)))), ((int)(((byte)(139)))));
            this.radbuttonSkip.Location = new System.Drawing.Point(567, 447);
            this.radbuttonSkip.Name = "radbuttonSkip";
            this.radbuttonSkip.Size = new System.Drawing.Size(74, 25);
            this.radbuttonSkip.TabIndex = 13;
            this.radbuttonSkip.Text = "Skip";
            this.radbuttonSkip.Click += new System.EventHandler(this.radbuttonSkip_Click);
            this.radbuttonSkip.MouseClick += new System.Windows.Forms.MouseEventHandler(this.radbuttonSkip_MouseClick);
            // 
            // radprogressBarImporting
            // 
            this.radprogressBarImporting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radprogressBarImporting.Location = new System.Drawing.Point(88, 453);
            this.radprogressBarImporting.Name = "radprogressBarImporting";
            this.radprogressBarImporting.Size = new System.Drawing.Size(145, 16);
            this.radprogressBarImporting.TabIndex = 6;
            // 
            // radProgressBarListUpdate
            // 
            this.radProgressBarListUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radProgressBarListUpdate.Location = new System.Drawing.Point(88, 423);
            this.radProgressBarListUpdate.Name = "radProgressBarListUpdate";
            this.radProgressBarListUpdate.Size = new System.Drawing.Size(145, 16);
            this.radProgressBarListUpdate.TabIndex = 5;
            // 
            // radProgressBarStatusBar
            // 
            this.radProgressBarStatusBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.radProgressBarStatusBar.Location = new System.Drawing.Point(88, 392);
            this.radProgressBarStatusBar.Name = "radProgressBarStatusBar";
            this.radProgressBarStatusBar.Size = new System.Drawing.Size(145, 16);
            this.radProgressBarStatusBar.TabIndex = 4;
            // 
            // timerStatus
            // 
            this.timerStatus.Interval = 50;
            this.timerStatus.Tick += new System.EventHandler(this.timerStatus_Tick);
            // 
            // radDesktopAlert1
            // 
            this.radDesktopAlert1.AutoSize = true;
            this.radDesktopAlert1.ButtonItems.AddRange(new Telerik.WinControls.RadItem[] {
            this.radBtnOk});
            this.radDesktopAlert1.FadeAnimationFrames = 50;
            // 
            // radBtnOk
            // 
            this.radBtnOk.ClickMode = Telerik.WinControls.ClickMode.Press;
            this.radBtnOk.Name = "radBtnOk";
            this.radBtnOk.Text = "OK";
            // 
            // eBayAPIInterfaceService1
            // 
            this.eBayAPIInterfaceService1.Credentials = null;
            this.eBayAPIInterfaceService1.RequesterCredentials = null;
            this.eBayAPIInterfaceService1.Url = "https://api.ebay.com/wsapi";
            this.eBayAPIInterfaceService1.UseDefaultCredentials = false;
            // 
            // ImportSplashScreen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1057, 484);
            this.Controls.Add(this.labelImportingRow);
            this.Controls.Add(this.labelValidateRow);
            this.Controls.Add(this.labelFailure);
            this.Controls.Add(this.labelSkipped);
            this.Controls.Add(this.labelSuccess);
            this.Controls.Add(this.radLabel1);
            this.Controls.Add(this.labelImporting);
            this.Controls.Add(this.labelListUpdate);
            this.Controls.Add(this.labelValidating);
            this.Controls.Add(this.radbuttonCancelImportSplashScreen);
            this.Controls.Add(this.radbuttonExport);
            this.Controls.Add(this.radbuttonUndoAll);
            this.Controls.Add(this.radbuttonQuit);
            this.Controls.Add(this.radbuttonSkipAll);
            this.Controls.Add(this.radbuttonSkip);
            this.Controls.Add(this.radprogressBarImporting);
            this.Controls.Add(this.radProgressBarListUpdate);
            this.Controls.Add(this.radProgressBarStatusBar);
            this.Controls.Add(this.radGridViewImportStatus);
            this.DoubleBuffered = false;
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportSplashScreen";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Axis Import Progress";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ImportSplashScreen_FormClosing);
            this.Load += new System.EventHandler(this.ImportSplashScreen_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewImportStatus.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewImportStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImportingRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelValidateRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelFailure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSkipped)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelSuccess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelImporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelListUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.labelValidating)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonCancelImportSplashScreen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonUndoAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonQuit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonSkipAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radbuttonSkip)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radprogressBarImporting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBarListUpdate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radProgressBarStatusBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceImportSplash)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Telerik.WinControls.UI.RadGridView radGridViewImportStatus;
        private Telerik.WinControls.UI.RadLabel labelImportingRow;
        private Telerik.WinControls.UI.RadLabel labelValidateRow;
        private Telerik.WinControls.UI.RadLabel labelFailure;
        private Telerik.WinControls.UI.RadLabel labelSkipped;
        private Telerik.WinControls.UI.RadLabel labelSuccess;
        public Telerik.WinControls.UI.RadLabel radLabel1;
        public Telerik.WinControls.UI.RadLabel labelImporting;
        public Telerik.WinControls.UI.RadLabel labelListUpdate;
        public Telerik.WinControls.UI.RadLabel labelValidating;
        private Telerik.WinControls.UI.RadButton radbuttonCancelImportSplashScreen;
        private Telerik.WinControls.UI.RadButton radbuttonExport;
        private Telerik.WinControls.UI.RadButton radbuttonUndoAll;
        private Telerik.WinControls.UI.RadButton radbuttonQuit;
        private Telerik.WinControls.UI.RadButton radbuttonSkipAll;
        private Telerik.WinControls.UI.RadButton radbuttonSkip;
        private Telerik.WinControls.UI.RadProgressBar radprogressBarImporting;
        private Telerik.WinControls.UI.RadProgressBar radProgressBarListUpdate;
        private Telerik.WinControls.UI.RadProgressBar radProgressBarStatusBar;
        private GeteBayOfficialTime.com.ebay.developer.eBayAPIInterfaceService eBayAPIInterfaceService1;
        private System.Windows.Forms.Timer timerStatus;
        private System.Windows.Forms.BindingSource bindingSourceImportSplash;
        private Telerik.WinControls.Themes.Office2007SilverTheme office2007SilverTheme1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_ImportErrors;
        public Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;
        private Telerik.WinControls.UI.RadButtonElement radBtnOk;
    }
}
