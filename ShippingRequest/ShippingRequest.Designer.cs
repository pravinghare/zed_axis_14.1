﻿namespace DataProcessingBlocks
{
    partial class ShippingRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShippingRequest));
            this.groupBoxFrom = new System.Windows.Forms.GroupBox();
            this.textBoxPrefix = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxFromPhone = new System.Windows.Forms.TextBox();
            this.label1FromPhone = new System.Windows.Forms.Label();
            this.textBoxFromCompany = new System.Windows.Forms.TextBox();
            this.labelCompany = new System.Windows.Forms.Label();
            this.textBoxInstruction3 = new System.Windows.Forms.TextBox();
            this.textBoxInstruction2 = new System.Windows.Forms.TextBox();
            this.groupBoxPackages = new System.Windows.Forms.GroupBox();
            this.dataGridViewPackages = new System.Windows.Forms.DataGridView();
            this.groupBoxShippingService = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxServiceSelected = new System.Windows.Forms.ComboBox();
            this.textBoxBookedBy = new System.Windows.Forms.TextBox();
            this.textBoxReferenceNumber = new System.Windows.Forms.TextBox();
            this.labelPickUpTime = new System.Windows.Forms.Label();
            this.labelBookedBy = new System.Windows.Forms.Label();
            this.labelReferenceNumber = new System.Windows.Forms.Label();
            this.labelServiceSelected = new System.Windows.Forms.Label();
            this.groupBoxShippingStatus = new System.Windows.Forms.GroupBox();
            this.labelJobDispatched = new System.Windows.Forms.Label();
            this.labelDateTime1 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.labelJobBooked1 = new System.Windows.Forms.Label();
            this.buttonPrintLabel = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.richTextBoxMessageSummary = new System.Windows.Forms.RichTextBox();
            this.groupBoxDeliveryInstructions = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxInstruction1 = new System.Windows.Forms.TextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.labelPhone = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelToCompany = new System.Windows.Forms.Label();
            this.textBoxToCompany = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.textBoxPhone = new System.Windows.Forms.TextBox();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBoxDeliveryDetails = new System.Windows.Forms.GroupBox();
            this.textBoxAddr2 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxAddr1 = new System.Windows.Forms.TextBox();
            this.labelAddress = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelDelivery = new System.Windows.Forms.Label();
            this.textBoxCountry = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.textBoxState = new System.Windows.Forms.TextBox();
            this.labelCountry = new System.Windows.Forms.Label();
            this.labelCity = new System.Windows.Forms.Label();
            this.textBoxPostalCode = new System.Windows.Forms.TextBox();
            this.labelPostalCode = new System.Windows.Forms.Label();
            this.labelState = new System.Windows.Forms.Label();
            this.groupBoxTo = new System.Windows.Forms.GroupBox();
            this.groupBoxPickupDetails = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPickupPostCode = new System.Windows.Forms.TextBox();
            this.textBoxPickupState = new System.Windows.Forms.TextBox();
            this.textBoxPickupCity = new System.Windows.Forms.TextBox();
            this.textBoxPickupAddr2 = new System.Windows.Forms.TextBox();
            this.textBoxPickupAddr1 = new System.Windows.Forms.TextBox();
            this.groupBoxFrom.SuspendLayout();
            this.groupBoxPackages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPackages)).BeginInit();
            this.groupBoxShippingService.SuspendLayout();
            this.groupBoxShippingStatus.SuspendLayout();
            this.groupBoxDeliveryInstructions.SuspendLayout();
            this.groupBoxDeliveryDetails.SuspendLayout();
            this.groupBoxTo.SuspendLayout();
            this.groupBoxPickupDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxFrom
            // 
            this.groupBoxFrom.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxFrom.Controls.Add(this.textBoxPrefix);
            this.groupBoxFrom.Controls.Add(this.label11);
            this.groupBoxFrom.Controls.Add(this.textBoxFromPhone);
            this.groupBoxFrom.Controls.Add(this.label1FromPhone);
            this.groupBoxFrom.Controls.Add(this.textBoxFromCompany);
            this.groupBoxFrom.Controls.Add(this.labelCompany);
            this.groupBoxFrom.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxFrom.ForeColor = System.Drawing.SystemColors.ControlText;
            this.groupBoxFrom.Location = new System.Drawing.Point(26, 12);
            this.groupBoxFrom.Name = "groupBoxFrom";
            this.groupBoxFrom.Size = new System.Drawing.Size(446, 115);
            this.groupBoxFrom.TabIndex = 0;
            this.groupBoxFrom.TabStop = false;
            this.groupBoxFrom.Text = "From";
            // 
            // textBoxPrefix
            // 
            this.textBoxPrefix.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPrefix.Location = new System.Drawing.Point(146, 75);
            this.textBoxPrefix.Name = "textBoxPrefix";
            this.textBoxPrefix.ReadOnly = true;
            this.textBoxPrefix.Size = new System.Drawing.Size(263, 21);
            this.textBoxPrefix.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label11.Location = new System.Drawing.Point(53, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Prefix ";
            // 
            // textBoxFromPhone
            // 
            this.textBoxFromPhone.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFromPhone.Location = new System.Drawing.Point(146, 48);
            this.textBoxFromPhone.Name = "textBoxFromPhone";
            this.textBoxFromPhone.ReadOnly = true;
            this.textBoxFromPhone.Size = new System.Drawing.Size(263, 21);
            this.textBoxFromPhone.TabIndex = 2;
            // 
            // label1FromPhone
            // 
            this.label1FromPhone.AutoSize = true;
            this.label1FromPhone.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1FromPhone.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1FromPhone.Location = new System.Drawing.Point(55, 51);
            this.label1FromPhone.Name = "label1FromPhone";
            this.label1FromPhone.Size = new System.Drawing.Size(42, 13);
            this.label1FromPhone.TabIndex = 2;
            this.label1FromPhone.Text = "Phone";
            // 
            // textBoxFromCompany
            // 
            this.textBoxFromCompany.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFromCompany.Location = new System.Drawing.Point(146, 21);
            this.textBoxFromCompany.Name = "textBoxFromCompany";
            this.textBoxFromCompany.ReadOnly = true;
            this.textBoxFromCompany.Size = new System.Drawing.Size(263, 21);
            this.textBoxFromCompany.TabIndex = 1;
            // 
            // labelCompany
            // 
            this.labelCompany.AutoSize = true;
            this.labelCompany.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompany.ForeColor = System.Drawing.SystemColors.ControlText;
            this.labelCompany.Location = new System.Drawing.Point(39, 24);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(62, 13);
            this.labelCompany.TabIndex = 0;
            this.labelCompany.Text = "Company";
            // 
            // textBoxInstruction3
            // 
            this.textBoxInstruction3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInstruction3.Location = new System.Drawing.Point(85, 59);
            this.textBoxInstruction3.MaxLength = 40;
            this.textBoxInstruction3.Name = "textBoxInstruction3";
            this.textBoxInstruction3.Size = new System.Drawing.Size(276, 21);
            this.textBoxInstruction3.TabIndex = 25;
            this.textBoxInstruction3.TextChanged += new System.EventHandler(this.textBoxInstruction3_TextChanged);
            // 
            // textBoxInstruction2
            // 
            this.textBoxInstruction2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInstruction2.Location = new System.Drawing.Point(85, 39);
            this.textBoxInstruction2.MaxLength = 40;
            this.textBoxInstruction2.Name = "textBoxInstruction2";
            this.textBoxInstruction2.Size = new System.Drawing.Size(276, 21);
            this.textBoxInstruction2.TabIndex = 24;
            this.textBoxInstruction2.TextChanged += new System.EventHandler(this.textBoxInstruction2_TextChanged);
            // 
            // groupBoxPackages
            // 
            this.groupBoxPackages.Controls.Add(this.dataGridViewPackages);
            this.groupBoxPackages.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxPackages.Location = new System.Drawing.Point(497, 104);
            this.groupBoxPackages.Name = "groupBoxPackages";
            this.groupBoxPackages.Size = new System.Drawing.Size(421, 218);
            this.groupBoxPackages.TabIndex = 36;
            this.groupBoxPackages.TabStop = false;
            this.groupBoxPackages.Text = "Packages";
            // 
            // dataGridViewPackages
            // 
            this.dataGridViewPackages.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewPackages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPackages.Location = new System.Drawing.Point(6, 20);
            this.dataGridViewPackages.Name = "dataGridViewPackages";
            this.dataGridViewPackages.ReadOnly = true;
            this.dataGridViewPackages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPackages.Size = new System.Drawing.Size(409, 192);
            this.dataGridViewPackages.TabIndex = 37;
            this.dataGridViewPackages.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPackages_CellDoubleClick);
            // 
            // groupBoxShippingService
            // 
            this.groupBoxShippingService.Controls.Add(this.label22);
            this.groupBoxShippingService.Controls.Add(this.label10);
            this.groupBoxShippingService.Controls.Add(this.dateTimePicker);
            this.groupBoxShippingService.Controls.Add(this.label3);
            this.groupBoxShippingService.Controls.Add(this.label2);
            this.groupBoxShippingService.Controls.Add(this.comboBoxServiceSelected);
            this.groupBoxShippingService.Controls.Add(this.textBoxBookedBy);
            this.groupBoxShippingService.Controls.Add(this.textBoxReferenceNumber);
            this.groupBoxShippingService.Controls.Add(this.labelPickUpTime);
            this.groupBoxShippingService.Controls.Add(this.labelBookedBy);
            this.groupBoxShippingService.Controls.Add(this.labelReferenceNumber);
            this.groupBoxShippingService.Controls.Add(this.labelServiceSelected);
            this.groupBoxShippingService.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxShippingService.Location = new System.Drawing.Point(497, 328);
            this.groupBoxShippingService.Name = "groupBoxShippingService";
            this.groupBoxShippingService.Size = new System.Drawing.Size(421, 160);
            this.groupBoxShippingService.TabIndex = 26;
            this.groupBoxShippingService.TabStop = false;
            this.groupBoxShippingService.Text = "Shipping Service";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(135, 55);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 17);
            this.label22.TabIndex = 31;
            this.label22.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(135, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 30;
            this.label10.Text = "*";
            // 
            // dateTimePicker
            // 
            this.dateTimePicker.CustomFormat = "dd/MM/yyyy  HH:mm:ss";
            this.dateTimePicker.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker.Location = new System.Drawing.Point(158, 116);
            this.dateTimePicker.Name = "dateTimePicker";
            this.dateTimePicker.Size = new System.Drawing.Size(227, 21);
            this.dateTimePicker.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(135, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 28;
            this.label3.Text = "*";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(135, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 17);
            this.label2.TabIndex = 27;
            this.label2.Text = "*";
            // 
            // comboBoxServiceSelected
            // 
            this.comboBoxServiceSelected.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxServiceSelected.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxServiceSelected.FormattingEnabled = true;
            this.comboBoxServiceSelected.ItemHeight = 13;
            this.comboBoxServiceSelected.Location = new System.Drawing.Point(158, 23);
            this.comboBoxServiceSelected.Name = "comboBoxServiceSelected";
            this.comboBoxServiceSelected.Size = new System.Drawing.Size(227, 21);
            this.comboBoxServiceSelected.TabIndex = 22;
            // 
            // textBoxBookedBy
            // 
            this.textBoxBookedBy.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxBookedBy.Location = new System.Drawing.Point(158, 83);
            this.textBoxBookedBy.MaxLength = 50;
            this.textBoxBookedBy.Name = "textBoxBookedBy";
            this.textBoxBookedBy.Size = new System.Drawing.Size(227, 21);
            this.textBoxBookedBy.TabIndex = 29;
            // 
            // textBoxReferenceNumber
            // 
            this.textBoxReferenceNumber.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxReferenceNumber.Location = new System.Drawing.Point(158, 54);
            this.textBoxReferenceNumber.MaxLength = 6;
            this.textBoxReferenceNumber.Name = "textBoxReferenceNumber";
            this.textBoxReferenceNumber.Size = new System.Drawing.Size(227, 21);
            this.textBoxReferenceNumber.TabIndex = 28;
            this.textBoxReferenceNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxReferenceNumber_KeyPress);
            // 
            // labelPickUpTime
            // 
            this.labelPickUpTime.AutoSize = true;
            this.labelPickUpTime.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPickUpTime.Location = new System.Drawing.Point(42, 121);
            this.labelPickUpTime.Name = "labelPickUpTime";
            this.labelPickUpTime.Size = new System.Drawing.Size(77, 13);
            this.labelPickUpTime.TabIndex = 12;
            this.labelPickUpTime.Text = "Pick up time";
            // 
            // labelBookedBy
            // 
            this.labelBookedBy.AutoSize = true;
            this.labelBookedBy.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBookedBy.Location = new System.Drawing.Point(51, 87);
            this.labelBookedBy.Name = "labelBookedBy";
            this.labelBookedBy.Size = new System.Drawing.Size(69, 13);
            this.labelBookedBy.TabIndex = 11;
            this.labelBookedBy.Text = "Booked By";
            // 
            // labelReferenceNumber
            // 
            this.labelReferenceNumber.AutoSize = true;
            this.labelReferenceNumber.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReferenceNumber.Location = new System.Drawing.Point(6, 62);
            this.labelReferenceNumber.Name = "labelReferenceNumber";
            this.labelReferenceNumber.Size = new System.Drawing.Size(114, 13);
            this.labelReferenceNumber.TabIndex = 10;
            this.labelReferenceNumber.Text = "Reference Number";
            // 
            // labelServiceSelected
            // 
            this.labelServiceSelected.AutoSize = true;
            this.labelServiceSelected.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServiceSelected.Location = new System.Drawing.Point(17, 31);
            this.labelServiceSelected.Name = "labelServiceSelected";
            this.labelServiceSelected.Size = new System.Drawing.Size(103, 13);
            this.labelServiceSelected.TabIndex = 9;
            this.labelServiceSelected.Text = "Service Selected";
            // 
            // groupBoxShippingStatus
            // 
            this.groupBoxShippingStatus.Controls.Add(this.labelJobDispatched);
            this.groupBoxShippingStatus.Controls.Add(this.labelDateTime1);
            this.groupBoxShippingStatus.Controls.Add(this.label);
            this.groupBoxShippingStatus.Controls.Add(this.labelJobBooked1);
            this.groupBoxShippingStatus.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxShippingStatus.Location = new System.Drawing.Point(497, 494);
            this.groupBoxShippingStatus.Name = "groupBoxShippingStatus";
            this.groupBoxShippingStatus.Size = new System.Drawing.Size(421, 81);
            this.groupBoxShippingStatus.TabIndex = 30;
            this.groupBoxShippingStatus.TabStop = false;
            this.groupBoxShippingStatus.Text = "Shipping Status";
            // 
            // labelJobDispatched
            // 
            this.labelJobDispatched.AutoSize = true;
            this.labelJobDispatched.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJobDispatched.Location = new System.Drawing.Point(196, 52);
            this.labelJobDispatched.Name = "labelJobDispatched";
            this.labelJobDispatched.Size = new System.Drawing.Size(84, 13);
            this.labelJobDispatched.TabIndex = 31;
            this.labelJobDispatched.Text = "<Date Time>";
            // 
            // labelDateTime1
            // 
            this.labelDateTime1.AutoSize = true;
            this.labelDateTime1.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDateTime1.Location = new System.Drawing.Point(196, 23);
            this.labelDateTime1.Name = "labelDateTime1";
            this.labelDateTime1.Size = new System.Drawing.Size(84, 13);
            this.labelDateTime1.TabIndex = 31;
            this.labelDateTime1.Text = "<Date Time>";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(22, 52);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(93, 13);
            this.label.TabIndex = 13;
            this.label.Text = "Job Dispatched";
            // 
            // labelJobBooked1
            // 
            this.labelJobBooked1.AutoSize = true;
            this.labelJobBooked1.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJobBooked1.Location = new System.Drawing.Point(42, 25);
            this.labelJobBooked1.Name = "labelJobBooked1";
            this.labelJobBooked1.Size = new System.Drawing.Size(73, 13);
            this.labelJobBooked1.TabIndex = 11;
            this.labelJobBooked1.Text = "Job Booked";
            // 
            // buttonPrintLabel
            // 
            this.buttonPrintLabel.Enabled = false;
            this.buttonPrintLabel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrintLabel.Location = new System.Drawing.Point(522, 584);
            this.buttonPrintLabel.Name = "buttonPrintLabel";
            this.buttonPrintLabel.Size = new System.Drawing.Size(75, 23);
            this.buttonPrintLabel.TabIndex = 35;
            this.buttonPrintLabel.Text = "Print Label";
            this.buttonPrintLabel.UseVisualStyleBackColor = true;
            this.buttonPrintLabel.Click += new System.EventHandler(this.buttonPrintLabel_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSave.Location = new System.Drawing.Point(686, 584);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 33;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCancel.Location = new System.Drawing.Point(782, 584);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 34;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // richTextBoxMessageSummary
            // 
            this.richTextBoxMessageSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxMessageSummary.BackColor = System.Drawing.Color.AliceBlue;
            this.richTextBoxMessageSummary.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxMessageSummary.ForeColor = System.Drawing.Color.Red;
            this.richTextBoxMessageSummary.Location = new System.Drawing.Point(24, 613);
            this.richTextBoxMessageSummary.Name = "richTextBoxMessageSummary";
            this.richTextBoxMessageSummary.ReadOnly = true;
            this.richTextBoxMessageSummary.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.richTextBoxMessageSummary.Size = new System.Drawing.Size(893, 57);
            this.richTextBoxMessageSummary.TabIndex = 36;
            this.richTextBoxMessageSummary.Text = "";
            // 
            // groupBoxDeliveryInstructions
            // 
            this.groupBoxDeliveryInstructions.Controls.Add(this.label14);
            this.groupBoxDeliveryInstructions.Controls.Add(this.label13);
            this.groupBoxDeliveryInstructions.Controls.Add(this.label12);
            this.groupBoxDeliveryInstructions.Controls.Add(this.textBoxInstruction1);
            this.groupBoxDeliveryInstructions.Controls.Add(this.textBoxInstruction3);
            this.groupBoxDeliveryInstructions.Controls.Add(this.textBoxInstruction2);
            this.groupBoxDeliveryInstructions.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDeliveryInstructions.Location = new System.Drawing.Point(496, 11);
            this.groupBoxDeliveryInstructions.Name = "groupBoxDeliveryInstructions";
            this.groupBoxDeliveryInstructions.Size = new System.Drawing.Size(421, 87);
            this.groupBoxDeliveryInstructions.TabIndex = 22;
            this.groupBoxDeliveryInstructions.TabStop = false;
            this.groupBoxDeliveryInstructions.Text = "Delivery Instructions";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(52, 62);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(18, 13);
            this.label14.TabIndex = 40;
            this.label14.Text = "3.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(52, 42);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(18, 13);
            this.label13.TabIndex = 39;
            this.label13.Text = "2.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(52, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(18, 13);
            this.label12.TabIndex = 38;
            this.label12.Text = "1.";
            // 
            // textBoxInstruction1
            // 
            this.textBoxInstruction1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInstruction1.Location = new System.Drawing.Point(85, 19);
            this.textBoxInstruction1.MaxLength = 40;
            this.textBoxInstruction1.Name = "textBoxInstruction1";
            this.textBoxInstruction1.Size = new System.Drawing.Size(276, 21);
            this.textBoxInstruction1.TabIndex = 23;
            this.textBoxInstruction1.TextChanged += new System.EventHandler(this.textBoxInstruction1_TextChanged);
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.Location = new System.Drawing.Point(55, 84);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(38, 13);
            this.labelEmail.TabIndex = 10;
            this.labelEmail.Text = "Email";
            // 
            // labelPhone
            // 
            this.labelPhone.AutoSize = true;
            this.labelPhone.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhone.Location = new System.Drawing.Point(49, 111);
            this.labelPhone.Name = "labelPhone";
            this.labelPhone.Size = new System.Drawing.Size(42, 13);
            this.labelPhone.TabIndex = 9;
            this.labelPhone.Text = "Phone";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(53, 52);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(40, 13);
            this.labelName.TabIndex = 2;
            this.labelName.Text = "Name";
            // 
            // labelToCompany
            // 
            this.labelToCompany.AutoSize = true;
            this.labelToCompany.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelToCompany.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToCompany.Location = new System.Drawing.Point(36, 23);
            this.labelToCompany.Name = "labelToCompany";
            this.labelToCompany.Size = new System.Drawing.Size(62, 13);
            this.labelToCompany.TabIndex = 1;
            this.labelToCompany.Text = "Company";
            // 
            // textBoxToCompany
            // 
            this.textBoxToCompany.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxToCompany.Location = new System.Drawing.Point(132, 20);
            this.textBoxToCompany.MaxLength = 50;
            this.textBoxToCompany.Name = "textBoxToCompany";
            this.textBoxToCompany.Size = new System.Drawing.Size(288, 21);
            this.textBoxToCompany.TabIndex = 5;
            // 
            // textBoxName
            // 
            this.textBoxName.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxName.Location = new System.Drawing.Point(132, 49);
            this.textBoxName.MaxLength = 20;
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(289, 21);
            this.textBoxName.TabIndex = 6;
            // 
            // textBoxPhone
            // 
            this.textBoxPhone.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPhone.Location = new System.Drawing.Point(132, 111);
            this.textBoxPhone.MaxLength = 13;
            this.textBoxPhone.Name = "textBoxPhone";
            this.textBoxPhone.Size = new System.Drawing.Size(289, 21);
            this.textBoxPhone.TabIndex = 8;
            this.textBoxPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPhone_KeyPress);
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxEmail.Location = new System.Drawing.Point(132, 81);
            this.textBoxEmail.MaxLength = 50;
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.Size = new System.Drawing.Size(288, 21);
            this.textBoxEmail.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(109, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 28;
            this.label4.Text = "*";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(111, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 17);
            this.label5.TabIndex = 29;
            this.label5.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(109, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 17);
            this.label8.TabIndex = 32;
            this.label8.Text = "*";
            // 
            // groupBoxDeliveryDetails
            // 
            this.groupBoxDeliveryDetails.Controls.Add(this.textBoxAddr2);
            this.groupBoxDeliveryDetails.Controls.Add(this.label9);
            this.groupBoxDeliveryDetails.Controls.Add(this.textBoxAddr1);
            this.groupBoxDeliveryDetails.Controls.Add(this.labelAddress);
            this.groupBoxDeliveryDetails.Controls.Add(this.label7);
            this.groupBoxDeliveryDetails.Controls.Add(this.labelDelivery);
            this.groupBoxDeliveryDetails.Controls.Add(this.textBoxCountry);
            this.groupBoxDeliveryDetails.Controls.Add(this.label6);
            this.groupBoxDeliveryDetails.Controls.Add(this.textBoxCity);
            this.groupBoxDeliveryDetails.Controls.Add(this.textBoxState);
            this.groupBoxDeliveryDetails.Controls.Add(this.labelCountry);
            this.groupBoxDeliveryDetails.Controls.Add(this.labelCity);
            this.groupBoxDeliveryDetails.Controls.Add(this.textBoxPostalCode);
            this.groupBoxDeliveryDetails.Controls.Add(this.labelPostalCode);
            this.groupBoxDeliveryDetails.Controls.Add(this.labelState);
            this.groupBoxDeliveryDetails.Location = new System.Drawing.Point(16, 289);
            this.groupBoxDeliveryDetails.Name = "groupBoxDeliveryDetails";
            this.groupBoxDeliveryDetails.Size = new System.Drawing.Size(413, 179);
            this.groupBoxDeliveryDetails.TabIndex = 15;
            this.groupBoxDeliveryDetails.TabStop = false;
            this.groupBoxDeliveryDetails.Text = "Delivery Details";
            // 
            // textBoxAddr2
            // 
            this.textBoxAddr2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddr2.Location = new System.Drawing.Point(118, 50);
            this.textBoxAddr2.MaxLength = 25;
            this.textBoxAddr2.Name = "textBoxAddr2";
            this.textBoxAddr2.Size = new System.Drawing.Size(286, 21);
            this.textBoxAddr2.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(95, 117);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 17);
            this.label9.TabIndex = 33;
            this.label9.Text = "*";
            // 
            // textBoxAddr1
            // 
            this.textBoxAddr1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAddr1.Location = new System.Drawing.Point(119, 23);
            this.textBoxAddr1.MaxLength = 25;
            this.textBoxAddr1.Name = "textBoxAddr1";
            this.textBoxAddr1.Size = new System.Drawing.Size(286, 21);
            this.textBoxAddr1.TabIndex = 16;
            // 
            // labelAddress
            // 
            this.labelAddress.AutoSize = true;
            this.labelAddress.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAddress.Location = new System.Drawing.Point(22, 50);
            this.labelAddress.Name = "labelAddress";
            this.labelAddress.Size = new System.Drawing.Size(60, 13);
            this.labelAddress.TabIndex = 4;
            this.labelAddress.Text = "Address2";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(95, 87);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 17);
            this.label7.TabIndex = 31;
            this.label7.Text = "*";
            // 
            // labelDelivery
            // 
            this.labelDelivery.AutoSize = true;
            this.labelDelivery.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDelivery.Location = new System.Drawing.Point(24, 23);
            this.labelDelivery.Name = "labelDelivery";
            this.labelDelivery.Size = new System.Drawing.Size(60, 13);
            this.labelDelivery.TabIndex = 3;
            this.labelDelivery.Text = "Address1";
            // 
            // textBoxCountry
            // 
            this.textBoxCountry.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCountry.Location = new System.Drawing.Point(117, 142);
            this.textBoxCountry.MaxLength = 50;
            this.textBoxCountry.Name = "textBoxCountry";
            this.textBoxCountry.Size = new System.Drawing.Size(286, 21);
            this.textBoxCountry.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(96, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "*";
            // 
            // textBoxCity
            // 
            this.textBoxCity.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCity.Location = new System.Drawing.Point(118, 87);
            this.textBoxCity.MaxLength = 50;
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.Size = new System.Drawing.Size(287, 21);
            this.textBoxCity.TabIndex = 17;
            // 
            // textBoxState
            // 
            this.textBoxState.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxState.Location = new System.Drawing.Point(119, 116);
            this.textBoxState.MaxLength = 5;
            this.textBoxState.Name = "textBoxState";
            this.textBoxState.Size = new System.Drawing.Size(152, 21);
            this.textBoxState.TabIndex = 19;
            // 
            // labelCountry
            // 
            this.labelCountry.AutoSize = true;
            this.labelCountry.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCountry.Location = new System.Drawing.Point(29, 145);
            this.labelCountry.Name = "labelCountry";
            this.labelCountry.Size = new System.Drawing.Size(53, 13);
            this.labelCountry.TabIndex = 8;
            this.labelCountry.Text = "Country";
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCity.Location = new System.Drawing.Point(55, 89);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(30, 13);
            this.labelCity.TabIndex = 5;
            this.labelCity.Text = "City";
            // 
            // textBoxPostalCode
            // 
            this.textBoxPostalCode.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPostalCode.Location = new System.Drawing.Point(275, 115);
            this.textBoxPostalCode.MaxLength = 10;
            this.textBoxPostalCode.Name = "textBoxPostalCode";
            this.textBoxPostalCode.Size = new System.Drawing.Size(129, 21);
            this.textBoxPostalCode.TabIndex = 20;
            this.textBoxPostalCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPostalCode_KeyPress);
            // 
            // labelPostalCode
            // 
            this.labelPostalCode.AutoSize = true;
            this.labelPostalCode.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPostalCode.Location = new System.Drawing.Point(13, 124);
            this.labelPostalCode.Name = "labelPostalCode";
            this.labelPostalCode.Size = new System.Drawing.Size(71, 13);
            this.labelPostalCode.TabIndex = 7;
            this.labelPostalCode.Text = "PostalCode";
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelState.Location = new System.Drawing.Point(37, 111);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(42, 13);
            this.labelState.TabIndex = 6;
            this.labelState.Text = "State/";
            // 
            // groupBoxTo
            // 
            this.groupBoxTo.Controls.Add(this.groupBoxPickupDetails);
            this.groupBoxTo.Controls.Add(this.groupBoxDeliveryDetails);
            this.groupBoxTo.Controls.Add(this.label8);
            this.groupBoxTo.Controls.Add(this.label5);
            this.groupBoxTo.Controls.Add(this.label4);
            this.groupBoxTo.Controls.Add(this.textBoxEmail);
            this.groupBoxTo.Controls.Add(this.textBoxPhone);
            this.groupBoxTo.Controls.Add(this.textBoxName);
            this.groupBoxTo.Controls.Add(this.textBoxToCompany);
            this.groupBoxTo.Controls.Add(this.labelToCompany);
            this.groupBoxTo.Controls.Add(this.labelName);
            this.groupBoxTo.Controls.Add(this.labelPhone);
            this.groupBoxTo.Controls.Add(this.labelEmail);
            this.groupBoxTo.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxTo.Location = new System.Drawing.Point(26, 133);
            this.groupBoxTo.Name = "groupBoxTo";
            this.groupBoxTo.Size = new System.Drawing.Size(446, 474);
            this.groupBoxTo.TabIndex = 4;
            this.groupBoxTo.TabStop = false;
            this.groupBoxTo.Text = "To";
            // 
            // groupBoxPickupDetails
            // 
            this.groupBoxPickupDetails.Controls.Add(this.label21);
            this.groupBoxPickupDetails.Controls.Add(this.label20);
            this.groupBoxPickupDetails.Controls.Add(this.label19);
            this.groupBoxPickupDetails.Controls.Add(this.label18);
            this.groupBoxPickupDetails.Controls.Add(this.label17);
            this.groupBoxPickupDetails.Controls.Add(this.label16);
            this.groupBoxPickupDetails.Controls.Add(this.label15);
            this.groupBoxPickupDetails.Controls.Add(this.label1);
            this.groupBoxPickupDetails.Controls.Add(this.textBoxPickupPostCode);
            this.groupBoxPickupDetails.Controls.Add(this.textBoxPickupState);
            this.groupBoxPickupDetails.Controls.Add(this.textBoxPickupCity);
            this.groupBoxPickupDetails.Controls.Add(this.textBoxPickupAddr2);
            this.groupBoxPickupDetails.Controls.Add(this.textBoxPickupAddr1);
            this.groupBoxPickupDetails.Location = new System.Drawing.Point(16, 138);
            this.groupBoxPickupDetails.Name = "groupBoxPickupDetails";
            this.groupBoxPickupDetails.Size = new System.Drawing.Size(413, 145);
            this.groupBoxPickupDetails.TabIndex = 9;
            this.groupBoxPickupDetails.TabStop = false;
            this.groupBoxPickupDetails.Text = "Pickup Details";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(96, 109);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 17);
            this.label21.TabIndex = 33;
            this.label21.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(96, 77);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 17);
            this.label20.TabIndex = 32;
            this.label20.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(96, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 17);
            this.label19.TabIndex = 31;
            this.label19.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(22, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(60, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Address1";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(22, 47);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 12;
            this.label17.Text = "Address2";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 113);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "PostalCode";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(37, 100);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 10;
            this.label15.Text = "State/";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(52, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "City";
            // 
            // textBoxPickupPostCode
            // 
            this.textBoxPickupPostCode.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPickupPostCode.Location = new System.Drawing.Point(274, 105);
            this.textBoxPickupPostCode.MaxLength = 10;
            this.textBoxPickupPostCode.Name = "textBoxPickupPostCode";
            this.textBoxPickupPostCode.Size = new System.Drawing.Size(129, 21);
            this.textBoxPickupPostCode.TabIndex = 14;
            // 
            // textBoxPickupState
            // 
            this.textBoxPickupState.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPickupState.Location = new System.Drawing.Point(119, 105);
            this.textBoxPickupState.MaxLength = 5;
            this.textBoxPickupState.Name = "textBoxPickupState";
            this.textBoxPickupState.Size = new System.Drawing.Size(152, 21);
            this.textBoxPickupState.TabIndex = 13;
            // 
            // textBoxPickupCity
            // 
            this.textBoxPickupCity.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPickupCity.Location = new System.Drawing.Point(119, 74);
            this.textBoxPickupCity.MaxLength = 17;
            this.textBoxPickupCity.Name = "textBoxPickupCity";
            this.textBoxPickupCity.Size = new System.Drawing.Size(286, 21);
            this.textBoxPickupCity.TabIndex = 12;
            // 
            // textBoxPickupAddr2
            // 
            this.textBoxPickupAddr2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPickupAddr2.Location = new System.Drawing.Point(119, 47);
            this.textBoxPickupAddr2.MaxLength = 25;
            this.textBoxPickupAddr2.Name = "textBoxPickupAddr2";
            this.textBoxPickupAddr2.Size = new System.Drawing.Size(286, 21);
            this.textBoxPickupAddr2.TabIndex = 11;
            // 
            // textBoxPickupAddr1
            // 
            this.textBoxPickupAddr1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPickupAddr1.Location = new System.Drawing.Point(119, 20);
            this.textBoxPickupAddr1.MaxLength = 25;
            this.textBoxPickupAddr1.Name = "textBoxPickupAddr1";
            this.textBoxPickupAddr1.Size = new System.Drawing.Size(286, 21);
            this.textBoxPickupAddr1.TabIndex = 10;
            // 
            // ShippingRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(930, 682);
            this.Controls.Add(this.groupBoxDeliveryInstructions);
            this.Controls.Add(this.richTextBoxMessageSummary);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonPrintLabel);
            this.Controls.Add(this.groupBoxTo);
            this.Controls.Add(this.groupBoxPackages);
            this.Controls.Add(this.groupBoxShippingService);
            this.Controls.Add(this.groupBoxShippingStatus);
            this.Controls.Add(this.groupBoxFrom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ShippingRequest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Axis - Shipping Request";
            this.Load += new System.EventHandler(this.ShippingRequest_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ShippingRequest_FormClosing);
            this.groupBoxFrom.ResumeLayout(false);
            this.groupBoxFrom.PerformLayout();
            this.groupBoxPackages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPackages)).EndInit();
            this.groupBoxShippingService.ResumeLayout(false);
            this.groupBoxShippingService.PerformLayout();
            this.groupBoxShippingStatus.ResumeLayout(false);
            this.groupBoxShippingStatus.PerformLayout();
            this.groupBoxDeliveryInstructions.ResumeLayout(false);
            this.groupBoxDeliveryInstructions.PerformLayout();
            this.groupBoxDeliveryDetails.ResumeLayout(false);
            this.groupBoxDeliveryDetails.PerformLayout();
            this.groupBoxTo.ResumeLayout(false);
            this.groupBoxTo.PerformLayout();
            this.groupBoxPickupDetails.ResumeLayout(false);
            this.groupBoxPickupDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxFrom;
        private System.Windows.Forms.GroupBox groupBoxPackages;
        private System.Windows.Forms.GroupBox groupBoxShippingService;
        private System.Windows.Forms.GroupBox groupBoxShippingStatus;
        private System.Windows.Forms.Button buttonPrintLabel;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.TextBox textBoxFromCompany;
        private System.Windows.Forms.ComboBox comboBoxServiceSelected;
        private System.Windows.Forms.TextBox textBoxBookedBy;
        private System.Windows.Forms.TextBox textBoxReferenceNumber;
        private System.Windows.Forms.Label labelBookedBy;
        private System.Windows.Forms.Label labelReferenceNumber;
        private System.Windows.Forms.Label labelServiceSelected;
        public System.Windows.Forms.Label labelPickUpTime;
        private System.Windows.Forms.Label labelJobDispatched;
        private System.Windows.Forms.Label labelDateTime1;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label labelJobBooked1;
        private System.Windows.Forms.DataGridView dataGridViewPackages;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.RichTextBox richTextBoxMessageSummary;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxFromPhone;
        private System.Windows.Forms.Label label1FromPhone;
        private System.Windows.Forms.TextBox textBoxPrefix;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxInstruction3;
        private System.Windows.Forms.TextBox textBoxInstruction2;
        private System.Windows.Forms.GroupBox groupBoxDeliveryInstructions;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxInstruction1;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.Label labelPhone;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelToCompany;
        private System.Windows.Forms.TextBox textBoxToCompany;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.TextBox textBoxPhone;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBoxDeliveryDetails;
        private System.Windows.Forms.TextBox textBoxAddr2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxAddr1;
        private System.Windows.Forms.Label labelAddress;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelDelivery;
        private System.Windows.Forms.TextBox textBoxCountry;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.TextBox textBoxState;
        private System.Windows.Forms.Label labelCountry;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.TextBox textBoxPostalCode;
        private System.Windows.Forms.Label labelPostalCode;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.GroupBox groupBoxTo;
        private System.Windows.Forms.GroupBox groupBoxPickupDetails;
        private System.Windows.Forms.TextBox textBoxPickupPostCode;
        private System.Windows.Forms.TextBox textBoxPickupState;
        private System.Windows.Forms.TextBox textBoxPickupCity;
        private System.Windows.Forms.TextBox textBoxPickupAddr2;
        private System.Windows.Forms.TextBox textBoxPickupAddr1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label22;
    }
}