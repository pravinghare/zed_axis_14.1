﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class Provide interface to edit items
    /// in Order from Inbox.
    /// </summary>
    public partial class ItemPackage : Form
    {
        #region Private Members
        private static ItemPackage m_ItemPackage;
        
        private int m_ItemCount;
        private DataTable m_ModifiedItem;
        private int m_rowIndex;
        private bool m_isSentItemOpen = false;
        FinancialEntities.Items item;
        #endregion

        #region  Properties


        public int RowIndex
        {
            get { return m_rowIndex; }
            set { m_rowIndex = value; }
        }

        public DataTable ModifiedItem
        {
            get { return m_ModifiedItem; }
            set { m_ModifiedItem = value; }
        }
       
        public int ItemCount
        {
            get { return m_ItemCount; }
            set { m_ItemCount = value; }
        }

        public bool IsSentItemOpen
        {
            get { return m_isSentItemOpen; }
            set { m_isSentItemOpen = value; }
        }
        
        #endregion

        #region Constructor

        public ItemPackage()
        {
            InitializeComponent();
            item = new FinancialEntities.Items();
        }

        public static ItemPackage GetInstance()
        {
            if (m_ItemPackage == null)
                m_ItemPackage = new ItemPackage();
            return m_ItemPackage;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// POpulate form controls with dataRow values.
        /// </summary>
        private void PopulateForm()
        {
            if (m_isSentItemOpen)
            {
                buttonOK.Enabled = false;
            }
            if (ModifiedItem != null)
            {
                DataRow dr = ModifiedItem.Rows[RowIndex];
                this.textBoxItemName.Text =  dr["itemName"].ToString();
                this.textBoxLength.Text =  dr["length"].ToString();
                this.textBoxWidth.Text = dr["width"].ToString();
                this.textBoxHeight.Text = dr["height"].ToString(); 
                this.textBoxWeight.Text = dr["weight"].ToString();
                if (dr["DangerousGood"].ToString() == "true")
                {
                    checkBoxDangerousGoods.Checked = true;
                }
                else
                {
                    checkBoxDangerousGoods.Checked = false;
                }
            }
        }

        /// <summary>
        /// Calculates the volume when click on "OK" button.
        /// </summary>
        private string CalculateVolume(double l, double w, double h)
        {
            double volume = 0;
            try
            {
                //volume = Convert.ToDouble(textBoxLength.Text) * Convert.ToDouble(textBoxWidth.Text) * Convert.ToDouble(textBoxHeight.Text);
                //Converting the length, width and height to meter.(1m = 100cm)
                volume = (l / 100) * (w / 100) * (h / 100);
            }
            catch(Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return string.Format("{0:0.000}", volume);
        }

       
        #endregion
        
        #region Event Handlers

        private void ItemPackage_Load(object sender, EventArgs e)
        {           
            this.PopulateForm();
        }

       
        private void buttonOK_Click(object sender, EventArgs e)
        {
            double l = 0;
            double wdth = 0;
            double h = 0;
            double w = 0;
            //Validate and save data and reflect the changes back
            //in datagrid of shipping Request.
            if (textBoxLength.Text.Trim() == string.Empty || textBoxWidth.Text.Trim() == string.Empty || textBoxHeight.Text.Trim() == string.Empty || textBoxWeight.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Please Enter the values for mandatory fields.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            string[] length = textBoxLength.Text.Split('.');
            string[] width = textBoxWidth.Text.Split('.');
            string[] height = textBoxHeight.Text.Split('.');
            string[] weight = textBoxWeight.Text.Split('.');

            if (length.Length > 2 || width.Length > 2 || height.Length > 2 || weight.Length > 2)
            {
                MessageBox.Show("Please Enter the values in proper format(eg: 4.3).", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            DataRow dr = ModifiedItem.Rows[RowIndex];
            dr["itemName"] = textBoxItemName.Text.Trim();
            if (length[0] == string.Empty && length[1] == string.Empty)
            {
                dr["length"] = 0;            
            }
            else
            {
                l = Convert.ToDouble(textBoxLength.Text.Trim());
                dr["length"] = System.Math.Round(l).ToString();
            }
            if (width[0] == string.Empty && width[1] == string.Empty)
            {
                dr["width"] = 0;            
            }
            else
            {
                wdth = Convert.ToDouble(textBoxWidth.Text.Trim());
                dr["width"] = System.Math.Round(wdth).ToString();
            }
            if (height[0] == string.Empty && height[1] == string.Empty)
            {
                dr["height"] = 0;            
            }
            else
            {
                h = Convert.ToDouble(textBoxHeight.Text.Trim());
                dr["height"] = System.Math.Round(h).ToString();
            }
            dr["Volume"] = CalculateVolume(l,wdth,h);
            if (textBoxWeight.Text.Trim() != string.Empty)
            {
                if (weight[0] == string.Empty && weight[1] == string.Empty)
                {
                    dr["weight"] = 0;                  
                }
                else
                { 
                    w = Convert.ToDouble(textBoxWeight.Text.Trim());
                    dr["weight"] = System.Math.Round(w).ToString();
                }
            }
            else
            {
                dr["weight"] = "0";
            }
            if (checkBoxDangerousGoods.Checked == true)
            {
                dr["DangerousGood"] = "true";
            }
            else
            {
                dr["DangerousGood"] = "false";
            }
            ModifiedItem.AcceptChanges();   
            this.Close();           
        }
     
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            m_ItemPackage = null;
        }

        private void textBoxLength_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validNumberForItem;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void textBoxWidth_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validNumberForItem;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void textBoxHeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validNumberForItem;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void textBoxWeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validNumberForItem;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }
     
        private void ItemPackage_FormClosing(object sender, FormClosingEventArgs e)
        {          
            m_ItemPackage = null;
        }

        #endregion                     
    }
}
