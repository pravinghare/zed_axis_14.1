﻿using System;
using System.Collections.Generic;
using System.Text;
using EDI.Constant;
using System.Globalization;
using DataProcessingBlocks;
using System.Xml.Serialization;
using TransactionImporter;
using System.Windows.Forms;
using EDI.Message;
using QuickBookEntities;
using System.Collections.ObjectModel;
using Streams;
using System.Xml;
using Interop.QBXMLRP2Lib;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class is used to Performed operation related to Items.
    /// </summary>
 
    public class QuickBooksMethods
    {
        #region Private Memebers

        string status = string.Empty;
        #endregion

        /// <summary>
        /// This method is used to get Custom Fileds from QuickBooks.
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="length"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void GetItemCustomFields(string itemName, string QBFileName, ref string length, ref string width, ref string height, ref string weight)
        {           
            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
            qbXMLMsgsRq.AppendChild(ItemQueryRq);
            ItemQueryRq.SetAttribute("requestID", "1");

            XmlElement FullName = pxmldoc.CreateElement("FullName");
            FullName.InnerText = itemName;
            ItemQueryRq.AppendChild(FullName);

            XmlElement OwnerId = pxmldoc.CreateElement("OwnerID");
            OwnerId.InnerText = "0";
            ItemQueryRq.AppendChild(OwnerId);

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string resp = string.Empty;
            try
            {
                DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch
            { }

            try
            {
                ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            finally
            {
                //Process response to get custom fileds i.e length, weidth, heigth.
                if (resp != string.Empty)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(resp);
                    string statusSeverity = string.Empty;
                    foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                    {
                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                    }
                    if (statusSeverity == "Info")
                    {
                        XmlNodeList itemNodeList = xmldoc.GetElementsByTagName("DataExtRet");
                        foreach (XmlNode nodes in itemNodeList)
                        {
                            XmlNode node = nodes.SelectSingleNode("DataExtName");
                            switch (node.InnerText)
                            {
                                case "Length":
                                    length = nodes.SelectSingleNode("DataExtValue").InnerText;
                                    break;

                                case "Width":
                                    width = nodes.SelectSingleNode("DataExtValue").InnerText;
                                    break;

                                case "Height":
                                    height = nodes.SelectSingleNode("DataExtValue").InnerText;
                                    break;

                                case "Weight":
                                    weight = nodes.SelectSingleNode("DataExtValue").InnerText;
                                    break;
                            }

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Check if Item is present in QuickBooks.
        /// </summary>
        public bool ItemQuery(string itemName, string QBFileName)
        {                    

            XmlDocument pxmldoc = new XmlDocument();
            pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
            pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXML = pxmldoc.CreateElement("QBXML");
            pxmldoc.AppendChild(qbXML);
            XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
            qbXML.AppendChild(qbXMLMsgsRq);
            qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
            XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
            qbXMLMsgsRq.AppendChild(ItemQueryRq);
            ItemQueryRq.SetAttribute("requestID", "1");

            XmlElement FullName = pxmldoc.CreateElement("FullName");
            FullName.InnerText = itemName;
            ItemQueryRq.AppendChild(FullName);

            XmlElement OwnerId = pxmldoc.CreateElement("OwnerID");
            OwnerId.InnerText = "0";
            ItemQueryRq.AppendChild(OwnerId);

            string pinput = pxmldoc.OuterXml;
            string ticket = string.Empty;
            string resp = string.Empty;
            try
            {
                DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.EndSession(DataProcessingBlocks.CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch
            { }

            try
            {
                ticket = DataProcessingBlocks.CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(QBFileName, QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(ticket, pinput);
                //Process response to get custom fileds i.e length, weidth, heigth.
                if (resp != string.Empty)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(resp);
                    string statusSeverity = string.Empty;
                    foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                    {
                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();                        
                    }
                    if (statusSeverity == "Info")
                    {
                        return true;
                    }
                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return true;
           
        }

        /// <summary>
        /// Create Item in Quickbooks if not present.
        /// </summary>
        public void CreateQBItem(string itemName, string QBFileName, string length, string width, string height, string weight)
        {
            try
            {
                DefaultAccountSettings defaultSettings = new DefaultAccountSettings();
                defaultSettings = defaultSettings.GetDefaultAccountSettings();

                string[] arr = new string[15];
                arr = itemName.Split(':');                
                #region Set Item Query

                for (int i = 0; i < arr.Length; i++)
                {
                    int a = 0;
                    int item = 0;
                    if (arr[i] != null && arr[i] != string.Empty)
                    {                       
                        #region Passing Items Query
                        XmlDocument pxmldoc = new XmlDocument();
                        pxmldoc.AppendChild(pxmldoc.CreateXmlDeclaration("1.0", null, null));
                        pxmldoc.AppendChild(pxmldoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                        XmlElement qbXML = pxmldoc.CreateElement("QBXML");
                        pxmldoc.AppendChild(qbXML);
                        XmlElement qbXMLMsgsRq = pxmldoc.CreateElement("QBXMLMsgsRq");
                        qbXML.AppendChild(qbXMLMsgsRq);
                        qbXMLMsgsRq.SetAttribute("onError", "stopOnError");
                        XmlElement ItemQueryRq = pxmldoc.CreateElement("ItemQueryRq");
                        qbXMLMsgsRq.AppendChild(ItemQueryRq);
                        ItemQueryRq.SetAttribute("requestID", "1");

                        if (i > 0)
                        {
                            if (arr[i] != null && arr[i] != string.Empty)
                            {
                                XmlElement FullName = pxmldoc.CreateElement("FullName");

                                for (item = 0; item <= i; item++)
                                {
                                    if (arr[item].Trim() != string.Empty)
                                    {
                                        FullName.InnerText += arr[item].Trim() + ":";
                                    }
                                }
                                if (FullName.InnerText != string.Empty)
                                {
                                    ItemQueryRq.AppendChild(FullName);
                                }

                            }
                        }
                        else
                        {
                            XmlElement FullName = pxmldoc.CreateElement("FullName");
                            FullName.InnerText = arr[i];
                            //FullName.InnerText = dr["ItemFullName"].ToString();
                            ItemQueryRq.AppendChild(FullName);
                        }

                        string pinput = pxmldoc.OuterXml;

                        string resp = string.Empty;
                         try
                         {
                              CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                         }
                         catch { }
                         try
                         {
                             CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                             resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, pinput);
                         }
                        catch (Exception ex)
                        {
                            CommonUtilities.WriteErrorLog(ex.Message);
                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                        }
                        finally
                        {

                            if (resp != string.Empty)
                            {
                                System.Xml.XmlDocument outputXMLDoc = new System.Xml.XmlDocument();
                                outputXMLDoc.LoadXml(resp);
                                string StatusSeverity = string.Empty;
                                foreach (System.Xml.XmlNode oNode in outputXMLDoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemQueryRs"))
                                {
                                    StatusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                }
                                outputXMLDoc.RemoveAll();
                                if (StatusSeverity == "Error" || StatusSeverity == "Warn")
                                {
                                    //two types of when item is not prssent in QuickBook and we create item using hardcoded values

                                    if (defaultSettings.Type == "NonInventoryPart")
                                    {
                                        #region Set Item Non inverntory Query

                                        XmlDocument ItemNonInvendoc = new XmlDocument();
                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        //ItemNonInvendoc.AppendChild(ItemNonInvendoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                                        XmlElement qbXMLINI = ItemNonInvendoc.CreateElement("QBXML");
                                        ItemNonInvendoc.AppendChild(qbXMLINI);
                                        XmlElement qbXMLMsgsRqINI = ItemNonInvendoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLINI.AppendChild(qbXMLMsgsRqINI);
                                        qbXMLMsgsRqINI.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemNonInventoryAddRq = ItemNonInvendoc.CreateElement("ItemNonInventoryAddRq");
                                        qbXMLMsgsRqINI.AppendChild(ItemNonInventoryAddRq);
                                        ItemNonInventoryAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemNonInventoryAdd = ItemNonInvendoc.CreateElement("ItemNonInventoryAdd");
                                        ItemNonInventoryAddRq.AppendChild(ItemNonInventoryAdd);

                                        XmlElement ININame = ItemNonInvendoc.CreateElement("Name");
                                        ININame.InnerText = arr[i];
                                        ItemNonInventoryAdd.AppendChild(ININame);

                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement INIChildFullName = ItemNonInvendoc.CreateElement("FullName");

                                                for (a = 0; a <= i - 1; a++)
                                                {
                                                    if (arr[a].Trim() != string.Empty)
                                                    {
                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                    }
                                                }
                                                if (INIChildFullName.InnerText != string.Empty)
                                                {
                                                    //Adding Parent
                                                    XmlElement INIParent = ItemNonInvendoc.CreateElement("ParentRef");
                                                    ItemNonInventoryAdd.AppendChild(INIParent);

                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                            }
                                        }


                                        //Adding Tax Code Element.
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                XmlElement INISalesTaxCodeRef = ItemNonInvendoc.CreateElement("SalesTaxCodeRef");
                                                ItemNonInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INIFullName = ItemNonInvendoc.CreateElement("FullName");
                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                            }
                                        }

                                        XmlElement INISalesAndPurchase = ItemNonInvendoc.CreateElement("SalesOrPurchase");
                                        bool IsPresent = false;
                                        //ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);


                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement INIIncomeAccountRef = ItemNonInvendoc.CreateElement("AccountRef");
                                            INISalesAndPurchase.AppendChild(INIIncomeAccountRef);

                                            XmlElement INIAccountRefFullName = ItemNonInvendoc.CreateElement("FullName");
                                            //INIFullName.InnerText = "Sales";
                                            INIAccountRefFullName.InnerText = defaultSettings.IncomeAccount;
                                            INIIncomeAccountRef.AppendChild(INIAccountRefFullName);

                                            IsPresent = true;
                                        }

                                        if (IsPresent == true)
                                        {
                                            ItemNonInventoryAdd.AppendChild(INISalesAndPurchase);
                                        }


                                        string ItemNonInvendocinput = ItemNonInvendoc.OuterXml;

                                        //ItemNonInvendoc.Save("C://ItemNonInvendoc.xml");
                                        string respItemNonInvendoc = string.Empty;
                                        try
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        }
                                        catch { }
                                        try
                                        {
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);

                                            respItemNonInvendoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemNonInvendocinput);
                                            if (respItemNonInvendoc != string.Empty)
                                            {
                                                XmlDocument xmldoc = new XmlDocument();
                                                xmldoc.LoadXml(respItemNonInvendoc);
                                                string statusSeverity = string.Empty;
                                                string statusMessage = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                    statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    if (i == (arr.Length - 1))
                                                    {
                                                        if (statusSeverity == "Info")
                                                        {
                                                            //Create Length Custom Field.
                                                            ItemDataFieldsQuery(itemName, "Length", length);
                                                            //Create Width Custom Field
                                                            ItemDataFieldsQuery(itemName, "Width", width);
                                                            //Create Height Custom Field
                                                            ItemDataFieldsQuery(itemName, "Heigth", height);
                                                            //Create Weight Custom field.
                                                            ItemDataFieldsQuery(itemName, "Weight", weight);

                                                        }
                                                    }
                                                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                    {
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }

                                        #endregion
                                    }
                                    else if (defaultSettings.Type == "Service")
                                    {
                                        #region Set Item Service Query

                                        XmlDocument ItemServiceAdddoc = new XmlDocument();
                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemServiceAdddoc.AppendChild(ItemServiceAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXMLIS = ItemServiceAdddoc.CreateElement("QBXML");
                                        ItemServiceAdddoc.AppendChild(qbXMLIS);
                                        XmlElement qbXMLMsgsRqIS = ItemServiceAdddoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemServiceAddRq = ItemServiceAdddoc.CreateElement("ItemServiceAddRq");
                                        qbXMLMsgsRqIS.AppendChild(ItemServiceAddRq);
                                        ItemServiceAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemServiceAdd = ItemServiceAdddoc.CreateElement("ItemServiceAdd");
                                        ItemServiceAddRq.AppendChild(ItemServiceAdd);

                                        XmlElement NameIS = ItemServiceAdddoc.CreateElement("Name");
                                        NameIS.InnerText = arr[i];
                                        ItemServiceAdd.AppendChild(NameIS);

                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement INIChildFullName = ItemServiceAdddoc.CreateElement("FullName");

                                                for (a = 0; a <= i - 1; a++)
                                                {
                                                    if (arr[a].Trim() != string.Empty)
                                                    {
                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                    }
                                                }
                                                if (INIChildFullName.InnerText != string.Empty)
                                                {
                                                    //Adding Parent
                                                    XmlElement INIParent = ItemServiceAdddoc.CreateElement("ParentRef");
                                                    ItemServiceAdd.AppendChild(INIParent);

                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                            }
                                        }

                                        //Adding Tax code Element.
                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                XmlElement INISalesTaxCodeRef = ItemServiceAdddoc.CreateElement("SalesTaxCodeRef");
                                                ItemServiceAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INISTCodeRefFullName = ItemServiceAdddoc.CreateElement("FullName");
                                                INISTCodeRefFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INISTCodeRefFullName);
                                            }
                                        }

                                        XmlElement ISSalesAndPurchase = ItemServiceAdddoc.CreateElement("SalesOrPurchase");
                                        bool IsPresent = false;

                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement ISIncomeAccountRef = ItemServiceAdddoc.CreateElement("AccountRef");
                                            ISSalesAndPurchase.AppendChild(ISIncomeAccountRef);

                                            //Adding IncomeAccount FullName.
                                            XmlElement ISFullName = ItemServiceAdddoc.CreateElement("FullName");
                                            ISFullName.InnerText = defaultSettings.IncomeAccount;
                                            ISIncomeAccountRef.AppendChild(ISFullName);

                                            IsPresent = true;
                                        }

                                        if (IsPresent == true)
                                        {
                                            ItemServiceAdd.AppendChild(ISSalesAndPurchase);
                                        }

                                        string ItemServiceAddinput = ItemServiceAdddoc.OuterXml;
                                        string respItemServicedoc = string.Empty;
                                        try
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        }
                                        catch { }
                                        try
                                        {
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            respItemServicedoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemServiceAddinput);
                                            if (respItemServicedoc != string.Empty)
                                            {
                                                XmlDocument xmldoc = new XmlDocument();
                                                xmldoc.LoadXml(respItemServicedoc);
                                                string statusSeverity = string.Empty;
                                                string statusMessage = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemServiceAddRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                    statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    //for last item in array
                                                    if (i == (arr.Length - 1))
                                                    {
                                                        if (statusSeverity == "Info")
                                                        {
                                                            //Create Length Custom Field.
                                                            ItemDataFieldsQuery(itemName, "Length", length);
                                                            //Create Width Custom Field
                                                            ItemDataFieldsQuery(itemName, "Width", width);
                                                            //Create Height Custom Field
                                                            ItemDataFieldsQuery(itemName, "Height", height);
                                                            //Create Weight Custom Field
                                                            ItemDataFieldsQuery(itemName, "Weight", weight);

                                                        }
                                                    }
                                                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                    {
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }

                                        #endregion
                                    }
                                    else if (defaultSettings.Type == "InventoryPart")
                                    {
                                        #region Set Item Inventory Query
                                        XmlDocument ItemInventoryAdddoc = new XmlDocument();
                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateXmlDeclaration("1.0", null, null));
                                        ItemInventoryAdddoc.AppendChild(ItemInventoryAdddoc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
                                        XmlElement qbXMLIS = ItemInventoryAdddoc.CreateElement("QBXML");
                                        ItemInventoryAdddoc.AppendChild(qbXMLIS);
                                        XmlElement qbXMLMsgsRqIS = ItemInventoryAdddoc.CreateElement("QBXMLMsgsRq");
                                        qbXMLIS.AppendChild(qbXMLMsgsRqIS);
                                        qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");
                                        XmlElement ItemInventoryAddRq = ItemInventoryAdddoc.CreateElement("ItemInventoryAddRq");
                                        qbXMLMsgsRqIS.AppendChild(ItemInventoryAddRq);
                                        ItemInventoryAddRq.SetAttribute("requestID", "1");

                                        XmlElement ItemInventoryAdd = ItemInventoryAdddoc.CreateElement("ItemInventoryAdd");
                                        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

                                        XmlElement NameIS = ItemInventoryAdddoc.CreateElement("Name");
                                        NameIS.InnerText = arr[i];
                                        ItemInventoryAdd.AppendChild(NameIS);

                                        if (i > 0)
                                        {
                                            if (arr[i] != null && arr[i] != string.Empty)
                                            {
                                                XmlElement INIChildFullName = ItemInventoryAdddoc.CreateElement("FullName");

                                                for (a = 0; a <= i - 1; a++)
                                                {
                                                    if (arr[a].Trim() != string.Empty)
                                                    {
                                                        INIChildFullName.InnerText += arr[a].Trim() + ":";
                                                    }
                                                }
                                                if (INIChildFullName.InnerText != string.Empty)
                                                {
                                                    //Adding Parent
                                                    XmlElement INIParent = ItemInventoryAdddoc.CreateElement("ParentRef");
                                                    ItemInventoryAdd.AppendChild(INIParent);

                                                    INIParent.AppendChild(INIChildFullName);
                                                }
                                            }
                                        }

                                        if (defaultSettings.TaxCode != string.Empty)
                                        {
                                            if (defaultSettings.TaxCode.Length < 4)
                                            {
                                                //Adding Tax code Element.
                                                XmlElement INISalesTaxCodeRef = ItemInventoryAdddoc.CreateElement("SalesTaxCodeRef");
                                                ItemInventoryAdd.AppendChild(INISalesTaxCodeRef);

                                                XmlElement INIFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                                INIFullName.InnerText = defaultSettings.TaxCode;
                                                INISalesTaxCodeRef.AppendChild(INIFullName);
                                            }
                                        }


                                        //Adding IncomeAccountRef
                                        if (defaultSettings.IncomeAccount != string.Empty)
                                        {
                                            XmlElement INIIncomeAccountRef = ItemInventoryAdddoc.CreateElement("IncomeAccountRef");
                                            ItemInventoryAdd.AppendChild(INIIncomeAccountRef);

                                            XmlElement INIIncomeAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIIncomeAccountFullName.InnerText = defaultSettings.IncomeAccount;
                                            INIIncomeAccountRef.AppendChild(INIIncomeAccountFullName);
                                        }

                                        //Adding COGSAccountRef
                                        if (defaultSettings.COGSAccount != string.Empty)
                                        {
                                            XmlElement INICOGSAccountRef = ItemInventoryAdddoc.CreateElement("COGSAccountRef");
                                            ItemInventoryAdd.AppendChild(INICOGSAccountRef);

                                            XmlElement INICOGSAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INICOGSAccountFullName.InnerText = defaultSettings.COGSAccount;
                                            INICOGSAccountRef.AppendChild(INICOGSAccountFullName);
                                        }

                                        //Adding AssetAccountRef
                                        if (defaultSettings.AssetAccount != string.Empty)
                                        {
                                            XmlElement INIAssetAccountRef = ItemInventoryAdddoc.CreateElement("AssetAccountRef");
                                            ItemInventoryAdd.AppendChild(INIAssetAccountRef);

                                            XmlElement INIAssetAccountFullName = ItemInventoryAdddoc.CreateElement("FullName");
                                            INIAssetAccountFullName.InnerText = defaultSettings.AssetAccount;
                                            INIAssetAccountRef.AppendChild(INIAssetAccountFullName);
                                        }

                                        string ItemInventoryAddinput = ItemInventoryAdddoc.OuterXml;
                                        string respItemInventoryAddinputdoc = string.Empty;
                                        try
                                        {
                                            CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
                                        }
                                        catch { }

                                        try
                                        {
                                            CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                                            respItemInventoryAddinputdoc = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, ItemInventoryAddinput);
                                            if (respItemInventoryAddinputdoc != string.Empty)
                                            {
                                                XmlDocument xmldoc = new XmlDocument();
                                                xmldoc.LoadXml(respItemInventoryAddinputdoc);
                                                string statusSeverity = string.Empty;
                                                string statusMessage = string.Empty;
                                                foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/ItemInventoryAddRs"))
                                                {
                                                    statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                                                    statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                                                    if (i == (arr.Length - 1))
                                                    {
                                                        if (statusSeverity == "Info")
                                                        {
                                                            //Create Length Custom Field.
                                                            ItemDataFieldsQuery(itemName, "Length", length);
                                                            //Create Width Custom Field
                                                            ItemDataFieldsQuery(itemName, "Width", width);
                                                            //Create Height Custom Field
                                                            ItemDataFieldsQuery(itemName, "Height", height);
                                                            //Create Weight Custom field.
                                                            ItemDataFieldsQuery(itemName, "Weight", weight);
                                                        }
                                                    }
                                                    if (statusSeverity == "Error" || statusSeverity == "Warn")
                                                    {
                                                        CommonUtilities.WriteErrorLog(statusMessage);
                                                    }
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }

                                        #endregion
                                    }

                                }
                            }

                        }

                        #endregion
                    }
                }

                #endregion
            }
            catch(Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message.ToString());
                CommonUtilities.WriteErrorLog(ex.StackTrace.ToString());
            }
        }

        /// <summary>
        /// Update Item with data if already present in QuickBooks.
        /// </summary>
        public void UpdateQBItem(string itemName, string QBFileName, string length, string width, string height,string weight)
        {
            DefaultAccountSettings defaultSettings = new DefaultAccountSettings();
            defaultSettings.GetDefaultAccountSettings();
            
            //Update Custom Fields
            ItemDataFieldsQuery(itemName, "Length", length);
            ItemDataFieldsQuery(itemName, "Width", width);
            ItemDataFieldsQuery(itemName, "Height", height);
            ItemDataFieldsQuery(itemName, "Weight", weight);
        }


        /// <summary>
        /// Check whether Custom fields exist in QuickBooks.
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="DataExtValue"></param>
        /// <param name="DataExtValue"></param>
        public void ItemDataFieldsQuery(string itemName , string DataExtName, string DataExtValue)
        {

            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", null, null));
            doc.AppendChild(doc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXMLIS = doc.CreateElement("QBXML");
            doc.AppendChild(qbXMLIS);
            XmlElement qbXMLMsgsRqIS = doc.CreateElement("QBXMLMsgsRq");
            qbXMLIS.AppendChild(qbXMLMsgsRqIS);
            qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");

            XmlElement DataExtAddRq = doc.CreateElement("DataExtDefQueryRq");
            qbXMLMsgsRqIS.AppendChild(DataExtAddRq);
            DataExtAddRq.SetAttribute("requestID", "1");
           
            XmlElement ObjectAssign = doc.CreateElement("AssignToObject");
            ObjectAssign.InnerText = "Item";
            DataExtAddRq.AppendChild(ObjectAssign);

            XmlElement includeRetElement = doc.CreateElement("IncludeRetElement");
            includeRetElement.InnerText = "DataExtName";
            DataExtAddRq.AppendChild(includeRetElement);
         
            string xmlInput = doc.OuterXml;
            string resp = string.Empty;
            try
            {
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch { }

            try
            {
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, xmlInput);
                if (resp != string.Empty)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(resp);
                    string dataExtName = string.Empty;
                    //This check whether current  query response contain dataExtName.
                    bool IsExist = false;
                    XmlNodeList itemNodeList = xmldoc.GetElementsByTagName("DataExtDefRet");

                    foreach (XmlNode nodes in itemNodeList)
                    {                       
                        if(IsExist == true)
                        {
                           break;
                        }
                        XmlNode node = nodes.SelectSingleNode("DataExtName");
                        if (node.InnerText == DataExtName)
                        {
                            switch (DataExtName)
                            {
                                case "Length":
                                    IsExist = true;
                                    UpdateItemDataExtfields(itemName, DataExtName, DataExtValue);
                                    break;
                                case "Width":
                                    IsExist = true;
                                    UpdateItemDataExtfields(itemName, DataExtName, DataExtValue);
                                    break;
                                case "Height":
                                    IsExist = true;
                                    UpdateItemDataExtfields(itemName, DataExtName, DataExtValue);
                                    break;
                                case "Weight":
                                    IsExist = true;
                                    UpdateItemDataExtfields(itemName, DataExtName, DataExtValue);
                                    break;
                            }
                        }
                    }
                    if (IsExist == false)
                    { 
                        //Define Custom fields.
                        DefineItemDataExtFields(itemName, DataExtName, DataExtValue);
                        //Create Custom fields. 
                        CreateItemDataExtFields(itemName, DataExtName, DataExtValue);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }

        /// <summary>
        /// Define an Custom Fileds for an Item.
        /// </summary>
        /// <param name="ItemName"></param>
        /// <param name="DataExtName"></param>
        public void DefineItemDataExtFields(string ItemName, string DataExtName, string DataExtValue)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", null, null));
            doc.AppendChild(doc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXMLIS = doc.CreateElement("QBXML");
            doc.AppendChild(qbXMLIS);
            XmlElement qbXMLMsgsRqIS = doc.CreateElement("QBXMLMsgsRq");
            qbXMLIS.AppendChild(qbXMLMsgsRqIS);
            qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");

            XmlElement DataExtAddRq = doc.CreateElement("DataExtDefAddRq");
            qbXMLMsgsRqIS.AppendChild(DataExtAddRq);
            DataExtAddRq.SetAttribute("requestID", "1");

            XmlElement DataExtAdd = doc.CreateElement("DataExtDefAdd");
            DataExtAddRq.AppendChild(DataExtAdd);

            XmlElement ownerID = doc.CreateElement("OwnerID");
            ownerID.InnerText = "0";
            DataExtAdd.AppendChild(ownerID);

            XmlElement dataExtName = doc.CreateElement("DataExtName");
            dataExtName.InnerText = DataExtName;
            DataExtAdd.AppendChild(dataExtName);

            XmlElement dataExtType = doc.CreateElement("DataExtType");
            dataExtType.InnerText = "STR255TYPE";
            DataExtAdd.AppendChild(dataExtType);

            XmlElement assignToObject = doc.CreateElement("AssignToObject");
            assignToObject.InnerText = "Item";
            DataExtAdd.AppendChild(assignToObject);

            string xmlInput = doc.OuterXml;
            string resp = string.Empty;
            try
            {
                CommonUtilities.GetInstance().QBRequestProcessor.EndSession(CommonUtilities.GetInstance().QBSessionTicket);
            }
            catch { }

            try
            {
                CommonUtilities.GetInstance().QBSessionTicket = CommonUtilities.GetInstance().QBRequestProcessor.BeginSession(DataProcessingBlocks.CommonUtilities.GetAppSettingValue("QBFILE"), Interop.QBXMLRP2Lib.QBFileMode.qbFileOpenDoNotCare);
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, xmlInput);
                if (resp != string.Empty)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(resp);
                    string statusSeverity = string.Empty;
                    string statusMessage = string.Empty;
                    foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/DataExtDefAddRs"))
                    {
                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                        if (statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            CommonUtilities.WriteErrorLog(statusMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
        }       

        /// <summary>
        /// Create an custom fields for an Item.
        /// </summary>
        public void CreateItemDataExtFields(string ItemName, string DataExtName, string DataExtValue)
        {           
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", null, null));
            doc.AppendChild(doc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXMLIS = doc.CreateElement("QBXML");
            doc.AppendChild(qbXMLIS);
            XmlElement qbXMLMsgsRqIS = doc.CreateElement("QBXMLMsgsRq");
            qbXMLIS.AppendChild(qbXMLMsgsRqIS);
            qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");

            XmlElement DataExtAddRq = doc.CreateElement("DataExtAddRq");
            qbXMLMsgsRqIS.AppendChild(DataExtAddRq);
            DataExtAddRq.SetAttribute("requestID", "1");

            XmlElement DataExtAdd = doc.CreateElement("DataExtAdd");
            DataExtAddRq.AppendChild(DataExtAdd);

            XmlElement ownerID = doc.CreateElement("OwnerID");
            ownerID.InnerText = "0";
            DataExtAdd.AppendChild(ownerID);

            XmlElement dataExtName = doc.CreateElement("DataExtName");
            dataExtName.InnerText = DataExtName;
            DataExtAdd.AppendChild(dataExtName);

            XmlElement ListDataType = doc.CreateElement("ListDataExtType");
            ListDataType.InnerText = "Item";
            DataExtAdd.AppendChild(ListDataType);

            XmlElement ListObjRef = doc.CreateElement("ListObjRef");
            DataExtAdd.AppendChild(ListObjRef);

            XmlElement ListName = doc.CreateElement("FullName");
            ListName.InnerText = ItemName;
            ListObjRef.AppendChild(ListName);

            XmlElement dataExtValue = doc.CreateElement("DataExtValue");
            dataExtValue.InnerText = DataExtValue;
            DataExtAdd.AppendChild(dataExtValue);

            string xmlInput = doc.OuterXml;
            string resp = string.Empty;
           

             try
             {
                 resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, xmlInput);
                 if (resp != string.Empty)
                 {
                     XmlDocument xmldoc = new XmlDocument();
                     xmldoc.LoadXml(resp);
                     string statusSeverity = string.Empty;
                     string statusMessage = string.Empty;
                     foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/DataExtAddRs"))
                     {
                         statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                         statusMessage = oNode.Attributes["statusMessage"].Value.ToString();                        
                         if (statusSeverity == "Error" || statusSeverity == "Warn")
                         {
                             CommonUtilities.WriteErrorLog(statusMessage);
                         }
                     }
                 }
             }
             catch(Exception ex)
             {
                 CommonUtilities.WriteErrorLog(ex.Message);
                 CommonUtilities.WriteErrorLog(ex.StackTrace);
             }            
        }

        /// <summary>
        /// Update Custom Fieds of an Item.
        /// </summary>
        /// <param name="ItemName"></param>
        /// <param name="DataExtName"></param>
        /// <param name="DataExtValue"></param>
        public void UpdateItemDataExtfields(string ItemName, string DataExtName, string DataExtValue)
        {
            XmlDocument doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", null, null));
            doc.AppendChild(doc.CreateProcessingInstruction("qbxml", "version=\"" + CommonUtilities.GetInstance().SuportedVersion + "\""));
            XmlElement qbXMLIS = doc.CreateElement("QBXML");
            doc.AppendChild(qbXMLIS);
            XmlElement qbXMLMsgsRqIS = doc.CreateElement("QBXMLMsgsRq");
            qbXMLIS.AppendChild(qbXMLMsgsRqIS);
            qbXMLMsgsRqIS.SetAttribute("onError", "stopOnError");

            XmlElement DataExtAddRq = doc.CreateElement("DataExtModRq");
            qbXMLMsgsRqIS.AppendChild(DataExtAddRq);
            DataExtAddRq.SetAttribute("requestID", "1");

            XmlElement DataExtAdd = doc.CreateElement("DataExtMod");
            DataExtAddRq.AppendChild(DataExtAdd);

            XmlElement ownerID = doc.CreateElement("OwnerID");
            ownerID.InnerText = "0";
            DataExtAdd.AppendChild(ownerID);

            XmlElement dataExtName = doc.CreateElement("DataExtName");
            dataExtName.InnerText = DataExtName;
            DataExtAdd.AppendChild(dataExtName);

            XmlElement ListDataType = doc.CreateElement("ListDataExtType");
            ListDataType.InnerText = "Item";
            DataExtAdd.AppendChild(ListDataType);

            XmlElement ListObjRef = doc.CreateElement("ListObjRef");
            DataExtAdd.AppendChild(ListObjRef);

            XmlElement ListName = doc.CreateElement("FullName");
            ListName.InnerText = ItemName;
            ListObjRef.AppendChild(ListName);

            XmlElement dataExtValue = doc.CreateElement("DataExtValue");
            dataExtValue.InnerText = DataExtValue;
            DataExtAdd.AppendChild(dataExtValue);

            string xmlInput = doc.OuterXml;
            string resp = string.Empty;


            try
            {
                resp = CommonUtilities.GetInstance().QBRequestProcessor.ProcessRequest(CommonUtilities.GetInstance().QBSessionTicket, xmlInput);
                if (resp != string.Empty)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(resp);
                    string statusSeverity = string.Empty;
                    string statusMessage = string.Empty;
                    foreach (System.Xml.XmlNode oNode in xmldoc.SelectNodes("/QBXML/QBXMLMsgsRs/DataExtModRs"))
                    {
                        statusSeverity = oNode.Attributes["statusSeverity"].Value.ToString();
                        statusMessage = oNode.Attributes["statusMessage"].Value.ToString();
                        if (statusSeverity == "Error" || statusSeverity == "Warn")
                        {
                            CommonUtilities.WriteErrorLog(statusMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }            
        }
    }
}
