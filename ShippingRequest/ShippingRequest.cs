﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using EDI.Constant;
using FinancialEntities;
using System.Collections.ObjectModel;
using System.Web.Services.Protocols;
using System.Net;
using Barcode;
using System.Drawing.Printing;
using System.Management;
using TnTWebServiceTesting;
using TnTWebServiceProduction;
using DBConnection;
using TSCActiveX;

namespace DataProcessingBlocks
{
    /// <summary>
    /// This class provide order Details populated into shipping Request form 
    /// and save Booking to Allied Express.
    /// </summary>
    public partial class ShippingRequest : Form
    {

        #region Private Members

        private static ShippingRequest m_ShippingRequest = null;             
        int totalItems = 0;
        double totalWeight = 0;
        double totalVolumes = 0.0;
        AlliedExpress alliedExpress;
        private string m_mailProtocol;
        private string m_connectedSoftware;
        private string m_QbCompanyFile;
        private int m_messageID;
        private bool m_IsSentItemOpen = false;
        string length = string.Empty;
        string width = string.Empty;
        string height = string.Empty;
        string weight = string.Empty;       
        string parcelID = string.Empty;
        private DataTable m_iemDataTable;
        string consignmentNumber =  string.Empty;
        string consignmentNote = string.Empty;
        string cannoteNumber = string.Empty;
        string attachment = string.Empty;
        string printerName = string.Empty;

        TSCLIBClass tsc;
       
        QuickBooksMethods QBMethods;
        //TnTWebService Test
        TnTWebServiceTesting.TnTWebService serviceTest;
        TnTWebServiceTesting.Job jobTest;
        TnTWebServiceTesting.Account accountTest;
        
        //TnTWebService Production
        TnTWebServiceProduction.TnTWebService serviceProduction;
        TnTWebServiceProduction.Job jobProduction;
        TnTWebServiceProduction.Account accountProduction;       
        Items item;
        Dictionary dictionary;
       
       //Getting current version of System. BUG(676)
       string countryName = string.Empty;
       //BarcodeLib b;
       //Image img;
        #endregion

        #region Properties

        public DataTable ItemDataTable
        {
            get { return m_iemDataTable; }
            set { m_iemDataTable = value; }
        }
       
        //set the mailProtocol as Ebay or OSCommerce.
        public string MailProtocol
        {
            get { return m_mailProtocol; }
            set { m_mailProtocol = value; }
        }

        public string ConnectedSoftware
        {
            get { return m_connectedSoftware; }
            set { m_connectedSoftware = value; }
        }

        public string QBCompanyFile
        {
            get { return m_QbCompanyFile; }
            set { m_QbCompanyFile = value; }
        }

        public int MessageID
        {
            get { return m_messageID; }
            set { m_messageID = value; }
        }

        public bool IsSentItemOpen
        {
            get { return m_IsSentItemOpen; }
            set { m_IsSentItemOpen = value; }
        }

        #endregion

        #region Constructor

        public ShippingRequest()
        {
            InitializeComponent();
           
            QBMethods = new QuickBooksMethods();
            dictionary = new Dictionary();           
        }     

        #endregion

        #region Static Methods

        public static ShippingRequest GetInstance()
        {
            if (m_ShippingRequest == null)
                m_ShippingRequest = new ShippingRequest();
            return m_ShippingRequest;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind the columns with respective columns and headertext.
        /// </summary>
        private void DataGridColumnBinding()
        {
            dataGridViewPackages.Columns.Add("Item","Item");
            dataGridViewPackages.Columns.Add("Quantity","Qty");
            dataGridViewPackages.Columns.Add("length","length");
            dataGridViewPackages.Columns.Add("width","width");
            dataGridViewPackages.Columns.Add("height","height");
            dataGridViewPackages.Columns.Add("volume","vol");
            dataGridViewPackages.Columns.Add("weight","kg");
            dataGridViewPackages.Columns.Add("DangerousGood","DGood Flag");
            dataGridViewPackages.Columns["DangerousGood"].Visible = false;
            //Bind DataPropertyName
            dataGridViewPackages.Columns["Item"].DataPropertyName = ShippingRequestColumns.itemName.ToString();
            dataGridViewPackages.Columns["Quantity"].DataPropertyName = ShippingRequestColumns.Quantity.ToString();
            dataGridViewPackages.Columns["length"].DataPropertyName = ShippingRequestColumns.length.ToString();
            dataGridViewPackages.Columns["width"].DataPropertyName = ShippingRequestColumns.width.ToString();
            dataGridViewPackages.Columns["height"].DataPropertyName = ShippingRequestColumns.height.ToString();
            dataGridViewPackages.Columns["volume"].DataPropertyName = ShippingRequestColumns.volume.ToString();
            dataGridViewPackages.Columns["weight"].DataPropertyName = ShippingRequestColumns.weight.ToString();
            dataGridViewPackages.Columns["DangerousGood"].DataPropertyName = ShippingRequestColumns.DangerousGood.ToString();
        }

        /// <summary>
        /// Populate ComboBox ServiceLevel.
        /// </summary>
        private void PopulateServiceLevel()
        {
            comboBoxServiceSelected.Items.Add("-Select-");
            comboBoxServiceSelected.Items.Add(Constants.OvernightExpress);
            comboBoxServiceSelected.Items.Add(Constants.Local);
            comboBoxServiceSelected.Items.Add(Constants.RoadExpress);
            comboBoxServiceSelected.Items.Add(Constants.SameDay);
            comboBoxServiceSelected.Items.Add(Constants.International);
            comboBoxServiceSelected.Items.Add(Constants.Saturday);
            comboBoxServiceSelected.Items.Add(Constants.Pallet);
            comboBoxServiceSelected.SelectedIndex = 0;
        }

        /// <summary>
        /// Populate the Shhipping Request from Order Details received from Ebay or OScommerce.
        /// </summary>
        private void PopulateShippingRequest()
        {
            string index = string.Empty;
            if (!m_IsSentItemOpen)
            {
                if (alliedExpress != null)
                {
                    this.textBoxFromCompany.Text = alliedExpress.FromCompanyName;
                    this.textBoxFromPhone.Text = alliedExpress.FromCompanyPhone;
                    this.textBoxPrefix.Text = alliedExpress.Prefix.ToUpper();
                    this.textBoxName.Text = alliedExpress.CustomerName;
                    this.textBoxToCompany.Text = alliedExpress.ToCompanyName;
                    this.textBoxAddr1.Text = alliedExpress.AddressLine1;
                    this.textBoxAddr2.Text = alliedExpress.AddressLine2;
                    this.textBoxCity.Text = alliedExpress.City;
                    this.textBoxState.Text = alliedExpress.State;
                    this.textBoxPostalCode.Text = alliedExpress.PostalCode;
                    this.textBoxCountry.Text = alliedExpress.Country;
                    this.textBoxPhone.Text = alliedExpress.Phone;
                    this.textBoxEmail.Text = alliedExpress.EmailAddress;
                    this.textBoxReferenceNumber.Text = alliedExpress.RefNumber;

                    this.textBoxBookedBy.Text = UserSettings.GetValueFromRegistry("BookedByName", "Shipping Request");
                    this.textBoxPickupAddr1.Text = UserSettings.GetValueFromRegistry("PickupAddress1", "Shipping Request");
                    this.textBoxPickupAddr2.Text = UserSettings.GetValueFromRegistry("PickupAddress2", "Shipping Request");
                    this.textBoxPickupCity.Text = UserSettings.GetValueFromRegistry("PickupCity", "Shipping Request");
                    this.textBoxPickupState.Text = UserSettings.GetValueFromRegistry("PickupState", "Shipping Request");
                    this.textBoxPickupPostCode.Text = UserSettings.GetValueFromRegistry("PickupPostcode", "Shipping Request");
                    index = UserSettings.GetValueFromRegistry("ServiceSelected", "Shipping Request");
                    if (index != string.Empty)
                    {
                        comboBoxServiceSelected.SelectedIndex = Convert.ToInt32(index);
                    }
                    //Populate datagrid Packages
                    if (alliedExpress.ItemsArray.Count > 0)
                    {
                        dataGridViewPackages.Enabled = true;
                        this.ItemDataTable = CreateDataTable(alliedExpress.ItemsArray);
                        dataGridViewPackages.DataSource = ItemDataTable;
                    }
                    else
                    {
                        dataGridViewPackages.Enabled = false;
                    }
                }
            }
            else
            {
                buttonSave.Enabled = false;
                buttonPrintLabel.Enabled = false;              
                //Parse order and populate Shipping Request form.
                 DataSet ds = DBConnection.MySqlDataAcess.ExecuteDataset(string.Format(SQLQueries.Queries.MAWithAttachID, m_messageID.ToString()));
                 if (ds != null)
                 {               
                     attachment = ASCIIEncoding.ASCII.GetString((byte[])ds.Tables[0].Rows[0]["AttachFile"]);
                 }
                 ParseOrder(attachment, string.Empty, string.Empty);
                 if (alliedExpress.ItemsArray.Count > 0)
                 {
                     dataGridViewPackages.Enabled = true;
                     this.ItemDataTable = CreateDataTable(alliedExpress.ItemsArray);
                     dataGridViewPackages.DataSource = ItemDataTable;
                 }
                 else
                 {
                     dataGridViewPackages.Enabled = false;
                 }
            }
        }

        private DataTable CreateDataTable(Collection<FinancialEntities.Items> items)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(ShippingRequestColumns.itemName.ToString());
            dt.Columns.Add(ShippingRequestColumns.Quantity.ToString());
            dt.Columns.Add(ShippingRequestColumns.length.ToString());
            dt.Columns.Add(ShippingRequestColumns.width.ToString());
            dt.Columns.Add(ShippingRequestColumns.height.ToString());
            dt.Columns.Add(ShippingRequestColumns.volume.ToString());
            dt.Columns.Add(ShippingRequestColumns.weight.ToString());
            dt.Columns.Add(ShippingRequestColumns.DangerousGood.ToString());

            foreach (FinancialEntities.Items item in items)
            {
                dt.Rows.Add(item.ItemName,item.Quantity,item.Length,item.Width,item.Height,item.Volume,item.Weight,item.DangerousGood);               
            }
            return dt;
        }        
        
        /// <summary>
        /// Calculate volume l*b*h.
        /// </summary>
        private string CalculateVolume(double l, double w, double h)
        {
            double volume = 0;
            try
            {
                //Converting the length, width and height to meter.(1m = 100cm)
                volume = (l / 100) * (w / 100) * (h / 100);
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }
            return string.Format("{0:0.000}", volume);
        }

        /// <summary>
        /// Validate Ready Date as per the Rules applied.
        /// </summary>
        /// <returns></returns>
        private DateTime ValidateReadyDate()
        {
            DateTime dt = new DateTime();
            dt = dateTimePicker.Value;
            string dateValue = dt.ToString();
            string date = DateTime.Parse(dateValue).ToString("MM/dd/yyyy");
            //Compare with dispatched time.
            try
            {                
                if (dateTimePicker.Value < DateTime.Now)
                    return DateTime.Now;
                else if (dt.Hour < 6) //Compare with Allied Opening time.
                {
                    dateValue = date + " " + "6:00:00";
                    dt = Convert.ToDateTime(dateValue);
                }
                else if (dt.Hour > 18) //Compare with Allied Closing time.
                {
                    dt = dt.AddDays(1);
                    date = dt.ToString("MM/dd/yyy");
                    dateValue = date + " " + "6:00:00";
                    dt = Convert.ToDateTime(dateValue);
                }
                else //use dateTime as specified.
                {
                    return dateTimePicker.Value;
                }
                return dt;
            }
            catch
            {
                return DateTime.Now;
            } 
        }

        /// <summary>
        /// Construct Test job.
        /// </summary>
        private TnTWebServiceTesting.Job ConstructTestJob()
        {

            //Initialize web service object.
            serviceTest = new TnTWebServiceTesting.TnTWebService();
            //Initialize job Object.
            jobTest = new TnTWebServiceTesting.Job();

            //Get Account
            accountTest = serviceTest.getAccountDefaults(alliedExpress.AccountNumber,
                                                 alliedExpress.AccountCode,
                                                 alliedExpress.AESTate,
                                                 "AOE");

            //Construct Geographic Pickup Address(P and D)
            TnTWebServiceTesting.GeographicAddress PickupAddress = new TnTWebServiceTesting.GeographicAddress();
            PickupAddress.address1 = this.textBoxPickupAddr1.Text.Trim();
            PickupAddress.suburb = this.textBoxPickupCity.Text.Trim().ToUpper();
            PickupAddress.state = this.textBoxPickupState.Text.Trim().ToUpper();
            PickupAddress.postCode = this.textBoxPickupPostCode.Text.Trim();

            if (this.textBoxPickupAddr2.Text.Trim() != string.Empty)
            {
                PickupAddress.address2 = this.textBoxPickupAddr2.Text.Trim();
            }
            if (this.textBoxCountry.Text.Trim() != string.Empty)
            {
                PickupAddress.country = this.textBoxCountry.Text.Trim();
            }

            //Construct Geographic Delivery Address(P and D)
            TnTWebServiceTesting.GeographicAddress deliveryAddress = new TnTWebServiceTesting.GeographicAddress();
            deliveryAddress.address1 = this.textBoxAddr1.Text.Trim();
            deliveryAddress.suburb = this.textBoxCity.Text.Trim().ToUpper();
            deliveryAddress.state = this.textBoxState.Text.Trim().ToUpper();
            deliveryAddress.postCode = this.textBoxPostalCode.Text.Trim();

            if (this.textBoxAddr2.Text.Trim() != string.Empty)
            {
                deliveryAddress.address2 = this.textBoxAddr2.Text.Trim();
            }
            if (this.textBoxCountry.Text.Trim() != string.Empty)
            {
                deliveryAddress.country = this.textBoxCountry.Text.Trim();
            }

            //Construct JobStop object
            TnTWebServiceTesting.JobStop pickupStop = new TnTWebServiceTesting.JobStop();
            pickupStop.companyName = this.textBoxToCompany.Text.Trim();
            pickupStop.contact = this.textBoxName.Text.Trim();
            pickupStop.geographicAddress = PickupAddress;
            pickupStop.phoneNumber = this.textBoxPhone.Text.Trim();
            pickupStop.stopType = "P";
            pickupStop.stopNumber = 1;

            TnTWebServiceTesting.JobStop deliveryStop = new TnTWebServiceTesting.JobStop();
            deliveryStop.companyName = this.textBoxToCompany.Text.Trim();
            deliveryStop.contact = this.textBoxName.Text.Trim();
            deliveryStop.geographicAddress = deliveryAddress;
            deliveryStop.phoneNumber = this.textBoxPhone.Text.Trim();
            deliveryStop.stopType = "D";
            deliveryStop.stopNumber = 2;

            TnTWebServiceTesting.JobStop[] jobStops = new TnTWebServiceTesting.JobStop[] { pickupStop, deliveryStop };

            //Construct ItemObject
            int index = 0;
            TnTWebServiceTesting.Item[] jobItems = new TnTWebServiceTesting.Item[dataGridViewPackages.Rows.Count - 1];
            foreach (DataGridViewRow dataGridRowView in dataGridViewPackages.Rows)
            {
                try
                {
                    TnTWebServiceTesting.Item item = new TnTWebServiceTesting.Item();
                    DataRowView dataRowView = (DataRowView)dataGridRowView.DataBoundItem;
                    DataRow row = dataRowView.Row;
                    string flag = row["DangerousGood"].ToString();
                    if (flag == "true")
                        item.dangerous = true;
                    else
                        item.dangerous = false;
                    item.length = System.Math.Round(Convert.ToDouble(row["length"]));
                    item.width = System.Math.Round(Convert.ToDouble(row["width"]));
                    item.height = System.Math.Round(Convert.ToDouble(row["height"]));
                    item.volume = Convert.ToDouble(row["volume"]);
                    item.weight = System.Math.Round(Convert.ToDouble(row["weight"]));

                    if (Convert.ToString(row["Quantity"]) != string.Empty)
                    {
                        item.itemCount = Convert.ToInt32(row["Quantity"]);
                    }
                    jobItems.SetValue(item, index);
                    index++;
                }
                catch
                { }
            }

            //Get ItemCount ,Weight, Volume
            foreach (TnTWebServiceTesting.Item item in jobItems)
            {
                if (item != null)
                {
                    totalItems += item.itemCount;
                    totalVolumes += item.volume * item.itemCount;
                    totalWeight += item.weight * item.itemCount;
                }
            }

            //Construct Job Object
            jobTest.account = accountTest;
            jobTest.jobStops = jobStops;
            jobTest.items = jobItems;
            jobTest.itemCount = totalItems;
            jobTest.weight = totalWeight;
            jobTest.volume = totalVolumes;

            //Get Service Level
            if (this.comboBoxServiceSelected.SelectedIndex > 0)
            {
                string[] level = this.comboBoxServiceSelected.SelectedItem.ToString().Split('(');
                jobTest.serviceLevel = level[0];
            }
            //Get Instructions.
            jobTest.instructions = textBoxInstruction1.Text.Trim() + textBoxInstruction2.Text.Trim() + textBoxInstruction3.Text.Trim();
            //Construct Reference Number
            if (this.textBoxReferenceNumber.Text.Trim() != string.Empty)
            {
                string[] referenceNumbers = new string[] { this.textBoxReferenceNumber.Text.Trim() };
                jobTest.referenceNumbers = referenceNumbers;
            }
            //Get Booked By Name.
            jobTest.bookedBy = this.textBoxBookedBy.Text.Trim();
            //Create Ready Date
            DateTime? readyDate = ValidateReadyDate();
            jobTest.readyDate = readyDate;

            return jobTest;
        }

        /// <summary>
        /// Constrcut Production job.
        /// </summary>
        /// <returns></returns>
        private TnTWebServiceProduction.Job ConstructProductionJob()
        {
            //Initialize web service object.
            serviceProduction = new TnTWebServiceProduction.TnTWebService();
            //Initialize job Object.
            jobProduction = new TnTWebServiceProduction.Job();

            //Get Account
            accountProduction = serviceProduction.getAccountDefaults(alliedExpress.AccountNumber,
                                                 alliedExpress.AccountCode,
                                                 alliedExpress.AESTate,
                                                 "AOE");

            //Construct Geographic Pickup Address(P and D)
            TnTWebServiceProduction.GeographicAddress PickupAddress = new TnTWebServiceProduction.GeographicAddress();
            PickupAddress.address1 = this.textBoxPickupAddr1.Text.Trim();
            PickupAddress.suburb = this.textBoxPickupCity.Text.Trim().ToUpper();
            PickupAddress.state = this.textBoxPickupState.Text.Trim().ToUpper();
            PickupAddress.postCode = this.textBoxPickupPostCode.Text.Trim();

            if (this.textBoxPickupAddr2.Text.Trim() != string.Empty)
            {
                PickupAddress.address2 = this.textBoxPickupAddr2.Text.Trim();
            }
            if (this.textBoxCountry.Text.Trim() != string.Empty)
            {
                PickupAddress.country = this.textBoxCountry.Text.Trim();
            }

            //Construct Geographic Delivery Address(P and D)
            TnTWebServiceProduction.GeographicAddress deliveryAddress = new TnTWebServiceProduction.GeographicAddress();
            deliveryAddress.address1 = this.textBoxAddr1.Text.Trim();
            deliveryAddress.suburb = this.textBoxCity.Text.Trim().ToUpper();
            deliveryAddress.state = this.textBoxState.Text.Trim().ToUpper();
            deliveryAddress.postCode = this.textBoxPostalCode.Text.Trim();

            if (this.textBoxAddr2.Text.Trim() != string.Empty)
            {
                deliveryAddress.address2 = this.textBoxAddr2.Text.Trim();
            }
            if (this.textBoxCountry.Text.Trim() != string.Empty)
            {
                deliveryAddress.country = this.textBoxCountry.Text.Trim();
            }

            //Construct JobStop object
            TnTWebServiceProduction.JobStop pickupStop = new TnTWebServiceProduction.JobStop();
            pickupStop.companyName = this.textBoxToCompany.Text.Trim();
            pickupStop.contact = this.textBoxName.Text.Trim();
            pickupStop.geographicAddress = PickupAddress;
            pickupStop.phoneNumber = this.textBoxPhone.Text.Trim();
            pickupStop.stopType = "P";
            pickupStop.stopNumber = 1;

            TnTWebServiceProduction.JobStop deliveryStop = new TnTWebServiceProduction.JobStop();
            deliveryStop.companyName = this.textBoxToCompany.Text.Trim();
            deliveryStop.contact = this.textBoxName.Text.Trim();
            deliveryStop.geographicAddress = deliveryAddress;
            deliveryStop.phoneNumber = this.textBoxPhone.Text.Trim();
            deliveryStop.stopType = "D";
            deliveryStop.stopNumber = 2;

            TnTWebServiceProduction.JobStop[] jobStops = new TnTWebServiceProduction.JobStop[] { pickupStop, deliveryStop };

            //Construct ItemObject
            int index = 0;
            TnTWebServiceProduction.Item[] jobItems = new TnTWebServiceProduction.Item[dataGridViewPackages.Rows.Count - 1];
            foreach (DataGridViewRow dataGridRowView in dataGridViewPackages.Rows)
            {
                try
                {
                    TnTWebServiceProduction.Item item = new TnTWebServiceProduction.Item();
                    DataRowView dataRowView = (DataRowView)dataGridRowView.DataBoundItem;
                    DataRow row = dataRowView.Row;
                    string flag = row["DangerousGood"].ToString();
                    if (flag == "true")
                        item.dangerous = true;
                    else
                        item.dangerous = false;
                    item.length = System.Math.Round(Convert.ToDouble(row["length"]));
                    item.width = System.Math.Round(Convert.ToDouble(row["width"]));
                    item.height = System.Math.Round(Convert.ToDouble(row["height"]));
                    item.volume = Convert.ToDouble(row["volume"]);
                    item.weight = System.Math.Round(Convert.ToDouble(row["weight"]));

                    if (Convert.ToString(row["Quantity"]) != string.Empty)
                    {
                        item.itemCount = Convert.ToInt32(row["Quantity"]);
                    }

                    jobItems.SetValue(item, index);
                    index++;
                }
                catch
                { }
            }

            //Get ItemCount ,Weight, Volume
            foreach (TnTWebServiceProduction.Item item in jobItems)
            {
                if (item != null)
                {
                    totalItems += item.itemCount;
                    totalVolumes += item.volume * item.itemCount;
                    totalWeight += item.weight * item.itemCount;
                }
            }

            //Construct Job Object
            jobProduction.account = accountProduction;
            jobProduction.jobStops = jobStops;
            jobProduction.items = jobItems;
            jobProduction.itemCount = totalItems;
            jobProduction.weight = totalWeight;
            jobProduction.volume = totalVolumes;

            //Get Service Level
            if (this.comboBoxServiceSelected.SelectedIndex > 0)
            {
                string[] level = this.comboBoxServiceSelected.SelectedItem.ToString().Split('(');
                jobProduction.serviceLevel = level[0];
            }
            //Get Instructions.
            jobProduction.instructions = textBoxInstruction1.Text.Trim() + textBoxInstruction2.Text.Trim() + textBoxInstruction3.Text.Trim();
            //Construct Reference Number
            if (this.textBoxReferenceNumber.Text.Trim() != string.Empty)
            {
                string[] referenceNumbers = new string[] { this.textBoxReferenceNumber.Text.Trim() };
                jobProduction.referenceNumbers = referenceNumbers;
            }
            //Get Booked By Name.
            jobProduction.bookedBy = this.textBoxBookedBy.Text.Trim();
            //Create Ready Date
            DateTime? readyDate = ValidateReadyDate();
            jobProduction.readyDate = readyDate;

            return jobProduction;           
        }

        /// <summary>
        /// Generate an consignment note which is send while sending notification to eBay Or OSCommerce.
        /// </summary>
        /// <returns></returns>
        private string GenerateConsignmentNote()
        {
            int number = Convert.ToInt32(alliedExpress.RefNumber);
            cannoteNumber = string.Format("{0:000000}", number);

            return textBoxPrefix.Text.Trim() + cannoteNumber;
        }

        /// <summary>
        /// Genrate an barcode format as Consignment number
        /// with parcel number.
        /// </summary>
        /// <returns></returns>
        private string GenerateConsignmentNumber(int itemCount)
        {
            int number = 0;
            //cannoteNumber = UserSettings.GetValueFromRegistry("CannoteNumber", "Shipping Request");
            //parcelID = UserSettings.GetValueFromRegistry("ParcelID", "Shipping Request");
            parcelID = string.Format("{0:000}", itemCount);
            if (string.IsNullOrEmpty(this.textBoxReferenceNumber.Text.Trim()))
            {
                number = Convert.ToInt32(alliedExpress.RefNumber);
                cannoteNumber = string.Format("{0:000000}", number);
            }
            else
            {
                number = Convert.ToInt32(this.textBoxReferenceNumber.Text.Trim());
                cannoteNumber = string.Format("{0:000000}", number);
            }
          
            //if (parcelID == string.Empty)
            //{
            //    parcelID = "001";
            //}
            //else
            //{
            //    if (parcelID == "999")
            //        parcelID = "001";
            //    else
            //    {
            //        int ID = Convert.ToInt32(parcelID) + 1;
            //        parcelID = ID.ToString();
            //        if (parcelID.Length == 1 || parcelID.Length == 2)
            //        {
            //            parcelID = string.Format("{0:000}", ID);
            //        }
            //    }               
            //}
            //Ex: ALL123456001              
            return textBoxPrefix.Text.Trim() + cannoteNumber + parcelID; 
        }

        /// <summary>
        /// Parse eBay1 for Order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddEbay1(string[] stream)
        {
            if (stream.Length > 7)
            {
                if (!IsSentItemOpen)
                {
                    if (stream[3].Trim() != string.Empty)
                        alliedExpress.RefNumber = stream[3].Trim();
                    if (stream[6].Trim() != string.Empty)
                        alliedExpress.ToCompanyName = stream[6].Trim();
                    if (stream[7].Trim() != string.Empty)
                        alliedExpress.Phone = stream[7].Trim();
                    if (stream[8].Trim() != string.Empty)
                        alliedExpress.AddressLine1 = stream[8].Trim();
                    if (stream[9].Trim() != string.Empty)
                        alliedExpress.AddressLine2 = stream[9].Trim();
                    if (stream[10].Trim() != string.Empty)
                        alliedExpress.City = stream[10].Trim();
                    if (stream[11].Trim() != string.Empty)
                        alliedExpress.State = stream[11].Trim();
                    if (stream[12].Trim() != string.Empty)
                        alliedExpress.PostalCode = stream[12].Trim();
                    if (stream[13].Trim() != string.Empty)
                        alliedExpress.Country = stream[13].Trim();
                    //Set transaction ID
                    alliedExpress.TransactionID = "0";
                    return true;
                }
                else //If user viewing sent item mail.
                {

                    this.textBoxReferenceNumber.Text = stream[3].Trim();
                    this.textBoxToCompany.Text = stream[6].Trim();
                    this.textBoxPhone.Text = stream[7].Trim();
                    this.textBoxAddr1.Text = stream[8].Trim();
                    this.textBoxAddr2.Text = stream[9].Trim();
                    this.textBoxCity.Text = stream[10].Trim();
                    this.textBoxState.Text = stream[11].Trim();
                    this.textBoxPostalCode.Text = stream[12].Trim();
                    this.textBoxCountry.Text = stream[13].Trim();
                    this.textBoxFromCompany.Text = stream[22].Trim();
                    this.textBoxFromPhone.Text = stream[23].Trim();
                    this.textBoxPrefix.Text = stream[24].Trim();
                    this.textBoxName.Text = stream[25].Trim();
                    this.textBoxPickupAddr1.Text = stream[26].Trim();
                    this.textBoxPickupAddr2.Text = stream[27].Trim();
                    this.textBoxPickupCity.Text = stream[28].Trim();
                    this.textBoxPickupState.Text = stream[29].Trim();
                    this.textBoxPickupPostCode.Text = stream[30].Trim();
                    this.textBoxInstruction1.Text = stream[31].Trim();
                    this.textBoxInstruction2.Text = stream[32].Trim();
                    this.textBoxInstruction3.Text = stream[33].Trim();
                    this.comboBoxServiceSelected.SelectedIndex = Convert.ToInt32(stream[34].Trim());
                    this.textBoxBookedBy.Text = stream[35].Trim();
                    this.dateTimePicker.Value = Convert.ToDateTime(stream[36]);
                    this.labelDateTime1.Text = stream[37].Trim();
                    this.labelJobDispatched.Text = stream[38].Trim();
                    this.textBoxEmail.Text = stream[39].Trim();
                    return true;

                }
            }
            else
            {
                throw new Exception("Attachment is not in correct format");
            }           
        }

        /// <summary>
        /// Parse eBay2 for Items to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddEbay2(string[] stream)
        {
            item = new Items();
            length = string.Empty;
            width = string.Empty;
            height = string.Empty;
            weight = string.Empty;

            if (stream[2].Trim() != string.Empty)
            {
                item.ItemID = stream[2].Trim();
            }
            if (stream[3].Trim() != string.Empty)
            {
                if (stream[3].Length > 31)
                {
                    item.ItemName = stream[3].Substring(0, 30);
                }
                else 
                {
                    item.ItemName = stream[3].Trim();
                }
            }                      
            if (stream[5].Trim() != string.Empty)
            {
                try
                {
                    item.Quantity = Convert.ToInt32(stream[5].Trim());
                }
                catch(Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
            }
          
            if (!string.IsNullOrEmpty(item.ItemName))
            {
                if (m_connectedSoftware == EDI.Constant.Constants.QBstring)
                {
                    string itemName = item.ItemName;
                    //Constructing an item Name with 31 digit
                    string[] arr = new string[15];
                    arr = itemName.Split(':');
                    itemName = string.Empty;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].Length > 31)
                        {
                            arr[i] = arr[i].Substring(0, 30);
                        }
                        if (i == (arr.Length - 1))
                        {
                            itemName += arr[i];
                        }
                        else
                        {
                            itemName += arr[i] + ":";
                        }
                    }
                    QBMethods.GetItemCustomFields(itemName, m_QbCompanyFile, ref length, ref width, ref height, ref weight);
                }
                if (length != string.Empty)
                {
                    try
                    {
                        if (length.Length > 10)
                        {
                            item.Length = 0;
                        }
                        else
                        {
                            item.Length = System.Math.Round(Convert.ToDouble(length));
                        }
                    }
                    catch(Exception ex)
                    {
                        item.Length = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (width != string.Empty)
                {
                    try
                    {
                        if (width.Length > 10)
                        {
                            item.Width = 0;
                        }
                        else
                        {
                            item.Width = System.Math.Round(Convert.ToDouble(width));
                        }
                    }
                    catch(Exception ex)
                    {
                        item.Width = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (height != string.Empty)
                {
                    try
                    {
                        if (height.Length > 10)
                        {
                            item.Height = 0;
                        }
                        else
                        {
                            item.Height = System.Math.Round(Convert.ToDouble(height));
                        }
                    }
                    catch(Exception ex)
                    {
                        item.Height = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (weight != string.Empty)
                {
                    try
                    {
                        if (weight.Length > 10)
                        {
                            item.Weight = 0;
                        }
                        else
                        {
                            item.Weight = System.Math.Round(Convert.ToDouble(weight));
                        }
                    }
                    catch(Exception ex)
                    {
                        item.Weight = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
               
                item.Volume = CalculateVolume(item.Length, item.Width, item.Height);
                //item.DangerousGood = "true";
                item.DangerousGood = "false";
                alliedExpress.ItemsArray.Add(item); 
             }                       
            return true;
        }

        /// <summary>
        /// Parse OSCommerce for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddOSCommerce1(string[] stream)
        {
            if (stream.Length > 7)
            {
                if (!IsSentItemOpen)
                {
                    if (stream[1].Trim() != string.Empty)
                        alliedExpress.RefNumber = stream[1].Trim();
                    if (stream[2].Trim() != string.Empty)
                        alliedExpress.ToCompanyName = stream[2].Trim();
                    if (stream[3].Trim() != string.Empty)
                        alliedExpress.AddressLine1 = stream[3].Trim();
                    if (stream[4].Trim() != string.Empty)
                        alliedExpress.AddressLine2 = stream[4].Trim();
                    if (stream[5].Trim() != string.Empty)
                        alliedExpress.City = stream[5].Trim();
                    if (stream[7].Trim() != string.Empty)
                        alliedExpress.State = stream[7].Trim();
                    if (stream[6].Trim() != string.Empty)
                        alliedExpress.PostalCode = stream[6].Trim();
                    if (stream[8].Trim() != string.Empty)
                        alliedExpress.Country = stream[8].Trim();
                    if (stream[9].Trim() != string.Empty)
                        alliedExpress.Phone = stream[9].Trim();
                    //Set transaction ID
                    alliedExpress.TransactionID = "0";
                    return true;
                }
                else//If user viewing sent item mail
                {
                    this.textBoxReferenceNumber.Text = stream[1].Trim();
                    this.textBoxToCompany.Text = stream[2].Trim();
                    this.textBoxAddr1.Text = stream[3].Trim();
                    this.textBoxAddr2.Text = stream[4].Trim();
                    this.textBoxCity.Text = stream[5].Trim();
                    this.textBoxState.Text = stream[7].Trim();
                    this.textBoxPostalCode.Text = stream[6].Trim();
                    this.textBoxCountry.Text = stream[8].Trim();
                    this.textBoxPhone.Text = stream[9].Trim();
                    this.textBoxFromCompany.Text = stream[17].Trim();
                    this.textBoxFromPhone.Text = stream[18].Trim();
                    this.textBoxPrefix.Text = stream[19].Trim();
                    this.textBoxName.Text = stream[20].Trim();
                    this.textBoxPickupAddr1.Text = stream[21].Trim();
                    this.textBoxPickupAddr2.Text = stream[22].Trim();
                    this.textBoxPickupCity.Text = stream[23].Trim();
                    this.textBoxPickupState.Text = stream[24].Trim();
                    this.textBoxPickupPostCode.Text = stream[25].Trim();
                    this.textBoxInstruction1.Text = stream[26].Trim();
                    this.textBoxInstruction2.Text = stream[27].Trim();
                    this.textBoxInstruction3.Text = stream[28].Trim();
                    this.comboBoxServiceSelected.SelectedIndex = Convert.ToInt32(stream[29].Trim());
                    this.textBoxBookedBy.Text = stream[30].Trim();
                    this.dateTimePicker.Value = Convert.ToDateTime(stream[31]);
                    this.labelDateTime1.Text = stream[32].Trim();
                    this.labelJobDispatched.Text = stream[33].Trim();
                    this.textBoxEmail.Text = stream[34].Trim();
                    return true;
                }
            }
            else
            {
                throw new Exception("Attachment is not in correct format");
            }        
        }




        /// <summary>
        /// Parse SR1(SalesReceipt) for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddSR1(string[] stream)
        {
            if (stream[1].Trim() != string.Empty)//Ref Number
                alliedExpress.RefNumber = stream[1].Trim();

            if (stream[3].Trim() != string.Empty)//ShipAddress1 to Company Name
            {
                alliedExpress.ToCompanyName = stream[3].Trim();
            }

            if (stream[4].Trim() != string.Empty)//ShipAddress2 to AddressLine1
                alliedExpress.AddressLine1 = stream[4].Trim();

            if (stream[5].Trim() != string.Empty)//ShipAddress3 to AddressLine2
                alliedExpress.AddressLine2 = stream[5].Trim();

            if (stream[6].Trim() != string.Empty)//City
                alliedExpress.City = stream[6].Trim();

            if (stream[7].Trim() != string.Empty)//state
                alliedExpress.State = stream[7].Trim();

            if (stream[8].Trim() != string.Empty)//postal code
                alliedExpress.PostalCode = stream[8].Trim();

            if (stream[9].Trim() != string.Empty)//Country
                alliedExpress.Country = stream[9].Trim();

            if (stream[10].Trim() != string.Empty)//Phone
                alliedExpress.Phone = stream[10].Trim();

            if (stream[11].Trim() != string.Empty)//Email
                alliedExpress.EmailAddress = stream[11].Trim();

            //Set transaction ID
            alliedExpress.TransactionID = "0";
            return true;


        }


        /// <summary>
        /// Parse SR2(SalesReceipt) for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddSR2(string[] stream)
        {
            item = new Items();
            length = string.Empty;
            width = string.Empty;
            height = string.Empty;
            weight = string.Empty;

            if (stream[3].Trim() != string.Empty)
            {
                item.ItemName = stream[3].Trim();
            }

            if (stream[4].Trim() != string.Empty)
            {
                try
                {
                    item.Quantity = Convert.ToInt32(stream[4].Trim());
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
            }

            if (!string.IsNullOrEmpty(item.ItemName))
            {
                if (m_connectedSoftware == EDI.Constant.Constants.QBstring)
                {
                    string itemName = item.ItemName;

                    //Constructing an item Name with 31 digit
                    string[] arr = new string[15];

                    arr = itemName.Split(':');
                    itemName = string.Empty;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].Length > 31)
                        {
                            arr[i] = arr[i].Substring(0, 30);
                        }
                        if (i == (arr.Length - 1))
                        {
                            itemName += arr[i];
                        }
                        else
                        {
                            itemName += arr[i] + ":";
                        }
                    }
                    QBMethods.GetItemCustomFields(itemName, m_QbCompanyFile, ref length, ref width, ref height, ref weight);
                }
               
                if (length != string.Empty)
                {
                    try
                    {
                        if (length.Length > 10)
                        {
                            item.Length = 0;
                        }
                        else
                        {
                            item.Length = System.Math.Round(Convert.ToDouble(length));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Length = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (width != string.Empty)
                {
                    try
                    {
                        if (width.Length > 10)
                        {
                            item.Width = 0;
                        }
                        else
                        {
                            item.Width = System.Math.Round(Convert.ToDouble(width));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Width = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (height != string.Empty)
                {
                    try
                    {
                        if (height.Length > 10)
                        {
                            item.Height = 0;
                        }
                        else
                        {
                            item.Height = System.Math.Round(Convert.ToDouble(height));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Height = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (weight != string.Empty)
                {
                    try
                    {
                        if (weight.Length > 10)
                        {
                            item.Weight = 0;
                        }
                        else
                        {
                            item.Weight = System.Math.Round(Convert.ToDouble(weight));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Weight = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }

                item.Volume = CalculateVolume(item.Length, item.Width, item.Height);
                //item.DangerousGood = "true";
                item.DangerousGood = "false";
                alliedExpress.ItemsArray.Add(item);
            }
            return true;
        }



        /// <summary>
        /// Parse OR1(SalesOrder) for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddOR1(string[] stream)
        {
            if (stream[1].Trim() != string.Empty)//Ref Number
                alliedExpress.RefNumber = stream[1].Trim();

            if (stream[3].Trim() != string.Empty)//ShipAddress1 To Company Name
            {
                alliedExpress.ToCompanyName = stream[3].Trim();
            }

            if (stream[4].Trim() != string.Empty)//ShipAddress2 To AddressLine1
                alliedExpress.AddressLine1 = stream[4].Trim();

            if (stream[5].Trim() != string.Empty)//ShipAddress3 To AddressLine2
                alliedExpress.AddressLine2 = stream[5].Trim();

            if (stream[6].Trim() != string.Empty)//City
                alliedExpress.City = stream[6].Trim();

            if (stream[7].Trim() != string.Empty)//state
                alliedExpress.State = stream[7].Trim();

            if (stream[8].Trim() != string.Empty)//postal code
                alliedExpress.PostalCode = stream[8].Trim();

            if (stream[9].Trim() != string.Empty)//Country
                alliedExpress.Country = stream[9].Trim();

            if (stream[10].Trim() != string.Empty)//Phone
                alliedExpress.Phone = stream[10].Trim();

            if (stream[11].Trim() != string.Empty)//Email
                alliedExpress.EmailAddress = stream[11].Trim();

            //Set transaction ID
            alliedExpress.TransactionID = "0";
            return true;
        }


        /// <summary>
        /// Parse OR2(SalesOrder) for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddOR2(string[] stream)
        {
            item = new Items();
            length = string.Empty;
            width = string.Empty;
            height = string.Empty;
            weight = string.Empty;

            if (stream[3].Trim() != string.Empty)
            {
                item.ItemName = stream[3].Trim();
            }

            if (stream[4].Trim() != string.Empty)
            {
                try
                {
                    item.Quantity = Convert.ToInt32(stream[4].Trim());
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
            }

            if (!string.IsNullOrEmpty(item.ItemName))
            {
                if (m_connectedSoftware == EDI.Constant.Constants.QBstring)
                {
                    string itemName = item.ItemName;

                    //Constructing an item Name with 31 digit
                    string[] arr = new string[15];

                    arr = itemName.Split(':');
                    itemName = string.Empty;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].Length > 31)
                        {
                            arr[i] = arr[i].Substring(0, 30);
                        }
                        if (i == (arr.Length - 1))
                        {
                            itemName += arr[i];
                        }
                        else
                        {
                            itemName += arr[i] + ":";
                        }
                    }
                    QBMethods.GetItemCustomFields(itemName, m_QbCompanyFile, ref length, ref width, ref height, ref weight);
                }
                if (length != string.Empty)
                {
                    try
                    {
                        if (length.Length > 10)
                        {
                            item.Length = 0;
                        }
                        else
                        {
                            item.Length = System.Math.Round(Convert.ToDouble(length));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Length = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (width != string.Empty)
                {
                    try
                    {
                        if (width.Length > 10)
                        {
                            item.Width = 0;
                        }
                        else
                        {
                            item.Width = System.Math.Round(Convert.ToDouble(width));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Width = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (height != string.Empty)
                {
                    try
                    {
                        if (height.Length > 10)
                        {
                            item.Height = 0;
                        }
                        else
                        {
                            item.Height = System.Math.Round(Convert.ToDouble(height));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Height = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (weight != string.Empty)
                {
                    try
                    {
                        if (weight.Length > 10)
                        {
                            item.Weight = 0;
                        }
                        else
                        {
                            item.Weight = System.Math.Round(Convert.ToDouble(weight));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Weight = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }

                item.Volume = CalculateVolume(item.Length, item.Width, item.Height);
                //item.DangerousGood = "true";
                item.DangerousGood = "false";
                alliedExpress.ItemsArray.Add(item);
            }
            return true;
        }


        /// <summary>
        /// Parse PC1(Invoice) for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddPC1(string[] stream)
        {
            if (stream[1].Trim() != string.Empty)//Ref Number
                alliedExpress.RefNumber = stream[1].Trim();

            if (stream[3].Trim() != string.Empty)//ShipAddress1 To Company Name
            {
                alliedExpress.ToCompanyName = stream[3].Trim();
            }

            if (stream[4].Trim() != string.Empty)//ShipAddress2 To AddressLine1
                alliedExpress.AddressLine1 = stream[4].Trim();

            if (stream[5].Trim() != string.Empty)//ShipAddress3 To AddressLine2
                alliedExpress.AddressLine2 = stream[5].Trim();

            if (stream[6].Trim() != string.Empty)//City
                alliedExpress.City = stream[6].Trim();

            if (stream[7].Trim() != string.Empty)//state
                alliedExpress.State = stream[7].Trim();

            if (stream[8].Trim() != string.Empty)//postal code
                alliedExpress.PostalCode = stream[8].Trim();

            if (stream[9].Trim() != string.Empty)//Country
                alliedExpress.Country = stream[9].Trim();

            if (stream[10].Trim() != string.Empty)//Phone
                alliedExpress.Phone = stream[10].Trim();

            if (stream[11].Trim() != string.Empty)//Email
                alliedExpress.EmailAddress = stream[11].Trim();

            //Set transaction ID
            alliedExpress.TransactionID = "0";
            return true;
        }


        /// <summary>
        /// Parse PC2(Invoice) for order Details to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddPC2(string[] stream)
        {
            item = new Items();
            length = string.Empty;
            width = string.Empty;
            height = string.Empty;
            weight = string.Empty;

            if (stream[3].Trim() != string.Empty)
            {
                item.ItemName = stream[3].Trim();
            }

            if (stream[4].Trim() != string.Empty)
            {
                try
                {
                    item.Quantity = Convert.ToInt32(stream[4].Trim());
                }
                catch (Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
            }

            if (!string.IsNullOrEmpty(item.ItemName))
            {
                if (m_connectedSoftware == EDI.Constant.Constants.QBstring)
                {
                    string itemName = item.ItemName;

                    //Constructing an item Name with 31 digit
                    string[] arr = new string[15];

                    arr = itemName.Split(':');
                    itemName = string.Empty;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].Length > 31)
                        {
                            arr[i] = arr[i].Substring(0, 30);
                        }
                        if (i == (arr.Length - 1))
                        {
                            itemName += arr[i];
                        }
                        else
                        {
                            itemName += arr[i] + ":";
                        }
                    }
                    QBMethods.GetItemCustomFields(itemName, m_QbCompanyFile, ref length, ref width, ref height, ref weight);
                }
                if (length != string.Empty)
                {
                    try
                    {
                        if (length.Length > 10)
                        {
                            item.Length = 0;
                        }
                        else
                        {
                            item.Length = System.Math.Round(Convert.ToDouble(length));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Length = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (width != string.Empty)
                {
                    try
                    {
                        if (width.Length > 10)
                        {
                            item.Width = 0;
                        }
                        else
                        {
                            item.Width = System.Math.Round(Convert.ToDouble(width));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Width = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (height != string.Empty)
                {
                    try
                    {
                        if (height.Length > 10)
                        {
                            item.Height = 0;
                        }
                        else
                        {
                            item.Height = System.Math.Round(Convert.ToDouble(height));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Height = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (weight != string.Empty)
                {
                    try
                    {
                        if (weight.Length > 10)
                        {
                            item.Weight = 0;
                        }
                        else
                        {
                            item.Weight = System.Math.Round(Convert.ToDouble(weight));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Weight = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }

                item.Volume = CalculateVolume(item.Length, item.Width, item.Height);
                //item.DangerousGood = "true";
                item.DangerousGood = "false";
                alliedExpress.ItemsArray.Add(item);
            }
            return true;
        }


        /// <summary>
        /// Parse OSCommerce for order items to AlliedExpress.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private bool AddOSCommerce2(string[] stream)
        {
            item = new Items();
            length = string.Empty;
            width = string.Empty;
            height = string.Empty;
            weight = string.Empty;

            if (stream[2].Trim() != string.Empty)
            {
                item.OrderID = stream[1].Trim();
            }
            if (stream[3].Trim() != string.Empty)
            {
                item.ItemName = stream[3].Trim();
            }
           
            if (stream[7].Trim() != string.Empty)
            {
                try
                {
                    item.Quantity = Convert.ToInt32(stream[7].Trim());
                }
                catch(Exception ex)
                {
                    CommonUtilities.WriteErrorLog(ex.Message);
                    CommonUtilities.WriteErrorLog(ex.StackTrace);
                }
            }

            if (!string.IsNullOrEmpty(item.ItemName))
            {
                if (m_connectedSoftware == EDI.Constant.Constants.QBstring)
                {
                    string itemName = item.ItemName;
                    //Constructing an item Name with 31 digit
                    string[] arr = new string[15];
                    arr = itemName.Split(':');
                    itemName = string.Empty;
                    for (int i = 0; i < arr.Length; i++)
                    {
                        if (arr[i].Length > 31)
                        {
                            arr[i] = arr[i].Substring(0, 30);
                        }
                        if (i == (arr.Length - 1))
                        {
                            itemName += arr[i];
                        }
                        else
                        {
                            itemName += arr[i] + ":";
                        }
                    }
                    QBMethods.GetItemCustomFields(itemName, m_QbCompanyFile, ref length, ref width, ref height, ref weight);
                }
                if (length != string.Empty)
                {
                    try
                    {
                        if (length.Length > 10)
                        {
                            item.Length = 0;
                        }
                        else
                        {
                            item.Length = System.Math.Round(Convert.ToDouble(length));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Length = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (width != string.Empty)
                {
                    try
                    {
                        if (width.Length > 10)
                        {
                            item.Width = 0;
                        }
                        else
                        {
                            item.Width = System.Math.Round(Convert.ToDouble(width));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Width = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (height != string.Empty)
                {
                    try
                    {
                        if (height.Length > 10)
                        {
                            item.Height = 0;
                        }
                        else
                        {
                            item.Height = System.Math.Round(Convert.ToDouble(height));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Height = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }
                if (weight != string.Empty)
                {
                    try
                    {
                        if (weight.Length > 10)
                        {
                            item.Weight = 0;
                        }
                        else
                        {
                            item.Weight = System.Math.Round(Convert.ToDouble(weight));
                        }
                    }
                    catch (Exception ex)
                    {
                        item.Weight = 0;
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                }

                item.Volume = CalculateVolume(item.Length, item.Width, item.Height);
                //item.DangerousGood = "true";
                item.DangerousGood = "false";
                alliedExpress.ItemsArray.Add(item);
            }
            return true;
        }       
        
        /// <summary>
        /// Update the attachment and update in mail_Attachment table.
        /// </summary>
        private void UpdateAttachment()
        {            
            int attachID = 0;
            //Get Attachment Dataset from messageID. 
            DataSet ds = DBConnection.MySqlDataAcess.ExecuteDataset(string.Format(SQLQueries.Queries.MAWithAttachID, m_messageID.ToString()));
            if (ds != null)
            {               
                attachment = ASCIIEncoding.ASCII.GetString((byte[])ds.Tables[0].Rows[0]["AttachFile"]);
                attachID = Convert.ToInt32(ds.Tables[0].Rows[0]["AttachID"].ToString());                
            } 
            //Parse Attachment.
            attachment = attachment.Replace("=0A=\r\n", "\n");
            attachment = attachment.Replace("=\r\n", string.Empty);
            attachment = attachment.Replace("=0A=", string.Empty);
            attachment = attachment.Replace("=20", "\n");
            attachment = attachment.Replace("\r", string.Empty);
            int startIndex = attachment.IndexOf(EDI.Constant.Constants.NEWLINE) + EDI.Constant.Constants.NEWLINE.Length;
            int endIndex = attachment.IndexOf(EDI.Constant.Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachment.Length : endIndex;
            StringBuilder sb = new StringBuilder();
            string[] headerLine = new string[5];
            headerLine[0] = attachment.Substring(0, startIndex - EDI.Constant.Constants.NEWLINE.Length);
            sb.AppendLine(headerLine[0]);
            while (endIndex <= (attachment.Length))
            {                
                string[] stream = attachment.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());
                //For eBay.
                if (stream[0].Trim().Equals("eBay1"))
                {
                    //Update eBay1 stream array.
                    headerLine[1] = stream[0] + "|" + stream[1] + "|" + stream[2] + "|" + this.textBoxReferenceNumber.Text.Trim() + "|"
                                 + stream[4] + "|" + stream[5] + "|" + this.textBoxToCompany.Text.Trim() + "|" + this.textBoxPhone.Text.Trim() +
                                 "|" + this.textBoxAddr1.Text + "|" + this.textBoxAddr2.Text + "|" + this.textBoxCity.Text + "|"
                                 + this.textBoxState.Text.Trim() + "|" + this.textBoxPostalCode.Text.Trim() + "|" + this.textBoxCountry.Text.Trim() +
                                 "|" + stream[14] + "|" + stream[15] + "|" + stream[16] + "|" + stream[17] + "|" + stream[18]
                                 + "|" + stream[19] + "|" + stream[20] + "|" + stream[21] + "|" + this.textBoxFromCompany.Text.Trim()
                                 + "|" + this.textBoxFromPhone.Text.Trim() + "|" + this.textBoxPrefix.Text.Trim() + "|" + this.textBoxName.Text.Trim()
                                 + "|" + this.textBoxPickupAddr1.Text.Trim() + "|" + this.textBoxPickupAddr2.Text.Trim() + "|" +
                                 this.textBoxPickupCity.Text.Trim() + "|" + this.textBoxPickupState.Text.Trim() + "|" + this.textBoxPostalCode.Text.Trim()
                                 + "|" + this.textBoxInstruction1.Text.Trim() + "|" + this.textBoxInstruction2.Text.Trim() + "|" +
                                 this.textBoxInstruction3.Text.Trim() + "|" + this.comboBoxServiceSelected.SelectedIndex.ToString().Trim() + "|"
                                 + this.textBoxBookedBy.Text.Trim() + "|" + dateTimePicker.Value.ToString() + "|" + this.labelDateTime1.Text.Trim()
                                 + "|" + this.labelJobDispatched.Text.Trim() + "|" + this.textBoxEmail.Text.Trim() + "\n"; 
                }
                if (stream[0].Trim().Equals("eBay2"))
                {
                    headerLine[2] = attachment.Substring(startIndex, endIndex - startIndex);                   
                }
                //For OSCommerce.
                if (stream[0].Trim().Equals("OSCommerce1"))
                {
                    //Update OSCommerce stream array.
                    headerLine[1] = stream[0] + "|" + this.textBoxReferenceNumber.Text.Trim() + "|" + this.textBoxToCompany.Text.Trim()
                                 + "|" + this.textBoxPickupAddr1.Text.Trim() + "|" + this.textBoxPickupAddr2.Text.Trim() + "|" +
                                 this.textBoxCity.Text.Trim() + "|" + this.textBoxPostalCode.Text.Trim() + "|" + this.textBoxState.Text.Trim()
                                 + "|" + this.textBoxCountry.Text.Trim() + "|" + this.textBoxPhone.Text.Trim() + "|" + stream[10] + "|" +
                                 stream[11] + "|" + stream[12] + "|" + stream[13] + "|" + stream[14] + "|" + stream[15] + "|" + stream[16]
                                 + "|" + this.textBoxFromCompany.Text.Trim() + "|" + this.textBoxFromPhone.Text.Trim() + "|" +
                                 this.textBoxPrefix.Text.Trim() + "|" + this.textBoxName.Text.Trim() + "|" + this.textBoxPickupAddr1.Text.Trim()
                                 + "|" + this.textBoxPickupAddr2.Text.Trim() + "|" + this.textBoxPickupCity.Text.Trim() + "|" +
                                 this.textBoxPickupState.Text.Trim() + "|" + this.textBoxPickupPostCode.Text.Trim() + "|" +
                                 this.textBoxInstruction1.Text.Trim() + "|" + this.textBoxInstruction2.Text.Trim() + "|" +
                                 this.textBoxInstruction3.Text.Trim() + "|" + this.comboBoxServiceSelected.SelectedIndex.ToString() + "|"
                                 + this.textBoxBookedBy.Text.Trim() + "|" + this.dateTimePicker.Value.ToString() + "|" +
                                 this.labelDateTime1.Text.Trim() + "|" + this.labelJobDispatched.Text.Trim() + "|" + this.textBoxEmail.Text.Trim() + "\n";
                }
                if (stream[0].Trim().Equals("OSCommerce2"))
                {
                    headerLine[2] = attachment.Substring(startIndex, endIndex - startIndex);
                }
                startIndex = endIndex + EDI.Constant.Constants.NEWLINE.Length;
                if (startIndex < attachment.Length)
                {
                    endIndex = attachment.IndexOf(EDI.Constant.Constants.NEWLINE, startIndex);
                    endIndex = endIndex == -1 ? attachment.Length : endIndex;
                }
                else
                {
                    sb.AppendLine(headerLine[1]);
                    sb.AppendLine(headerLine[2]);
                    string attachData = sb.ToString();
                    try
                    {
                        //Update Attachment in database.
                        DBConnection.MySqlDataAcess.ExecuteNonQuery(string.Format(SQLQueries.Queries.UpdateAttachFile, attachData, attachID));
                    }
                    catch (Exception ex)
                    {
                        CommonUtilities.WriteErrorLog(ex.Message);
                        CommonUtilities.WriteErrorLog(ex.StackTrace);
                    }
                    break;
                }               
            }     
           
        }
		
		
        /// <summary>
        /// Set the current user Defined values on Shippping Request form to
        /// BarcodeLib for printing Shipping Label.
        /// </summary>
        private bool SetShippingLabelValues(string consignmentNumber, int currentItem)
        {          

            string HeaderPort = string.Empty;
            string manDate = DateTime.Now.ToString("dd/MM/yyyy");
            string city = string.Empty;           
            tsc.ActiveXdownloadpcx(Application.StartupPath + Constants.ULPCXFilePath, "UL.PCX");
            //Setup height, width and other parameter for Shipping Label.
            tsc.ActiveXsetup("98", "148", "5", "8", "0", "0", "0");
            //tsc.ActiveXformfeed();
            //tsc.ActiveXnobackfeed();
            //Set the Direction to 1.
            tsc.ActiveXsendcommand("DIRECTION 1[,0]");
            tsc.ActiveXsendcommand("GAP 4,0");
            //tsc.ActiveXsendcommand("SET GAP 0");
            //tsc.ActiveXsendcommand("SET TEAR ON");
            tsc.ActiveXclearbuffer();
            tsc.ActiveXwindowsfont(24, 1120, 60, 90, 2, 0, "Arial", "Allied Express");
            tsc.ActiveXwindowsfont(96, 1120, 25, 90, 0, 0, "Arial", "From:");
            tsc.ActiveXwindowsfont(96, 1056, 30, 90, 0, 0, "Arial", this.textBoxFromCompany.Text.Trim());
            tsc.ActiveXwindowsfont(128, 1120, 25, 90, 0, 0, "Arial", "Contact:");
            tsc.ActiveXwindowsfont(128, 1024, 30, 90, 0, 0, "Arial", this.textBoxName.Text.Trim());
            tsc.ActiveXwindowsfont(160, 1120, 25, 90, 0, 0, "Arial", "ConNote:");
            //CanNote Number.           
            tsc.ActiveXwindowsfont(160, 1024, 30, 90, 0, 0, "Arial", consignmentNumber.Substring(0, 9));
            tsc.ActiveXwindowsfont(96, 624, 25, 90, 0, 0, "Arial", "Phone:");
            tsc.ActiveXwindowsfont(96, 528, 30, 90, 0, 0, "Arial", this.textBoxFromPhone.Text.Trim());
            tsc.ActiveXwindowsfont(128, 624, 25, 90, 0, 0, "Arial", "Phone:");
            tsc.ActiveXwindowsfont(128, 528, 30, 90, 0, 0, "Arial", this.textBoxPhone.Text.Trim());
            tsc.ActiveXwindowsfont(160, 752, 20, 90, 0, 0, "Arial", "Order Ref:");
            tsc.ActiveXwindowsfont(160, 624, 35, 90, 0, 0, "Arial", this.textBoxReferenceNumber.Text.Trim());
            tsc.ActiveXwindowsfont(96, 320, 25, 90, 0, 0, "Arial", "Date:");
            tsc.ActiveXwindowsfont(96, 256, 30, 90, 0, 0, "Arial", manDate);
            tsc.ActiveXwindowsfont(160, 328, 20, 90, 0, 0, "Arial", "Package:");
            tsc.ActiveXwindowsfont(160, 248, 35, 90, 0, 0, "Arial", currentItem.ToString());
            tsc.ActiveXwindowsfont(160, 168, 35, 90, 0, 0, "Arial", "of");
            tsc.ActiveXwindowsfont(160, 80, 35, 90, 0, 0, "Arial", totalItems.ToString());
            tsc.ActiveXbarcode("208", "1120", "39", "160", "0", "270", "3", "10", consignmentNumber);
            //Consignment number.
            tsc.ActiveXwindowsfont(368, 1120, 40, 90, 2, 0, "Arial", consignmentNumber);

            //Service Type.
            tsc.ActiveXwindowsfont(376, 768, 20, 90, 0, 0, "Arial", "Service Type:");
            if (this.comboBoxServiceSelected.SelectedIndex > 0)
            {
                string[] level = this.comboBoxServiceSelected.SelectedItem.ToString().Split('(');
                string[] level1 = level[1].ToString().Split(')');
                tsc.ActiveXwindowsfont(376, 640, 35, 90, 2, 0, "Arial", level1[0].ToString().ToUpper());
            }
            tsc.ActiveXwindowsfont(376, 256, 25, 90, 0, 0, "Arial", "Weight:");
            tsc.ActiveXwindowsfont(376, 176, 30, 90, 0, 0, "Arial", totalWeight.ToString());
            tsc.ActiveXwindowsfont(400, 336, 25, 90, 0, 0, "Arial", "Package:");
            tsc.ActiveXwindowsfont(400, 248, 35, 90, 0, 0, "Arial", currentItem.ToString());
            tsc.ActiveXwindowsfont(400, 168, 35, 90, 0, 0, "Arial", "of");
            //tsc.ActiveXwindowsfont(400, 80, 35, 90, 0, 0, "Arial", totalItems.ToString());
            tsc.ActiveXwindowsfont(400, 80, 35, 90, 0, 0, "Arial", (dataGridViewPackages.Rows.Count - 1).ToString());
            //Address Line.
            tsc.ActiveXwindowsfont(416, 1120, 25, 90, 0, 0, "Arial", "To:");
            tsc.ActiveXwindowsfont(416, 1080, 25, 90, 0, 0, "Arial", this.textBoxName.Text.Trim().ToUpper());
            tsc.ActiveXwindowsfont(448, 1120, 25, 90, 0, 0, "Arial", this.textBoxAddr1.Text.Trim().ToUpper());
            if (textBoxAddr2.Text.Trim() != string.Empty)
            {
                tsc.ActiveXwindowsfont(480, 1120, 25, 90, 0, 0, "Arial", this.textBoxAddr2.Text.Trim().ToUpper());
            }
            //Consolidated Code.           
            //Use Reverse command to print black background.
            //Consolidated code
            if (comboBoxServiceSelected.SelectedItem.ToString() == Constants.OvernightExpress || comboBoxServiceSelected.SelectedItem.ToString() == Constants.International)
            {
                //consolidated code = EXP
                tsc.ActiveXsendcommand("TEXT 448,336,\"5\",270,1,1,\"EXP\"");
                tsc.ActiveXsendcommand("REVERSE 443,230,55,112");
            }
            if (comboBoxServiceSelected.SelectedItem.ToString() == Constants.Local || comboBoxServiceSelected.SelectedItem.ToString() == Constants.RoadExpress || comboBoxServiceSelected.SelectedItem.ToString() == Constants.Pallet)
            {
                //Consolidated code = ROAD
                tsc.ActiveXsendcommand("TEXT 448,336,\"5\",270,1,1,\"ROAD\"");
                tsc.ActiveXsendcommand("REVERSE 443,200,55,142");
            }
            if (comboBoxServiceSelected.SelectedItem.ToString() == Constants.SameDay || comboBoxServiceSelected.SelectedItem.ToString() == Constants.Saturday)
            {
                //Consolidated code = AIR
                tsc.ActiveXsendcommand("TEXT 448,336,\"5\",270,1,1,\"AIR\"");
                tsc.ActiveXsendcommand("REVERSE 443,230,55,112");
            }

            //Header port
            HeaderPort = dictionary.GetKeyValue(textBoxState.Text.Trim().ToUpper() + "," + textBoxPostalCode.Text.Trim() + "," + textBoxCity.Text.Trim().ToUpper());
            tsc.ActiveXwindowsfont(496, 344, 60, 90, 2, 0, "Arial", HeaderPort);
            if (HeaderPort == "Not Found")
            {
                return false;
            }

            tsc.ActiveXwindowsfont(488, 192, 25, 90, 0, 0, "Arial", "Date:");
            tsc.ActiveXwindowsfont(512, 192, 35, 90, 0, 0, "Arial", manDate);

            //State, City and postal code.
         
            tsc.ActiveXwindowsfont(536, 1120, 35, 90, 2, 0, "Arial", this.textBoxCity.Text.Trim().ToUpper());
          
            tsc.ActiveXwindowsfont(536, 368, 60, 90, 2, 0, "Arial", this.textBoxState.Text.Trim().ToUpper());
            tsc.ActiveXwindowsfont(536, 144, 60, 90, 2, 0, "Arial", this.textBoxPostalCode.Text.Trim());

            //Instructions.
            tsc.ActiveXwindowsfont(584, 1120, 25, 90, 0, 0, "Arial", "Special Instructions:");
            tsc.ActiveXwindowsfont(608, 1120, 25, 90, 0, 0, "Arial", this.textBoxInstruction1.Text.Trim().ToUpper());
            tsc.ActiveXwindowsfont(632, 1120, 25, 90, 0, 0, "Arial", this.textBoxInstruction2.Text.Trim().ToUpper());
            tsc.ActiveXwindowsfont(656, 1120, 25, 90, 0, 0, "Arial", this.textBoxInstruction3.Text.Trim().ToUpper());

            //Bottom Part
            tsc.ActiveXwindowsfont(632, 168, 30, 90, 0, 0, "Arial", "Date");
            tsc.ActiveXwindowsfont(696, 1120, 30, 90, 0, 0, "Arial", "Name");
            tsc.ActiveXwindowsfont(696, 752, 30, 90, 0, 0, "Arial", "Signature");
            tsc.ActiveXwindowsfont(696, 168, 30, 90, 0, 0, "Arial", "Time");

            //Bar            
            tsc.ActiveXsendcommand("BAR 728, 30, 5, 1100");

            //Peelable Section
            tsc.ActiveXwindowsfont(736, 1120, 25, 90, 0, 0, "Arial", "ALLIED EXPRESS");
            tsc.ActiveXwindowsfont(736, 800, 25, 90, 0, 0, "Arial", "RECEIVED IN GOOD CONDITION.SUBJECT TO CARRIER TERMS");
            //tsc.ActiveXwindowsfont(736, 368, 30, 90, 0, 0, "Arial", "SUBJECT TO CARRIER TERMS AND CONDITION.");            

            //tsc.ActiveXprintlabel("1", "1");
            //tsc.ActiveXcloseport();  

            return true;
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Parse the eBay/OSCommerce order.
        /// </summary>
        /// <param name="attachmentContent"></param>
        /// <param name="companyName"></param>
        /// <param name="accountNumber"></param>
        /// <param name="companyPhone"></param>
        /// <param name="prefix">prefix of cannote specif by user in TPAB</param>
        public void ParseOrder(string attachmentContent, string companyName , string accountNumber)
        {           
            alliedExpress = new AlliedExpress();     

            //Assign Trading Partner Address Book details to alliedExpress.     
            alliedExpress.AccountNumber = accountNumber;
            alliedExpress.FromCompanyName = companyName;
            alliedExpress.FromCompanyPhone = Convert.ToString(MySqlDataAcess.ExecuteScaler(SQLQueries.Queries.SQ061));
            alliedExpress.Prefix = Convert.ToString(MySqlDataAcess.ExecuteScaler(SQLQueries.Queries.SQ062));
            object connectionType = MySqlDataAcess.ExecuteScaler(SQLQueries.Queries.SQ063);
            if (connectionType == DBNull.Value)
            {
                connectionType = "Test";
            }
            alliedExpress.Type = connectionType.ToString();
            alliedExpress.AccountCode =Convert.ToString( MySqlDataAcess.ExecuteScaler(SQLQueries.Queries.SQ064));
            object AEState = MySqlDataAcess.ExecuteScaler(SQLQueries.Queries.SQ065);
            if (AEState == DBNull.Value)
            {
                //Default first value in combboBox.
                AEState = "NSW";
            }
            alliedExpress.AESTate = AEState.ToString();

            int startIndex = attachmentContent.IndexOf(EDI.Constant.Constants.NEWLINE) + EDI.Constant.Constants.NEWLINE.Length;
            int endIndex = attachmentContent.IndexOf(EDI.Constant.Constants.NEWLINE, startIndex);
            endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
            while (endIndex <= (attachmentContent.Length))
            {
                string[] stream = attachmentContent.Substring(startIndex, endIndex - startIndex).Split("|".ToCharArray());
                //For eBay.
                if (stream[0].Trim().Equals("eBay1"))
                {
                    if (IsSentItemOpen)
                    {
                        if (stream.Length < 23)
                        {
                            MessageBox.Show("Attachment is not in proper format.", EDI.Constant.Constants.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (!AddEbay1(stream))
                    {
                        throw new Exception("Error in parsing eBay stream in the attachment");
                    }                   
                }
                else if (stream[0].Trim().Equals("eBay2"))
                {
                    if (!AddEbay2(stream))
                    {
                        throw new Exception("Error in parsing eBay Item stream in the attachment");
                    }                                        
                }
                //For OSCommerce.
                if (stream[0].Trim().Equals("OSCommerce1"))
                {
                    if (IsSentItemOpen)
                    {
                        if (stream.Length < 18)
                        {
                            MessageBox.Show("Attachment is not in proper format.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                    if (!AddOSCommerce1(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }
                else if (stream[0].Trim().Equals("OSCommerce2"))
                {
                    if (!AddOSCommerce2(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce Item stream in the attachment");
                    }
                }

                //new changes in version 6.0 for Sales Receipt
                if (stream[0].Trim().Equals("SR1"))
                {
                    if (!AddSR1(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }
                else if (stream[0].Trim().Equals("SR2"))
                {
                    if (!AddSR2(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }

                //new changes in version 6.0 sales order
                if (stream[0].Trim().Equals("OR1"))
                {
                    if (!AddOR1(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }
                else if (stream[0].Trim().Equals("OR2"))
                {
                    if (!AddOR2(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }

                //new changes in version 6.0 Invoice
                if (stream[0].Trim().Equals("PC1"))
                {
                    if (!AddPC1(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }
                else if (stream[0].Trim().Equals("PC2"))
                {
                    if (!AddPC2(stream))
                    {
                        throw new Exception("Error in parsing OSCommerce stream in the attachment");
                    }
                }


                startIndex = endIndex + EDI.Constant.Constants.NEWLINE.Length;
                if (startIndex < attachmentContent.Length)
                {
                    endIndex = attachmentContent.IndexOf(EDI.Constant.Constants.NEWLINE, startIndex);
                    endIndex = endIndex == -1 ? attachmentContent.Length : endIndex;
                }
                else
                {
                    break;
                }
            }           
        }

        #endregion

        #region Events Handler

        private void ShippingRequest_Load(object sender, EventArgs e)
        {
            dataGridViewPackages.DataSource = null;
            this.DataGridColumnBinding();
            this.PopulateServiceLevel();
            this.PopulateShippingRequest();
            this.richTextBoxMessageSummary.SelectionAlignment = HorizontalAlignment.Center;
        }
      
        private void dataGridViewPackages_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //Assgin a selected row value to Itempackage.
            ItemPackage item = ItemPackage.GetInstance();
            item.RowIndex = e.RowIndex;
            if (e.RowIndex != -1)
            {
                try
                {
                    DataRow dataRow;
                    DataGridViewSelectedRowCollection datagridviewSelectRows = dataGridViewPackages.SelectedRows;
                    //DataRowView dataRowView = (DataRowView)dataGridViewPackages.SelectedRows[e.RowIndex].DataBoundItem;
                    DataRowView dataRowView = (DataRowView)datagridviewSelectRows[0].DataBoundItem;
                    dataRow = dataRowView.Row;
                }
                catch
                {
                    MessageBox.Show("Please Select a Row containing data.",Constants.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Warning);
                    return;
                }
                finally
                {
                    item.ModifiedItem = ItemDataTable;
                }

                dataGridViewPackages.Columns["DangerousGood"].Visible = true;
                if (m_IsSentItemOpen)
                {
                    ItemPackage.GetInstance().IsSentItemOpen = true;
                }
                ItemPackage.GetInstance().ShowDialog();

                item.Dispose();
            }
            else
            {
                MessageBox.Show("Please Select a Row.",Constants.ProductName,MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return;            
            }
        }
        
        /// <summary>
        /// On save Booking(job) is created and send to Allied Express.
        /// On sucess Notification are also send to Ebay/OSCommerce.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSave_Click(object sender, EventArgs e)
        {
            dataGridViewPackages.Refresh();
            totalItems = 0;
            totalVolumes = 0;
            totalWeight = 0;
            //Get Job Booked time.
            DateTime bookedTime = DateTime.Now;
            richTextBoxMessageSummary.Text = string.Empty;
            richTextBoxMessageSummary.Text = "Please Wait...";
            Cursor.Current = Cursors.WaitCursor;
            //Get the country name from localized settings.
            countryName = System.Globalization.RegionInfo.CurrentRegion.DisplayName;

            #region Store values in registry.
            //Save CustomerName in Registry.
            try
            {
                UserSettings.StoreValueInRegistry("BookedByName", textBoxBookedBy.Text, "Shipping Request");
                UserSettings.StoreValueInRegistry("PickupAddress1", textBoxPickupAddr1.Text, "Shipping Request");
                UserSettings.StoreValueInRegistry("PickupAddress2", textBoxPickupAddr2.Text, "Shipping Request");
                UserSettings.StoreValueInRegistry("PickupCity", textBoxPickupCity.Text, "Shipping Request");
                UserSettings.StoreValueInRegistry("PickupState", textBoxPickupState.Text, "Shipping Request");
                UserSettings.StoreValueInRegistry("PickupPostcode", textBoxPostalCode.Text, "Shipping Request");
                UserSettings.StoreValueInRegistry("ServiceSelected", comboBoxServiceSelected.SelectedIndex.ToString(), "Shipping Request");
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
            }

            #endregion

            #region Validation
            //If there is no item present in Order.
            if (dataGridViewPackages.Enabled == false)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show(DataProcessingBlocks.MessageCodes.GetValue("Zed Axis MSG123"), EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (this.textBoxName.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Customer Name.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxName.Focus();
                return;
            }
            if (this.textBoxToCompany.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter To Company Name.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxToCompany.Focus();
                return;
            }
            if (textBoxPickupAddr1.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Pickup Address.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxPickupAddr1.Focus();
                return;
            }
            if (textBoxPickupCity.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Pickup City.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxPickupCity.Focus();
                return;
            }
            if (textBoxPickupState.Text.Trim() == string.Empty || textBoxPickupState.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Pickup State and Postal Code.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (textBoxPickupState.Text.Trim() == string.Empty)
                {
                    textBoxPickupState.Focus();
                }
                else
                {
                    textBoxPickupPostCode.Focus();
                }
                return;
            }
            if (this.textBoxAddr1.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Delivery Address.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxAddr1.Focus();
                return;
            }
            if (this.textBoxCity.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Delivery City.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxCity.Focus();
                return;
            }
            if (this.textBoxState.Text.Trim() == string.Empty || this.textBoxPostalCode.Text == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Delivery State and Postal Code.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                if (textBoxPickupState.Text.Trim() == string.Empty)
                {
                    textBoxState.Focus();
                }
                else
                {
                    textBoxPostalCode.Focus();
                }
                return;
            }
            if (this.textBoxPhone.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter the Phone No.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxPhone.Focus();
                return;
            }
            if (comboBoxServiceSelected.SelectedIndex == 0)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Select Service Level.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBoxServiceSelected.Focus();
                return;
            }

            if (textBoxReferenceNumber.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter Reference Number.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxReferenceNumber.Focus();
                return;
            }

            if (this.textBoxBookedBy.Text.Trim() == string.Empty)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Please Enter value for Booked By.", EDI.Constant.Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxBookedBy.Focus();
                return;
            }

            #endregion

            #region Create and Update Items.

            foreach (DataGridViewRow dataGridRowView in dataGridViewPackages.Rows)
            {
                try
                {
                    DataRowView dataRowView = (DataRowView)dataGridRowView.DataBoundItem;
                    DataRow row = dataRowView.Row;
                    string item = row["itemName"].ToString();
                    length = row["length"].ToString();
                    width = row["width"].ToString();
                    height = row["height"].ToString();
                    weight = row["weight"].ToString();

                    if (m_connectedSoftware == EDI.Constant.Constants.QBstring)
                    {
                        //Constructing an item Name with 31 digit
                        string[] arr = new string[15];
                        arr = item.Split(':');
                        item = string.Empty;
                        for (int i = 0; i < arr.Length; i++)
                        {
                            if (arr[i].Length > 31)
                            {
                                arr[i] = arr[i].Substring(0, 30);
                            }
                            if (i == (arr.Length - 1))
                            {
                                item += arr[i];
                            }
                            else
                            {
                                item += arr[i] + ":";
                            }
                        }
                        //Check if item exist 
                        if (QBMethods.ItemQuery(item, m_QbCompanyFile))
                        {
                            //Update item in Quickbooks.
                            QBMethods.UpdateQBItem(item, m_QbCompanyFile, length, width, height, weight);
                        }
                        else
                        {
                            //Create item in Quickbooks.
                            QBMethods.CreateQBItem(item, m_QbCompanyFile, length, width, height, weight);
                        }
                    }
                }
                catch//To catch if DataboundItem is null.
                { }
            }

            #endregion


            try
            {
                #region Construct Job and Save
                if (alliedExpress.Type == "Test")
                {
                    //Construct job
                    ConstructTestJob();
                    //Validate Job
                    jobTest = serviceTest.validateBooking(alliedExpress.AccountNumber, jobTest);

                    if (jobTest.jobErrors == null || jobTest.jobErrors[0] == null)
                    {
                        //Get Job Status.
                        this.richTextBoxMessageSummary.Text = jobTest.jobStatus;
                        //Job Validated.
                        //Save Pending job.
                        serviceTest.savePendingJob(alliedExpress.AccountNumber, jobTest);
                        //Get Pending Job.
                        TnTWebServiceTesting.Job[] jobs = serviceTest.getPendingJobs(alliedExpress.AccountNumber, accountTest);
                        if (jobs != null && jobs.Length > 0)
                        {
                            int i = 0;
                            int[] jobIDs = new int[jobs.Length];
                            //Dispatch job.
                            foreach (TnTWebServiceTesting.Job j in jobs)
                            {
                                jobIDs[i++] = j.jobNumber;
                            }
                            jobs = serviceTest.dispatchPendingJobs(alliedExpress.AccountNumber, jobIDs);
                        }
                        //Booking is successfully send to Allied Express.
                        richTextBoxMessageSummary.Text = "Booking has been send successfully to Allied Express.";

                    }
                    else
                    {
                        //Job not validated.
                        string[] errors = new string[jobTest.jobErrors.Length];
                        int i = 0;
                        //Get Job errors.
                        foreach (TnTWebServiceTesting.JobError jE in jobTest.jobErrors)
                        {
                            errors[i++] = jE.errorMsg;
                        }
                        this.richTextBoxMessageSummary.Text = string.Empty;
                        this.richTextBoxMessageSummary.Lines = errors;
                        return;
                    }
                }
                else //if AlliedExpress.Type = "Production"
                {
                    //Construct job
                    ConstructProductionJob();
                    //Validate Job
                    jobProduction = serviceProduction.validateBooking(alliedExpress.AccountNumber, jobProduction);

                    if (jobProduction.jobErrors == null || jobProduction.jobErrors[0] == null)
                    {
                        //Get Job Status.
                        this.richTextBoxMessageSummary.Text = jobProduction.jobStatus;
                        //Job Validated.
                        //Save Pending job.
                        serviceProduction.savePendingJob(alliedExpress.AccountNumber, jobProduction);
                        //Get Pending Job.
                        TnTWebServiceProduction.Job[] jobs = serviceProduction.getPendingJobs(alliedExpress.AccountNumber, accountProduction);
                        if (jobs != null && jobs.Length > 0)
                        {
                            int i = 0;
                            int[] jobIDs = new int[jobs.Length];
                            //Dispatch job.
                            foreach (TnTWebServiceProduction.Job j in jobs)
                            {
                                jobIDs[i++] = j.jobNumber;
                            }
                            jobs = serviceProduction.dispatchPendingJobs(alliedExpress.AccountNumber, jobIDs);
                        }
                        //Booking is successfully send to Allied Express.
                        richTextBoxMessageSummary.Text = "Booking has been send successfully to Allied Express.";
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                    }
                    else
                    {
                        //Job not validated.
                        string[] errors = new string[jobProduction.jobErrors.Length];
                        int i = 0;
                        //Get Job errors.
                        foreach (TnTWebServiceProduction.JobError jE in jobProduction.jobErrors)
                        {
                            errors[i++] = jE.errorMsg;
                        }
                        this.richTextBoxMessageSummary.Text = string.Empty;
                        this.richTextBoxMessageSummary.Lines = errors;
                        return;
                    }
                }

                #endregion

                #region Notify Ebay or OSCommerce.

                //Get job Dispatched time.
                if (countryName == "United States")
                {
                    this.labelJobDispatched.Text = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
                }
                else
                {
                    this.labelJobDispatched.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                }

                alliedExpress.ShippedTIme = DateTime.Now;
                //Send Notification to Ebay/OSCommerce
                object tradingPartnerID = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.TPGetTradingPartnerID, m_messageID.ToString()));
                string AckMessage = string.Empty;                
                consignmentNote = GenerateConsignmentNote();
                if (m_mailProtocol == "eBay")
                {
                    string ebayAuthToken = (string)DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQ058, tradingPartnerID.ToString()));

                    //Get the eBAyType.
                    object ebayType = DBConnection.MySqlDataAcess.ExecuteScaler(string.Format(SQLQueries.Queries.SQ0EbayType, tradingPartnerID.ToString()));

                    if (ebayType == DBNull.Value)
                        ebayType = "SandBox";

                    //For mulitple itemID Notification is send to eBay with different itemID.
                    for (int i = 0; i < alliedExpress.ItemsArray.Count; i++)
                    {
                        AckMessage = eBayLibrary.GetInstance().CompleteSale(alliedExpress.ItemsArray[i].ItemID, alliedExpress.RefNumber, alliedExpress.ShippedTIme, true, alliedExpress.TransactionID, ebayAuthToken, consignmentNote, ebayType.ToString());
                    }
                }
                else//If OSCommerce
                {
                    AckMessage = Convert.ToString(OSCommerce.GetInstance().UpdateStatus(item.OrderID, tradingPartnerID.ToString(),alliedExpress.ShippedTIme, consignmentNote));
                }
                if (AckMessage == "Success" || AckMessage == "True")
                {
                    if (m_mailProtocol == "eBay")
                    {
                        richTextBoxMessageSummary.Text += "\n" + "Notification has been send successfully to eBay.";
                        buttonPrintLabel.Enabled = true;
                    }
                    else
                    {
                        richTextBoxMessageSummary.Text += "\n" + "Notification has been send successfully to OSCommerce.";
                        buttonPrintLabel.Enabled = true;
                    }
                   
                    //Display the job booked time to label.
                    if (countryName == "United States")
                    {
                        this.labelDateTime1.Text = bookedTime.ToString("MM/dd/yyyy HH:mm");
                    }
                    else
                    {
                        this.labelDateTime1.Text = bookedTime.ToString("dd/MM/yyyy HH:mm");                      
                    }
                    //Update Status of order.
                    eBayLibrary.GetInstance().UpdateStatus(m_messageID, (int)MessageStatus.Dispatched);                   
                   
                    //Save is enabled since Job is Dispatched and send notification.
                    buttonSave.Enabled = false;               
                    UpdateAttachment();
                }
                else
                {
                    labelJobDispatched.Text = string.Empty;
                    //richTextBoxMessageSummary.Text = string.Empty;
                    if (m_mailProtocol == "eBay")
                    {
                        richTextBoxMessageSummary.Text += "\n" + "There was an Error while sending Notification to Ebay:" + AckMessage;
                    }
                    else
                    {
                        richTextBoxMessageSummary.Text += "\n" + "There was an Error while sending Notification to OSCommerce:" + AckMessage + ". Please Check log file.";                    
                    }
                }               
                #endregion
            }
            catch (SoapException ex)
            {          
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Unable to Connect Allied Express. Please check the Log file.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return;
            }
            catch (WebException ex)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show("Error Occurs while acccessing the Network. Please try again.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return;
            }
            catch (Exception ex)
            {
                richTextBoxMessageSummary.Text = string.Empty;
                MessageBox.Show(ex.Message);
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return;
            }
           
            Cursor.Current = Cursors.Default;
        }
       

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
            m_ShippingRequest = null;
        }
               
        private void ShippingRequest_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_ShippingRequest = null;
        }

        private void buttonPrintLabel_Click(object sender, EventArgs e)
        {
            
            bool foundPrinter = false;                     
           
            try
            {
                //Print image to USB  printer
                PrintDocument doc = new PrintDocument();    
                tsc = new TSCLIBClass();
              
                //PrintDialog printerDialog = new PrintDialog();
                //printerDialog.ShowDialog();
                //printerName = printerDialog.PrinterSettings.PrinterName;

                printerName = "TSC TDP-245 Plus";

                ManagementScope scope = new ManagementScope(@"\root\cimv2");
                scope.Connect();

                // Select All Printers from my computer
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
                ManagementObjectCollection collection = searcher.Get();
               
                foreach (ManagementObject printer in collection)
                {
                    if (printer["Name"].ToString().Contains(printerName))//if (printer["Name"].ToString() == printerName)
                    {
                        foundPrinter = true;
                        //Printer is Offline.
                        if (printer["WorkOffline"].ToString().ToLower().Equals("true")) //Check if printer is offline.
                        {
                            MessageBox.Show("Printer is Offline.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        else //Printer is online
                        {
                            doc.PrinterSettings.PrinterName = printerName.ToString();
                            if (Convert.ToInt32(printer.GetPropertyValue("PrinterState")) == 0)
                            {
                                DialogResult dlgResult = MessageBox.Show("Please ensure the Printer Settings are correct before Priting labels.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                if (dlgResult == DialogResult.OK || dlgResult == DialogResult.OK)
                                {
                                    //Open Printer Port.
                                    //tsc.ActiveXabout();
                                    tsc.ActiveXopenport(printerName);            
                                    Cursor.Current = Cursors.WaitCursor;
                                    for (int i = 1; i <= dataGridViewPackages.Rows.Count - 1; i++)
                                    {
                                        consignmentNumber = GenerateConsignmentNumber(i);
                                        //Set values to Barcodelib.
                                        if (!SetShippingLabelValues(consignmentNumber, i))
                                        {
                                            MessageBox.Show("Could not found Header Port for specified Address Details.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return;
                                        }
                                        //Print Label.
                                        tsc.ActiveXprintlabel("1", "1");                                                                            
                                        //Disable Button Print.
                                        buttonPrintLabel.Enabled = false;

                                        //Update parcelD entries in registry.
                                        try
                                        {
                                            //UserSettings.StoreValueInRegistry("ParcelID", parcelID, "Shipping Request");
                                        }
                                        catch (Exception ex)
                                        {
                                            CommonUtilities.WriteErrorLog(ex.Message);
                                            CommonUtilities.WriteErrorLog(ex.StackTrace);
                                        }
                                    }
                                    //Close Printer Port.
                                    tsc.ActiveXcloseport();   
                                    Cursor.Current = Cursors.Default;
                                }                                

                            }//End of For
                            else
                            {
                                MessageBox.Show("Please make sure there are no documents in Printer Queue.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return;
                            }
                        }
                    }
                }
                if (foundPrinter == false)
                {
                    MessageBox.Show("TSC TDP-245 Plus Printer does not exist.", Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
               
            }
            catch (Exception ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                MessageBox.Show(ex.Message, Constants.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

        }

        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
                   
        } 

        private void textBoxPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validnumber;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }

        private void textBoxPostalCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validnumber;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));
        }
		
	    private void textBoxPickupPostCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            string str = EDI.Constant.Constants.validnumber;
            e.Handled = !(str.Contains(e.KeyChar.ToString()));        
        }

        private void textBoxReferenceNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
           string str = EDI.Constant.Constants.validnumber;
           e.Handled = !(str.Contains(e.KeyChar.ToString())); 
        }

        private void textBoxInstruction1_TextChanged(object sender, EventArgs e)
        {
            if (textBoxInstruction1.Text.Trim().Length == textBoxInstruction1.MaxLength)
                textBoxInstruction2.Focus();
        }

        private void textBoxInstruction2_TextChanged(object sender, EventArgs e)
        {
            if (textBoxInstruction2.Text.Trim().Length == textBoxInstruction2.MaxLength)
                textBoxInstruction3.Focus();
            //While backspace focus to previous textbox.
            if (textBoxInstruction2.Text.Trim().Length == 0)
                textBoxInstruction1.Focus();
        }

        private void textBoxInstruction3_TextChanged(object sender, EventArgs e)
        {
            if (textBoxInstruction3.Text.Trim().Length == 0)
                textBoxInstruction2.Focus();
        }            

        
        #endregion
      
    }
}
