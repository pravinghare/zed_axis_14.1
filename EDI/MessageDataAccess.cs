using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Odbc;
using OBDCConnection;
using OB.ApplicationBlocks.Data;
using DataProcessingBlocks;
using SQLQueries;

namespace EDI.Message
{
    /// <summary>
    /// Class contains data acess functions for message class
    /// </summary>
    public class MessageDataAccess
    {
        /// <summary>
        /// Insert a new message into message_schema
        /// </summary>
        /// <param name="message">Message class object</param>
        /// <returns>Boolean value</returns>
        internal static int Save(Message message)
        {
            try
            {
                string commandText = string.Format(Queries.SQ001,
                                                   message.MessageDate,
                                                   message.ArchiveDate,
                                                   message.FromAddress,
                                                   message.ToAddress,
                                                   message.RefMessageId,
                                                   message.Subject,
                                                   message.Body,
                                                   message.Status,
                                                   message.AttachFile,
                                                   message.AttachName);

                return OdbcHelper.ExecuteNonQuery(MySQLConnection.Open(), CommandType.Text, commandText);
                
            }
            catch (OdbcException ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
            finally
            {
                MySQLConnection.Close();
            }


        }
    }
}
