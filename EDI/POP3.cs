using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Net.Security;
using DataProcessingBlocks;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;

namespace EDI.Message
{
	/// <summary>
	/// Class provides methods to connect and retrive information and messages from a pop3 server.
	/// </summary>
	internal class POP3
    {
        #region Private Members

        private string m_pop3host;
        private int m_inPortNo;
        private string m_userName;
        private string m_password;
        private string m_command;
        private TcpClient m_tcpClient;
        private Stream m_networkStream;
        private StreamReader m_streamReader;
        private byte[] m_data;
        private const string CTRLF = "\r\n";
        
        #endregion
        
        #region Public Properties

        /// <summary>
        /// POP3 server/host name
        /// </summary>
        public string Pop3Host
        {
            get { return m_pop3host; }
            set { m_pop3host = value; }
        }

        /// <summary>
        /// Incoming port number
        /// </summary>
        public int InPortNo
        {
            get { return m_inPortNo; }
            set { m_inPortNo = value; }
        }

        /// <summary>
        /// Email address
        /// </summary>
        public string UserName
        {
            get { return m_userName; }
            set { m_userName = value; }
        }

        /// <summary>
        /// Email password
        /// </summary>
        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }

        /// <summary>
        /// POP3 command
        /// </summary>
        public string Command
        {
            get { return m_command; }
            set { m_command = value; }
        }

        /// <summary>
        /// Provides client connections for TCP network services.
        /// </summary>
        public TcpClient TcpClient
        {
            get { return m_tcpClient; }
            set { m_tcpClient = value; }
        }

        /// <summary>
        /// Network Stream for sending and recieving data over stream socket.
        /// </summary>
        public Stream NetworkStream
        {
            get { return m_networkStream; }
            set{m_networkStream = value;}
        }

        /// <summary>
        /// Stream Reader to read data from server.
        /// </summary>
        public StreamReader ReadStream
        {
            get { return m_streamReader; }
            set { m_streamReader = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public byte[] Data
        {
            get { return m_data; }
            set { m_data = value; }

        }
            
        #endregion

        #region Public Methods
        /// <summary>
        /// Creates a connection with the pop3 server by sending the user and password
        /// </summary>
        /// <returns>Response from server</returns>
        public string Connect()
		{
            try
            {
                // create POP3 connection
                this.m_tcpClient = new TcpClient(this.m_pop3host, this.m_inPortNo);

                
                // initialization
                this.m_networkStream = this.m_tcpClient.GetStream();
                this.m_streamReader = new StreamReader(this.m_tcpClient.GetStream());
                this.m_streamReader.ReadLine();

                // send login
                this.m_command = "USER " + this.m_userName + CTRLF;
                this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
                this.m_networkStream.Write(this.m_data, 0, this.m_data.Length);
                this.m_streamReader.ReadLine();

                // send pwd
                this.m_command = "PASS " + this.m_password + CTRLF;
                this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
                this.m_networkStream.Write(this.m_data, 0, this.m_data.Length);
                return m_streamReader.ReadLine();
                

            }
            
           
            catch (SocketException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
           
		}

        public void  SSLConnect()
        {
            // create POP3 connection
            this.m_tcpClient = new TcpClient(this.m_pop3host, this.m_inPortNo);
            
            m_networkStream = new SslStream(m_tcpClient.GetStream(), false);
            ((SslStream)m_networkStream).AuthenticateAsClient(m_pop3host);

            this.m_streamReader = new StreamReader(this.m_tcpClient.GetStream());
                // send login
                this.m_command = "USER " + this.m_userName + CTRLF;
                this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
                m_networkStream.Write(this.m_data, 0, this.m_data.Length);
                byte [] reader = new byte[100];
                m_networkStream.Read(reader, 0, 100);
                string str = ASCIIEncoding.ASCII.GetString(reader);    

                this.m_command = "PASS " + this.m_password + CTRLF;
                this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
                m_networkStream.Write(this.m_data, 0, this.m_data.Length);
                m_networkStream.Read(reader, 0, 100);
                str = ASCIIEncoding.ASCII.GetString(reader);
               
        }

        /// <summary>
        /// Gets number of mail and total size
        /// </summary>
        /// <returns>Respone from server</returns>
		public string GetStat()
		{

            this.m_command = "STAT\r\n";
            this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
            this.m_networkStream.Write(this.m_data, 0, this.m_data.Length);
            byte[] reader = new byte[100];
            m_networkStream.Read(reader, 0, 100);
            return ASCIIEncoding.ASCII.GetString(reader); 
            //return m_streamReader.ReadLine();
		}

        /// <summary>
        /// Gets the content of a mail message
        /// </summary>
        /// <param name="number">Message number</param>
        /// <returns>Contents of the message</returns>
        public string Retr(int number)
        {
            string sTemp;
            string sBody = string.Empty;
            try
            {
                this.m_command = "RETR " + number + CTRLF;
                this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
                this.m_networkStream.Write(this.m_data, 0, this.m_data.Length);

                sTemp = this.m_streamReader.ReadLine();
                if (sTemp != null && sTemp[0] != '-')	//errors begins with -
                {
                    while (sTemp != ".")	// . - is the end of the server response
                    {
                        sBody += sTemp + CTRLF;
                        sTemp = this.m_streamReader.ReadLine();
                    }
                }
                else
                {
                    return sTemp;
                }
            }
            catch (InvalidOperationException ex)
            {
                CommonUtilities.WriteErrorLog(ex.Message);
                CommonUtilities.WriteErrorLog(ex.StackTrace);
                return string.Empty;
            }
          
            return sBody;
           
        }
       
        /// <summary>
        /// Closes the connection with the server
        /// </summary>
        /// <returns>Response from the server</returns>
        public string Quit()
        {
            // Send QUIT
            this.m_command = "QUIT\r\n";
            this.m_data = Encoding.ASCII.GetBytes(this.m_command.ToCharArray());
            this.m_networkStream.Write(this.m_data, 0, this.m_data.Length);
            string tmp = this.m_streamReader.ReadLine();
            this.m_networkStream.Close();
            this.m_streamReader.Close();
            return tmp;
        }

        #endregion

        #region NotUsed
        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
		public string GetList(int num)// Send LIST command with number of a letter
		{
			m_command = "LIST " + num + CTRLF;
			m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
			m_networkStream.Write(m_data,0,m_data.Length);
			return m_streamReader.ReadLine();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetList()// Send LIST command with no parametrs to get all information
        {
            string sTemp;	// For saving 'list' results
            string sList = "";
            m_command = "LIST\r\n";
            m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
            m_networkStream.Write(m_data, 0, m_data.Length);
            sTemp = m_streamReader.ReadLine();

            if (sTemp[0] != '-')	// errors begins with '-'
            {
                while (sTemp != ".")	//saving data to string while not found '.'
                {
                    sList += sTemp + CTRLF;
                    sTemp = m_streamReader.ReadLine();
                }
            }
            else
            {
                return sTemp;
            }
            return sList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
		public string Dele(int num)
		{
			// Send DELE command to delete message with specified number
			m_command = "DELE " + num + CTRLF;				
			m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
			m_networkStream.Write(m_data,0,m_data.Length);
			return m_streamReader.ReadLine();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public string Rset()
		{
			// Send RSET command to unmark all deleteting messages
			m_command = "RSET\r\n";				
			m_data = Encoding.ASCII.GetBytes(m_command.ToCharArray());
			m_networkStream.Write(m_data,0,m_data.Length);
			return m_streamReader.ReadLine();
		}

       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
		public string GetTop(int num)
		{
			string sTemp;
			string sTop = "";
			try
			{
				// retrieve mail with number mail parameter
				m_command = "TOP "+ num+" n\r\n";				
				m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
				m_networkStream.Write(m_data,0,m_data.Length);				

				sTemp = m_streamReader.ReadLine();
				if(sTemp[0] != '-') 
				{
					while(sTemp != ".")
					{
						sTop += sTemp+CTRLF;
						sTemp = m_streamReader.ReadLine();
					}
				}
				else
				{
					return  sTemp;
				}
			}
			catch(InvalidOperationException err)
			{
				return ("Error: "+err.ToString());
			}
			return sTop;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num_mess"></param>
        /// <param name="num_strok"></param>
        /// <returns></returns>
		public string GetTop(int num_mess, int num_strok)
		{
			string sTemp;
			string sTop = "";
			try
			{
				// retrieve mail with number mail parameter
				m_command = "TOP " + num_mess + " " + num_strok + CTRLF;				
				m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
				m_networkStream.Write(m_data,0,m_data.Length);				

				sTemp = m_streamReader.ReadLine();
				if(sTemp[0] != '-') 
				{
					while(sTemp != ".")
					{
						sTop += sTemp+CTRLF;
						sTemp = m_streamReader.ReadLine();
					}
				}
				else
				{
					return sTemp;
				}
			}
			catch(InvalidOperationException err)
			{
				return ("Error: "+err.ToString());
			}
			return sTop;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public string GetUidl()
		{
			string sTemp;
			string sUidl = "";
			m_command = "UIDL\r\n";				
			m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
			m_networkStream.Write(m_data,0,m_data.Length);
			sTemp = m_streamReader.ReadLine();

			if(sTemp[0] != '-')	// errors begins with '-'
			{
				while(sTemp != ".")	//saving data to string while not found '.'
				{
					sUidl += sTemp+CTRLF;
					sTemp = m_streamReader.ReadLine();
				}
			}
			else
			{
				return sTemp;
			}
			return sUidl;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
		public string GetUidl(int num)
		{
			m_command = "UIDL " + num + CTRLF;				
			m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
			m_networkStream.Write(m_data,0,m_data.Length);
			return m_streamReader.ReadLine();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		public string Noop()
		{
			// Send NOOP command to check if we are connected
			m_command = "NOOP\r\n";				
			m_data = System.Text.Encoding.ASCII.GetBytes(m_command.ToCharArray());
			m_networkStream.Write(m_data,0,m_data.Length);
			return m_streamReader.ReadLine();
		}

        #endregion
	}
}
