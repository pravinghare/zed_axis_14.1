////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Developer: Swapnil.
///
///Date: 2/03/2009
///
///Modified By:
///
///Date:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Globalization;
using MailSettings;
using DataProcessingBlocks;



namespace EDI.Message
{
    /// <summary>
    /// Class contains members and properties for messages_schema table.
    /// </summary>
    public class Message
    {
        #region Private Members
        /// <summary>
        /// Message Id
        /// </summary>
        private int m_messageId;
        /// <summary>
        /// Date message was sent
        /// </summary>
        private DateTime m_messageDate;
        /// <summary>
        /// Date message was downloaded
        /// </summary>
        private DateTime m_archiveDate;
        /// <summary>
        /// From address
        /// </summary>
        private string m_fromAddress;
        /// <summary>
        /// To address
        /// </summary>
        private string m_toAddress;
        /// <summary>
        /// Cc addresses
        /// </summary>
        private string m_cc;
        /// <summary>
        /// Refrences message number.
        /// </summary>
        private string m_refMessageId;
        /// <summary>
        /// Subject of mail
        /// </summary>
        private string m_subject;
        /// <summary>
        /// Body of mail
        /// </summary>
        private string m_body;
        /// <summary>
        /// Mail status flag
        /// </summary>
        private string m_status;
        /// <summary>
        /// Mail Attachments;
        /// </summary>
        private List<MailAttachment> mailAttachment;
        /// <summary>
        /// Mail read flag
        /// </summary>
        private int m_readFlag;

        #endregion

        #region Public Properties

        /// <summary>
        /// Internally maintained MessageID, Auto Incremented in DB 
        /// </summary>
        public int MessageID
        {
            get { return m_messageId; }
            set { m_messageId = value; }
        }

        /// <summary>
        /// Date when message was recieved
        /// </summary>
        public DateTime MessageDate
        {
            get { return m_messageDate; }
            set { m_messageDate = Convert.ToDateTime(value); }
        }

        /// <summary>
        /// Date when message was archived
        /// </summary>
        public DateTime ArchiveDate
        {
            get { return m_archiveDate; }
            set { m_archiveDate = value;}
        }

        /// <summary>
        /// Sender email address
        /// </summary>
        public string FromAddress
        {
            get { return m_fromAddress.Replace('\'',' '); }
            set
            {
                if (value.Length <= 100)
                {
                    m_fromAddress = value.Replace("\r\n", " ");
                }
                else
                {
                    m_fromAddress = value.Replace("\r\n", " ").Substring(0, 100);
                }
            }
        }

        /// <summary>
        /// Reciever's email address
        /// </summary>
        public string ToAddress
        {
            get { return m_toAddress.Replace('\'', ' '); }
            set
            {
                if (value.Length <= 400)
                {
                    m_toAddress = value.Replace("\r\n", " ");
                }
                else
                {
                    m_toAddress = value.Replace("\r\n", " ").Substring(0, 400);
                }
            }
        }

        /// <summary>
        /// Cc addresses
        /// </summary>
        public string Cc
        {
            get { return m_cc; }
            set
            {
                if (value.Length <= 400)
                {
                    m_cc = value.Replace("\r\n", " ");
                }
                else
                {
                    m_cc = value.Replace("\r\n", " ").Substring(0, 400);
                }
            }
        }

        /// <summary>
        /// Refrence id of another message
        /// </summary>
        public string RefMessageId
        {
            get { return m_refMessageId; }
            set { m_refMessageId = value; }
        }

        /// <summary>
        /// Subject of the message
        /// </summary>
        public string Subject
        {
            get { return m_subject.Replace('\'', ' '); ; }
            set { m_subject = value; }
        }

        /// <summary>
        /// Body of the message not more than 2000
        /// </summary>
        public string Body
        {
            get { return m_body.Replace('\'', ' '); }
            set {
                //Accept body less than 2000kb as defined in DB
                if (value.Length <= 2000)
                {
                    m_body = value;
                }
                else
                    m_body = value.Substring(0, 2000);
                }
        }

        /// <summary>
        /// Status of the message form MessageStatus enumeration
        /// </summary>
        public string Status
        {
            get { return m_status; }
            set { m_status = value; }
        }

        /// <summary>
        /// Gets or Sets the attachments in the message.
        /// </summary>
        public List<MailAttachment> MailAttachment
        {
            get { return mailAttachment; }
            set { mailAttachment = value; }
        }

        /// <summary>
        /// Status of the message form MessageStatus enumeration
        /// </summary>
        public int ReadFlag
        {
            get { return m_readFlag; }
            set { m_readFlag = value; }
        }
        #endregion

        #region Constructor

        public Message()
        {
            mailAttachment = new List<MailAttachment>();
        }
        #endregion

        #region Public Methods
       

        /// <summary>
        /// Message Rule check for Trading Partner id.
        /// </summary>
        /// <param name="dtTPartner"></param>
        /// <returns></returns>
        public bool IsTradingPartner(string dtTPartner,string ruleName)
        {
            if (!string.IsNullOrEmpty(dtTPartner))
            {
                try
                {
                    if (m_fromAddress.Contains(dtTPartner))
                        return true;
                    else
                    {
                        //CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("TPRuleMsg"));
                        return false;
                    }
                }
                catch 
                {
                    //CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("TPRuleMsg"));
                    return false; 
                }
            }
            return true;
        }

        /// <summary>
        /// Message Rule check for the Subject.
        /// </summary>
        /// <param name="dtSubject"></param>
        /// <returns></returns>
        public bool IsSubjectEqual(string dtSubject, string ruleName)
        {
            if (!string.IsNullOrEmpty(dtSubject))
            {
                try
                {
                    if (m_subject.Equals(dtSubject))
                        return true;
                    else
                    {
                        CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("ExactSubjectRuleMsg"));
                        return false;
                    }
                }
                catch 
                {
                    CommonUtilities.WriteErrorLog(MessageCodes.GetValue(ruleName + "ExactSubjectRuleMsg"));
                    return false; 
                }
            }
            return true;
        }

        /// <summary>
        /// Message rule for specific word.
        /// </summary>
        /// <param name="dtSubMatch"></param>
        /// <returns></returns>
        public bool IsSubjectMatch(string dtSubMatch, string ruleName)
        {
            int result = -1;
            if(!string.IsNullOrEmpty(dtSubMatch))
            {
                try
                {
                    if (result != (CultureInfo.CurrentUICulture.CompareInfo.IndexOf(m_subject, dtSubMatch, CompareOptions.IgnoreCase)))
                        return true;
                    else
                    {
                        CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("SpecificSubjectRuleMsg"));                        
                        return false;
                    }
                }
                catch 
                {
                    CommonUtilities.WriteErrorLog(ruleName + MessageCodes.GetValue("SpecificSubjectRuleMsg"));                                            
                    return false; 
                }
            }
            return true;
        }

        public string UpdateMail(int result)
        {
            string res = string.Empty;
            if (result == 0)
            {
                res = m_status.ToString();

            }
            return res;
        }
        
        #endregion
    }
}
