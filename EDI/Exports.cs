using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Odbc;
using SQLQueries;
using EDI.Constant;
using DataProcessingBlocks;

namespace EDI.Exports
{
    public class Exports
    {
        #region Private Members


        #endregion

        #region Constructor

        public Exports()
        {

        }

        #endregion

        #region Static Methods

        /// <summary>
        /// This method is used for Insert Exported data into MySql.
        /// </summary>
        /// <param name="Filepath">Path of the File</param>
        /// <param name="RefMessageID">Reference messsage id.</param>
        /// <returns></returns>
        public static bool ExportISMtoOutBox(string Filepath, string RefMessageID,string subjectLine)
        {
            try
            {
                if (!File.Exists(Filepath))
                {
                    return false;
                }
                else
                {
                    //Information about files.
                    FileInfo flinfo = new FileInfo(Filepath);

                    //Open file into File stream.                   
                      
                    FileStream ismfileStream = new FileStream(Filepath, FileMode.Open, FileAccess.Read);
                    //Reading File content into Binary.
                    BinaryReader ismfileReader = new BinaryReader(ismfileStream);

                    

                    string Attachname = flinfo.Name;
                    string MessageDate = DateTime.Now.ToString(EDI.Constant.Constants.MySqlDateFormat);
                    //Adding ODBC Parameter for store file.
                    OdbcParameter paramFile = new OdbcParameter(EDI.Constant.Constants.ISMParameter.ToString(), ismfileReader.ReadBytes((int)ismfileStream.Length));


                    //string commandText = string.Format(Queries.SQ001, MessageDate, MessageDate, string.Empty, string.Empty, RefMessageID, subjectLine,
                      //           string.Empty, DataProcessingBlocks.MessageStatus.Draft.ToString(), 0);
                    
                    string commandText = string.Format(Queries.SQ001, MessageDate, MessageDate, string.Empty, string.Empty, RefMessageID, subjectLine,
                               string.Empty, "1", 0);

                    DBConnection.MySqlDataAcess.ExecuteNonQuery(commandText);
                    object messageID = DBConnection.MySqlDataAcess.ExecuteScaler(SQLQueries.Queries.SQ027);
                    string insertMessageCommand=string.Empty;
                    if (messageID != null)

                        insertMessageCommand = string.Format(Queries.SQ028, messageID.ToString(), Attachname, "?", ((int)MessageStatus.Draft).ToString());

                    DBConnection.MySqlDataAcess.InsertISMFile(insertMessageCommand, paramFile);
                    try
                    {
                        File.Delete(Filepath);
                    }
                    catch
                    { }
                    return true;

                }
            }
            catch
            {
                return false;
            }
        }

        #endregion


    }
}
