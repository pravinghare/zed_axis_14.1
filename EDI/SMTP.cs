////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///Developer: Swapnil.
///
///Date: 2/03/2009
///
///Modified By:
///
///Date:
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace EDI.Message
{
    /// <summary>
    /// Class allows to send email using SMTP server
    /// </summary>
    public class SMTP
    {
        public MailMessage Create()
        {
            //Attachment attach = new Attachment();
            MailMessage mailMessage = new MailMessage(new MailAddress("swapnil@omni-bridge.com"), new MailAddress("sandeep@omni-bridge.com"));
            mailMessage.Subject = "Test smtp message";
            mailMessage.Body = "Hi test smtp message body";
            
            return mailMessage;
        }
        public void SendMail()
        {
        SmtpClient smtp = new SmtpClient("mail.omni-bridge.com", 25);
        smtp.Send(Create());
        }

        
    }
}
